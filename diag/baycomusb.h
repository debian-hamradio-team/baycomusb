/*****************************************************************************/

/*
 *      baycomusb.h  --  Baycom USB modem diagnostics utility.
 *
 *      Copyright (C) 1999-2001  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 */

/*****************************************************************************/

#ifndef _BAYCOMUSB_H
#define _BAYCOMUSB_H

/* ---------------------------------------------------------------------- */

#include "sysdeps.h"
#include "usbdrv.h"

/* ---------------------------------------------------------------------- */

#define MODE_NONE       0
#define MODE_FSK        1
#define MODE_EXTERNAL   2
#define MODE_AFSK       3
#define MODE_AUDIO      4
#define MODE_BSCAN      5

struct configentry {
	char serial[32];
	struct {
		unsigned int mode;
		unsigned int fclk;
		unsigned int bitraterx;
		unsigned int bitratetx;
		unsigned int samplerate;
		unsigned int loopback;
		unsigned int pttmute;
		unsigned int filtmode;
		unsigned int gain;
	} adapter;
	struct {
		unsigned int txdelay;
		unsigned int slottime;
		unsigned int ppersistence;
		unsigned int txtail;
		unsigned int fullduplex;
	} chaccess;
	struct {
		char ifname[16];
		char hwaddr[16];
		char ipaddr[16];
		char netmask[16];
		char broadcast[16];
	} ifcfg;
	struct {
		unsigned int output;
		unsigned int direction;
		unsigned int rxc;
		unsigned int txc;
		unsigned int txd;
	} mdisc;
	struct {
		unsigned int ctrl;
	} t7f;
};

extern int config_parse(void);
extern int config_save(void);
extern void config_default(struct configentry *cfg);
extern void config_setfile(const char *name);
extern const char *config_getfile(void);
extern struct configentry *config_entry(unsigned int index);
extern struct configentry *config_findentry(const char *ser);
extern struct configentry *config_lookup(const char *ser);
extern struct configentry *config_newentry(void);
extern int config_deleteentry(struct configentry *cfg);
/* currently only under Windows */
extern int config_setvalue(const char *propname, const char *value);
extern int config_getvalue(const char *propname, char *value, size_t vsize);

/* ---------------------------------------------------------------------- */

#define BAYCOMUSB_VENDORID         0xbac0
#define BAYCOMUSB_PRODUCTID_EMPTY  0x6134
#define BAYCOMUSB_PRODUCTID_FPGALD 0x6135
#define BAYCOMUSB_PRODUCTID_MODEM  0x6136
#define BAYCOMUSB_PRODUCTID_AUDIO  0x6137

/* ---------------------------------------------------------------------- */

extern int adapter_reset(struct usbdevice *dev);
extern int adapter_configure(struct usbdevice *dev);
extern struct configentry *adapter_findconfig(struct usbdevice *dev);

/* ---------------------------------------------------------------------- */

/*
 * description of the test procedures defined
 */

extern const struct usbtests {
	const char *name;
	const char *desc;
	void (*cfg)(struct configentry *);
	int (*func)(struct usbdevice *, struct configentry *);
} usbtests[];

/* ---------------------------------------------------------------------- */

/*
 * provided externally!
 */

extern int lprintf(unsigned vl, const char *format, ...)
__attribute__ ((format (printf, 2, 3)));

extern int tprintf(const char *format, ...)
__attribute__ ((format (printf, 1, 2)));

extern int idle_callback(unsigned us_delay);

/* ---------------------------------------------------------------------- */

extern int snprintpkt(char *buf, size_t sz, const u_int8_t *pkt, unsigned len);

/* ---------------------------------------------------------------------- */
#endif /* _BAYCOMUSB_H */
