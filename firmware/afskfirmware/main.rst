                              1 	.module main
                              2 
                              3 	;; ENDPOINTS
                              4 	;; EP0 in/out   Control
                              5 	;; EP1 in       Interrupt:  Status
                              6 	;;              Byte 0: Modem Status
                              7 	;;                Bit 0-1: Transmitter status
                              8 	;;                         0: idle (off)
                              9 	;;                         1: keyup
                             10 	;;                         2: transmitting packets
                             11 	;;                         3: tail
                             12 	;;                Bit 2:   PTT status (1=on)
                             13 	;;                Bit 3:   DCD
                             14 	;;                Bit 4:   RXB (Rx Packet Ready)
                             15 	;;                Bit 5:   UART transmitter empty
                             16 	;;                Bit 6-7: unused
                             17 	;;              Byte 1: Number of empty 64 byte chunks in TX fifo
                             18 	;;              Byte 2: Number of full 64 byte chunks in RX fifo
                             19 	;;              Byte 3: RSSI value
                             20 	;;              Byte 4:	IRQ count
                             21 	;;              Byte 5-20: (as needed) UART receiver chars
                             22 	;; EP2 out      Packets to be transmitted
                             23 	;; EP2 in       Received packets; note they have the CRC appended
                             24 
                             25 	;; COMMAND LIST
                             26 	;; C0 C0  read status (max. 23 bytes, first 6 same as EP1 in)
                             27 	;; C0 C8  read mode
                             28 	;;          Return:
                             29 	;;            Byte 0: 3 (MODE_AFSK)
                             30 	;; C0 C9  return serial number string
                             31 	;; C0 D0  get/set PTT/DCD/RSSI
                             32 	;;          wIndex = 1:	 set forced ptt to wValue
                             33 	;;          Return:
                             34 	;;            Byte 0: PTT status
                             35 	;;            Byte 1: DCD status
                             36 	;;            Byte 2: RSSI status
                             37 	;; C0 D1  get Bitrate
                             38 	;;          Return:
                             39 	;;            Byte 0-2: TX Bitrate
                             40 	;;            Byte 3-5: RX Bitrate
                             41 	;; 40 D2  set CON/STA led
                             42 	;;          Bits 0-1 of wValue
                             43 	;; 40 D3  send byte to UART
                             44 	;;          Byte in wValue
                             45 	;; C0 D4  get/set modem disconnect port (only if internal modem used, stalls otherwise)
                             46 	;;          wIndex = 1:	 write wValue to output register
                             47 	;;          wIndex = 2:	 write wValue to tristate mask register (1 = input, 0 = output)
                             48 	;;          Return:
                             49 	;;            Byte 0: Modem Disconnect Input
                             50 	;;            Byte 1: Modem Disconnect Output register
                             51 	;;            Byte 2: Modem Disconnect Tristate register
                             52 	;; C0 D5  get/set T7F port
                             53 	;;          wIndex = 1:	 write wValue to T7F output register
                             54 	;;          Return:
                             55 	;;            Byte 0: T7F Input
                             56 	;;            Byte 1: T7F Output register
                             57 
                             58 	;; define code segments link order
                             59 	.area CODE (CODE)
                             60 	.area CSEG (CODE)
                             61 	.area GSINIT (CODE)
                             62 	.area GSINIT2 (CODE)
                             63 
                             64 	;; -----------------------------------------------------
                             65 
                             66 	;; special function registers (which are not predefined)
                    0082     67 	dpl0    = 0x82
                    0083     68 	dph0    = 0x83
                    0084     69 	dpl1    = 0x84
                    0085     70 	dph1    = 0x85
                    0086     71 	dps     = 0x86
                    008E     72 	ckcon   = 0x8E
                    008F     73 	spc_fnc = 0x8F
                    0091     74 	exif    = 0x91
                    0092     75 	mpage   = 0x92
                    0098     76 	scon0   = 0x98
                    0099     77 	sbuf0   = 0x99
                    00C0     78 	scon1   = 0xC0
                    00C1     79 	sbuf1   = 0xC1
                    00D8     80 	eicon   = 0xD8
                    00E8     81 	eie     = 0xE8
                    00F8     82 	eip     = 0xF8
                             83 
                             84 	;; anchor xdata registers
                    7F00     85 	IN0BUF		= 0x7F00
                    7EC0     86 	OUT0BUF		= 0x7EC0
                    7E80     87 	IN1BUF		= 0x7E80
                    7E40     88 	OUT1BUF		= 0x7E40
                    7E00     89 	IN2BUF		= 0x7E00
                    7DC0     90 	OUT2BUF		= 0x7DC0
                    7D80     91 	IN3BUF		= 0x7D80
                    7D40     92 	OUT3BUF		= 0x7D40
                    7D00     93 	IN4BUF		= 0x7D00
                    7CC0     94 	OUT4BUF		= 0x7CC0
                    7C80     95 	IN5BUF		= 0x7C80
                    7C40     96 	OUT5BUF		= 0x7C40
                    7C00     97 	IN6BUF		= 0x7C00
                    7BC0     98 	OUT6BUF		= 0x7BC0
                    7B80     99 	IN7BUF		= 0x7B80
                    7B40    100 	OUT7BUF		= 0x7B40
                    7FE8    101 	SETUPBUF	= 0x7FE8
                    7FE8    102 	SETUPDAT	= 0x7FE8
                            103 
                    7FB4    104 	EP0CS		= 0x7FB4
                    7FB5    105 	IN0BC		= 0x7FB5
                    7FB6    106 	IN1CS		= 0x7FB6
                    7FB7    107 	IN1BC		= 0x7FB7
                    7FB8    108 	IN2CS		= 0x7FB8
                    7FB9    109 	IN2BC		= 0x7FB9
                    7FBA    110 	IN3CS		= 0x7FBA
                    7FBB    111 	IN3BC		= 0x7FBB
                    7FBC    112 	IN4CS		= 0x7FBC
                    7FBD    113 	IN4BC		= 0x7FBD
                    7FBE    114 	IN5CS		= 0x7FBE
                    7FBF    115 	IN5BC		= 0x7FBF
                    7FC0    116 	IN6CS		= 0x7FC0
                    7FC1    117 	IN6BC		= 0x7FC1
                    7FC2    118 	IN7CS		= 0x7FC2
                    7FC3    119 	IN7BC		= 0x7FC3
                    7FC5    120 	OUT0BC		= 0x7FC5
                    7FC6    121 	OUT1CS		= 0x7FC6
                    7FC7    122 	OUT1BC		= 0x7FC7
                    7FC8    123 	OUT2CS		= 0x7FC8
                    7FC9    124 	OUT2BC		= 0x7FC9
                    7FCA    125 	OUT3CS		= 0x7FCA
                    7FCB    126 	OUT3BC		= 0x7FCB
                    7FCC    127 	OUT4CS		= 0x7FCC
                    7FCD    128 	OUT4BC		= 0x7FCD
                    7FCE    129 	OUT5CS		= 0x7FCE
                    7FCF    130 	OUT5BC		= 0x7FCF
                    7FD0    131 	OUT6CS		= 0x7FD0
                    7FD1    132 	OUT6BC		= 0x7FD1
                    7FD2    133 	OUT7CS		= 0x7FD2
                    7FD3    134 	OUT7BC		= 0x7FD3
                            135 
                    7FA8    136 	IVEC		= 0x7FA8
                    7FA9    137 	IN07IRQ		= 0x7FA9
                    7FAA    138 	OUT07IRQ	= 0x7FAA
                    7FAB    139 	USBIRQ		= 0x7FAB
                    7FAC    140 	IN07IEN		= 0x7FAC
                    7FAD    141 	OUT07IEN	= 0x7FAD
                    7FAE    142 	USBIEN		= 0x7FAE
                    7FAF    143 	USBBAV		= 0x7FAF
                    7FB2    144 	BPADDRH		= 0x7FB2
                    7FB3    145 	BPADDRL		= 0x7FB3
                            146 
                    7FD4    147 	SUDPTRH		= 0x7FD4
                    7FD5    148 	SUDPTRL		= 0x7FD5
                    7FD6    149 	USBCS		= 0x7FD6
                    7FD7    150 	TOGCTL		= 0x7FD7
                    7FD8    151 	USBFRAMEL	= 0x7FD8
                    7FD9    152 	USBFRAMEH	= 0x7FD9
                    7FDB    153 	FNADDR		= 0x7FDB
                    7FDD    154 	USBPAIR		= 0x7FDD
                    7FDE    155 	IN07VAL		= 0x7FDE
                    7FDF    156 	OUT07VAL	= 0x7FDF
                    7FE3    157 	AUTOPTRH	= 0x7FE3
                    7FE4    158 	AUTOPTRL	= 0x7FE4
                    7FE5    159 	AUTODATA	= 0x7FE5
                            160 
                            161 	;; isochronous endpoints. only available if ISODISAB=0
                            162 
                    7F60    163 	OUT8DATA	= 0x7F60
                    7F61    164 	OUT9DATA	= 0x7F61
                    7F62    165 	OUT10DATA	= 0x7F62
                    7F63    166 	OUT11DATA	= 0x7F63
                    7F64    167 	OUT12DATA	= 0x7F64
                    7F65    168 	OUT13DATA	= 0x7F65
                    7F66    169 	OUT14DATA	= 0x7F66
                    7F67    170 	OUT15DATA	= 0x7F67
                            171 
                    7F68    172 	IN8DATA		= 0x7F68
                    7F69    173 	IN9DATA		= 0x7F69
                    7F6A    174 	IN10DATA	= 0x7F6A
                    7F6B    175 	IN11DATA	= 0x7F6B
                    7F6C    176 	IN12DATA	= 0x7F6C
                    7F6D    177 	IN13DATA	= 0x7F6D
                    7F6E    178 	IN14DATA	= 0x7F6E
                    7F6F    179 	IN15DATA	= 0x7F6F
                            180 
                    7F70    181 	OUT8BCH		= 0x7F70
                    7F71    182 	OUT8BCL		= 0x7F71
                    7F72    183 	OUT9BCH		= 0x7F72
                    7F73    184 	OUT9BCL		= 0x7F73
                    7F74    185 	OUT10BCH	= 0x7F74
                    7F75    186 	OUT10BCL	= 0x7F75
                    7F76    187 	OUT11BCH	= 0x7F76
                    7F77    188 	OUT11BCL	= 0x7F77
                    7F78    189 	OUT12BCH	= 0x7F78
                    7F79    190 	OUT12BCL	= 0x7F79
                    7F7A    191 	OUT13BCH	= 0x7F7A
                    7F7B    192 	OUT13BCL	= 0x7F7B
                    7F7C    193 	OUT14BCH	= 0x7F7C
                    7F7D    194 	OUT14BCL	= 0x7F7D
                    7F7E    195 	OUT15BCH	= 0x7F7E
                    7F7F    196 	OUT15BCL	= 0x7F7F
                            197 
                    7FF0    198 	OUT8ADDR	= 0x7FF0
                    7FF1    199 	OUT9ADDR	= 0x7FF1
                    7FF2    200 	OUT10ADDR	= 0x7FF2
                    7FF3    201 	OUT11ADDR	= 0x7FF3
                    7FF4    202 	OUT12ADDR	= 0x7FF4
                    7FF5    203 	OUT13ADDR	= 0x7FF5
                    7FF6    204 	OUT14ADDR	= 0x7FF6
                    7FF7    205 	OUT15ADDR	= 0x7FF7
                    7FF8    206 	IN8ADDR		= 0x7FF8
                    7FF9    207 	IN9ADDR		= 0x7FF9
                    7FFA    208 	IN10ADDR	= 0x7FFA
                    7FFB    209 	IN11ADDR	= 0x7FFB
                    7FFC    210 	IN12ADDR	= 0x7FFC
                    7FFD    211 	IN13ADDR	= 0x7FFD
                    7FFE    212 	IN14ADDR	= 0x7FFE
                    7FFF    213 	IN15ADDR	= 0x7FFF
                            214 
                    7FA0    215 	ISOERR		= 0x7FA0
                    7FA1    216 	ISOCTL		= 0x7FA1
                    7FA2    217 	ZBCOUNT		= 0x7FA2
                    7FE0    218 	INISOVAL	= 0x7FE0
                    7FE1    219 	OUTISOVAL	= 0x7FE1
                    7FE2    220 	FASTXFR		= 0x7FE2
                            221 
                            222 	;; CPU control registers
                            223 
                    7F92    224 	CPUCS		= 0x7F92
                            225 
                            226 	;; IO port control registers
                            227 
                    7F93    228 	PORTACFG	= 0x7F93
                    7F94    229 	PORTBCFG	= 0x7F94
                    7F95    230 	PORTCCFG	= 0x7F95
                    7F96    231 	OUTA		= 0x7F96
                    7F97    232 	OUTB		= 0x7F97
                    7F98    233 	OUTC		= 0x7F98
                    7F99    234 	PINSA		= 0x7F99
                    7F9A    235 	PINSB		= 0x7F9A
                    7F9B    236 	PINSC		= 0x7F9B
                    7F9C    237 	OEA		= 0x7F9C
                    7F9D    238 	OEB		= 0x7F9D
                    7F9E    239 	OEC		= 0x7F9E
                            240 
                            241 	;; I2C controller registers
                            242 
                    7FA5    243 	I2CS		= 0x7FA5
                    7FA6    244 	I2DAT		= 0x7FA6
                            245 
                            246 	;; Xilinx FPGA registers
                    C000    247 	FSKSHREG	= 0xc000
                    C001    248 	FSKSHSTATUS	= 0xc001
                    C004    249 	FSKRSSI		= 0xc004
                    C008    250 	FSKCTRL		= 0xc008
                    C009    251 	FSKSTAT		= 0xc009
                    C00A    252 	FSKT7FOUT	= 0xc00a
                    C00B    253 	FSKT7FIN	= 0xc00b
                    C00C    254 	FSKMDISCTRIS	= 0xc00c
                    C00D    255 	FSKMDISCOUT	= 0xc00d
                    C00E    256 	FSKMDISCIN	= 0xc00e
                            257 
                            258 	;; -----------------------------------------------------
                            259 
                            260 	.area CODE (CODE)
   0000 02 0E AB            261 	ljmp	startup
   0003 02 04 3B            262 	ljmp	int0_isr
   0006                     263 	.ds	5
   000B 02 04 5C            264 	ljmp	timer0_isr
   000E                     265 	.ds	5
   0013 02 04 7D            266 	ljmp	int1_isr
   0016                     267 	.ds	5
   001B 02 04 9E            268 	ljmp	timer1_isr
   001E                     269 	.ds	5
   0023 02 04 BF            270 	ljmp	ser0_isr
   0026                     271 	.ds	5
   002B 02 04 FD            272 	ljmp	timer2_isr
   002E                     273 	.ds	5
   0033 02 05 1E            274 	ljmp	resume_isr
   0036                     275 	.ds	5
   003B 02 05 3F            276 	ljmp	ser1_isr
   003E                     277 	.ds	5
   0043 02 01 00            278 	ljmp	usb_isr
   0046                     279 	.ds	5
   004B 02 05 62            280 	ljmp	i2c_isr
   004E                     281 	.ds	5
   0053 02 05 87            282 	ljmp	int4_isr
   0056                     283 	.ds	5
   005B 02 05 AC            284 	ljmp	int5_isr
   005E                     285 	.ds	5
   0063 02 05 D1            286 	ljmp	int6_isr
                            287 
                            288 	;; Parameter block at 0xe0
   0066                     289 	.ds	0x7a
   00E0 B0 04 00            290 parbitratetx:	.db	<1200,>1200,0
   00E3 B0 04 00            291 parbitraterx:	.db	<1200,>1200,0
   00E6 00                  292 parextmodem:	.db	0
   00E7 01                  293 parpttmute:	.db	1
                            294 
                            295 	;; Serial# string at 0xf0
   00E8                     296 	.ds	8
   00F0                     297 parserial:
   00F0 30 30 30 30 30 30   298 	.db	'0,'0,'0,'0,'0,'0,'0,'0,0
        30 30 00
   00F9                     299 	.ds	7
                            300 
                            301 	;; USB interrupt dispatch table
   0100                     302 usb_isr:
   0100 02 05 F2            303 	ljmp	usb_sudav_isr
   0103                     304 	.ds	1
   0104 02 0A 04            305 	ljmp	usb_sof_isr
   0107                     306 	.ds	1
   0108 02 0A 2F            307 	ljmp	usb_sutok_isr
   010B                     308 	.ds	1
   010C 02 0A 5A            309 	ljmp	usb_suspend_isr
   010F                     310 	.ds	1
   0110 02 0A 85            311 	ljmp	usb_usbreset_isr
   0113                     312 	.ds	1
   0114 32                  313 	reti
   0115                     314 	.ds	3
   0118 02 0A B0            315 	ljmp	usb_ep0in_isr
   011B                     316 	.ds	1
   011C 02 0A EB            317 	ljmp	usb_ep0out_isr
   011F                     318 	.ds	1
   0120 02 0B 9D            319 	ljmp	usb_ep1in_isr
   0123                     320 	.ds	1
   0124 02 0B CB            321 	ljmp	usb_ep1out_isr
   0127                     322 	.ds	1
   0128 02 0B F6            323 	ljmp	usb_ep2in_isr
   012B                     324 	.ds	1
   012C 02 0C 21            325 	ljmp	usb_ep2out_isr
   012F                     326 	.ds	1
   0130 02 0C 4C            327 	ljmp	usb_ep3in_isr
   0133                     328 	.ds	1
   0134 02 0C 77            329 	ljmp	usb_ep3out_isr
   0137                     330 	.ds	1
   0138 02 0C A2            331 	ljmp	usb_ep4in_isr
   013B                     332 	.ds	1
   013C 02 0C CD            333 	ljmp	usb_ep4out_isr
   013F                     334 	.ds	1
   0140 02 0C F8            335 	ljmp	usb_ep5in_isr
   0143                     336 	.ds	1
   0144 02 0D 23            337 	ljmp	usb_ep5out_isr
   0147                     338 	.ds	1
   0148 02 0D 4E            339 	ljmp	usb_ep6in_isr
   014B                     340 	.ds	1
   014C 02 0D 79            341 	ljmp	usb_ep6out_isr
   014F                     342 	.ds	1
   0150 02 0D A4            343 	ljmp	usb_ep7in_isr
   0153                     344 	.ds	1
   0154 02 0D CF            345 	ljmp	usb_ep7out_isr
                            346 
                            347 	;; -----------------------------------------------------
                            348 
                    0020    349 	RXCHUNKS = 32
                    0010    350 	TXCHUNKS = 16
                            351 
                            352 	.area	OSEG (OVR,DATA)
                            353 	.area	BSEG (BIT)
   0000                     354 ctrl_ptt:	.ds	1
   0001                     355 ctrl_pttmute:	.ds	1
   0002                     356 ctrl_ledptt:	.ds	1
   0003                     357 ctrl_leddcdsrc:	.ds	1	; not implemented
   0004                     358 ctrl_clksel:	.ds	1	; not implemented
   0005                     359 ctrl_indacd:	.ds	1	; not implemented
   0006                     360 ctrl_indacz:	.ds	1	; not implemented
   0007                     361 ctrl_txdsrc:	.ds	1	; not implemented
                    0020    362 ctrlreg		=	0x20	; ((ctrl_ptt/8)+0x20)
                            363 
   0008                     364 pttmute:	.ds	1
   0009                     365 pttforcechg:	.ds	1
   000A                     366 uartempty:	.ds	1
                            367 
                            368 	.area	ISEG (DATA)
   0080                     369 txbcnt:		.ds	TXCHUNKS
   0090                     370 rxbcnt:		.ds	RXCHUNKS
   00B0                     371 stack:		.ds	0x80-RXCHUNKS-TXCHUNKS
                            372 
                            373 	.area	DSEG (DATA)
   0040                     374 ctrlcode:	.ds	1
   0041                     375 ctrlcount:	.ds	2
   0043                     376 leddiv:		.ds	1
   0044                     377 irqcount:	.ds	1
                            378 	;; transmitter variables
   0045                     379 txstate:	.ds	1
   0046                     380 flagcnt:	.ds	2
   0048                     381 txwr:		.ds	1
   0049                     382 txrd:		.ds	1
   004A                     383 txtwr:		.ds	1
   004B                     384 txcrc:		.ds	2
   004D                     385 txshreg:	.ds	1
   004E                     386 txbnum:		.ds	1
   004F                     387 txptr:		.ds	1
   0050                     388 txskip:		.ds	1
   0051                     389 pttforce:	.ds	1
                            390 	;; receiver variables
   0052                     391 rxstate:	.ds	1
   0053                     392 rxwr:		.ds	1
   0054                     393 rxrd:		.ds	1
   0055                     394 rxtwr:		.ds	1
   0056                     395 rxcntc:		.ds	1
   0057                     396 rxcrc:		.ds	2
   0059                     397 rxshreg:	.ds	1
   005A                     398 rxbreg:		.ds	1
   005B                     399 rxbnum:		.ds	1
   005C                     400 rxptr:		.ds	1
                            401 
   005D                     402 tmprxcnt:	.ds	1
                            403 
                            404 	;; UART receiver
   005E                     405 uartbuf:	.ds	16
   006E                     406 uartwr:		.ds	1
   006F                     407 uartrd:		.ds	1
                            408 
                            409 	;; USB state
   0070                     410 numconfig:	.ds	1
   0071                     411 altsetting:	.ds	1
                            412 	
                            413 	.area	XSEG (DATA)
   1000                     414 txbuf:	.ds	TXCHUNKS*64
   1400                     415 rxbuf:	.ds	RXCHUNKS*64
                            416 
                            417 
                            418 	.area	GSINIT (CODE)
   0EAB                     419 startup:
   0EAB 75 81 B0            420 	mov	sp,#stack	; -1
   0EAE E4                  421 	clr	a
   0EAF F5 D0               422 	mov	psw,a
   0EB1 F5 86               423 	mov	dps,a
                            424 	;lcall	__sdcc_external_startup
                            425 	;mov	a,dpl0
                            426 	;jz	__sdcc_init_data
                            427 	;ljmp	__sdcc_program_startup
   0EB3                     428 __sdcc_init_data:
                            429 
                            430 	.area	GSINIT2 (CODE)
   0EB3                     431 __sdcc_program_startup:
                            432 	;; assembler code startup
   0EB3 E4                  433 	clr	a
   0EB4 F5 45               434 	mov	txstate,a
   0EB6 F5 48               435 	mov	txwr,a
   0EB8 F5 49               436 	mov	txrd,a
   0EBA F5 4A               437 	mov	txtwr,a
   0EBC F5 51               438 	mov	pttforce,a
   0EBE F5 52               439 	mov	rxstate,a
   0EC0 F5 53               440 	mov	rxwr,a
   0EC2 F5 54               441 	mov	rxrd,a
   0EC4 F5 55               442 	mov	rxtwr,a
   0EC6 F5 44               443  	mov	irqcount,a
   0EC8 F5 6F               444 	mov	uartrd,a
   0ECA F5 6E               445 	mov	uartwr,a
   0ECC F5 86               446 	mov	dps,a
   0ECE D2 0A               447 	setb	uartempty
                            448 	;; some indirect register setup
   0ED0 75 8E 30            449 	mov	ckcon,#0x30	; zero external wait states, to avoid chip bugs
                            450 	;; Timer setup:
                            451 	;; timer 0: LED blink    : T0M=0, CT0=0, MODE0=1
                            452 	;; timer 1: Baudrate gen : T1M=1, CT1=0, MODE1=2
   0ED3 75 89 21            453 	mov	tmod,#0x21
   0ED6 75 88 55            454 	mov	tcon,#0x55	; INT0/INT1 edge
   0ED9 75 8D 64            455 	mov	th1,#256-156	; 1200 bauds
   0EDC 75 87 00            456 	mov	pcon,#0		; SMOD0=0
                            457 	;; init USB subsystem
   0EDF 90 7F A1            458 	mov	dptr,#ISOCTL
   0EE2 74 01               459 	mov	a,#1		; disable ISO endpoints
   0EE4 F0                  460 	movx	@dptr,a
   0EE5 90 7F AF            461 	mov	dptr,#USBBAV
   0EE8 74 01               462 	mov	a,#1		; enable autovector, disable breakpoint logic
   0EEA F0                  463 	movx	@dptr,a
   0EEB E4                  464 	clr	a
   0EEC 90 7F E0            465 	mov	dptr,#INISOVAL
   0EEF F0                  466 	movx	@dptr,a
   0EF0 90 7F E1            467 	mov	dptr,#OUTISOVAL
   0EF3 F0                  468 	movx	@dptr,a
   0EF4 90 7F DD            469 	mov	dptr,#USBPAIR
   0EF7 74 09               470 	mov	a,#0x9		; pair EP 2&3 for input & output
   0EF9 F0                  471 	movx	@dptr,a
   0EFA 90 7F DE            472 	mov	dptr,#IN07VAL
   0EFD 74 07               473 	mov	a,#0x7		; enable EP0+EP1+EP2
   0EFF F0                  474 	movx	@dptr,a
   0F00 90 7F DF            475 	mov	dptr,#OUT07VAL
   0F03 74 05               476 	mov	a,#0x5		; enable EP0+EP2
   0F05 F0                  477 	movx	@dptr,a
                            478 	;; USB:	init endpoint toggles
   0F06 90 7F D7            479 	mov	dptr,#TOGCTL
   0F09 74 12               480 	mov	a,#0x12
   0F0B F0                  481 	movx	@dptr,a
   0F0C 74 32               482 	mov	a,#0x32		; clear EP 2 in toggle
   0F0E F0                  483 	movx	@dptr,a
   0F0F 74 02               484 	mov	a,#0x02
   0F11 F0                  485 	movx	@dptr,a
   0F12 74 22               486 	mov	a,#0x22		; clear EP 2 out toggle
   0F14 F0                  487 	movx	@dptr,a
                            488 	;; configure IO ports
   0F15 90 7F 93            489 	mov	dptr,#PORTACFG
   0F18 74 00               490 	mov	a,#0
   0F1A F0                  491 	movx	@dptr,a
   0F1B 90 7F 96            492 	mov	dptr,#OUTA
   0F1E 74 82               493 	mov	a,#0x82		; set PROG hi
   0F20 F0                  494 	movx	@dptr,a
   0F21 90 7F 9C            495 	mov	dptr,#OEA
   0F24 74 C2               496 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   0F26 F0                  497 	movx	@dptr,a
   0F27 90 7F 94            498 	mov	dptr,#PORTBCFG
   0F2A 74 00               499 	mov	a,#0
   0F2C F0                  500 	movx	@dptr,a
   0F2D 90 7F 9D            501 	mov	dptr,#OEB
   0F30 74 00               502 	mov	a,#0
   0F32 F0                  503 	movx	@dptr,a
   0F33 90 7F 95            504 	mov	dptr,#PORTCCFG
   0F36 74 C3               505 	mov	a,#0xc3		; RD/WR/TXD0/RXD0 are special function pins
   0F38 F0                  506 	movx	@dptr,a
   0F39 90 7F 98            507 	mov	dptr,#OUTC
   0F3C 74 28               508 	mov	a,#0x28
   0F3E F0                  509 	movx	@dptr,a
   0F3F 90 7F 9E            510 	mov	dptr,#OEC
   0F42 74 2A               511 	mov	a,#0x2a		; out: LEDCON,LEDSTA,TCK  in: TDO,INIT
   0F44 F0                  512 	movx	@dptr,a
                            513 	;; enable interrupts
   0F45 75 A8 92            514 	mov	ie,#0x92	; enable timer 0 and ser 0 int
   0F48 75 E8 01            515 	mov	eie,#0x01	; enable USB interrupts
   0F4B 90 7F AE            516 	mov	dptr,#USBIEN
   0F4E 74 01               517 	mov	a,#1		; enable SUDAV interrupt
   0F50 F0                  518 	movx	@dptr,a
   0F51 90 7F AC            519 	mov	dptr,#IN07IEN
   0F54 74 03               520 	mov	a,#3		; enable EP0+EP1 interrupt
   0F56 F0                  521 	movx	@dptr,a
   0F57 90 7F AD            522 	mov	dptr,#OUT07IEN
   0F5A 74 01               523 	mov	a,#1		; enable EP0 interrupt
   0F5C F0                  524 	movx	@dptr,a
                            525 	;; initialize UART 0 for T7F communication
   0F5D 75 98 52            526 	mov	scon0,#0x52	; Mode 1, Timer 1, Receiver enable
                            527 	;; copy configuration to bit addressable variables
   0F60 75 20 42            528 	mov	ctrlreg,#0x42
   0F63 78 E7               529 	mov	r0,#parpttmute
   0F65 E2                  530 	movx	a,@r0
   0F66 A2 E0               531 	mov	c,acc.0
   0F68 92 08               532 	mov	pttmute,c
   0F6A 92 01               533 	mov	ctrl_pttmute,c
                            534 	;; turn off transmitter
   0F6C E5 20               535 	mov	a,ctrlreg
   0F6E 90 C0 08            536 	mov	dptr,#FSKCTRL
   0F71 F0                  537 	movx	@dptr,a
                            538 	;; Initialize modem disc port / t7f port
   0F72 90 C0 0C            539 	mov	dptr,#FSKMDISCTRIS
   0F75 74 FF               540 	mov	a,#0xff
   0F77 F0                  541 	movx	@dptr,a
   0F78 90 C0 0D            542 	mov	dptr,#FSKMDISCOUT
   0F7B E4                  543 	clr	a
   0F7C F0                  544 	movx	@dptr,a
   0F7D 90 C0 0A            545 	mov	dptr,#FSKT7FOUT
   0F80 74 1F               546 	mov	a,#0x1f
   0F82 F0                  547 	movx	@dptr,a
                            548 	;; Copy serial number
   0F83 78 F0               549 	mov	r0,#parserial
   0F85 90 0E 8B            550 	mov	dptr,#stringserial+2
   0F88 E2                  551 1$:	movx	a,@r0
   0F89 60 06               552 	jz	2$
   0F8B F0                  553 	movx	@dptr,a
   0F8C A3                  554 	inc	dptr
   0F8D A3                  555 	inc	dptr
   0F8E 08                  556 	inc	r0
   0F8F 80 F7               557 	sjmp	1$
   0F91 E8                  558 2$:	mov	a,r0
   0F92 24 11               559 	add	a,#1-0xf0	; 1-parserial
   0F94 25 E0               560 	add	a,acc
   0F96 90 0E 89            561 	mov	dptr,#stringserial
   0F99 F0                  562 	movx	@dptr,a
                            563 	;; initialize USB state
   0F9A                     564 usbinit:
   0F9A E4                  565 	clr	a
   0F9B F5 70               566 	mov	numconfig,a
   0F9D F5 71               567 	mov	altsetting,a
                            568 	;; give Windows a chance to finish the writecpucs control transfer
                            569 	;; 20ms delay loop
   0F9F 90 D1 20            570 	mov	dptr,#(-12000)&0xffff
   0FA2 A3                  571 2$:	inc	dptr		; 3 cycles
   0FA3 E5 82               572 	mov	a,dpl0		; 2 cycles
   0FA5 45 83               573 	orl	a,dph0		; 2 cycles
   0FA7 70 F9               574 	jnz	2$		; 3 cycles
                    0001    575 	.if	1
                            576 	;; disconnect from USB bus
   0FA9 90 7F D6            577 	mov	dptr,#USBCS
   0FAC 74 0A               578 	mov	a,#10
   0FAE F0                  579 	movx	@dptr,a
                            580 	;; wait 0.3 sec
   0FAF 7A 1E               581 	mov	r2,#30
                            582 	;; 10ms delay loop
   0FB1 90 E8 90            583 0$:	mov	dptr,#(-6000)&0xffff
   0FB4 A3                  584 1$:	inc	dptr            ; 3 cycles
   0FB5 E5 82               585 	mov	a,dpl0          ; 2 cycles
   0FB7 45 83               586 	orl	a,dph0          ; 2 cycles
   0FB9 70 F9               587 	jnz	1$              ; 3 cycles
   0FBB DA F4               588 	djnz	r2,0$
                            589 	;; reconnect to USB bus
   0FBD 90 7F D6            590 	mov	dptr,#USBCS
                            591 	;mov	a,#2		; 8051 handles control
                            592 	;movx	@dptr,a
   0FC0 74 06               593 	mov	a,#6		; reconnect, 8051 handles control
   0FC2 F0                  594 	movx	@dptr,a
                            595 	.endif
                            596 	
                            597 	;; final
   0FC3 12 0B 26            598 	lcall	fillusbintr
                            599 	;; kludge; first OUT2 packet seems to be bogus
                            600 	;; wait for packet with length 1 and contents 0x55
   0FC6                     601 waitpkt1:
   0FC6 90 7F C8            602 	mov	dptr,#OUT2CS
   0FC9 E0                  603 	movx	a,@dptr
   0FCA 20 E1 F9            604 	jb	acc.1,waitpkt1
   0FCD 90 7F C9            605 	mov	dptr,#OUT2BC
   0FD0 E0                  606 	movx	a,@dptr
   0FD1 B4 01 0D            607 	cjne	a,#1,0$
   0FD4 90 7D C0            608 	mov	dptr,#OUT2BUF
   0FD7 E0                  609 	movx	a,@dptr
   0FD8 B4 55 06            610 	cjne	a,#0x55,0$
   0FDB 90 7F C9            611 	mov	dptr,#OUT2BC
   0FDE F0                  612 	movx	@dptr,a
   0FDF 80 06               613 	sjmp	pkt1received
   0FE1 90 7F C9            614 0$:	mov	dptr,#OUT2BC
   0FE4 F0                  615 	movx	@dptr,a
   0FE5 80 DF               616 	sjmp	waitpkt1
   0FE7                     617 pkt1received:
                            618 	;; start normal operation
   0FE7 02 01 79            619 	ljmp	mainloop
                            620 
                            621 
                            622 	.area	CSEG (CODE)
                    0002    623 	ar2 = 0x02
                    0003    624 	ar3 = 0x03
                    0004    625 	ar4 = 0x04
                    0005    626 	ar5 = 0x05
                    0006    627 	ar6 = 0x06
                    0007    628 	ar7 = 0x07
                    0000    629 	ar0 = 0x00
                    0001    630 	ar1 = 0x01
                            631 	
   0157                     632 setptt:
   0157 E5 45               633 	mov	a,txstate
   0159 45 51               634 	orl	a,pttforce
   015B 60 0D               635 	jz	pttoff
                            636 
   015D                     637 ptton:
   015D C2 01               638 	clr	ctrl_pttmute
   015F D2 00               639 	setb	ctrl_ptt
   0161 D2 02               640 	setb	ctrl_ledptt
   0163 E5 20               641 	mov	a,ctrlreg
   0165 90 C0 08            642 	mov	dptr,#FSKCTRL
   0168 F0                  643 	movx	@dptr,a
   0169 22                  644 	ret
                            645 
   016A                     646 pttoff:
   016A C2 00               647 	clr	ctrl_ptt
   016C A2 08               648 	mov	c,pttmute
   016E 92 01               649 	mov	ctrl_pttmute,c
   0170 C2 02               650 	clr	ctrl_ledptt
   0172 E5 20               651 	mov	a,ctrlreg
   0174 90 C0 08            652 	mov	dptr,#FSKCTRL
   0177 F0                  653 	movx	@dptr,a
   0178 22                  654 	ret
                            655 
   0179                     656 mainloop:
                            657 	
                            658 	;; HDLC encoder
   0179                     659 txnopkt:
   0179 E5 49               660 	mov	a,txrd
   017B B5 48 14            661 	cjne	a,txwr,txstartsend
   017E 12 02 C8            662 	lcall	periodic
   0181 E4                  663 	clr	a
   0182 F5 45               664 	mov	txstate,a
   0184 90 C0 00            665 	mov	dptr,#FSKSHREG
   0187 F0                  666 	movx	@dptr,a
   0188 10 09 02            667 	jbc	pttforcechg,0$
   018B 80 EC               668 	sjmp	txnopkt
   018D 12 01 57            669 0$:	lcall	setptt
   0190 80 E7               670 	sjmp	txnopkt
   0192                     671 txstartsend:
   0192 75 45 01            672 	mov	txstate,#1
   0195 75 4E 08            673 	mov	txbnum,#8
   0198 75 4B FF            674 	mov	txcrc,#0xff
   019B 75 4C FF            675 	mov	txcrc+1,#0xff
                            676 	;; start txdelay
                            677 	;; read the number of flags to send from the first two bytes of the first packet
                    0001    678 	.if	1
   019E E5 49               679 	mov	a,txrd
   01A0 03                  680 	rr	a
   01A1 03                  681 	rr	a
   01A2 F5 82               682 	mov	dpl0,a
   01A4 54 C0               683 	anl	a,#0xc0
   01A6 24 00               684 	add	a,#<txbuf
   01A8 C5 82               685 	xch	a,dpl0
   01AA 54 3F               686 	anl	a,#0x3f
   01AC 34 10               687 	addc	a,#>txbuf
   01AE F5 83               688 	mov	dph0,a
                            689 	.else
                            690 	mov	a,txrd
                            691 	rr	a
                            692 	rr	a
                            693 	mov	dpl0,a
                            694 	anl	a,#0xc0
                            695 	xch	a,dpl0
                            696 	anl	a,#0x3f
                            697 	add	a,#>txbuf
                            698 	mov	dph0,a
                            699 	.endif
   01B0 E0                  700 	movx	a,@dptr
   01B1 F5 46               701 	mov	flagcnt,a
   01B3 A3                  702 	inc	dptr
   01B4 E0                  703 	movx	a,@dptr
   01B5 F5 47               704 	mov	flagcnt+1,a
   01B7 75 50 02            705 	mov	txskip,#2
                            706 	;; sanity check (limit txdelay to about 8s)
   01BA 78 E1               707 	mov	r0,#parbitratetx+1
   01BC E2                  708 	movx	a,@r0
   01BD C3                  709 	clr	c
   01BE 95 47               710 	subb	a,flagcnt+1
   01C0 50 03               711 	jnc	2$
   01C2 E2                  712 	movx	a,@r0
   01C3 F5 47               713 	mov	flagcnt+1,a
   01C5                     714 2$:
                            715 	;; check if count is at least two
   01C5 E5 49               716 	mov	a,txrd
   01C7 24 80               717 	add	a,#txbcnt
   01C9 F8                  718 	mov	r0,a
   01CA E6                  719 	mov	a,@r0
   01CB 24 FE               720 	add	a,#-2
   01CD 40 09               721 	jc	1$
   01CF 75 46 01            722 	mov	flagcnt,#1
   01D2 75 47 00            723 	mov	flagcnt+1,#0
   01D5 75 50 00            724 	mov	txskip,#0
   01D8                     725 1$:
                            726 	;; turn on PTT
   01D8 12 01 5D            727 	lcall	ptton
   01DB                     728 txkeyup:
   01DB E5 46               729 	mov	a,flagcnt
   01DD 24 FF               730 	add	a,#-1
   01DF F5 46               731 	mov	flagcnt,a
   01E1 E5 47               732 	mov	a,flagcnt+1
   01E3 34 FF               733 	addc	a,#-1
   01E5 F5 47               734 	mov	flagcnt+1,a
   01E7 50 0B               735 	jnc	txpreparepkt
   01E9 12 02 C8            736 	lcall	periodic
   01EC 74 7E               737 	mov	a,#0x7e
   01EE 90 C0 00            738 	mov	dptr,#FSKSHREG
   01F1 F0                  739 	movx	@dptr,a
   01F2 80 E7               740 	sjmp	txkeyup
   01F4                     741 txpreparepkt:
   01F4 85 50 4F            742 	mov	txptr,txskip
   01F7 75 45 02            743 	mov	txstate,#2
   01FA                     744 txpktbyteloop:
   01FA E5 49               745 	mov	a,txrd
   01FC 24 80               746 	add	a,#txbcnt
   01FE F8                  747 	mov	r0,a
   01FF E6                  748 	mov	a,@r0
   0200 B5 4F 2B            749 	cjne	a,txptr,txchunkcont
                            750 	;; tx chunk ended
   0203 75 50 00            751 	mov	txskip,#0
   0206 20 E6 18            752 	jb	acc.6,txchunknoend
   0209 E5 4B               753 	mov	a,txcrc
   020B F4                  754 	cpl	a
   020C 12 02 83            755 	lcall	txbytenocrc
   020F E5 4C               756 	mov	a,txcrc+1
   0211 F4                  757 	cpl	a
   0212 12 02 83            758 	lcall	txbytenocrc
   0215 12 02 61            759 	lcall	txflag
   0218 75 4B FF            760 	mov	txcrc,#0xff
   021B 75 4C FF            761 	mov	txcrc+1,#0xff	
   021E 75 50 02            762 	mov	txskip,#2
   0221                     763 txchunknoend:
   0221 E5 49               764 	mov	a,txrd
   0223 04                  765 	inc	a
   0224 54 0F               766 	anl	a,#(TXCHUNKS-1)
   0226 F5 49               767 	mov	txrd,a
   0228 B5 48 C9            768 	cjne	a,txwr,txpreparepkt
   022B 02 02 4A            769 	ljmp	txtail
   022E                     770 txchunkcont:
                    0001    771 	.if	1
   022E E5 49               772 	mov	a,txrd
   0230 03                  773 	rr	a
   0231 03                  774 	rr	a
   0232 F5 82               775 	mov	dpl0,a
   0234 54 C0               776 	anl	a,#0xc0
   0236 25 4F               777 	add	a,txptr
   0238 24 00               778 	add	a,#<txbuf
   023A C5 82               779 	xch	a,dpl0
   023C 54 3F               780 	anl	a,#0x3f
   023E 34 10               781 	addc	a,#>txbuf
   0240 F5 83               782 	mov	dph0,a
                            783 	.else
                            784 	mov	a,txrd
                            785 	rr	a
                            786 	rr	a
                            787 	mov	dpl0,a
                            788 	anl	a,#0xc0
                            789 	add	a,txptr
                            790 	xch	a,dpl0
                            791 	anl	a,#0x3f
                            792 	addc	a,#>txbuf
                            793 	mov	dph0,a
                            794 	.endif
   0242 E0                  795 	movx	a,@dptr
   0243 12 02 9A            796 	lcall	txbyte	
   0246 05 4F               797 	inc	txptr
   0248 80 B0               798 	sjmp	txpktbyteloop
                            799 
   024A                     800 txtail:
   024A 12 02 61            801 	lcall	txflag
   024D 75 45 03            802 	mov	txstate,#3
   0250 7A 14               803 	mov	r2,#20
   0252 D3                  804 0$:	setb	c
   0253 12 02 6E            805 	lcall	txbit
   0256 DA FA               806 	djnz	r2,0$
                            807 	;; turn off PTT
   0258 75 45 00            808 	mov	txstate,#0
   025B 12 01 57            809 	lcall	setptt
   025E 02 01 79            810 	ljmp	txnopkt
                            811 
   0261                     812 txflag:
   0261 7B 7E               813 	mov	r3,#0x7e
   0263 7A 08               814 	mov	r2,#8
   0265 EB                  815 0$:	mov	a,r3
   0266 13                  816 	rrc	a
   0267 FB                  817 	mov	r3,a
   0268 12 02 6E            818 	lcall	txbit
   026B DA F8               819 	djnz	r2,0$
   026D 22                  820 	ret	
                            821 	
   026E                     822 txbit:
   026E E5 4D               823 	mov	a,txshreg
   0270 13                  824 	rrc	a
   0271 F5 4D               825 	mov	txshreg,a
   0273 D5 4E 0C            826 	djnz	txbnum,0$
   0276 75 4E 08            827 	mov	txbnum,#8
   0279 12 02 C8            828 	lcall	periodic
   027C E5 4D               829 	mov	a,txshreg
   027E 90 C0 00            830 	mov	dptr,#FSKSHREG
   0281 F0                  831 	movx	@dptr,a
   0282 22                  832 0$:	ret
                            833 
                            834 
   0283                     835 txbytenocrc:
   0283 FB                  836 	mov	r3,a
   0284 7A 08               837 	mov	r2,#8
   0286 EB                  838 0$:	mov	a,r3
   0287 13                  839 	rrc	a
   0288 FB                  840 	mov	r3,a
   0289 12 02 6E            841 	lcall	txbit
   028C E5 4D               842 	mov	a,txshreg
   028E F4                  843 	cpl	a
   028F 54 F8               844 	anl	a,#0xf8
   0291 70 04               845 	jnz	1$
   0293 C3                  846 	clr	c
   0294 12 02 6E            847 	lcall	txbit		; add stuff bit
   0297 DA ED               848 1$:	djnz	r2,0$
   0299 22                  849 	ret
                            850 
   029A                     851 txbyte:
   029A FB                  852 	mov	r3,a
   029B 7A 08               853 	mov	r2,#8
   029D C3                  854 0$:	clr	c
   029E E5 4C               855 	mov	a,txcrc+1
   02A0 13                  856 	rrc	a
   02A1 F5 4C               857 	mov	txcrc+1,a
   02A3 E5 4B               858 	mov	a,txcrc
   02A5 13                  859 	rrc	a
   02A6 F5 4B               860 	mov	txcrc,a
   02A8 EB                  861 	mov	a,r3
   02A9 30 E0 01            862 	jnb	acc.0,2$
   02AC B3                  863 	cpl	c
   02AD 50 06               864 2$:	jnc	3$
   02AF 63 4C 84            865 	xrl	txcrc+1,#0x84
   02B2 63 4B 08            866 	xrl	txcrc,#0x08
   02B5 13                  867 3$:	rrc	a
   02B6 FB                  868 	mov	r3,a
   02B7 12 02 6E            869 	lcall	txbit
   02BA E5 4D               870 	mov	a,txshreg
   02BC F4                  871 	cpl	a
   02BD 54 F8               872 	anl	a,#0xf8
   02BF 70 04               873 	jnz	1$
   02C1 C3                  874 	clr	c
   02C2 12 02 6E            875 	lcall	txbit		; add stuff bit
   02C5 DA D6               876 1$:	djnz	r2,0$
   02C7 22                  877 	ret
                            878 
                            879 	
   02C8                     880 periodic:
   02C8 90 C0 01            881 	mov	dptr,#FSKSHSTATUS
   02CB E0                  882 	movx	a,@dptr
   02CC 30 E1 01            883 	jnb	acc.1,0$
   02CF 22                  884 	ret
   02D0 30 E0 03            885 0$:	jnb	acc.0,1$
   02D3 12 03 63            886 	lcall	hdlcdec
   02D6                     887 1$:
                            888 
   02D6                     889 usbiostart:
                            890 	;; check for USB modem->host
   02D6 E5 54               891 	mov	a,rxrd
   02D8 B5 53 47            892 	cjne	a,rxwr,usbcheckin
                            893 	;; check for USB host->modem
   02DB                     894 usbcheckout:
   02DB 90 7F C8            895 	mov	dptr,#OUT2CS
   02DE E0                  896 	movx	a,@dptr
   02DF 20 E1 3E            897 	jb	acc.1,endusb2
   02E2 E5 48               898 	mov	a,txwr
   02E4 24 80               899 	add	a,#txbcnt
   02E6 F8                  900 	mov	r0,a
   02E7 E5 48               901 	mov	a,txwr
   02E9 04                  902 	inc	a
   02EA 54 0F               903 	anl	a,#(TXCHUNKS-1)
   02EC B5 49 03            904 	cjne	a,txrd,usbout2
   02EF 02 03 60            905 	ljmp	endusb
   02F2                     906 usbout2:
   02F2 FF                  907 	mov	r7,a
   02F3 90 7F C9            908 	mov	dptr,#OUT2BC
   02F6 E0                  909 	movx	a,@dptr
   02F7 F6                  910 	mov	@r0,a
   02F8 60 20               911 	jz	usbout3
   02FA FE                  912 	mov	r6,a
                    0001    913 	.if	1
   02FB E5 48               914 	mov	a,txwr
   02FD 03                  915 	rr	a
   02FE 03                  916 	rr	a
   02FF F5 84               917 	mov	dpl1,a
   0301 54 C0               918 	anl	a,#0xc0
   0303 24 00               919 	add	a,#<txbuf
   0305 C5 84               920 	xch	a,dpl1
   0307 54 3F               921 	anl	a,#0x3f
   0309 34 10               922 	addc	a,#>txbuf
   030B F5 85               923 	mov	dph1,a
                            924 	.else
                            925 	mov	a,txwr
                            926 	rr	a
                            927 	rr	a
                            928 	mov	dpl1,a
                            929 	anl	a,#0xc0
                            930 	xch	a,dpl1
                            931 	anl	a,#0x3f
                            932 	add	a,#>txbuf
                            933 	mov	dph1,a
                            934 	.endif
   030D 90 7D C0            935 	mov	dptr,#OUT2BUF
   0310                     936 usboutloop:
   0310 E0                  937 	movx	a,@dptr
   0311 A3                  938 	inc	dptr
   0312 05 86               939 	inc	dps
   0314 F0                  940 	movx	@dptr,a
   0315 A3                  941 	inc	dptr
   0316 15 86               942 	dec	dps
   0318 DE F6               943 	djnz	r6,usboutloop
   031A                     944 usbout3:
   031A 8F 48               945 	mov	txwr,r7
   031C 90 7F C9            946 	mov	dptr,#OUT2BC
   031F F0                  947 	movx	@dptr,a		; rearm OUT2
   0320                     948 endusb2:
   0320 80 3E               949 	sjmp	endusb
                            950 
   0322                     951 usbcheckin:
   0322 90 7F B8            952 	mov	dptr,#IN2CS
   0325 E0                  953 	movx	a,@dptr
   0326 20 E1 B2            954 	jb	acc.1,usbcheckout
   0329 E5 54               955 	mov	a,rxrd
   032B 24 90               956 	add	a,#rxbcnt
   032D F8                  957 	mov	r0,a
   032E E6                  958 	mov	a,@r0
   032F 60 20               959 	jz	usbin1
   0331 FE                  960 	mov	r6,a
                    0001    961 	.if	1
   0332 E5 54               962 	mov	a,rxrd
   0334 03                  963 	rr	a
   0335 03                  964 	rr	a
   0336 F5 84               965 	mov	dpl1,a
   0338 54 C0               966 	anl	a,#0xc0
   033A 24 00               967 	add	a,#<rxbuf
   033C C5 84               968 	xch	a,dpl1	
   033E 54 3F               969 	anl	a,#0x3f
   0340 34 14               970 	addc	a,#>rxbuf
   0342 F5 85               971 	mov	dph1,a
                            972 	.else
                            973 	mov	a,rxrd
                            974 	rr	a
                            975 	rr	a
                            976 	mov	dpl1,a
                            977 	anl	a,#0xc0
                            978 	xch	a,dpl1	
                            979 	anl	a,#0x3f
                            980 	addc	a,#>rxbuf
                            981 	mov	dph1,a
                            982 	.endif
   0344 90 7E 00            983 	mov	dptr,#IN2BUF
   0347                     984 usbinloop:
   0347 05 86               985 	inc	dps
   0349 E0                  986 	movx	a,@dptr
   034A A3                  987 	inc	dptr
   034B 15 86               988 	dec	dps
   034D F0                  989 	movx	@dptr,a
   034E A3                  990 	inc	dptr
   034F DE F6               991 	djnz	r6,usbinloop
   0351                     992 usbin1:
   0351 E6                  993 	mov	a,@r0
   0352 90 7F B9            994 	mov	dptr,#IN2BC
   0355 F0                  995 	movx	@dptr,a
   0356 E5 54               996 	mov	a,rxrd
   0358 04                  997 	inc	a
   0359 54 1F               998 	anl	a,#(RXCHUNKS-1)
   035B F5 54               999 	mov	rxrd,a
   035D 02 02 DB           1000 	ljmp	usbcheckout
                           1001 
   0360                    1002 endusb:
   0360 02 02 C8           1003 	ljmp	periodic
                           1004 
                           1005 
                           1006 
                           1007 	;; Software HDLC decoder
   0363                    1008 hdlcdec:
   0363 90 C0 00           1009 	mov	dptr,#FSKSHREG
   0366 E0                 1010 	movx	a,@dptr
   0367 FE                 1011 	mov	r6,a
   0368 7F 08              1012 	mov	r7,#8
   036A                    1013 hdlcdecloop:
   036A EE                 1014 	mov	a,r6
   036B 13                 1015 	rrc	a
   036C FE                 1016 	mov	r6,a
   036D E5 59              1017 	mov	a,rxshreg
   036F 33                 1018 	rlc	a
   0370 F5 59              1019 	mov	rxshreg,a
   0372 B4 7E 03           1020 	cjne	a,#0x7e,$0
   0375 02 03 F2           1021 	ljmp	hdlcdecflag
   0378 F4                 1022 $0:	cpl	a
   0379 54 7F              1023 	anl	a,#0x7f
   037B 60 0E              1024 	jz	hdlcdecabort
   037D E5 52              1025 $1:	mov	a,rxstate
   037F 60 07              1026 	jz	hdlcdecendloop
   0381 E5 59              1027 	mov	a,rxshreg
   0383 54 3F              1028 	anl	a,#0x3f
   0385 B4 3E 08           1029 	cjne	a,#0x3e,hdlcdecrxbit
   0388                    1030 hdlcdecendloop:
   0388 DF E0              1031 	djnz	r7,hdlcdecloop
   038A 22                 1032 	ret
                           1033 
   038B                    1034 hdlcdecabort:
   038B 75 52 00           1035 	mov	rxstate,#0	; abort received
   038E 80 F8              1036 	sjmp	hdlcdecendloop
                           1037 
   0390                    1038 hdlcdecrxbit:
   0390 C3                 1039 	clr	c
   0391 E5 57              1040 	mov	a,rxcrc
   0393 33                 1041 	rlc	a
   0394 F5 57              1042 	mov	rxcrc,a
   0396 E5 58              1043 	mov	a,rxcrc+1
   0398 33                 1044 	rlc	a
   0399 F5 58              1045 	mov	rxcrc+1,a
   039B E5 59              1046 	mov	a,rxshreg
   039D 30 E0 01           1047 	jnb	acc.0,0$
   03A0 B3                 1048 	cpl	c
   03A1 50 06              1049 0$:	jnc	1$
   03A3 63 57 21           1050 	xrl	rxcrc,#0x21
   03A6 63 58 10           1051 	xrl	rxcrc+1,#0x10
   03A9 13                 1052 1$:	rrc	a
   03AA E5 5A              1053 	mov	a,rxbreg
   03AC 13                 1054 	rrc	a
   03AD F5 5A              1055 	mov	rxbreg,a
   03AF D5 5B D6           1056 	djnz	rxbnum,hdlcdecendloop
   03B2 75 5B 08           1057 	mov	rxbnum,#8
                    0001   1058 	.if	1
   03B5 E5 55              1059 	mov	a,rxtwr
   03B7 03                 1060 	rr	a
   03B8 03                 1061 	rr	a
   03B9 F5 82              1062 	mov	dpl0,a
   03BB 54 C0              1063 	anl	a,#0xc0
   03BD 25 5C              1064 	add	a,rxptr
   03BF 24 00              1065 	add	a,#<rxbuf
   03C1 C5 82              1066 	xch	a,dpl0
   03C3 54 3F              1067 	anl	a,#0x3f
   03C5 34 14              1068 	addc	a,#>rxbuf
   03C7 F5 83              1069 	mov	dph0,a
                           1070 	.else
                           1071 	mov	a,rxtwr
                           1072 	rr	a
                           1073 	rr	a
                           1074 	mov	dpl0,a
                           1075 	anl	a,#0xc0
                           1076 	add	a,rxptr
                           1077 	xch	a,dpl0
                           1078 	anl	a,#0x3f
                           1079 	addc	a,#>rxbuf
                           1080 	mov	dph0,a
                           1081 	.endif
   03C9 E5 5A              1082 	mov	a,rxbreg
   03CB F0                 1083 	movx	@dptr,a
   03CC 05 5C              1084 	inc	rxptr
   03CE E5 5C              1085 	mov	a,rxptr
   03D0 30 E6 B5           1086 	jnb	acc.6,hdlcdecendloop
   03D3 E5 55              1087 	mov	a,rxtwr
   03D5 24 90              1088 	add	a,#rxbcnt
   03D7 F8                 1089 	mov	r0,a
   03D8 76 40              1090 	mov	@r0,#64
   03DA E5 55              1091 	mov	a,rxtwr
   03DC 04                 1092 	inc	a
   03DD 54 1F              1093 	anl	a,#RXCHUNKS-1
   03DF B5 54 06           1094 	cjne	a,rxrd,2$
   03E2 75 52 00           1095 	mov	rxstate,#0
   03E5 02 03 88           1096 	ljmp	hdlcdecendloop
   03E8 F5 55              1097 2$:	mov	rxtwr,a
   03EA 75 5C 00           1098 	mov	rxptr,#0
   03ED 05 56              1099 	inc	rxcntc
   03EF 02 03 88           1100 	ljmp	hdlcdecendloop
                           1101 	
   03F2                    1102 hdlcdecflag:
   03F2 E5 52              1103 	mov	a,rxstate
   03F4 60 2F              1104 	jz	hdlcdecpreparenewpkt
                           1105 	;; check for correct CRC:
                           1106 	;; 1D0F -0-> 3A1E -1-> 641D -1-> D81B -1-> B036 -1-> 606C -1-> D0F9 -1-> A1F2
   03F6 E5 57              1107 	mov	a,rxcrc
   03F8 64 F2              1108 	xrl	a,#0xf2
   03FA F8                 1109 	mov	r0,a
   03FB E5 58              1110 	mov	a,rxcrc+1
   03FD 64 A1              1111 	xrl	a,#0xa1
   03FF 48                 1112 	orl	a,r0
   0400 70 23              1113 	jnz	hdlcdecpreparenewpkt
   0402 E5 55              1114 	mov	a,rxtwr
   0404 24 90              1115 	add	a,#rxbcnt
   0406 F8                 1116 	mov	r0,a
   0407 E5 5C              1117 	mov	a,rxptr
   0409 F6                 1118 	mov	@r0,a
   040A E5 56              1119 	mov	a,rxcntc
   040C 70 07              1120 	jnz	3$
   040E 74 FC              1121 	mov	a,#-4
   0410 26                 1122 	add	a,@r0
   0411 50 12              1123 	jnc	hdlcdecpreparenewpkt	; frame too short
   0413 80 04              1124 	sjmp	4$
   0415 24 F8              1125 3$:	add	a,#-8
   0417 40 0C              1126 	jc	hdlcdecpreparenewpkt	; frame too long
   0419 E5 55              1127 4$:	mov	a,rxtwr
   041B 04                 1128 	inc	a
   041C 54 1F              1129 	anl	a,#RXCHUNKS-1
   041E B5 54 02           1130 	cjne	a,rxrd,5$
   0421 80 02              1131 	sjmp	hdlcdecpreparenewpkt
   0423 F5 53              1132 5$:	mov	rxwr,a
   0425                    1133 hdlcdecpreparenewpkt:
   0425 E4                 1134 	clr	a
   0426 F5 56              1135 	mov	rxcntc,a
   0428 F5 5C              1136 	mov	rxptr,a
   042A 75 52 01           1137 	mov	rxstate,#1
   042D F4                 1138 	cpl	a
   042E F5 57              1139 	mov	rxcrc,a
   0430 F5 58              1140 	mov	rxcrc+1,a
   0432 75 5B 08           1141 	mov	rxbnum,#8
   0435 85 53 55           1142 	mov	rxtwr,rxwr
   0438 02 03 88           1143 	ljmp	hdlcdecendloop
                           1144 
                           1145 	
                    0000   1146 	.if	0
                           1147 	;; IO copy routine
                           1148 ioc1:
                           1149 	;; txpointers
                           1150 	add	a,#txbcnt
                           1151 	mov	r0,a
                    0001   1152 	.if	1
                           1153 	mov	a,txrd
                           1154 	rr	a
                           1155 	rr	a
                           1156 	mov	dpl1,a
                           1157 	anl	a,#0xc0
                           1158 	add	a,#<txbuf
                           1159 	xch	a,dpl1
                           1160 	anl	a,#0x3f
                           1161 	addc	a,#>txbuf
                           1162 	mov	dph1,a
                           1163 	.else
                           1164 	mov	a,txrd
                           1165 	rr	a
                           1166 	rr	a
                           1167 	mov	dpl1,a
                           1168 	anl	a,#0xc0
                           1169 	xch	a,dpl1
                           1170 	anl	a,#0x3f
                           1171 	add	a,#>txbuf
                           1172 	mov	dph1,a
                           1173 	.endif
                           1174 	;; rxpointers
                           1175 	mov	a,rxtwr
                           1176 	add	a,#rxbcnt
                           1177 	mov	r1,a
                    0001   1178 	.if	1
                           1179 	mov	a,rxtwr
                           1180 	rr	a
                           1181 	rr	a
                           1182 	mov	dpl0,a
                           1183 	anl	a,#0xc0
                           1184 	add	a,#<rxbuf
                           1185 	xch	a,dpl0
                           1186 	anl	a,#0x3f
                           1187 	addc	a,#>rxbuf
                           1188 	mov	dph0,a
                           1189 	.else
                           1190 	mov	a,rxtwr
                           1191 	rr	a
                           1192 	rr	a
                           1193 	mov	dpl0,a
                           1194 	anl	a,#0xc0
                           1195 	xch	a,dpl0
                           1196 	anl	a,#0x3f
                           1197 	add	a,#>rxbuf
                           1198 	mov	dph0,a
                           1199 	.endif
                           1200 	;; update rxpointer
                           1201 	mov	a,rxtwr
                           1202 	inc	a
                           1203 	anl	a,#(RXCHUNKS-1)
                           1204 	mov	rxtwr,a
                           1205 	cjne	a,rxrd,ioc2
                           1206 iocret:
                           1207 	ret
                           1208 
                           1209 	;; entry point
                           1210 iocopy:
                           1211 	mov	a,txrd
                           1212 	cjne	a,txwr,ioc1
                           1213 	ret
                           1214 
                           1215 ioc2:
                           1216 	mov	a,@r0
                           1217 	mov	@r1,a
                           1218 	jz	ioc3
                           1219 	mov	r7,a
                           1220 ioc4:
                           1221 	inc	dps
                           1222 	movx	a,@dptr
                           1223 	inc	dptr
                           1224 	dec	dps
                           1225 	movx	@dptr,a
                           1226 	inc	dptr
                           1227 	djnz	r7,ioc4
                           1228 ioc3:
                           1229 	mov	a,txrd
                           1230 	inc	a
                           1231 	anl	a,#(TXCHUNKS-1)
                           1232 	mov	txrd,a
                           1233 	mov	a,@r0
                           1234 	add	a,#-0x40
                           1235 	jc	iocret
                           1236 	mov	rxwr,rxtwr
                           1237 	ret
                           1238 	.endif
                           1239 
                           1240 	;; ------------------ interrupt handlers ------------------------
                           1241 
   043B                    1242 int0_isr:
   043B C0 E0              1243 	push	acc
   043D C0 F0              1244 	push	b
   043F C0 82              1245 	push	dpl0
   0441 C0 83              1246 	push	dph0
   0443 C0 D0              1247 	push	psw
   0445 75 D0 00           1248 	mov	psw,#0x00
   0448 C0 86              1249 	push	dps
   044A 75 86 00           1250 	mov	dps,#0
                           1251 	;; clear interrupt
   044D C2 89              1252 	clr	tcon+1
                           1253 	;; handle interrupt
                           1254 	;; epilogue
   044F D0 86              1255 	pop	dps
   0451 D0 D0              1256 	pop	psw
   0453 D0 83              1257 	pop	dph0
   0455 D0 82              1258 	pop	dpl0
   0457 D0 F0              1259 	pop	b
   0459 D0 E0              1260 	pop	acc
   045B 32                 1261 	reti
                           1262 
   045C                    1263 timer0_isr:
   045C C0 E0              1264 	push	acc
   045E C0 F0              1265 	push	b
   0460 C0 82              1266 	push	dpl0
   0462 C0 83              1267 	push	dph0
   0464 C0 D0              1268 	push	psw
   0466 75 D0 00           1269 	mov	psw,#0x00
   0469 C0 86              1270 	push	dps
   046B 75 86 00           1271 	mov	dps,#0
                           1272 	;; clear interrupt
   046E C2 8D              1273 	clr	tcon+5
                           1274 	;; handle interrupt
                    0000   1275 	.if	0
                           1276 	inc	leddiv
                           1277 	mov	a,leddiv
                           1278 	anl	a,#7
                           1279 	jnz	0$
                           1280 	mov	dptr,#OUTC
                           1281 	movx	a,@dptr
                           1282 	xrl	a,#0x08
                           1283 	movx	@dptr,a
                           1284 0$:
                           1285 	.endif
                           1286 	;; epilogue
   0470 D0 86              1287 	pop	dps
   0472 D0 D0              1288 	pop	psw
   0474 D0 83              1289 	pop	dph0
   0476 D0 82              1290 	pop	dpl0
   0478 D0 F0              1291 	pop	b
   047A D0 E0              1292 	pop	acc
   047C 32                 1293 	reti
                           1294 
   047D                    1295 int1_isr:
   047D C0 E0              1296 	push	acc
   047F C0 F0              1297 	push	b
   0481 C0 82              1298 	push	dpl0
   0483 C0 83              1299 	push	dph0
   0485 C0 D0              1300 	push	psw
   0487 75 D0 00           1301 	mov	psw,#0x00
   048A C0 86              1302 	push	dps
   048C 75 86 00           1303 	mov	dps,#0
                           1304 	;; clear interrupt
   048F C2 8B              1305 	clr	tcon+3
                           1306 	;; handle interrupt
                           1307 	;; epilogue
   0491 D0 86              1308 	pop	dps
   0493 D0 D0              1309 	pop	psw
   0495 D0 83              1310 	pop	dph0
   0497 D0 82              1311 	pop	dpl0
   0499 D0 F0              1312 	pop	b
   049B D0 E0              1313 	pop	acc
   049D 32                 1314 	reti
                           1315 
   049E                    1316 timer1_isr:
   049E C0 E0              1317 	push	acc
   04A0 C0 F0              1318 	push	b
   04A2 C0 82              1319 	push	dpl0
   04A4 C0 83              1320 	push	dph0
   04A6 C0 D0              1321 	push	psw
   04A8 75 D0 00           1322 	mov	psw,#0x00
   04AB C0 86              1323 	push	dps
   04AD 75 86 00           1324 	mov	dps,#0
                           1325 	;; clear interrupt
   04B0 C2 8F              1326 	clr	tcon+7
                           1327 	;; handle interrupt
                           1328 	;; epilogue
   04B2 D0 86              1329 	pop	dps
   04B4 D0 D0              1330 	pop	psw
   04B6 D0 83              1331 	pop	dph0
   04B8 D0 82              1332 	pop	dpl0
   04BA D0 F0              1333 	pop	b
   04BC D0 E0              1334 	pop	acc
   04BE 32                 1335 	reti
                           1336 
   04BF                    1337 ser0_isr:
   04BF C0 E0              1338 	push	acc
   04C1 C0 F0              1339 	push	b
   04C3 C0 82              1340 	push	dpl0
   04C5 C0 83              1341 	push	dph0
   04C7 C0 D0              1342 	push	psw
   04C9 75 D0 00           1343 	mov	psw,#0x00
   04CC C0 86              1344 	push	dps
   04CE 75 86 00           1345 	mov	dps,#0
   04D1 C0 00              1346 	push	ar0
                           1347 	;; clear interrupt
   04D3 10 98 16           1348 	jbc	scon0+0,1$	; RI
   04D6 10 99 0F           1349 0$:	jbc	scon0+1,2$	; TI
                           1350 	;; handle interrupt
                           1351 	;; epilogue
   04D9 D0 00              1352 3$:	pop	ar0
   04DB D0 86              1353 	pop	dps
   04DD D0 D0              1354 	pop	psw
   04DF D0 83              1355 	pop	dph0
   04E1 D0 82              1356 	pop	dpl0
   04E3 D0 F0              1357 	pop	b
   04E5 D0 E0              1358 	pop	acc
   04E7 32                 1359 	reti
                           1360 
   04E8 D2 0A              1361 2$:	setb	uartempty
   04EA 80 EA              1362 	sjmp	0$
                           1363 
   04EC E5 6E              1364 1$:	mov	a,uartwr
   04EE 24 5E              1365 	add	a,#uartbuf
   04F0 F8                 1366 	mov	r0,a
   04F1 E5 99              1367 	mov	a,sbuf0
   04F3 F6                 1368 	mov	@r0,a
   04F4 E5 6E              1369 	mov	a,uartwr
   04F6 04                 1370 	inc	a
   04F7 54 0F              1371 	anl	a,#0xf
   04F9 F5 6E              1372 	mov	uartwr,a
   04FB 80 DC              1373 	sjmp	3$
                           1374 
   04FD                    1375 timer2_isr:
   04FD C0 E0              1376 	push	acc
   04FF C0 F0              1377 	push	b
   0501 C0 82              1378 	push	dpl0
   0503 C0 83              1379 	push	dph0
   0505 C0 D0              1380 	push	psw
   0507 75 D0 00           1381 	mov	psw,#0x00
   050A C0 86              1382 	push	dps
   050C 75 86 00           1383 	mov	dps,#0
                           1384 	;; clear interrupt
   050F C2 CF              1385 	clr	t2con+7
                           1386 	;; handle interrupt
                           1387 	;; epilogue
   0511 D0 86              1388 	pop	dps
   0513 D0 D0              1389 	pop	psw
   0515 D0 83              1390 	pop	dph0
   0517 D0 82              1391 	pop	dpl0
   0519 D0 F0              1392 	pop	b
   051B D0 E0              1393 	pop	acc
   051D 32                 1394 	reti
                           1395 
   051E                    1396 resume_isr:
   051E C0 E0              1397 	push	acc
   0520 C0 F0              1398 	push	b
   0522 C0 82              1399 	push	dpl0
   0524 C0 83              1400 	push	dph0
   0526 C0 D0              1401 	push	psw
   0528 75 D0 00           1402 	mov	psw,#0x00
   052B C0 86              1403 	push	dps
   052D 75 86 00           1404 	mov	dps,#0
                           1405 	;; clear interrupt
   0530 C2 DC              1406 	clr	eicon+4
                           1407 	;; handle interrupt
                           1408 	;; epilogue
   0532 D0 86              1409 	pop	dps
   0534 D0 D0              1410 	pop	psw
   0536 D0 83              1411 	pop	dph0
   0538 D0 82              1412 	pop	dpl0
   053A D0 F0              1413 	pop	b
   053C D0 E0              1414 	pop	acc
   053E 32                 1415 	reti
                           1416 
   053F                    1417 ser1_isr:
   053F C0 E0              1418 	push	acc
   0541 C0 F0              1419 	push	b
   0543 C0 82              1420 	push	dpl0
   0545 C0 83              1421 	push	dph0
   0547 C0 D0              1422 	push	psw
   0549 75 D0 00           1423 	mov	psw,#0x00
   054C C0 86              1424 	push	dps
   054E 75 86 00           1425 	mov	dps,#0
                           1426 	;; clear interrupt
   0551 C2 C0              1427 	clr	scon1+0
   0553 C2 C1              1428 	clr	scon1+1
                           1429 	;; handle interrupt
                           1430 	;; epilogue
   0555 D0 86              1431 	pop	dps
   0557 D0 D0              1432 	pop	psw
   0559 D0 83              1433 	pop	dph0
   055B D0 82              1434 	pop	dpl0
   055D D0 F0              1435 	pop	b
   055F D0 E0              1436 	pop	acc
   0561 32                 1437 	reti
                           1438 
   0562                    1439 i2c_isr:
   0562 C0 E0              1440 	push	acc
   0564 C0 F0              1441 	push	b
   0566 C0 82              1442 	push	dpl0
   0568 C0 83              1443 	push	dph0
   056A C0 D0              1444 	push	psw
   056C 75 D0 00           1445 	mov	psw,#0x00
   056F C0 86              1446 	push	dps
   0571 75 86 00           1447 	mov	dps,#0
                           1448 	;; clear interrupt
   0574 E5 91              1449 	mov	a,exif
   0576 C2 E5              1450 	clr	acc.5
   0578 F5 91              1451 	mov	exif,a
                           1452 	;; handle interrupt
                           1453 	;; epilogue
   057A D0 86              1454 	pop	dps
   057C D0 D0              1455 	pop	psw
   057E D0 83              1456 	pop	dph0
   0580 D0 82              1457 	pop	dpl0
   0582 D0 F0              1458 	pop	b
   0584 D0 E0              1459 	pop	acc
   0586 32                 1460 	reti
                           1461 
   0587                    1462 int4_isr:
   0587 C0 E0              1463 	push	acc
   0589 C0 F0              1464 	push	b
   058B C0 82              1465 	push	dpl0
   058D C0 83              1466 	push	dph0
   058F C0 D0              1467 	push	psw
   0591 75 D0 00           1468 	mov	psw,#0x00
   0594 C0 86              1469 	push	dps
   0596 75 86 00           1470 	mov	dps,#0
                           1471 	;; clear interrupt
   0599 E5 91              1472 	mov	a,exif
   059B C2 E6              1473 	clr	acc.6
   059D F5 91              1474 	mov	exif,a
                           1475 	;; handle interrupt
                           1476 	;; epilogue
   059F D0 86              1477 	pop	dps
   05A1 D0 D0              1478 	pop	psw
   05A3 D0 83              1479 	pop	dph0
   05A5 D0 82              1480 	pop	dpl0
   05A7 D0 F0              1481 	pop	b
   05A9 D0 E0              1482 	pop	acc
   05AB 32                 1483 	reti
                           1484 
   05AC                    1485 int5_isr:
   05AC C0 E0              1486 	push	acc
   05AE C0 F0              1487 	push	b
   05B0 C0 82              1488 	push	dpl0
   05B2 C0 83              1489 	push	dph0
   05B4 C0 D0              1490 	push	psw
   05B6 75 D0 00           1491 	mov	psw,#0x00
   05B9 C0 86              1492 	push	dps
   05BB 75 86 00           1493 	mov	dps,#0
                           1494 	;; clear interrupt
   05BE E5 91              1495 	mov	a,exif
   05C0 C2 E7              1496 	clr	acc.7
   05C2 F5 91              1497 	mov	exif,a
                           1498 	;; handle interrupt
                           1499 	;; epilogue
   05C4 D0 86              1500 	pop	dps
   05C6 D0 D0              1501 	pop	psw
   05C8 D0 83              1502 	pop	dph0
   05CA D0 82              1503 	pop	dpl0
   05CC D0 F0              1504 	pop	b
   05CE D0 E0              1505 	pop	acc
   05D0 32                 1506 	reti
                           1507 
   05D1                    1508 int6_isr:
   05D1 C0 E0              1509 	push	acc
   05D3 C0 F0              1510 	push	b
   05D5 C0 82              1511 	push	dpl0
   05D7 C0 83              1512 	push	dph0
   05D9 C0 D0              1513 	push	psw
   05DB 75 D0 00           1514 	mov	psw,#0x00
   05DE C0 86              1515 	push	dps
   05E0 75 86 00           1516 	mov	dps,#0
                           1517 	;; clear interrupt
   05E3 C2 DB              1518 	clr	eicon+3
                           1519 	;; handle interrupt
                           1520 	;; epilogue
   05E5 D0 86              1521 	pop	dps
   05E7 D0 D0              1522 	pop	psw
   05E9 D0 83              1523 	pop	dph0
   05EB D0 82              1524 	pop	dpl0
   05ED D0 F0              1525 	pop	b
   05EF D0 E0              1526 	pop	acc
   05F1 32                 1527 	reti
                           1528 
   05F2                    1529 usb_sudav_isr:
   05F2 C0 E0              1530 	push	acc
   05F4 C0 F0              1531 	push	b
   05F6 C0 82              1532 	push	dpl0
   05F8 C0 83              1533 	push	dph0
   05FA C0 84              1534 	push	dpl1
   05FC C0 85              1535 	push	dph1
   05FE C0 D0              1536 	push	psw
   0600 75 D0 00           1537 	mov	psw,#0x00
   0603 C0 86              1538 	push	dps
   0605 75 86 00           1539 	mov	dps,#0
   0608 C0 00              1540 	push	ar0
   060A C0 07              1541 	push	ar7
                           1542 	;; clear interrupt
   060C E5 91              1543 	mov	a,exif
   060E C2 E4              1544 	clr	acc.4
   0610 F5 91              1545 	mov	exif,a
   0612 90 7F AB           1546 	mov	dptr,#USBIRQ
   0615 74 01              1547 	mov	a,#0x01
   0617 F0                 1548 	movx	@dptr,a
                           1549 	;; handle interrupt
   0618 75 40 00           1550 	mov	ctrlcode,#0		; reset control out code
   061B 90 7F E9           1551 	mov	dptr,#(SETUPDAT+1)
   061E E0                 1552 	movx	a,@dptr			; bRequest field
                           1553 	;; standard commands
                           1554 	;; USB_REQ_GET_DESCRIPTOR
   061F B4 06 59           1555 	cjne	a,#USB_REQ_GET_DESCRIPTOR,cmdnotgetdesc
   0622 90 7F E8           1556 	mov	dptr,#SETUPDAT		; bRequestType == 0x80
   0625 E0                 1557 	movx	a,@dptr
   0626 B4 80 4F           1558 	cjne	a,#USB_DIR_IN,setupstallstd
   0629 90 7F EB           1559 	mov	dptr,#SETUPDAT+3
   062C E0                 1560 	movx	a,@dptr
   062D B4 01 0C           1561 	cjne	a,#USB_DT_DEVICE,cmdnotgetdescdev
   0630 90 7F D4           1562 	mov	dptr,#SUDPTRH
   0633 74 0D              1563 	mov	a,#>devicedescr
   0635 F0                 1564 	movx	@dptr,a
   0636 A3                 1565 	inc	dptr
   0637 74 FA              1566 	mov	a,#<devicedescr
   0639 F0                 1567 	movx	@dptr,a
   063A 80 39              1568 	sjmp	setupackstd
   063C                    1569 cmdnotgetdescdev:
   063C B4 02 12           1570 	cjne	a,#USB_DT_CONFIG,cmdnotgetdesccfg
   063F 90 7F EA           1571 	mov	dptr,#SETUPDAT+2
   0642 E0                 1572 	movx	a,@dptr
   0643 70 33              1573 	jnz	setupstallstd
   0645 90 7F D4           1574 	mov	dptr,#SUDPTRH
   0648 74 0E              1575 	mov	a,#>config0descr
   064A F0                 1576 	movx	@dptr,a
   064B A3                 1577 	inc	dptr
   064C 74 0C              1578 	mov	a,#<config0descr
   064E F0                 1579 	movx	@dptr,a
   064F 80 24              1580 	sjmp	setupackstd
   0651                    1581 cmdnotgetdesccfg:
   0651 B4 03 24           1582 	cjne	a,#USB_DT_STRING,setupstallstd
   0654 90 7F EA           1583 	mov	dptr,#SETUPDAT+2
   0657 E0                 1584 	movx	a,@dptr
   0658 24 FC              1585 	add	a,#-numstrings
   065A 40 1C              1586 	jc	setupstallstd
   065C E0                 1587 	movx	a,@dptr
   065D 25 E0              1588 	add	a,acc
   065F 24 51              1589 	add	a,#<stringdescr
   0661 F5 82              1590 	mov	dpl0,a
   0663 E4                 1591 	clr	a
   0664 34 0E              1592 	addc	a,#>stringdescr
   0666 F5 83              1593 	mov	dph0,a
   0668 E0                 1594 	movx	a,@dptr
   0669 F5 F0              1595 	mov	b,a
   066B A3                 1596 	inc	dptr
   066C E0                 1597 	movx	a,@dptr
   066D 90 7F D4           1598 	mov	dptr,#SUDPTRH
   0670 F0                 1599 	movx	@dptr,a
   0671 A3                 1600 	inc	dptr
   0672 E5 F0              1601 	mov	a,b
   0674 F0                 1602 	movx	@dptr,a
                           1603 	; sjmp	setupackstd	
   0675                    1604 setupackstd:
   0675 02 09 E9           1605 	ljmp	setupack
   0678                    1606 setupstallstd:
   0678 02 09 E5           1607 	ljmp	setupstall
   067B                    1608 cmdnotgetdesc:
                           1609 	;; USB_REQ_SET_CONFIGURATION
   067B B4 09 41           1610 	cjne	a,#USB_REQ_SET_CONFIGURATION,cmdnotsetconf
   067E 90 7F E8           1611 	mov	dptr,#SETUPDAT
   0681 E0                 1612 	movx	a,@dptr
   0682 70 F4              1613 	jnz	setupstallstd
   0684 90 7F EA           1614 	mov	dptr,#SETUPDAT+2
   0687 E0                 1615 	movx	a,@dptr
   0688 24 FE              1616 	add	a,#-2
   068A 40 EC              1617 	jc	setupstallstd
   068C E0                 1618 	movx	a,@dptr
   068D F5 70              1619 	mov	numconfig,a
   068F                    1620 cmdresettoggleshalt:
   068F 90 7F D7           1621 	mov	dptr,#TOGCTL
   0692 78 07              1622 	mov	r0,#7
   0694 E8                 1623 0$:	mov	a,r0
   0695 44 10              1624 	orl	a,#0x10
   0697 F0                 1625 	movx	@dptr,a
   0698 44 30              1626 	orl	a,#0x30
   069A F0                 1627 	movx	@dptr,a
   069B E8                 1628 	mov	a,r0
   069C F0                 1629 	movx	@dptr,a
   069D 44 20              1630 	orl	a,#0x20
   069F F0                 1631 	movx	@dptr,a
   06A0 D8 F2              1632 	djnz	r0,0$
   06A2 E4                 1633 	clr	a
   06A3 F0                 1634 	movx	@dptr,a
   06A4 74 02              1635 	mov	a,#2
   06A6 90 7F B6           1636 	mov	dptr,#IN1CS
   06A9 78 07              1637 	mov	r0,#7
   06AB F0                 1638 1$:	movx	@dptr,a
   06AC A3                 1639 	inc	dptr
   06AD A3                 1640 	inc	dptr
   06AE D8 FB              1641 	djnz	r0,1$
   06B0 90 7F C6           1642 	mov	dptr,#OUT1CS
   06B3 78 07              1643 	mov	r0,#7
   06B5 F0                 1644 2$:	movx	@dptr,a
   06B6 A3                 1645 	inc	dptr
   06B7 A3                 1646 	inc	dptr
   06B8 D8 FB              1647 	djnz	r0,2$
   06BA 12 0B 26           1648 	lcall	fillusbintr
   06BD 80 B6              1649 	sjmp	setupackstd
   06BF                    1650 cmdnotsetconf:
                           1651 	;; USB_REQ_SET_INTERFACE
   06BF B4 0B 1A           1652 	cjne	a,#USB_REQ_SET_INTERFACE,cmdnotsetint
   06C2 90 7F E8           1653 	mov	dptr,#SETUPDAT
   06C5 E0                 1654 	movx	a,@dptr
   06C6 B4 01 AF           1655 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_OUT,setupstallstd
   06C9 E5 70              1656 	mov	a,numconfig
   06CB B4 01 AA           1657 	cjne	a,#1,setupstallstd
   06CE 90 7F EC           1658 	mov	dptr,#SETUPDAT+4
   06D1 E0                 1659 	movx	a,@dptr
   06D2 70 A4              1660 	jnz	setupstallstd
   06D4 90 7F EA           1661 	mov	dptr,#SETUPDAT+2
   06D7 E0                 1662 	movx	a,@dptr
   06D8 F5 71              1663 	mov	altsetting,a
   06DA 80 B3              1664 	sjmp	cmdresettoggleshalt
   06DC                    1665 cmdnotsetint:
                           1666 	;; USB_REQ_GET_INTERFACE
   06DC B4 0A 20           1667 	cjne	a,#USB_REQ_GET_INTERFACE,cmdnotgetint
   06DF 90 7F E8           1668 	mov	dptr,#SETUPDAT
   06E2 E0                 1669 	movx	a,@dptr
   06E3 B4 81 92           1670 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,setupstallstd
   06E6 E5 70              1671 	mov	a,numconfig
   06E8 B4 01 8D           1672 	cjne	a,#1,setupstallstd
   06EB 90 7F EC           1673 	mov	dptr,#SETUPDAT+4
   06EE E0                 1674 	movx	a,@dptr
   06EF 70 87              1675 	jnz	setupstallstd
   06F1 E5 71              1676 	mov	a,altsetting
   06F3                    1677 cmdrespondonebyte:
   06F3 90 7F 00           1678 	mov	dptr,#IN0BUF
   06F6 F0                 1679 	movx	@dptr,a
   06F7 90 7F B5           1680 	mov	dptr,#IN0BC
   06FA 74 01              1681 	mov	a,#1
   06FC F0                 1682 	movx	@dptr,a	
   06FD 80 4E              1683 	sjmp	setupackstd2
   06FF                    1684 cmdnotgetint:
                           1685 	;; USB_REQ_GET_CONFIGURATION
   06FF B4 08 0B           1686 	cjne	a,#USB_REQ_GET_CONFIGURATION,cmdnotgetconf
   0702 90 7F E8           1687 	mov	dptr,#SETUPDAT
   0705 E0                 1688 	movx	a,@dptr
   0706 B4 80 47           1689 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,setupstallstd2
   0709 E5 70              1690 	mov	a,numconfig
   070B 80 E6              1691 	sjmp	cmdrespondonebyte	
   070D                    1692 cmdnotgetconf:
                           1693 	;; USB_REQ_GET_STATUS (0)
   070D 70 44              1694 	jnz	cmdnotgetstat
   070F 90 7F E8           1695 	mov	dptr,#SETUPDAT
   0712 E0                 1696 	movx	a,@dptr
   0713 B4 80 11           1697 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,cmdnotgetstatdev
   0716 74 01              1698 	mov	a,#1
   0718                    1699 cmdrespondstat:
   0718 90 7F 00           1700 	mov	dptr,#IN0BUF
   071B F0                 1701 	movx	@dptr,a
   071C A3                 1702 	inc	dptr
   071D E4                 1703 	clr	a
   071E F0                 1704 	movx	@dptr,a
   071F 90 7F B5           1705 	mov	dptr,#IN0BC
   0722 74 02              1706 	mov	a,#2
   0724 F0                 1707 	movx	@dptr,a	
   0725 80 26              1708 	sjmp	setupackstd2
   0727                    1709 cmdnotgetstatdev:
   0727 B4 81 03           1710 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,cmdnotgetstatintf
   072A E4                 1711 	clr	a
   072B 80 EB              1712 	sjmp	cmdrespondstat
   072D                    1713 cmdnotgetstatintf:
   072D B4 82 20           1714 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_IN,setupstallstd2
   0730 90 7F EC           1715 	mov	dptr,#SETUPDAT+4
   0733 E0                 1716 	movx	a,@dptr
   0734 90 7F C4           1717 	mov	dptr,#OUT1CS-2
   0737 30 E7 03           1718 	jnb	acc.7,0$
   073A 90 7F B4           1719 	mov	dptr,#IN1CS-2
   073D 54 0F              1720 0$:	anl	a,#15
   073F 60 0F              1721 	jz	setupstallstd2
   0741 20 E3 0C           1722 	jb	acc.3,setupstallstd2
   0744 25 E0              1723 	add	a,acc
   0746 25 82              1724 	add	a,dpl0
   0748 F5 82              1725 	mov	dpl0,a
   074A E0                 1726 	movx	a,@dptr
   074B 80 CB              1727 	sjmp	cmdrespondstat
   074D                    1728 setupackstd2:
   074D 02 09 E9           1729 	ljmp	setupack
   0750                    1730 setupstallstd2:
   0750 02 09 E5           1731 	ljmp	setupstall
   0753                    1732 cmdnotgetstat:
                           1733 	;; USB_REQ_SET_FEATURE
   0753 B4 03 05           1734 	cjne	a,#USB_REQ_SET_FEATURE,cmdnotsetftr
   0756 75 F0 01           1735 	mov	b,#1
   0759 80 06              1736 	sjmp	handleftr
   075B                    1737 cmdnotsetftr:
                           1738 	;; USB_REQ_CLEAR_FEATURE
   075B B4 01 44           1739 	cjne	a,#USB_REQ_CLEAR_FEATURE,cmdnotclrftr
   075E 75 F0 00           1740 	mov	b,#0
   0761                    1741 handleftr:
   0761 90 7F E8           1742 	mov	dptr,#SETUPDAT
   0764 E0                 1743 	movx	a,@dptr
   0765 B4 02 E8           1744 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_OUT,setupstallstd2
   0768 A3                 1745 	inc	dptr
   0769 A3                 1746 	inc	dptr
   076A E0                 1747 	movx	a,@dptr
   076B 70 E3              1748 	jnz	setupstallstd2	; not ENDPOINT_HALT feature
   076D A3                 1749 	inc	dptr
   076E E0                 1750 	movx	a,@dptr
   076F 70 DF              1751 	jnz	setupstallstd2
   0771 A3                 1752 	inc	dptr
   0772 E0                 1753 	movx	a,@dptr
   0773 90 7F C4           1754 	mov	dptr,#OUT1CS-2
   0776 30 E7 05           1755 	jnb	acc.7,0$
   0779 90 7F B4           1756 	mov	dptr,#IN1CS-2
   077C 44 10              1757 	orl	a,#0x10
   077E 20 E3 CF           1758 0$:	jb	acc.3,setupstallstd2
                           1759 	;; clear data toggle
   0781 54 1F              1760 	anl	a,#0x1f
   0783 05 86              1761 	inc	dps
   0785 90 7F D7           1762 	mov	dptr,#TOGCTL
   0788 F0                 1763 	movx	@dptr,a
   0789 44 20              1764 	orl	a,#0x20
   078B F0                 1765 	movx	@dptr,a
   078C 54 0F              1766 	anl	a,#15
   078E F0                 1767 	movx	@dptr,a
   078F 15 86              1768 	dec	dps	
                           1769 	;; clear/set ep halt feature
   0791 25 E0              1770 	add	a,acc
   0793 25 82              1771 	add	a,dpl0
   0795 F5 82              1772 	mov	dpl0,a
   0797 E5 F0              1773 	mov	a,b
   0799 F0                 1774 	movx	@dptr,a
   079A 80 B1              1775 	sjmp	setupackstd2
                           1776 
   079C                    1777 cmdnotc0_1:
   079C 02 08 5F           1778 	ljmp	cmdnotc0
   079F                    1779 setupstallc0_1:
   079F 02 09 E5           1780 	ljmp	setupstall
                           1781 	
   07A2                    1782 cmdnotclrftr:
                           1783 	;; vendor specific commands
                           1784 	;; 0xc0
   07A2 B4 C0 F7           1785 	cjne	a,#0xc0,cmdnotc0_1
   07A5 90 7F E8           1786 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   07A8 E0                 1787 	movx	a,@dptr
   07A9 B4 C0 F3           1788 	cjne	a,#0xc0,setupstallc0_1
                           1789 	;; fill status buffer
   07AC E5 45              1790 	mov	a,txstate
   07AE F5 F0              1791 	mov	b,a
   07B0 D2 F2              1792 	setb	b.2
   07B2 45 51              1793 	orl	a,pttforce
   07B4 B4 00 02           1794 	cjne	a,#0,0$
   07B7 C2 F2              1795 	clr	b.2
   07B9 90 C0 09           1796 0$:	mov	dptr,#FSKSTAT
   07BC E0                 1797 	movx	a,@dptr
   07BD A2 E0              1798 	mov	c,acc.0
   07BF 92 F3              1799 	mov	b.3,c
   07C1 A2 0A              1800 	mov	c,uartempty
   07C3 92 F5              1801 	mov	b.5,c
   07C5 E4                 1802 	clr	a
   07C6 90 7F 04           1803 	mov	dptr,#(IN0BUF+4)
   07C9 F0                 1804 	movx	@dptr,a
                           1805 	;; bytewide elements
   07CA 90 7F 00           1806 	mov	dptr,#(IN0BUF)
   07CD E5 F0              1807 	mov	a,b
   07CF F0                 1808 	movx	@dptr,a
   07D0 E5 48              1809 	mov	a,txwr
   07D2 F4                 1810 	cpl	a
   07D3 25 49              1811 	add	a,txrd
   07D5 54 0F              1812 	anl	a,#(TXCHUNKS-1)
   07D7 90 7F 01           1813 	mov	dptr,#(IN0BUF+1)
   07DA F0                 1814 	movx	@dptr,a
   07DB E5 54              1815 	mov	a,rxrd
   07DD F4                 1816 	cpl	a
   07DE 25 53              1817 	add	a,rxwr
   07E0 04                 1818 	inc	a
   07E1 54 1F              1819 	anl	a,#(RXCHUNKS-1)
   07E3 90 7F 02           1820 	mov	dptr,#(IN0BUF+2)
   07E6 F0                 1821 	movx	@dptr,a
   07E7 90 C0 04           1822 	mov	dptr,#FSKRSSI
   07EA E0                 1823 	movx	a,@dptr
   07EB 90 7F 03           1824 	mov	dptr,#(IN0BUF+3)
   07EE F0                 1825 	movx	@dptr,a
                           1826 	;; counter
   07EF 05 44              1827 	inc	irqcount
   07F1 E5 44              1828 	mov	a,irqcount
   07F3 90 7F 05           1829 	mov	dptr,#(IN0BUF+5)
   07F6 F0                 1830 	movx	@dptr,a
                           1831 	;; additional fields (HDLC state mach)
   07F7 E5 45              1832 	mov	a,txstate
   07F9 A3                 1833 	inc	dptr
   07FA F0                 1834 	movx	@dptr,a
   07FB E5 49              1835 	mov	a,txrd
   07FD A3                 1836 	inc	dptr
   07FE F0                 1837 	movx	@dptr,a
   07FF E5 48              1838 	mov	a,txwr
   0801 A3                 1839 	inc	dptr
   0802 F0                 1840 	movx	@dptr,a
   0803 E5 4A              1841 	mov	a,txtwr
   0805 A3                 1842 	inc	dptr
   0806 F0                 1843 	movx	@dptr,a
   0807 74 00              1844 	mov	a,#0
   0809 A3                 1845 	inc	dptr
   080A F0                 1846 	movx	@dptr,a
   080B E5 46              1847 	mov	a,flagcnt
   080D A3                 1848 	inc	dptr
   080E F0                 1849 	movx	@dptr,a
   080F E5 47              1850 	mov	a,flagcnt+1
   0811 A3                 1851 	inc	dptr
   0812 F0                 1852 	movx	@dptr,a
   0813 E5 52              1853 	mov	a,rxstate
   0815 A3                 1854 	inc	dptr
   0816 F0                 1855 	movx	@dptr,a
   0817 E5 54              1856 	mov	a,rxrd
   0819 A3                 1857 	inc	dptr
   081A F0                 1858 	movx	@dptr,a
   081B E5 53              1859 	mov	a,rxwr
   081D A3                 1860 	inc	dptr
   081E F0                 1861 	movx	@dptr,a
   081F E5 55              1862 	mov	a,rxtwr
   0821 A3                 1863 	inc	dptr
   0822 F0                 1864 	movx	@dptr,a
   0823 E5 56              1865 	mov	a,rxcntc
   0825 A3                 1866 	inc	dptr
   0826 F0                 1867 	movx	@dptr,a
                           1868 	;; FPGA registers
   0827 E5 5D              1869 	mov	a,tmprxcnt
   0829 90 7F 12           1870 	mov	dptr,#(IN0BUF+18)
   082C F0                 1871 	movx	@dptr,a
   082D 90 C0 01           1872 	mov	dptr,#FSKSHSTATUS
   0830 E0                 1873 	movx	a,@dptr
   0831 90 7F 13           1874 	mov	dptr,#(IN0BUF+19)
   0834 F0                 1875 	movx	@dptr,a
   0835 90 C0 08           1876 	mov	dptr,#FSKCTRL
   0838 E0                 1877 	movx	a,@dptr
   0839 90 7F 14           1878 	mov	dptr,#(IN0BUF+20)
   083C F0                 1879 	movx	@dptr,a
   083D 90 C0 09           1880 	mov	dptr,#FSKSTAT
   0840 E0                 1881 	movx	a,@dptr
   0841 90 7F 15           1882 	mov	dptr,#(IN0BUF+21)
   0844 F0                 1883 	movx	@dptr,a
                           1884 	;; Anchor Registers
   0845 90 7F C8           1885 	mov	dptr,#OUT2CS
   0848 E0                 1886 	movx	a,@dptr
   0849 90 7F 16           1887 	mov	dptr,#(IN0BUF+22)
   084C F0                 1888 	movx	@dptr,a
                           1889 	;; set length
   084D 90 7F EE           1890 	mov	dptr,#SETUPDAT+6	; wLength
   0850 E0                 1891 	movx	a,@dptr
   0851 24 E9              1892 	add	a,#-(6+12+4+1)
   0853 50 01              1893 	jnc	4$
   0855 E4                 1894 	clr	a
   0856 24 17              1895 4$:	add	a,#(6+12+4+1)
   0858 90 7F B5           1896 	mov	dptr,#IN0BC
   085B F0                 1897 	movx	@dptr,a
   085C 02 09 E9           1898 	ljmp	setupack
   085F                    1899 cmdnotc0:
                           1900 	;; 0xc8
   085F B4 C8 19           1901 	cjne	a,#0xc8,cmdnotc8
   0862 90 7F E8           1902 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0865 E0                 1903 	movx	a,@dptr
   0866 B4 C0 0F           1904 	cjne	a,#0xc0,setupstallc8
   0869 74 03              1905 	mov	a,#3
   086B 90 7F 00           1906 	mov	dptr,#IN0BUF
   086E F0                 1907 	movx	@dptr,a
   086F 90 7F B5           1908 	mov	dptr,#IN0BC
   0872 74 01              1909 	mov	a,#1
   0874 F0                 1910 	movx	@dptr,a
   0875 02 09 E9           1911 	ljmp	setupack
   0878                    1912 setupstallc8:
   0878 02 09 E5           1913 	ljmp	setupstall
   087B                    1914 cmdnotc8:
                           1915 	;; 0xc9
   087B B4 C9 21           1916 	cjne	a,#0xc9,cmdnotc9
   087E 90 7F E8           1917 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0881 E0                 1918 	movx	a,@dptr
   0882 B4 C0 17           1919 	cjne	a,#0xc0,setupstallc9
   0885 90 7F 00           1920 	mov	dptr,#IN0BUF
   0888 78 F0              1921 	mov	r0,#parserial
   088A E2                 1922 0$:	movx	a,@r0
   088B 60 05              1923 	jz	1$
   088D F0                 1924 	movx	@dptr,a
   088E 08                 1925 	inc	r0
   088F A3                 1926 	inc	dptr
   0890 80 F8              1927 	sjmp	0$
   0892 E8                 1928 1$:	mov	a,r0
   0893 24 10              1929 	add	a,#-0xf0	; -parserial
   0895 90 7F B5           1930 	mov	dptr,#IN0BC
   0898 F0                 1931 	movx	@dptr,a
   0899 02 09 E9           1932 	ljmp	setupack
   089C                    1933 setupstallc9:
   089C 02 09 E5           1934 	ljmp	setupstall
   089F                    1935 cmdnotc9:
                           1936 	;; 0xd0
   089F B4 D0 4D           1937 	cjne	a,#0xd0,cmdnotd0
   08A2 90 7F E8           1938 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   08A5 E0                 1939 	movx	a,@dptr
   08A6 B4 C0 43           1940 	cjne	a,#0xc0,setupstalld0
   08A9 90 7F EC           1941 	mov	dptr,#SETUPDAT+4	; wIndex
   08AC E0                 1942 	movx	a,@dptr
   08AD B4 01 0A           1943 	cjne	a,#1,0$
   08B0 90 7F EA           1944 	mov	dptr,#SETUPDAT+2	; wValue
   08B3 E0                 1945 	movx	a,@dptr
   08B4 54 01              1946 	anl	a,#1
   08B6 F5 51              1947 	mov	pttforce,a
   08B8 D2 09              1948 	setb	pttforcechg
   08BA                    1949 0$:	;; PTT status
   08BA 90 7F 00           1950 	mov	dptr,#IN0BUF
   08BD E5 45              1951 	mov	a,txstate
   08BF 45 51              1952 	orl	a,pttforce
   08C1 60 02              1953 	jz	1$
   08C3 74 01              1954 	mov	a,#1
   08C5 F0                 1955 1$:	movx	@dptr,a
                           1956 	;; DCD status
   08C6 90 C0 09           1957 	mov	dptr,#FSKSTAT
   08C9 E0                 1958 	movx	a,@dptr
   08CA 54 01              1959 	anl	a,#1
   08CC 64 01              1960 	xrl	a,#1
   08CE 90 7F 01           1961 	mov	dptr,#IN0BUF+1
   08D1 F0                 1962 	movx	@dptr,a
                           1963 	;; RSSI
   08D2 90 C0 04           1964 	mov	dptr,#FSKRSSI
   08D5 E0                 1965 	movx	a,@dptr
   08D6 90 7F 02           1966 	mov	dptr,#IN0BUF+2
   08D9 F0                 1967 	movx	@dptr,a
                           1968 	;; length
   08DA 90 7F EE           1969 	mov	dptr,#SETUPDAT+6	; wLength
   08DD E0                 1970 	movx	a,@dptr
   08DE 24 FD              1971 	add	a,#-3
   08E0 50 01              1972 	jnc	2$
   08E2 E4                 1973 	clr	a
   08E3 24 03              1974 2$:	add	a,#3
   08E5 90 7F B5           1975 	mov	dptr,#IN0BC
   08E8 F0                 1976 	movx	@dptr,a
   08E9 02 09 E9           1977 	ljmp	setupack
   08EC                    1978 setupstalld0:
   08EC 02 09 E5           1979 	ljmp	setupstall
   08EF                    1980 cmdnotd0:
                           1981 	;; 0xd1
   08EF B4 D1 29           1982 	cjne	a,#0xd1,cmdnotd1
   08F2 90 7F E8           1983 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   08F5 E0                 1984 	movx	a,@dptr
   08F6 B4 C0 1F           1985 	cjne	a,#0xc0,setupstalld1
   08F9 90 7F 00           1986 	mov	dptr,#IN0BUF
   08FC 78 E0              1987 	mov	r0,#parbitratetx
   08FE 7F 06              1988 	mov	r7,#6
   0900 E2                 1989 1$:	movx	a,@r0
   0901 F0                 1990 	movx	@dptr,a
   0902 A3                 1991 	inc	dptr
   0903 08                 1992 	inc	r0
   0904 DF FA              1993 	djnz	r7,1$
                           1994 	;; length
   0906 90 7F EE           1995 	mov	dptr,#SETUPDAT+6	; wLength
   0909 E0                 1996 	movx	a,@dptr
   090A 24 FA              1997 	add	a,#-6
   090C 50 01              1998 	jnc	2$
   090E E4                 1999 	clr	a
   090F 24 06              2000 2$:	add	a,#6
   0911 90 7F B5           2001 	mov	dptr,#IN0BC
   0914 F0                 2002 	movx	@dptr,a
   0915 02 09 E9           2003 	ljmp	setupack
   0918                    2004 setupstalld1:
   0918 02 09 E5           2005 	ljmp	setupstall
   091B                    2006 cmdnotd1:
                           2007 	;; 0xd2
   091B B4 D2 20           2008 	cjne	a,#0xd2,cmdnotd2
   091E 90 7F E8           2009 	mov	dptr,#SETUPDAT	; bRequestType == 0x40
   0921 E0                 2010 	movx	a,@dptr
   0922 B4 40 16           2011 	cjne	a,#0x40,setupstalld2
   0925 90 7F EA           2012 	mov	dptr,#SETUPDAT+2	; wValue
   0928 E0                 2013 	movx	a,@dptr
   0929 F5 F0              2014 	mov	b,a
   092B 90 7F 98           2015 	mov	dptr,#OUTC
   092E E0                 2016 	movx	a,@dptr
   092F A2 F0              2017 	mov	c,b.0
   0931 92 E3              2018 	mov	acc.3,c
   0933 A2 F1              2019 	mov	c,b.1
   0935 92 E5              2020 	mov	acc.5,c
   0937 F0                 2021 	movx	@dptr,a
   0938 02 09 E9           2022 	ljmp	setupack
   093B                    2023 setupstalld2:
   093B 02 09 E5           2024 	ljmp	setupstall
   093E                    2025 cmdnotd2:
                           2026 	;; 0xd3
   093E B4 D3 16           2027 	cjne	a,#0xd3,cmdnotd3
   0941 90 7F E8           2028 	mov	dptr,#SETUPDAT	; bRequestType == 0x40
   0944 E0                 2029 	movx	a,@dptr
   0945 B4 40 07           2030 	cjne	a,#0x40,setupstalld3
   0948 90 7F EA           2031 	mov	dptr,#SETUPDAT+2	; wValue
   094B E0                 2032 	movx	a,@dptr
   094C 10 0A 03           2033 	jbc	uartempty,cmdd2cont
   094F                    2034 setupstalld3:
   094F 02 09 E5           2035 	ljmp	setupstall
   0952                    2036 cmdd2cont:
   0952 F5 99              2037 	mov	sbuf0,a
   0954 02 09 E9           2038 	ljmp	setupack
   0957                    2039 cmdnotd3:
                           2040 	;; 0xd4
   0957 B4 D4 4D           2041 	cjne	a,#0xd4,cmdnotd4
   095A 90 7F E8           2042 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   095D E0                 2043 	movx	a,@dptr
   095E B4 C0 43           2044 	cjne	a,#0xc0,setupstalld4
   0961 90 7F EC           2045 	mov	dptr,#SETUPDAT+4	; wIndex
   0964 E0                 2046 	movx	a,@dptr
   0965 90 7F EA           2047 	mov	dptr,#SETUPDAT+2	; wValue
   0968 B4 01 07           2048 	cjne	a,#1,0$
   096B E0                 2049 	movx	a,@dptr
   096C 90 C0 0D           2050 	mov	dptr,#FSKMDISCOUT
   096F F0                 2051 	movx	@dptr,a
   0970 80 08              2052 	sjmp	1$
   0972 B4 02 05           2053 0$:	cjne	a,#2,1$
   0975 E0                 2054 	movx	a,@dptr
   0976 90 C0 0C           2055 	mov	dptr,#FSKMDISCTRIS
   0979 F0                 2056 	movx	@dptr,a
   097A 90 C0 0E           2057 1$:	mov	dptr,#FSKMDISCIN
   097D E0                 2058 	movx	a,@dptr
   097E 90 7F 00           2059 	mov	dptr,#IN0BUF+0
   0981 F0                 2060 	movx	@dptr,a
   0982 90 C0 0D           2061 	mov	dptr,#FSKMDISCOUT
   0985 E0                 2062 	movx	a,@dptr
   0986 90 7F 01           2063 	mov	dptr,#IN0BUF+1
   0989 F0                 2064 	movx	@dptr,a
   098A 90 C0 0C           2065 	mov	dptr,#FSKMDISCTRIS
   098D E0                 2066 	movx	a,@dptr
   098E 90 7F 02           2067 	mov	dptr,#IN0BUF+2
   0991 F0                 2068 	movx	@dptr,a
                           2069 	;; length
   0992 90 7F EE           2070 	mov	dptr,#SETUPDAT+6	; wLength
   0995 E0                 2071 	movx	a,@dptr
   0996 24 FD              2072 	add	a,#-3
   0998 50 01              2073 	jnc	2$
   099A E4                 2074 	clr	a
   099B 24 03              2075 2$:	add	a,#3
   099D 90 7F B5           2076 	mov	dptr,#IN0BC
   09A0 F0                 2077 	movx	@dptr,a
   09A1 02 09 E9           2078 	ljmp	setupack
   09A4                    2079 setupstalld4:
   09A4 02 09 E5           2080 	ljmp	setupstall
   09A7                    2081 cmdnotd4:
                           2082 	;; 0xd5
   09A7 B4 D5 3B           2083 	cjne	a,#0xd5,cmdnotd5
   09AA 90 7F E8           2084 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   09AD E0                 2085 	movx	a,@dptr
   09AE B4 C0 31           2086 	cjne	a,#0xc0,setupstalld5
   09B1 90 7F EC           2087 	mov	dptr,#SETUPDAT+4	; wIndex
   09B4 E0                 2088 	movx	a,@dptr
   09B5 B4 01 08           2089 	cjne	a,#1,0$
   09B8 90 7F EA           2090 	mov	dptr,#SETUPDAT+2	; wValue
   09BB E0                 2091 	movx	a,@dptr
   09BC 90 C0 0A           2092 	mov	dptr,#FSKT7FOUT
   09BF F0                 2093 	movx	@dptr,a
   09C0 90 C0 0B           2094 0$:	mov	dptr,#FSKT7FIN
   09C3 E0                 2095 	movx	a,@dptr
   09C4 90 7F 00           2096 	mov	dptr,#IN0BUF+0
   09C7 F0                 2097 	movx	@dptr,a
   09C8 90 C0 0A           2098 	mov	dptr,#FSKT7FOUT
   09CB E0                 2099 	movx	a,@dptr
   09CC 90 7F 01           2100 	mov	dptr,#IN0BUF+1
   09CF F0                 2101 	movx	@dptr,a
                           2102 	;; length
   09D0 90 7F EE           2103 	mov	dptr,#SETUPDAT+6	; wLength
   09D3 E0                 2104 	movx	a,@dptr
   09D4 24 FE              2105 	add	a,#-2
   09D6 50 01              2106 	jnc	2$
   09D8 E4                 2107 	clr	a
   09D9 24 02              2108 2$:	add	a,#2
   09DB 90 7F B5           2109 	mov	dptr,#IN0BC
   09DE F0                 2110 	movx	@dptr,a
   09DF 02 09 E9           2111 	ljmp	setupack
   09E2                    2112 setupstalld5:
   09E2 02 09 E5           2113 	ljmp	setupstall
   09E5                    2114 cmdnotd5:
                           2115 	;; unknown commands fall through to setupstall
                           2116 
   09E5                    2117 setupstall:
   09E5 74 03              2118 	mov	a,#3
   09E7 80 02              2119 	sjmp	endsetup
   09E9                    2120 setupack:
   09E9 74 02              2121 	mov	a,#2
   09EB                    2122 endsetup:
   09EB 90 7F B4           2123 	mov	dptr,#EP0CS
   09EE F0                 2124 	movx	@dptr,a
   09EF                    2125 endusbisr:
                           2126 	;; epilogue
   09EF D0 07              2127 	pop	ar7
   09F1 D0 00              2128 	pop	ar0
   09F3 D0 86              2129 	pop	dps
   09F5 D0 D0              2130 	pop	psw
   09F7 D0 85              2131 	pop	dph1
   09F9 D0 84              2132 	pop	dpl1
   09FB D0 83              2133 	pop	dph0
   09FD D0 82              2134 	pop	dpl0
   09FF D0 F0              2135 	pop	b
   0A01 D0 E0              2136 	pop	acc
   0A03 32                 2137 	reti
                           2138 
   0A04                    2139 usb_sof_isr:
   0A04 C0 E0              2140 	push	acc
   0A06 C0 F0              2141 	push	b
   0A08 C0 82              2142 	push	dpl0
   0A0A C0 83              2143 	push	dph0
   0A0C C0 D0              2144 	push	psw
   0A0E 75 D0 00           2145 	mov	psw,#0x00
   0A11 C0 86              2146 	push	dps
   0A13 75 86 00           2147 	mov	dps,#0
                           2148 	;; clear interrupt
   0A16 E5 91              2149 	mov	a,exif
   0A18 C2 E4              2150 	clr	acc.4
   0A1A F5 91              2151 	mov	exif,a
   0A1C 90 7F AB           2152 	mov	dptr,#USBIRQ
   0A1F 74 02              2153 	mov	a,#0x02
   0A21 F0                 2154 	movx	@dptr,a
                           2155 	;; handle interrupt
                           2156 	;; epilogue
   0A22 D0 86              2157 	pop	dps
   0A24 D0 D0              2158 	pop	psw
   0A26 D0 83              2159 	pop	dph0
   0A28 D0 82              2160 	pop	dpl0
   0A2A D0 F0              2161 	pop	b
   0A2C D0 E0              2162 	pop	acc
   0A2E 32                 2163 	reti
                           2164 
                           2165 
   0A2F                    2166 usb_sutok_isr:
   0A2F C0 E0              2167 	push	acc
   0A31 C0 F0              2168 	push	b
   0A33 C0 82              2169 	push	dpl0
   0A35 C0 83              2170 	push	dph0
   0A37 C0 D0              2171 	push	psw
   0A39 75 D0 00           2172 	mov	psw,#0x00
   0A3C C0 86              2173 	push	dps
   0A3E 75 86 00           2174 	mov	dps,#0
                           2175 	;; clear interrupt
   0A41 E5 91              2176 	mov	a,exif
   0A43 C2 E4              2177 	clr	acc.4
   0A45 F5 91              2178 	mov	exif,a
   0A47 90 7F AB           2179 	mov	dptr,#USBIRQ
   0A4A 74 04              2180 	mov	a,#0x04
   0A4C F0                 2181 	movx	@dptr,a
                           2182 	;; handle interrupt
                           2183 	;; epilogue
   0A4D D0 86              2184 	pop	dps
   0A4F D0 D0              2185 	pop	psw
   0A51 D0 83              2186 	pop	dph0
   0A53 D0 82              2187 	pop	dpl0
   0A55 D0 F0              2188 	pop	b
   0A57 D0 E0              2189 	pop	acc
   0A59 32                 2190 	reti
                           2191 
   0A5A                    2192 usb_suspend_isr:
   0A5A C0 E0              2193 	push	acc
   0A5C C0 F0              2194 	push	b
   0A5E C0 82              2195 	push	dpl0
   0A60 C0 83              2196 	push	dph0
   0A62 C0 D0              2197 	push	psw
   0A64 75 D0 00           2198 	mov	psw,#0x00
   0A67 C0 86              2199 	push	dps
   0A69 75 86 00           2200 	mov	dps,#0
                           2201 	;; clear interrupt
   0A6C E5 91              2202 	mov	a,exif
   0A6E C2 E4              2203 	clr	acc.4
   0A70 F5 91              2204 	mov	exif,a
   0A72 90 7F AB           2205 	mov	dptr,#USBIRQ
   0A75 74 08              2206 	mov	a,#0x08
   0A77 F0                 2207 	movx	@dptr,a
                           2208 	;; handle interrupt
                           2209 	;; epilogue
   0A78 D0 86              2210 	pop	dps
   0A7A D0 D0              2211 	pop	psw
   0A7C D0 83              2212 	pop	dph0
   0A7E D0 82              2213 	pop	dpl0
   0A80 D0 F0              2214 	pop	b
   0A82 D0 E0              2215 	pop	acc
   0A84 32                 2216 	reti
                           2217 
   0A85                    2218 usb_usbreset_isr:
   0A85 C0 E0              2219 	push	acc
   0A87 C0 F0              2220 	push	b
   0A89 C0 82              2221 	push	dpl0
   0A8B C0 83              2222 	push	dph0
   0A8D C0 D0              2223 	push	psw
   0A8F 75 D0 00           2224 	mov	psw,#0x00
   0A92 C0 86              2225 	push	dps
   0A94 75 86 00           2226 	mov	dps,#0
                           2227 	;; clear interrupt
   0A97 E5 91              2228 	mov	a,exif
   0A99 C2 E4              2229 	clr	acc.4
   0A9B F5 91              2230 	mov	exif,a
   0A9D 90 7F AB           2231 	mov	dptr,#USBIRQ
   0AA0 74 10              2232 	mov	a,#0x10
   0AA2 F0                 2233 	movx	@dptr,a
                           2234 	;; handle interrupt
                           2235 	;; epilogue
   0AA3 D0 86              2236 	pop	dps
   0AA5 D0 D0              2237 	pop	psw
   0AA7 D0 83              2238 	pop	dph0
   0AA9 D0 82              2239 	pop	dpl0
   0AAB D0 F0              2240 	pop	b
   0AAD D0 E0              2241 	pop	acc
   0AAF 32                 2242 	reti
                           2243 
   0AB0                    2244 usb_ep0in_isr:
   0AB0 C0 E0              2245 	push	acc
   0AB2 C0 F0              2246 	push	b
   0AB4 C0 82              2247 	push	dpl0
   0AB6 C0 83              2248 	push	dph0
   0AB8 C0 84              2249 	push	dpl1
   0ABA C0 85              2250 	push	dph1
   0ABC C0 D0              2251 	push	psw
   0ABE 75 D0 00           2252 	mov	psw,#0x00
   0AC1 C0 86              2253 	push	dps
   0AC3 75 86 00           2254 	mov	dps,#0
   0AC6 C0 00              2255 	push	ar0
   0AC8 C0 07              2256 	push	ar7
                           2257 	;; clear interrupt
   0ACA E5 91              2258 	mov	a,exif
   0ACC C2 E4              2259 	clr	acc.4
   0ACE F5 91              2260 	mov	exif,a
   0AD0 90 7F A9           2261 	mov	dptr,#IN07IRQ
   0AD3 74 01              2262 	mov	a,#0x01
   0AD5 F0                 2263 	movx	@dptr,a
                           2264 	;; handle interrupt
                           2265 
                           2266 ;ep0install:
                           2267 ;	mov	a,#3
                           2268 ;	sjmp	ep0incs
                           2269 ;ep0inack:
                           2270 ;	mov	a,#2
                           2271 ;ep0incs:
                           2272 ;	mov	dptr,#EP0CS
                           2273 ;	movx	@dptr,a
                           2274 ;ep0inendisr:
                           2275 	;; epilogue
   0AD6 D0 07              2276 	pop	ar7
   0AD8 D0 00              2277 	pop	ar0
   0ADA D0 86              2278 	pop	dps
   0ADC D0 D0              2279 	pop	psw
   0ADE D0 85              2280 	pop	dph1
   0AE0 D0 84              2281 	pop	dpl1
   0AE2 D0 83              2282 	pop	dph0
   0AE4 D0 82              2283 	pop	dpl0
   0AE6 D0 F0              2284 	pop	b
   0AE8 D0 E0              2285 	pop	acc
   0AEA 32                 2286 	reti
                           2287 
   0AEB                    2288 usb_ep0out_isr:
   0AEB C0 E0              2289 	push	acc
   0AED C0 F0              2290 	push	b
   0AEF C0 82              2291 	push	dpl0
   0AF1 C0 83              2292 	push	dph0
   0AF3 C0 84              2293 	push	dpl1
   0AF5 C0 85              2294 	push	dph1
   0AF7 C0 D0              2295 	push	psw
   0AF9 75 D0 00           2296 	mov	psw,#0x00
   0AFC C0 86              2297 	push	dps
   0AFE 75 86 00           2298 	mov	dps,#0
   0B01 C0 00              2299 	push	ar0
   0B03 C0 06              2300 	push	ar6
                           2301 	;; clear interrupt
   0B05 E5 91              2302 	mov	a,exif
   0B07 C2 E4              2303 	clr	acc.4
   0B09 F5 91              2304 	mov	exif,a
   0B0B 90 7F AA           2305 	mov	dptr,#OUT07IRQ
   0B0E 74 01              2306 	mov	a,#0x01
   0B10 F0                 2307 	movx	@dptr,a
                           2308 	;; handle interrupt
                           2309 
                           2310 ;ep0outstall:
                           2311 ;	mov	ctrlcode,#0
                           2312 ;	mov	a,#3
                           2313 ;	sjmp	ep0outcs
                           2314 ;ep0outack:
                           2315 ;	mov	txwr,txtwr
                           2316 ;	mov	ctrlcode,#0
                           2317 ;	mov	a,#2
                           2318 ;ep0outcs:
                           2319 ;	mov	dptr,#EP0CS
                           2320 ;	movx	@dptr,a
                           2321 ;ep0outendisr:
                           2322 	;; epilogue
   0B11 D0 06              2323 	pop	ar6
   0B13 D0 00              2324 	pop	ar0
   0B15 D0 86              2325 	pop	dps
   0B17 D0 D0              2326 	pop	psw
   0B19 D0 85              2327 	pop	dph1
   0B1B D0 84              2328 	pop	dpl1
   0B1D D0 83              2329 	pop	dph0
   0B1F D0 82              2330 	pop	dpl0
   0B21 D0 F0              2331 	pop	b
   0B23 D0 E0              2332 	pop	acc
   0B25 32                 2333 	reti
                           2334 
   0B26                    2335 fillusbintr::
   0B26 E5 45              2336 	mov	a,txstate
   0B28 F5 F0              2337 	mov	b,a
   0B2A D2 F2              2338 	setb	b.2
   0B2C 45 51              2339 	orl	a,pttforce
   0B2E B4 00 02           2340 	cjne	a,#0,0$
   0B31 C2 F2              2341 	clr	b.2
   0B33 90 C0 09           2342 0$:	mov	dptr,#FSKSTAT
   0B36 E0                 2343 	movx	a,@dptr
   0B37 A2 E0              2344 	mov	c,acc.0
   0B39 92 F3              2345 	mov	b.3,c
   0B3B A2 0A              2346 	mov	c,uartempty
   0B3D 92 F5              2347 	mov	b.5,c
   0B3F D3                 2348 	setb	c
   0B40 E5 54              2349 	mov	a,rxrd
   0B42 B5 53 06           2350 	cjne	a,rxwr,1$
   0B45 90 7F B8           2351 	mov	dptr,#(IN2CS)
   0B48 E0                 2352 	movx	a,@dptr
   0B49 A2 E1              2353 	mov	c,acc.1
   0B4B 92 F4              2354 1$:	mov	b.4,c
                           2355 	;; bytewide elements
   0B4D 90 7E 80           2356 	mov	dptr,#(IN1BUF)
   0B50 E5 F0              2357 	mov	a,b
   0B52 F0                 2358 	movx	@dptr,a
   0B53 E5 48              2359 	mov	a,txwr
   0B55 F4                 2360 	cpl	a
   0B56 25 49              2361 	add	a,txrd
   0B58 54 0F              2362 	anl	a,#(TXCHUNKS-1)
   0B5A 90 7E 81           2363 	mov	dptr,#(IN1BUF+1)
   0B5D F0                 2364 	movx	@dptr,a
   0B5E E5 54              2365 	mov	a,rxrd
   0B60 F4                 2366 	cpl	a
   0B61 25 53              2367 	add	a,rxwr
   0B63 04                 2368 	inc	a
   0B64 54 1F              2369 	anl	a,#(RXCHUNKS-1)
   0B66 90 7E 82           2370 	mov	dptr,#(IN1BUF+2)
   0B69 F0                 2371 	movx	@dptr,a
   0B6A 90 C0 04           2372 	mov	dptr,#FSKRSSI
   0B6D E0                 2373 	movx	a,@dptr
   0B6E 90 7E 83           2374 	mov	dptr,#(IN1BUF+3)
   0B71 F0                 2375 	movx	@dptr,a
                           2376 	; counter
   0B72 05 44              2377 	inc	irqcount
   0B74 E5 44              2378 	mov	a,irqcount
   0B76 90 7E 84           2379 	mov	dptr,#(IN1BUF+4)
   0B79 F0                 2380 	movx	@dptr,a
                           2381 	; UART buffer
   0B7A 75 F0 05           2382 	mov	b,#5
   0B7D 90 7E 85           2383 	mov	dptr,#(IN1BUF+5)
   0B80 E5 6F              2384 2$:	mov	a,uartrd
   0B82 B5 6E 07           2385 	cjne	a,uartwr,3$
                           2386 	; set length
   0B85 90 7F B7           2387 	mov	dptr,#IN1BC
   0B88 E5 F0              2388 	mov	a,b
   0B8A F0                 2389 	movx	@dptr,a
   0B8B 22                 2390 	ret
                           2391 	
   0B8C 24 5E              2392 3$:	add	a,#uartbuf
   0B8E F8                 2393 	mov	r0,a
   0B8F E6                 2394 	mov	a,@r0
   0B90 F0                 2395 	movx	@dptr,a
   0B91 A3                 2396 	inc	dptr
   0B92 05 F0              2397 	inc	b
   0B94 E5 6F              2398 	mov	a,uartrd
   0B96 04                 2399 	inc	a
   0B97 54 0F              2400 	anl	a,#0xf
   0B99 F5 6F              2401 	mov	uartrd,a
   0B9B 80 E3              2402 	sjmp	2$
                           2403 
                           2404 	
   0B9D                    2405 usb_ep1in_isr:
   0B9D C0 E0              2406 	push	acc
   0B9F C0 F0              2407 	push	b
   0BA1 C0 82              2408 	push	dpl0
   0BA3 C0 83              2409 	push	dph0
   0BA5 C0 D0              2410 	push	psw
   0BA7 75 D0 00           2411 	mov	psw,#0x00
   0BAA C0 86              2412 	push	dps
   0BAC 75 86 00           2413 	mov	dps,#0
                           2414 	;; clear interrupt
   0BAF E5 91              2415 	mov	a,exif
   0BB1 C2 E4              2416 	clr	acc.4
   0BB3 F5 91              2417 	mov	exif,a
   0BB5 90 7F A9           2418 	mov	dptr,#IN07IRQ
   0BB8 74 02              2419 	mov	a,#0x02
   0BBA F0                 2420 	movx	@dptr,a
                           2421 	;; handle interrupt
   0BBB 12 0B 26           2422 	lcall	fillusbintr
                           2423 	;; epilogue
   0BBE D0 86              2424 	pop	dps
   0BC0 D0 D0              2425 	pop	psw
   0BC2 D0 83              2426 	pop	dph0
   0BC4 D0 82              2427 	pop	dpl0
   0BC6 D0 F0              2428 	pop	b
   0BC8 D0 E0              2429 	pop	acc
   0BCA 32                 2430 	reti
                           2431 
   0BCB                    2432 usb_ep1out_isr:
   0BCB C0 E0              2433 	push	acc
   0BCD C0 F0              2434 	push	b
   0BCF C0 82              2435 	push	dpl0
   0BD1 C0 83              2436 	push	dph0
   0BD3 C0 D0              2437 	push	psw
   0BD5 75 D0 00           2438 	mov	psw,#0x00
   0BD8 C0 86              2439 	push	dps
   0BDA 75 86 00           2440 	mov	dps,#0
                           2441 	;; clear interrupt
   0BDD E5 91              2442 	mov	a,exif
   0BDF C2 E4              2443 	clr	acc.4
   0BE1 F5 91              2444 	mov	exif,a
   0BE3 90 7F AA           2445 	mov	dptr,#OUT07IRQ
   0BE6 74 02              2446 	mov	a,#0x02
   0BE8 F0                 2447 	movx	@dptr,a
                           2448 	;; handle interrupt
                           2449 	;; epilogue
   0BE9 D0 86              2450 	pop	dps
   0BEB D0 D0              2451 	pop	psw
   0BED D0 83              2452 	pop	dph0
   0BEF D0 82              2453 	pop	dpl0
   0BF1 D0 F0              2454 	pop	b
   0BF3 D0 E0              2455 	pop	acc
   0BF5 32                 2456 	reti
                           2457 
   0BF6                    2458 usb_ep2in_isr:
   0BF6 C0 E0              2459 	push	acc
   0BF8 C0 F0              2460 	push	b
   0BFA C0 82              2461 	push	dpl0
   0BFC C0 83              2462 	push	dph0
   0BFE C0 D0              2463 	push	psw
   0C00 75 D0 00           2464 	mov	psw,#0x00
   0C03 C0 86              2465 	push	dps
   0C05 75 86 00           2466 	mov	dps,#0
                           2467 	;; clear interrupt
   0C08 E5 91              2468 	mov	a,exif
   0C0A C2 E4              2469 	clr	acc.4
   0C0C F5 91              2470 	mov	exif,a
   0C0E 90 7F A9           2471 	mov	dptr,#IN07IRQ
   0C11 74 04              2472 	mov	a,#0x04
   0C13 F0                 2473 	movx	@dptr,a
                           2474 	;; handle interrupt
                           2475 	;; epilogue
   0C14 D0 86              2476 	pop	dps
   0C16 D0 D0              2477 	pop	psw
   0C18 D0 83              2478 	pop	dph0
   0C1A D0 82              2479 	pop	dpl0
   0C1C D0 F0              2480 	pop	b
   0C1E D0 E0              2481 	pop	acc
   0C20 32                 2482 	reti
                           2483 
   0C21                    2484 usb_ep2out_isr:
   0C21 C0 E0              2485 	push	acc
   0C23 C0 F0              2486 	push	b
   0C25 C0 82              2487 	push	dpl0
   0C27 C0 83              2488 	push	dph0
   0C29 C0 D0              2489 	push	psw
   0C2B 75 D0 00           2490 	mov	psw,#0x00
   0C2E C0 86              2491 	push	dps
   0C30 75 86 00           2492 	mov	dps,#0
                           2493 	;; clear interrupt
   0C33 E5 91              2494 	mov	a,exif
   0C35 C2 E4              2495 	clr	acc.4
   0C37 F5 91              2496 	mov	exif,a
   0C39 90 7F AA           2497 	mov	dptr,#OUT07IRQ
   0C3C 74 04              2498 	mov	a,#0x04
   0C3E F0                 2499 	movx	@dptr,a
                           2500 	;; handle interrupt
                           2501 	;; epilogue
   0C3F D0 86              2502 	pop	dps
   0C41 D0 D0              2503 	pop	psw
   0C43 D0 83              2504 	pop	dph0
   0C45 D0 82              2505 	pop	dpl0
   0C47 D0 F0              2506 	pop	b
   0C49 D0 E0              2507 	pop	acc
   0C4B 32                 2508 	reti
                           2509 
   0C4C                    2510 usb_ep3in_isr:
   0C4C C0 E0              2511 	push	acc
   0C4E C0 F0              2512 	push	b
   0C50 C0 82              2513 	push	dpl0
   0C52 C0 83              2514 	push	dph0
   0C54 C0 D0              2515 	push	psw
   0C56 75 D0 00           2516 	mov	psw,#0x00
   0C59 C0 86              2517 	push	dps
   0C5B 75 86 00           2518 	mov	dps,#0
                           2519 	;; clear interrupt
   0C5E E5 91              2520 	mov	a,exif
   0C60 C2 E4              2521 	clr	acc.4
   0C62 F5 91              2522 	mov	exif,a
   0C64 90 7F A9           2523 	mov	dptr,#IN07IRQ
   0C67 74 08              2524 	mov	a,#0x08
   0C69 F0                 2525 	movx	@dptr,a
                           2526 	;; handle interrupt
                           2527 	;; epilogue
   0C6A D0 86              2528 	pop	dps
   0C6C D0 D0              2529 	pop	psw
   0C6E D0 83              2530 	pop	dph0
   0C70 D0 82              2531 	pop	dpl0
   0C72 D0 F0              2532 	pop	b
   0C74 D0 E0              2533 	pop	acc
   0C76 32                 2534 	reti
                           2535 
   0C77                    2536 usb_ep3out_isr:
   0C77 C0 E0              2537 	push	acc
   0C79 C0 F0              2538 	push	b
   0C7B C0 82              2539 	push	dpl0
   0C7D C0 83              2540 	push	dph0
   0C7F C0 D0              2541 	push	psw
   0C81 75 D0 00           2542 	mov	psw,#0x00
   0C84 C0 86              2543 	push	dps
   0C86 75 86 00           2544 	mov	dps,#0
                           2545 	;; clear interrupt
   0C89 E5 91              2546 	mov	a,exif
   0C8B C2 E4              2547 	clr	acc.4
   0C8D F5 91              2548 	mov	exif,a
   0C8F 90 7F AA           2549 	mov	dptr,#OUT07IRQ
   0C92 74 08              2550 	mov	a,#0x08
   0C94 F0                 2551 	movx	@dptr,a
                           2552 	;; handle interrupt
                           2553 	;; epilogue
   0C95 D0 86              2554 	pop	dps
   0C97 D0 D0              2555 	pop	psw
   0C99 D0 83              2556 	pop	dph0
   0C9B D0 82              2557 	pop	dpl0
   0C9D D0 F0              2558 	pop	b
   0C9F D0 E0              2559 	pop	acc
   0CA1 32                 2560 	reti
                           2561 
   0CA2                    2562 usb_ep4in_isr:
   0CA2 C0 E0              2563 	push	acc
   0CA4 C0 F0              2564 	push	b
   0CA6 C0 82              2565 	push	dpl0
   0CA8 C0 83              2566 	push	dph0
   0CAA C0 D0              2567 	push	psw
   0CAC 75 D0 00           2568 	mov	psw,#0x00
   0CAF C0 86              2569 	push	dps
   0CB1 75 86 00           2570 	mov	dps,#0
                           2571 	;; clear interrupt
   0CB4 E5 91              2572 	mov	a,exif
   0CB6 C2 E4              2573 	clr	acc.4
   0CB8 F5 91              2574 	mov	exif,a
   0CBA 90 7F A9           2575 	mov	dptr,#IN07IRQ
   0CBD 74 10              2576 	mov	a,#0x10
   0CBF F0                 2577 	movx	@dptr,a
                           2578 	;; handle interrupt
                           2579 	;; epilogue
   0CC0 D0 86              2580 	pop	dps
   0CC2 D0 D0              2581 	pop	psw
   0CC4 D0 83              2582 	pop	dph0
   0CC6 D0 82              2583 	pop	dpl0
   0CC8 D0 F0              2584 	pop	b
   0CCA D0 E0              2585 	pop	acc
   0CCC 32                 2586 	reti
                           2587 
   0CCD                    2588 usb_ep4out_isr:
   0CCD C0 E0              2589 	push	acc
   0CCF C0 F0              2590 	push	b
   0CD1 C0 82              2591 	push	dpl0
   0CD3 C0 83              2592 	push	dph0
   0CD5 C0 D0              2593 	push	psw
   0CD7 75 D0 00           2594 	mov	psw,#0x00
   0CDA C0 86              2595 	push	dps
   0CDC 75 86 00           2596 	mov	dps,#0
                           2597 	;; clear interrupt
   0CDF E5 91              2598 	mov	a,exif
   0CE1 C2 E4              2599 	clr	acc.4
   0CE3 F5 91              2600 	mov	exif,a
   0CE5 90 7F AA           2601 	mov	dptr,#OUT07IRQ
   0CE8 74 10              2602 	mov	a,#0x10
   0CEA F0                 2603 	movx	@dptr,a
                           2604 	;; handle interrupt
                           2605 	;; epilogue
   0CEB D0 86              2606 	pop	dps
   0CED D0 D0              2607 	pop	psw
   0CEF D0 83              2608 	pop	dph0
   0CF1 D0 82              2609 	pop	dpl0
   0CF3 D0 F0              2610 	pop	b
   0CF5 D0 E0              2611 	pop	acc
   0CF7 32                 2612 	reti
                           2613 
   0CF8                    2614 usb_ep5in_isr:
   0CF8 C0 E0              2615 	push	acc
   0CFA C0 F0              2616 	push	b
   0CFC C0 82              2617 	push	dpl0
   0CFE C0 83              2618 	push	dph0
   0D00 C0 D0              2619 	push	psw
   0D02 75 D0 00           2620 	mov	psw,#0x00
   0D05 C0 86              2621 	push	dps
   0D07 75 86 00           2622 	mov	dps,#0
                           2623 	;; clear interrupt
   0D0A E5 91              2624 	mov	a,exif
   0D0C C2 E4              2625 	clr	acc.4
   0D0E F5 91              2626 	mov	exif,a
   0D10 90 7F A9           2627 	mov	dptr,#IN07IRQ
   0D13 74 20              2628 	mov	a,#0x20
   0D15 F0                 2629 	movx	@dptr,a
                           2630 	;; handle interrupt
                           2631 	;; epilogue
   0D16 D0 86              2632 	pop	dps
   0D18 D0 D0              2633 	pop	psw
   0D1A D0 83              2634 	pop	dph0
   0D1C D0 82              2635 	pop	dpl0
   0D1E D0 F0              2636 	pop	b
   0D20 D0 E0              2637 	pop	acc
   0D22 32                 2638 	reti
                           2639 
   0D23                    2640 usb_ep5out_isr:
   0D23 C0 E0              2641 	push	acc
   0D25 C0 F0              2642 	push	b
   0D27 C0 82              2643 	push	dpl0
   0D29 C0 83              2644 	push	dph0
   0D2B C0 D0              2645 	push	psw
   0D2D 75 D0 00           2646 	mov	psw,#0x00
   0D30 C0 86              2647 	push	dps
   0D32 75 86 00           2648 	mov	dps,#0
                           2649 	;; clear interrupt
   0D35 E5 91              2650 	mov	a,exif
   0D37 C2 E4              2651 	clr	acc.4
   0D39 F5 91              2652 	mov	exif,a
   0D3B 90 7F AA           2653 	mov	dptr,#OUT07IRQ
   0D3E 74 20              2654 	mov	a,#0x20
   0D40 F0                 2655 	movx	@dptr,a
                           2656 	;; handle interrupt
                           2657 	;; epilogue
   0D41 D0 86              2658 	pop	dps
   0D43 D0 D0              2659 	pop	psw
   0D45 D0 83              2660 	pop	dph0
   0D47 D0 82              2661 	pop	dpl0
   0D49 D0 F0              2662 	pop	b
   0D4B D0 E0              2663 	pop	acc
   0D4D 32                 2664 	reti
                           2665 
   0D4E                    2666 usb_ep6in_isr:
   0D4E C0 E0              2667 	push	acc
   0D50 C0 F0              2668 	push	b
   0D52 C0 82              2669 	push	dpl0
   0D54 C0 83              2670 	push	dph0
   0D56 C0 D0              2671 	push	psw
   0D58 75 D0 00           2672 	mov	psw,#0x00
   0D5B C0 86              2673 	push	dps
   0D5D 75 86 00           2674 	mov	dps,#0
                           2675 	;; clear interrupt
   0D60 E5 91              2676 	mov	a,exif
   0D62 C2 E4              2677 	clr	acc.4
   0D64 F5 91              2678 	mov	exif,a
   0D66 90 7F A9           2679 	mov	dptr,#IN07IRQ
   0D69 74 40              2680 	mov	a,#0x40
   0D6B F0                 2681 	movx	@dptr,a
                           2682 	;; handle interrupt
                           2683 	;; epilogue
   0D6C D0 86              2684 	pop	dps
   0D6E D0 D0              2685 	pop	psw
   0D70 D0 83              2686 	pop	dph0
   0D72 D0 82              2687 	pop	dpl0
   0D74 D0 F0              2688 	pop	b
   0D76 D0 E0              2689 	pop	acc
   0D78 32                 2690 	reti
                           2691 
   0D79                    2692 usb_ep6out_isr:
   0D79 C0 E0              2693 	push	acc
   0D7B C0 F0              2694 	push	b
   0D7D C0 82              2695 	push	dpl0
   0D7F C0 83              2696 	push	dph0
   0D81 C0 D0              2697 	push	psw
   0D83 75 D0 00           2698 	mov	psw,#0x00
   0D86 C0 86              2699 	push	dps
   0D88 75 86 00           2700 	mov	dps,#0
                           2701 	;; clear interrupt
   0D8B E5 91              2702 	mov	a,exif
   0D8D C2 E4              2703 	clr	acc.4
   0D8F F5 91              2704 	mov	exif,a
   0D91 90 7F AA           2705 	mov	dptr,#OUT07IRQ
   0D94 74 40              2706 	mov	a,#0x40
   0D96 F0                 2707 	movx	@dptr,a
                           2708 	;; handle interrupt
                           2709 	;; epilogue
   0D97 D0 86              2710 	pop	dps
   0D99 D0 D0              2711 	pop	psw
   0D9B D0 83              2712 	pop	dph0
   0D9D D0 82              2713 	pop	dpl0
   0D9F D0 F0              2714 	pop	b
   0DA1 D0 E0              2715 	pop	acc
   0DA3 32                 2716 	reti
                           2717 
   0DA4                    2718 usb_ep7in_isr:
   0DA4 C0 E0              2719 	push	acc
   0DA6 C0 F0              2720 	push	b
   0DA8 C0 82              2721 	push	dpl0
   0DAA C0 83              2722 	push	dph0
   0DAC C0 D0              2723 	push	psw
   0DAE 75 D0 00           2724 	mov	psw,#0x00
   0DB1 C0 86              2725 	push	dps
   0DB3 75 86 00           2726 	mov	dps,#0
                           2727 	;; clear interrupt
   0DB6 E5 91              2728 	mov	a,exif
   0DB8 C2 E4              2729 	clr	acc.4
   0DBA F5 91              2730 	mov	exif,a
   0DBC 90 7F A9           2731 	mov	dptr,#IN07IRQ
   0DBF 74 80              2732 	mov	a,#0x80
   0DC1 F0                 2733 	movx	@dptr,a
                           2734 	;; handle interrupt
                           2735 	;; epilogue
   0DC2 D0 86              2736 	pop	dps
   0DC4 D0 D0              2737 	pop	psw
   0DC6 D0 83              2738 	pop	dph0
   0DC8 D0 82              2739 	pop	dpl0
   0DCA D0 F0              2740 	pop	b
   0DCC D0 E0              2741 	pop	acc
   0DCE 32                 2742 	reti
                           2743 
   0DCF                    2744 usb_ep7out_isr:
   0DCF C0 E0              2745 	push	acc
   0DD1 C0 F0              2746 	push	b
   0DD3 C0 82              2747 	push	dpl0
   0DD5 C0 83              2748 	push	dph0
   0DD7 C0 D0              2749 	push	psw
   0DD9 75 D0 00           2750 	mov	psw,#0x00
   0DDC C0 86              2751 	push	dps
   0DDE 75 86 00           2752 	mov	dps,#0
                           2753 	;; clear interrupt
   0DE1 E5 91              2754 	mov	a,exif
   0DE3 C2 E4              2755 	clr	acc.4
   0DE5 F5 91              2756 	mov	exif,a
   0DE7 90 7F AA           2757 	mov	dptr,#OUT07IRQ
   0DEA 74 80              2758 	mov	a,#0x80
   0DEC F0                 2759 	movx	@dptr,a
                           2760 	;; handle interrupt
                           2761 	;; epilogue
   0DED D0 86              2762 	pop	dps
   0DEF D0 D0              2763 	pop	psw
   0DF1 D0 83              2764 	pop	dph0
   0DF3 D0 82              2765 	pop	dpl0
   0DF5 D0 F0              2766 	pop	b
   0DF7 D0 E0              2767 	pop	acc
   0DF9 32                 2768 	reti
                           2769 
                           2770 	;; -----------------------------------------------------
                           2771 	;; USB descriptors
                           2772 	;; -----------------------------------------------------
                           2773 
                           2774 	;; Device and/or Interface Class codes
                    0000   2775 	USB_CLASS_PER_INTERFACE         = 0
                    0001   2776 	USB_CLASS_AUDIO                 = 1
                    0002   2777 	USB_CLASS_COMM                  = 2
                    0003   2778 	USB_CLASS_HID                   = 3
                    0007   2779 	USB_CLASS_PRINTER               = 7
                    0008   2780 	USB_CLASS_MASS_STORAGE          = 8
                    0009   2781 	USB_CLASS_HUB                   = 9
                    00FF   2782 	USB_CLASS_VENDOR_SPEC           = 0xff
                           2783 
                           2784 	;; Descriptor types
                    0001   2785 	USB_DT_DEVICE                   = 0x01
                    0002   2786 	USB_DT_CONFIG                   = 0x02
                    0003   2787 	USB_DT_STRING                   = 0x03
                    0004   2788 	USB_DT_INTERFACE                = 0x04
                    0005   2789 	USB_DT_ENDPOINT                 = 0x05
                           2790 
                           2791 	;; Standard requests
                    0000   2792 	USB_REQ_GET_STATUS              = 0x00
                    0001   2793 	USB_REQ_CLEAR_FEATURE           = 0x01
                    0003   2794 	USB_REQ_SET_FEATURE             = 0x03
                    0005   2795 	USB_REQ_SET_ADDRESS             = 0x05
                    0006   2796 	USB_REQ_GET_DESCRIPTOR          = 0x06
                    0007   2797 	USB_REQ_SET_DESCRIPTOR          = 0x07
                    0008   2798 	USB_REQ_GET_CONFIGURATION       = 0x08
                    0009   2799 	USB_REQ_SET_CONFIGURATION       = 0x09
                    000A   2800 	USB_REQ_GET_INTERFACE           = 0x0A
                    000B   2801 	USB_REQ_SET_INTERFACE           = 0x0B
                    000C   2802 	USB_REQ_SYNCH_FRAME             = 0x0C
                           2803 
                           2804 	;; USB Request Type and Endpoint Directions
                    0000   2805 	USB_DIR_OUT                     = 0
                    0080   2806 	USB_DIR_IN                      = 0x80
                           2807 
                    0000   2808 	USB_TYPE_STANDARD               = (0x00 << 5)
                    0020   2809 	USB_TYPE_CLASS                  = (0x01 << 5)
                    0040   2810 	USB_TYPE_VENDOR                 = (0x02 << 5)
                    0060   2811 	USB_TYPE_RESERVED               = (0x03 << 5)
                           2812 
                    0000   2813 	USB_RECIP_DEVICE                = 0x00
                    0001   2814 	USB_RECIP_INTERFACE             = 0x01
                    0002   2815 	USB_RECIP_ENDPOINT              = 0x02
                    0003   2816 	USB_RECIP_OTHER                 = 0x03
                           2817 
                           2818 	;; Request target types.
                    0000   2819 	USB_RT_DEVICE                   = 0x00
                    0001   2820 	USB_RT_INTERFACE                = 0x01
                    0002   2821 	USB_RT_ENDPOINT                 = 0x02
                           2822 
                    BAC0   2823 	VENDID	= 0xbac0
                    6136   2824 	PRODID	= 0x6136
                           2825 
   0DFA                    2826 devicedescr:
   0DFA 12                 2827 	.db	18			; bLength
   0DFB 01                 2828 	.db	USB_DT_DEVICE		; bDescriptorType
   0DFC 00 01              2829 	.db	0x00, 0x01		; bcdUSB
   0DFE FF                 2830 	.db	USB_CLASS_VENDOR_SPEC	; bDeviceClass
   0DFF 00                 2831 	.db	0			; bDeviceSubClass
   0E00 FF                 2832 	.db	0xff			; bDeviceProtocol
   0E01 40                 2833 	.db	0x40			; bMaxPacketSize0
   0E02 C0 BA              2834 	.db	<VENDID,>VENDID		; idVendor
   0E04 36 61              2835 	.db	<PRODID,>PRODID		; idProduct
   0E06 01 00              2836 	.db	0x01,0x00		; bcdDevice
   0E08 01                 2837 	.db	1			; iManufacturer
   0E09 02                 2838 	.db	2			; iProduct
   0E0A 03                 2839 	.db	3			; iSerialNumber
   0E0B 01                 2840 	.db	1			; bNumConfigurations
                           2841 
   0E0C                    2842 config0descr:
   0E0C 09                 2843 	.db	9			; bLength
   0E0D 02                 2844 	.db	USB_DT_CONFIG		; bDescriptorType
   0E0E 45 00              2845 	.db	<config0sz,>config0sz	; wTotalLength
   0E10 01                 2846 	.db	1			; bNumInterfaces
   0E11 01                 2847 	.db	1			; bConfigurationValue
   0E12 00                 2848 	.db	0			; iConfiguration
   0E13 40                 2849 	.db	0b01000000		; bmAttributs (self powered)
   0E14 00                 2850 	.db	0			; MaxPower (mA/2) (self powered so 0)
                           2851 	;; interface descriptor I0:A0
   0E15 09                 2852 	.db	9			; bLength
   0E16 04                 2853 	.db	USB_DT_INTERFACE	; bDescriptorType
   0E17 00                 2854 	.db	0			; bInterfaceNumber
   0E18 00                 2855 	.db	0			; bAlternateSetting
   0E19 03                 2856 	.db	3			; bNumEndpoints
   0E1A FF                 2857 	.db	0xff			; bInterfaceClass (vendor specific)
   0E1B 00                 2858 	.db	0x00			; bInterfaceSubClass
   0E1C FF                 2859 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0E1D 00                 2860 	.db	0			; iInterface
                           2861 	;; endpoint descriptor I0:A0:E0
   0E1E 07                 2862 	.db	7			; bLength
   0E1F 05                 2863 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E20 81                 2864 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0E21 02                 2865 	.db	0x02			; bmAttributes (bulk)
   0E22 40 00              2866 	.db	0x40,0x00		; wMaxPacketSize
   0E24 00                 2867 	.db	0			; bInterval
                           2868 	;; endpoint descriptor I0:A0:E1
   0E25 07                 2869 	.db	7			; bLength
   0E26 05                 2870 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E27 82                 2871 	.db	(USB_DIR_IN | 2)	; bEndpointAddress
   0E28 02                 2872 	.db	0x02			; bmAttributes (bulk)
   0E29 40 00              2873 	.db	0x40,0x00		; wMaxPacketSize
   0E2B 00                 2874 	.db	0			; bInterval
                           2875 	;; endpoint descriptor I0:A0:E2
   0E2C 07                 2876 	.db	7			; bLength
   0E2D 05                 2877 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E2E 02                 2878 	.db	(USB_DIR_OUT | 2)	; bEndpointAddress
   0E2F 02                 2879 	.db	0x02			; bmAttributes (bulk)
   0E30 40 00              2880 	.db	0x40,0x00		; wMaxPacketSize
   0E32 00                 2881 	.db	0			; bInterval
                           2882 	;; interface descriptor I0:A1
   0E33 09                 2883 	.db	9			; bLength
   0E34 04                 2884 	.db	USB_DT_INTERFACE	; bDescriptorType
   0E35 00                 2885 	.db	0			; bInterfaceNumber
   0E36 01                 2886 	.db	1			; bAlternateSetting
   0E37 03                 2887 	.db	3			; bNumEndpoints
   0E38 FF                 2888 	.db	0xff			; bInterfaceClass (vendor specific)
   0E39 00                 2889 	.db	0x00			; bInterfaceSubClass
   0E3A FF                 2890 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0E3B 00                 2891 	.db	0			; iInterface
                           2892 	;; endpoint descriptor I0:A1:E0
   0E3C 07                 2893 	.db	7			; bLength
   0E3D 05                 2894 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E3E 81                 2895 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0E3F 03                 2896 	.db	0x03			; bmAttributes (interrupt)
   0E40 40 00              2897 	.db	0x40,0x00		; wMaxPacketSize
   0E42 0A                 2898 	.db	10			; bInterval
                           2899 	;; endpoint descriptor I0:A1:E1
   0E43 07                 2900 	.db	7			; bLength
   0E44 05                 2901 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E45 82                 2902 	.db	(USB_DIR_IN | 2)	; bEndpointAddress
   0E46 02                 2903 	.db	0x02			; bmAttributes (bulk)
   0E47 40 00              2904 	.db	0x40,0x00		; wMaxPacketSize
   0E49 00                 2905 	.db	0			; bInterval
                           2906 	;; endpoint descriptor I0:A1:E2
   0E4A 07                 2907 	.db	7			; bLength
   0E4B 05                 2908 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E4C 02                 2909 	.db	(USB_DIR_OUT | 2)	; bEndpointAddress
   0E4D 02                 2910 	.db	0x02			; bmAttributes (bulk)
   0E4E 40 00              2911 	.db	0x40,0x00		; wMaxPacketSize
   0E50 00                 2912 	.db	0			; bInterval
                           2913 
                    0045   2914 config0sz = . - config0descr
                           2915 
   0E51                    2916 stringdescr:
   0E51 59 0E              2917 	.db	<string0,>string0
   0E53 5D 0E              2918 	.db	<string1,>string1
   0E55 6B 0E              2919 	.db	<string2,>string2
   0E57 89 0E              2920 	.db	<stringserial,>stringserial
                           2921 
                    0004   2922 numstrings = (. - stringdescr)/2
                           2923 
   0E59                    2924 string0:
   0E59 04                 2925 	.db	string0sz		; bLength
   0E5A 03                 2926 	.db	USB_DT_STRING		; bDescriptorType
   0E5B 00 00              2927 	.db	0,0			; LANGID[0]: Lang Neutral
                    0004   2928 string0sz = . - string0
                           2929 
   0E5D                    2930 string1:
   0E5D 0E                 2931 	.db	string1sz		; bLength
   0E5E 03                 2932 	.db	USB_DT_STRING		; bDescriptorType
   0E5F 42 00 61 00 79 00  2933 	.db	'B,0,'a,0,'y,0,'c,0,'o,0,'m,0
        63 00 6F 00 6D 00
                    000E   2934 string1sz = . - string1
                           2935 
   0E6B                    2936 string2:
   0E6B 1E                 2937 	.db	string2sz		; bLength
   0E6C 03                 2938 	.db	USB_DT_STRING		; bDescriptorType
   0E6D 55 00 53 00 42 00  2939 	.db	'U,0,'S,0,'B,0,'F,0,'L,0,'E,0,'X,0,' ,0
        46 00 4C 00 45 00
        58 00 20 00
   0E7D 28 00 41 00 46 00  2940 	.db	'(,0,'A,0,'F,0,'S,0,'K,0,'),0
        53 00 4B 00 29 00
                    001E   2941 string2sz = . - string2
                           2942 
   0E89                    2943 stringserial:
   0E89 02                 2944 	.db	2			; bLength
   0E8A 03                 2945 	.db	USB_DT_STRING		; bDescriptorType
   0E8B 00 00 00 00 00 00  2946 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
   0E9B 00 00 00 00 00 00  2947 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
                           2948 
