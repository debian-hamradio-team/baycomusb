                              1 	.module main
                              2 
                              3 	;; define code segments link order
                              4 	.area CODE (CODE)
                              5 	.area CSEG (CODE)
                              6 	.area GSINIT (CODE)
                              7 	.area GSINIT2 (CODE)
                              8 
                              9 	;; -----------------------------------------------------
                             10 
                             11 	;; special function registers (which are not predefined)
                    0082     12 	dpl0    = 0x82
                    0083     13 	dph0    = 0x83
                    0084     14 	dpl1    = 0x84
                    0085     15 	dph1    = 0x85
                    0086     16 	dps     = 0x86
                    008E     17 	ckcon   = 0x8E
                    008F     18 	spc_fnc = 0x8F
                    0091     19 	exif    = 0x91
                    0092     20 	mpage   = 0x92
                    0098     21 	scon0   = 0x98
                    0099     22 	sbuf0   = 0x99
                    00C0     23 	scon1   = 0xC0
                    00C1     24 	sbuf1   = 0xC1
                    00D8     25 	eicon   = 0xD8
                    00E8     26 	eie     = 0xE8
                    00F8     27 	eip     = 0xF8
                             28 
                             29 	;; anchor xdata registers
                    7F00     30 	IN0BUF		= 0x7F00
                    7EC0     31 	OUT0BUF		= 0x7EC0
                    7E80     32 	IN1BUF		= 0x7E80
                    7E40     33 	OUT1BUF		= 0x7E40
                    7E00     34 	IN2BUF		= 0x7E00
                    7DC0     35 	OUT2BUF		= 0x7DC0
                    7D80     36 	IN3BUF		= 0x7D80
                    7D40     37 	OUT3BUF		= 0x7D40
                    7D00     38 	IN4BUF		= 0x7D00
                    7CC0     39 	OUT4BUF		= 0x7CC0
                    7C80     40 	IN5BUF		= 0x7C80
                    7C40     41 	OUT5BUF		= 0x7C40
                    7C00     42 	IN6BUF		= 0x7C00
                    7BC0     43 	OUT6BUF		= 0x7BC0
                    7B80     44 	IN7BUF		= 0x7B80
                    7B40     45 	OUT7BUF		= 0x7B40
                    7FE8     46 	SETUPBUF	= 0x7FE8
                    7FE8     47 	SETUPDAT	= 0x7FE8
                             48 
                    7FB4     49 	EP0CS		= 0x7FB4
                    7FB5     50 	IN0BC		= 0x7FB5
                    7FB6     51 	IN1CS		= 0x7FB6
                    7FB7     52 	IN1BC		= 0x7FB7
                    7FB8     53 	IN2CS		= 0x7FB8
                    7FB9     54 	IN2BC		= 0x7FB9
                    7FBA     55 	IN3CS		= 0x7FBA
                    7FBB     56 	IN3BC		= 0x7FBB
                    7FBC     57 	IN4CS		= 0x7FBC
                    7FBD     58 	IN4BC		= 0x7FBD
                    7FBE     59 	IN5CS		= 0x7FBE
                    7FBF     60 	IN5BC		= 0x7FBF
                    7FC0     61 	IN6CS		= 0x7FC0
                    7FC1     62 	IN6BC		= 0x7FC1
                    7FC2     63 	IN7CS		= 0x7FC2
                    7FC3     64 	IN7BC		= 0x7FC3
                    7FC5     65 	OUT0BC		= 0x7FC5
                    7FC6     66 	OUT1CS		= 0x7FC6
                    7FC7     67 	OUT1BC		= 0x7FC7
                    7FC8     68 	OUT2CS		= 0x7FC8
                    7FC9     69 	OUT2BC		= 0x7FC9
                    7FCA     70 	OUT3CS		= 0x7FCA
                    7FCB     71 	OUT3BC		= 0x7FCB
                    7FCC     72 	OUT4CS		= 0x7FCC
                    7FCD     73 	OUT4BC		= 0x7FCD
                    7FCE     74 	OUT5CS		= 0x7FCE
                    7FCF     75 	OUT5BC		= 0x7FCF
                    7FD0     76 	OUT6CS		= 0x7FD0
                    7FD1     77 	OUT6BC		= 0x7FD1
                    7FD2     78 	OUT7CS		= 0x7FD2
                    7FD3     79 	OUT7BC		= 0x7FD3
                             80 
                    7FA8     81 	IVEC		= 0x7FA8
                    7FA9     82 	IN07IRQ		= 0x7FA9
                    7FAA     83 	OUT07IRQ	= 0x7FAA
                    7FAB     84 	USBIRQ		= 0x7FAB
                    7FAC     85 	IN07IEN		= 0x7FAC
                    7FAD     86 	OUT07IEN	= 0x7FAD
                    7FAE     87 	USBIEN		= 0x7FAE
                    7FAF     88 	USBBAV		= 0x7FAF
                    7FB2     89 	BPADDRH		= 0x7FB2
                    7FB3     90 	BPADDRL		= 0x7FB3
                             91 
                    7FD4     92 	SUDPTRH		= 0x7FD4
                    7FD5     93 	SUDPTRL		= 0x7FD5
                    7FD6     94 	USBCS		= 0x7FD6
                    7FD7     95 	TOGCTL		= 0x7FD7
                    7FD8     96 	USBFRAMEL	= 0x7FD8
                    7FD9     97 	USBFRAMEH	= 0x7FD9
                    7FDB     98 	FNADDR		= 0x7FDB
                    7FDD     99 	USBPAIR		= 0x7FDD
                    7FDE    100 	IN07VAL		= 0x7FDE
                    7FDF    101 	OUT07VAL	= 0x7FDF
                    7FE3    102 	AUTOPTRH	= 0x7FE3
                    7FE4    103 	AUTOPTRL	= 0x7FE4
                    7FE5    104 	AUTODATA	= 0x7FE5
                            105 
                            106 	;; isochronous endpoints. only available if ISODISAB=0
                            107 
                    7F60    108 	OUT8DATA	= 0x7F60
                    7F61    109 	OUT9DATA	= 0x7F61
                    7F62    110 	OUT10DATA	= 0x7F62
                    7F63    111 	OUT11DATA	= 0x7F63
                    7F64    112 	OUT12DATA	= 0x7F64
                    7F65    113 	OUT13DATA	= 0x7F65
                    7F66    114 	OUT14DATA	= 0x7F66
                    7F67    115 	OUT15DATA	= 0x7F67
                            116 
                    7F68    117 	IN8DATA		= 0x7F68
                    7F69    118 	IN9DATA		= 0x7F69
                    7F6A    119 	IN10DATA	= 0x7F6A
                    7F6B    120 	IN11DATA	= 0x7F6B
                    7F6C    121 	IN12DATA	= 0x7F6C
                    7F6D    122 	IN13DATA	= 0x7F6D
                    7F6E    123 	IN14DATA	= 0x7F6E
                    7F6F    124 	IN15DATA	= 0x7F6F
                            125 
                    7F70    126 	OUT8BCH		= 0x7F70
                    7F71    127 	OUT8BCL		= 0x7F71
                    7F72    128 	OUT9BCH		= 0x7F72
                    7F73    129 	OUT9BCL		= 0x7F73
                    7F74    130 	OUT10BCH	= 0x7F74
                    7F75    131 	OUT10BCL	= 0x7F75
                    7F76    132 	OUT11BCH	= 0x7F76
                    7F77    133 	OUT11BCL	= 0x7F77
                    7F78    134 	OUT12BCH	= 0x7F78
                    7F79    135 	OUT12BCL	= 0x7F79
                    7F7A    136 	OUT13BCH	= 0x7F7A
                    7F7B    137 	OUT13BCL	= 0x7F7B
                    7F7C    138 	OUT14BCH	= 0x7F7C
                    7F7D    139 	OUT14BCL	= 0x7F7D
                    7F7E    140 	OUT15BCH	= 0x7F7E
                    7F7F    141 	OUT15BCL	= 0x7F7F
                            142 
                    7FF0    143 	OUT8ADDR	= 0x7FF0
                    7FF1    144 	OUT9ADDR	= 0x7FF1
                    7FF2    145 	OUT10ADDR	= 0x7FF2
                    7FF3    146 	OUT11ADDR	= 0x7FF3
                    7FF4    147 	OUT12ADDR	= 0x7FF4
                    7FF5    148 	OUT13ADDR	= 0x7FF5
                    7FF6    149 	OUT14ADDR	= 0x7FF6
                    7FF7    150 	OUT15ADDR	= 0x7FF7
                    7FF8    151 	IN8ADDR		= 0x7FF8
                    7FF9    152 	IN9ADDR		= 0x7FF9
                    7FFA    153 	IN10ADDR	= 0x7FFA
                    7FFB    154 	IN11ADDR	= 0x7FFB
                    7FFC    155 	IN12ADDR	= 0x7FFC
                    7FFD    156 	IN13ADDR	= 0x7FFD
                    7FFE    157 	IN14ADDR	= 0x7FFE
                    7FFF    158 	IN15ADDR	= 0x7FFF
                            159 
                    7FA0    160 	ISOERR		= 0x7FA0
                    7FA1    161 	ISOCTL		= 0x7FA1
                    7FA2    162 	ZBCOUNT		= 0x7FA2
                    7FE0    163 	INISOVAL	= 0x7FE0
                    7FE1    164 	OUTISOVAL	= 0x7FE1
                    7FE2    165 	FASTXFR		= 0x7FE2
                            166 
                            167 	;; CPU control registers
                            168 
                    7F92    169 	CPUCS		= 0x7F92
                            170 
                            171 	;; IO port control registers
                            172 
                    7F93    173 	PORTACFG	= 0x7F93
                    7F94    174 	PORTBCFG	= 0x7F94
                    7F95    175 	PORTCCFG	= 0x7F95
                    7F96    176 	OUTA		= 0x7F96
                    7F97    177 	OUTB		= 0x7F97
                    7F98    178 	OUTC		= 0x7F98
                    7F99    179 	PINSA		= 0x7F99
                    7F9A    180 	PINSB		= 0x7F9A
                    7F9B    181 	PINSC		= 0x7F9B
                    7F9C    182 	OEA		= 0x7F9C
                    7F9D    183 	OEB		= 0x7F9D
                    7F9E    184 	OEC		= 0x7F9E
                            185 
                            186 	;; I2C controller registers
                            187 
                    7FA5    188 	I2CS		= 0x7FA5
                    7FA6    189 	I2DAT		= 0x7FA6
                            190 
                            191 	;; FPGA defines
                    0003    192 	XC4K_IRLENGTH	= 3
                    0000    193 	XC4K_EXTEST	= 0
                    0001    194 	XC4K_PRELOAD	= 1
                    0005    195 	XC4K_CONFIGURE	= 5
                    0007    196 	XC4K_BYPASS	= 7
                            197 
                    2E64    198 	FPGA_CONFIGSIZE = 11876
                    0158    199 	FPGA_BOUND	= 344
                            200 
                    0000    201 	SOFTWARECONFIG	= 0
                            202 
                            203 	;; -----------------------------------------------------
                            204 
                            205 	.area CODE (CODE)
   0000 02 0B DF            206 	ljmp	startup
   0003 02 02 36            207 	ljmp	int0_isr
   0006                     208 	.ds	5
   000B 02 02 57            209 	ljmp	timer0_isr
   000E                     210 	.ds	5
   0013 02 02 87            211 	ljmp	int1_isr
   0016                     212 	.ds	5
   001B 02 02 A8            213 	ljmp	timer1_isr
   001E                     214 	.ds	5
   0023 02 02 C9            215 	ljmp	ser0_isr
   0026                     216 	.ds	5
   002B 02 02 EC            217 	ljmp	timer2_isr
   002E                     218 	.ds	5
   0033 02 03 0D            219 	ljmp	resume_isr
   0036                     220 	.ds	5
   003B 02 03 2E            221 	ljmp	ser1_isr
   003E                     222 	.ds	5
   0043 02 01 00            223 	ljmp	usb_isr
   0046                     224 	.ds	5
   004B 02 03 51            225 	ljmp	i2c_isr
   004E                     226 	.ds	5
   0053 02 03 76            227 	ljmp	int4_isr
   0056                     228 	.ds	5
   005B 02 03 9B            229 	ljmp	int5_isr
   005E                     230 	.ds	5
   0063 02 03 C0            231 	ljmp	int6_isr
                            232 
   0066                     233 	.ds	0x9a
                            234 	;; USB interrupt dispatch table
   0100                     235 usb_isr:
   0100 02 04 29            236 	ljmp	usb_sudav_isr
   0103                     237 	.ds	1
   0104 02 07 11            238 	ljmp	usb_sof_isr
   0107                     239 	.ds	1
   0108 02 07 3C            240 	ljmp	usb_sutok_isr
   010B                     241 	.ds	1
   010C 02 07 67            242 	ljmp	usb_suspend_isr
   010F                     243 	.ds	1
   0110 02 07 92            244 	ljmp	usb_usbreset_isr
   0113                     245 	.ds	1
   0114 32                  246 	reti
   0115                     247 	.ds	3
   0118 02 07 BD            248 	ljmp	usb_ep0in_isr
   011B                     249 	.ds	1
   011C 02 08 13            250 	ljmp	usb_ep0out_isr
   011F                     251 	.ds	1
   0120 02 08 CF            252 	ljmp	usb_ep1in_isr
   0123                     253 	.ds	1
   0124 02 08 FD            254 	ljmp	usb_ep1out_isr
   0127                     255 	.ds	1
   0128 02 09 28            256 	ljmp	usb_ep2in_isr
   012B                     257 	.ds	1
   012C 02 09 53            258 	ljmp	usb_ep2out_isr
   012F                     259 	.ds	1
   0130 02 09 7E            260 	ljmp	usb_ep3in_isr
   0133                     261 	.ds	1
   0134 02 09 A9            262 	ljmp	usb_ep3out_isr
   0137                     263 	.ds	1
   0138 02 09 D4            264 	ljmp	usb_ep4in_isr
   013B                     265 	.ds	1
   013C 02 09 FF            266 	ljmp	usb_ep4out_isr
   013F                     267 	.ds	1
   0140 02 0A 2A            268 	ljmp	usb_ep5in_isr
   0143                     269 	.ds	1
   0144 02 0A 55            270 	ljmp	usb_ep5out_isr
   0147                     271 	.ds	1
   0148 02 0A 80            272 	ljmp	usb_ep6in_isr
   014B                     273 	.ds	1
   014C 02 0A AB            274 	ljmp	usb_ep6out_isr
   014F                     275 	.ds	1
   0150 02 0A D6            276 	ljmp	usb_ep7in_isr
   0153                     277 	.ds	1
   0154 02 0B 01            278 	ljmp	usb_ep7out_isr
                            279 
                            280 	;; -----------------------------------------------------
                            281 
                            282 	.area   BSEG (BIT)
                            283 
                            284 	.area	ISEG (DATA)
   0080                     285 stack:		.ds	0x80
                            286 
                            287 	.area	DSEG (DATA)
   0040                     288 errcode:	.ds	1
   0041                     289 errval:		.ds	1
   0042                     290 cfgcount:	.ds	2
   0044                     291 leddiv:		.ds	1
   0045                     292 irqcount:	.ds	1
   0046                     293 ctrlcode:	.ds	1
   0047                     294 ctrladdr:	.ds	2
   0049                     295 ctrllen:	.ds	2
                            296 
                            297 	;; USB state
   004B                     298 numconfig:	.ds	1
   004C                     299 altsetting:	.ds	1
                            300 	
                            301 	.area	XSEG (DATA)
                            302 
                            303 
                            304 	.area	GSINIT (CODE)
   0BDF                     305 startup:
   0BDF 75 81 80            306 	mov	sp,#stack	; -1
   0BE2 E4                  307 	clr	a
   0BE3 F5 D0               308 	mov	psw,a
   0BE5 F5 86               309 	mov	dps,a
                            310 	;lcall	__sdcc_external_startup
                            311 	;mov	a,dpl0
                            312 	;jz	__sdcc_init_data
                            313 	;ljmp	__sdcc_program_startup
   0BE7                     314 __sdcc_init_data:
                            315 
                            316 	.area	GSINIT2 (CODE)
   0BE7                     317 __sdcc_program_startup:
                            318 	;; assembler code startup
   0BE7 E4                  319 	clr	a
   0BE8 F5 40               320 	mov	errcode,a
   0BEA F5 41               321 	mov	errval,a
   0BEC F5 42               322 	mov	cfgcount,a
   0BEE F5 43               323 	mov	cfgcount+1,a
   0BF0 F5 45               324  	mov	irqcount,a
   0BF2 F5 46               325  	mov	ctrlcode,a
   0BF4 F5 86               326 	mov	dps,a
   0BF6 F5 A8               327 	mov	ie,a
                            328 	;; some indirect register setup
   0BF8 75 8E 31            329 	mov	ckcon,#0x31	; one external wait state, to avoid chip bugs
                            330 	;; Timer setup:
                            331 	;; timer 0: LED blink    : T0M=0, CT0=0, MODE0=1
                            332 	;; timer 1: Baudrate gen : T1M=1, CT1=0, MODE1=2
   0BFB 75 89 21            333 	mov	tmod,#0x21
   0BFE 75 88 55            334 	mov	tcon,#0x55	; INT0/INT1 edge
   0C01 75 8D 64            335 	mov	th1,#256-156	; 1200 bauds
   0C04 75 87 00            336 	mov	pcon,#0		; SMOD0=0
                            337 	;; give Windows a chance to finish the writecpucs control transfer
                            338 	;; 10ms delay loop
   0C07 90 E8 90            339 	mov	dptr,#(-6000)&0xffff
   0C0A A3                  340 9$:	inc	dptr		; 3 cycles
   0C0B E5 82               341 	mov	a,dpl0		; 2 cycles
   0C0D 45 83               342 	orl	a,dph0		; 2 cycles
   0C0F 70 F9               343 	jnz	9$		; 3 cycles
                            344 	;; init USB subsystem
   0C11 90 7F A1            345 	mov	dptr,#ISOCTL
   0C14 74 01               346 	mov	a,#1		; disable ISO endpoints
   0C16 F0                  347 	movx	@dptr,a
   0C17 90 7F AF            348 	mov	dptr,#USBBAV
   0C1A 74 01               349 	mov	a,#1		; enable autovector, disable breakpoint logic
   0C1C F0                  350 	movx	@dptr,a
   0C1D E4                  351 	clr	a
   0C1E 90 7F E0            352 	mov	dptr,#INISOVAL
   0C21 F0                  353 	movx	@dptr,a
   0C22 90 7F E1            354 	mov	dptr,#OUTISOVAL
   0C25 F0                  355 	movx	@dptr,a
   0C26 90 7F DD            356 	mov	dptr,#USBPAIR
   0C29 74 09               357 	mov	a,#0x9		; pair EP 2&3 for input & output
   0C2B F0                  358 	movx	@dptr,a
   0C2C 90 7F DE            359 	mov	dptr,#IN07VAL
   0C2F 74 07               360 	mov	a,#0x7		; enable EP0+EP1+EP2
   0C31 F0                  361 	movx	@dptr,a
   0C32 90 7F DF            362 	mov	dptr,#OUT07VAL
   0C35 74 05               363 	mov	a,#0x5		; enable EP0+EP2
   0C37 F0                  364 	movx	@dptr,a
                            365 	;; USB:	init endpoint toggles
   0C38 90 7F D7            366 	mov	dptr,#TOGCTL
   0C3B 74 12               367 	mov	a,#0x12
   0C3D F0                  368 	movx	@dptr,a
   0C3E 74 32               369 	mov	a,#0x32		; clear EP 2 in toggle
   0C40 F0                  370 	movx	@dptr,a
   0C41 74 02               371 	mov	a,#0x02
   0C43 F0                  372 	movx	@dptr,a
   0C44 74 22               373 	mov	a,#0x22		; clear EP 2 out toggle
   0C46 F0                  374 	movx	@dptr,a
                            375 	;; configure IO ports
   0C47 90 7F 93            376 	mov	dptr,#PORTACFG
   0C4A 74 00               377 	mov	a,#0
   0C4C F0                  378 	movx	@dptr,a
   0C4D 90 7F 96            379 	mov	dptr,#OUTA
   0C50 74 80               380 	mov	a,#0x80		; set PROG lo
   0C52 F0                  381 	movx	@dptr,a
   0C53 90 7F 9C            382 	mov	dptr,#OEA
   0C56 74 C2               383 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   0C58 F0                  384 	movx	@dptr,a
   0C59 90 7F 94            385 	mov	dptr,#PORTBCFG
   0C5C 74 00               386 	mov	a,#0
   0C5E F0                  387 	movx	@dptr,a
   0C5F 90 7F 9D            388 	mov	dptr,#OEB
   0C62 74 00               389 	mov	a,#0
   0C64 F0                  390 	movx	@dptr,a
   0C65 90 7F 95            391 	mov	dptr,#PORTCCFG
   0C68 74 00               392 	mov	a,#0
   0C6A F0                  393 	movx	@dptr,a
   0C6B 90 7F 98            394 	mov	dptr,#OUTC
   0C6E 74 20               395 	mov	a,#0x20
   0C70 F0                  396 	movx	@dptr,a
   0C71 90 7F 9E            397 	mov	dptr,#OEC
   0C74 74 2E               398 	mov	a,#0x2e		; out: LEDCON,LEDSTA,TCK,INIT  in: TDO
   0C76 F0                  399 	movx	@dptr,a
                            400 	;; enable interrupts
   0C77 75 A8 82            401 	mov	ie,#0x82	; enable timer 0
   0C7A 75 E8 01            402 	mov	eie,#0x01	; enable USB interrupts
   0C7D 90 7F AE            403 	mov	dptr,#USBIEN
   0C80 74 01               404 	mov	a,#1		; enable SUDAV interrupt
   0C82 F0                  405 	movx	@dptr,a
   0C83 90 7F AC            406 	mov	dptr,#IN07IEN
   0C86 74 03               407 	mov	a,#3		; enable EP0+EP1 interrupt
   0C88 F0                  408 	movx	@dptr,a
   0C89 90 7F AD            409 	mov	dptr,#OUT07IEN
   0C8C 74 01               410 	mov	a,#1		; enable EP0 interrupt
   0C8E F0                  411 	movx	@dptr,a
                            412 	;; initialize UART 0 for config loading
   0C8F 75 98 20            413 	mov	scon0,#0x20	; mode 0, CLK24/4
   0C92 75 99 FF            414 	mov	sbuf0,#0xff
                            415 	;; initialize USB state
   0C95 E4                  416 	clr	a
   0C96 F5 4B               417 	mov	numconfig,a
   0C98 F5 4C               418 	mov	altsetting,a
                    0001    419 	.if	1
                            420 	;; disconnect from USB bus
   0C9A 90 7F D6            421 	mov	dptr,#USBCS
   0C9D 74 0A               422 	mov	a,#10
   0C9F F0                  423 	movx	@dptr,a
                            424 	;; wait 0.3 sec
   0CA0 7A 1E               425 	mov	r2,#30
                            426 	;; 10ms delay loop
   0CA2 90 E8 90            427 0$:	mov	dptr,#(-6000)&0xffff
   0CA5 A3                  428 1$:	inc	dptr            ; 3 cycles
   0CA6 E5 82               429 	mov	a,dpl0          ; 2 cycles
   0CA8 45 83               430 	orl	a,dph0          ; 2 cycles
   0CAA 70 F9               431 	jnz	1$              ; 3 cycles
   0CAC DA F4               432 	djnz	r2,0$
                            433 	;; reconnect to USB bus
   0CAE 90 7F D6            434 	mov	dptr,#USBCS
   0CB1 74 02               435 	mov	a,#2		; 8051 handles control
   0CB3 F0                  436 	movx	@dptr,a
   0CB4 74 06               437 	mov	a,#6		; reconnect, 8051 handles control
   0CB6 F0                  438 	movx	@dptr,a
                            439 	.endif
                            440 
                            441 	;; final
   0CB7 12 08 B0            442 	lcall	fillusbintr
                            443 	;; kludge; first OUT2 packet seems to be bogus
                            444 	;; wait for packet with length 1 and contents 0x55
                            445 
                            446 	;; some short delay
   0CBA 90 7F 96            447 	mov	dptr,#OUTA
   0CBD 74 82               448 	mov	a,#0x82		; set PROG hi
   0CBF F0                  449 	movx	@dptr,a
   0CC0 90 7F 99            450 	mov	dptr,#PINSA
   0CC3 E0                  451 	movx	a,@dptr
   0CC4 30 E2 06            452 	jnb	acc.2,8$
                            453 	;; DONE stuck high
   0CC7 75 40 80            454 	mov	errcode,#0x80
   0CCA 02 0D F6            455 	ljmp	fpgaerr
   0CCD 12 01 57            456 8$:	lcall	jtag_reset_tap
   0CD0 7A 05               457 	mov	r2,#5
   0CD2 7B 00               458 	mov	r3,#0
   0CD4 7C 06               459 	mov	r4,#6
   0CD6 12 01 5D            460 	lcall	jtag_shiftout	; enter SHIFT-IR state
   0CD9 7A 08               461 	mov	r2,#8
   0CDB 7B 00               462 	mov	r3,#0
   0CDD 7C 00               463 	mov	r4,#0
   0CDF 12 01 5D            464 	lcall	jtag_shiftout	; assume max. 8bit IR
   0CE2 7A 08               465 	mov	r2,#8
   0CE4 7B 01               466 	mov	r3,#1
   0CE6 7C 00               467 	mov	r4,#0
   0CE8 12 01 5D            468 	lcall	jtag_shift	; shift in a single bit
   0CEB 74 F8               469 	mov	a,#-(1<<XC4K_IRLENGTH)
   0CED 2F                  470 	add	a,r7
   0CEE 60 08               471 	jz	2$
                            472 	;; JTAG instruction register error
   0CF0 75 40 81            473 	mov	errcode,#0x81
   0CF3 8F 41               474 	mov	errval,r7
   0CF5 02 0D F6            475 	ljmp	fpgaerr
   0CF8 7A 05               476 2$:	mov	r2,#XC4K_IRLENGTH+2
   0CFA 7B 05               477 	mov	r3,#XC4K_CONFIGURE
   0CFC 7C 0C               478 	mov	r4,#3<<(XC4K_IRLENGTH-1)
   0CFE 12 01 5D            479 	lcall	jtag_shiftout	; shift in CONFIGURE insn, goto RUNTEST/IDLE state
   0D01 90 7F 9E            480 	mov	dptr,#OEC
   0D04 74 2A               481 	mov	a,#0x2a		; out: LEDCON,LEDSTA,TCK  in: TDO,INIT
   0D06 F0                  482 	movx	@dptr,a
   0D07 75 40 01            483 	mov	errcode,#1
   0D0A 90 7F 9B            484 	mov	dptr,#PINSC
   0D0D E0                  485 3$:	movx	a,@dptr
   0D0E 30 E2 FC            486 	jnb	acc.2,3$
   0D11 7A 03               487 	mov	r2,#3
   0D13 7B FF               488 	mov	r3,#0xff
   0D15 7C 01               489 	mov	r4,#1
   0D17 12 01 5D            490 	lcall	jtag_shiftout	; advance to SHIFT-DR state
                            491 	;; software configuration: enable UART special func pins
   0D1A 90 7F 96            492 	mov	dptr,#OUTA
   0D1D 74 42               493 	mov	a,#0x42		; PROG hi, TMS lo, TDI hi
   0D1F F0                  494 	movx	@dptr,a
                    0000    495 	.if	SOFTWARECONFIG
                            496 	.else
   0D20 90 7F 93            497 	mov	dptr,#PORTACFG
   0D23 74 40               498 	mov	a,#0x40		; TDI is special function pin
   0D25 F0                  499 	movx	@dptr,a
   0D26 90 7F 95            500 	mov	dptr,#PORTCCFG
   0D29 74 02               501 	mov	a,#0x02		; TCK is special function pin
   0D2B F0                  502 	movx	@dptr,a
                            503 	.endif
                            504 	;; start
   0D2C 75 40 10            505 	mov	errcode,#0x10
                            506 
   0D2F 90 7F C8            507 	mov	dptr,#OUT2CS
   0D32 E4                  508 	clr	a
   0D33 F0                  509 	movx	@dptr,a
   0D34 90 7F C9            510 	mov	dptr,#OUT2BC
   0D37 F0                  511 	movx	@dptr,a		; arm EP2 OUT
                            512 	
                            513 	
   0D38                     514 ldstloop:
   0D38 90 7F C9            515 	mov	dptr,#OUT2BC
   0D3B F0                  516 	movx	@dptr,a		; arm EP2 OUT
   0D3C 90 7F C8            517 0$:	mov	dptr,#OUT2CS
   0D3F E0                  518 	movx	a,@dptr
   0D40 20 E1 F9            519 	jb	acc.1,0$
   0D43 90 7F C9            520 	mov	dptr,#OUT2BC
   0D46 E0                  521 	movx	a,@dptr
   0D47 24 FE               522 	add	a,#-2
   0D49 50 ED               523 	jnc	ldstloop
   0D4B 90 7D C0            524 	mov	dptr,#OUT2BUF
   0D4E E0                  525 	movx	a,@dptr
   0D4F B4 FF E6            526 	cjne	a,#0xff,ldstloop
   0D52 A3                  527 	inc	dptr
   0D53 E0                  528 	movx	a,@dptr
   0D54 B4 04 E1            529 	cjne	a,#0x04,ldstloop
   0D57 75 40 11            530 	mov	errcode,#0x11
   0D5A 80 0B               531 	sjmp	xxloadloop
                            532 
   0D5C                     533 loadloop:
   0D5C 90 7F C9            534 	mov	dptr,#OUT2BC
   0D5F F0                  535 	movx	@dptr,a		; arm EP2 OUT
   0D60 90 7F C8            536 0$:	mov	dptr,#OUT2CS
   0D63 E0                  537 	movx	a,@dptr
   0D64 20 E1 F9            538 	jb	acc.1,0$
   0D67                     539 xxloadloop:
   0D67 90 7F C9            540 	mov	dptr,#OUT2BC
   0D6A E0                  541 	movx	a,@dptr
   0D6B 60 EF               542 	jz	loadloop
   0D6D FF                  543 	mov	r7,a
   0D6E 25 42               544 	add	a,cfgcount
   0D70 F5 42               545 	mov	cfgcount,a
   0D72 E4                  546 	clr	a
   0D73 35 43               547 	addc	a,cfgcount+1
   0D75 F5 43               548 	mov	cfgcount+1,a
   0D77 90 7D C0            549 	mov	dptr,#OUT2BUF
                    0000    550 	.if	SOFTWARECONFIG
                            551 1$:	movx	a,@dptr
                            552 	mov	r3,a
                            553 	mov	r2,#8
                            554 	inc	dps
                            555 2$:	mov	a,#2
                            556 	xch	a,r3
                            557 	rrc	a
                            558 	xch	a,r3
                            559 	mov	acc.6,c
                            560 	mov	dptr,#OUTA
                            561 	movx	@dptr,a
                            562 	mov	dptr,#OUTC
                            563 	movx	a,@dptr
                            564 	setb	acc.1
                            565 	movx	@dptr,a
                            566 	clr	acc.1
                            567 	movx	@dptr,a
                            568 	djnz	r2,2$
                            569 	dec	dps
                            570 	inc	dptr
                            571 	djnz	r7,1$
                            572 	.else
   0D7A E0                  573 	movx	a,@dptr
   0D7B 80 04               574 	sjmp	3$
   0D7D E0                  575 1$:	movx	a,@dptr
   0D7E 30 99 FD            576 2$:	jnb	scon0+1,2$
   0D81 C2 99               577 3$:	clr	scon0+1
   0D83 F5 99               578 	mov	sbuf0,a
   0D85 A3                  579 	inc	dptr
   0D86 DF F5               580 	djnz	r7,1$
                            581 	.endif
                            582 	;; check if init went low - indicates configuration error
   0D88 90 7F 9B            583 	mov	dptr,#PINSC
   0D8B E0                  584 	movx	a,@dptr
   0D8C 20 E2 0C            585 	jb	acc.2,5$
                            586 	;; INIT low
   0D8F 90 7F C8            587 	mov	dptr,#OUT2CS
   0D92 74 01               588 	mov	a,#0x01
   0D94 F0                  589 	movx	@dptr,a
   0D95 75 40 90            590 	mov	errcode,#0x90
   0D98 02 0D F6            591 	ljmp	fpgaerr
   0D9B                     592 5$:	;; check for end of FPGA configuration data
   0D9B 74 9C               593 	mov	a,#<(-FPGA_CONFIGSIZE)
   0D9D 25 42               594 	add	a,cfgcount
   0D9F 74 D1               595 	mov	a,#>(-FPGA_CONFIGSIZE)
   0DA1 35 43               596 	addc	a,cfgcount+1
   0DA3 50 B7               597 	jnc	loadloop
                            598 	;; configuration download complete, start FPGA
   0DA5 90 7F 94            599 	mov	dptr,#PORTBCFG
   0DA8 74 00               600 	mov	a,#0		; TDI no longer special function pin
   0DAA F0                  601 	movx	@dptr,a
   0DAB 90 7F 95            602 	mov	dptr,#PORTCCFG
   0DAE 74 00               603 	mov	a,#0		; TCK no longer special function pin
   0DB0 F0                  604 	movx	@dptr,a
                            605 	;; wait for done loop
   0DB1 7F FA               606 	mov	r7,#250
   0DB3                     607 doneloop:
   0DB3 90 7F 99            608 	mov	dptr,#PINSA
   0DB6 E0                  609 	movx	a,@dptr
   0DB7 20 E2 1F            610 	jb	acc.2,doneactive
   0DBA 90 7F 9B            611 	mov	dptr,#PINSC
   0DBD E0                  612 	movx	a,@dptr
   0DBE 20 E2 06            613 	jb	acc.2,1$
                            614 	;; INIT low
   0DC1 75 40 90            615 	mov	errcode,#0x90
   0DC4 02 0D F6            616 	ljmp	fpgaerr
   0DC7 90 7F 98            617 1$:	mov	dptr,#OUTC
   0DCA E0                  618 	movx	a,@dptr
   0DCB D2 E1               619 	setb	acc.1
   0DCD F0                  620 	movx	@dptr,a
   0DCE C2 E1               621 	clr	acc.1
   0DD0 F0                  622 	movx	@dptr,a
   0DD1 DF E0               623 	djnz	r7,doneloop
                            624 	;; DONE stuck low
   0DD3 75 40 91            625 	mov	errcode,#0x91
   0DD6 02 0D F6            626 	ljmp	fpgaerr
                            627 	
   0DD9                     628 doneactive:
                            629 	;; generate 16 clock pulses to start up FPGA
   0DD9 7F 10               630 	mov	r7,#16
   0DDB 90 7F 98            631 	mov	dptr,#OUTC
   0DDE E0                  632 	movx	a,@dptr
   0DDF D2 E1               633 0$:	setb	acc.1
   0DE1 F0                  634 	movx	@dptr,a
   0DE2 C2 E1               635 	clr	acc.1
   0DE4 F0                  636 	movx	@dptr,a
   0DE5 DF F8               637 	djnz	r7,0$
   0DE7 90 7F 95            638 	mov	dptr,#PORTCCFG
   0DEA 74 C0               639 	mov	a,#0xc0		; RD/WR is special function pin
   0DEC F0                  640 	movx	@dptr,a
   0DED 75 40 20            641 	mov	errcode,#0x20
                            642 
                            643 	;; turn off PTT
   0DF0 90 C0 08            644 	mov	dptr,#0xc008
   0DF3 74 42               645 	mov	a,#0x42
   0DF5 F0                  646 	movx	@dptr,a
                            647 	
   0DF6                     648 fpgaerr:
   0DF6 80 FE               649 	sjmp	fpgaerr
                            650 
                            651 	.area	CSEG (CODE)
                    0002    652 	ar2 = 0x02
                    0003    653 	ar3 = 0x03
                    0004    654 	ar4 = 0x04
                    0005    655 	ar5 = 0x05
                    0006    656 	ar6 = 0x06
                    0007    657 	ar7 = 0x07
                    0000    658 	ar0 = 0x00
                    0001    659 	ar1 = 0x01
                            660 
                            661 	;; jtag_shift
                            662 	;; r2 = num
                            663 	;; r3 = tdi
                            664 	;; r4 = tms
                            665 	;; return: r7 = tdo
   0157                     666 jtag_reset_tap:
   0157 7A 05               667 	mov	r2,#5
   0159 7B 00               668 	mov	r3,#0
   015B 7C FF               669 	mov	r4,#0xff
   015D                     670 jtag_shiftout:
   015D                     671 jtag_shift:
   015D 7E 01               672 	mov	r6,#1
   015F 7F 00               673 	mov	r7,#0
   0161 74 02               674 1$:	mov	a,#2
   0163 CB                  675 	xch	a,r3
   0164 13                  676 	rrc	a
   0165 CB                  677 	xch	a,r3
   0166 92 E6               678 	mov	acc.6,c
   0168 CC                  679 	xch	a,r4
   0169 13                  680 	rrc	a
   016A CC                  681 	xch	a,r4
   016B 92 E7               682 	mov	acc.7,c
   016D 90 7F 96            683 	mov	dptr,#OUTA
   0170 F0                  684 	movx	@dptr,a
   0171 90 7F 9B            685 	mov	dptr,#PINSC
   0174 E0                  686 	movx	a,@dptr
   0175 30 E0 03            687 	jnb	acc.0,2$
   0178 EE                  688 	mov	a,r6
   0179 42 07               689 	orl	ar7,a
   017B 90 7F 98            690 2$:	mov	dptr,#OUTC
   017E E0                  691 	movx	a,@dptr
   017F D2 E1               692 	setb	acc.1
   0181 F0                  693 	movx	@dptr,a
   0182 C2 E1               694 	clr	acc.1
   0184 F0                  695 	movx	@dptr,a
   0185 EE                  696 	mov	a,r6
   0186 23                  697 	rl	a
   0187 FE                  698 	mov	r6,a
   0188 DA D7               699 	djnz	r2,1$
   018A 22                  700 	ret
                            701 
                            702 	;; EEPROM address where the serial number is located
                            703 	
   018B                     704 eepromstraddr:
   018B 10                  705 	.db	0x10
                            706 
                            707 	;; I2C Routines
                            708 	;; note: ckcon should be set to #0x31 to avoid chip bugs
                            709 
   018C                     710 writei2c:
                            711 	;; dptr: data to be sent
                            712 	;; b:    device address
                            713 	;; r7:   transfer length
                            714 	;; return: a: status (bytes remaining, 0xff bus error)
   018C 05 86               715 	inc	dps
   018E 90 7F A5            716 	mov	dptr,#I2CS
   0191 C2 F0               717 	clr	b.0		; r/w = 0
   0193 74 80               718 	mov	a,#0x80		; start condition
   0195 F0                  719 	movx	@dptr,a
   0196 90 7F A6            720 	mov	dptr,#I2DAT
   0199 E5 F0               721 	mov	a,b
   019B F0                  722 	movx	@dptr,a
   019C 90 7F A5            723 	mov	dptr,#I2CS	
   019F E0                  724 0$:	movx	a,@dptr
   01A0 20 E2 20            725 	jb	acc.2,3$	; BERR
   01A3 30 E0 F9            726 	jnb	acc.0,0$	; DONE
   01A6 30 E1 0F            727 	jnb	acc.1,1$	; ACK
   01A9 90 7F A6            728 	mov	dptr,#I2DAT
   01AC 15 86               729 	dec	dps
   01AE E0                  730 	movx	a,@dptr
   01AF A3                  731 	inc	dptr
   01B0 05 86               732 	inc	dps
   01B2 F0                  733 	movx	@dptr,a
   01B3 90 7F A5            734 	mov	dptr,#I2CS
   01B6 DF E7               735 	djnz	r7,0$
   01B8 74 40               736 1$:	mov	a,#0x40		; stop condition
   01BA F0                  737 	movx	@dptr,a
   01BB E0                  738 2$:	movx	a,@dptr
   01BC 20 E6 FC            739 	jb	acc.6,2$
   01BF 15 86               740 	dec	dps
   01C1 EF                  741 	mov	a,r7
   01C2 22                  742 	ret
   01C3 7F FF               743 3$:	mov	r7,#255
   01C5 80 F1               744 	sjmp	1$
                            745 
   01C7                     746 readi2c:
                            747 	;; dptr: data to be sent
                            748 	;; b:    device address
                            749 	;; r7:   transfer length
                            750 	;; return: a: status (bytes remaining, 0xff bus error)
   01C7 05 86               751 	inc	dps
   01C9 90 7F A5            752 	mov	dptr,#I2CS
   01CC D2 F0               753 	setb	b.0		; r/w = 1
   01CE 74 80               754 	mov	a,#0x80		; start condition
   01D0 F0                  755 	movx	@dptr,a
   01D1 90 7F A6            756 	mov	dptr,#I2DAT
   01D4 E5 F0               757 	mov	a,b
   01D6 F0                  758 	movx	@dptr,a
   01D7 90 7F A5            759 	mov	dptr,#I2CS
   01DA E0                  760 0$:	movx	a,@dptr
   01DB 20 E2 4C            761 	jb	acc.2,9$	; BERR
   01DE 30 E0 F9            762 	jnb	acc.0,0$	; DONE
   01E1 30 E1 03            763 	jnb	acc.1,5$	; ACK
   01E4 EF                  764 	mov	a,r7
   01E5 70 0B               765 	jnz	1$
   01E7 74 40               766 5$:	mov	a,#0x40		; stop condition
   01E9 F0                  767 	movx	@dptr,a
   01EA E0                  768 2$:	movx	a,@dptr
   01EB 20 E6 FC            769 	jb	acc.6,2$
   01EE 15 86               770 	dec	dps
   01F0 E4                  771 	clr	a
   01F1 22                  772 	ret
   01F2 B4 01 03            773 1$:	cjne	a,#1,3$
   01F5 74 20               774 	mov	a,#0x20		; LASTRD = 1
   01F7 F0                  775 	movx	@dptr,a
   01F8 90 7F A6            776 3$:	mov	dptr,#I2DAT
   01FB E0                  777 	movx	a,@dptr		; initiate first read
   01FC 90 7F A5            778 	mov	dptr,#I2CS
   01FF E0                  779 4$:	movx	a,@dptr
   0200 20 E2 27            780 	jb	acc.2,9$	; BERR
   0203 30 E0 F9            781 	jnb	acc.0,4$	; DONE
   0206 EF                  782 	mov	a,r7
   0207 B4 02 03            783 	cjne	a,#2,6$
   020A 74 20               784 	mov	a,#0x20		; LASTRD = 1
   020C F0                  785 	movx	@dptr,a
   020D B4 01 03            786 6$:	cjne	a,#1,7$
   0210 74 40               787 	mov	a,#0x40		; stop condition
   0212 F0                  788 	movx	@dptr,a
   0213 90 7F A6            789 7$:	mov	dptr,#I2DAT
   0216 E0                  790 	movx	a,@dptr		; read data
   0217 15 86               791 	dec	dps
   0219 F0                  792 	movx	@dptr,a
   021A A3                  793 	inc	dptr
   021B 05 86               794 	inc	dps
   021D 90 7F A5            795 	mov	dptr,#I2CS
   0220 DF DD               796 	djnz	r7,4$
   0222 E0                  797 8$:	movx	a,@dptr
   0223 20 E6 FC            798 	jb	acc.6,8$
   0226 15 86               799 	dec	dps
   0228 E4                  800 	clr	a
   0229 22                  801 	ret
   022A 74 40               802 9$:	mov	a,#0x40		; stop condition
   022C F0                  803 	movx	@dptr,a
   022D E0                  804 10$:	movx	a,@dptr
   022E 20 E6 FC            805 	jb	acc.6,10$
   0231 74 FF               806 	mov	a,#255
   0233 15 86               807 	dec	dps
   0235 22                  808 	ret
                            809 
                            810 	;; ------------------ interrupt handlers ------------------------
                            811 
   0236                     812 int0_isr:
   0236 C0 E0               813 	push	acc
   0238 C0 F0               814 	push	b
   023A C0 82               815 	push	dpl0
   023C C0 83               816 	push	dph0
   023E C0 D0               817 	push	psw
   0240 75 D0 00            818 	mov	psw,#0x00
   0243 C0 86               819 	push	dps
   0245 75 86 00            820 	mov	dps,#0
                            821 	;; clear interrupt
   0248 C2 89               822 	clr	tcon+1
                            823 	;; handle interrupt
                            824 	;; epilogue
   024A D0 86               825 	pop	dps
   024C D0 D0               826 	pop	psw
   024E D0 83               827 	pop	dph0
   0250 D0 82               828 	pop	dpl0
   0252 D0 F0               829 	pop	b
   0254 D0 E0               830 	pop	acc
   0256 32                  831 	reti
                            832 
   0257                     833 timer0_isr:
   0257 C0 E0               834 	push	acc
   0259 C0 F0               835 	push	b
   025B C0 82               836 	push	dpl0
   025D C0 83               837 	push	dph0
   025F C0 D0               838 	push	psw
   0261 75 D0 00            839 	mov	psw,#0x00
   0264 C0 86               840 	push	dps
   0266 75 86 00            841 	mov	dps,#0
                            842 	;; clear interrupt
   0269 C2 8D               843 	clr	tcon+5
                            844 	;; handle interrupt
   026B 05 44               845 	inc	leddiv
   026D E5 44               846 	mov	a,leddiv
   026F 54 07               847 	anl	a,#7
   0271 70 07               848 	jnz	0$
   0273 90 7F 98            849 	mov	dptr,#OUTC
   0276 E0                  850 	movx	a,@dptr
   0277 64 08               851 	xrl	a,#0x08
   0279 F0                  852 	movx	@dptr,a
   027A                     853 0$:
                            854 	;; epilogue
   027A D0 86               855 	pop	dps
   027C D0 D0               856 	pop	psw
   027E D0 83               857 	pop	dph0
   0280 D0 82               858 	pop	dpl0
   0282 D0 F0               859 	pop	b
   0284 D0 E0               860 	pop	acc
   0286 32                  861 	reti
                            862 
   0287                     863 int1_isr:
   0287 C0 E0               864 	push	acc
   0289 C0 F0               865 	push	b
   028B C0 82               866 	push	dpl0
   028D C0 83               867 	push	dph0
   028F C0 D0               868 	push	psw
   0291 75 D0 00            869 	mov	psw,#0x00
   0294 C0 86               870 	push	dps
   0296 75 86 00            871 	mov	dps,#0
                            872 	;; clear interrupt
   0299 C2 8B               873 	clr	tcon+3
                            874 	;; handle interrupt
                            875 	;; epilogue
   029B D0 86               876 	pop	dps
   029D D0 D0               877 	pop	psw
   029F D0 83               878 	pop	dph0
   02A1 D0 82               879 	pop	dpl0
   02A3 D0 F0               880 	pop	b
   02A5 D0 E0               881 	pop	acc
   02A7 32                  882 	reti
                            883 
   02A8                     884 timer1_isr:
   02A8 C0 E0               885 	push	acc
   02AA C0 F0               886 	push	b
   02AC C0 82               887 	push	dpl0
   02AE C0 83               888 	push	dph0
   02B0 C0 D0               889 	push	psw
   02B2 75 D0 00            890 	mov	psw,#0x00
   02B5 C0 86               891 	push	dps
   02B7 75 86 00            892 	mov	dps,#0
                            893 	;; clear interrupt
   02BA C2 8F               894 	clr	tcon+7
                            895 	;; handle interrupt
                            896 	;; epilogue
   02BC D0 86               897 	pop	dps
   02BE D0 D0               898 	pop	psw
   02C0 D0 83               899 	pop	dph0
   02C2 D0 82               900 	pop	dpl0
   02C4 D0 F0               901 	pop	b
   02C6 D0 E0               902 	pop	acc
   02C8 32                  903 	reti
                            904 
   02C9                     905 ser0_isr:
   02C9 C0 E0               906 	push	acc
   02CB C0 F0               907 	push	b
   02CD C0 82               908 	push	dpl0
   02CF C0 83               909 	push	dph0
   02D1 C0 D0               910 	push	psw
   02D3 75 D0 00            911 	mov	psw,#0x00
   02D6 C0 86               912 	push	dps
   02D8 75 86 00            913 	mov	dps,#0
                            914 	;; clear interrupt
   02DB C2 98               915 	clr	scon0+0
   02DD C2 99               916 	clr	scon0+1
                            917 	;; handle interrupt
                            918 	;; epilogue
   02DF D0 86               919 	pop	dps
   02E1 D0 D0               920 	pop	psw
   02E3 D0 83               921 	pop	dph0
   02E5 D0 82               922 	pop	dpl0
   02E7 D0 F0               923 	pop	b
   02E9 D0 E0               924 	pop	acc
   02EB 32                  925 	reti
                            926 
   02EC                     927 timer2_isr:
   02EC C0 E0               928 	push	acc
   02EE C0 F0               929 	push	b
   02F0 C0 82               930 	push	dpl0
   02F2 C0 83               931 	push	dph0
   02F4 C0 D0               932 	push	psw
   02F6 75 D0 00            933 	mov	psw,#0x00
   02F9 C0 86               934 	push	dps
   02FB 75 86 00            935 	mov	dps,#0
                            936 	;; clear interrupt
   02FE C2 CF               937 	clr	t2con+7
                            938 	;; handle interrupt
                            939 	;; epilogue
   0300 D0 86               940 	pop	dps
   0302 D0 D0               941 	pop	psw
   0304 D0 83               942 	pop	dph0
   0306 D0 82               943 	pop	dpl0
   0308 D0 F0               944 	pop	b
   030A D0 E0               945 	pop	acc
   030C 32                  946 	reti
                            947 
   030D                     948 resume_isr:
   030D C0 E0               949 	push	acc
   030F C0 F0               950 	push	b
   0311 C0 82               951 	push	dpl0
   0313 C0 83               952 	push	dph0
   0315 C0 D0               953 	push	psw
   0317 75 D0 00            954 	mov	psw,#0x00
   031A C0 86               955 	push	dps
   031C 75 86 00            956 	mov	dps,#0
                            957 	;; clear interrupt
   031F C2 DC               958 	clr	eicon+4
                            959 	;; handle interrupt
                            960 	;; epilogue
   0321 D0 86               961 	pop	dps
   0323 D0 D0               962 	pop	psw
   0325 D0 83               963 	pop	dph0
   0327 D0 82               964 	pop	dpl0
   0329 D0 F0               965 	pop	b
   032B D0 E0               966 	pop	acc
   032D 32                  967 	reti
                            968 
   032E                     969 ser1_isr:
   032E C0 E0               970 	push	acc
   0330 C0 F0               971 	push	b
   0332 C0 82               972 	push	dpl0
   0334 C0 83               973 	push	dph0
   0336 C0 D0               974 	push	psw
   0338 75 D0 00            975 	mov	psw,#0x00
   033B C0 86               976 	push	dps
   033D 75 86 00            977 	mov	dps,#0
                            978 	;; clear interrupt
   0340 C2 C0               979 	clr	scon1+0
   0342 C2 C1               980 	clr	scon1+1
                            981 	;; handle interrupt
                            982 	;; epilogue
   0344 D0 86               983 	pop	dps
   0346 D0 D0               984 	pop	psw
   0348 D0 83               985 	pop	dph0
   034A D0 82               986 	pop	dpl0
   034C D0 F0               987 	pop	b
   034E D0 E0               988 	pop	acc
   0350 32                  989 	reti
                            990 
   0351                     991 i2c_isr:
   0351 C0 E0               992 	push	acc
   0353 C0 F0               993 	push	b
   0355 C0 82               994 	push	dpl0
   0357 C0 83               995 	push	dph0
   0359 C0 D0               996 	push	psw
   035B 75 D0 00            997 	mov	psw,#0x00
   035E C0 86               998 	push	dps
   0360 75 86 00            999 	mov	dps,#0
                           1000 	;; clear interrupt
   0363 E5 91              1001 	mov	a,exif
   0365 C2 E5              1002 	clr	acc.5
   0367 F5 91              1003 	mov	exif,a
                           1004 	;; handle interrupt
                           1005 	;; epilogue
   0369 D0 86              1006 	pop	dps
   036B D0 D0              1007 	pop	psw
   036D D0 83              1008 	pop	dph0
   036F D0 82              1009 	pop	dpl0
   0371 D0 F0              1010 	pop	b
   0373 D0 E0              1011 	pop	acc
   0375 32                 1012 	reti
                           1013 
   0376                    1014 int4_isr:
   0376 C0 E0              1015 	push	acc
   0378 C0 F0              1016 	push	b
   037A C0 82              1017 	push	dpl0
   037C C0 83              1018 	push	dph0
   037E C0 D0              1019 	push	psw
   0380 75 D0 00           1020 	mov	psw,#0x00
   0383 C0 86              1021 	push	dps
   0385 75 86 00           1022 	mov	dps,#0
                           1023 	;; clear interrupt
   0388 E5 91              1024 	mov	a,exif
   038A C2 E6              1025 	clr	acc.6
   038C F5 91              1026 	mov	exif,a
                           1027 	;; handle interrupt
                           1028 	;; epilogue
   038E D0 86              1029 	pop	dps
   0390 D0 D0              1030 	pop	psw
   0392 D0 83              1031 	pop	dph0
   0394 D0 82              1032 	pop	dpl0
   0396 D0 F0              1033 	pop	b
   0398 D0 E0              1034 	pop	acc
   039A 32                 1035 	reti
                           1036 
   039B                    1037 int5_isr:
   039B C0 E0              1038 	push	acc
   039D C0 F0              1039 	push	b
   039F C0 82              1040 	push	dpl0
   03A1 C0 83              1041 	push	dph0
   03A3 C0 D0              1042 	push	psw
   03A5 75 D0 00           1043 	mov	psw,#0x00
   03A8 C0 86              1044 	push	dps
   03AA 75 86 00           1045 	mov	dps,#0
                           1046 	;; clear interrupt
   03AD E5 91              1047 	mov	a,exif
   03AF C2 E7              1048 	clr	acc.7
   03B1 F5 91              1049 	mov	exif,a
                           1050 	;; handle interrupt
                           1051 	;; epilogue
   03B3 D0 86              1052 	pop	dps
   03B5 D0 D0              1053 	pop	psw
   03B7 D0 83              1054 	pop	dph0
   03B9 D0 82              1055 	pop	dpl0
   03BB D0 F0              1056 	pop	b
   03BD D0 E0              1057 	pop	acc
   03BF 32                 1058 	reti
                           1059 
   03C0                    1060 int6_isr:
   03C0 C0 E0              1061 	push	acc
   03C2 C0 F0              1062 	push	b
   03C4 C0 82              1063 	push	dpl0
   03C6 C0 83              1064 	push	dph0
   03C8 C0 D0              1065 	push	psw
   03CA 75 D0 00           1066 	mov	psw,#0x00
   03CD C0 86              1067 	push	dps
   03CF 75 86 00           1068 	mov	dps,#0
                           1069 	;; clear interrupt
   03D2 C2 DB              1070 	clr	eicon+3
                           1071 	;; handle interrupt
                           1072 	;; epilogue
   03D4 D0 86              1073 	pop	dps
   03D6 D0 D0              1074 	pop	psw
   03D8 D0 83              1075 	pop	dph0
   03DA D0 82              1076 	pop	dpl0
   03DC D0 F0              1077 	pop	b
   03DE D0 E0              1078 	pop	acc
   03E0 32                 1079 	reti
                           1080 
   03E1                    1081 xmemread::
   03E1 E5 49              1082 	mov	a,ctrllen
   03E3 FF                 1083 	mov	r7,a
   03E4 24 C0              1084 	add	a,#-64
   03E6 F5 49              1085 	mov	ctrllen,a
   03E8 E5 4A              1086 	mov	a,ctrllen+1
   03EA 34 00              1087 	addc	a,#0
   03EC 50 12              1088 	jnc	0$
   03EE E4                 1089 	clr	a
   03EF F5 49              1090 	mov	ctrllen,a
   03F1 F5 4A              1091 	mov	ctrllen+1,a
   03F3 F5 46              1092 	mov	ctrlcode,a
   03F5 74 02              1093 	mov	a,#2		; ack control transfer
   03F7 90 7F B4           1094 	mov	dptr,#EP0CS
   03FA F0                 1095 	movx	@dptr,a
   03FB EF                 1096 	mov	a,r7
   03FC 60 25              1097 	jz	2$
   03FE 80 04              1098 	sjmp	1$
   0400 F5 4A              1099 0$:	mov	ctrllen+1,a
   0402 7F 40              1100 	mov	r7,#64
   0404 8F 00              1101 1$:	mov	ar0,r7
   0406 85 47 82           1102 	mov	dpl0,ctrladdr
   0409 85 48 83           1103 	mov	dph0,ctrladdr+1
   040C 05 86              1104 	inc	dps
   040E 90 7F 00           1105 	mov	dptr,#IN0BUF
   0411 15 86              1106 	dec	dps
   0413 E0                 1107 3$:	movx	a,@dptr
   0414 A3                 1108 	inc	dptr
   0415 05 86              1109 	inc	dps
   0417 F0                 1110 	movx	@dptr,a
   0418 A3                 1111 	inc	dptr
   0419 15 86              1112 	dec	dps
   041B D8 F6              1113 	djnz	r0,3$
   041D 85 82 47           1114 	mov	ctrladdr,dpl0
   0420 85 83 48           1115 	mov	ctrladdr+1,dph0
   0423 EF                 1116 2$:	mov	a,r7
   0424 90 7F B5           1117 	mov	dptr,#IN0BC
   0427 F0                 1118 	movx	@dptr,a
   0428 22                 1119 	ret
                           1120 
   0429                    1121 usb_sudav_isr:
   0429 C0 E0              1122 	push	acc
   042B C0 F0              1123 	push	b
   042D C0 82              1124 	push	dpl0
   042F C0 83              1125 	push	dph0
   0431 C0 84              1126 	push	dpl1
   0433 C0 85              1127 	push	dph1
   0435 C0 D0              1128 	push	psw
   0437 75 D0 00           1129 	mov	psw,#0x00
   043A C0 86              1130 	push	dps
   043C 75 86 00           1131 	mov	dps,#0
   043F C0 00              1132 	push	ar0
   0441 C0 07              1133 	push	ar7
                           1134 	;; clear interrupt
   0443 E5 91              1135 	mov	a,exif
   0445 C2 E4              1136 	clr	acc.4
   0447 F5 91              1137 	mov	exif,a
   0449 90 7F AB           1138 	mov	dptr,#USBIRQ
   044C 74 01              1139 	mov	a,#0x01
   044E F0                 1140 	movx	@dptr,a
                           1141 	;; handle interrupt
   044F 75 46 00           1142 	mov	ctrlcode,#0		; reset control out code
   0452 90 7F E9           1143 	mov	dptr,#SETUPDAT+1
   0455 E0                 1144 	movx	a,@dptr			; bRequest field
                           1145 	;; standard commands
                           1146 	;; USB_REQ_GET_DESCRIPTOR
   0456 B4 06 59           1147 	cjne	a,#USB_REQ_GET_DESCRIPTOR,cmdnotgetdesc
   0459 90 7F E8           1148 	mov	dptr,#SETUPDAT		; bRequestType == 0x80
   045C E0                 1149 	movx	a,@dptr
   045D B4 80 4F           1150 	cjne	a,#USB_DIR_IN,setupstallstd
   0460 90 7F EB           1151 	mov	dptr,#SETUPDAT+3
   0463 E0                 1152 	movx	a,@dptr
   0464 B4 01 0C           1153 	cjne	a,#USB_DT_DEVICE,cmdnotgetdescdev
   0467 90 7F D4           1154 	mov	dptr,#SUDPTRH
   046A 74 0B              1155 	mov	a,#>devicedescr
   046C F0                 1156 	movx	@dptr,a
   046D A3                 1157 	inc	dptr
   046E 74 2C              1158 	mov	a,#<devicedescr
   0470 F0                 1159 	movx	@dptr,a
   0471 80 39              1160 	sjmp	setupackstd
   0473                    1161 cmdnotgetdescdev:
   0473 B4 02 12           1162 	cjne	a,#USB_DT_CONFIG,cmdnotgetdesccfg
   0476 90 7F EA           1163 	mov	dptr,#SETUPDAT+2
   0479 E0                 1164 	movx	a,@dptr
   047A 70 33              1165 	jnz	setupstallstd
   047C 90 7F D4           1166 	mov	dptr,#SUDPTRH
   047F 74 0B              1167 	mov	a,#>config0descr
   0481 F0                 1168 	movx	@dptr,a
   0482 A3                 1169 	inc	dptr
   0483 74 3E              1170 	mov	a,#<config0descr
   0485 F0                 1171 	movx	@dptr,a
   0486 80 24              1172 	sjmp	setupackstd
   0488                    1173 cmdnotgetdesccfg:
   0488 B4 03 24           1174 	cjne	a,#USB_DT_STRING,setupstallstd
   048B 90 7F EA           1175 	mov	dptr,#SETUPDAT+2
   048E E0                 1176 	movx	a,@dptr
   048F 24 FC              1177 	add	a,#-numstrings
   0491 40 1C              1178 	jc	setupstallstd
   0493 E0                 1179 	movx	a,@dptr
   0494 25 E0              1180 	add	a,acc
   0496 24 83              1181 	add	a,#<stringdescr
   0498 F5 82              1182 	mov	dpl0,a
   049A E4                 1183 	clr	a
   049B 34 0B              1184 	addc	a,#>stringdescr
   049D F5 83              1185 	mov	dph0,a
   049F E0                 1186 	movx	a,@dptr
   04A0 F5 F0              1187 	mov	b,a
   04A2 A3                 1188 	inc	dptr
   04A3 E0                 1189 	movx	a,@dptr
   04A4 90 7F D4           1190 	mov	dptr,#SUDPTRH
   04A7 F0                 1191 	movx	@dptr,a
   04A8 A3                 1192 	inc	dptr
   04A9 E5 F0              1193 	mov	a,b
   04AB F0                 1194 	movx	@dptr,a
                           1195 	; sjmp	setupackstd	
   04AC                    1196 setupackstd:
   04AC 02 06 F6           1197 	ljmp	setupack
   04AF                    1198 setupstallstd:
   04AF 02 06 F2           1199 	ljmp	setupstall
   04B2                    1200 cmdnotgetdesc:
                           1201 	;; USB_REQ_SET_CONFIGURATION
   04B2 B4 09 41           1202 	cjne	a,#USB_REQ_SET_CONFIGURATION,cmdnotsetconf
   04B5 90 7F E8           1203 	mov	dptr,#SETUPDAT
   04B8 E0                 1204 	movx	a,@dptr
   04B9 70 F4              1205 	jnz	setupstallstd
   04BB 90 7F EA           1206 	mov	dptr,#SETUPDAT+2
   04BE E0                 1207 	movx	a,@dptr
   04BF 24 FE              1208 	add	a,#-2
   04C1 40 EC              1209 	jc	setupstallstd
   04C3 E0                 1210 	movx	a,@dptr
   04C4 F5 4B              1211 	mov	numconfig,a
   04C6                    1212 cmdresettoggleshalt:
   04C6 90 7F D7           1213 	mov	dptr,#TOGCTL
   04C9 78 07              1214 	mov	r0,#7
   04CB E8                 1215 0$:	mov	a,r0
   04CC 44 10              1216 	orl	a,#0x10
   04CE F0                 1217 	movx	@dptr,a
   04CF 44 30              1218 	orl	a,#0x30
   04D1 F0                 1219 	movx	@dptr,a
   04D2 E8                 1220 	mov	a,r0
   04D3 F0                 1221 	movx	@dptr,a
   04D4 44 20              1222 	orl	a,#0x20
   04D6 F0                 1223 	movx	@dptr,a
   04D7 D8 F2              1224 	djnz	r0,0$
   04D9 E4                 1225 	clr	a
   04DA F0                 1226 	movx	@dptr,a
   04DB 74 02              1227 	mov	a,#2
   04DD 90 7F B6           1228 	mov	dptr,#IN1CS
   04E0 78 07              1229 	mov	r0,#7
   04E2 F0                 1230 1$:	movx	@dptr,a
   04E3 A3                 1231 	inc	dptr
   04E4 A3                 1232 	inc	dptr
   04E5 D8 FB              1233 	djnz	r0,1$
   04E7 90 7F C6           1234 	mov	dptr,#OUT1CS
   04EA 78 07              1235 	mov	r0,#7
   04EC F0                 1236 2$:	movx	@dptr,a
   04ED A3                 1237 	inc	dptr
   04EE A3                 1238 	inc	dptr
   04EF D8 FB              1239 	djnz	r0,2$
   04F1 12 08 B0           1240 	lcall	fillusbintr
   04F4 80 B6              1241 	sjmp	setupackstd
   04F6                    1242 cmdnotsetconf:
                           1243 	;; USB_REQ_SET_INTERFACE
   04F6 B4 0B 1A           1244 	cjne	a,#USB_REQ_SET_INTERFACE,cmdnotsetint
   04F9 90 7F E8           1245 	mov	dptr,#SETUPDAT
   04FC E0                 1246 	movx	a,@dptr
   04FD B4 01 AF           1247 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_OUT,setupstallstd
   0500 E5 4B              1248 	mov	a,numconfig
   0502 B4 01 AA           1249 	cjne	a,#1,setupstallstd
   0505 90 7F EC           1250 	mov	dptr,#SETUPDAT+4
   0508 E0                 1251 	movx	a,@dptr
   0509 70 A4              1252 	jnz	setupstallstd
   050B 90 7F EA           1253 	mov	dptr,#SETUPDAT+2
   050E E0                 1254 	movx	a,@dptr
   050F F5 4C              1255 	mov	altsetting,a
   0511 80 B3              1256 	sjmp	cmdresettoggleshalt
   0513                    1257 cmdnotsetint:
                           1258 	;; USB_REQ_GET_INTERFACE
   0513 B4 0A 20           1259 	cjne	a,#USB_REQ_GET_INTERFACE,cmdnotgetint
   0516 90 7F E8           1260 	mov	dptr,#SETUPDAT
   0519 E0                 1261 	movx	a,@dptr
   051A B4 81 92           1262 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,setupstallstd
   051D E5 4B              1263 	mov	a,numconfig
   051F B4 01 8D           1264 	cjne	a,#1,setupstallstd
   0522 90 7F EC           1265 	mov	dptr,#SETUPDAT+4
   0525 E0                 1266 	movx	a,@dptr
   0526 70 87              1267 	jnz	setupstallstd
   0528 E5 4C              1268 	mov	a,altsetting
   052A                    1269 cmdrespondonebyte:
   052A 90 7F 00           1270 	mov	dptr,#IN0BUF
   052D F0                 1271 	movx	@dptr,a
   052E 90 7F B5           1272 	mov	dptr,#IN0BC
   0531 74 01              1273 	mov	a,#1
   0533 F0                 1274 	movx	@dptr,a	
   0534 80 4E              1275 	sjmp	setupackstd2
   0536                    1276 cmdnotgetint:
                           1277 	;; USB_REQ_GET_CONFIGURATION
   0536 B4 08 0B           1278 	cjne	a,#USB_REQ_GET_CONFIGURATION,cmdnotgetconf
   0539 90 7F E8           1279 	mov	dptr,#SETUPDAT
   053C E0                 1280 	movx	a,@dptr
   053D B4 80 47           1281 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,setupstallstd2
   0540 E5 4B              1282 	mov	a,numconfig
   0542 80 E6              1283 	sjmp	cmdrespondonebyte	
   0544                    1284 cmdnotgetconf:
                           1285 	;; USB_REQ_GET_STATUS (0)
   0544 70 44              1286 	jnz	cmdnotgetstat
   0546 90 7F E8           1287 	mov	dptr,#SETUPDAT
   0549 E0                 1288 	movx	a,@dptr
   054A B4 80 11           1289 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,cmdnotgetstatdev
   054D 74 01              1290 	mov	a,#1
   054F                    1291 cmdrespondstat:
   054F 90 7F 00           1292 	mov	dptr,#IN0BUF
   0552 F0                 1293 	movx	@dptr,a
   0553 A3                 1294 	inc	dptr
   0554 E4                 1295 	clr	a
   0555 F0                 1296 	movx	@dptr,a
   0556 90 7F B5           1297 	mov	dptr,#IN0BC
   0559 74 02              1298 	mov	a,#2
   055B F0                 1299 	movx	@dptr,a	
   055C 80 26              1300 	sjmp	setupackstd2
   055E                    1301 cmdnotgetstatdev:
   055E B4 81 03           1302 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,cmdnotgetstatintf
   0561 E4                 1303 	clr	a
   0562 80 EB              1304 	sjmp	cmdrespondstat
   0564                    1305 cmdnotgetstatintf:
   0564 B4 82 20           1306 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_IN,setupstallstd2
   0567 90 7F EC           1307 	mov	dptr,#SETUPDAT+4
   056A E0                 1308 	movx	a,@dptr
   056B 90 7F C4           1309 	mov	dptr,#OUT1CS-2
   056E 30 E7 03           1310 	jnb	acc.7,0$
   0571 90 7F B4           1311 	mov	dptr,#IN1CS-2
   0574 54 0F              1312 0$:	anl	a,#15
   0576 60 0F              1313 	jz	setupstallstd2
   0578 20 E3 0C           1314 	jb	acc.3,setupstallstd2
   057B 25 E0              1315 	add	a,acc
   057D 25 82              1316 	add	a,dpl0
   057F F5 82              1317 	mov	dpl0,a
   0581 E0                 1318 	movx	a,@dptr
   0582 80 CB              1319 	sjmp	cmdrespondstat
   0584                    1320 setupackstd2:
   0584 02 06 F6           1321 	ljmp	setupack
   0587                    1322 setupstallstd2:
   0587 02 06 F2           1323 	ljmp	setupstall
   058A                    1324 cmdnotgetstat:
                           1325 	;; USB_REQ_SET_FEATURE
   058A B4 03 05           1326 	cjne	a,#USB_REQ_SET_FEATURE,cmdnotsetftr
   058D 75 F0 01           1327 	mov	b,#1
   0590 80 06              1328 	sjmp	handleftr
   0592                    1329 cmdnotsetftr:
                           1330 	;; USB_REQ_CLEAR_FEATURE
   0592 B4 01 3E           1331 	cjne	a,#USB_REQ_CLEAR_FEATURE,cmdnotclrftr
   0595 75 F0 00           1332 	mov	b,#0
   0598                    1333 handleftr:
   0598 90 7F E8           1334 	mov	dptr,#SETUPDAT
   059B E0                 1335 	movx	a,@dptr
   059C B4 02 E8           1336 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_OUT,setupstallstd2
   059F A3                 1337 	inc	dptr
   05A0 A3                 1338 	inc	dptr
   05A1 E0                 1339 	movx	a,@dptr
   05A2 70 E3              1340 	jnz	setupstallstd2	; not ENDPOINT_HALT feature
   05A4 A3                 1341 	inc	dptr
   05A5 E0                 1342 	movx	a,@dptr
   05A6 70 DF              1343 	jnz	setupstallstd2
   05A8 A3                 1344 	inc	dptr
   05A9 E0                 1345 	movx	a,@dptr
   05AA 90 7F C4           1346 	mov	dptr,#OUT1CS-2
   05AD 30 E7 05           1347 	jnb	acc.7,0$
   05B0 90 7F B4           1348 	mov	dptr,#IN1CS-2
   05B3 44 10              1349 	orl	a,#0x10
   05B5 20 E3 CF           1350 0$:	jb	acc.3,setupstallstd2
                           1351 	;; clear data toggle
   05B8 54 1F              1352 	anl	a,#0x1f
   05BA 05 86              1353 	inc	dps
   05BC 90 7F D7           1354 	mov	dptr,#TOGCTL
   05BF F0                 1355 	movx	@dptr,a
   05C0 44 20              1356 	orl	a,#0x20
   05C2 F0                 1357 	movx	@dptr,a
   05C3 54 0F              1358 	anl	a,#15
   05C5 F0                 1359 	movx	@dptr,a
   05C6 15 86              1360 	dec	dps	
                           1361 	;; clear/set ep halt feature
   05C8 25 E0              1362 	add	a,acc
   05CA 25 82              1363 	add	a,dpl0
   05CC F5 82              1364 	mov	dpl0,a
   05CE E5 F0              1365 	mov	a,b
   05D0 F0                 1366 	movx	@dptr,a
   05D1 80 B1              1367 	sjmp	setupackstd2
   05D3                    1368 cmdnotclrftr:
                           1369 	;; vendor specific commands
                           1370 	;; 0xa3
   05D3 B4 A3 3E           1371 	cjne	a,#0xa3,cmdnota3
   05D6 90 7F EA           1372 	mov	dptr,#SETUPDAT+2
   05D9 E0                 1373 	movx	a,@dptr
   05DA F5 47              1374 	mov	ctrladdr,a
   05DC A3                 1375 	inc	dptr
   05DD E0                 1376 	movx	a,@dptr
   05DE F5 48              1377 	mov	ctrladdr+1,a
   05E0 24 50              1378 	add	a,#80
   05E2 50 2D              1379 	jnc	setupstalla3
   05E4 90 7F EE           1380 	mov	dptr,#SETUPDAT+6
   05E7 E0                 1381 	movx	a,@dptr
   05E8 F5 49              1382 	mov	ctrllen,a
   05EA 25 47              1383 	add	a,ctrladdr
   05EC A3                 1384 	inc	dptr
   05ED E0                 1385 	movx	a,@dptr
   05EE F5 4A              1386 	mov	ctrllen+1,a
   05F0 35 48              1387 	addc	a,ctrladdr+1
   05F2 40 1D              1388 	jc	setupstalla3
   05F4 90 7F E8           1389 	mov	dptr,#SETUPDAT		; bRequestType == 0x40
   05F7 E0                 1390 	movx	a,@dptr
   05F8 B4 40 0A           1391 	cjne	a,#0x40,1$
   05FB 75 46 01           1392 	mov	ctrlcode,#1
   05FE 90 7F C5           1393 	mov	dptr,#OUT0BC
   0601 F0                 1394 	movx	@dptr,a
   0602 02 06 FC           1395 	ljmp	endusbisr
   0605 B4 C0 09           1396 1$:	cjne	a,#0xc0,setupstalla3	; bRequestType == 0xc0
   0608 75 46 02           1397 	mov	ctrlcode,#2
   060B 12 03 E1           1398 	lcall	xmemread
   060E 02 06 FC           1399 	ljmp	endusbisr
   0611                    1400 setupstalla3:
   0611 02 06 F2           1401 	ljmp	setupstall
   0614                    1402 cmdnota3:
                           1403 	;; 0xb1
   0614 B4 B1 3E           1404 	cjne	a,#0xb1,cmdnotb1
   0617 90 7F E8           1405 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   061A E0                 1406 	movx	a,@dptr
   061B B4 C0 34           1407 	cjne	a,#0xc0,setupstallb1
   061E 90 7F 00           1408 	mov	dptr,#IN0BUF
   0621 E5 40              1409 	mov	a,errcode
   0623 F0                 1410 	movx	@dptr,a
   0624 A3                 1411 	inc	dptr
   0625 E5 41              1412 	mov	a,errval
   0627 F0                 1413 	movx	@dptr,a
   0628 A3                 1414 	inc	dptr
   0629 E5 81              1415 	mov	a,sp
                           1416 
                           1417 ;;; xxxx
   062B C0 82              1418 	push	dpl0
   062D C0 83              1419 	push	dph0
   062F 90 7F DF           1420 	mov	dptr,#OUT07VAL
   0632 E0                 1421 	movx	a,@dptr
   0633 D0 83              1422 	pop	dph0
   0635 D0 82              1423 	pop	dpl0
                           1424 ;;; xxxx
                           1425 	
   0637 F0                 1426 	movx	@dptr,a
   0638 A3                 1427 	inc	dptr
   0639 74 00              1428 	mov	a,#0
                           1429 
                           1430 ;;; xxxx
   063B C0 82              1431 	push	dpl0
   063D C0 83              1432 	push	dph0
   063F 90 7F C8           1433 	mov	dptr,#OUT2CS
   0642 E0                 1434 	movx	a,@dptr
   0643 D0 83              1435 	pop	dph0
   0645 D0 82              1436 	pop	dpl0
                           1437 ;;; xxxx
                           1438 	
   0647 F0                 1439 	movx	@dptr,a
   0648 A3                 1440 	inc	dptr
   0649 90 7F B5           1441 	mov	dptr,#IN0BC
   064C 74 04              1442 	mov	a,#4
   064E F0                 1443 	movx	@dptr,a
   064F 02 06 F6           1444 	ljmp	setupack
   0652                    1445 setupstallb1:
   0652 02 06 F2           1446 	ljmp	setupstall
   0655                    1447 cmdnotb1:
                           1448 	;; 0xb2
   0655 B4 B2 2B           1449 	cjne	a,#0xb2,cmdnotb2
   0658 90 7F E8           1450 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   065B E0                 1451 	movx	a,@dptr
   065C B4 C0 21           1452 	cjne	a,#0xc0,setupstallb2
   065F 90 7F 00           1453 	mov	dptr,#IN0BUF
   0662 E5 40              1454 	mov	a,errcode
   0664 F0                 1455 	movx	@dptr,a
   0665 A3                 1456 	inc	dptr
   0666 E5 41              1457 	mov	a,errval
   0668 F0                 1458 	movx	@dptr,a
   0669 A3                 1459 	inc	dptr
   066A E5 42              1460 	mov	a,cfgcount
   066C F0                 1461 	movx	@dptr,a
   066D A3                 1462 	inc	dptr
   066E E5 43              1463 	mov	a,cfgcount+1
   0670 F0                 1464 	movx	@dptr,a
   0671 A3                 1465 	inc	dptr
   0672 E5 45              1466 	mov	a,irqcount
   0674 F0                 1467 	movx	@dptr,a
   0675 90 7F B5           1468 	mov	dptr,#IN0BC
   0678 74 05              1469 	mov	a,#5
   067A F0                 1470 	movx	@dptr,a
   067B 05 45              1471 	inc	irqcount
   067D 02 06 F6           1472 	ljmp	setupack
   0680                    1473 setupstallb2:
   0680 02 06 F2           1474 	ljmp	setupstall
   0683                    1475 cmdnotb2:
                           1476 	;; 0xb3
   0683 B4 B3 2A           1477 	cjne	a,#0xb3,cmdnotb3
   0686 90 7F E8           1478 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   0689 E0                 1479 	movx	a,@dptr
   068A B4 C0 20           1480 	cjne	a,#0xc0,setupstallb3
                           1481 	;; read EEPROM
                    0006   1482 	serstrlen = 6
   068D 75 F0 A0           1483 	mov	b,#0xa0		; EEPROM address
   0690 90 01 8B           1484 	mov	dptr,#eepromstraddr
   0693 7F 01              1485 	mov	r7,#1
   0695 12 01 8C           1486 	lcall	writei2c
   0698 70 13              1487 	jnz	setupstallb3
   069A 90 7F 00           1488 	mov	dptr,#IN0BUF
   069D 7F 06              1489 	mov	r7,#serstrlen
   069F 12 01 C7           1490 	lcall	readi2c
   06A2 70 09              1491 	jnz	setupstallb3
   06A4 74 06              1492 	mov	a,#serstrlen
   06A6 90 7F B5           1493 	mov	dptr,#IN0BC
   06A9 F0                 1494 	movx	@dptr,a
   06AA 02 06 F6           1495 	ljmp	setupack
   06AD                    1496 setupstallb3:
   06AD 02 06 F2           1497 	ljmp	setupstall
   06B0                    1498 cmdnotb3:
                           1499 	;; 0xb4
   06B0 B4 B4 3F           1500 	cjne	a,#0xb4,cmdnotb4
   06B3 90 7F EF           1501 	mov	dptr,#SETUPDAT+7
   06B6 E0                 1502 	movx	a,@dptr
   06B7 70 36              1503 	jnz	setupstallb4
   06B9 90 7F EE           1504 	mov	dptr,#SETUPDAT+6
   06BC E0                 1505 	movx	a,@dptr
   06BD F8                 1506 	mov	r0,a
   06BE FF                 1507 	mov	r7,a
   06BF 24 C0              1508 	add	a,#-64
   06C1 40 2C              1509 	jc	setupstallb4
   06C3 90 7F EC           1510 	mov	dptr,#SETUPDAT+4	; wIndex
   06C6 E0                 1511 	movx	a,@dptr
   06C7 F5 F0              1512 	mov	b,a
   06C9 F5 47              1513 	mov	ctrladdr,a
   06CB 90 7F E8           1514 	mov	dptr,#SETUPDAT
   06CE E0                 1515 	movx	a,@dptr
   06CF B4 40 0A           1516 	cjne	a,#0x40,0$		; bRequestType == 0x40
   06D2 75 46 02           1517 	mov	ctrlcode,#2
   06D5 90 7F C5           1518 	mov	dptr,#OUT0BC
   06D8 F0                 1519 	movx	@dptr,a
   06D9 02 06 FC           1520 	ljmp	endusbisr
   06DC B4 C0 10           1521 0$:	cjne	a,#0xc0,setupstallb4	; bRequestType == 0xc0
   06DF 90 7F 00           1522 	mov	dptr,#IN0BUF
   06E2 12 01 C7           1523 	lcall	readi2c
   06E5 70 08              1524 	jnz	setupstallb4
   06E7 E8                 1525 	mov	a,r0
   06E8 90 7F B5           1526 	mov	dptr,#IN0BC
   06EB F0                 1527 	movx	@dptr,a
   06EC 02 06 F6           1528 	ljmp	setupack
   06EF                    1529 setupstallb4:
   06EF 02 06 F2           1530 	ljmp	setupstall
   06F2                    1531 cmdnotb4:
                           1532 	;; unknown commands fall through to setupstall
                           1533 
   06F2                    1534 setupstall:
   06F2 74 03              1535 	mov	a,#3
   06F4 80 02              1536 	sjmp	endsetup
   06F6                    1537 setupack:
   06F6 74 02              1538 	mov	a,#2
   06F8                    1539 endsetup:
   06F8 90 7F B4           1540 	mov	dptr,#EP0CS
   06FB F0                 1541 	movx	@dptr,a
   06FC                    1542 endusbisr:
                           1543 	;; epilogue
   06FC D0 07              1544 	pop	ar7
   06FE D0 00              1545 	pop	ar0
   0700 D0 86              1546 	pop	dps
   0702 D0 D0              1547 	pop	psw
   0704 D0 85              1548 	pop	dph1
   0706 D0 84              1549 	pop	dpl1
   0708 D0 83              1550 	pop	dph0
   070A D0 82              1551 	pop	dpl0
   070C D0 F0              1552 	pop	b
   070E D0 E0              1553 	pop	acc
   0710 32                 1554 	reti
                           1555 
   0711                    1556 usb_sof_isr:
   0711 C0 E0              1557 	push	acc
   0713 C0 F0              1558 	push	b
   0715 C0 82              1559 	push	dpl0
   0717 C0 83              1560 	push	dph0
   0719 C0 D0              1561 	push	psw
   071B 75 D0 00           1562 	mov	psw,#0x00
   071E C0 86              1563 	push	dps
   0720 75 86 00           1564 	mov	dps,#0
                           1565 	;; clear interrupt
   0723 E5 91              1566 	mov	a,exif
   0725 C2 E4              1567 	clr	acc.4
   0727 F5 91              1568 	mov	exif,a
   0729 90 7F AB           1569 	mov	dptr,#USBIRQ
   072C 74 02              1570 	mov	a,#0x02
   072E F0                 1571 	movx	@dptr,a
                           1572 	;; handle interrupt
                           1573 	;; epilogue
   072F D0 86              1574 	pop	dps
   0731 D0 D0              1575 	pop	psw
   0733 D0 83              1576 	pop	dph0
   0735 D0 82              1577 	pop	dpl0
   0737 D0 F0              1578 	pop	b
   0739 D0 E0              1579 	pop	acc
   073B 32                 1580 	reti
                           1581 
                           1582 
   073C                    1583 usb_sutok_isr:
   073C C0 E0              1584 	push	acc
   073E C0 F0              1585 	push	b
   0740 C0 82              1586 	push	dpl0
   0742 C0 83              1587 	push	dph0
   0744 C0 D0              1588 	push	psw
   0746 75 D0 00           1589 	mov	psw,#0x00
   0749 C0 86              1590 	push	dps
   074B 75 86 00           1591 	mov	dps,#0
                           1592 	;; clear interrupt
   074E E5 91              1593 	mov	a,exif
   0750 C2 E4              1594 	clr	acc.4
   0752 F5 91              1595 	mov	exif,a
   0754 90 7F AB           1596 	mov	dptr,#USBIRQ
   0757 74 04              1597 	mov	a,#0x04
   0759 F0                 1598 	movx	@dptr,a
                           1599 	;; handle interrupt
                           1600 	;; epilogue
   075A D0 86              1601 	pop	dps
   075C D0 D0              1602 	pop	psw
   075E D0 83              1603 	pop	dph0
   0760 D0 82              1604 	pop	dpl0
   0762 D0 F0              1605 	pop	b
   0764 D0 E0              1606 	pop	acc
   0766 32                 1607 	reti
                           1608 
   0767                    1609 usb_suspend_isr:
   0767 C0 E0              1610 	push	acc
   0769 C0 F0              1611 	push	b
   076B C0 82              1612 	push	dpl0
   076D C0 83              1613 	push	dph0
   076F C0 D0              1614 	push	psw
   0771 75 D0 00           1615 	mov	psw,#0x00
   0774 C0 86              1616 	push	dps
   0776 75 86 00           1617 	mov	dps,#0
                           1618 	;; clear interrupt
   0779 E5 91              1619 	mov	a,exif
   077B C2 E4              1620 	clr	acc.4
   077D F5 91              1621 	mov	exif,a
   077F 90 7F AB           1622 	mov	dptr,#USBIRQ
   0782 74 08              1623 	mov	a,#0x08
   0784 F0                 1624 	movx	@dptr,a
                           1625 	;; handle interrupt
                           1626 	;; epilogue
   0785 D0 86              1627 	pop	dps
   0787 D0 D0              1628 	pop	psw
   0789 D0 83              1629 	pop	dph0
   078B D0 82              1630 	pop	dpl0
   078D D0 F0              1631 	pop	b
   078F D0 E0              1632 	pop	acc
   0791 32                 1633 	reti
                           1634 
   0792                    1635 usb_usbreset_isr:
   0792 C0 E0              1636 	push	acc
   0794 C0 F0              1637 	push	b
   0796 C0 82              1638 	push	dpl0
   0798 C0 83              1639 	push	dph0
   079A C0 D0              1640 	push	psw
   079C 75 D0 00           1641 	mov	psw,#0x00
   079F C0 86              1642 	push	dps
   07A1 75 86 00           1643 	mov	dps,#0
                           1644 	;; clear interrupt
   07A4 E5 91              1645 	mov	a,exif
   07A6 C2 E4              1646 	clr	acc.4
   07A8 F5 91              1647 	mov	exif,a
   07AA 90 7F AB           1648 	mov	dptr,#USBIRQ
   07AD 74 10              1649 	mov	a,#0x10
   07AF F0                 1650 	movx	@dptr,a
                           1651 	;; handle interrupt
                           1652 	;; epilogue
   07B0 D0 86              1653 	pop	dps
   07B2 D0 D0              1654 	pop	psw
   07B4 D0 83              1655 	pop	dph0
   07B6 D0 82              1656 	pop	dpl0
   07B8 D0 F0              1657 	pop	b
   07BA D0 E0              1658 	pop	acc
   07BC 32                 1659 	reti
                           1660 
   07BD                    1661 usb_ep0in_isr:
   07BD C0 E0              1662 	push	acc
   07BF C0 F0              1663 	push	b
   07C1 C0 82              1664 	push	dpl0
   07C3 C0 83              1665 	push	dph0
   07C5 C0 84              1666 	push	dpl1
   07C7 C0 85              1667 	push	dph1
   07C9 C0 D0              1668 	push	psw
   07CB 75 D0 00           1669 	mov	psw,#0x00
   07CE C0 86              1670 	push	dps
   07D0 75 86 00           1671 	mov	dps,#0
   07D3 C0 00              1672 	push	ar0
   07D5 C0 07              1673 	push	ar7
                           1674 	;; clear interrupt
   07D7 E5 91              1675 	mov	a,exif
   07D9 C2 E4              1676 	clr	acc.4
   07DB F5 91              1677 	mov	exif,a
   07DD 90 7F A9           1678 	mov	dptr,#IN07IRQ
   07E0 74 01              1679 	mov	a,#0x01
   07E2 F0                 1680 	movx	@dptr,a
                           1681 	;; handle interrupt
   07E3 E5 46              1682 	mov	a,ctrlcode
   07E5 B4 02 05           1683 	cjne	a,#2,0$
   07E8 12 03 E1           1684 	lcall	xmemread
   07EB 80 11              1685 	sjmp	ep0inendisr
   07ED 90 7F B5           1686 0$:	mov	dptr,#IN0BC
   07F0 E4                 1687 	clr	a
   07F1 F0                 1688 	movx	@dptr,a
   07F2 80 04              1689 	sjmp	ep0inack
                           1690 
   07F4                    1691 ep0install:
   07F4 74 03              1692 	mov	a,#3
   07F6 80 02              1693 	sjmp	ep0incs
   07F8                    1694 ep0inack:
   07F8 74 02              1695 	mov	a,#2
   07FA                    1696 ep0incs:
   07FA 90 7F B4           1697 	mov	dptr,#EP0CS
   07FD F0                 1698 	movx	@dptr,a
   07FE                    1699 ep0inendisr:
                           1700 	;; epilogue
   07FE D0 07              1701 	pop	ar7
   0800 D0 00              1702 	pop	ar0
   0802 D0 86              1703 	pop	dps
   0804 D0 D0              1704 	pop	psw
   0806 D0 85              1705 	pop	dph1
   0808 D0 84              1706 	pop	dpl1
   080A D0 83              1707 	pop	dph0
   080C D0 82              1708 	pop	dpl0
   080E D0 F0              1709 	pop	b
   0810 D0 E0              1710 	pop	acc
   0812 32                 1711 	reti
                           1712 
   0813                    1713 usb_ep0out_isr:
   0813 C0 E0              1714 	push	acc
   0815 C0 F0              1715 	push	b
   0817 C0 82              1716 	push	dpl0
   0819 C0 83              1717 	push	dph0
   081B C0 84              1718 	push	dpl1
   081D C0 85              1719 	push	dph1
   081F C0 D0              1720 	push	psw
   0821 75 D0 00           1721 	mov	psw,#0x00
   0824 C0 86              1722 	push	dps
   0826 75 86 00           1723 	mov	dps,#0
   0829 C0 00              1724 	push	ar0
   082B C0 06              1725 	push	ar6
                           1726 	;; clear interrupt
   082D E5 91              1727 	mov	a,exif
   082F C2 E4              1728 	clr	acc.4
   0831 F5 91              1729 	mov	exif,a
   0833 90 7F AA           1730 	mov	dptr,#OUT07IRQ
   0836 74 01              1731 	mov	a,#0x01
   0838 F0                 1732 	movx	@dptr,a
                           1733 	;; handle interrupt
   0839 E5 46              1734 	mov	a,ctrlcode		; check control code
   083B B4 01 36           1735 	cjne	a,#0x01,i2cwr
                           1736 	;; write to external memory
   083E 90 7F C5           1737 	mov	dptr,#OUT0BC
   0841 E0                 1738 	movx	a,@dptr
   0842 60 28              1739 	jz	0$
   0844 FF                 1740 	mov	r7,a
   0845 C3                 1741 	clr	c
   0846 E5 49              1742 	mov	a,ctrllen
   0848 9F                 1743 	subb	a,r7
   0849 F5 49              1744 	mov	ctrllen,a
   084B E5 4A              1745 	mov	a,ctrllen+1
   084D 94 00              1746 	subb	a,#0
   084F F5 4A              1747 	mov	ctrllen+1,a
   0851 40 38              1748 	jc	ep0outstall
   0853 90 7E C0           1749 	mov	dptr,#OUT0BUF
   0856 85 47 84           1750 	mov	dpl1,ctrladdr
   0859 85 48 85           1751 	mov	dph1,ctrladdr+1
   085C E0                 1752 1$:	movx	a,@dptr
   085D A3                 1753 	inc	dptr
   085E 05 86              1754 	inc	dps
   0860 F0                 1755 	movx	@dptr,a
   0861 A3                 1756 	inc	dptr
   0862 15 86              1757 	dec	dps
   0864 DF F6              1758 	djnz	r7,1$
   0866 85 84 47           1759 	mov	ctrladdr,dpl1
   0869 85 85 48           1760 	mov	ctrladdr+1,dph1
   086C E5 49              1761 0$:	mov	a,ctrllen
   086E 45 4A              1762 	orl	a,ctrllen+1
   0870 60 20              1763 	jz	ep0outack
   0872 80 27              1764 	sjmp	ep0outendisr
                           1765 
                           1766 	;; write I2C eeprom
   0874 B4 02 14           1767 i2cwr:	cjne	a,#0x02,ep0outstall
   0877 90 7F C5           1768 	mov	dptr,#OUT0BC
   087A E0                 1769 	movx	a,@dptr
   087B 60 15              1770 	jz	ep0outack
   087D FF                 1771 	mov	r7,a
   087E 85 47 F0           1772 	mov	b,ctrladdr
   0881 90 7E C0           1773 	mov	dptr,#OUT0BUF
   0884 12 01 8C           1774 	lcall	writei2c
   0887 70 02              1775 	jnz	ep0outstall
   0889 80 07              1776 	sjmp	ep0outack
                           1777 
   088B                    1778 ep0outstall:
   088B 75 46 00           1779 	mov	ctrlcode,#0
   088E 74 03              1780 	mov	a,#3
   0890 80 05              1781 	sjmp	ep0outcs
   0892                    1782 ep0outack:
   0892 75 46 00           1783 	mov	ctrlcode,#0
   0895 74 02              1784 	mov	a,#2
   0897                    1785 ep0outcs:
   0897 90 7F B4           1786 	mov	dptr,#EP0CS
   089A F0                 1787 	movx	@dptr,a
   089B                    1788 ep0outendisr:
                           1789 	;; epilogue
   089B D0 06              1790 	pop	ar6
   089D D0 00              1791 	pop	ar0
   089F D0 86              1792 	pop	dps
   08A1 D0 D0              1793 	pop	psw
   08A3 D0 85              1794 	pop	dph1
   08A5 D0 84              1795 	pop	dpl1
   08A7 D0 83              1796 	pop	dph0
   08A9 D0 82              1797 	pop	dpl0
   08AB D0 F0              1798 	pop	b
   08AD D0 E0              1799 	pop	acc
   08AF 32                 1800 	reti
                           1801 
   08B0                    1802 fillusbintr::
   08B0 90 7E 80           1803 	mov	dptr,#IN1BUF
   08B3 E5 40              1804 	mov	a,errcode
   08B5 F0                 1805 	movx	@dptr,a
   08B6 A3                 1806 	inc	dptr
   08B7 E5 41              1807 	mov	a,errval
   08B9 F0                 1808 	movx	@dptr,a
   08BA A3                 1809 	inc	dptr
   08BB E5 42              1810 	mov	a,cfgcount
   08BD F0                 1811 	movx	@dptr,a
   08BE A3                 1812 	inc	dptr
   08BF E5 43              1813 	mov	a,cfgcount+1
   08C1 F0                 1814 	movx	@dptr,a
   08C2 A3                 1815 	inc	dptr
   08C3 E5 45              1816 	mov	a,irqcount
   08C5 F0                 1817 	movx	@dptr,a
   08C6 90 7F B7           1818 	mov	dptr,#IN1BC
   08C9 74 05              1819 	mov	a,#5
   08CB F0                 1820 	movx	@dptr,a
   08CC 05 45              1821 	inc	irqcount
   08CE 22                 1822 	ret
                           1823 
   08CF                    1824 usb_ep1in_isr:
   08CF C0 E0              1825 	push	acc
   08D1 C0 F0              1826 	push	b
   08D3 C0 82              1827 	push	dpl0
   08D5 C0 83              1828 	push	dph0
   08D7 C0 D0              1829 	push	psw
   08D9 75 D0 00           1830 	mov	psw,#0x00
   08DC C0 86              1831 	push	dps
   08DE 75 86 00           1832 	mov	dps,#0
                           1833 	;; clear interrupt
   08E1 E5 91              1834 	mov	a,exif
   08E3 C2 E4              1835 	clr	acc.4
   08E5 F5 91              1836 	mov	exif,a
   08E7 90 7F A9           1837 	mov	dptr,#IN07IRQ
   08EA 74 02              1838 	mov	a,#0x02
   08EC F0                 1839 	movx	@dptr,a
                           1840 	;; handle interrupt
   08ED 12 08 B0           1841 	lcall	fillusbintr
                           1842 	;; epilogue
   08F0 D0 86              1843 	pop	dps
   08F2 D0 D0              1844 	pop	psw
   08F4 D0 83              1845 	pop	dph0
   08F6 D0 82              1846 	pop	dpl0
   08F8 D0 F0              1847 	pop	b
   08FA D0 E0              1848 	pop	acc
   08FC 32                 1849 	reti
                           1850 
   08FD                    1851 usb_ep1out_isr:
   08FD C0 E0              1852 	push	acc
   08FF C0 F0              1853 	push	b
   0901 C0 82              1854 	push	dpl0
   0903 C0 83              1855 	push	dph0
   0905 C0 D0              1856 	push	psw
   0907 75 D0 00           1857 	mov	psw,#0x00
   090A C0 86              1858 	push	dps
   090C 75 86 00           1859 	mov	dps,#0
                           1860 	;; clear interrupt
   090F E5 91              1861 	mov	a,exif
   0911 C2 E4              1862 	clr	acc.4
   0913 F5 91              1863 	mov	exif,a
   0915 90 7F AA           1864 	mov	dptr,#OUT07IRQ
   0918 74 02              1865 	mov	a,#0x02
   091A F0                 1866 	movx	@dptr,a
                           1867 	;; handle interrupt
                           1868 	;; epilogue
   091B D0 86              1869 	pop	dps
   091D D0 D0              1870 	pop	psw
   091F D0 83              1871 	pop	dph0
   0921 D0 82              1872 	pop	dpl0
   0923 D0 F0              1873 	pop	b
   0925 D0 E0              1874 	pop	acc
   0927 32                 1875 	reti
                           1876 
   0928                    1877 usb_ep2in_isr:
   0928 C0 E0              1878 	push	acc
   092A C0 F0              1879 	push	b
   092C C0 82              1880 	push	dpl0
   092E C0 83              1881 	push	dph0
   0930 C0 D0              1882 	push	psw
   0932 75 D0 00           1883 	mov	psw,#0x00
   0935 C0 86              1884 	push	dps
   0937 75 86 00           1885 	mov	dps,#0
                           1886 	;; clear interrupt
   093A E5 91              1887 	mov	a,exif
   093C C2 E4              1888 	clr	acc.4
   093E F5 91              1889 	mov	exif,a
   0940 90 7F A9           1890 	mov	dptr,#IN07IRQ
   0943 74 04              1891 	mov	a,#0x04
   0945 F0                 1892 	movx	@dptr,a
                           1893 	;; handle interrupt
                           1894 	;; epilogue
   0946 D0 86              1895 	pop	dps
   0948 D0 D0              1896 	pop	psw
   094A D0 83              1897 	pop	dph0
   094C D0 82              1898 	pop	dpl0
   094E D0 F0              1899 	pop	b
   0950 D0 E0              1900 	pop	acc
   0952 32                 1901 	reti
                           1902 
   0953                    1903 usb_ep2out_isr:
   0953 C0 E0              1904 	push	acc
   0955 C0 F0              1905 	push	b
   0957 C0 82              1906 	push	dpl0
   0959 C0 83              1907 	push	dph0
   095B C0 D0              1908 	push	psw
   095D 75 D0 00           1909 	mov	psw,#0x00
   0960 C0 86              1910 	push	dps
   0962 75 86 00           1911 	mov	dps,#0
                           1912 	;; clear interrupt
   0965 E5 91              1913 	mov	a,exif
   0967 C2 E4              1914 	clr	acc.4
   0969 F5 91              1915 	mov	exif,a
   096B 90 7F AA           1916 	mov	dptr,#OUT07IRQ
   096E 74 04              1917 	mov	a,#0x04
   0970 F0                 1918 	movx	@dptr,a
                           1919 	;; handle interrupt
                           1920 	;; epilogue
   0971 D0 86              1921 	pop	dps
   0973 D0 D0              1922 	pop	psw
   0975 D0 83              1923 	pop	dph0
   0977 D0 82              1924 	pop	dpl0
   0979 D0 F0              1925 	pop	b
   097B D0 E0              1926 	pop	acc
   097D 32                 1927 	reti
                           1928 
   097E                    1929 usb_ep3in_isr:
   097E C0 E0              1930 	push	acc
   0980 C0 F0              1931 	push	b
   0982 C0 82              1932 	push	dpl0
   0984 C0 83              1933 	push	dph0
   0986 C0 D0              1934 	push	psw
   0988 75 D0 00           1935 	mov	psw,#0x00
   098B C0 86              1936 	push	dps
   098D 75 86 00           1937 	mov	dps,#0
                           1938 	;; clear interrupt
   0990 E5 91              1939 	mov	a,exif
   0992 C2 E4              1940 	clr	acc.4
   0994 F5 91              1941 	mov	exif,a
   0996 90 7F A9           1942 	mov	dptr,#IN07IRQ
   0999 74 08              1943 	mov	a,#0x08
   099B F0                 1944 	movx	@dptr,a
                           1945 	;; handle interrupt
                           1946 	;; epilogue
   099C D0 86              1947 	pop	dps
   099E D0 D0              1948 	pop	psw
   09A0 D0 83              1949 	pop	dph0
   09A2 D0 82              1950 	pop	dpl0
   09A4 D0 F0              1951 	pop	b
   09A6 D0 E0              1952 	pop	acc
   09A8 32                 1953 	reti
                           1954 
   09A9                    1955 usb_ep3out_isr:
   09A9 C0 E0              1956 	push	acc
   09AB C0 F0              1957 	push	b
   09AD C0 82              1958 	push	dpl0
   09AF C0 83              1959 	push	dph0
   09B1 C0 D0              1960 	push	psw
   09B3 75 D0 00           1961 	mov	psw,#0x00
   09B6 C0 86              1962 	push	dps
   09B8 75 86 00           1963 	mov	dps,#0
                           1964 	;; clear interrupt
   09BB E5 91              1965 	mov	a,exif
   09BD C2 E4              1966 	clr	acc.4
   09BF F5 91              1967 	mov	exif,a
   09C1 90 7F AA           1968 	mov	dptr,#OUT07IRQ
   09C4 74 08              1969 	mov	a,#0x08
   09C6 F0                 1970 	movx	@dptr,a
                           1971 	;; handle interrupt
                           1972 	;; epilogue
   09C7 D0 86              1973 	pop	dps
   09C9 D0 D0              1974 	pop	psw
   09CB D0 83              1975 	pop	dph0
   09CD D0 82              1976 	pop	dpl0
   09CF D0 F0              1977 	pop	b
   09D1 D0 E0              1978 	pop	acc
   09D3 32                 1979 	reti
                           1980 
   09D4                    1981 usb_ep4in_isr:
   09D4 C0 E0              1982 	push	acc
   09D6 C0 F0              1983 	push	b
   09D8 C0 82              1984 	push	dpl0
   09DA C0 83              1985 	push	dph0
   09DC C0 D0              1986 	push	psw
   09DE 75 D0 00           1987 	mov	psw,#0x00
   09E1 C0 86              1988 	push	dps
   09E3 75 86 00           1989 	mov	dps,#0
                           1990 	;; clear interrupt
   09E6 E5 91              1991 	mov	a,exif
   09E8 C2 E4              1992 	clr	acc.4
   09EA F5 91              1993 	mov	exif,a
   09EC 90 7F A9           1994 	mov	dptr,#IN07IRQ
   09EF 74 10              1995 	mov	a,#0x10
   09F1 F0                 1996 	movx	@dptr,a
                           1997 	;; handle interrupt
                           1998 	;; epilogue
   09F2 D0 86              1999 	pop	dps
   09F4 D0 D0              2000 	pop	psw
   09F6 D0 83              2001 	pop	dph0
   09F8 D0 82              2002 	pop	dpl0
   09FA D0 F0              2003 	pop	b
   09FC D0 E0              2004 	pop	acc
   09FE 32                 2005 	reti
                           2006 
   09FF                    2007 usb_ep4out_isr:
   09FF C0 E0              2008 	push	acc
   0A01 C0 F0              2009 	push	b
   0A03 C0 82              2010 	push	dpl0
   0A05 C0 83              2011 	push	dph0
   0A07 C0 D0              2012 	push	psw
   0A09 75 D0 00           2013 	mov	psw,#0x00
   0A0C C0 86              2014 	push	dps
   0A0E 75 86 00           2015 	mov	dps,#0
                           2016 	;; clear interrupt
   0A11 E5 91              2017 	mov	a,exif
   0A13 C2 E4              2018 	clr	acc.4
   0A15 F5 91              2019 	mov	exif,a
   0A17 90 7F AA           2020 	mov	dptr,#OUT07IRQ
   0A1A 74 10              2021 	mov	a,#0x10
   0A1C F0                 2022 	movx	@dptr,a
                           2023 	;; handle interrupt
                           2024 	;; epilogue
   0A1D D0 86              2025 	pop	dps
   0A1F D0 D0              2026 	pop	psw
   0A21 D0 83              2027 	pop	dph0
   0A23 D0 82              2028 	pop	dpl0
   0A25 D0 F0              2029 	pop	b
   0A27 D0 E0              2030 	pop	acc
   0A29 32                 2031 	reti
                           2032 
   0A2A                    2033 usb_ep5in_isr:
   0A2A C0 E0              2034 	push	acc
   0A2C C0 F0              2035 	push	b
   0A2E C0 82              2036 	push	dpl0
   0A30 C0 83              2037 	push	dph0
   0A32 C0 D0              2038 	push	psw
   0A34 75 D0 00           2039 	mov	psw,#0x00
   0A37 C0 86              2040 	push	dps
   0A39 75 86 00           2041 	mov	dps,#0
                           2042 	;; clear interrupt
   0A3C E5 91              2043 	mov	a,exif
   0A3E C2 E4              2044 	clr	acc.4
   0A40 F5 91              2045 	mov	exif,a
   0A42 90 7F A9           2046 	mov	dptr,#IN07IRQ
   0A45 74 20              2047 	mov	a,#0x20
   0A47 F0                 2048 	movx	@dptr,a
                           2049 	;; handle interrupt
                           2050 	;; epilogue
   0A48 D0 86              2051 	pop	dps
   0A4A D0 D0              2052 	pop	psw
   0A4C D0 83              2053 	pop	dph0
   0A4E D0 82              2054 	pop	dpl0
   0A50 D0 F0              2055 	pop	b
   0A52 D0 E0              2056 	pop	acc
   0A54 32                 2057 	reti
                           2058 
   0A55                    2059 usb_ep5out_isr:
   0A55 C0 E0              2060 	push	acc
   0A57 C0 F0              2061 	push	b
   0A59 C0 82              2062 	push	dpl0
   0A5B C0 83              2063 	push	dph0
   0A5D C0 D0              2064 	push	psw
   0A5F 75 D0 00           2065 	mov	psw,#0x00
   0A62 C0 86              2066 	push	dps
   0A64 75 86 00           2067 	mov	dps,#0
                           2068 	;; clear interrupt
   0A67 E5 91              2069 	mov	a,exif
   0A69 C2 E4              2070 	clr	acc.4
   0A6B F5 91              2071 	mov	exif,a
   0A6D 90 7F AA           2072 	mov	dptr,#OUT07IRQ
   0A70 74 20              2073 	mov	a,#0x20
   0A72 F0                 2074 	movx	@dptr,a
                           2075 	;; handle interrupt
                           2076 	;; epilogue
   0A73 D0 86              2077 	pop	dps
   0A75 D0 D0              2078 	pop	psw
   0A77 D0 83              2079 	pop	dph0
   0A79 D0 82              2080 	pop	dpl0
   0A7B D0 F0              2081 	pop	b
   0A7D D0 E0              2082 	pop	acc
   0A7F 32                 2083 	reti
                           2084 
   0A80                    2085 usb_ep6in_isr:
   0A80 C0 E0              2086 	push	acc
   0A82 C0 F0              2087 	push	b
   0A84 C0 82              2088 	push	dpl0
   0A86 C0 83              2089 	push	dph0
   0A88 C0 D0              2090 	push	psw
   0A8A 75 D0 00           2091 	mov	psw,#0x00
   0A8D C0 86              2092 	push	dps
   0A8F 75 86 00           2093 	mov	dps,#0
                           2094 	;; clear interrupt
   0A92 E5 91              2095 	mov	a,exif
   0A94 C2 E4              2096 	clr	acc.4
   0A96 F5 91              2097 	mov	exif,a
   0A98 90 7F A9           2098 	mov	dptr,#IN07IRQ
   0A9B 74 40              2099 	mov	a,#0x40
   0A9D F0                 2100 	movx	@dptr,a
                           2101 	;; handle interrupt
                           2102 	;; epilogue
   0A9E D0 86              2103 	pop	dps
   0AA0 D0 D0              2104 	pop	psw
   0AA2 D0 83              2105 	pop	dph0
   0AA4 D0 82              2106 	pop	dpl0
   0AA6 D0 F0              2107 	pop	b
   0AA8 D0 E0              2108 	pop	acc
   0AAA 32                 2109 	reti
                           2110 
   0AAB                    2111 usb_ep6out_isr:
   0AAB C0 E0              2112 	push	acc
   0AAD C0 F0              2113 	push	b
   0AAF C0 82              2114 	push	dpl0
   0AB1 C0 83              2115 	push	dph0
   0AB3 C0 D0              2116 	push	psw
   0AB5 75 D0 00           2117 	mov	psw,#0x00
   0AB8 C0 86              2118 	push	dps
   0ABA 75 86 00           2119 	mov	dps,#0
                           2120 	;; clear interrupt
   0ABD E5 91              2121 	mov	a,exif
   0ABF C2 E4              2122 	clr	acc.4
   0AC1 F5 91              2123 	mov	exif,a
   0AC3 90 7F AA           2124 	mov	dptr,#OUT07IRQ
   0AC6 74 40              2125 	mov	a,#0x40
   0AC8 F0                 2126 	movx	@dptr,a
                           2127 	;; handle interrupt
                           2128 	;; epilogue
   0AC9 D0 86              2129 	pop	dps
   0ACB D0 D0              2130 	pop	psw
   0ACD D0 83              2131 	pop	dph0
   0ACF D0 82              2132 	pop	dpl0
   0AD1 D0 F0              2133 	pop	b
   0AD3 D0 E0              2134 	pop	acc
   0AD5 32                 2135 	reti
                           2136 
   0AD6                    2137 usb_ep7in_isr:
   0AD6 C0 E0              2138 	push	acc
   0AD8 C0 F0              2139 	push	b
   0ADA C0 82              2140 	push	dpl0
   0ADC C0 83              2141 	push	dph0
   0ADE C0 D0              2142 	push	psw
   0AE0 75 D0 00           2143 	mov	psw,#0x00
   0AE3 C0 86              2144 	push	dps
   0AE5 75 86 00           2145 	mov	dps,#0
                           2146 	;; clear interrupt
   0AE8 E5 91              2147 	mov	a,exif
   0AEA C2 E4              2148 	clr	acc.4
   0AEC F5 91              2149 	mov	exif,a
   0AEE 90 7F A9           2150 	mov	dptr,#IN07IRQ
   0AF1 74 80              2151 	mov	a,#0x80
   0AF3 F0                 2152 	movx	@dptr,a
                           2153 	;; handle interrupt
                           2154 	;; epilogue
   0AF4 D0 86              2155 	pop	dps
   0AF6 D0 D0              2156 	pop	psw
   0AF8 D0 83              2157 	pop	dph0
   0AFA D0 82              2158 	pop	dpl0
   0AFC D0 F0              2159 	pop	b
   0AFE D0 E0              2160 	pop	acc
   0B00 32                 2161 	reti
                           2162 
   0B01                    2163 usb_ep7out_isr:
   0B01 C0 E0              2164 	push	acc
   0B03 C0 F0              2165 	push	b
   0B05 C0 82              2166 	push	dpl0
   0B07 C0 83              2167 	push	dph0
   0B09 C0 D0              2168 	push	psw
   0B0B 75 D0 00           2169 	mov	psw,#0x00
   0B0E C0 86              2170 	push	dps
   0B10 75 86 00           2171 	mov	dps,#0
                           2172 	;; clear interrupt
   0B13 E5 91              2173 	mov	a,exif
   0B15 C2 E4              2174 	clr	acc.4
   0B17 F5 91              2175 	mov	exif,a
   0B19 90 7F AA           2176 	mov	dptr,#OUT07IRQ
   0B1C 74 80              2177 	mov	a,#0x80
   0B1E F0                 2178 	movx	@dptr,a
                           2179 	;; handle interrupt
                           2180 	;; epilogue
   0B1F D0 86              2181 	pop	dps
   0B21 D0 D0              2182 	pop	psw
   0B23 D0 83              2183 	pop	dph0
   0B25 D0 82              2184 	pop	dpl0
   0B27 D0 F0              2185 	pop	b
   0B29 D0 E0              2186 	pop	acc
   0B2B 32                 2187 	reti
                           2188 	
                           2189 	;; -----------------------------------------------------
                           2190 	;; USB descriptors
                           2191 	;; -----------------------------------------------------
                           2192 
                           2193 	;; Device and/or Interface Class codes
                    0000   2194 	USB_CLASS_PER_INTERFACE         = 0
                    0001   2195 	USB_CLASS_AUDIO                 = 1
                    0002   2196 	USB_CLASS_COMM                  = 2
                    0003   2197 	USB_CLASS_HID                   = 3
                    0007   2198 	USB_CLASS_PRINTER               = 7
                    0008   2199 	USB_CLASS_MASS_STORAGE          = 8
                    0009   2200 	USB_CLASS_HUB                   = 9
                    00FF   2201 	USB_CLASS_VENDOR_SPEC           = 0xff
                           2202 
                           2203 	;; Descriptor types
                    0001   2204 	USB_DT_DEVICE                   = 0x01
                    0002   2205 	USB_DT_CONFIG                   = 0x02
                    0003   2206 	USB_DT_STRING                   = 0x03
                    0004   2207 	USB_DT_INTERFACE                = 0x04
                    0005   2208 	USB_DT_ENDPOINT                 = 0x05
                           2209 
                           2210 	;; Standard requests
                    0000   2211 	USB_REQ_GET_STATUS              = 0x00
                    0001   2212 	USB_REQ_CLEAR_FEATURE           = 0x01
                    0003   2213 	USB_REQ_SET_FEATURE             = 0x03
                    0005   2214 	USB_REQ_SET_ADDRESS             = 0x05
                    0006   2215 	USB_REQ_GET_DESCRIPTOR          = 0x06
                    0007   2216 	USB_REQ_SET_DESCRIPTOR          = 0x07
                    0008   2217 	USB_REQ_GET_CONFIGURATION       = 0x08
                    0009   2218 	USB_REQ_SET_CONFIGURATION       = 0x09
                    000A   2219 	USB_REQ_GET_INTERFACE           = 0x0A
                    000B   2220 	USB_REQ_SET_INTERFACE           = 0x0B
                    000C   2221 	USB_REQ_SYNCH_FRAME             = 0x0C
                           2222 
                           2223 	;; USB Request Type and Endpoint Directions
                    0000   2224 	USB_DIR_OUT                     = 0
                    0080   2225 	USB_DIR_IN                      = 0x80
                           2226 
                    0000   2227 	USB_TYPE_STANDARD               = (0x00 << 5)
                    0020   2228 	USB_TYPE_CLASS                  = (0x01 << 5)
                    0040   2229 	USB_TYPE_VENDOR                 = (0x02 << 5)
                    0060   2230 	USB_TYPE_RESERVED               = (0x03 << 5)
                           2231 
                    0000   2232 	USB_RECIP_DEVICE                = 0x00
                    0001   2233 	USB_RECIP_INTERFACE             = 0x01
                    0002   2234 	USB_RECIP_ENDPOINT              = 0x02
                    0003   2235 	USB_RECIP_OTHER                 = 0x03
                           2236 
                           2237 	;; Request target types.
                    0000   2238 	USB_RT_DEVICE                   = 0x00
                    0001   2239 	USB_RT_INTERFACE                = 0x01
                    0002   2240 	USB_RT_ENDPOINT                 = 0x02
                           2241 
                    BAC0   2242 	VENDID	= 0xbac0
                    6135   2243 	PRODID	= 0x6135
                           2244 
   0B2C                    2245 devicedescr:
   0B2C 12                 2246 	.db	18			; bLength
   0B2D 01                 2247 	.db	USB_DT_DEVICE		; bDescriptorType
   0B2E 00 01              2248 	.db	0x00, 0x01		; bcdUSB
   0B30 FF                 2249 	.db	USB_CLASS_VENDOR_SPEC	; bDeviceClass
   0B31 00                 2250 	.db	0			; bDeviceSubClass
   0B32 FF                 2251 	.db	0xff			; bDeviceProtocol
   0B33 40                 2252 	.db	0x40			; bMaxPacketSize0
   0B34 C0 BA              2253 	.db	<VENDID,>VENDID		; idVendor
   0B36 35 61              2254 	.db	<PRODID,>PRODID		; idProduct
   0B38 02 00              2255 	.db	0x02,0x00		; bcdDevice
   0B3A 01                 2256 	.db	1			; iManufacturer
   0B3B 02                 2257 	.db	2			; iProduct
   0B3C 03                 2258 	.db	3			; iSerialNumber
   0B3D 01                 2259 	.db	1			; bNumConfigurations
                           2260 
   0B3E                    2261 config0descr:
   0B3E 09                 2262 	.db	9			; bLength
   0B3F 02                 2263 	.db	USB_DT_CONFIG		; bDescriptorType
   0B40 45 00              2264 	.db	<config0sz,>config0sz	; wTotalLength
   0B42 01                 2265 	.db	1			; bNumInterfaces
   0B43 01                 2266 	.db	1			; bConfigurationValue
   0B44 00                 2267 	.db	0			; iConfiguration
   0B45 40                 2268 	.db	0b01000000		; bmAttributs (self powered)
   0B46 00                 2269 	.db	0			; MaxPower (mA/2) (self powered so 0)
                           2270 	;; interface descriptor I0:A0
   0B47 09                 2271 	.db	9			; bLength
   0B48 04                 2272 	.db	USB_DT_INTERFACE	; bDescriptorType
   0B49 00                 2273 	.db	0			; bInterfaceNumber
   0B4A 00                 2274 	.db	0			; bAlternateSetting
   0B4B 03                 2275 	.db	3			; bNumEndpoints
   0B4C FF                 2276 	.db	0xff			; bInterfaceClass (vendor specific)
   0B4D 00                 2277 	.db	0x00			; bInterfaceSubClass
   0B4E FF                 2278 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0B4F 00                 2279 	.db	0			; iInterface
                           2280 	;; endpoint descriptor I0:A0:E0
   0B50 07                 2281 	.db	7			; bLength
   0B51 05                 2282 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B52 81                 2283 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0B53 02                 2284 	.db	0x02			; bmAttributes (bulk)
   0B54 40 00              2285 	.db	0x40,0x00		; wMaxPacketSize
   0B56 00                 2286 	.db	0			; bInterval
                           2287 	;; endpoint descriptor I0:A0:E1
   0B57 07                 2288 	.db	7			; bLength
   0B58 05                 2289 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B59 82                 2290 	.db	(USB_DIR_IN | 2)	; bEndpointAddress
   0B5A 02                 2291 	.db	0x02			; bmAttributes (bulk)
   0B5B 40 00              2292 	.db	0x40,0x00		; wMaxPacketSize
   0B5D 00                 2293 	.db	0			; bInterval
                           2294 	;; endpoint descriptor I0:A0:E2
   0B5E 07                 2295 	.db	7			; bLength
   0B5F 05                 2296 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B60 02                 2297 	.db	(USB_DIR_OUT | 2)	; bEndpointAddress
   0B61 02                 2298 	.db	0x02			; bmAttributes (bulk)
   0B62 40 00              2299 	.db	0x40,0x00		; wMaxPacketSize
   0B64 00                 2300 	.db	0			; bInterval
                           2301 	;; interface descriptor I0:A1
   0B65 09                 2302 	.db	9			; bLength
   0B66 04                 2303 	.db	USB_DT_INTERFACE	; bDescriptorType
   0B67 00                 2304 	.db	0			; bInterfaceNumber
   0B68 01                 2305 	.db	1			; bAlternateSetting
   0B69 03                 2306 	.db	3			; bNumEndpoints
   0B6A FF                 2307 	.db	0xff			; bInterfaceClass (vendor specific)
   0B6B 00                 2308 	.db	0x00			; bInterfaceSubClass
   0B6C FF                 2309 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0B6D 00                 2310 	.db	0			; iInterface
                           2311 	;; endpoint descriptor I0:A1:E0
   0B6E 07                 2312 	.db	7			; bLength
   0B6F 05                 2313 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B70 81                 2314 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0B71 03                 2315 	.db	0x03			; bmAttributes (interrupt)
   0B72 40 00              2316 	.db	0x40,0x00		; wMaxPacketSize
   0B74 0A                 2317 	.db	10			; bInterval
                           2318 	;; endpoint descriptor I0:A1:E1
   0B75 07                 2319 	.db	7			; bLength
   0B76 05                 2320 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B77 82                 2321 	.db	(USB_DIR_IN | 2)	; bEndpointAddress
   0B78 02                 2322 	.db	0x02			; bmAttributes (bulk)
   0B79 40 00              2323 	.db	0x40,0x00		; wMaxPacketSize
   0B7B 00                 2324 	.db	0			; bInterval
                           2325 	;; endpoint descriptor I0:A1:E2
   0B7C 07                 2326 	.db	7			; bLength
   0B7D 05                 2327 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B7E 02                 2328 	.db	(USB_DIR_OUT | 2)	; bEndpointAddress
   0B7F 02                 2329 	.db	0x02			; bmAttributes (bulk)
   0B80 40 00              2330 	.db	0x40,0x00		; wMaxPacketSize
   0B82 00                 2331 	.db	0			; bInterval
                           2332 
                    0045   2333 config0sz = . - config0descr
                           2334 
   0B83                    2335 stringdescr:
   0B83 8B 0B              2336 	.db	<string0,>string0
   0B85 8F 0B              2337 	.db	<string1,>string1
   0B87 9D 0B              2338 	.db	<string2,>string2
   0B89 BD 0B              2339 	.db	<stringserial,>stringserial
                           2340 
                    0004   2341 numstrings = (. - stringdescr)/2
                           2342 
   0B8B                    2343 string0:
   0B8B 04                 2344 	.db	string0sz		; bLength
   0B8C 03                 2345 	.db	USB_DT_STRING		; bDescriptorType
   0B8D 00 00              2346 	.db	0,0			; LANGID[0]: Lang Neutral
                    0004   2347 string0sz = . - string0
                           2348 
   0B8F                    2349 string1:
   0B8F 0E                 2350 	.db	string1sz		; bLength
   0B90 03                 2351 	.db	USB_DT_STRING		; bDescriptorType
   0B91 42 00 61 00 79 00  2352 	.db	'B,0,'a,0,'y,0,'c,0,'o,0,'m,0
        63 00 6F 00 6D 00
                    000E   2353 string1sz = . - string1
                           2354 
   0B9D                    2355 string2:
   0B9D 20                 2356 	.db	string2sz		; bLength
   0B9E 03                 2357 	.db	USB_DT_STRING		; bDescriptorType
   0B9F 55 00 53 00 42 00  2358 	.db	'U,0,'S,0,'B,0,'F,0,'L,0,'E,0,'X,0,' ,0
        46 00 4C 00 45 00
        58 00 20 00
   0BAF 28 00 62 00 6C 00  2359 	.db	'(,0,'b,0,'l,0,'a,0,'n,0,'k,0,'),0
        61 00 6E 00 6B 00
        29 00
                    0020   2360 string2sz = . - string2
                           2361 
   0BBD                    2362 stringserial:
   0BBD 02                 2363 	.db	2			; bLength
   0BBE 03                 2364 	.db	USB_DT_STRING		; bDescriptorType
   0BBF 00 00 00 00 00 00  2365 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
   0BCF 00 00 00 00 00 00  2366 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
                           2367 
