                              1 	.module main
                              2 
                              3 	;; ENDPOINTS
                              4 	;; EP0 in/out   Control
                              5 	;; COMMAND LIST
                              6 	;; C0 B4  read I2C eeprom
                              7 	;; 40 B4  write I2C eeprom
                              8 	
                              9 	;; define code segments link order
                             10 	.area CODE (CODE)
                             11 	.area CSEG (CODE)
                             12 	.area GSINIT (CODE)
                             13 	.area GSINIT2 (CODE)
                             14 
                             15 	;; -----------------------------------------------------
                             16 
                             17 	;; special function registers (which are not predefined)
                    0082     18 	dpl0    = 0x82
                    0083     19 	dph0    = 0x83
                    0084     20 	dpl1    = 0x84
                    0085     21 	dph1    = 0x85
                    0086     22 	dps     = 0x86
                    008E     23 	ckcon   = 0x8E
                    008F     24 	spc_fnc = 0x8F
                    0091     25 	exif    = 0x91
                    0092     26 	mpage   = 0x92
                    0098     27 	scon0   = 0x98
                    0099     28 	sbuf0   = 0x99
                    00C0     29 	scon1   = 0xC0
                    00C1     30 	sbuf1   = 0xC1
                    00D8     31 	eicon   = 0xD8
                    00E8     32 	eie     = 0xE8
                    00F8     33 	eip     = 0xF8
                             34 
                             35 	;; anchor xdata registers
                    7F00     36 	IN0BUF		= 0x7F00
                    7EC0     37 	OUT0BUF		= 0x7EC0
                    7E80     38 	IN1BUF		= 0x7E80
                    7E40     39 	OUT1BUF		= 0x7E40
                    7E00     40 	IN2BUF		= 0x7E00
                    7DC0     41 	OUT2BUF		= 0x7DC0
                    7D80     42 	IN3BUF		= 0x7D80
                    7D40     43 	OUT3BUF		= 0x7D40
                    7D00     44 	IN4BUF		= 0x7D00
                    7CC0     45 	OUT4BUF		= 0x7CC0
                    7C80     46 	IN5BUF		= 0x7C80
                    7C40     47 	OUT5BUF		= 0x7C40
                    7C00     48 	IN6BUF		= 0x7C00
                    7BC0     49 	OUT6BUF		= 0x7BC0
                    7B80     50 	IN7BUF		= 0x7B80
                    7B40     51 	OUT7BUF		= 0x7B40
                    7FE8     52 	SETUPBUF	= 0x7FE8
                    7FE8     53 	SETUPDAT	= 0x7FE8
                             54 
                    7FB4     55 	EP0CS		= 0x7FB4
                    7FB5     56 	IN0BC		= 0x7FB5
                    7FB6     57 	IN1CS		= 0x7FB6
                    7FB7     58 	IN1BC		= 0x7FB7
                    7FB8     59 	IN2CS		= 0x7FB8
                    7FB9     60 	IN2BC		= 0x7FB9
                    7FBA     61 	IN3CS		= 0x7FBA
                    7FBB     62 	IN3BC		= 0x7FBB
                    7FBC     63 	IN4CS		= 0x7FBC
                    7FBD     64 	IN4BC		= 0x7FBD
                    7FBE     65 	IN5CS		= 0x7FBE
                    7FBF     66 	IN5BC		= 0x7FBF
                    7FC0     67 	IN6CS		= 0x7FC0
                    7FC1     68 	IN6BC		= 0x7FC1
                    7FC2     69 	IN7CS		= 0x7FC2
                    7FC3     70 	IN7BC		= 0x7FC3
                    7FC5     71 	OUT0BC		= 0x7FC5
                    7FC6     72 	OUT1CS		= 0x7FC6
                    7FC7     73 	OUT1BC		= 0x7FC7
                    7FC8     74 	OUT2CS		= 0x7FC8
                    7FC9     75 	OUT2BC		= 0x7FC9
                    7FCA     76 	OUT3CS		= 0x7FCA
                    7FCB     77 	OUT3BC		= 0x7FCB
                    7FCC     78 	OUT4CS		= 0x7FCC
                    7FCD     79 	OUT4BC		= 0x7FCD
                    7FCE     80 	OUT5CS		= 0x7FCE
                    7FCF     81 	OUT5BC		= 0x7FCF
                    7FD0     82 	OUT6CS		= 0x7FD0
                    7FD1     83 	OUT6BC		= 0x7FD1
                    7FD2     84 	OUT7CS		= 0x7FD2
                    7FD3     85 	OUT7BC		= 0x7FD3
                             86 
                    7FA8     87 	IVEC		= 0x7FA8
                    7FA9     88 	IN07IRQ		= 0x7FA9
                    7FAA     89 	OUT07IRQ	= 0x7FAA
                    7FAB     90 	USBIRQ		= 0x7FAB
                    7FAC     91 	IN07IEN		= 0x7FAC
                    7FAD     92 	OUT07IEN	= 0x7FAD
                    7FAE     93 	USBIEN		= 0x7FAE
                    7FAF     94 	USBBAV		= 0x7FAF
                    7FB2     95 	BPADDRH		= 0x7FB2
                    7FB3     96 	BPADDRL		= 0x7FB3
                             97 
                    7FD4     98 	SUDPTRH		= 0x7FD4
                    7FD5     99 	SUDPTRL		= 0x7FD5
                    7FD6    100 	USBCS		= 0x7FD6
                    7FD7    101 	TOGCTL		= 0x7FD7
                    7FD8    102 	USBFRAMEL	= 0x7FD8
                    7FD9    103 	USBFRAMEH	= 0x7FD9
                    7FDB    104 	FNADDR		= 0x7FDB
                    7FDD    105 	USBPAIR		= 0x7FDD
                    7FDE    106 	IN07VAL		= 0x7FDE
                    7FDF    107 	OUT07VAL	= 0x7FDF
                    7FE3    108 	AUTOPTRH	= 0x7FE3
                    7FE4    109 	AUTOPTRL	= 0x7FE4
                    7FE5    110 	AUTODATA	= 0x7FE5
                            111 
                            112 	;; isochronous endpoints. only available if ISODISAB=0
                            113 
                    7F60    114 	OUT8DATA	= 0x7F60
                    7F61    115 	OUT9DATA	= 0x7F61
                    7F62    116 	OUT10DATA	= 0x7F62
                    7F63    117 	OUT11DATA	= 0x7F63
                    7F64    118 	OUT12DATA	= 0x7F64
                    7F65    119 	OUT13DATA	= 0x7F65
                    7F66    120 	OUT14DATA	= 0x7F66
                    7F67    121 	OUT15DATA	= 0x7F67
                            122 
                    7F68    123 	IN8DATA		= 0x7F68
                    7F69    124 	IN9DATA		= 0x7F69
                    7F6A    125 	IN10DATA	= 0x7F6A
                    7F6B    126 	IN11DATA	= 0x7F6B
                    7F6C    127 	IN12DATA	= 0x7F6C
                    7F6D    128 	IN13DATA	= 0x7F6D
                    7F6E    129 	IN14DATA	= 0x7F6E
                    7F6F    130 	IN15DATA	= 0x7F6F
                            131 
                    7F70    132 	OUT8BCH		= 0x7F70
                    7F71    133 	OUT8BCL		= 0x7F71
                    7F72    134 	OUT9BCH		= 0x7F72
                    7F73    135 	OUT9BCL		= 0x7F73
                    7F74    136 	OUT10BCH	= 0x7F74
                    7F75    137 	OUT10BCL	= 0x7F75
                    7F76    138 	OUT11BCH	= 0x7F76
                    7F77    139 	OUT11BCL	= 0x7F77
                    7F78    140 	OUT12BCH	= 0x7F78
                    7F79    141 	OUT12BCL	= 0x7F79
                    7F7A    142 	OUT13BCH	= 0x7F7A
                    7F7B    143 	OUT13BCL	= 0x7F7B
                    7F7C    144 	OUT14BCH	= 0x7F7C
                    7F7D    145 	OUT14BCL	= 0x7F7D
                    7F7E    146 	OUT15BCH	= 0x7F7E
                    7F7F    147 	OUT15BCL	= 0x7F7F
                            148 
                    7FF0    149 	OUT8ADDR	= 0x7FF0
                    7FF1    150 	OUT9ADDR	= 0x7FF1
                    7FF2    151 	OUT10ADDR	= 0x7FF2
                    7FF3    152 	OUT11ADDR	= 0x7FF3
                    7FF4    153 	OUT12ADDR	= 0x7FF4
                    7FF5    154 	OUT13ADDR	= 0x7FF5
                    7FF6    155 	OUT14ADDR	= 0x7FF6
                    7FF7    156 	OUT15ADDR	= 0x7FF7
                    7FF8    157 	IN8ADDR		= 0x7FF8
                    7FF9    158 	IN9ADDR		= 0x7FF9
                    7FFA    159 	IN10ADDR	= 0x7FFA
                    7FFB    160 	IN11ADDR	= 0x7FFB
                    7FFC    161 	IN12ADDR	= 0x7FFC
                    7FFD    162 	IN13ADDR	= 0x7FFD
                    7FFE    163 	IN14ADDR	= 0x7FFE
                    7FFF    164 	IN15ADDR	= 0x7FFF
                            165 
                    7FA0    166 	ISOERR		= 0x7FA0
                    7FA1    167 	ISOCTL		= 0x7FA1
                    7FA2    168 	ZBCOUNT		= 0x7FA2
                    7FE0    169 	INISOVAL	= 0x7FE0
                    7FE1    170 	OUTISOVAL	= 0x7FE1
                    7FE2    171 	FASTXFR		= 0x7FE2
                            172 
                            173 	;; CPU control registers
                            174 
                    7F92    175 	CPUCS		= 0x7F92
                            176 
                            177 	;; IO port control registers
                            178 
                    7F93    179 	PORTACFG	= 0x7F93
                    7F94    180 	PORTBCFG	= 0x7F94
                    7F95    181 	PORTCCFG	= 0x7F95
                    7F96    182 	OUTA		= 0x7F96
                    7F97    183 	OUTB		= 0x7F97
                    7F98    184 	OUTC		= 0x7F98
                    7F99    185 	PINSA		= 0x7F99
                    7F9A    186 	PINSB		= 0x7F9A
                    7F9B    187 	PINSC		= 0x7F9B
                    7F9C    188 	OEA		= 0x7F9C
                    7F9D    189 	OEB		= 0x7F9D
                    7F9E    190 	OEC		= 0x7F9E
                            191 
                            192 	;; I2C controller registers
                            193 
                    7FA5    194 	I2CS		= 0x7FA5
                    7FA6    195 	I2DAT		= 0x7FA6
                            196 
                            197 	;; FPGA defines
                    0003    198 	XC4K_IRLENGTH	= 3
                    0000    199 	XC4K_EXTEST	= 0
                    0001    200 	XC4K_PRELOAD	= 1
                    0005    201 	XC4K_CONFIGURE	= 5
                    0007    202 	XC4K_BYPASS	= 7
                            203 
                    2E64    204 	FPGA_CONFIGSIZE = 11876
                    0158    205 	FPGA_BOUND	= 344
                            206 
                    0000    207 	SOFTWARECONFIG	= 0
                            208 	
                            209 	;; -----------------------------------------------------
                            210 
                            211 	.area CODE (CODE)
   0000 02 09 AB            212 	ljmp	startup
   0003 02 02 36            213 	ljmp	int0_isr
   0006                     214 	.ds	5
   000B 02 02 57            215 	ljmp	timer0_isr
   000E                     216 	.ds	5
   0013 02 02 87            217 	ljmp	int1_isr
   0016                     218 	.ds	5
   001B 02 02 A8            219 	ljmp	timer1_isr
   001E                     220 	.ds	5
   0023 02 02 C9            221 	ljmp	ser0_isr
   0026                     222 	.ds	5
   002B 02 02 EC            223 	ljmp	timer2_isr
   002E                     224 	.ds	5
   0033 02 03 0D            225 	ljmp	resume_isr
   0036                     226 	.ds	5
   003B 02 03 2E            227 	ljmp	ser1_isr
   003E                     228 	.ds	5
   0043 02 01 00            229 	ljmp	usb_isr
   0046                     230 	.ds	5
   004B 02 03 51            231 	ljmp	i2c_isr
   004E                     232 	.ds	5
   0053 02 03 76            233 	ljmp	int4_isr
   0056                     234 	.ds	5
   005B 02 03 9B            235 	ljmp	int5_isr
   005E                     236 	.ds	5
   0063 02 03 C0            237 	ljmp	int6_isr
   0066                     238 	.ds	0x9a
                            239 
   0100                     240 usb_isr:
   0100 02 04 29            241 	ljmp	usb_sudav_isr
   0103                     242 	.ds	1
   0104 02 05 94            243 	ljmp	usb_sof_isr
   0107                     244 	.ds	1
   0108 02 05 BF            245 	ljmp	usb_sutok_isr
   010B                     246 	.ds	1
   010C 02 05 EA            247 	ljmp	usb_suspend_isr
   010F                     248 	.ds	1
   0110 02 06 15            249 	ljmp	usb_usbreset_isr
   0113                     250 	.ds	1
   0114 32                  251 	reti
   0115                     252 	.ds	3
   0118 02 06 40            253 	ljmp	usb_ep0in_isr
   011B                     254 	.ds	1
   011C 02 06 96            255 	ljmp	usb_ep0out_isr
   011F                     256 	.ds	1
   0120 02 07 4E            257 	ljmp	usb_ep1in_isr
   0123                     258 	.ds	1
   0124 02 07 7C            259 	ljmp	usb_ep1out_isr
   0127                     260 	.ds	1
   0128 02 07 A7            261 	ljmp	usb_ep2in_isr
   012B                     262 	.ds	1
   012C 02 07 D2            263 	ljmp	usb_ep2out_isr
   012F                     264 	.ds	1
   0130 02 07 FD            265 	ljmp	usb_ep3in_isr
   0133                     266 	.ds	1
   0134 02 08 28            267 	ljmp	usb_ep3out_isr
   0137                     268 	.ds	1
   0138 02 08 53            269 	ljmp	usb_ep4in_isr
   013B                     270 	.ds	1
   013C 02 08 7E            271 	ljmp	usb_ep4out_isr
   013F                     272 	.ds	1
   0140 02 08 A9            273 	ljmp	usb_ep5in_isr
   0143                     274 	.ds	1
   0144 02 08 D4            275 	ljmp	usb_ep5out_isr
   0147                     276 	.ds	1
   0148 02 08 FF            277 	ljmp	usb_ep6in_isr
   014B                     278 	.ds	1
   014C 02 09 2A            279 	ljmp	usb_ep6out_isr
   014F                     280 	.ds	1
   0150 02 09 55            281 	ljmp	usb_ep7in_isr
   0153                     282 	.ds	1
   0154 02 09 80            283 	ljmp	usb_ep7out_isr
                            284 
                            285 	;; -----------------------------------------------------
                            286 
                    0020    287 	RXCHUNKS = 32
                    0010    288 	TXCHUNKS = 16
                            289 
                    0000    290 	DEBUGIOCOPY	= 0
                    0000    291 	DEBUGRECEIVER	= 0
                            292 	
                            293 	.area	OSEG (OVR,DATA)
                            294 	.area	BSEG (BIT)
                            295 
                            296 
                            297 	.area	ISEG (DATA)
   0080                     298 txbcnt:		.ds	TXCHUNKS
   0090                     299 rxbcnt:		.ds	RXCHUNKS
   00B0                     300 stack:		.ds	0x80-RXCHUNKS-TXCHUNKS
                            301 
                            302 	.area	DSEG (DATA)
   0040                     303 errcode:	.ds	1
   0041                     304 errval:		.ds	1
   0042                     305 cfgcount:	.ds	2
   0044                     306 leddiv:		.ds	1
   0045                     307 irqcount:	.ds	1
   0046                     308 ctrlcode:	.ds	1
   0047                     309 ctrladdr:	.ds	2
   0049                     310 ctrllen:	.ds	2
                            311 
                            312 	.area	XSEG (DATA)
                            313 
                            314 
                            315 	.area	GSINIT (CODE)
   09AB                     316 startup:
   09AB 75 81 B0            317 	mov	sp,#stack	; -1
   09AE E4                  318 	clr	a
   09AF F5 D0               319 	mov	psw,a
   09B1 F5 86               320 	mov	dps,a
                            321 	;lcall	__sdcc_external_startup
                            322 	;mov	a,dpl0
                            323 	;jz	__sdcc_init_data
                            324 	;ljmp	__sdcc_program_startup
   09B3                     325 __sdcc_init_data:
                            326 
                            327 	.area	GSINIT2 (CODE)
                            328 
                    0002    329 	ar2 = 0x02
                    0003    330 	ar3 = 0x03
                    0004    331 	ar4 = 0x04
                    0005    332 	ar5 = 0x05
                    0006    333 	ar6 = 0x06
                    0007    334 	ar7 = 0x07
                    0000    335 	ar0 = 0x00
                    0001    336 	ar1 = 0x01
                            337 
   09B3                     338 __sdcc_program_startup:
                            339 	;; assembler code startup
   09B3 E4                  340 	clr	a
   09B4 F5 40               341 	mov	errcode,a
   09B6 F5 41               342 	mov	errval,a
   09B8 F5 42               343 	mov	cfgcount,a
   09BA F5 43               344 	mov	cfgcount+1,a
   09BC F5 45               345  	mov	irqcount,a
   09BE F5 46               346  	mov	ctrlcode,a
                            347 	;; some indirect register setup
   09C0 75 8E 31            348 	mov	ckcon,#0x31	; one external wait state, to avoid chip bugs
                            349 	;; Timer setup:
                            350 	;; timer 0: LED blink    : T0M=0, CT0=0, MODE0=1
                            351 	;; timer 1: Baudrate gen : T1M=1, CT1=0, MODE1=2
   09C3 75 89 21            352 	mov	tmod,#0x21
   09C6 75 88 55            353 	mov	tcon,#0x55	; INT0/INT1 edge
   09C9 75 8D 64            354 	mov	th1,#256-156	; 1200 bauds
   09CC 75 87 00            355 	mov	pcon,#0		; SMOD0=0
                            356 	;; give Windows a chance to finish the writecpucs control transfer
                            357 	;; 5ms delay loop
   09CF 90 F4 48            358 	mov	dptr,#(-3000)&0xffff
   09D2 A3                  359 0$:	inc	dptr		; 3 cycles
   09D3 E5 82               360 	mov	a,dpl0		; 2 cycles
   09D5 45 83               361 	orl	a,dph0		; 2 cycles
   09D7 70 F9               362 	jnz	0$		; 3 cycles
                            363 	;; init USB subsystem
   09D9 90 7F A1            364 	mov	dptr,#ISOCTL
   09DC 74 01               365 	mov	a,#1		; disable ISO endpoints
   09DE F0                  366 	movx	@dptr,a
   09DF 90 7F AF            367 	mov	dptr,#USBBAV
   09E2 74 01               368 	mov	a,#1		; enable autovector, disable breakpoint logic
   09E4 F0                  369 	movx	@dptr,a
   09E5 E4                  370 	clr	a
   09E6 90 7F E0            371 	mov	dptr,#INISOVAL
   09E9 F0                  372 	movx	@dptr,a
   09EA 90 7F E1            373 	mov	dptr,#OUTISOVAL
   09ED F0                  374 	movx	@dptr,a
   09EE 90 7F DD            375 	mov	dptr,#USBPAIR
   09F1 74 09               376 	mov	a,#0x9		; pair EP 2&3 for input & output
   09F3 F0                  377 	movx	@dptr,a
   09F4 90 7F DE            378 	mov	dptr,#IN07VAL
   09F7 74 03               379 	mov	a,#0x3		; enable EP0+EP1
   09F9 F0                  380 	movx	@dptr,a
   09FA 90 7F DF            381 	mov	dptr,#OUT07VAL
   09FD 74 05               382 	mov	a,#0x5		; enable EP0+EP2
   09FF F0                  383 	movx	@dptr,a
                            384 	;; USB:	init endpoint toggles
   0A00 90 7F D7            385 	mov	dptr,#TOGCTL
   0A03 74 12               386 	mov	a,#0x12
   0A05 F0                  387 	movx	@dptr,a
   0A06 74 32               388 	mov	a,#0x32		; clear EP 2 in toggle
   0A08 F0                  389 	movx	@dptr,a
   0A09 74 02               390 	mov	a,#0x02
   0A0B F0                  391 	movx	@dptr,a
   0A0C 74 22               392 	mov	a,#0x22		; clear EP 2 out toggle
   0A0E F0                  393 	movx	@dptr,a
                            394 	;; configure IO ports
   0A0F 90 7F 93            395 	mov	dptr,#PORTACFG
   0A12 74 00               396 	mov	a,#0
   0A14 F0                  397 	movx	@dptr,a
   0A15 90 7F 96            398 	mov	dptr,#OUTA
   0A18 74 80               399 	mov	a,#0x80		; set PROG lo
   0A1A F0                  400 	movx	@dptr,a
   0A1B 90 7F 9C            401 	mov	dptr,#OEA
   0A1E 74 C2               402 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   0A20 F0                  403 	movx	@dptr,a
   0A21 90 7F 94            404 	mov	dptr,#PORTBCFG
   0A24 74 00               405 	mov	a,#0
   0A26 F0                  406 	movx	@dptr,a
   0A27 90 7F 9D            407 	mov	dptr,#OEB
   0A2A 74 00               408 	mov	a,#0
   0A2C F0                  409 	movx	@dptr,a
   0A2D 90 7F 95            410 	mov	dptr,#PORTCCFG
   0A30 74 00               411 	mov	a,#0
   0A32 F0                  412 	movx	@dptr,a
   0A33 90 7F 98            413 	mov	dptr,#OUTC
   0A36 74 20               414 	mov	a,#0x20
   0A38 F0                  415 	movx	@dptr,a
   0A39 90 7F 9E            416 	mov	dptr,#OEC
   0A3C 74 2E               417 	mov	a,#0x2e		; out: LEDCON,LEDSTA,TCK,INIT  in: TDO
   0A3E F0                  418 	movx	@dptr,a
                            419 	;; enable interrupts
   0A3F 75 A8 82            420 	mov	ie,#0x82	; enable timer 0 int
   0A42 75 E8 01            421 	mov	eie,#0x01	; enable USB interrupts
   0A45 90 7F AE            422 	mov	dptr,#USBIEN
   0A48 74 01               423 	mov	a,#1		; enable SUDAV interrupt
   0A4A F0                  424 	movx	@dptr,a
   0A4B 90 7F AC            425 	mov	dptr,#IN07IEN
   0A4E 74 03               426 	mov	a,#3		; enable EP0+EP1 interrupt
   0A50 F0                  427 	movx	@dptr,a
   0A51 90 7F AD            428 	mov	dptr,#OUT07IEN
   0A54 74 01               429 	mov	a,#1		; enable EP0 interrupt
   0A56 F0                  430 	movx	@dptr,a
                            431 	;; initialize UART 0 for config loading
   0A57 75 98 20            432 	mov	scon0,#0x20	; mode 0, CLK24/4
   0A5A 75 99 FF            433 	mov	sbuf0,#0xff
                            434 	;; initialize EP1 IN (irq)
   0A5D 12 07 2F            435 	lcall	fillusbintr
                            436 
                            437 	;; some short delay
   0A60 90 7F 96            438 	mov	dptr,#OUTA
   0A63 74 82               439 	mov	a,#0x82		; set PROG hi
   0A65 F0                  440 	movx	@dptr,a
   0A66 90 7F 99            441 	mov	dptr,#PINSA
   0A69 E0                  442 	movx	a,@dptr
   0A6A 30 E2 06            443 	jnb	acc.2,1$
                            444 	;; DONE stuck high
   0A6D 75 40 80            445 	mov	errcode,#0x80
   0A70 02 0B 9C            446 	ljmp	fpgaerr
   0A73 12 01 57            447 1$:	lcall	jtag_reset_tap
   0A76 7A 05               448 	mov	r2,#5
   0A78 7B 00               449 	mov	r3,#0
   0A7A 7C 06               450 	mov	r4,#6
   0A7C 12 01 5D            451 	lcall	jtag_shiftout	; enter SHIFT-IR state
   0A7F 7A 08               452 	mov	r2,#8
   0A81 7B 00               453 	mov	r3,#0
   0A83 7C 00               454 	mov	r4,#0
   0A85 12 01 5D            455 	lcall	jtag_shiftout	; assume max. 8bit IR
   0A88 7A 08               456 	mov	r2,#8
   0A8A 7B 01               457 	mov	r3,#1
   0A8C 7C 00               458 	mov	r4,#0
   0A8E 12 01 5D            459 	lcall	jtag_shift	; shift in a single bit
   0A91 74 F8               460 	mov	a,#-(1<<XC4K_IRLENGTH)
   0A93 2F                  461 	add	a,r7
   0A94 60 08               462 	jz	2$
                            463 	;; JTAG instruction register error
   0A96 75 40 81            464 	mov	errcode,#0x81
   0A99 8F 41               465 	mov	errval,r7
   0A9B 02 0B 9C            466 	ljmp	fpgaerr
   0A9E 7A 05               467 2$:	mov	r2,#XC4K_IRLENGTH+2
   0AA0 7B 05               468 	mov	r3,#XC4K_CONFIGURE
   0AA2 7C 0C               469 	mov	r4,#3<<(XC4K_IRLENGTH-1)
   0AA4 12 01 5D            470 	lcall	jtag_shiftout	; shift in CONFIGURE insn, goto RUNTEST/IDLE state
   0AA7 90 7F 9E            471 	mov	dptr,#OEC
   0AAA 74 2A               472 	mov	a,#0x2a		; out: LEDCON,LEDSTA,TCK  in: TDO,INIT
   0AAC F0                  473 	movx	@dptr,a
   0AAD 75 40 01            474 	mov	errcode,#1
   0AB0 90 7F 9B            475 	mov	dptr,#PINSC
   0AB3 E0                  476 3$:	movx	a,@dptr
   0AB4 30 E2 FC            477 	jnb	acc.2,3$
   0AB7 7A 03               478 	mov	r2,#3
   0AB9 7B FF               479 	mov	r3,#0xff
   0ABB 7C 01               480 	mov	r4,#1
   0ABD 12 01 5D            481 	lcall	jtag_shiftout	; advance to SHIFT-DR state
                            482 	;; software configuration: enable UART special func pins
   0AC0 90 7F 96            483 	mov	dptr,#OUTA
   0AC3 74 42               484 	mov	a,#0x42		; PROG hi, TMS lo, TDI hi
   0AC5 F0                  485 	movx	@dptr,a
                    0000    486 	.if	SOFTWARECONFIG
                            487 	.else
   0AC6 90 7F 93            488 	mov	dptr,#PORTACFG
   0AC9 74 40               489 	mov	a,#0x40		; TDI is special function pin
   0ACB F0                  490 	movx	@dptr,a
   0ACC 90 7F 95            491 	mov	dptr,#PORTCCFG
   0ACF 74 02               492 	mov	a,#0x02		; TCK is special function pin
   0AD1 F0                  493 	movx	@dptr,a
                            494 	.endif
                            495 	;; start
   0AD2 75 40 10            496 	mov	errcode,#0x10
                            497 
   0AD5 90 7F C8            498 	mov	dptr,#OUT2CS
   0AD8 E4                  499 	clr	a
   0AD9 F0                  500 	movx	@dptr,a
   0ADA 90 7F C9            501 	mov	dptr,#OUT2BC
   0ADD F0                  502 	movx	@dptr,a		; arm EP2 OUT
                            503 	
                            504 	
   0ADE                     505 ldstloop:
   0ADE 90 7F C9            506 	mov	dptr,#OUT2BC
   0AE1 F0                  507 	movx	@dptr,a		; arm EP2 OUT
   0AE2 90 7F C8            508 0$:	mov	dptr,#OUT2CS
   0AE5 E0                  509 	movx	a,@dptr
   0AE6 20 E1 F9            510 	jb	acc.1,0$
   0AE9 90 7F C9            511 	mov	dptr,#OUT2BC
   0AEC E0                  512 	movx	a,@dptr
   0AED 24 FE               513 	add	a,#-2
   0AEF 50 ED               514 	jnc	ldstloop
   0AF1 90 7D C0            515 	mov	dptr,#OUT2BUF
   0AF4 E0                  516 	movx	a,@dptr
   0AF5 B4 FF E6            517 	cjne	a,#0xff,ldstloop
   0AF8 A3                  518 	inc	dptr
   0AF9 E0                  519 	movx	a,@dptr
   0AFA B4 04 E1            520 	cjne	a,#0x04,ldstloop
   0AFD 75 40 11            521 	mov	errcode,#0x11
   0B00 80 0B               522 	sjmp	xxloadloop
                            523 
   0B02                     524 loadloop:
   0B02 90 7F C9            525 	mov	dptr,#OUT2BC
   0B05 F0                  526 	movx	@dptr,a		; arm EP2 OUT
   0B06 90 7F C8            527 0$:	mov	dptr,#OUT2CS
   0B09 E0                  528 	movx	a,@dptr
   0B0A 20 E1 F9            529 	jb	acc.1,0$
   0B0D                     530 xxloadloop:
   0B0D 90 7F C9            531 	mov	dptr,#OUT2BC
   0B10 E0                  532 	movx	a,@dptr
   0B11 60 EF               533 	jz	loadloop
   0B13 FF                  534 	mov	r7,a
   0B14 25 42               535 	add	a,cfgcount
   0B16 F5 42               536 	mov	cfgcount,a
   0B18 E4                  537 	clr	a
   0B19 35 43               538 	addc	a,cfgcount+1
   0B1B F5 43               539 	mov	cfgcount+1,a
   0B1D 90 7D C0            540 	mov	dptr,#OUT2BUF
                    0000    541 	.if	SOFTWARECONFIG
                            542 1$:	movx	a,@dptr
                            543 	mov	r3,a
                            544 	mov	r2,#8
                            545 	inc	dps
                            546 2$:	mov	a,#2
                            547 	xch	a,r3
                            548 	rrc	a
                            549 	xch	a,r3
                            550 	mov	acc.6,c
                            551 	mov	dptr,#OUTA
                            552 	movx	@dptr,a
                            553 	mov	dptr,#OUTC
                            554 	movx	a,@dptr
                            555 	setb	acc.1
                            556 	movx	@dptr,a
                            557 	clr	acc.1
                            558 	movx	@dptr,a
                            559 	djnz	r2,2$
                            560 	dec	dps
                            561 	inc	dptr
                            562 	djnz	r7,1$
                            563 	.else
   0B20 E0                  564 	movx	a,@dptr
   0B21 80 04               565 	sjmp	3$
   0B23 E0                  566 1$:	movx	a,@dptr
   0B24 30 99 FD            567 2$:	jnb	scon0+1,2$
   0B27 C2 99               568 3$:	clr	scon0+1
   0B29 F5 99               569 	mov	sbuf0,a
   0B2B A3                  570 	inc	dptr
   0B2C DF F5               571 	djnz	r7,1$
                            572 	.endif
                            573 	;; check if init went low - indicates configuration error
   0B2E 90 7F 9B            574 	mov	dptr,#PINSC
   0B31 E0                  575 	movx	a,@dptr
   0B32 20 E2 0C            576 	jb	acc.2,5$
                            577 	;; INIT low
   0B35 90 7F C8            578 	mov	dptr,#OUT2CS
   0B38 74 01               579 	mov	a,#0x01
   0B3A F0                  580 	movx	@dptr,a
   0B3B 75 40 90            581 	mov	errcode,#0x90
   0B3E 02 0B 9C            582 	ljmp	fpgaerr
   0B41                     583 5$:	;; check for end of FPGA configuration data
   0B41 74 9C               584 	mov	a,#<(-FPGA_CONFIGSIZE)
   0B43 25 42               585 	add	a,cfgcount
   0B45 74 D1               586 	mov	a,#>(-FPGA_CONFIGSIZE)
   0B47 35 43               587 	addc	a,cfgcount+1
   0B49 50 B7               588 	jnc	loadloop
                            589 	;; configuration download complete, start FPGA
   0B4B 90 7F 94            590 	mov	dptr,#PORTBCFG
   0B4E 74 00               591 	mov	a,#0		; TDI no longer special function pin
   0B50 F0                  592 	movx	@dptr,a
   0B51 90 7F 95            593 	mov	dptr,#PORTCCFG
   0B54 74 00               594 	mov	a,#0		; TCK no longer special function pin
   0B56 F0                  595 	movx	@dptr,a
                            596 	;; wait for done loop
   0B57 7F FA               597 	mov	r7,#250
   0B59                     598 doneloop:
   0B59 90 7F 99            599 	mov	dptr,#PINSA
   0B5C E0                  600 	movx	a,@dptr
   0B5D 20 E2 1F            601 	jb	acc.2,doneactive
   0B60 90 7F 9B            602 	mov	dptr,#PINSC
   0B63 E0                  603 	movx	a,@dptr
   0B64 20 E2 06            604 	jb	acc.2,1$
                            605 	;; INIT low
   0B67 75 40 90            606 	mov	errcode,#0x90
   0B6A 02 0B 9C            607 	ljmp	fpgaerr
   0B6D 90 7F 98            608 1$:	mov	dptr,#OUTC
   0B70 E0                  609 	movx	a,@dptr
   0B71 D2 E1               610 	setb	acc.1
   0B73 F0                  611 	movx	@dptr,a
   0B74 C2 E1               612 	clr	acc.1
   0B76 F0                  613 	movx	@dptr,a
   0B77 DF E0               614 	djnz	r7,doneloop
                            615 	;; DONE stuck low
   0B79 75 40 91            616 	mov	errcode,#0x91
   0B7C 02 0B 9C            617 	ljmp	fpgaerr
                            618 	
   0B7F                     619 doneactive:
                            620 	;; generate 16 clock pulses to start up FPGA
   0B7F 7F 10               621 	mov	r7,#16
   0B81 90 7F 98            622 	mov	dptr,#OUTC
   0B84 E0                  623 	movx	a,@dptr
   0B85 D2 E1               624 0$:	setb	acc.1
   0B87 F0                  625 	movx	@dptr,a
   0B88 C2 E1               626 	clr	acc.1
   0B8A F0                  627 	movx	@dptr,a
   0B8B DF F8               628 	djnz	r7,0$
   0B8D 90 7F 95            629 	mov	dptr,#PORTCCFG
   0B90 74 C0               630 	mov	a,#0xc0		; RD/WR is special function pin
   0B92 F0                  631 	movx	@dptr,a
   0B93 75 40 20            632 	mov	errcode,#0x20
                            633 
   0B96 90 C0 08            634 	mov	dptr,#0xc008
   0B99 74 C7               635 	mov	a,#0xc7
   0B9B F0                  636 	movx	@dptr,a
                            637 	
   0B9C                     638 fpgaerr:
   0B9C 80 FE               639 	sjmp	fpgaerr
                            640 
                            641 	.area	CSEG (CODE)
                    0002    642 	ar2 = 0x02
                    0003    643 	ar3 = 0x03
                    0004    644 	ar4 = 0x04
                    0005    645 	ar5 = 0x05
                    0006    646 	ar6 = 0x06
                    0007    647 	ar7 = 0x07
                    0000    648 	ar0 = 0x00
                    0001    649 	ar1 = 0x01
                            650 
                            651 	;; jtag_shift
                            652 	;; r2 = num
                            653 	;; r3 = tdi
                            654 	;; r4 = tms
                            655 	;; return: r7 = tdo
   0157                     656 jtag_reset_tap:
   0157 7A 05               657 	mov	r2,#5
   0159 7B 00               658 	mov	r3,#0
   015B 7C FF               659 	mov	r4,#0xff
   015D                     660 jtag_shiftout:
   015D                     661 jtag_shift:
   015D 7E 01               662 	mov	r6,#1
   015F 7F 00               663 	mov	r7,#0
   0161 74 02               664 1$:	mov	a,#2
   0163 CB                  665 	xch	a,r3
   0164 13                  666 	rrc	a
   0165 CB                  667 	xch	a,r3
   0166 92 E6               668 	mov	acc.6,c
   0168 CC                  669 	xch	a,r4
   0169 13                  670 	rrc	a
   016A CC                  671 	xch	a,r4
   016B 92 E7               672 	mov	acc.7,c
   016D 90 7F 96            673 	mov	dptr,#OUTA
   0170 F0                  674 	movx	@dptr,a
   0171 90 7F 9B            675 	mov	dptr,#PINSC
   0174 E0                  676 	movx	a,@dptr
   0175 30 E0 03            677 	jnb	acc.0,2$
   0178 EE                  678 	mov	a,r6
   0179 42 07               679 	orl	ar7,a
   017B 90 7F 98            680 2$:	mov	dptr,#OUTC
   017E E0                  681 	movx	a,@dptr
   017F D2 E1               682 	setb	acc.1
   0181 F0                  683 	movx	@dptr,a
   0182 C2 E1               684 	clr	acc.1
   0184 F0                  685 	movx	@dptr,a
   0185 EE                  686 	mov	a,r6
   0186 23                  687 	rl	a
   0187 FE                  688 	mov	r6,a
   0188 DA D7               689 	djnz	r2,1$
   018A 22                  690 	ret
                            691 
                            692 	;; EEPROM address where the serial number is located
                            693 	
   018B                     694 eepromstraddr:
   018B 10                  695 	.db	0x10
                            696 
                            697 	;; I2C Routines
                            698 	;; note: ckcon should be set to #0x31 to avoid chip bugs
                            699 
   018C                     700 writei2c:
                            701 	;; dptr: data to be sent
                            702 	;; b:    device address
                            703 	;; r7:   transfer length
                            704 	;; return: a: status (bytes remaining, 0xff bus error)
   018C 05 86               705 	inc	dps
   018E 90 7F A5            706 	mov	dptr,#I2CS
   0191 C2 F0               707 	clr	b.0		; r/w = 0
   0193 74 80               708 	mov	a,#0x80		; start condition
   0195 F0                  709 	movx	@dptr,a
   0196 90 7F A6            710 	mov	dptr,#I2DAT
   0199 E5 F0               711 	mov	a,b
   019B F0                  712 	movx	@dptr,a
   019C 90 7F A5            713 	mov	dptr,#I2CS	
   019F E0                  714 0$:	movx	a,@dptr
   01A0 20 E2 20            715 	jb	acc.2,3$	; BERR
   01A3 30 E0 F9            716 	jnb	acc.0,0$	; DONE
   01A6 30 E1 0F            717 	jnb	acc.1,1$	; ACK
   01A9 90 7F A6            718 	mov	dptr,#I2DAT
   01AC 15 86               719 	dec	dps
   01AE E0                  720 	movx	a,@dptr
   01AF A3                  721 	inc	dptr
   01B0 05 86               722 	inc	dps
   01B2 F0                  723 	movx	@dptr,a
   01B3 90 7F A5            724 	mov	dptr,#I2CS
   01B6 DF E7               725 	djnz	r7,0$
   01B8 74 40               726 1$:	mov	a,#0x40		; stop condition
   01BA F0                  727 	movx	@dptr,a
   01BB E0                  728 2$:	movx	a,@dptr
   01BC 20 E6 FC            729 	jb	acc.6,2$
   01BF 15 86               730 	dec	dps
   01C1 EF                  731 	mov	a,r7
   01C2 22                  732 	ret
   01C3 7F FF               733 3$:	mov	r7,#255
   01C5 80 F1               734 	sjmp	1$
                            735 
   01C7                     736 readi2c:
                            737 	;; dptr: data to be sent
                            738 	;; b:    device address
                            739 	;; r7:   transfer length
                            740 	;; return: a: status (bytes remaining, 0xff bus error)
   01C7 05 86               741 	inc	dps
   01C9 90 7F A5            742 	mov	dptr,#I2CS
   01CC D2 F0               743 	setb	b.0		; r/w = 1
   01CE 74 80               744 	mov	a,#0x80		; start condition
   01D0 F0                  745 	movx	@dptr,a
   01D1 90 7F A6            746 	mov	dptr,#I2DAT
   01D4 E5 F0               747 	mov	a,b
   01D6 F0                  748 	movx	@dptr,a
   01D7 90 7F A5            749 	mov	dptr,#I2CS
   01DA E0                  750 0$:	movx	a,@dptr
   01DB 20 E2 4C            751 	jb	acc.2,9$	; BERR
   01DE 30 E0 F9            752 	jnb	acc.0,0$	; DONE
   01E1 30 E1 03            753 	jnb	acc.1,5$	; ACK
   01E4 EF                  754 	mov	a,r7
   01E5 70 0B               755 	jnz	1$
   01E7 74 40               756 5$:	mov	a,#0x40		; stop condition
   01E9 F0                  757 	movx	@dptr,a
   01EA E0                  758 2$:	movx	a,@dptr
   01EB 20 E6 FC            759 	jb	acc.6,2$
   01EE 15 86               760 	dec	dps
   01F0 E4                  761 	clr	a
   01F1 22                  762 	ret
   01F2 B4 01 03            763 1$:	cjne	a,#1,3$
   01F5 74 20               764 	mov	a,#0x20		; LASTRD = 1
   01F7 F0                  765 	movx	@dptr,a
   01F8 90 7F A6            766 3$:	mov	dptr,#I2DAT
   01FB E0                  767 	movx	a,@dptr		; initiate first read
   01FC 90 7F A5            768 	mov	dptr,#I2CS
   01FF E0                  769 4$:	movx	a,@dptr
   0200 20 E2 27            770 	jb	acc.2,9$	; BERR
   0203 30 E0 F9            771 	jnb	acc.0,4$	; DONE
   0206 EF                  772 	mov	a,r7
   0207 B4 02 03            773 	cjne	a,#2,6$
   020A 74 20               774 	mov	a,#0x20		; LASTRD = 1
   020C F0                  775 	movx	@dptr,a
   020D B4 01 03            776 6$:	cjne	a,#1,7$
   0210 74 40               777 	mov	a,#0x40		; stop condition
   0212 F0                  778 	movx	@dptr,a
   0213 90 7F A6            779 7$:	mov	dptr,#I2DAT
   0216 E0                  780 	movx	a,@dptr		; read data
   0217 15 86               781 	dec	dps
   0219 F0                  782 	movx	@dptr,a
   021A A3                  783 	inc	dptr
   021B 05 86               784 	inc	dps
   021D 90 7F A5            785 	mov	dptr,#I2CS
   0220 DF DD               786 	djnz	r7,4$
   0222 E0                  787 8$:	movx	a,@dptr
   0223 20 E6 FC            788 	jb	acc.6,8$
   0226 15 86               789 	dec	dps
   0228 E4                  790 	clr	a
   0229 22                  791 	ret
   022A 74 40               792 9$:	mov	a,#0x40		; stop condition
   022C F0                  793 	movx	@dptr,a
   022D E0                  794 10$:	movx	a,@dptr
   022E 20 E6 FC            795 	jb	acc.6,10$
   0231 74 FF               796 	mov	a,#255
   0233 15 86               797 	dec	dps
   0235 22                  798 	ret
                            799 
                            800 	;; ------------------ interrupt handlers ------------------------
                            801 
   0236                     802 int0_isr:
   0236 C0 E0               803 	push	acc
   0238 C0 F0               804 	push	b
   023A C0 82               805 	push	dpl0
   023C C0 83               806 	push	dph0
   023E C0 D0               807 	push	psw
   0240 75 D0 00            808 	mov	psw,#0x00
   0243 C0 86               809 	push	dps
   0245 75 86 00            810 	mov	dps,#0
                            811 	;; clear interrupt
   0248 C2 89               812 	clr	tcon+1
                            813 	;; handle interrupt
                            814 	;; epilogue
   024A D0 86               815 	pop	dps
   024C D0 D0               816 	pop	psw
   024E D0 83               817 	pop	dph0
   0250 D0 82               818 	pop	dpl0
   0252 D0 F0               819 	pop	b
   0254 D0 E0               820 	pop	acc
   0256 32                  821 	reti
                            822 
   0257                     823 timer0_isr:
   0257 C0 E0               824 	push	acc
   0259 C0 F0               825 	push	b
   025B C0 82               826 	push	dpl0
   025D C0 83               827 	push	dph0
   025F C0 D0               828 	push	psw
   0261 75 D0 00            829 	mov	psw,#0x00
   0264 C0 86               830 	push	dps
   0266 75 86 00            831 	mov	dps,#0
                            832 	;; clear interrupt
   0269 C2 8D               833 	clr	tcon+5
                            834 	;; handle interrupt
   026B 05 44               835 	inc	leddiv
   026D E5 44               836 	mov	a,leddiv
   026F 54 07               837 	anl	a,#7
   0271 70 07               838 	jnz	0$
   0273 90 7F 98            839 	mov	dptr,#OUTC
   0276 E0                  840 	movx	a,@dptr
   0277 64 08               841 	xrl	a,#0x08
   0279 F0                  842 	movx	@dptr,a
   027A                     843 0$:
                            844 	;; epilogue
   027A D0 86               845 	pop	dps
   027C D0 D0               846 	pop	psw
   027E D0 83               847 	pop	dph0
   0280 D0 82               848 	pop	dpl0
   0282 D0 F0               849 	pop	b
   0284 D0 E0               850 	pop	acc
   0286 32                  851 	reti
                            852 
   0287                     853 int1_isr:
   0287 C0 E0               854 	push	acc
   0289 C0 F0               855 	push	b
   028B C0 82               856 	push	dpl0
   028D C0 83               857 	push	dph0
   028F C0 D0               858 	push	psw
   0291 75 D0 00            859 	mov	psw,#0x00
   0294 C0 86               860 	push	dps
   0296 75 86 00            861 	mov	dps,#0
                            862 	;; clear interrupt
   0299 C2 8B               863 	clr	tcon+3
                            864 	;; handle interrupt
                            865 	;; epilogue
   029B D0 86               866 	pop	dps
   029D D0 D0               867 	pop	psw
   029F D0 83               868 	pop	dph0
   02A1 D0 82               869 	pop	dpl0
   02A3 D0 F0               870 	pop	b
   02A5 D0 E0               871 	pop	acc
   02A7 32                  872 	reti
                            873 
   02A8                     874 timer1_isr:
   02A8 C0 E0               875 	push	acc
   02AA C0 F0               876 	push	b
   02AC C0 82               877 	push	dpl0
   02AE C0 83               878 	push	dph0
   02B0 C0 D0               879 	push	psw
   02B2 75 D0 00            880 	mov	psw,#0x00
   02B5 C0 86               881 	push	dps
   02B7 75 86 00            882 	mov	dps,#0
                            883 	;; clear interrupt
   02BA C2 8F               884 	clr	tcon+7
                            885 	;; handle interrupt
                            886 	;; epilogue
   02BC D0 86               887 	pop	dps
   02BE D0 D0               888 	pop	psw
   02C0 D0 83               889 	pop	dph0
   02C2 D0 82               890 	pop	dpl0
   02C4 D0 F0               891 	pop	b
   02C6 D0 E0               892 	pop	acc
   02C8 32                  893 	reti
                            894 
   02C9                     895 ser0_isr:
   02C9 C0 E0               896 	push	acc
   02CB C0 F0               897 	push	b
   02CD C0 82               898 	push	dpl0
   02CF C0 83               899 	push	dph0
   02D1 C0 D0               900 	push	psw
   02D3 75 D0 00            901 	mov	psw,#0x00
   02D6 C0 86               902 	push	dps
   02D8 75 86 00            903 	mov	dps,#0
                            904 	;; clear interrupt
   02DB C2 98               905 	clr	scon0+0
   02DD C2 99               906 	clr	scon0+1
                            907 	;; handle interrupt
                            908 	;; epilogue
   02DF D0 86               909 	pop	dps
   02E1 D0 D0               910 	pop	psw
   02E3 D0 83               911 	pop	dph0
   02E5 D0 82               912 	pop	dpl0
   02E7 D0 F0               913 	pop	b
   02E9 D0 E0               914 	pop	acc
   02EB 32                  915 	reti
                            916 
   02EC                     917 timer2_isr:
   02EC C0 E0               918 	push	acc
   02EE C0 F0               919 	push	b
   02F0 C0 82               920 	push	dpl0
   02F2 C0 83               921 	push	dph0
   02F4 C0 D0               922 	push	psw
   02F6 75 D0 00            923 	mov	psw,#0x00
   02F9 C0 86               924 	push	dps
   02FB 75 86 00            925 	mov	dps,#0
                            926 	;; clear interrupt
   02FE C2 CF               927 	clr	t2con+7
                            928 	;; handle interrupt
                            929 	;; epilogue
   0300 D0 86               930 	pop	dps
   0302 D0 D0               931 	pop	psw
   0304 D0 83               932 	pop	dph0
   0306 D0 82               933 	pop	dpl0
   0308 D0 F0               934 	pop	b
   030A D0 E0               935 	pop	acc
   030C 32                  936 	reti
                            937 
   030D                     938 resume_isr:
   030D C0 E0               939 	push	acc
   030F C0 F0               940 	push	b
   0311 C0 82               941 	push	dpl0
   0313 C0 83               942 	push	dph0
   0315 C0 D0               943 	push	psw
   0317 75 D0 00            944 	mov	psw,#0x00
   031A C0 86               945 	push	dps
   031C 75 86 00            946 	mov	dps,#0
                            947 	;; clear interrupt
   031F C2 DC               948 	clr	eicon+4
                            949 	;; handle interrupt
                            950 	;; epilogue
   0321 D0 86               951 	pop	dps
   0323 D0 D0               952 	pop	psw
   0325 D0 83               953 	pop	dph0
   0327 D0 82               954 	pop	dpl0
   0329 D0 F0               955 	pop	b
   032B D0 E0               956 	pop	acc
   032D 32                  957 	reti
                            958 
   032E                     959 ser1_isr:
   032E C0 E0               960 	push	acc
   0330 C0 F0               961 	push	b
   0332 C0 82               962 	push	dpl0
   0334 C0 83               963 	push	dph0
   0336 C0 D0               964 	push	psw
   0338 75 D0 00            965 	mov	psw,#0x00
   033B C0 86               966 	push	dps
   033D 75 86 00            967 	mov	dps,#0
                            968 	;; clear interrupt
   0340 C2 C0               969 	clr	scon1+0
   0342 C2 C1               970 	clr	scon1+1
                            971 	;; handle interrupt
                            972 	;; epilogue
   0344 D0 86               973 	pop	dps
   0346 D0 D0               974 	pop	psw
   0348 D0 83               975 	pop	dph0
   034A D0 82               976 	pop	dpl0
   034C D0 F0               977 	pop	b
   034E D0 E0               978 	pop	acc
   0350 32                  979 	reti
                            980 
   0351                     981 i2c_isr:
   0351 C0 E0               982 	push	acc
   0353 C0 F0               983 	push	b
   0355 C0 82               984 	push	dpl0
   0357 C0 83               985 	push	dph0
   0359 C0 D0               986 	push	psw
   035B 75 D0 00            987 	mov	psw,#0x00
   035E C0 86               988 	push	dps
   0360 75 86 00            989 	mov	dps,#0
                            990 	;; clear interrupt
   0363 E5 91               991 	mov	a,exif
   0365 C2 E5               992 	clr	acc.5
   0367 F5 91               993 	mov	exif,a
                            994 	;; handle interrupt
                            995 	;; epilogue
   0369 D0 86               996 	pop	dps
   036B D0 D0               997 	pop	psw
   036D D0 83               998 	pop	dph0
   036F D0 82               999 	pop	dpl0
   0371 D0 F0              1000 	pop	b
   0373 D0 E0              1001 	pop	acc
   0375 32                 1002 	reti
                           1003 
   0376                    1004 int4_isr:
   0376 C0 E0              1005 	push	acc
   0378 C0 F0              1006 	push	b
   037A C0 82              1007 	push	dpl0
   037C C0 83              1008 	push	dph0
   037E C0 D0              1009 	push	psw
   0380 75 D0 00           1010 	mov	psw,#0x00
   0383 C0 86              1011 	push	dps
   0385 75 86 00           1012 	mov	dps,#0
                           1013 	;; clear interrupt
   0388 E5 91              1014 	mov	a,exif
   038A C2 E6              1015 	clr	acc.6
   038C F5 91              1016 	mov	exif,a
                           1017 	;; handle interrupt
                           1018 	;; epilogue
   038E D0 86              1019 	pop	dps
   0390 D0 D0              1020 	pop	psw
   0392 D0 83              1021 	pop	dph0
   0394 D0 82              1022 	pop	dpl0
   0396 D0 F0              1023 	pop	b
   0398 D0 E0              1024 	pop	acc
   039A 32                 1025 	reti
                           1026 
   039B                    1027 int5_isr:
   039B C0 E0              1028 	push	acc
   039D C0 F0              1029 	push	b
   039F C0 82              1030 	push	dpl0
   03A1 C0 83              1031 	push	dph0
   03A3 C0 D0              1032 	push	psw
   03A5 75 D0 00           1033 	mov	psw,#0x00
   03A8 C0 86              1034 	push	dps
   03AA 75 86 00           1035 	mov	dps,#0
                           1036 	;; clear interrupt
   03AD E5 91              1037 	mov	a,exif
   03AF C2 E7              1038 	clr	acc.7
   03B1 F5 91              1039 	mov	exif,a
                           1040 	;; handle interrupt
                           1041 	;; epilogue
   03B3 D0 86              1042 	pop	dps
   03B5 D0 D0              1043 	pop	psw
   03B7 D0 83              1044 	pop	dph0
   03B9 D0 82              1045 	pop	dpl0
   03BB D0 F0              1046 	pop	b
   03BD D0 E0              1047 	pop	acc
   03BF 32                 1048 	reti
                           1049 
   03C0                    1050 int6_isr:
   03C0 C0 E0              1051 	push	acc
   03C2 C0 F0              1052 	push	b
   03C4 C0 82              1053 	push	dpl0
   03C6 C0 83              1054 	push	dph0
   03C8 C0 D0              1055 	push	psw
   03CA 75 D0 00           1056 	mov	psw,#0x00
   03CD C0 86              1057 	push	dps
   03CF 75 86 00           1058 	mov	dps,#0
                           1059 	;; clear interrupt
   03D2 C2 DB              1060 	clr	eicon+3
                           1061 	;; handle interrupt
                           1062 	;; epilogue
   03D4 D0 86              1063 	pop	dps
   03D6 D0 D0              1064 	pop	psw
   03D8 D0 83              1065 	pop	dph0
   03DA D0 82              1066 	pop	dpl0
   03DC D0 F0              1067 	pop	b
   03DE D0 E0              1068 	pop	acc
   03E0 32                 1069 	reti
                           1070 
   03E1                    1071 xmemread::
   03E1 E5 49              1072 	mov	a,ctrllen
   03E3 FF                 1073 	mov	r7,a
   03E4 24 C0              1074 	add	a,#-64
   03E6 F5 49              1075 	mov	ctrllen,a
   03E8 E5 4A              1076 	mov	a,ctrllen+1
   03EA 34 00              1077 	addc	a,#0
   03EC 50 12              1078 	jnc	0$
   03EE E4                 1079 	clr	a
   03EF F5 49              1080 	mov	ctrllen,a
   03F1 F5 4A              1081 	mov	ctrllen+1,a
   03F3 F5 46              1082 	mov	ctrlcode,a
   03F5 74 02              1083 	mov	a,#2		; ack control transfer
   03F7 90 7F B4           1084 	mov	dptr,#EP0CS
   03FA F0                 1085 	movx	@dptr,a
   03FB EF                 1086 	mov	a,r7
   03FC 60 25              1087 	jz	2$
   03FE 80 04              1088 	sjmp	1$
   0400 F5 4A              1089 0$:	mov	ctrllen+1,a
   0402 7F 40              1090 	mov	r7,#64
   0404 8F 00              1091 1$:	mov	ar0,r7
   0406 85 47 82           1092 	mov	dpl0,ctrladdr
   0409 85 48 83           1093 	mov	dph0,ctrladdr+1
   040C 05 86              1094 	inc	dps
   040E 90 7F 00           1095 	mov	dptr,#IN0BUF
   0411 15 86              1096 	dec	dps
   0413 E0                 1097 3$:	movx	a,@dptr
   0414 A3                 1098 	inc	dptr
   0415 05 86              1099 	inc	dps
   0417 F0                 1100 	movx	@dptr,a
   0418 A3                 1101 	inc	dptr
   0419 15 86              1102 	dec	dps
   041B D8 F6              1103 	djnz	r0,3$
   041D 85 82 47           1104 	mov	ctrladdr,dpl0
   0420 85 83 48           1105 	mov	ctrladdr+1,dph0
   0423 EF                 1106 2$:	mov	a,r7
   0424 90 7F B5           1107 	mov	dptr,#IN0BC
   0427 F0                 1108 	movx	@dptr,a
   0428 22                 1109 	ret
                           1110 
   0429                    1111 usb_sudav_isr:
   0429 C0 E0              1112 	push	acc
   042B C0 F0              1113 	push	b
   042D C0 82              1114 	push	dpl0
   042F C0 83              1115 	push	dph0
   0431 C0 84              1116 	push	dpl1
   0433 C0 85              1117 	push	dph1
   0435 C0 D0              1118 	push	psw
   0437 75 D0 00           1119 	mov	psw,#0x00
   043A C0 86              1120 	push	dps
   043C 75 86 00           1121 	mov	dps,#0
   043F C0 00              1122 	push	ar0
   0441 C0 07              1123 	push	ar7
                           1124 	;; clear interrupt
   0443 E5 91              1125 	mov	a,exif
   0445 C2 E4              1126 	clr	acc.4
   0447 F5 91              1127 	mov	exif,a
   0449 90 7F AB           1128 	mov	dptr,#USBIRQ
   044C 74 01              1129 	mov	a,#0x01
   044E F0                 1130 	movx	@dptr,a
                           1131 	;; handle interrupt
   044F 75 46 00           1132 	mov	ctrlcode,#0		; reset control out code
   0452 90 7F E9           1133 	mov	dptr,#SETUPDAT+1
   0455 E0                 1134 	movx	a,@dptr			; bRequest field
                           1135 	;; vendor specific commands
                           1136 	;; 0xa3
   0456 B4 A3 3E           1137 	cjne	a,#0xa3,cmdnota3
   0459 90 7F EA           1138 	mov	dptr,#SETUPDAT+2
   045C E0                 1139 	movx	a,@dptr
   045D F5 47              1140 	mov	ctrladdr,a
   045F A3                 1141 	inc	dptr
   0460 E0                 1142 	movx	a,@dptr
   0461 F5 48              1143 	mov	ctrladdr+1,a
   0463 24 50              1144 	add	a,#80
   0465 50 2D              1145 	jnc	setupstalla3
   0467 90 7F EE           1146 	mov	dptr,#SETUPDAT+6
   046A E0                 1147 	movx	a,@dptr
   046B F5 49              1148 	mov	ctrllen,a
   046D 25 47              1149 	add	a,ctrladdr
   046F A3                 1150 	inc	dptr
   0470 E0                 1151 	movx	a,@dptr
   0471 F5 4A              1152 	mov	ctrllen+1,a
   0473 35 48              1153 	addc	a,ctrladdr+1
   0475 40 1D              1154 	jc	setupstalla3
   0477 90 7F E8           1155 	mov	dptr,#SETUPDAT		; bRequestType == 0x40
   047A E0                 1156 	movx	a,@dptr
   047B B4 40 0A           1157 	cjne	a,#0x40,1$
   047E 75 46 01           1158 	mov	ctrlcode,#1
   0481 90 7F C5           1159 	mov	dptr,#OUT0BC
   0484 F0                 1160 	movx	@dptr,a
   0485 02 05 7F           1161 	ljmp	endusbisr
   0488 B4 C0 09           1162 1$:	cjne	a,#0xc0,setupstalla3	; bRequestType == 0xc0
   048B 75 46 02           1163 	mov	ctrlcode,#2
   048E 12 03 E1           1164 	lcall	xmemread
   0491 02 05 7F           1165 	ljmp	endusbisr
   0494                    1166 setupstalla3:
   0494 02 05 75           1167 	ljmp	setupstall
   0497                    1168 cmdnota3:
                           1169 	;; 0xb1
   0497 B4 B1 3E           1170 	cjne	a,#0xb1,cmdnotb1
   049A 90 7F E8           1171 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   049D E0                 1172 	movx	a,@dptr
   049E B4 C0 34           1173 	cjne	a,#0xc0,setupstallb1
   04A1 90 7F 00           1174 	mov	dptr,#IN0BUF
   04A4 E5 40              1175 	mov	a,errcode
   04A6 F0                 1176 	movx	@dptr,a
   04A7 A3                 1177 	inc	dptr
   04A8 E5 41              1178 	mov	a,errval
   04AA F0                 1179 	movx	@dptr,a
   04AB A3                 1180 	inc	dptr
   04AC E5 81              1181 	mov	a,sp
                           1182 
                           1183 ;;; xxxx
   04AE C0 82              1184 	push	dpl0
   04B0 C0 83              1185 	push	dph0
   04B2 90 7F DF           1186 	mov	dptr,#OUT07VAL
   04B5 E0                 1187 	movx	a,@dptr
   04B6 D0 83              1188 	pop	dph0
   04B8 D0 82              1189 	pop	dpl0
                           1190 ;;; xxxx
                           1191 	
   04BA F0                 1192 	movx	@dptr,a
   04BB A3                 1193 	inc	dptr
   04BC 74 00              1194 	mov	a,#0
                           1195 
                           1196 ;;; xxxx
   04BE C0 82              1197 	push	dpl0
   04C0 C0 83              1198 	push	dph0
   04C2 90 7F C8           1199 	mov	dptr,#OUT2CS
   04C5 E0                 1200 	movx	a,@dptr
   04C6 D0 83              1201 	pop	dph0
   04C8 D0 82              1202 	pop	dpl0
                           1203 ;;; xxxx
                           1204 	
   04CA F0                 1205 	movx	@dptr,a
   04CB A3                 1206 	inc	dptr
   04CC 90 7F B5           1207 	mov	dptr,#IN0BC
   04CF 74 04              1208 	mov	a,#4
   04D1 F0                 1209 	movx	@dptr,a
   04D2 02 05 79           1210 	ljmp	setupack
   04D5                    1211 setupstallb1:
   04D5 02 05 75           1212 	ljmp	setupstall
   04D8                    1213 cmdnotb1:
                           1214 	;; 0xb2
   04D8 B4 B2 2B           1215 	cjne	a,#0xb2,cmdnotb2
   04DB 90 7F E8           1216 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   04DE E0                 1217 	movx	a,@dptr
   04DF B4 C0 21           1218 	cjne	a,#0xc0,setupstallb2
   04E2 90 7F 00           1219 	mov	dptr,#IN0BUF
   04E5 E5 40              1220 	mov	a,errcode
   04E7 F0                 1221 	movx	@dptr,a
   04E8 A3                 1222 	inc	dptr
   04E9 E5 41              1223 	mov	a,errval
   04EB F0                 1224 	movx	@dptr,a
   04EC A3                 1225 	inc	dptr
   04ED E5 42              1226 	mov	a,cfgcount
   04EF F0                 1227 	movx	@dptr,a
   04F0 A3                 1228 	inc	dptr
   04F1 E5 43              1229 	mov	a,cfgcount+1
   04F3 F0                 1230 	movx	@dptr,a
   04F4 A3                 1231 	inc	dptr
   04F5 E5 45              1232 	mov	a,irqcount
   04F7 F0                 1233 	movx	@dptr,a
   04F8 90 7F B5           1234 	mov	dptr,#IN0BC
   04FB 74 05              1235 	mov	a,#5
   04FD F0                 1236 	movx	@dptr,a
   04FE 05 45              1237 	inc	irqcount
   0500 02 05 79           1238 	ljmp	setupack
   0503                    1239 setupstallb2:
   0503 02 05 75           1240 	ljmp	setupstall
   0506                    1241 cmdnotb2:
                           1242 	;; 0xb3
   0506 B4 B3 2A           1243 	cjne	a,#0xb3,cmdnotb3
   0509 90 7F E8           1244 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   050C E0                 1245 	movx	a,@dptr
   050D B4 C0 20           1246 	cjne	a,#0xc0,setupstallb3
                           1247 	;; read EEPROM
                    0006   1248 	serstrlen = 6
   0510 75 F0 A0           1249 	mov	b,#0xa0		; EEPROM address
   0513 90 01 8B           1250 	mov	dptr,#eepromstraddr
   0516 7F 01              1251 	mov	r7,#1
   0518 12 01 8C           1252 	lcall	writei2c
   051B 70 13              1253 	jnz	setupstallb3
   051D 90 7F 00           1254 	mov	dptr,#IN0BUF
   0520 7F 06              1255 	mov	r7,#serstrlen
   0522 12 01 C7           1256 	lcall	readi2c
   0525 70 09              1257 	jnz	setupstallb3
   0527 74 06              1258 	mov	a,#serstrlen
   0529 90 7F B5           1259 	mov	dptr,#IN0BC
   052C F0                 1260 	movx	@dptr,a
   052D 02 05 79           1261 	ljmp	setupack
   0530                    1262 setupstallb3:
   0530 02 05 75           1263 	ljmp	setupstall
   0533                    1264 cmdnotb3:
                           1265 	;; 0xb4
   0533 B4 B4 3F           1266 	cjne	a,#0xb4,cmdnotb4
   0536 90 7F EF           1267 	mov	dptr,#SETUPDAT+7
   0539 E0                 1268 	movx	a,@dptr
   053A 70 36              1269 	jnz	setupstallb4
   053C 90 7F EE           1270 	mov	dptr,#SETUPDAT+6
   053F E0                 1271 	movx	a,@dptr
   0540 F8                 1272 	mov	r0,a
   0541 FF                 1273 	mov	r7,a
   0542 24 C0              1274 	add	a,#-64
   0544 40 2C              1275 	jc	setupstallb4
   0546 90 7F EC           1276 	mov	dptr,#SETUPDAT+4	; wIndex
   0549 E0                 1277 	movx	a,@dptr
   054A F5 F0              1278 	mov	b,a
   054C F5 47              1279 	mov	ctrladdr,a
   054E 90 7F E8           1280 	mov	dptr,#SETUPDAT
   0551 E0                 1281 	movx	a,@dptr
   0552 B4 40 0A           1282 	cjne	a,#0x40,0$		; bRequestType == 0x40
   0555 75 46 02           1283 	mov	ctrlcode,#2
   0558 90 7F C5           1284 	mov	dptr,#OUT0BC
   055B F0                 1285 	movx	@dptr,a
   055C 02 05 7F           1286 	ljmp	endusbisr
   055F B4 C0 10           1287 0$:	cjne	a,#0xc0,setupstallb4	; bRequestType == 0xc0
   0562 90 7F 00           1288 	mov	dptr,#IN0BUF
   0565 12 01 C7           1289 	lcall	readi2c
   0568 70 08              1290 	jnz	setupstallb4
   056A E8                 1291 	mov	a,r0
   056B 90 7F B5           1292 	mov	dptr,#IN0BC
   056E F0                 1293 	movx	@dptr,a
   056F 02 05 79           1294 	ljmp	setupack
   0572                    1295 setupstallb4:
   0572 02 05 75           1296 	ljmp	setupstall
   0575                    1297 cmdnotb4:
                           1298 	;; unknown commands fall through to setupstall
                           1299 
   0575                    1300 setupstall:
   0575 74 03              1301 	mov	a,#3
   0577 80 02              1302 	sjmp	endsetup
   0579                    1303 setupack:
   0579 74 02              1304 	mov	a,#2
   057B                    1305 endsetup:
   057B 90 7F B4           1306 	mov	dptr,#EP0CS
   057E F0                 1307 	movx	@dptr,a
   057F                    1308 endusbisr:
                           1309 	;; epilogue
   057F D0 07              1310 	pop	ar7
   0581 D0 00              1311 	pop	ar0
   0583 D0 86              1312 	pop	dps
   0585 D0 D0              1313 	pop	psw
   0587 D0 85              1314 	pop	dph1
   0589 D0 84              1315 	pop	dpl1
   058B D0 83              1316 	pop	dph0
   058D D0 82              1317 	pop	dpl0
   058F D0 F0              1318 	pop	b
   0591 D0 E0              1319 	pop	acc
   0593 32                 1320 	reti
                           1321 
   0594                    1322 usb_sof_isr:
   0594 C0 E0              1323 	push	acc
   0596 C0 F0              1324 	push	b
   0598 C0 82              1325 	push	dpl0
   059A C0 83              1326 	push	dph0
   059C C0 D0              1327 	push	psw
   059E 75 D0 00           1328 	mov	psw,#0x00
   05A1 C0 86              1329 	push	dps
   05A3 75 86 00           1330 	mov	dps,#0
                           1331 	;; clear interrupt
   05A6 E5 91              1332 	mov	a,exif
   05A8 C2 E4              1333 	clr	acc.4
   05AA F5 91              1334 	mov	exif,a
   05AC 90 7F AB           1335 	mov	dptr,#USBIRQ
   05AF 74 02              1336 	mov	a,#0x02
   05B1 F0                 1337 	movx	@dptr,a
                           1338 	;; handle interrupt
                           1339 	;; epilogue
   05B2 D0 86              1340 	pop	dps
   05B4 D0 D0              1341 	pop	psw
   05B6 D0 83              1342 	pop	dph0
   05B8 D0 82              1343 	pop	dpl0
   05BA D0 F0              1344 	pop	b
   05BC D0 E0              1345 	pop	acc
   05BE 32                 1346 	reti
                           1347 
                           1348 
   05BF                    1349 usb_sutok_isr:
   05BF C0 E0              1350 	push	acc
   05C1 C0 F0              1351 	push	b
   05C3 C0 82              1352 	push	dpl0
   05C5 C0 83              1353 	push	dph0
   05C7 C0 D0              1354 	push	psw
   05C9 75 D0 00           1355 	mov	psw,#0x00
   05CC C0 86              1356 	push	dps
   05CE 75 86 00           1357 	mov	dps,#0
                           1358 	;; clear interrupt
   05D1 E5 91              1359 	mov	a,exif
   05D3 C2 E4              1360 	clr	acc.4
   05D5 F5 91              1361 	mov	exif,a
   05D7 90 7F AB           1362 	mov	dptr,#USBIRQ
   05DA 74 04              1363 	mov	a,#0x04
   05DC F0                 1364 	movx	@dptr,a
                           1365 	;; handle interrupt
                           1366 	;; epilogue
   05DD D0 86              1367 	pop	dps
   05DF D0 D0              1368 	pop	psw
   05E1 D0 83              1369 	pop	dph0
   05E3 D0 82              1370 	pop	dpl0
   05E5 D0 F0              1371 	pop	b
   05E7 D0 E0              1372 	pop	acc
   05E9 32                 1373 	reti
                           1374 
   05EA                    1375 usb_suspend_isr:
   05EA C0 E0              1376 	push	acc
   05EC C0 F0              1377 	push	b
   05EE C0 82              1378 	push	dpl0
   05F0 C0 83              1379 	push	dph0
   05F2 C0 D0              1380 	push	psw
   05F4 75 D0 00           1381 	mov	psw,#0x00
   05F7 C0 86              1382 	push	dps
   05F9 75 86 00           1383 	mov	dps,#0
                           1384 	;; clear interrupt
   05FC E5 91              1385 	mov	a,exif
   05FE C2 E4              1386 	clr	acc.4
   0600 F5 91              1387 	mov	exif,a
   0602 90 7F AB           1388 	mov	dptr,#USBIRQ
   0605 74 08              1389 	mov	a,#0x08
   0607 F0                 1390 	movx	@dptr,a
                           1391 	;; handle interrupt
                           1392 	;; epilogue
   0608 D0 86              1393 	pop	dps
   060A D0 D0              1394 	pop	psw
   060C D0 83              1395 	pop	dph0
   060E D0 82              1396 	pop	dpl0
   0610 D0 F0              1397 	pop	b
   0612 D0 E0              1398 	pop	acc
   0614 32                 1399 	reti
                           1400 
   0615                    1401 usb_usbreset_isr:
   0615 C0 E0              1402 	push	acc
   0617 C0 F0              1403 	push	b
   0619 C0 82              1404 	push	dpl0
   061B C0 83              1405 	push	dph0
   061D C0 D0              1406 	push	psw
   061F 75 D0 00           1407 	mov	psw,#0x00
   0622 C0 86              1408 	push	dps
   0624 75 86 00           1409 	mov	dps,#0
                           1410 	;; clear interrupt
   0627 E5 91              1411 	mov	a,exif
   0629 C2 E4              1412 	clr	acc.4
   062B F5 91              1413 	mov	exif,a
   062D 90 7F AB           1414 	mov	dptr,#USBIRQ
   0630 74 10              1415 	mov	a,#0x10
   0632 F0                 1416 	movx	@dptr,a
                           1417 	;; handle interrupt
                           1418 	;; epilogue
   0633 D0 86              1419 	pop	dps
   0635 D0 D0              1420 	pop	psw
   0637 D0 83              1421 	pop	dph0
   0639 D0 82              1422 	pop	dpl0
   063B D0 F0              1423 	pop	b
   063D D0 E0              1424 	pop	acc
   063F 32                 1425 	reti
                           1426 
   0640                    1427 usb_ep0in_isr:
   0640 C0 E0              1428 	push	acc
   0642 C0 F0              1429 	push	b
   0644 C0 82              1430 	push	dpl0
   0646 C0 83              1431 	push	dph0
   0648 C0 84              1432 	push	dpl1
   064A C0 85              1433 	push	dph1
   064C C0 D0              1434 	push	psw
   064E 75 D0 00           1435 	mov	psw,#0x00
   0651 C0 86              1436 	push	dps
   0653 75 86 00           1437 	mov	dps,#0
   0656 C0 00              1438 	push	ar0
   0658 C0 07              1439 	push	ar7
                           1440 	;; clear interrupt
   065A E5 91              1441 	mov	a,exif
   065C C2 E4              1442 	clr	acc.4
   065E F5 91              1443 	mov	exif,a
   0660 90 7F A9           1444 	mov	dptr,#IN07IRQ
   0663 74 01              1445 	mov	a,#0x01
   0665 F0                 1446 	movx	@dptr,a
                           1447 	;; handle interrupt
   0666 E5 46              1448 	mov	a,ctrlcode
   0668 B4 02 05           1449 	cjne	a,#2,0$
   066B 12 03 E1           1450 	lcall	xmemread
   066E 80 11              1451 	sjmp	ep0inendisr
   0670 90 7F B5           1452 0$:	mov	dptr,#IN0BC
   0673 E4                 1453 	clr	a
   0674 F0                 1454 	movx	@dptr,a
   0675 80 04              1455 	sjmp	ep0inack
                           1456 
   0677                    1457 ep0install:
   0677 74 03              1458 	mov	a,#3
   0679 80 02              1459 	sjmp	ep0incs
   067B                    1460 ep0inack:
   067B 74 02              1461 	mov	a,#2
   067D                    1462 ep0incs:
   067D 90 7F B4           1463 	mov	dptr,#EP0CS
   0680 F0                 1464 	movx	@dptr,a
   0681                    1465 ep0inendisr:
                           1466 	;; epilogue
   0681 D0 07              1467 	pop	ar7
   0683 D0 00              1468 	pop	ar0
   0685 D0 86              1469 	pop	dps
   0687 D0 D0              1470 	pop	psw
   0689 D0 85              1471 	pop	dph1
   068B D0 84              1472 	pop	dpl1
   068D D0 83              1473 	pop	dph0
   068F D0 82              1474 	pop	dpl0
   0691 D0 F0              1475 	pop	b
   0693 D0 E0              1476 	pop	acc
   0695 32                 1477 	reti
                           1478 
   0696                    1479 usb_ep0out_isr:
   0696 C0 E0              1480 	push	acc
   0698 C0 F0              1481 	push	b
   069A C0 82              1482 	push	dpl0
   069C C0 83              1483 	push	dph0
   069E C0 84              1484 	push	dpl1
   06A0 C0 85              1485 	push	dph1
   06A2 C0 D0              1486 	push	psw
   06A4 75 D0 00           1487 	mov	psw,#0x00
   06A7 C0 86              1488 	push	dps
   06A9 75 86 00           1489 	mov	dps,#0
   06AC C0 07              1490 	push	ar7
                           1491 	;; clear interrupt
   06AE E5 91              1492 	mov	a,exif
   06B0 C2 E4              1493 	clr	acc.4
   06B2 F5 91              1494 	mov	exif,a
   06B4 90 7F AA           1495 	mov	dptr,#OUT07IRQ
   06B7 74 01              1496 	mov	a,#0x01
   06B9 F0                 1497 	movx	@dptr,a
                           1498 	;; handle interrupt
   06BA E5 46              1499 	mov	a,ctrlcode		; check control code
   06BC B4 01 36           1500 	cjne	a,#0x01,i2cwr
                           1501 	;; write to external memory
   06BF 90 7F C5           1502 	mov	dptr,#OUT0BC
   06C2 E0                 1503 	movx	a,@dptr
   06C3 60 28              1504 	jz	0$
   06C5 FF                 1505 	mov	r7,a
   06C6 C3                 1506 	clr	c
   06C7 E5 49              1507 	mov	a,ctrllen
   06C9 9F                 1508 	subb	a,r7
   06CA F5 49              1509 	mov	ctrllen,a
   06CC E5 4A              1510 	mov	a,ctrllen+1
   06CE 94 00              1511 	subb	a,#0
   06D0 F5 4A              1512 	mov	ctrllen+1,a
   06D2 40 38              1513 	jc	ep0outstall
   06D4 90 7E C0           1514 	mov	dptr,#OUT0BUF
   06D7 85 47 84           1515 	mov	dpl1,ctrladdr
   06DA 85 48 85           1516 	mov	dph1,ctrladdr+1
   06DD E0                 1517 1$:	movx	a,@dptr
   06DE A3                 1518 	inc	dptr
   06DF 05 86              1519 	inc	dps
   06E1 F0                 1520 	movx	@dptr,a
   06E2 A3                 1521 	inc	dptr
   06E3 15 86              1522 	dec	dps
   06E5 DF F6              1523 	djnz	r7,1$
   06E7 85 84 47           1524 	mov	ctrladdr,dpl1
   06EA 85 85 48           1525 	mov	ctrladdr+1,dph1
   06ED E5 49              1526 0$:	mov	a,ctrllen
   06EF 45 4A              1527 	orl	a,ctrllen+1
   06F1 60 20              1528 	jz	ep0outack
   06F3 80 27              1529 	sjmp	ep0outendisr
                           1530 
                           1531 	;; write I2C eeprom
   06F5 B4 02 14           1532 i2cwr:	cjne	a,#0x02,ep0outstall
   06F8 90 7F C5           1533 	mov	dptr,#OUT0BC
   06FB E0                 1534 	movx	a,@dptr
   06FC 60 15              1535 	jz	ep0outack
   06FE FF                 1536 	mov	r7,a
   06FF 85 47 F0           1537 	mov	b,ctrladdr
   0702 90 7E C0           1538 	mov	dptr,#OUT0BUF
   0705 12 01 8C           1539 	lcall	writei2c
   0708 70 02              1540 	jnz	ep0outstall
   070A 80 07              1541 	sjmp	ep0outack
                           1542 
   070C                    1543 ep0outstall:
   070C 75 46 00           1544 	mov	ctrlcode,#0
   070F 74 03              1545 	mov	a,#3
   0711 80 05              1546 	sjmp	ep0outcs
   0713                    1547 ep0outack:
   0713 75 46 00           1548 	mov	ctrlcode,#0
   0716 74 02              1549 	mov	a,#2
   0718                    1550 ep0outcs:
   0718 90 7F B4           1551 	mov	dptr,#EP0CS
   071B F0                 1552 	movx	@dptr,a
   071C                    1553 ep0outendisr:
                           1554 	;; epilogue
   071C D0 07              1555 	pop	ar7
   071E D0 86              1556 	pop	dps
   0720 D0 D0              1557 	pop	psw
   0722 D0 85              1558 	pop	dph1
   0724 D0 84              1559 	pop	dpl1
   0726 D0 83              1560 	pop	dph0
   0728 D0 82              1561 	pop	dpl0
   072A D0 F0              1562 	pop	b
   072C D0 E0              1563 	pop	acc
   072E 32                 1564 	reti
                           1565 
   072F                    1566 fillusbintr::
   072F 90 7E 80           1567 	mov	dptr,#IN1BUF
   0732 E5 40              1568 	mov	a,errcode
   0734 F0                 1569 	movx	@dptr,a
   0735 A3                 1570 	inc	dptr
   0736 E5 41              1571 	mov	a,errval
   0738 F0                 1572 	movx	@dptr,a
   0739 A3                 1573 	inc	dptr
   073A E5 42              1574 	mov	a,cfgcount
   073C F0                 1575 	movx	@dptr,a
   073D A3                 1576 	inc	dptr
   073E E5 43              1577 	mov	a,cfgcount+1
   0740 F0                 1578 	movx	@dptr,a
   0741 A3                 1579 	inc	dptr
   0742 E5 45              1580 	mov	a,irqcount
   0744 F0                 1581 	movx	@dptr,a
   0745 90 7F B7           1582 	mov	dptr,#IN1BC
   0748 74 05              1583 	mov	a,#5
   074A F0                 1584 	movx	@dptr,a
   074B 05 45              1585 	inc	irqcount
   074D 22                 1586 	ret
                           1587 
   074E                    1588 usb_ep1in_isr:
   074E C0 E0              1589 	push	acc
   0750 C0 F0              1590 	push	b
   0752 C0 82              1591 	push	dpl0
   0754 C0 83              1592 	push	dph0
   0756 C0 D0              1593 	push	psw
   0758 75 D0 00           1594 	mov	psw,#0x00
   075B C0 86              1595 	push	dps
   075D 75 86 00           1596 	mov	dps,#0
                           1597 	;; clear interrupt
   0760 E5 91              1598 	mov	a,exif
   0762 C2 E4              1599 	clr	acc.4
   0764 F5 91              1600 	mov	exif,a
   0766 90 7F A9           1601 	mov	dptr,#IN07IRQ
   0769 74 02              1602 	mov	a,#0x02
   076B F0                 1603 	movx	@dptr,a
                           1604 	;; handle interrupt
   076C 12 07 2F           1605 	lcall	fillusbintr
                           1606 	;; epilogue
   076F D0 86              1607 	pop	dps
   0771 D0 D0              1608 	pop	psw
   0773 D0 83              1609 	pop	dph0
   0775 D0 82              1610 	pop	dpl0
   0777 D0 F0              1611 	pop	b
   0779 D0 E0              1612 	pop	acc
   077B 32                 1613 	reti
                           1614 
   077C                    1615 usb_ep1out_isr:
   077C C0 E0              1616 	push	acc
   077E C0 F0              1617 	push	b
   0780 C0 82              1618 	push	dpl0
   0782 C0 83              1619 	push	dph0
   0784 C0 D0              1620 	push	psw
   0786 75 D0 00           1621 	mov	psw,#0x00
   0789 C0 86              1622 	push	dps
   078B 75 86 00           1623 	mov	dps,#0
                           1624 	;; clear interrupt
   078E E5 91              1625 	mov	a,exif
   0790 C2 E4              1626 	clr	acc.4
   0792 F5 91              1627 	mov	exif,a
   0794 90 7F AA           1628 	mov	dptr,#OUT07IRQ
   0797 74 02              1629 	mov	a,#0x02
   0799 F0                 1630 	movx	@dptr,a
                           1631 	;; handle interrupt
                           1632 	;; epilogue
   079A D0 86              1633 	pop	dps
   079C D0 D0              1634 	pop	psw
   079E D0 83              1635 	pop	dph0
   07A0 D0 82              1636 	pop	dpl0
   07A2 D0 F0              1637 	pop	b
   07A4 D0 E0              1638 	pop	acc
   07A6 32                 1639 	reti
                           1640 
   07A7                    1641 usb_ep2in_isr:
   07A7 C0 E0              1642 	push	acc
   07A9 C0 F0              1643 	push	b
   07AB C0 82              1644 	push	dpl0
   07AD C0 83              1645 	push	dph0
   07AF C0 D0              1646 	push	psw
   07B1 75 D0 00           1647 	mov	psw,#0x00
   07B4 C0 86              1648 	push	dps
   07B6 75 86 00           1649 	mov	dps,#0
                           1650 	;; clear interrupt
   07B9 E5 91              1651 	mov	a,exif
   07BB C2 E4              1652 	clr	acc.4
   07BD F5 91              1653 	mov	exif,a
   07BF 90 7F A9           1654 	mov	dptr,#IN07IRQ
   07C2 74 04              1655 	mov	a,#0x04
   07C4 F0                 1656 	movx	@dptr,a
                           1657 	;; handle interrupt
                           1658 	;; epilogue
   07C5 D0 86              1659 	pop	dps
   07C7 D0 D0              1660 	pop	psw
   07C9 D0 83              1661 	pop	dph0
   07CB D0 82              1662 	pop	dpl0
   07CD D0 F0              1663 	pop	b
   07CF D0 E0              1664 	pop	acc
   07D1 32                 1665 	reti
                           1666 
   07D2                    1667 usb_ep2out_isr:
   07D2 C0 E0              1668 	push	acc
   07D4 C0 F0              1669 	push	b
   07D6 C0 82              1670 	push	dpl0
   07D8 C0 83              1671 	push	dph0
   07DA C0 D0              1672 	push	psw
   07DC 75 D0 00           1673 	mov	psw,#0x00
   07DF C0 86              1674 	push	dps
   07E1 75 86 00           1675 	mov	dps,#0
                           1676 	;; clear interrupt
   07E4 E5 91              1677 	mov	a,exif
   07E6 C2 E4              1678 	clr	acc.4
   07E8 F5 91              1679 	mov	exif,a
   07EA 90 7F AA           1680 	mov	dptr,#OUT07IRQ
   07ED 74 04              1681 	mov	a,#0x04
   07EF F0                 1682 	movx	@dptr,a
                           1683 	;; handle interrupt
                           1684 	;; epilogue
   07F0 D0 86              1685 	pop	dps
   07F2 D0 D0              1686 	pop	psw
   07F4 D0 83              1687 	pop	dph0
   07F6 D0 82              1688 	pop	dpl0
   07F8 D0 F0              1689 	pop	b
   07FA D0 E0              1690 	pop	acc
   07FC 32                 1691 	reti
                           1692 
   07FD                    1693 usb_ep3in_isr:
   07FD C0 E0              1694 	push	acc
   07FF C0 F0              1695 	push	b
   0801 C0 82              1696 	push	dpl0
   0803 C0 83              1697 	push	dph0
   0805 C0 D0              1698 	push	psw
   0807 75 D0 00           1699 	mov	psw,#0x00
   080A C0 86              1700 	push	dps
   080C 75 86 00           1701 	mov	dps,#0
                           1702 	;; clear interrupt
   080F E5 91              1703 	mov	a,exif
   0811 C2 E4              1704 	clr	acc.4
   0813 F5 91              1705 	mov	exif,a
   0815 90 7F A9           1706 	mov	dptr,#IN07IRQ
   0818 74 08              1707 	mov	a,#0x08
   081A F0                 1708 	movx	@dptr,a
                           1709 	;; handle interrupt
                           1710 	;; epilogue
   081B D0 86              1711 	pop	dps
   081D D0 D0              1712 	pop	psw
   081F D0 83              1713 	pop	dph0
   0821 D0 82              1714 	pop	dpl0
   0823 D0 F0              1715 	pop	b
   0825 D0 E0              1716 	pop	acc
   0827 32                 1717 	reti
                           1718 
   0828                    1719 usb_ep3out_isr:
   0828 C0 E0              1720 	push	acc
   082A C0 F0              1721 	push	b
   082C C0 82              1722 	push	dpl0
   082E C0 83              1723 	push	dph0
   0830 C0 D0              1724 	push	psw
   0832 75 D0 00           1725 	mov	psw,#0x00
   0835 C0 86              1726 	push	dps
   0837 75 86 00           1727 	mov	dps,#0
                           1728 	;; clear interrupt
   083A E5 91              1729 	mov	a,exif
   083C C2 E4              1730 	clr	acc.4
   083E F5 91              1731 	mov	exif,a
   0840 90 7F AA           1732 	mov	dptr,#OUT07IRQ
   0843 74 08              1733 	mov	a,#0x08
   0845 F0                 1734 	movx	@dptr,a
                           1735 	;; handle interrupt
                           1736 	;; epilogue
   0846 D0 86              1737 	pop	dps
   0848 D0 D0              1738 	pop	psw
   084A D0 83              1739 	pop	dph0
   084C D0 82              1740 	pop	dpl0
   084E D0 F0              1741 	pop	b
   0850 D0 E0              1742 	pop	acc
   0852 32                 1743 	reti
                           1744 
   0853                    1745 usb_ep4in_isr:
   0853 C0 E0              1746 	push	acc
   0855 C0 F0              1747 	push	b
   0857 C0 82              1748 	push	dpl0
   0859 C0 83              1749 	push	dph0
   085B C0 D0              1750 	push	psw
   085D 75 D0 00           1751 	mov	psw,#0x00
   0860 C0 86              1752 	push	dps
   0862 75 86 00           1753 	mov	dps,#0
                           1754 	;; clear interrupt
   0865 E5 91              1755 	mov	a,exif
   0867 C2 E4              1756 	clr	acc.4
   0869 F5 91              1757 	mov	exif,a
   086B 90 7F A9           1758 	mov	dptr,#IN07IRQ
   086E 74 10              1759 	mov	a,#0x10
   0870 F0                 1760 	movx	@dptr,a
                           1761 	;; handle interrupt
                           1762 	;; epilogue
   0871 D0 86              1763 	pop	dps
   0873 D0 D0              1764 	pop	psw
   0875 D0 83              1765 	pop	dph0
   0877 D0 82              1766 	pop	dpl0
   0879 D0 F0              1767 	pop	b
   087B D0 E0              1768 	pop	acc
   087D 32                 1769 	reti
                           1770 
   087E                    1771 usb_ep4out_isr:
   087E C0 E0              1772 	push	acc
   0880 C0 F0              1773 	push	b
   0882 C0 82              1774 	push	dpl0
   0884 C0 83              1775 	push	dph0
   0886 C0 D0              1776 	push	psw
   0888 75 D0 00           1777 	mov	psw,#0x00
   088B C0 86              1778 	push	dps
   088D 75 86 00           1779 	mov	dps,#0
                           1780 	;; clear interrupt
   0890 E5 91              1781 	mov	a,exif
   0892 C2 E4              1782 	clr	acc.4
   0894 F5 91              1783 	mov	exif,a
   0896 90 7F AA           1784 	mov	dptr,#OUT07IRQ
   0899 74 10              1785 	mov	a,#0x10
   089B F0                 1786 	movx	@dptr,a
                           1787 	;; handle interrupt
                           1788 	;; epilogue
   089C D0 86              1789 	pop	dps
   089E D0 D0              1790 	pop	psw
   08A0 D0 83              1791 	pop	dph0
   08A2 D0 82              1792 	pop	dpl0
   08A4 D0 F0              1793 	pop	b
   08A6 D0 E0              1794 	pop	acc
   08A8 32                 1795 	reti
                           1796 
   08A9                    1797 usb_ep5in_isr:
   08A9 C0 E0              1798 	push	acc
   08AB C0 F0              1799 	push	b
   08AD C0 82              1800 	push	dpl0
   08AF C0 83              1801 	push	dph0
   08B1 C0 D0              1802 	push	psw
   08B3 75 D0 00           1803 	mov	psw,#0x00
   08B6 C0 86              1804 	push	dps
   08B8 75 86 00           1805 	mov	dps,#0
                           1806 	;; clear interrupt
   08BB E5 91              1807 	mov	a,exif
   08BD C2 E4              1808 	clr	acc.4
   08BF F5 91              1809 	mov	exif,a
   08C1 90 7F A9           1810 	mov	dptr,#IN07IRQ
   08C4 74 20              1811 	mov	a,#0x20
   08C6 F0                 1812 	movx	@dptr,a
                           1813 	;; handle interrupt
                           1814 	;; epilogue
   08C7 D0 86              1815 	pop	dps
   08C9 D0 D0              1816 	pop	psw
   08CB D0 83              1817 	pop	dph0
   08CD D0 82              1818 	pop	dpl0
   08CF D0 F0              1819 	pop	b
   08D1 D0 E0              1820 	pop	acc
   08D3 32                 1821 	reti
                           1822 
   08D4                    1823 usb_ep5out_isr:
   08D4 C0 E0              1824 	push	acc
   08D6 C0 F0              1825 	push	b
   08D8 C0 82              1826 	push	dpl0
   08DA C0 83              1827 	push	dph0
   08DC C0 D0              1828 	push	psw
   08DE 75 D0 00           1829 	mov	psw,#0x00
   08E1 C0 86              1830 	push	dps
   08E3 75 86 00           1831 	mov	dps,#0
                           1832 	;; clear interrupt
   08E6 E5 91              1833 	mov	a,exif
   08E8 C2 E4              1834 	clr	acc.4
   08EA F5 91              1835 	mov	exif,a
   08EC 90 7F AA           1836 	mov	dptr,#OUT07IRQ
   08EF 74 20              1837 	mov	a,#0x20
   08F1 F0                 1838 	movx	@dptr,a
                           1839 	;; handle interrupt
                           1840 	;; epilogue
   08F2 D0 86              1841 	pop	dps
   08F4 D0 D0              1842 	pop	psw
   08F6 D0 83              1843 	pop	dph0
   08F8 D0 82              1844 	pop	dpl0
   08FA D0 F0              1845 	pop	b
   08FC D0 E0              1846 	pop	acc
   08FE 32                 1847 	reti
                           1848 
   08FF                    1849 usb_ep6in_isr:
   08FF C0 E0              1850 	push	acc
   0901 C0 F0              1851 	push	b
   0903 C0 82              1852 	push	dpl0
   0905 C0 83              1853 	push	dph0
   0907 C0 D0              1854 	push	psw
   0909 75 D0 00           1855 	mov	psw,#0x00
   090C C0 86              1856 	push	dps
   090E 75 86 00           1857 	mov	dps,#0
                           1858 	;; clear interrupt
   0911 E5 91              1859 	mov	a,exif
   0913 C2 E4              1860 	clr	acc.4
   0915 F5 91              1861 	mov	exif,a
   0917 90 7F A9           1862 	mov	dptr,#IN07IRQ
   091A 74 40              1863 	mov	a,#0x40
   091C F0                 1864 	movx	@dptr,a
                           1865 	;; handle interrupt
                           1866 	;; epilogue
   091D D0 86              1867 	pop	dps
   091F D0 D0              1868 	pop	psw
   0921 D0 83              1869 	pop	dph0
   0923 D0 82              1870 	pop	dpl0
   0925 D0 F0              1871 	pop	b
   0927 D0 E0              1872 	pop	acc
   0929 32                 1873 	reti
                           1874 
   092A                    1875 usb_ep6out_isr:
   092A C0 E0              1876 	push	acc
   092C C0 F0              1877 	push	b
   092E C0 82              1878 	push	dpl0
   0930 C0 83              1879 	push	dph0
   0932 C0 D0              1880 	push	psw
   0934 75 D0 00           1881 	mov	psw,#0x00
   0937 C0 86              1882 	push	dps
   0939 75 86 00           1883 	mov	dps,#0
                           1884 	;; clear interrupt
   093C E5 91              1885 	mov	a,exif
   093E C2 E4              1886 	clr	acc.4
   0940 F5 91              1887 	mov	exif,a
   0942 90 7F AA           1888 	mov	dptr,#OUT07IRQ
   0945 74 40              1889 	mov	a,#0x40
   0947 F0                 1890 	movx	@dptr,a
                           1891 	;; handle interrupt
                           1892 	;; epilogue
   0948 D0 86              1893 	pop	dps
   094A D0 D0              1894 	pop	psw
   094C D0 83              1895 	pop	dph0
   094E D0 82              1896 	pop	dpl0
   0950 D0 F0              1897 	pop	b
   0952 D0 E0              1898 	pop	acc
   0954 32                 1899 	reti
                           1900 
   0955                    1901 usb_ep7in_isr:
   0955 C0 E0              1902 	push	acc
   0957 C0 F0              1903 	push	b
   0959 C0 82              1904 	push	dpl0
   095B C0 83              1905 	push	dph0
   095D C0 D0              1906 	push	psw
   095F 75 D0 00           1907 	mov	psw,#0x00
   0962 C0 86              1908 	push	dps
   0964 75 86 00           1909 	mov	dps,#0
                           1910 	;; clear interrupt
   0967 E5 91              1911 	mov	a,exif
   0969 C2 E4              1912 	clr	acc.4
   096B F5 91              1913 	mov	exif,a
   096D 90 7F A9           1914 	mov	dptr,#IN07IRQ
   0970 74 80              1915 	mov	a,#0x80
   0972 F0                 1916 	movx	@dptr,a
                           1917 	;; handle interrupt
                           1918 	;; epilogue
   0973 D0 86              1919 	pop	dps
   0975 D0 D0              1920 	pop	psw
   0977 D0 83              1921 	pop	dph0
   0979 D0 82              1922 	pop	dpl0
   097B D0 F0              1923 	pop	b
   097D D0 E0              1924 	pop	acc
   097F 32                 1925 	reti
                           1926 
   0980                    1927 usb_ep7out_isr:
   0980 C0 E0              1928 	push	acc
   0982 C0 F0              1929 	push	b
   0984 C0 82              1930 	push	dpl0
   0986 C0 83              1931 	push	dph0
   0988 C0 D0              1932 	push	psw
   098A 75 D0 00           1933 	mov	psw,#0x00
   098D C0 86              1934 	push	dps
   098F 75 86 00           1935 	mov	dps,#0
                           1936 	;; clear interrupt
   0992 E5 91              1937 	mov	a,exif
   0994 C2 E4              1938 	clr	acc.4
   0996 F5 91              1939 	mov	exif,a
   0998 90 7F AA           1940 	mov	dptr,#OUT07IRQ
   099B 74 80              1941 	mov	a,#0x80
   099D F0                 1942 	movx	@dptr,a
                           1943 	;; handle interrupt
                           1944 	;; epilogue
   099E D0 86              1945 	pop	dps
   09A0 D0 D0              1946 	pop	psw
   09A2 D0 83              1947 	pop	dph0
   09A4 D0 82              1948 	pop	dpl0
   09A6 D0 F0              1949 	pop	b
   09A8 D0 E0              1950 	pop	acc
   09AA 32                 1951 	reti
