                              1 	.module main
                              2 
                              3 	;; ENDPOINTS
                              4 	;; EP0 in/out   Control
                              5 	;; EP1 in       Interrupt:  Status
                              6 	;;              Byte 0: Modem Status
                              7 	;;                Bit 0-1: Transmitter status
                              8 	;;                         0: idle (off)
                              9 	;;                         1: keyup
                             10 	;;                         2: transmitting packets
                             11 	;;                         3: tail
                             12 	;;                Bit 2:   PTT status (1=on)
                             13 	;;                Bit 3:   DCD
                             14 	;;                Bit 5:   UART transmitter empty
                             15 	;;                Bit 6-7: unused
                             16 	;;              Byte 1: Number of empty 64 byte chunks in TX fifo (sofcount)
                             17 	;;              Byte 2: Number of full 64 byte chunks in RX fifo (INISOVAL)
                             18 	;;              Byte 3: RSSI value
                             19 	;;              Byte 4:	IRQ count
                             20 	;;              Byte 5-20: (as needed) UART receiver chars
                             21 	;; EP8 out      audio output
                             22 	;; EP8 in       audio input
                             23 
                             24 	;; COMMAND LIST
                             25 	;; C0 C0  read status (max. 23 bytes, first 6 same as EP1 in)
                             26 	;; C0 C8  read mode
                             27 	;;          Return:
                             28 	;;            Byte 0: 4 (MODE_AUDIO)
                             29 	;; C0 C9  return serial number string
                             30 	;; C0 D0  get/set PTT/DCD/RSSI
                             31 	;;          wIndex = 1:	set forced ptt to wValue
                             32 	;;          Return:
                             33 	;;            Byte 0: PTT status
                             34 	;;            Byte 1: DCD status
                             35 	;;            Byte 2: RSSI status
                             36 	;; 40 D2  set CON/STA led
                             37 	;;          Bits 0-1 of wValue
                             38 	;; 40 D3  send byte to UART
                             39 	;;          Byte in wValue
                             40 	;; C0 D4  get/set modem disconnect port (only if internal modem used, stalls otherwise)
                             41 	;;          wIndex = 1:	write wValue to output register
                             42 	;;          wIndex = 2:	write wValue to tristate mask register (1 = input, 0 = output)
                             43 	;;          Return:
                             44 	;;            Byte 0: Modem Disconnect Input
                             45 	;;            Byte 1: Modem Disconnect Output register
                             46 	;;            Byte 2: Modem Disconnect Tristate register
                             47 	;; C0 D5  get/set T7F port
                             48 	;;          wIndex = 1:	write wValue to T7F output register
                             49 	;;          Return:
                             50 	;;            Byte 0: T7F Input
                             51 	;;            Byte 1: T7F Output register
                             52 	;; C0 E0  get/set control register/counter values
                             53 	;;          wIndex = 1:	write wValue to control register
                             54 	;;          Return:
                             55 	;;            Byte 0: control register value
                             56 	;;            Byte 1-3: counter0 register
                             57 	;;            Byte 4-6: counter1 register
                             58 	;; C0 E1  get debug status
                             59 	;;          Return:
                             60 	;;            Byte 0: SOF count
                             61 	;;            Byte 1: framesize
                             62 	;;            Byte 2-3: divider
                             63 	;;            Byte 4: divrel
                             64 	;;            Byte 5: pllcorrvar
                             65 	;;            Byte 6: txfifocount
                             66 	;; C0 E2  xxdebug stuff
                             67 	
                             68 	;; define code segments link order
                             69 	.area CODE (CODE)
                             70 	.area CSEG (CODE)
                             71 	.area GSINIT (CODE)
                             72 	.area GSINIT2 (CODE)
                             73 
                             74 	;; -----------------------------------------------------
                             75 
                             76 	;; special function registers (which are not predefined)
                    0082     77 	dpl0    = 0x82
                    0083     78 	dph0    = 0x83
                    0084     79 	dpl1    = 0x84
                    0085     80 	dph1    = 0x85
                    0086     81 	dps     = 0x86
                    008E     82 	ckcon   = 0x8E
                    008F     83 	spc_fnc = 0x8F
                    0091     84 	exif    = 0x91
                    0092     85 	mpage   = 0x92
                    0098     86 	scon0   = 0x98
                    0099     87 	sbuf0   = 0x99
                    00C0     88 	scon1   = 0xC0
                    00C1     89 	sbuf1   = 0xC1
                    00D8     90 	eicon   = 0xD8
                    00E8     91 	eie     = 0xE8
                    00F8     92 	eip     = 0xF8
                             93 
                             94 	;; anchor xdata registers
                    7F00     95 	IN0BUF		= 0x7F00
                    7EC0     96 	OUT0BUF		= 0x7EC0
                    7E80     97 	IN1BUF		= 0x7E80
                    7E40     98 	OUT1BUF		= 0x7E40
                    7E00     99 	IN2BUF		= 0x7E00
                    7DC0    100 	OUT2BUF		= 0x7DC0
                    7D80    101 	IN3BUF		= 0x7D80
                    7D40    102 	OUT3BUF		= 0x7D40
                    7D00    103 	IN4BUF		= 0x7D00
                    7CC0    104 	OUT4BUF		= 0x7CC0
                    7C80    105 	IN5BUF		= 0x7C80
                    7C40    106 	OUT5BUF		= 0x7C40
                    7C00    107 	IN6BUF		= 0x7C00
                    7BC0    108 	OUT6BUF		= 0x7BC0
                    7B80    109 	IN7BUF		= 0x7B80
                    7B40    110 	OUT7BUF		= 0x7B40
                    7FE8    111 	SETUPBUF	= 0x7FE8
                    7FE8    112 	SETUPDAT	= 0x7FE8
                            113 
                    7FB4    114 	EP0CS		= 0x7FB4
                    7FB5    115 	IN0BC		= 0x7FB5
                    7FB6    116 	IN1CS		= 0x7FB6
                    7FB7    117 	IN1BC		= 0x7FB7
                    7FB8    118 	IN2CS		= 0x7FB8
                    7FB9    119 	IN2BC		= 0x7FB9
                    7FBA    120 	IN3CS		= 0x7FBA
                    7FBB    121 	IN3BC		= 0x7FBB
                    7FBC    122 	IN4CS		= 0x7FBC
                    7FBD    123 	IN4BC		= 0x7FBD
                    7FBE    124 	IN5CS		= 0x7FBE
                    7FBF    125 	IN5BC		= 0x7FBF
                    7FC0    126 	IN6CS		= 0x7FC0
                    7FC1    127 	IN6BC		= 0x7FC1
                    7FC2    128 	IN7CS		= 0x7FC2
                    7FC3    129 	IN7BC		= 0x7FC3
                    7FC5    130 	OUT0BC		= 0x7FC5
                    7FC6    131 	OUT1CS		= 0x7FC6
                    7FC7    132 	OUT1BC		= 0x7FC7
                    7FC8    133 	OUT2CS		= 0x7FC8
                    7FC9    134 	OUT2BC		= 0x7FC9
                    7FCA    135 	OUT3CS		= 0x7FCA
                    7FCB    136 	OUT3BC		= 0x7FCB
                    7FCC    137 	OUT4CS		= 0x7FCC
                    7FCD    138 	OUT4BC		= 0x7FCD
                    7FCE    139 	OUT5CS		= 0x7FCE
                    7FCF    140 	OUT5BC		= 0x7FCF
                    7FD0    141 	OUT6CS		= 0x7FD0
                    7FD1    142 	OUT6BC		= 0x7FD1
                    7FD2    143 	OUT7CS		= 0x7FD2
                    7FD3    144 	OUT7BC		= 0x7FD3
                            145 
                    7FA8    146 	IVEC		= 0x7FA8
                    7FA9    147 	IN07IRQ		= 0x7FA9
                    7FAA    148 	OUT07IRQ	= 0x7FAA
                    7FAB    149 	USBIRQ		= 0x7FAB
                    7FAC    150 	IN07IEN		= 0x7FAC
                    7FAD    151 	OUT07IEN	= 0x7FAD
                    7FAE    152 	USBIEN		= 0x7FAE
                    7FAF    153 	USBBAV		= 0x7FAF
                    7FB2    154 	BPADDRH		= 0x7FB2
                    7FB3    155 	BPADDRL		= 0x7FB3
                            156 
                    7FD4    157 	SUDPTRH		= 0x7FD4
                    7FD5    158 	SUDPTRL		= 0x7FD5
                    7FD6    159 	USBCS		= 0x7FD6
                    7FD7    160 	TOGCTL		= 0x7FD7
                    7FD8    161 	USBFRAMEL	= 0x7FD8
                    7FD9    162 	USBFRAMEH	= 0x7FD9
                    7FDB    163 	FNADDR		= 0x7FDB
                    7FDD    164 	USBPAIR		= 0x7FDD
                    7FDE    165 	IN07VAL		= 0x7FDE
                    7FDF    166 	OUT07VAL	= 0x7FDF
                    7FE3    167 	AUTOPTRH	= 0x7FE3
                    7FE4    168 	AUTOPTRL	= 0x7FE4
                    7FE5    169 	AUTODATA	= 0x7FE5
                            170 
                            171 	;; isochronous endpoints. only available if ISODISAB=0
                            172 
                    7F60    173 	OUT8DATA	= 0x7F60
                    7F61    174 	OUT9DATA	= 0x7F61
                    7F62    175 	OUT10DATA	= 0x7F62
                    7F63    176 	OUT11DATA	= 0x7F63
                    7F64    177 	OUT12DATA	= 0x7F64
                    7F65    178 	OUT13DATA	= 0x7F65
                    7F66    179 	OUT14DATA	= 0x7F66
                    7F67    180 	OUT15DATA	= 0x7F67
                            181 
                    7F68    182 	IN8DATA		= 0x7F68
                    7F69    183 	IN9DATA		= 0x7F69
                    7F6A    184 	IN10DATA	= 0x7F6A
                    7F6B    185 	IN11DATA	= 0x7F6B
                    7F6C    186 	IN12DATA	= 0x7F6C
                    7F6D    187 	IN13DATA	= 0x7F6D
                    7F6E    188 	IN14DATA	= 0x7F6E
                    7F6F    189 	IN15DATA	= 0x7F6F
                            190 
                    7F70    191 	OUT8BCH		= 0x7F70
                    7F71    192 	OUT8BCL		= 0x7F71
                    7F72    193 	OUT9BCH		= 0x7F72
                    7F73    194 	OUT9BCL		= 0x7F73
                    7F74    195 	OUT10BCH	= 0x7F74
                    7F75    196 	OUT10BCL	= 0x7F75
                    7F76    197 	OUT11BCH	= 0x7F76
                    7F77    198 	OUT11BCL	= 0x7F77
                    7F78    199 	OUT12BCH	= 0x7F78
                    7F79    200 	OUT12BCL	= 0x7F79
                    7F7A    201 	OUT13BCH	= 0x7F7A
                    7F7B    202 	OUT13BCL	= 0x7F7B
                    7F7C    203 	OUT14BCH	= 0x7F7C
                    7F7D    204 	OUT14BCL	= 0x7F7D
                    7F7E    205 	OUT15BCH	= 0x7F7E
                    7F7F    206 	OUT15BCL	= 0x7F7F
                            207 
                    7FF0    208 	OUT8ADDR	= 0x7FF0
                    7FF1    209 	OUT9ADDR	= 0x7FF1
                    7FF2    210 	OUT10ADDR	= 0x7FF2
                    7FF3    211 	OUT11ADDR	= 0x7FF3
                    7FF4    212 	OUT12ADDR	= 0x7FF4
                    7FF5    213 	OUT13ADDR	= 0x7FF5
                    7FF6    214 	OUT14ADDR	= 0x7FF6
                    7FF7    215 	OUT15ADDR	= 0x7FF7
                    7FF8    216 	IN8ADDR		= 0x7FF8
                    7FF9    217 	IN9ADDR		= 0x7FF9
                    7FFA    218 	IN10ADDR	= 0x7FFA
                    7FFB    219 	IN11ADDR	= 0x7FFB
                    7FFC    220 	IN12ADDR	= 0x7FFC
                    7FFD    221 	IN13ADDR	= 0x7FFD
                    7FFE    222 	IN14ADDR	= 0x7FFE
                    7FFF    223 	IN15ADDR	= 0x7FFF
                            224 
                    7FA0    225 	ISOERR		= 0x7FA0
                    7FA1    226 	ISOCTL		= 0x7FA1
                    7FA2    227 	ZBCOUNT		= 0x7FA2
                    7FE0    228 	INISOVAL	= 0x7FE0
                    7FE1    229 	OUTISOVAL	= 0x7FE1
                    7FE2    230 	FASTXFR		= 0x7FE2
                            231 
                            232 	;; CPU control registers
                            233 
                    7F92    234 	CPUCS		= 0x7F92
                            235 
                            236 	;; IO port control registers
                            237 
                    7F93    238 	PORTACFG	= 0x7F93
                    7F94    239 	PORTBCFG	= 0x7F94
                    7F95    240 	PORTCCFG	= 0x7F95
                    7F96    241 	OUTA		= 0x7F96
                    7F97    242 	OUTB		= 0x7F97
                    7F98    243 	OUTC		= 0x7F98
                    7F99    244 	PINSA		= 0x7F99
                    7F9A    245 	PINSB		= 0x7F9A
                    7F9B    246 	PINSC		= 0x7F9B
                    7F9C    247 	OEA		= 0x7F9C
                    7F9D    248 	OEB		= 0x7F9D
                    7F9E    249 	OEC		= 0x7F9E
                            250 
                            251 	;; I2C controller registers
                            252 
                    7FA5    253 	I2CS		= 0x7FA5
                    7FA6    254 	I2DAT		= 0x7FA6
                            255 
                            256 	;; Xilinx FPGA registers
                    C000    257 	AUDIORXFIFO	= 0xc000
                    C000    258 	AUDIOTXFIFO	= 0xc000
                    C001    259 	AUDIORXFIFOCNT	= 0xc001
                    C002    260 	AUDIOTXFIFOCNT	= 0xc002
                    C001    261 	AUDIODIVIDERLO	= 0xc001
                    C002    262 	AUDIODIVIDERHI	= 0xc002
                    C004    263 	AUDIORSSI	= 0xc004
                    C005    264 	AUDIOCNTLOW	= 0xc005
                    C006    265 	AUDIOCNTMID	= 0xc006
                    C007    266 	AUDIOCNTHIGH	= 0xc007
                    C008    267 	AUDIOCTRL	= 0xc008
                    C009    268 	AUDIOSTAT	= 0xc009
                    C00A    269 	AUDIOT7FOUT	= 0xc00a
                    C00B    270 	AUDIOT7FIN	= 0xc00b
                    C00C    271 	AUDIOMDISCTRIS	= 0xc00c
                    C00D    272 	AUDIOMDISCOUT	= 0xc00d
                    C00E    273 	AUDIOMDISCIN	= 0xc00e
                            274 
                    0001    275 	AUDIOCTRLPTT	= 0x01
                    0002    276 	AUDIOCTRLMUTE	= 0x02
                    0004    277 	AUDIOCTRLLEDPTT	= 0x04
                    0008    278 	AUDIOCTRLLEDDCD	= 0x08
                    0000    279 	AUDIOCTRLCNTRES	= 0x00
                    0010    280 	AUDIOCTRLCNTDIS	= 0x10
                    0040    281 	AUDIOCTRLCNTCK0	= 0x40
                    0050    282 	AUDIOCTRLCNTCK1	= 0x50
                    0060    283 	AUDIOCTRLCNTCK2	= 0x60
                    0070    284 	AUDIOCTRLCNTCK3	= 0x70
                    0080    285 	AUDIOCTRLCNTRD1	= 0x80
                            286 
                            287 	;; -----------------------------------------------------
                            288 
                            289 	.area CODE (CODE)
   0000 02 0E 07            290 	ljmp	startup
   0003 02 01 6D            291 	ljmp	int0_isr
   0006                     292 	.ds	5
   000B 02 01 8E            293 	ljmp	timer0_isr
   000E                     294 	.ds	5
   0013 02 01 AF            295 	ljmp	int1_isr
   0016                     296 	.ds	5
   001B 02 01 D0            297 	ljmp	timer1_isr
   001E                     298 	.ds	5
   0023 02 01 F1            299 	ljmp	ser0_isr
   0026                     300 	.ds	5
   002B 02 02 2F            301 	ljmp	timer2_isr
   002E                     302 	.ds	5
   0033 02 02 50            303 	ljmp	resume_isr
   0036                     304 	.ds	5
   003B 02 02 71            305 	ljmp	ser1_isr
   003E                     306 	.ds	5
   0043 02 01 00            307 	ljmp	usb_isr
   0046                     308 	.ds	5
   004B 02 02 94            309 	ljmp	i2c_isr
   004E                     310 	.ds	5
   0053 02 02 B9            311 	ljmp	int4_isr
   0056                     312 	.ds	5
   005B 02 02 DE            313 	ljmp	int5_isr
   005E                     314 	.ds	5
   0063 02 03 03            315 	ljmp	int6_isr
                            316 	
                            317 	;; Parameter block at 0xe0
   0066                     318 	.ds	0x7a
   00E0 08                  319 parframesize:	.db	8
   00E1 01                  320 parpttmute:	.db	1
                            321 
                            322 	;; Serial# string at 0xf0
   00E2                     323 	.ds	14
   00F0                     324 parserial:
   00F0 30 30 30 30 30 30   325 	.db	'0,'0,'0,'0,'0,'0,'0,'0,0
        30 30 00
   00F9                     326 	.ds	7
                            327 
   0100                     328 usb_isr:
   0100 02 03 24            329 	ljmp	usb_sudav_isr
   0103                     330 	.ds	1
   0104 02 07 D2            331 	ljmp	usb_sof_isr
   0107                     332 	.ds	1
   0108 02 09 1C            333 	ljmp	usb_sutok_isr
   010B                     334 	.ds	1
   010C 02 09 47            335 	ljmp	usb_suspend_isr
   010F                     336 	.ds	1
   0110 02 09 72            337 	ljmp	usb_usbreset_isr
   0113                     338 	.ds	1
   0114 32                  339 	reti
   0115                     340 	.ds	3
   0118 02 09 9D            341 	ljmp	usb_ep0in_isr
   011B                     342 	.ds	1
   011C 02 09 D8            343 	ljmp	usb_ep0out_isr
   011F                     344 	.ds	1
   0120 02 0A 6E            345 	ljmp	usb_ep1in_isr
   0123                     346 	.ds	1
   0124 02 0A 9C            347 	ljmp	usb_ep1out_isr
   0127                     348 	.ds	1
   0128 02 0A C7            349 	ljmp	usb_ep2in_isr
   012B                     350 	.ds	1
   012C 02 0A F2            351 	ljmp	usb_ep2out_isr
   012F                     352 	.ds	1
   0130 02 0B 1D            353 	ljmp	usb_ep3in_isr
   0133                     354 	.ds	1
   0134 02 0B 48            355 	ljmp	usb_ep3out_isr
   0137                     356 	.ds	1
   0138 02 0B 73            357 	ljmp	usb_ep4in_isr
   013B                     358 	.ds	1
   013C 02 0B 9E            359 	ljmp	usb_ep4out_isr
   013F                     360 	.ds	1
   0140 02 0B C9            361 	ljmp	usb_ep5in_isr
   0143                     362 	.ds	1
   0144 02 0B F4            363 	ljmp	usb_ep5out_isr
   0147                     364 	.ds	1
   0148 02 0C 1F            365 	ljmp	usb_ep6in_isr
   014B                     366 	.ds	1
   014C 02 0C 4A            367 	ljmp	usb_ep6out_isr
   014F                     368 	.ds	1
   0150 02 0C 75            369 	ljmp	usb_ep7in_isr
   0153                     370 	.ds	1
   0154 02 0C A0            371 	ljmp	usb_ep7out_isr
                            372 
                            373 	;; -----------------------------------------------------
                            374 
                    0004    375 	NUMINTERFACES = 4
                            376 	
                            377 	.area	OSEG (OVR,DATA)
                            378 	.area	BSEG (BIT)
   0000                     379 ctrl_ptt:	.ds	1
   0001                     380 ctrl_pttmute:	.ds	1
   0002                     381 ctrl_ledptt:	.ds	1
   0003                     382 ctrl_leddcd:	.ds	1	
   0004                     383 ctrl_cntmode0:	.ds	1
   0005                     384 ctrl_cntmode1:	.ds	1
   0006                     385 ctrl_cntmode2:	.ds	1
   0007                     386 ctrl_cntsel:	.ds	1
                    0020    387 ctrlreg		=	0x20	; ((ctrl_ptt/8)+0x20)
                            388 
   0008                     389 pttmute:	.ds	1
   0009                     390 uartempty:	.ds	1
                            391 
                            392 
                            393 	.area	ISEG (DATA)
   0080                     394 txsamples:	.ds	0x40
   00C0                     395 stack:		.ds	0x80-0x40
                            396 
                            397 	.area	DSEG (DATA)
   0040                     398 ctrlcode:	.ds	1
   0041                     399 ctrlcount:	.ds	2
   0043                     400 leddiv:		.ds	1
   0044                     401 irqcount:	.ds	1
   0045                     402 sofcount:	.ds	1
   0046                     403 divider:	.ds	2
   0048                     404 divrel:		.ds	1
   0049                     405 framesize:	.ds	1
   004A                     406 pllcorrvar:	.ds	1
   004B                     407 txfifocount:	.ds	1
   004C                     408 pttforce:	.ds	1
                            409 
                            410 	;; UART receiver
   004D                     411 uartbuf:	.ds	16
   005D                     412 uartwr:		.ds	1
   005E                     413 uartrd:		.ds	1
                            414 
                            415 	;; Port state
   005F                     416 t7fout:		.ds	1
   0060                     417 mdisctris:	.ds	1
   0061                     418 mdiscout:	.ds	1
                            419 	
                            420 	;; USB state
   0062                     421 numconfig:	.ds	1
   0063                     422 altsetting:	.ds	NUMINTERFACES
                            423 
                            424 	.area	XSEG (DATA)
   1000                     425 blah:	.ds	1
                            426 
                            427 
                            428 	.area	GSINIT (CODE)
                    0002    429 	ar2 = 0x02
                    0003    430 	ar3 = 0x03
                    0004    431 	ar4 = 0x04
                    0005    432 	ar5 = 0x05
                    0006    433 	ar6 = 0x06
                    0007    434 	ar7 = 0x07
                    0000    435 	ar0 = 0x00
                    0001    436 	ar1 = 0x01
                            437 
   0E07                     438 startup:
   0E07 75 81 C0            439 	mov	sp,#stack	; -1
   0E0A E4                  440 	clr	a
   0E0B F5 D0               441 	mov	psw,a
   0E0D F5 86               442 	mov	dps,a
                            443 	;lcall	__sdcc_external_startup
                            444 	;mov	a,dpl0
                            445 	;jz	__sdcc_init_data
                            446 	;ljmp	__sdcc_program_startup
   0E0F                     447 __sdcc_init_data:
                            448 
                            449 	.area	GSINIT2 (CODE)
   0E0F                     450 __sdcc_program_startup:
                            451 	;; assembler code startup
   0E0F E4                  452 	clr	a
   0E10 F5 44               453  	mov	irqcount,a
   0E12 F5 45               454 	mov	sofcount,a
   0E14 F5 4C               455 	mov	pttforce,a
   0E16 F5 5E               456 	mov	uartrd,a
   0E18 F5 5D               457 	mov	uartwr,a
   0E1A F5 86               458 	mov	dps,a
   0E1C D2 09               459 	setb	uartempty
                            460 	;; some indirect register setup
   0E1E 75 8E 30            461 	mov	ckcon,#0x30	; zero external wait states, to avoid chip bugs
                            462 	;; Timer setup:
                            463 	;; timer 0: LED blink    : T0M=0, CT0=0, MODE0=1
                            464 	;; timer 1: Baudrate gen : T1M=1, CT1=0, MODE1=2
   0E21 75 89 21            465 	mov	tmod,#0x21
   0E24 75 88 55            466 	mov	tcon,#0x55	; INT0/INT1 edge
   0E27 75 8D 64            467 	mov	th1,#256-156	; 1200 bauds
   0E2A 75 87 00            468 	mov	pcon,#0		; SMOD0=0
                            469 	;; init USB subsystem
   0E2D 74 00               470 	mov	a,#0x00		; IN8 FIFO at address 0x0000
   0E2F 90 7F F8            471 	mov	dptr,#IN8ADDR
   0E32 F0                  472 	movx	@dptr,a
   0E33 74 04               473 	mov	a,#0x04		; OUT8 FIFO at address 0x0010
   0E35 90 7F F0            474 	mov	dptr,#OUT8ADDR
   0E38 F0                  475 	movx	@dptr,a
   0E39 90 7F A1            476 	mov	dptr,#ISOCTL
   0E3C E4                  477 	clr	a		; enable ISO endpoints
   0E3D F0                  478 	movx	@dptr,a
   0E3E 90 7F AF            479 	mov	dptr,#USBBAV
   0E41 74 01               480 	mov	a,#1		; enable autovector, disable breakpoint logic
   0E43 F0                  481 	movx	@dptr,a
   0E44 74 01               482 	mov	a,#0x01		; enable ISO endpoint 8 for input/output
   0E46 90 7F E0            483 	mov	dptr,#INISOVAL
   0E49 F0                  484 	movx	@dptr,a
   0E4A 90 7F E1            485 	mov	dptr,#OUTISOVAL
   0E4D F0                  486 	movx	@dptr,a
   0E4E 90 7F DD            487 	mov	dptr,#USBPAIR
   0E51 74 89               488 	mov	a,#0x89		; pair EP 2&3 for input & output, ISOSEND0
   0E53 F0                  489 	movx	@dptr,a
   0E54 90 7F DE            490 	mov	dptr,#IN07VAL
   0E57 74 03               491 	mov	a,#0x3		; enable EP0+EP1
   0E59 F0                  492 	movx	@dptr,a
   0E5A 90 7F DF            493 	mov	dptr,#OUT07VAL
   0E5D 74 01               494 	mov	a,#0x1		; enable EP0
   0E5F F0                  495 	movx	@dptr,a
                            496 	;; USB:	init endpoint toggles
   0E60 90 7F D7            497 	mov	dptr,#TOGCTL
   0E63 74 12               498 	mov	a,#0x12
   0E65 F0                  499 	movx	@dptr,a
   0E66 74 32               500 	mov	a,#0x32		; clear EP 2 in toggle
   0E68 F0                  501 	movx	@dptr,a
   0E69 74 02               502 	mov	a,#0x02
   0E6B F0                  503 	movx	@dptr,a
   0E6C 74 22               504 	mov	a,#0x22		; clear EP 2 out toggle
   0E6E F0                  505 	movx	@dptr,a
                            506 	;; configure IO ports
   0E6F 90 7F 93            507 	mov	dptr,#PORTACFG
   0E72 74 00               508 	mov	a,#0
   0E74 F0                  509 	movx	@dptr,a
   0E75 90 7F 96            510 	mov	dptr,#OUTA
   0E78 74 82               511 	mov	a,#0x82		; set PROG hi
   0E7A F0                  512 	movx	@dptr,a
   0E7B 90 7F 9C            513 	mov	dptr,#OEA
   0E7E 74 C2               514 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   0E80 F0                  515 	movx	@dptr,a
   0E81 90 7F 94            516 	mov	dptr,#PORTBCFG
   0E84 74 00               517 	mov	a,#0
   0E86 F0                  518 	movx	@dptr,a
   0E87 90 7F 9D            519 	mov	dptr,#OEB
   0E8A 74 00               520 	mov	a,#0
   0E8C F0                  521 	movx	@dptr,a
   0E8D 90 7F 95            522 	mov	dptr,#PORTCCFG
   0E90 74 C3               523 	mov	a,#0xc3		; RD/WR/TXD0/RXD0 are special function pins
   0E92 F0                  524 	movx	@dptr,a
   0E93 90 7F 98            525 	mov	dptr,#OUTC
   0E96 74 28               526 	mov	a,#0x28
   0E98 F0                  527 	movx	@dptr,a
   0E99 90 7F 9E            528 	mov	dptr,#OEC
   0E9C 74 2A               529 	mov	a,#0x2a		; out: LEDCON,LEDSTA,TCK  in: TDO,INIT
   0E9E F0                  530 	movx	@dptr,a
                            531 	;; enable interrupts
   0E9F 75 A8 92            532 	mov	ie,#0x92	; enable timer 0 and ser 0 int
   0EA2 75 E8 01            533 	mov	eie,#0x01	; enable USB interrupts
   0EA5 90 7F AE            534 	mov	dptr,#USBIEN
   0EA8 74 01               535 	mov	a,#1		; enable SUDAV interrupt
   0EAA F0                  536 	movx	@dptr,a
   0EAB 90 7F AC            537 	mov	dptr,#IN07IEN
   0EAE 74 03               538 	mov	a,#3		; enable EP0+EP1 interrupt
   0EB0 F0                  539 	movx	@dptr,a
   0EB1 90 7F AD            540 	mov	dptr,#OUT07IEN
   0EB4 74 01               541 	mov	a,#1		; enable EP0 interrupt
   0EB6 F0                  542 	movx	@dptr,a
                            543 	;; initialize UART 0 for T7F communication
   0EB7 75 98 52            544 	mov	scon0,#0x52	; Mode 1, Timer 1, Receiver enable
                            545         ;; copy configuration to bit addressable variables
   0EBA 75 20 00            546         mov     ctrlreg,#AUDIOCTRLCNTRES
   0EBD 78 E1               547         mov     r0,#parpttmute
   0EBF E2                  548         movx    a,@r0
   0EC0 A2 E0               549         mov     c,acc.0
   0EC2 92 08               550         mov     pttmute,c
   0EC4 92 01               551         mov     ctrl_pttmute,c
                            552 	;; turn off transmitter
   0EC6 90 C0 08            553 	mov	dptr,#AUDIOCTRL
   0EC9 E5 20               554 	mov	a,ctrlreg
   0ECB F0                  555 	movx	@dptr,a
                            556 	;; Initialize modem disc port / t7f port
   0ECC 90 C0 0C            557 	mov	dptr,#AUDIOMDISCTRIS
   0ECF 74 FF               558 	mov	a,#0xff
   0ED1 F0                  559 	movx	@dptr,a
   0ED2 F5 60               560 	mov	mdisctris,a
   0ED4 90 C0 0D            561 	mov	dptr,#AUDIOMDISCOUT
   0ED7 E4                  562 	clr	a
   0ED8 F0                  563 	movx	@dptr,a
   0ED9 F5 61               564 	mov	mdiscout,a
   0EDB 90 C0 0A            565 	mov	dptr,#AUDIOT7FOUT
   0EDE 74 1F               566 	mov	a,#0x1f
   0EE0 F0                  567 	movx	@dptr,a
   0EE1 F5 5F               568 	mov	t7fout,a
                            569 	;; Copy serial number
   0EE3 78 F0               570 	mov	r0,#parserial
   0EE5 90 0D E7            571 	mov	dptr,#stringserial+2
   0EE8 E2                  572 1$:	movx	a,@r0
   0EE9 60 06               573 	jz	2$
   0EEB F0                  574 	movx	@dptr,a
   0EEC A3                  575 	inc	dptr
   0EED A3                  576 	inc	dptr
   0EEE 08                  577 	inc	r0
   0EEF 80 F7               578 	sjmp	1$
   0EF1 E8                  579 2$:	mov	a,r0
   0EF2 24 11               580 	add	a,#1-0xf0	; 1-parserial
   0EF4 25 E0               581 	add	a,acc
   0EF6 90 0D E5            582 	mov	dptr,#stringserial
   0EF9 F0                  583 	movx	@dptr,a
                            584 	;; check parameters
   0EFA                     585 chkparam:
   0EFA 75 48 80            586 	mov	divrel,#0x80
   0EFD 78 E0               587 	mov	r0,#parframesize
   0EFF E2                  588 	movx	a,@r0
   0F00 B4 02 02            589 	cjne	a,#02,11$
   0F03 80 4E               590 	sjmp	2$
   0F05 B4 03 02            591 11$:	cjne	a,#03,1$
   0F08 80 49               592 	sjmp	2$
   0F0A 24 F8               593 1$:	add	a,#-8
   0F0C 50 0B               594 	jnc	3$
   0F0E 24 F0               595 	add	a,#-16
   0F10 40 03               596 	jc	4$
   0F12 E2                  597 	movx	a,@r0
   0F13 80 06               598 	sjmp	5$
                            599 	;; sampling rate is: 24000000Hz/(divider+2)
   0F15 74 18               600 4$:	mov	a,#24
   0F17 80 02               601 	sjmp	5$
   0F19 74 08               602 3$:	mov	a,#8
                            603 	;; 16 bit by 8 bit divide
                            604 	;; r2:	divisor
                            605 	;; r3:	loop counter
                            606 	;; r4:	dividend/result low
                            607 	;; r5:	dividend/result mid
                            608 	;; r6:	dividend/result high
   0F1B FA                  609 5$:	mov	r2,a
   0F1C 7B 11               610 	mov	r3,#17
   0F1E 7C C0               611 	mov	r4,#24000
   0F20 7D 5D               612 	mov	r5,#24000>>8
   0F22 7E 00               613 	mov	r6,#0
   0F24 C3                  614 6$:	clr	c
   0F25 EE                  615 	mov	a,r6
   0F26 9A                  616 	subb	a,r2
   0F27 40 01               617 	jc	7$
   0F29 FE                  618 	mov	r6,a
   0F2A B3                  619 7$:	cpl	c
   0F2B EC                  620 	mov	a,r4
   0F2C 33                  621 	rlc	a
   0F2D FC                  622 	mov	r4,a
   0F2E ED                  623 	mov	a,r5
   0F2F 33                  624 	rlc	a
   0F30 FD                  625 	mov	r5,a
   0F31 EE                  626 	mov	a,r6
   0F32 33                  627 	rlc	a
   0F33 FE                  628 	mov	r6,a
   0F34 DB EE               629 	djnz	r3,6$
                            630 	;; subtract two
   0F36 EC                  631 	mov	a,r4
   0F37 C3                  632 	clr	c
   0F38 94 02               633 	subb	a,#2
   0F3A FC                  634 	mov	r4,a
   0F3B ED                  635 	mov	a,r5
   0F3C 94 00               636 	subb	a,#0
   0F3E FD                  637 	mov	r5,a
                            638 	;; store result into audio divider
   0F3F 90 C0 02            639 	mov	dptr,#AUDIODIVIDERHI
   0F42 F0                  640 	movx	@dptr,a
   0F43 EC                  641 	mov	a,r4
   0F44 90 C0 01            642 	mov	dptr,#AUDIODIVIDERLO
   0F47 F0                  643 	movx	@dptr,a
   0F48 C3                  644 	clr	c
   0F49 94 08               645 	subb	a,#0x08
   0F4B F5 46               646 	mov	divider,a
   0F4D ED                  647 	mov	a,r5
   0F4E 94 00               648 	subb	a,#0
   0F50 F5 47               649 	mov	divider+1,a
                            650 	;; reload divider into accu
   0F52 EA                  651 	mov	a,r2
   0F53 F5 49               652 2$:	mov	framesize,a
   0F55 90 0D 6B            653 	mov	dptr,#descinframesize
   0F58 F0                  654 	movx	@dptr,a
   0F59 90 0D 9F            655 	mov	dptr,#descoutframesize
   0F5C F0                  656 	movx	@dptr,a
                            657 	;; set sampling rate in descriptor
   0F5D 75 F0 E8            658 	mov	b,#1000
   0F60 A4                  659 	mul	ab
   0F61 90 0D 64            660 	mov	dptr,#descinsrate
   0F64 F0                  661 	movx	@dptr,a
   0F65 90 0D 98            662 	mov	dptr,#descoutsrate
   0F68 F0                  663 	movx	@dptr,a
   0F69 AB F0               664 	mov	r3,b
   0F6B E5 49               665 	mov	a,framesize
   0F6D 75 F0 03            666 	mov	b,#1000>>8
   0F70 A4                  667 	mul	ab
   0F71 2B                  668 	add	a,r3
   0F72 90 0D 65            669 	mov	dptr,#descinsrate+1
   0F75 F0                  670 	movx	@dptr,a
   0F76 90 0D 99            671 	mov	dptr,#descoutsrate+1
   0F79 F0                  672 	movx	@dptr,a
   0F7A E4                  673 	clr	a
   0F7B 35 F0               674 	addc	a,b
   0F7D 90 0D 66            675 	mov	dptr,#descinsrate+2
   0F80 F0                  676 	movx	@dptr,a
   0F81 90 0D 9A            677 	mov	dptr,#descoutsrate+2
   0F84 F0                  678 	movx	@dptr,a
                            679 	;; initialize USB state
   0F85                     680 usbinit:
                            681 
                    0000    682 	.if	0
                            683 	;;  XXXXX
                            684 	;; check if windows needs 11025Hz
                            685 	mov	a,#12
                            686 	mov	dptr,#descinframesize
                            687 	movx	@dptr,a
                            688 	mov	dptr,#descoutframesize
                            689 	movx	@dptr,a
                            690 	mov	a,#0x11
                            691 	mov	dptr,#descinsrate
                            692 	movx	@dptr,a
                            693 	mov	dptr,#descoutsrate
                            694 	movx	@dptr,a
                            695 	mov	a,#0x2b
                            696 	mov	dptr,#descinsrate+1
                            697 	movx	@dptr,a
                            698 	mov	dptr,#descoutsrate+1
                            699 	movx	@dptr,a
                            700 	mov	a,#0
                            701 	mov	dptr,#descinsrate+2
                            702 	movx	@dptr,a
                            703 	mov	dptr,#descoutsrate+2
                            704 	movx	@dptr,a
                            705 	;;  XXXXX
                            706 	.endif
                            707 	
   0F85 E4                  708 	clr	a
   0F86 F5 62               709 	mov	numconfig,a
   0F88 78 63               710 	mov	r0,#altsetting
   0F8A 7A 04               711 	mov	r2,#NUMINTERFACES
   0F8C F6                  712 3$:	mov	@r0,a
   0F8D 08                  713 	inc	r0
   0F8E DA FC               714 	djnz	r2,3$
                            715 	;; give Windows a chance to finish the writecpucs control transfer
                            716 	;; 20ms delay loop
   0F90 90 D1 20            717 	mov	dptr,#(-12000)&0xffff
   0F93 A3                  718 2$:	inc	dptr		; 3 cycles
   0F94 E5 82               719 	mov	a,dpl0		; 2 cycles
   0F96 45 83               720 	orl	a,dph0		; 2 cycles
   0F98 70 F9               721 	jnz	2$		; 3 cycles
                    0001    722 	.if	1
                            723 	;; disconnect from USB bus
   0F9A 90 7F D6            724 	mov	dptr,#USBCS
   0F9D 74 0A               725 	mov	a,#10
   0F9F F0                  726 	movx	@dptr,a
                            727 	;; wait 0.3 sec
   0FA0 7A 1E               728 	mov	r2,#30
                            729 	;; 10ms delay loop
   0FA2 90 E8 90            730 0$:	mov	dptr,#(-6000)&0xffff
   0FA5 A3                  731 1$:	inc	dptr            ; 3 cycles
   0FA6 E5 82               732 	mov	a,dpl0          ; 2 cycles
   0FA8 45 83               733 	orl	a,dph0          ; 2 cycles
   0FAA 70 F9               734 	jnz	1$              ; 3 cycles
   0FAC DA F4               735 	djnz	r2,0$
                            736 	;; reconnect to USB bus
   0FAE 90 7F D6            737 	mov	dptr,#USBCS
                            738 	;mov	a,#2		; 8051 handles control
                            739 	;movx	@dptr,a
   0FB1 74 06               740 	mov	a,#6		; reconnect, 8051 handles control
   0FB3 F0                  741 	movx	@dptr,a
                            742 	.endif
                            743 
                            744 	;; final
   0FB4 12 0A 13            745 	lcall	fillusbintr
   0FB7                     746 fifoinit:
                            747 	;; first wait for a new frame
   0FB7 90 7F D8            748 	mov	dptr,#USBFRAMEL
   0FBA E0                  749 	movx	a,@dptr
   0FBB FA                  750 	mov	r2,a
   0FBC E0                  751 1$:	movx	a,@dptr
   0FBD B5 02 02            752 	cjne	a,ar2,2$
   0FC0 80 FA               753 	sjmp	1$
   0FC2 90 C0 01            754 2$:	mov	dptr,#AUDIORXFIFOCNT
   0FC5 E0                  755 	movx	a,@dptr
   0FC6 24 FC               756 	add	a,#-4
   0FC8 54 3F               757 	anl	a,#0x3f
   0FCA 60 07               758 	jz	4$
   0FCC FA                  759 	mov	r2,a
   0FCD 90 C0 00            760 	mov	dptr,#AUDIORXFIFO
   0FD0 E0                  761 3$:	movx	a,@dptr
   0FD1 DA FD               762 	djnz	r2,3$
   0FD3 90 C0 02            763 4$:	mov	dptr,#AUDIOTXFIFOCNT
   0FD6 E0                  764 	movx	a,@dptr
   0FD7 24 FC               765 	add	a,#-4
   0FD9 54 3F               766 	anl	a,#0x3f
   0FDB 60 F6               767 	jz	4$
   0FDD FA                  768 	mov	r2,a
   0FDE 90 C0 00            769 	mov	dptr,#AUDIOTXFIFO
   0FE1 E4                  770 	clr	a
   0FE2 F0                  771 5$:	movx	@dptr,a
   0FE3 DA FD               772 	djnz	r2,5$
   0FE5                     773 6$:	;; clear SOFIR interrupt
   0FE5 90 7F AB            774 	mov	dptr,#USBIRQ
   0FE8 74 02               775 	mov	a,#0x02
   0FEA F0                  776 	movx	@dptr,a
                            777 	;; finally enable SOFIR interrupt
   0FEB 90 7F AE            778 	mov	dptr,#USBIEN
   0FEE 74 03               779 	mov	a,#3		; enable SUDAV+SOFIR interrupt
   0FF0 F0                  780 	movx	@dptr,a	
   0FF1 12 0A 13            781 	lcall	fillusbintr
   0FF4 02 01 57            782 	ljmp	mainloop
                            783 
                            784 
                            785 	.area	CSEG (CODE)
                    0002    786 	ar2 = 0x02
                    0003    787 	ar3 = 0x03
                    0004    788 	ar4 = 0x04
                    0005    789 	ar5 = 0x05
                    0006    790 	ar6 = 0x06
                    0007    791 	ar7 = 0x07
                    0000    792 	ar0 = 0x00
                    0001    793 	ar1 = 0x01
                            794 
                            795 	;; WARNING!  The assembler doesn't check for
                            796 	;; out of range short jump labels!! Double check
                            797 	;; that the jump labels are within the range!
   0157                     798 mainloop:
   0157 E5 4C               799 	mov	a,pttforce
   0159 A2 E0               800 	mov	c,acc.0
   015B 92 00               801 	mov	ctrl_ptt,c
   015D 92 02               802 	mov	ctrl_ledptt,c
   015F B3                  803 	cpl	c
   0160 82 08               804 	anl	c,pttmute
   0162 92 01               805 	mov	ctrl_pttmute,c
   0164 90 C0 08            806 	mov	dptr,#AUDIOCTRL
   0167 E5 20               807 	mov	a,ctrlreg
   0169 F0                  808 	movx	@dptr,a
   016A 02 01 57            809 	ljmp	mainloop
                            810 
                            811 	;; ------------------ interrupt handlers ------------------------
                            812 
   016D                     813 int0_isr:
   016D C0 E0               814 	push	acc
   016F C0 F0               815 	push	b
   0171 C0 82               816 	push	dpl0
   0173 C0 83               817 	push	dph0
   0175 C0 D0               818 	push	psw
   0177 75 D0 00            819 	mov	psw,#0x00
   017A C0 86               820 	push	dps
   017C 75 86 00            821 	mov	dps,#0
                            822 	;; clear interrupt
   017F C2 89               823 	clr	tcon+1
                            824 	;; handle interrupt
                            825 	;; epilogue
   0181 D0 86               826 	pop	dps
   0183 D0 D0               827 	pop	psw
   0185 D0 83               828 	pop	dph0
   0187 D0 82               829 	pop	dpl0
   0189 D0 F0               830 	pop	b
   018B D0 E0               831 	pop	acc
   018D 32                  832 	reti
                            833 
   018E                     834 timer0_isr:
   018E C0 E0               835 	push	acc
   0190 C0 F0               836 	push	b
   0192 C0 82               837 	push	dpl0
   0194 C0 83               838 	push	dph0
   0196 C0 D0               839 	push	psw
   0198 75 D0 00            840 	mov	psw,#0x00
   019B C0 86               841 	push	dps
   019D 75 86 00            842 	mov	dps,#0
                            843 	;; clear interrupt
   01A0 C2 8D               844 	clr	tcon+5
                            845 	;; handle interrupt
                    0000    846 	.if	0
                            847 	inc	leddiv
                            848 	mov	a,leddiv
                            849 	anl	a,#7
                            850 	jnz	0$
                            851 	mov	dptr,#OUTC
                            852 	movx	a,@dptr
                            853 	xrl	a,#0x08
                            854 	movx	@dptr,a
                            855 0$:
                            856 	.endif
                            857 	;; epilogue
   01A2 D0 86               858 	pop	dps
   01A4 D0 D0               859 	pop	psw
   01A6 D0 83               860 	pop	dph0
   01A8 D0 82               861 	pop	dpl0
   01AA D0 F0               862 	pop	b
   01AC D0 E0               863 	pop	acc
   01AE 32                  864 	reti
                            865 
   01AF                     866 int1_isr:
   01AF C0 E0               867 	push	acc
   01B1 C0 F0               868 	push	b
   01B3 C0 82               869 	push	dpl0
   01B5 C0 83               870 	push	dph0
   01B7 C0 D0               871 	push	psw
   01B9 75 D0 00            872 	mov	psw,#0x00
   01BC C0 86               873 	push	dps
   01BE 75 86 00            874 	mov	dps,#0
                            875 	;; clear interrupt
   01C1 C2 8B               876 	clr	tcon+3
                            877 	;; handle interrupt
                            878 	;; epilogue
   01C3 D0 86               879 	pop	dps
   01C5 D0 D0               880 	pop	psw
   01C7 D0 83               881 	pop	dph0
   01C9 D0 82               882 	pop	dpl0
   01CB D0 F0               883 	pop	b
   01CD D0 E0               884 	pop	acc
   01CF 32                  885 	reti
                            886 
   01D0                     887 timer1_isr:
   01D0 C0 E0               888 	push	acc
   01D2 C0 F0               889 	push	b
   01D4 C0 82               890 	push	dpl0
   01D6 C0 83               891 	push	dph0
   01D8 C0 D0               892 	push	psw
   01DA 75 D0 00            893 	mov	psw,#0x00
   01DD C0 86               894 	push	dps
   01DF 75 86 00            895 	mov	dps,#0
                            896 	;; clear interrupt
   01E2 C2 8F               897 	clr	tcon+7
                            898 	;; handle interrupt
                            899 	;; epilogue
   01E4 D0 86               900 	pop	dps
   01E6 D0 D0               901 	pop	psw
   01E8 D0 83               902 	pop	dph0
   01EA D0 82               903 	pop	dpl0
   01EC D0 F0               904 	pop	b
   01EE D0 E0               905 	pop	acc
   01F0 32                  906 	reti
                            907 
   01F1                     908 ser0_isr:
   01F1 C0 E0               909 	push	acc
   01F3 C0 F0               910 	push	b
   01F5 C0 82               911 	push	dpl0
   01F7 C0 83               912 	push	dph0
   01F9 C0 D0               913 	push	psw
   01FB 75 D0 00            914 	mov	psw,#0x00
   01FE C0 86               915 	push	dps
   0200 75 86 00            916 	mov	dps,#0
   0203 C0 00               917 	push	ar0
                            918 	;; clear interrupt
   0205 10 98 16            919 	jbc	scon0+0,1$	; RI
   0208 10 99 0F            920 0$:	jbc	scon0+1,2$	; TI
                            921 	;; handle interrupt
                            922 	;; epilogue
   020B D0 00               923 3$:	pop	ar0
   020D D0 86               924 	pop	dps
   020F D0 D0               925 	pop	psw
   0211 D0 83               926 	pop	dph0
   0213 D0 82               927 	pop	dpl0
   0215 D0 F0               928 	pop	b
   0217 D0 E0               929 	pop	acc
   0219 32                  930 	reti
                            931 
   021A D2 09               932 2$:	setb	uartempty
   021C 80 EA               933 	sjmp	0$
                            934 
   021E E5 5D               935 1$:	mov	a,uartwr
   0220 24 4D               936 	add	a,#uartbuf
   0222 F8                  937 	mov	r0,a
   0223 E5 99               938 	mov	a,sbuf0
   0225 F6                  939 	mov	@r0,a
   0226 E5 5D               940 	mov	a,uartwr
   0228 04                  941 	inc	a
   0229 54 0F               942 	anl	a,#0xf
   022B F5 5D               943 	mov	uartwr,a
   022D 80 DC               944 	sjmp	3$
                            945 
   022F                     946 timer2_isr:
   022F C0 E0               947 	push	acc
   0231 C0 F0               948 	push	b
   0233 C0 82               949 	push	dpl0
   0235 C0 83               950 	push	dph0
   0237 C0 D0               951 	push	psw
   0239 75 D0 00            952 	mov	psw,#0x00
   023C C0 86               953 	push	dps
   023E 75 86 00            954 	mov	dps,#0
                            955 	;; clear interrupt
   0241 C2 CF               956 	clr	t2con+7
                            957 	;; handle interrupt
                            958 	;; epilogue
   0243 D0 86               959 	pop	dps
   0245 D0 D0               960 	pop	psw
   0247 D0 83               961 	pop	dph0
   0249 D0 82               962 	pop	dpl0
   024B D0 F0               963 	pop	b
   024D D0 E0               964 	pop	acc
   024F 32                  965 	reti
                            966 
   0250                     967 resume_isr:
   0250 C0 E0               968 	push	acc
   0252 C0 F0               969 	push	b
   0254 C0 82               970 	push	dpl0
   0256 C0 83               971 	push	dph0
   0258 C0 D0               972 	push	psw
   025A 75 D0 00            973 	mov	psw,#0x00
   025D C0 86               974 	push	dps
   025F 75 86 00            975 	mov	dps,#0
                            976 	;; clear interrupt
   0262 C2 DC               977 	clr	eicon+4
                            978 	;; handle interrupt
                            979 	;; epilogue
   0264 D0 86               980 	pop	dps
   0266 D0 D0               981 	pop	psw
   0268 D0 83               982 	pop	dph0
   026A D0 82               983 	pop	dpl0
   026C D0 F0               984 	pop	b
   026E D0 E0               985 	pop	acc
   0270 32                  986 	reti
                            987 
   0271                     988 ser1_isr:
   0271 C0 E0               989 	push	acc
   0273 C0 F0               990 	push	b
   0275 C0 82               991 	push	dpl0
   0277 C0 83               992 	push	dph0
   0279 C0 D0               993 	push	psw
   027B 75 D0 00            994 	mov	psw,#0x00
   027E C0 86               995 	push	dps
   0280 75 86 00            996 	mov	dps,#0
                            997 	;; clear interrupt
   0283 C2 C0               998 	clr	scon1+0
   0285 C2 C1               999 	clr	scon1+1
                           1000 	;; handle interrupt
                           1001 	;; epilogue
   0287 D0 86              1002 	pop	dps
   0289 D0 D0              1003 	pop	psw
   028B D0 83              1004 	pop	dph0
   028D D0 82              1005 	pop	dpl0
   028F D0 F0              1006 	pop	b
   0291 D0 E0              1007 	pop	acc
   0293 32                 1008 	reti
                           1009 
   0294                    1010 i2c_isr:
   0294 C0 E0              1011 	push	acc
   0296 C0 F0              1012 	push	b
   0298 C0 82              1013 	push	dpl0
   029A C0 83              1014 	push	dph0
   029C C0 D0              1015 	push	psw
   029E 75 D0 00           1016 	mov	psw,#0x00
   02A1 C0 86              1017 	push	dps
   02A3 75 86 00           1018 	mov	dps,#0
                           1019 	;; clear interrupt
   02A6 E5 91              1020 	mov	a,exif
   02A8 C2 E5              1021 	clr	acc.5
   02AA F5 91              1022 	mov	exif,a
                           1023 	;; handle interrupt
                           1024 	;; epilogue
   02AC D0 86              1025 	pop	dps
   02AE D0 D0              1026 	pop	psw
   02B0 D0 83              1027 	pop	dph0
   02B2 D0 82              1028 	pop	dpl0
   02B4 D0 F0              1029 	pop	b
   02B6 D0 E0              1030 	pop	acc
   02B8 32                 1031 	reti
                           1032 
   02B9                    1033 int4_isr:
   02B9 C0 E0              1034 	push	acc
   02BB C0 F0              1035 	push	b
   02BD C0 82              1036 	push	dpl0
   02BF C0 83              1037 	push	dph0
   02C1 C0 D0              1038 	push	psw
   02C3 75 D0 00           1039 	mov	psw,#0x00
   02C6 C0 86              1040 	push	dps
   02C8 75 86 00           1041 	mov	dps,#0
                           1042 	;; clear interrupt
   02CB E5 91              1043 	mov	a,exif
   02CD C2 E6              1044 	clr	acc.6
   02CF F5 91              1045 	mov	exif,a
                           1046 	;; handle interrupt
                           1047 	;; epilogue
   02D1 D0 86              1048 	pop	dps
   02D3 D0 D0              1049 	pop	psw
   02D5 D0 83              1050 	pop	dph0
   02D7 D0 82              1051 	pop	dpl0
   02D9 D0 F0              1052 	pop	b
   02DB D0 E0              1053 	pop	acc
   02DD 32                 1054 	reti
                           1055 
   02DE                    1056 int5_isr:
   02DE C0 E0              1057 	push	acc
   02E0 C0 F0              1058 	push	b
   02E2 C0 82              1059 	push	dpl0
   02E4 C0 83              1060 	push	dph0
   02E6 C0 D0              1061 	push	psw
   02E8 75 D0 00           1062 	mov	psw,#0x00
   02EB C0 86              1063 	push	dps
   02ED 75 86 00           1064 	mov	dps,#0
                           1065 	;; clear interrupt
   02F0 E5 91              1066 	mov	a,exif
   02F2 C2 E7              1067 	clr	acc.7
   02F4 F5 91              1068 	mov	exif,a
                           1069 	;; handle interrupt
                           1070 	;; epilogue
   02F6 D0 86              1071 	pop	dps
   02F8 D0 D0              1072 	pop	psw
   02FA D0 83              1073 	pop	dph0
   02FC D0 82              1074 	pop	dpl0
   02FE D0 F0              1075 	pop	b
   0300 D0 E0              1076 	pop	acc
   0302 32                 1077 	reti
                           1078 
   0303                    1079 int6_isr:
   0303 C0 E0              1080 	push	acc
   0305 C0 F0              1081 	push	b
   0307 C0 82              1082 	push	dpl0
   0309 C0 83              1083 	push	dph0
   030B C0 D0              1084 	push	psw
   030D 75 D0 00           1085 	mov	psw,#0x00
   0310 C0 86              1086 	push	dps
   0312 75 86 00           1087 	mov	dps,#0
                           1088 	;; clear interrupt
   0315 C2 DB              1089 	clr	eicon+3
                           1090 	;; handle interrupt
                           1091 	;; epilogue
   0317 D0 86              1092 	pop	dps
   0319 D0 D0              1093 	pop	psw
   031B D0 83              1094 	pop	dph0
   031D D0 82              1095 	pop	dpl0
   031F D0 F0              1096 	pop	b
   0321 D0 E0              1097 	pop	acc
   0323 32                 1098 	reti
                           1099 
   0324                    1100 usb_sudav_isr:
   0324 C0 E0              1101 	push	acc
   0326 C0 F0              1102 	push	b
   0328 C0 82              1103 	push	dpl0
   032A C0 83              1104 	push	dph0
   032C C0 84              1105 	push	dpl1
   032E C0 85              1106 	push	dph1
   0330 C0 D0              1107 	push	psw
   0332 75 D0 00           1108 	mov	psw,#0x00
   0335 C0 86              1109 	push	dps
   0337 75 86 00           1110 	mov	dps,#0
   033A C0 00              1111 	push	ar0
   033C C0 07              1112 	push	ar7
                           1113 	;; clear interrupt
   033E E5 91              1114 	mov	a,exif
   0340 C2 E4              1115 	clr	acc.4
   0342 F5 91              1116 	mov	exif,a
   0344 90 7F AB           1117 	mov	dptr,#USBIRQ
   0347 74 01              1118 	mov	a,#0x01
   0349 F0                 1119 	movx	@dptr,a
                           1120 	;; handle interrupt
   034A 75 40 00           1121 	mov	ctrlcode,#0		; reset control out code
   034D 90 7F E9           1122 	mov	dptr,#SETUPDAT+1
   0350 E0                 1123 	movx	a,@dptr			; bRequest field
                           1124 	;; standard commands
                           1125 	;; USB_REQ_GET_DESCRIPTOR
   0351 B4 06 59           1126 	cjne	a,#USB_REQ_GET_DESCRIPTOR,cmdnotgetdesc
   0354 90 7F E8           1127 	mov	dptr,#SETUPDAT		; bRequestType == 0x80
   0357 E0                 1128 	movx	a,@dptr
   0358 B4 80 4F           1129 	cjne	a,#USB_DIR_IN,setupstallstd
   035B 90 7F EB           1130 	mov	dptr,#SETUPDAT+3
   035E E0                 1131 	movx	a,@dptr
   035F B4 01 0C           1132 	cjne	a,#USB_DT_DEVICE,cmdnotgetdescdev
   0362 90 7F D4           1133 	mov	dptr,#SUDPTRH
   0365 74 0C              1134 	mov	a,#>devicedescr
   0367 F0                 1135 	movx	@dptr,a
   0368 A3                 1136 	inc	dptr
   0369 74 CB              1137 	mov	a,#<devicedescr
   036B F0                 1138 	movx	@dptr,a
   036C 80 39              1139 	sjmp	setupackstd
   036E                    1140 cmdnotgetdescdev:
   036E B4 02 12           1141 	cjne	a,#USB_DT_CONFIG,cmdnotgetdesccfg
   0371 90 7F EA           1142 	mov	dptr,#SETUPDAT+2
   0374 E0                 1143 	movx	a,@dptr
   0375 70 33              1144 	jnz	setupstallstd
   0377 90 7F D4           1145 	mov	dptr,#SUDPTRH
   037A 74 0C              1146 	mov	a,#>config0descr
   037C F0                 1147 	movx	@dptr,a
   037D A3                 1148 	inc	dptr
   037E 74 DD              1149 	mov	a,#<config0descr
   0380 F0                 1150 	movx	@dptr,a
   0381 80 24              1151 	sjmp	setupackstd
   0383                    1152 cmdnotgetdesccfg:
   0383 B4 03 24           1153 	cjne	a,#USB_DT_STRING,setupstallstd
   0386 90 7F EA           1154 	mov	dptr,#SETUPDAT+2
   0389 E0                 1155 	movx	a,@dptr
   038A 24 FC              1156 	add	a,#-numstrings
   038C 40 1C              1157 	jc	setupstallstd
   038E E0                 1158 	movx	a,@dptr
   038F 25 E0              1159 	add	a,acc
   0391 24 AB              1160 	add	a,#<stringdescr
   0393 F5 82              1161 	mov	dpl0,a
   0395 E4                 1162 	clr	a
   0396 34 0D              1163 	addc	a,#>stringdescr
   0398 F5 83              1164 	mov	dph0,a
   039A E0                 1165 	movx	a,@dptr
   039B F5 F0              1166 	mov	b,a
   039D A3                 1167 	inc	dptr
   039E E0                 1168 	movx	a,@dptr
   039F 90 7F D4           1169 	mov	dptr,#SUDPTRH
   03A2 F0                 1170 	movx	@dptr,a
   03A3 A3                 1171 	inc	dptr
   03A4 E5 F0              1172 	mov	a,b
   03A6 F0                 1173 	movx	@dptr,a
                           1174 	; sjmp	setupackstd	
   03A7                    1175 setupackstd:
   03A7 02 07 B7           1176 	ljmp	setupack
   03AA                    1177 setupstallstd:
   03AA 02 07 B3           1178 	ljmp	setupstall
   03AD                    1179 cmdnotgetdesc:
                           1180 	;; USB_REQ_SET_CONFIGURATION
   03AD B4 09 41           1181 	cjne	a,#USB_REQ_SET_CONFIGURATION,cmdnotsetconf
   03B0 90 7F E8           1182 	mov	dptr,#SETUPDAT
   03B3 E0                 1183 	movx	a,@dptr
   03B4 70 F4              1184 	jnz	setupstallstd
   03B6 90 7F EA           1185 	mov	dptr,#SETUPDAT+2
   03B9 E0                 1186 	movx	a,@dptr
   03BA 24 FE              1187 	add	a,#-2
   03BC 40 EC              1188 	jc	setupstallstd
   03BE E0                 1189 	movx	a,@dptr
   03BF F5 62              1190 	mov	numconfig,a
   03C1                    1191 cmdresettoggleshalt:
   03C1 90 7F D7           1192 	mov	dptr,#TOGCTL
   03C4 78 07              1193 	mov	r0,#7
   03C6 E8                 1194 0$:	mov	a,r0
   03C7 44 10              1195 	orl	a,#0x10
   03C9 F0                 1196 	movx	@dptr,a
   03CA 44 30              1197 	orl	a,#0x30
   03CC F0                 1198 	movx	@dptr,a
   03CD E8                 1199 	mov	a,r0
   03CE F0                 1200 	movx	@dptr,a
   03CF 44 20              1201 	orl	a,#0x20
   03D1 F0                 1202 	movx	@dptr,a
   03D2 D8 F2              1203 	djnz	r0,0$
   03D4 E4                 1204 	clr	a
   03D5 F0                 1205 	movx	@dptr,a
   03D6 74 02              1206 	mov	a,#2
   03D8 90 7F B6           1207 	mov	dptr,#IN1CS
   03DB 78 07              1208 	mov	r0,#7
   03DD F0                 1209 1$:	movx	@dptr,a
   03DE A3                 1210 	inc	dptr
   03DF A3                 1211 	inc	dptr
   03E0 D8 FB              1212 	djnz	r0,1$
   03E2 90 7F C6           1213 	mov	dptr,#OUT1CS
   03E5 78 07              1214 	mov	r0,#7
   03E7 F0                 1215 2$:	movx	@dptr,a
   03E8 A3                 1216 	inc	dptr
   03E9 A3                 1217 	inc	dptr
   03EA D8 FB              1218 	djnz	r0,2$
   03EC 12 0A 13           1219 	lcall	fillusbintr
   03EF 80 B6              1220 	sjmp	setupackstd
   03F1                    1221 cmdnotsetconf:
                           1222 	;; USB_REQ_SET_INTERFACE
   03F1 B4 0B 1E           1223 	cjne	a,#USB_REQ_SET_INTERFACE,cmdnotsetint
   03F4 90 7F E8           1224 	mov	dptr,#SETUPDAT
   03F7 E0                 1225 	movx	a,@dptr
   03F8 B4 01 AF           1226 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_OUT,setupstallstd
   03FB E5 62              1227 	mov	a,numconfig
   03FD B4 01 AA           1228 	cjne	a,#1,setupstallstd
   0400 90 7F EC           1229 	mov	dptr,#SETUPDAT+4
   0403 E0                 1230 	movx	a,@dptr
   0404 24 FC              1231 	add	a,#-NUMINTERFACES
   0406 40 A2              1232 	jc	setupstallstd
   0408 24 67              1233 	add	a,#NUMINTERFACES+altsetting
   040A F8                 1234 	mov	r0,a
   040B 90 7F EA           1235 	mov	dptr,#SETUPDAT+2
   040E E0                 1236 	movx	a,@dptr
   040F F6                 1237 	mov	@r0,a
   0410 80 AF              1238 	sjmp	cmdresettoggleshalt
   0412                    1239 cmdnotsetint:
                           1240 	;; USB_REQ_GET_INTERFACE
   0412 B4 0A 24           1241 	cjne	a,#USB_REQ_GET_INTERFACE,cmdnotgetint
   0415 90 7F E8           1242 	mov	dptr,#SETUPDAT
   0418 E0                 1243 	movx	a,@dptr
   0419 B4 81 8E           1244 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,setupstallstd
   041C E5 62              1245 	mov	a,numconfig
   041E B4 01 89           1246 	cjne	a,#1,setupstallstd
   0421 90 7F EC           1247 	mov	dptr,#SETUPDAT+4
   0424 E0                 1248 	movx	a,@dptr
   0425 24 FC              1249 	add	a,#-NUMINTERFACES
   0427 40 81              1250 	jc	setupstallstd
   0429 24 67              1251 	add	a,#NUMINTERFACES+altsetting
   042B F8                 1252 	mov	r0,a
   042C E6                 1253 	mov	a,@r0
   042D                    1254 cmdrespondonebyte:
   042D 90 7F 00           1255 	mov	dptr,#IN0BUF
   0430 F0                 1256 	movx	@dptr,a
   0431 90 7F B5           1257 	mov	dptr,#IN0BC
   0434 74 01              1258 	mov	a,#1
   0436 F0                 1259 	movx	@dptr,a	
   0437 80 4E              1260 	sjmp	setupackstd2
   0439                    1261 cmdnotgetint:
                           1262 	;; USB_REQ_GET_CONFIGURATION
   0439 B4 08 0B           1263 	cjne	a,#USB_REQ_GET_CONFIGURATION,cmdnotgetconf
   043C 90 7F E8           1264 	mov	dptr,#SETUPDAT
   043F E0                 1265 	movx	a,@dptr
   0440 B4 80 47           1266 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,setupstallstd2
   0443 E5 62              1267 	mov	a,numconfig
   0445 80 E6              1268 	sjmp	cmdrespondonebyte	
   0447                    1269 cmdnotgetconf:
                           1270 	;; USB_REQ_GET_STATUS (0)
   0447 70 44              1271 	jnz	cmdnotgetstat
   0449 90 7F E8           1272 	mov	dptr,#SETUPDAT
   044C E0                 1273 	movx	a,@dptr
   044D B4 80 11           1274 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,cmdnotgetstatdev
   0450 74 01              1275 	mov	a,#1
   0452                    1276 cmdrespondstat:
   0452 90 7F 00           1277 	mov	dptr,#IN0BUF
   0455 F0                 1278 	movx	@dptr,a
   0456 A3                 1279 	inc	dptr
   0457 E4                 1280 	clr	a
   0458 F0                 1281 	movx	@dptr,a
   0459 90 7F B5           1282 	mov	dptr,#IN0BC
   045C 74 02              1283 	mov	a,#2
   045E F0                 1284 	movx	@dptr,a	
   045F 80 26              1285 	sjmp	setupackstd2
   0461                    1286 cmdnotgetstatdev:
   0461 B4 81 03           1287 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,cmdnotgetstatintf
   0464 E4                 1288 	clr	a
   0465 80 EB              1289 	sjmp	cmdrespondstat
   0467                    1290 cmdnotgetstatintf:
   0467 B4 82 20           1291 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_IN,setupstallstd2
   046A 90 7F EC           1292 	mov	dptr,#SETUPDAT+4
   046D E0                 1293 	movx	a,@dptr
   046E 90 7F C4           1294 	mov	dptr,#OUT1CS-2
   0471 30 E7 03           1295 	jnb	acc.7,0$
   0474 90 7F B4           1296 	mov	dptr,#IN1CS-2
   0477 54 0F              1297 0$:	anl	a,#15
   0479 60 0F              1298 	jz	setupstallstd2
   047B 20 E3 0C           1299 	jb	acc.3,setupstallstd2
   047E 25 E0              1300 	add	a,acc
   0480 25 82              1301 	add	a,dpl0
   0482 F5 82              1302 	mov	dpl0,a
   0484 E0                 1303 	movx	a,@dptr
   0485 80 CB              1304 	sjmp	cmdrespondstat
   0487                    1305 setupackstd2:
   0487 02 07 B7           1306 	ljmp	setupack
   048A                    1307 setupstallstd2:
   048A 02 07 B3           1308 	ljmp	setupstall
   048D                    1309 cmdnotgetstat:
                           1310 	;; USB_REQ_SET_FEATURE
   048D B4 03 05           1311 	cjne	a,#USB_REQ_SET_FEATURE,cmdnotsetftr
   0490 75 F0 01           1312 	mov	b,#1
   0493 80 06              1313 	sjmp	handleftr
   0495                    1314 cmdnotsetftr:
                           1315 	;; USB_REQ_CLEAR_FEATURE
   0495 B4 01 44           1316 	cjne	a,#USB_REQ_CLEAR_FEATURE,cmdnotclrftr
   0498 75 F0 00           1317 	mov	b,#0
   049B                    1318 handleftr:
   049B 90 7F E8           1319 	mov	dptr,#SETUPDAT
   049E E0                 1320 	movx	a,@dptr
   049F B4 02 E8           1321 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_OUT,setupstallstd2
   04A2 A3                 1322 	inc	dptr
   04A3 A3                 1323 	inc	dptr
   04A4 E0                 1324 	movx	a,@dptr
   04A5 70 E3              1325 	jnz	setupstallstd2	; not ENDPOINT_HALT feature
   04A7 A3                 1326 	inc	dptr
   04A8 E0                 1327 	movx	a,@dptr
   04A9 70 DF              1328 	jnz	setupstallstd2
   04AB A3                 1329 	inc	dptr
   04AC E0                 1330 	movx	a,@dptr
   04AD 90 7F C4           1331 	mov	dptr,#OUT1CS-2
   04B0 30 E7 05           1332 	jnb	acc.7,0$
   04B3 90 7F B4           1333 	mov	dptr,#IN1CS-2
   04B6 44 10              1334 	orl	a,#0x10
   04B8 20 E3 CF           1335 0$:	jb	acc.3,setupstallstd2
                           1336 	;; clear data toggle
   04BB 54 1F              1337 	anl	a,#0x1f
   04BD 05 86              1338 	inc	dps
   04BF 90 7F D7           1339 	mov	dptr,#TOGCTL
   04C2 F0                 1340 	movx	@dptr,a
   04C3 44 20              1341 	orl	a,#0x20
   04C5 F0                 1342 	movx	@dptr,a
   04C6 54 0F              1343 	anl	a,#15
   04C8 F0                 1344 	movx	@dptr,a
   04C9 15 86              1345 	dec	dps	
                           1346 	;; clear/set ep halt feature
   04CB 25 E0              1347 	add	a,acc
   04CD 25 82              1348 	add	a,dpl0
   04CF F5 82              1349 	mov	dpl0,a
   04D1 E5 F0              1350 	mov	a,b
   04D3 F0                 1351 	movx	@dptr,a
   04D4 80 B1              1352 	sjmp	setupackstd2
                           1353 
   04D6                    1354 cmdnotc0_1:
   04D6 02 05 6E           1355 	ljmp	cmdnotc0
   04D9                    1356 setupstallc0_1:
   04D9 02 07 B3           1357 	ljmp	setupstall
                           1358 	
   04DC                    1359 cmdnotclrftr:
                           1360 	;; vendor specific commands
                           1361 	;; 0xc0
   04DC B4 C0 F7           1362 	cjne	a,#0xc0,cmdnotc0_1
   04DF 90 7F E8           1363 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   04E2 E0                 1364 	movx	a,@dptr
   04E3 B4 C0 F3           1365 	cjne	a,#0xc0,setupstallc0_1
                           1366 	;; fill status buffer
   04E6 E5 4C              1367 	mov	a,pttforce
   04E8 F5 F0              1368 	mov	b,a
   04EA A2 E0              1369 	mov	c,acc.0
   04EC 92 F2              1370 	mov	b.2,c
   04EE 90 C0 09           1371 	mov	dptr,#AUDIOSTAT
   04F1 E0                 1372 	movx	a,@dptr
   04F2 A2 E0              1373 	mov	c,acc.0
   04F4 B3                 1374 	cpl	c
   04F5 92 F3              1375 	mov	b.3,c
   04F7 A2 09              1376 	mov	c,uartempty
   04F9 92 F5              1377 	mov	b.5,c
   04FB E4                 1378 	clr	a
   04FC 90 7F 04           1379 	mov	dptr,#(IN0BUF+4)
   04FF F0                 1380 	movx	@dptr,a
                           1381 	;; bytewide elements
   0500 90 7F 00           1382 	mov	dptr,#(IN0BUF)
   0503 E5 F0              1383 	mov	a,b
   0505 F0                 1384 	movx	@dptr,a
   0506 E4                 1385 	clr	a
   0507 90 7F 01           1386 	mov	dptr,#(IN0BUF+1)
   050A F0                 1387 	movx	@dptr,a
   050B 90 7F 02           1388 	mov	dptr,#(IN0BUF+2)
   050E F0                 1389 	movx	@dptr,a
   050F 90 C0 04           1390 	mov	dptr,#AUDIORSSI
   0512 E0                 1391 	movx	a,@dptr
   0513 90 7F 03           1392 	mov	dptr,#(IN0BUF+3)
   0516 F0                 1393 	movx	@dptr,a
                           1394 	;; counter
   0517 05 44              1395 	inc	irqcount
   0519 E5 44              1396 	mov	a,irqcount
   051B 90 7F 05           1397 	mov	dptr,#(IN0BUF+5)
   051E F0                 1398 	movx	@dptr,a
                           1399 	;; additional fields (HDLC state mach)
   051F E4                 1400 	clr	a
   0520 A3                 1401 	inc	dptr
   0521 F0                 1402 	movx	@dptr,a
   0522 A3                 1403 	inc	dptr
   0523 F0                 1404 	movx	@dptr,a
   0524 A3                 1405 	inc	dptr
   0525 F0                 1406 	movx	@dptr,a
   0526 A3                 1407 	inc	dptr
   0527 F0                 1408 	movx	@dptr,a
   0528 A3                 1409 	inc	dptr
   0529 F0                 1410 	movx	@dptr,a
   052A A3                 1411 	inc	dptr
   052B F0                 1412 	movx	@dptr,a
   052C A3                 1413 	inc	dptr
   052D F0                 1414 	movx	@dptr,a
   052E A3                 1415 	inc	dptr
   052F F0                 1416 	movx	@dptr,a
   0530 A3                 1417 	inc	dptr
   0531 F0                 1418 	movx	@dptr,a
   0532 A3                 1419 	inc	dptr
   0533 F0                 1420 	movx	@dptr,a
   0534 A3                 1421 	inc	dptr
   0535 F0                 1422 	movx	@dptr,a
   0536 A3                 1423 	inc	dptr
   0537 F0                 1424 	movx	@dptr,a
                           1425 	;; FPGA registers
   0538 90 C0 01           1426 	mov	dptr,#AUDIORXFIFOCNT
   053B E0                 1427 	movx	a,@dptr
   053C 90 7F 12           1428 	mov	dptr,#(IN0BUF+18)
   053F F0                 1429 	movx	@dptr,a
   0540 90 C0 02           1430 	mov	dptr,#AUDIOTXFIFOCNT
   0543 E0                 1431 	movx	a,@dptr
   0544 90 7F 13           1432 	mov	dptr,#(IN0BUF+19)
   0547 F0                 1433 	movx	@dptr,a
   0548 E5 20              1434 	mov	a,ctrlreg
   054A A3                 1435 	inc	dptr
   054B F0                 1436 	movx	@dptr,a
   054C 90 C0 09           1437 	mov	dptr,#AUDIOSTAT
   054F E0                 1438 	movx	a,@dptr
   0550 90 7F 15           1439 	mov	dptr,#(IN0BUF+21)
   0553 F0                 1440 	movx	@dptr,a
                           1441 	;; Anchor Registers
   0554 90 7F C8           1442 	mov	dptr,#OUT2CS
   0557 E0                 1443 	movx	a,@dptr
   0558 90 7F 16           1444 	mov	dptr,#(IN0BUF+22)
   055B F0                 1445 	movx	@dptr,a
                           1446 	;; set length
   055C 90 7F EE           1447 	mov	dptr,#SETUPDAT+6	; wLength
   055F E0                 1448 	movx	a,@dptr
   0560 24 E9              1449 	add	a,#-(6+12+4+1)
   0562 50 01              1450 	jnc	4$
   0564 E4                 1451 	clr	a
   0565 24 17              1452 4$:	add	a,#(6+12+4+1)
   0567 90 7F B5           1453 	mov	dptr,#IN0BC
   056A F0                 1454 	movx	@dptr,a
   056B 02 07 B7           1455 	ljmp	setupack
   056E                    1456 cmdnotc0:
                           1457 	;; 0xc8
   056E B4 C8 19           1458 	cjne	a,#0xc8,cmdnotc8
   0571 90 7F E8           1459 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0574 E0                 1460 	movx	a,@dptr
   0575 B4 C0 0F           1461 	cjne	a,#0xc0,setupstallc8
   0578 74 04              1462 	mov	a,#4
   057A 90 7F 00           1463 	mov	dptr,#IN0BUF
   057D F0                 1464 	movx	@dptr,a
   057E 90 7F B5           1465 	mov	dptr,#IN0BC
   0581 74 01              1466 	mov	a,#1
   0583 F0                 1467 	movx	@dptr,a
   0584 02 07 B7           1468 	ljmp	setupack
   0587                    1469 setupstallc8:
   0587 02 07 B3           1470 	ljmp	setupstall
   058A                    1471 cmdnotc8:
                           1472 	;; 0xc9
   058A B4 C9 21           1473 	cjne	a,#0xc9,cmdnotc9
   058D 90 7F E8           1474 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0590 E0                 1475 	movx	a,@dptr
   0591 B4 C0 17           1476 	cjne	a,#0xc0,setupstallc9
   0594 90 7F 00           1477 	mov	dptr,#IN0BUF
   0597 78 F0              1478 	mov	r0,#parserial
   0599 E2                 1479 0$:	movx	a,@r0
   059A 60 05              1480 	jz	1$
   059C F0                 1481 	movx	@dptr,a
   059D 08                 1482 	inc	r0
   059E A3                 1483 	inc	dptr
   059F 80 F8              1484 	sjmp	0$
   05A1 E8                 1485 1$:	mov	a,r0
   05A2 24 10              1486 	add	a,#-0xf0	; -parserial
   05A4 90 7F B5           1487 	mov	dptr,#IN0BC
   05A7 F0                 1488 	movx	@dptr,a
   05A8 02 07 B7           1489 	ljmp	setupack
   05AB                    1490 setupstallc9:
   05AB 02 07 B3           1491 	ljmp	setupstall
   05AE                    1492 cmdnotc9:
                           1493 	;; 0xd0
   05AE B4 D0 45           1494 	cjne	a,#0xd0,cmdnotd0
   05B1 90 7F E8           1495 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   05B4 E0                 1496 	movx	a,@dptr
   05B5 B4 C0 3B           1497 	cjne	a,#0xc0,setupstalld0
   05B8 90 7F EC           1498 	mov	dptr,#SETUPDAT+4	; wIndex
   05BB E0                 1499 	movx	a,@dptr
   05BC B4 01 08           1500 	cjne	a,#1,0$
   05BF 90 7F EA           1501 	mov	dptr,#SETUPDAT+2	; wValue
   05C2 E0                 1502 	movx	a,@dptr
   05C3 54 01              1503 	anl	a,#1
   05C5 F5 4C              1504 	mov	pttforce,a
   05C7                    1505 0$:	;; PTT status
   05C7 90 7F 00           1506 	mov	dptr,#IN0BUF
   05CA E5 4C              1507 	mov	a,pttforce
   05CC F0                 1508 	movx	@dptr,a
                           1509 	;; DCD status
   05CD 90 C0 09           1510 	mov	dptr,#AUDIOSTAT
   05D0 E0                 1511 	movx	a,@dptr
   05D1 54 01              1512 	anl	a,#1
   05D3 64 01              1513 	xrl	a,#1
   05D5 90 7F 01           1514 	mov	dptr,#IN0BUF+1
   05D8 F0                 1515 	movx	@dptr,a
                           1516 	;; RSSI
   05D9 90 C0 04           1517 	mov	dptr,#AUDIORSSI
   05DC E0                 1518 	movx	a,@dptr
   05DD 90 7F 02           1519 	mov	dptr,#IN0BUF+2
   05E0 F0                 1520 	movx	@dptr,a
                           1521 	;; length
   05E1 90 7F EE           1522 	mov	dptr,#SETUPDAT+6	; wLength
   05E4 E0                 1523 	movx	a,@dptr
   05E5 24 FD              1524 	add	a,#-3
   05E7 50 01              1525 	jnc	2$
   05E9 E4                 1526 	clr	a
   05EA 24 03              1527 2$:	add	a,#3
   05EC 90 7F B5           1528 	mov	dptr,#IN0BC
   05EF F0                 1529 	movx	@dptr,a
   05F0 02 07 B7           1530 	ljmp	setupack
   05F3                    1531 setupstalld0:
   05F3 02 07 B3           1532 	ljmp	setupstall
   05F6                    1533 cmdnotd0:
                           1534 	;; 0xd2
   05F6 B4 D2 20           1535 	cjne	a,#0xd2,cmdnotd2
   05F9 90 7F E8           1536 	mov	dptr,#SETUPDAT	; bRequestType == 0x40
   05FC E0                 1537 	movx	a,@dptr
   05FD B4 40 16           1538 	cjne	a,#0x40,setupstalld2
   0600 90 7F EA           1539 	mov	dptr,#SETUPDAT+2	; wValue
   0603 E0                 1540 	movx	a,@dptr
   0604 F5 F0              1541 	mov	b,a
   0606 90 7F 98           1542 	mov	dptr,#OUTC
   0609 E0                 1543 	movx	a,@dptr
   060A A2 F0              1544 	mov	c,b.0
   060C 92 E3              1545 	mov	acc.3,c
   060E A2 F1              1546 	mov	c,b.1
   0610 92 E5              1547 	mov	acc.5,c
   0612 F0                 1548 	movx	@dptr,a
   0613 02 07 B7           1549 	ljmp	setupack
   0616                    1550 setupstalld2:
   0616 02 07 B3           1551 	ljmp	setupstall
   0619                    1552 cmdnotd2:
                           1553 	;; 0xd3
   0619 B4 D3 16           1554 	cjne	a,#0xd3,cmdnotd3
   061C 90 7F E8           1555 	mov	dptr,#SETUPDAT	; bRequestType == 0x40
   061F E0                 1556 	movx	a,@dptr
   0620 B4 40 07           1557 	cjne	a,#0x40,setupstalld3
   0623 90 7F EA           1558 	mov	dptr,#SETUPDAT+2	; wValue
   0626 E0                 1559 	movx	a,@dptr
   0627 10 09 03           1560 	jbc	uartempty,cmdd2cont
   062A                    1561 setupstalld3:
   062A 02 07 B3           1562 	ljmp	setupstall
   062D                    1563 cmdd2cont:
   062D F5 99              1564 	mov	sbuf0,a
   062F 02 07 B7           1565 	ljmp	setupack
   0632                    1566 cmdnotd3:
                           1567 	;; 0xd4
   0632 B4 D4 49           1568 	cjne	a,#0xd4,cmdnotd4
   0635 90 7F E8           1569 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0638 E0                 1570 	movx	a,@dptr
   0639 B4 C0 3F           1571 	cjne	a,#0xc0,setupstalld4
   063C 90 7F EC           1572 	mov	dptr,#SETUPDAT+4	; wIndex
   063F E0                 1573 	movx	a,@dptr
   0640 90 7F EA           1574 	mov	dptr,#SETUPDAT+2	; wValue
   0643 B4 01 09           1575 	cjne	a,#1,0$
   0646 E0                 1576 	movx	a,@dptr
   0647 90 C0 0D           1577 	mov	dptr,#AUDIOMDISCOUT
   064A F0                 1578 	movx	@dptr,a
   064B F5 61              1579 	mov	mdiscout,a
   064D 80 0A              1580 	sjmp	1$
   064F B4 02 07           1581 0$:	cjne	a,#2,1$
   0652 E0                 1582 	movx	a,@dptr
   0653 90 C0 0C           1583 	mov	dptr,#AUDIOMDISCTRIS
   0656 F0                 1584 	movx	@dptr,a
   0657 F5 60              1585 	mov	mdisctris,a
   0659 90 C0 0E           1586 1$:	mov	dptr,#AUDIOMDISCIN
   065C E0                 1587 	movx	a,@dptr
   065D 90 7F 00           1588 	mov	dptr,#IN0BUF+0
   0660 F0                 1589 	movx	@dptr,a
   0661 E5 61              1590 	mov	a,mdiscout
   0663 A3                 1591 	inc	dptr
   0664 F0                 1592 	movx	@dptr,a
   0665 E5 60              1593 	mov	a,mdisctris
   0667 A3                 1594 	inc	dptr
   0668 F0                 1595 	movx	@dptr,a
                           1596 	;; length
   0669 90 7F EE           1597 	mov	dptr,#SETUPDAT+6	; wLength
   066C E0                 1598 	movx	a,@dptr
   066D 24 FD              1599 	add	a,#-3
   066F 50 01              1600 	jnc	2$
   0671 E4                 1601 	clr	a
   0672 24 03              1602 2$:	add	a,#3
   0674 90 7F B5           1603 	mov	dptr,#IN0BC
   0677 F0                 1604 	movx	@dptr,a
   0678 02 07 B7           1605 	ljmp	setupack
   067B                    1606 setupstalld4:
   067B 02 07 B3           1607 	ljmp	setupstall
   067E                    1608 cmdnotd4:
                           1609 	;; 0xd5
   067E B4 D5 3F           1610 	cjne	a,#0xd5,cmdnotd5
   0681 90 7F E8           1611 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0684 E0                 1612 	movx	a,@dptr
   0685 B4 C0 32           1613 	cjne	a,#0xc0,setupstalld5
   0688 90 7F EC           1614 	mov	dptr,#SETUPDAT+4	; wIndex
   068B E0                 1615 	movx	a,@dptr
   068C B4 01 0A           1616 	cjne	a,#1,0$
   068F 90 7F EA           1617 	mov	dptr,#SETUPDAT+2	; wValue
   0692 E0                 1618 	movx	a,@dptr
   0693 90 C0 0A           1619 	mov	dptr,#AUDIOT7FOUT
   0696 F0                 1620 	movx	@dptr,a
   0697 F5 5F              1621 	mov	t7fout,a
   0699 90 C0 0B           1622 0$:	mov	dptr,#AUDIOT7FIN
   069C E0                 1623 	movx	a,@dptr
   069D 90 7F 00           1624 	mov	dptr,#IN0BUF+0
   06A0 F0                 1625 	movx	@dptr,a
   06A1 E5 5F              1626 	mov	a,t7fout
   06A3 A3                 1627 	inc	dptr
   06A4 90 7F 01           1628 	mov	dptr,#IN0BUF+1
   06A7 F0                 1629 	movx	@dptr,a
                           1630 	;; length
   06A8 90 7F EE           1631 	mov	dptr,#SETUPDAT+6	; wLength
   06AB E0                 1632 	movx	a,@dptr
   06AC 24 FE              1633 	add	a,#-2
   06AE 50 01              1634 	jnc	2$
   06B0 E4                 1635 	clr	a
   06B1 24 02              1636 2$:	add	a,#2
   06B3 90 7F B5           1637 	mov	dptr,#IN0BC
   06B6 F0                 1638 	movx	@dptr,a
   06B7 02 07 B7           1639 	ljmp	setupack
   06BA                    1640 setupstalld5:
   06BA                    1641 setupstalle0:
   06BA 02 07 B3           1642 	ljmp	setupstall
   06BD                    1643 cmdnote0_0:
   06BD 02 07 53           1644 	ljmp    cmdnote0
   06C0                    1645 cmdnotd5:
                           1646 	;; 0xe0
   06C0 B4 E0 FA           1647 	cjne	a,#0xe0,cmdnote0_0
   06C3 90 7F E8           1648 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   06C6 E0                 1649 	movx	a,@dptr
   06C7 B4 C0 F0           1650 	cjne	a,#0xc0,setupstalle0
                           1651 	;; set PTT, LED's and counter mode
   06CA 90 7F EC           1652 	mov	dptr,#SETUPDAT+4	; wIndex
   06CD E0                 1653 	movx	a,@dptr
   06CE B4 01 12           1654 	cjne	a,#1,1$
   06D1 90 7F EA           1655 	mov	dptr,#SETUPDAT+2	; wValue
   06D4 E0                 1656 	movx	a,@dptr
   06D5 54 78              1657 	anl	a,#0x78
   06D7 C5 20              1658 	xch	a,ctrlreg
   06D9 54 07              1659 	anl	a,#0x07
   06DB 45 20              1660 	orl	a,ctrlreg
   06DD F5 20              1661 	mov	ctrlreg,a
   06DF 90 C0 08           1662 	mov	dptr,#AUDIOCTRL
   06E2 F0                 1663 	movx	@dptr,a
   06E3 90 7F EE           1664 1$:	mov	dptr,#SETUPDAT+6	; wLength
   06E6 E0                 1665 	movx	a,@dptr
   06E7 60 0D              1666 	jz	3$
   06E9 B4 01 11           1667 	cjne	a,#1,2$
   06EC E5 20              1668 	mov	a,ctrlreg
   06EE 90 7F 00           1669 	mov	dptr,#IN0BUF
   06F1 54 7F              1670 	anl	a,#0x7f
   06F3 F0                 1671 	movx	@dptr,a
   06F4 74 01              1672 	mov	a,#1
   06F6 90 7F B5           1673 3$:	mov	dptr,#IN0BC
   06F9 F0                 1674 	movx	@dptr,a
   06FA 02 07 B7           1675 	ljmp	setupack
   06FD 90 C0 08           1676 2$:	mov	dptr,#AUDIOCTRL
   0700 D2 07              1677 	setb	ctrl_cntsel
   0702 E5 20              1678 	mov	a,ctrlreg
   0704 F0                 1679 	movx	@dptr,a
   0705 90 C0 05           1680 	mov	dptr,#AUDIOCNTLOW
   0708 E0                 1681 	movx	a,@dptr
   0709 90 7F 04           1682 	mov	dptr,#IN0BUF+4
   070C F0                 1683 	movx	@dptr,a
   070D 90 C0 06           1684 	mov	dptr,#AUDIOCNTMID
   0710 E0                 1685 	movx	a,@dptr
   0711 90 7F 05           1686 	mov	dptr,#IN0BUF+5
   0714 F0                 1687 	movx	@dptr,a
   0715 90 C0 07           1688 	mov	dptr,#AUDIOCNTHIGH
   0718 E0                 1689 	movx	a,@dptr
   0719 90 7F 06           1690 	mov	dptr,#IN0BUF+6
   071C F0                 1691 	movx	@dptr,a
   071D 90 C0 08           1692 	mov	dptr,#AUDIOCTRL
   0720 C2 07              1693 	clr	ctrl_cntsel
   0722 E5 20              1694 	mov	a,ctrlreg
   0724 F0                 1695 	movx	@dptr,a
   0725 90 7F 00           1696 	mov	dptr,#IN0BUF
   0728 F0                 1697 	movx	@dptr,a
   0729 90 C0 05           1698 	mov	dptr,#AUDIOCNTLOW
   072C E0                 1699 	movx	a,@dptr
   072D 90 7F 01           1700 	mov	dptr,#IN0BUF+1
   0730 F0                 1701 	movx	@dptr,a
   0731 90 C0 06           1702 	mov	dptr,#AUDIOCNTMID
   0734 E0                 1703 	movx	a,@dptr
   0735 90 7F 02           1704 	mov	dptr,#IN0BUF+2
   0738 F0                 1705 	movx	@dptr,a
   0739 90 C0 07           1706 	mov	dptr,#AUDIOCNTHIGH
   073C E0                 1707 	movx	a,@dptr
   073D 90 7F 03           1708 	mov	dptr,#IN0BUF+3
   0740 F0                 1709 	movx	@dptr,a
                           1710 	;; length
   0741 90 7F EE           1711 	mov	dptr,#SETUPDAT+6	; wLength
   0744 E0                 1712 	movx	a,@dptr
   0745 24 F9              1713 	add	a,#-7
   0747 50 01              1714 	jnc	4$
   0749 E4                 1715 	clr	a
   074A 24 07              1716 4$:	add	a,#7
   074C 90 7F B5           1717 	mov	dptr,#IN0BC
   074F F0                 1718 	movx	@dptr,a
   0750 02 07 B7           1719 	ljmp	setupack
   0753                    1720 cmdnote0:
                           1721 	;; 0xe1
   0753 B4 E1 3A           1722 	cjne	a,#0xe1,cmdnote1
   0756 90 7F E8           1723 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   0759 E0                 1724 	movx	a,@dptr
   075A B4 C0 30           1725 	cjne	a,#0xc0,setupstalle1
   075D 90 7F 00           1726 	mov	dptr,#IN0BUF
   0760 E5 45              1727 	mov	a,sofcount
   0762 F0                 1728 	movx	@dptr,a
   0763 A3                 1729 	inc	dptr
   0764 E5 49              1730 	mov	a,framesize
   0766 F0                 1731 	movx	@dptr,a
   0767 A3                 1732 	inc	dptr
   0768 E5 46              1733 	mov	a,divider
   076A F0                 1734 	movx	@dptr,a
   076B A3                 1735 	inc	dptr
   076C E5 47              1736 	mov	a,divider+1
   076E F0                 1737 	movx	@dptr,a
   076F A3                 1738 	inc	dptr
   0770 E5 48              1739 	mov	a,divrel
   0772 F0                 1740 	movx	@dptr,a
   0773 A3                 1741 	inc	dptr
   0774 E5 4A              1742 	mov	a,pllcorrvar
   0776 F0                 1743 	movx	@dptr,a
   0777 A3                 1744 	inc	dptr
   0778 E5 4B              1745 	mov	a,txfifocount
   077A F0                 1746 	movx	@dptr,a
                           1747 
                           1748 	;mov	dptr,#AUDIOTXFIFOCNT
                           1749 	;movx	a,@dptr
                           1750 	;mov	dptr,#IN0BUF+6
                           1751 	;movx	@dptr,a
                           1752 
                           1753 	;; length
   077B 90 7F EE           1754 	mov	dptr,#SETUPDAT+6	; wLength
   077E E0                 1755 	movx	a,@dptr
   077F 24 F9              1756 	add	a,#-7
   0781 50 01              1757 	jnc	1$
   0783 E4                 1758 	clr	a
   0784 24 07              1759 1$:	add	a,#7
   0786 90 7F B5           1760 	mov	dptr,#IN0BC
   0789 F0                 1761 	movx	@dptr,a
   078A 02 07 B7           1762 	ljmp	setupack
   078D                    1763 setupstalle1:
   078D 02 07 B3           1764 	ljmp	setupstall
   0790                    1765 cmdnote1:
                           1766 	;; 0xe2
   0790 B4 E2 20           1767 	cjne	a,#0xe2,cmdnote2
   0793 90 7F E8           1768 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   0796 E0                 1769 	movx	a,@dptr
   0797 B4 C0 16           1770 	cjne	a,#0xc0,setupstalle2
   079A 90 7F 00           1771 	mov	dptr,#IN0BUF
   079D 78 80              1772 	mov	r0,#txsamples
   079F AF 49              1773 	mov	r7,framesize
   07A1 E6                 1774 1$:	mov	a,@r0
   07A2 F0                 1775 	movx	@dptr,a
   07A3 08                 1776 	inc	r0
   07A4 A3                 1777 	inc	dptr
   07A5 DF FA              1778 	djnz	r7,1$
   07A7 90 7F B5           1779 	mov	dptr,#IN0BC
   07AA E5 49              1780 	mov	a,framesize
   07AC F0                 1781 	movx	@dptr,a
   07AD 02 07 B7           1782 	ljmp	setupack
   07B0                    1783 setupstalle2:
   07B0 02 07 B3           1784 	ljmp	setupstall
   07B3                    1785 cmdnote2:
                           1786 
                           1787 	;; unknown commands fall through to setupstall
                           1788 
   07B3                    1789 setupstall:
   07B3 74 03              1790 	mov	a,#3
   07B5 80 02              1791 	sjmp	endsetup
   07B7                    1792 setupack:
   07B7 74 02              1793 	mov	a,#2
   07B9                    1794 endsetup:
   07B9 90 7F B4           1795 	mov	dptr,#EP0CS
   07BC F0                 1796 	movx	@dptr,a
   07BD                    1797 endusbisr:
                           1798 	;; epilogue
   07BD D0 07              1799 	pop	ar7
   07BF D0 00              1800 	pop	ar0
   07C1 D0 86              1801 	pop	dps
   07C3 D0 D0              1802 	pop	psw
   07C5 D0 85              1803 	pop	dph1
   07C7 D0 84              1804 	pop	dpl1
   07C9 D0 83              1805 	pop	dph0
   07CB D0 82              1806 	pop	dpl0
   07CD D0 F0              1807 	pop	b
   07CF D0 E0              1808 	pop	acc
   07D1 32                 1809 	reti
                           1810 
   07D2                    1811 usb_sof_isr:
   07D2 C0 E0              1812 	push	acc
   07D4 C0 F0              1813 	push	b
   07D6 C0 82              1814 	push	dpl0
   07D8 C0 83              1815 	push	dph0
   07DA C0 84              1816 	push	dpl1
   07DC C0 85              1817 	push	dph1
   07DE C0 D0              1818 	push	psw
   07E0 75 D0 00           1819 	mov	psw,#0x00
   07E3 C0 86              1820 	push	dps
   07E5 75 86 00           1821 	mov	dps,#0
   07E8 C0 02              1822 	push	ar2
   07EA C0 03              1823 	push	ar3
   07EC C0 00              1824 	push	ar0
                           1825 	;; clear interrupt
   07EE E5 91              1826 	mov	a,exif
   07F0 C2 E4              1827 	clr	acc.4
   07F2 F5 91              1828 	mov	exif,a
   07F4 90 7F AB           1829 	mov	dptr,#USBIRQ
   07F7 74 02              1830 	mov	a,#0x02
   07F9 F0                 1831 	movx	@dptr,a
                           1832 	;; handle interrupt
   07FA 05 45              1833 	inc	sofcount
                           1834 	;; debugging: we have two cases
                           1835 	;; if the ISO frame size is just 2 bytes,
                           1836 	;; we return the frame number, otherwise
                           1837 	;; the required numbers of samples
   07FC E5 49              1838 	mov	a,framesize
   07FE B4 02 14           1839 	cjne	a,#2,1$
                           1840 	;; copy the frame number
   0801 90 7F D8           1841 	mov	dptr,#USBFRAMEL
   0804 E0                 1842 	movx	a,@dptr
   0805 A3                 1843 	inc	dptr
   0806 05 86              1844 	inc	dps
   0808 90 7F 68           1845 	mov	dptr,#IN8DATA
   080B F0                 1846 	movx	@dptr,a
   080C 15 86              1847 	dec	dps
   080E E0                 1848 	movx	a,@dptr
   080F 05 86              1849 	inc	dps
   0811 F0                 1850 	movx	@dptr,a
   0812 02 09 05           1851 	ljmp	sofendisr
   0815 B4 03 36           1852 1$:	cjne	a,#3,sofsampleisr
   0818 90 7F A0           1853 	mov	dptr,#ISOERR
   081B E0                 1854 	movx	a,@dptr
   081C 20 E0 23           1855 	jb	acc.0,3$
   081F 90 7F 70           1856 	mov	dptr,#OUT8BCH
   0822 E0                 1857 	movx	a,@dptr
   0823 70 1D              1858 	jnz	3$
   0825 90 7F 71           1859 	mov	dptr,#OUT8BCL
   0828 E0                 1860 	movx	a,@dptr
   0829 B4 03 16           1861 	cjne	a,#3,3$
   082C FA                 1862 	mov	r2,a
   082D 90 7F 60           1863 	mov	dptr,#OUT8DATA
   0830 05 86              1864 	inc	dps
   0832 90 7F 68           1865 	mov	dptr,#IN8DATA
   0835 15 86              1866 	dec	dps
   0837 E0                 1867 2$:	movx	a,@dptr
   0838 05 86              1868 	inc	dps
   083A F0                 1869 	movx	@dptr,a
   083B 15 86              1870 	dec	dps
   083D DA F8              1871 	djnz	r2,2$
   083F 02 09 05           1872 	ljmp	sofendisr
   0842 7A 03              1873 3$:	mov	r2,#3
   0844 90 7F 68           1874 	mov	dptr,#IN8DATA
   0847 E4                 1875 	clr	a
   0848 F0                 1876 4$:	movx	@dptr,a
   0849 DA FD              1877 	djnz	r2,4$
   084B 02 09 05           1878 	ljmp	sofendisr
   084E                    1879 sofsampleisr:
   084E 90 C0 01           1880 	mov	dptr,#AUDIORXFIFOCNT
   0851 E0                 1881 	movx	a,@dptr
   0852 C3                 1882 	clr	c
   0853 95 49              1883 	subb	a,framesize
   0855 E0                 1884 	movx	a,@dptr
   0856 40 02              1885 	jc	1$
   0858 E5 49              1886 	mov	a,framesize
   085A FA                 1887 1$:	mov	r2,a
   085B FB                 1888 	mov	r3,a
   085C 60 12              1889 	jz	2$
   085E 90 C0 00           1890 	mov	dptr,#AUDIORXFIFO
   0861 05 86              1891 	inc	dps
   0863 90 7F 68           1892 	mov	dptr,#IN8DATA
   0866 15 86              1893 	dec	dps
   0868 E0                 1894 3$:	movx	a,@dptr
   0869 05 86              1895 	inc	dps
   086B F0                 1896 	movx	@dptr,a
   086C 15 86              1897 	dec	dps
   086E DA F8              1898 	djnz	r2,3$
   0870 E5 49              1899 2$:	mov	a,framesize
   0872 C3                 1900 	clr	c
   0873 9B                 1901 	subb	a,r3
   0874 60 08              1902 	jz	4$
   0876 FA                 1903 	mov	r2,a
   0877 90 7F 68           1904 	mov	dptr,#IN8DATA
   087A E4                 1905 	clr	a
   087B F0                 1906 5$:	movx	@dptr,a
   087C DA FD              1907 	djnz	r2,5$
                           1908 	;; sample rate adjustment
   087E 90 C0 01           1909 4$:	mov	dptr,#AUDIORXFIFOCNT
   0881 E0                 1910 	movx	a,@dptr
   0882 F5 4A              1911 	mov	pllcorrvar,a
   0884 24 FC              1912 	add	a,#-4
   0886 60 05              1913 	jz	10$		; no adjustment
   0888 33                 1914 	rlc	a
   0889 95 E0              1915 	subb	a,acc
   088B D2 E0              1916 	setb	acc.0
   088D FB                 1917 10$:	mov	r3,a		; r3 & acc contain the adjust value
   088E 25 48              1918 	add	a,divrel
                           1919 	;; zero is the guard value
   0890 60 02              1920 	jz	11$
   0892 F5 48              1921 	mov	divrel,a
   0894 E5 48              1922 11$:	mov	a,divrel
   0896 03                 1923 	rr	a
   0897 03                 1924 	rr	a
   0898 03                 1925 	rr	a
   0899 03                 1926 	rr	a
   089A 54 0F              1927 	anl	a,#0xf
   089C 2B                 1928 	add	a,r3		; momentary correction
   089D 25 46              1929 	add	a,divider
   089F FA                 1930 	mov	r2,a
   08A0 E5 47              1931 	mov	a,divider+1
   08A2 34 00              1932 	addc	a,#0
   08A4 90 C0 02           1933 	mov	dptr,#AUDIODIVIDERHI
   08A7 F0                 1934 	movx	@dptr,a
   08A8 EA                 1935 	mov	a,r2
   08A9 90 C0 01           1936 	mov	dptr,#AUDIODIVIDERLO
   08AC F0                 1937 	movx	@dptr,a
                           1938 	;; OUT data
   08AD 90 7F A0           1939 	mov	dptr,#ISOERR
   08B0 E0                 1940 	movx	a,@dptr
   08B1 20 E0 39           1941 	jb	acc.0,20$
   08B4 90 7F 70           1942 	mov	dptr,#OUT8BCH
   08B7 E0                 1943 	movx	a,@dptr
   08B8 70 33              1944 	jnz	20$
   08BA 90 7F 71           1945 	mov	dptr,#OUT8BCL
   08BD E0                 1946 	movx	a,@dptr
   08BE C3                 1947 	clr	c
   08BF 95 49              1948 	subb	a,framesize
   08C1 70 2A              1949 	jnz	20$
   08C3 90 C0 02           1950 	mov	dptr,#AUDIOTXFIFOCNT
   08C6 E0                 1951 	movx	a,@dptr
   08C7 F5 4B              1952 	mov	txfifocount,a
   08C9 C3                 1953 	clr	c
   08CA 95 49              1954 	subb	a,framesize
   08CC E5 4B              1955 	mov	a,txfifocount
   08CE 40 02              1956 	jc	14$
   08D0 E5 49              1957 	mov	a,framesize
   08D2 60 17              1958 14$:	jz	13$
   08D4 FA                 1959 	mov	r2,a
   08D5 90 7F 60           1960 	mov	dptr,#OUT8DATA
   08D8 05 86              1961 	inc	dps
   08DA 90 C0 00           1962 	mov	dptr,#AUDIOTXFIFO
   08DD 15 86              1963 	dec	dps
   08DF 78 80              1964 	mov	r0,#txsamples
   08E1 E0                 1965 12$:	movx	a,@dptr
   08E2 05 86              1966 	inc	dps
   08E4 F0                 1967 	movx	@dptr,a
   08E5 15 86              1968 	dec	dps
   08E7 F6                 1969 	mov	@r0,a
   08E8 08                 1970 	inc	r0
   08E9 DA F6              1971 	djnz	r2,12$
   08EB 80 18              1972 13$:	sjmp	sofendisr
                           1973 	;; initialize TX if invalid packet
   08ED 90 C0 02           1974 20$:	mov	dptr,#AUDIOTXFIFOCNT
   08F0 E0                 1975 	movx	a,@dptr
   08F1 F5 4B              1976 	mov	txfifocount,a
   08F3 24 FC              1977 	add	a,#-4
   08F5 54 3F              1978 	anl	a,#0x3f
   08F7 60 0C              1979 	jz	sofendisr
   08F9 FA                 1980 	mov	r2,a
   08FA 90 C0 00           1981 	mov	dptr,#AUDIOTXFIFO
   08FD E4                 1982 	clr	a
   08FE 78 80              1983 	mov	r0,#txsamples
   0900 F0                 1984 21$:	movx	@dptr,a
   0901 F6                 1985 	mov	@r0,a
   0902 08                 1986 	inc	r0
   0903 DA FB              1987 	djnz	r2,21$
                           1988 	
   0905                    1989 sofendisr:
                           1990 	;; epilogue
   0905 D0 00              1991 	pop	ar0
   0907 D0 03              1992 	pop	ar3
   0909 D0 02              1993 	pop	ar2
   090B D0 86              1994 	pop	dps
   090D D0 D0              1995 	pop	psw
   090F D0 85              1996 	pop	dph1
   0911 D0 84              1997 	pop	dpl1
   0913 D0 83              1998 	pop	dph0
   0915 D0 82              1999 	pop	dpl0
   0917 D0 F0              2000 	pop	b
   0919 D0 E0              2001 	pop	acc
   091B 32                 2002 	reti
                           2003 
                           2004 
   091C                    2005 usb_sutok_isr:
   091C C0 E0              2006 	push	acc
   091E C0 F0              2007 	push	b
   0920 C0 82              2008 	push	dpl0
   0922 C0 83              2009 	push	dph0
   0924 C0 D0              2010 	push	psw
   0926 75 D0 00           2011 	mov	psw,#0x00
   0929 C0 86              2012 	push	dps
   092B 75 86 00           2013 	mov	dps,#0
                           2014 	;; clear interrupt
   092E E5 91              2015 	mov	a,exif
   0930 C2 E4              2016 	clr	acc.4
   0932 F5 91              2017 	mov	exif,a
   0934 90 7F AB           2018 	mov	dptr,#USBIRQ
   0937 74 04              2019 	mov	a,#0x04
   0939 F0                 2020 	movx	@dptr,a
                           2021 	;; handle interrupt
                           2022 	;; epilogue
   093A D0 86              2023 	pop	dps
   093C D0 D0              2024 	pop	psw
   093E D0 83              2025 	pop	dph0
   0940 D0 82              2026 	pop	dpl0
   0942 D0 F0              2027 	pop	b
   0944 D0 E0              2028 	pop	acc
   0946 32                 2029 	reti
                           2030 
   0947                    2031 usb_suspend_isr:
   0947 C0 E0              2032 	push	acc
   0949 C0 F0              2033 	push	b
   094B C0 82              2034 	push	dpl0
   094D C0 83              2035 	push	dph0
   094F C0 D0              2036 	push	psw
   0951 75 D0 00           2037 	mov	psw,#0x00
   0954 C0 86              2038 	push	dps
   0956 75 86 00           2039 	mov	dps,#0
                           2040 	;; clear interrupt
   0959 E5 91              2041 	mov	a,exif
   095B C2 E4              2042 	clr	acc.4
   095D F5 91              2043 	mov	exif,a
   095F 90 7F AB           2044 	mov	dptr,#USBIRQ
   0962 74 08              2045 	mov	a,#0x08
   0964 F0                 2046 	movx	@dptr,a
                           2047 	;; handle interrupt
                           2048 	;; epilogue
   0965 D0 86              2049 	pop	dps
   0967 D0 D0              2050 	pop	psw
   0969 D0 83              2051 	pop	dph0
   096B D0 82              2052 	pop	dpl0
   096D D0 F0              2053 	pop	b
   096F D0 E0              2054 	pop	acc
   0971 32                 2055 	reti
                           2056 
   0972                    2057 usb_usbreset_isr:
   0972 C0 E0              2058 	push	acc
   0974 C0 F0              2059 	push	b
   0976 C0 82              2060 	push	dpl0
   0978 C0 83              2061 	push	dph0
   097A C0 D0              2062 	push	psw
   097C 75 D0 00           2063 	mov	psw,#0x00
   097F C0 86              2064 	push	dps
   0981 75 86 00           2065 	mov	dps,#0
                           2066 	;; clear interrupt
   0984 E5 91              2067 	mov	a,exif
   0986 C2 E4              2068 	clr	acc.4
   0988 F5 91              2069 	mov	exif,a
   098A 90 7F AB           2070 	mov	dptr,#USBIRQ
   098D 74 10              2071 	mov	a,#0x10
   098F F0                 2072 	movx	@dptr,a
                           2073 	;; handle interrupt
                           2074 	;; epilogue
   0990 D0 86              2075 	pop	dps
   0992 D0 D0              2076 	pop	psw
   0994 D0 83              2077 	pop	dph0
   0996 D0 82              2078 	pop	dpl0
   0998 D0 F0              2079 	pop	b
   099A D0 E0              2080 	pop	acc
   099C 32                 2081 	reti
                           2082 
   099D                    2083 usb_ep0in_isr:
   099D C0 E0              2084 	push	acc
   099F C0 F0              2085 	push	b
   09A1 C0 82              2086 	push	dpl0
   09A3 C0 83              2087 	push	dph0
   09A5 C0 84              2088 	push	dpl1
   09A7 C0 85              2089 	push	dph1
   09A9 C0 D0              2090 	push	psw
   09AB 75 D0 00           2091 	mov	psw,#0x00
   09AE C0 86              2092 	push	dps
   09B0 75 86 00           2093 	mov	dps,#0
   09B3 C0 00              2094 	push	ar0
   09B5 C0 07              2095 	push	ar7
                           2096 	;; clear interrupt
   09B7 E5 91              2097 	mov	a,exif
   09B9 C2 E4              2098 	clr	acc.4
   09BB F5 91              2099 	mov	exif,a
   09BD 90 7F A9           2100 	mov	dptr,#IN07IRQ
   09C0 74 01              2101 	mov	a,#0x01
   09C2 F0                 2102 	movx	@dptr,a
                           2103 	;; handle interrupt
                           2104 	;; epilogue
   09C3 D0 07              2105 	pop	ar7
   09C5 D0 00              2106 	pop	ar0
   09C7 D0 86              2107 	pop	dps
   09C9 D0 D0              2108 	pop	psw
   09CB D0 85              2109 	pop	dph1
   09CD D0 84              2110 	pop	dpl1
   09CF D0 83              2111 	pop	dph0
   09D1 D0 82              2112 	pop	dpl0
   09D3 D0 F0              2113 	pop	b
   09D5 D0 E0              2114 	pop	acc
   09D7 32                 2115 	reti
                           2116 
   09D8                    2117 usb_ep0out_isr:
   09D8 C0 E0              2118 	push	acc
   09DA C0 F0              2119 	push	b
   09DC C0 82              2120 	push	dpl0
   09DE C0 83              2121 	push	dph0
   09E0 C0 84              2122 	push	dpl1
   09E2 C0 85              2123 	push	dph1
   09E4 C0 D0              2124 	push	psw
   09E6 75 D0 00           2125 	mov	psw,#0x00
   09E9 C0 86              2126 	push	dps
   09EB 75 86 00           2127 	mov	dps,#0
   09EE C0 00              2128 	push	ar0
   09F0 C0 06              2129 	push	ar6
                           2130 	;; clear interrupt
   09F2 E5 91              2131 	mov	a,exif
   09F4 C2 E4              2132 	clr	acc.4
   09F6 F5 91              2133 	mov	exif,a
   09F8 90 7F AA           2134 	mov	dptr,#OUT07IRQ
   09FB 74 01              2135 	mov	a,#0x01
   09FD F0                 2136 	movx	@dptr,a
                           2137 	;; handle interrupt
                           2138 	;; epilogue
   09FE D0 06              2139 	pop	ar6
   0A00 D0 00              2140 	pop	ar0
   0A02 D0 86              2141 	pop	dps
   0A04 D0 D0              2142 	pop	psw
   0A06 D0 85              2143 	pop	dph1
   0A08 D0 84              2144 	pop	dpl1
   0A0A D0 83              2145 	pop	dph0
   0A0C D0 82              2146 	pop	dpl0
   0A0E D0 F0              2147 	pop	b
   0A10 D0 E0              2148 	pop	acc
   0A12 32                 2149 	reti
                           2150 
   0A13                    2151 fillusbintr::
   0A13 E5 4C              2152 	mov	a,pttforce
   0A15 F5 F0              2153 	mov	b,a
   0A17 A2 E0              2154 	mov	c,acc.0
   0A19 92 F2              2155 	mov	b.2,c
   0A1B 90 C0 09           2156 	mov	dptr,#AUDIOSTAT
   0A1E E0                 2157 	movx	a,@dptr
   0A1F A2 E0              2158 	mov	c,acc.0
   0A21 92 F3              2159 	mov	b.3,c
   0A23 A2 09              2160 	mov	c,uartempty
   0A25 92 F5              2161 	mov	b.5,c
                           2162 	;; bytewide elements
   0A27 90 7E 80           2163 	mov	dptr,#(IN1BUF)
   0A2A E5 F0              2164 	mov	a,b
   0A2C F0                 2165 	movx	@dptr,a
   0A2D E5 45              2166 	mov	a,sofcount
   0A2F 90 7E 81           2167 	mov	dptr,#(IN1BUF+1)
   0A32 F0                 2168 	movx	@dptr,a
   0A33 90 7F E0           2169 	mov	dptr,#INISOVAL
   0A36 E0                 2170 	movx	a,@dptr
   0A37 90 7E 82           2171 	mov	dptr,#(IN1BUF+2)
   0A3A F0                 2172 	movx	@dptr,a
   0A3B 90 C0 04           2173 	mov	dptr,#AUDIORSSI
   0A3E E0                 2174 	movx	a,@dptr
   0A3F 90 7E 83           2175 	mov	dptr,#(IN1BUF+3)
   0A42 F0                 2176 	movx	@dptr,a
                           2177 	; counter
   0A43 05 44              2178 	inc	irqcount
   0A45 E5 44              2179 	mov	a,irqcount
   0A47 90 7E 84           2180 	mov	dptr,#(IN1BUF+4)
   0A4A F0                 2181 	movx	@dptr,a
                           2182 	; UART buffer
   0A4B 75 F0 05           2183 	mov	b,#5
   0A4E 90 7E 85           2184 	mov	dptr,#(IN1BUF+5)
   0A51 E5 5E              2185 2$:	mov	a,uartrd
   0A53 B5 5D 07           2186 	cjne	a,uartwr,3$
                           2187 	; set length
   0A56 90 7F B7           2188 	mov	dptr,#IN1BC
   0A59 E5 F0              2189 	mov	a,b
   0A5B F0                 2190 	movx	@dptr,a
   0A5C 22                 2191 	ret
                           2192 	
   0A5D 24 4D              2193 3$:	add	a,#uartbuf
   0A5F F8                 2194 	mov	r0,a
   0A60 E6                 2195 	mov	a,@r0
   0A61 F0                 2196 	movx	@dptr,a
   0A62 A3                 2197 	inc	dptr
   0A63 05 F0              2198 	inc	b
   0A65 E5 5E              2199 	mov	a,uartrd
   0A67 04                 2200 	inc	a
   0A68 54 0F              2201 	anl	a,#0xf
   0A6A F5 5E              2202 	mov	uartrd,a
   0A6C 80 E3              2203 	sjmp	2$
                           2204 
                           2205 	
   0A6E                    2206 usb_ep1in_isr:
   0A6E C0 E0              2207 	push	acc
   0A70 C0 F0              2208 	push	b
   0A72 C0 82              2209 	push	dpl0
   0A74 C0 83              2210 	push	dph0
   0A76 C0 D0              2211 	push	psw
   0A78 75 D0 00           2212 	mov	psw,#0x00
   0A7B C0 86              2213 	push	dps
   0A7D 75 86 00           2214 	mov	dps,#0
                           2215 	;; clear interrupt
   0A80 E5 91              2216 	mov	a,exif
   0A82 C2 E4              2217 	clr	acc.4
   0A84 F5 91              2218 	mov	exif,a
   0A86 90 7F A9           2219 	mov	dptr,#IN07IRQ
   0A89 74 02              2220 	mov	a,#0x02
   0A8B F0                 2221 	movx	@dptr,a
                           2222 	;; handle interrupt
   0A8C 12 0A 13           2223 	lcall	fillusbintr
                           2224 	;; epilogue
   0A8F D0 86              2225 	pop	dps
   0A91 D0 D0              2226 	pop	psw
   0A93 D0 83              2227 	pop	dph0
   0A95 D0 82              2228 	pop	dpl0
   0A97 D0 F0              2229 	pop	b
   0A99 D0 E0              2230 	pop	acc
   0A9B 32                 2231 	reti
                           2232 
   0A9C                    2233 usb_ep1out_isr:
   0A9C C0 E0              2234 	push	acc
   0A9E C0 F0              2235 	push	b
   0AA0 C0 82              2236 	push	dpl0
   0AA2 C0 83              2237 	push	dph0
   0AA4 C0 D0              2238 	push	psw
   0AA6 75 D0 00           2239 	mov	psw,#0x00
   0AA9 C0 86              2240 	push	dps
   0AAB 75 86 00           2241 	mov	dps,#0
                           2242 	;; clear interrupt
   0AAE E5 91              2243 	mov	a,exif
   0AB0 C2 E4              2244 	clr	acc.4
   0AB2 F5 91              2245 	mov	exif,a
   0AB4 90 7F AA           2246 	mov	dptr,#OUT07IRQ
   0AB7 74 02              2247 	mov	a,#0x02
   0AB9 F0                 2248 	movx	@dptr,a
                           2249 	;; handle interrupt
                           2250 	;; epilogue
   0ABA D0 86              2251 	pop	dps
   0ABC D0 D0              2252 	pop	psw
   0ABE D0 83              2253 	pop	dph0
   0AC0 D0 82              2254 	pop	dpl0
   0AC2 D0 F0              2255 	pop	b
   0AC4 D0 E0              2256 	pop	acc
   0AC6 32                 2257 	reti
                           2258 
   0AC7                    2259 usb_ep2in_isr:
   0AC7 C0 E0              2260 	push	acc
   0AC9 C0 F0              2261 	push	b
   0ACB C0 82              2262 	push	dpl0
   0ACD C0 83              2263 	push	dph0
   0ACF C0 D0              2264 	push	psw
   0AD1 75 D0 00           2265 	mov	psw,#0x00
   0AD4 C0 86              2266 	push	dps
   0AD6 75 86 00           2267 	mov	dps,#0
                           2268 	;; clear interrupt
   0AD9 E5 91              2269 	mov	a,exif
   0ADB C2 E4              2270 	clr	acc.4
   0ADD F5 91              2271 	mov	exif,a
   0ADF 90 7F A9           2272 	mov	dptr,#IN07IRQ
   0AE2 74 04              2273 	mov	a,#0x04
   0AE4 F0                 2274 	movx	@dptr,a
                           2275 	;; handle interrupt
                           2276 	;; epilogue
   0AE5 D0 86              2277 	pop	dps
   0AE7 D0 D0              2278 	pop	psw
   0AE9 D0 83              2279 	pop	dph0
   0AEB D0 82              2280 	pop	dpl0
   0AED D0 F0              2281 	pop	b
   0AEF D0 E0              2282 	pop	acc
   0AF1 32                 2283 	reti
                           2284 
   0AF2                    2285 usb_ep2out_isr:
   0AF2 C0 E0              2286 	push	acc
   0AF4 C0 F0              2287 	push	b
   0AF6 C0 82              2288 	push	dpl0
   0AF8 C0 83              2289 	push	dph0
   0AFA C0 D0              2290 	push	psw
   0AFC 75 D0 00           2291 	mov	psw,#0x00
   0AFF C0 86              2292 	push	dps
   0B01 75 86 00           2293 	mov	dps,#0
                           2294 	;; clear interrupt
   0B04 E5 91              2295 	mov	a,exif
   0B06 C2 E4              2296 	clr	acc.4
   0B08 F5 91              2297 	mov	exif,a
   0B0A 90 7F AA           2298 	mov	dptr,#OUT07IRQ
   0B0D 74 04              2299 	mov	a,#0x04
   0B0F F0                 2300 	movx	@dptr,a
                           2301 	;; handle interrupt
                           2302 	;; epilogue
   0B10 D0 86              2303 	pop	dps
   0B12 D0 D0              2304 	pop	psw
   0B14 D0 83              2305 	pop	dph0
   0B16 D0 82              2306 	pop	dpl0
   0B18 D0 F0              2307 	pop	b
   0B1A D0 E0              2308 	pop	acc
   0B1C 32                 2309 	reti
                           2310 
   0B1D                    2311 usb_ep3in_isr:
   0B1D C0 E0              2312 	push	acc
   0B1F C0 F0              2313 	push	b
   0B21 C0 82              2314 	push	dpl0
   0B23 C0 83              2315 	push	dph0
   0B25 C0 D0              2316 	push	psw
   0B27 75 D0 00           2317 	mov	psw,#0x00
   0B2A C0 86              2318 	push	dps
   0B2C 75 86 00           2319 	mov	dps,#0
                           2320 	;; clear interrupt
   0B2F E5 91              2321 	mov	a,exif
   0B31 C2 E4              2322 	clr	acc.4
   0B33 F5 91              2323 	mov	exif,a
   0B35 90 7F A9           2324 	mov	dptr,#IN07IRQ
   0B38 74 08              2325 	mov	a,#0x08
   0B3A F0                 2326 	movx	@dptr,a
                           2327 	;; handle interrupt
                           2328 	;; epilogue
   0B3B D0 86              2329 	pop	dps
   0B3D D0 D0              2330 	pop	psw
   0B3F D0 83              2331 	pop	dph0
   0B41 D0 82              2332 	pop	dpl0
   0B43 D0 F0              2333 	pop	b
   0B45 D0 E0              2334 	pop	acc
   0B47 32                 2335 	reti
                           2336 
   0B48                    2337 usb_ep3out_isr:
   0B48 C0 E0              2338 	push	acc
   0B4A C0 F0              2339 	push	b
   0B4C C0 82              2340 	push	dpl0
   0B4E C0 83              2341 	push	dph0
   0B50 C0 D0              2342 	push	psw
   0B52 75 D0 00           2343 	mov	psw,#0x00
   0B55 C0 86              2344 	push	dps
   0B57 75 86 00           2345 	mov	dps,#0
                           2346 	;; clear interrupt
   0B5A E5 91              2347 	mov	a,exif
   0B5C C2 E4              2348 	clr	acc.4
   0B5E F5 91              2349 	mov	exif,a
   0B60 90 7F AA           2350 	mov	dptr,#OUT07IRQ
   0B63 74 08              2351 	mov	a,#0x08
   0B65 F0                 2352 	movx	@dptr,a
                           2353 	;; handle interrupt
                           2354 	;; epilogue
   0B66 D0 86              2355 	pop	dps
   0B68 D0 D0              2356 	pop	psw
   0B6A D0 83              2357 	pop	dph0
   0B6C D0 82              2358 	pop	dpl0
   0B6E D0 F0              2359 	pop	b
   0B70 D0 E0              2360 	pop	acc
   0B72 32                 2361 	reti
                           2362 
   0B73                    2363 usb_ep4in_isr:
   0B73 C0 E0              2364 	push	acc
   0B75 C0 F0              2365 	push	b
   0B77 C0 82              2366 	push	dpl0
   0B79 C0 83              2367 	push	dph0
   0B7B C0 D0              2368 	push	psw
   0B7D 75 D0 00           2369 	mov	psw,#0x00
   0B80 C0 86              2370 	push	dps
   0B82 75 86 00           2371 	mov	dps,#0
                           2372 	;; clear interrupt
   0B85 E5 91              2373 	mov	a,exif
   0B87 C2 E4              2374 	clr	acc.4
   0B89 F5 91              2375 	mov	exif,a
   0B8B 90 7F A9           2376 	mov	dptr,#IN07IRQ
   0B8E 74 10              2377 	mov	a,#0x10
   0B90 F0                 2378 	movx	@dptr,a
                           2379 	;; handle interrupt
                           2380 	;; epilogue
   0B91 D0 86              2381 	pop	dps
   0B93 D0 D0              2382 	pop	psw
   0B95 D0 83              2383 	pop	dph0
   0B97 D0 82              2384 	pop	dpl0
   0B99 D0 F0              2385 	pop	b
   0B9B D0 E0              2386 	pop	acc
   0B9D 32                 2387 	reti
                           2388 
   0B9E                    2389 usb_ep4out_isr:
   0B9E C0 E0              2390 	push	acc
   0BA0 C0 F0              2391 	push	b
   0BA2 C0 82              2392 	push	dpl0
   0BA4 C0 83              2393 	push	dph0
   0BA6 C0 D0              2394 	push	psw
   0BA8 75 D0 00           2395 	mov	psw,#0x00
   0BAB C0 86              2396 	push	dps
   0BAD 75 86 00           2397 	mov	dps,#0
                           2398 	;; clear interrupt
   0BB0 E5 91              2399 	mov	a,exif
   0BB2 C2 E4              2400 	clr	acc.4
   0BB4 F5 91              2401 	mov	exif,a
   0BB6 90 7F AA           2402 	mov	dptr,#OUT07IRQ
   0BB9 74 10              2403 	mov	a,#0x10
   0BBB F0                 2404 	movx	@dptr,a
                           2405 	;; handle interrupt
                           2406 	;; epilogue
   0BBC D0 86              2407 	pop	dps
   0BBE D0 D0              2408 	pop	psw
   0BC0 D0 83              2409 	pop	dph0
   0BC2 D0 82              2410 	pop	dpl0
   0BC4 D0 F0              2411 	pop	b
   0BC6 D0 E0              2412 	pop	acc
   0BC8 32                 2413 	reti
                           2414 
   0BC9                    2415 usb_ep5in_isr:
   0BC9 C0 E0              2416 	push	acc
   0BCB C0 F0              2417 	push	b
   0BCD C0 82              2418 	push	dpl0
   0BCF C0 83              2419 	push	dph0
   0BD1 C0 D0              2420 	push	psw
   0BD3 75 D0 00           2421 	mov	psw,#0x00
   0BD6 C0 86              2422 	push	dps
   0BD8 75 86 00           2423 	mov	dps,#0
                           2424 	;; clear interrupt
   0BDB E5 91              2425 	mov	a,exif
   0BDD C2 E4              2426 	clr	acc.4
   0BDF F5 91              2427 	mov	exif,a
   0BE1 90 7F A9           2428 	mov	dptr,#IN07IRQ
   0BE4 74 20              2429 	mov	a,#0x20
   0BE6 F0                 2430 	movx	@dptr,a
                           2431 	;; handle interrupt
                           2432 	;; epilogue
   0BE7 D0 86              2433 	pop	dps
   0BE9 D0 D0              2434 	pop	psw
   0BEB D0 83              2435 	pop	dph0
   0BED D0 82              2436 	pop	dpl0
   0BEF D0 F0              2437 	pop	b
   0BF1 D0 E0              2438 	pop	acc
   0BF3 32                 2439 	reti
                           2440 
   0BF4                    2441 usb_ep5out_isr:
   0BF4 C0 E0              2442 	push	acc
   0BF6 C0 F0              2443 	push	b
   0BF8 C0 82              2444 	push	dpl0
   0BFA C0 83              2445 	push	dph0
   0BFC C0 D0              2446 	push	psw
   0BFE 75 D0 00           2447 	mov	psw,#0x00
   0C01 C0 86              2448 	push	dps
   0C03 75 86 00           2449 	mov	dps,#0
                           2450 	;; clear interrupt
   0C06 E5 91              2451 	mov	a,exif
   0C08 C2 E4              2452 	clr	acc.4
   0C0A F5 91              2453 	mov	exif,a
   0C0C 90 7F AA           2454 	mov	dptr,#OUT07IRQ
   0C0F 74 20              2455 	mov	a,#0x20
   0C11 F0                 2456 	movx	@dptr,a
                           2457 	;; handle interrupt
                           2458 	;; epilogue
   0C12 D0 86              2459 	pop	dps
   0C14 D0 D0              2460 	pop	psw
   0C16 D0 83              2461 	pop	dph0
   0C18 D0 82              2462 	pop	dpl0
   0C1A D0 F0              2463 	pop	b
   0C1C D0 E0              2464 	pop	acc
   0C1E 32                 2465 	reti
                           2466 
   0C1F                    2467 usb_ep6in_isr:
   0C1F C0 E0              2468 	push	acc
   0C21 C0 F0              2469 	push	b
   0C23 C0 82              2470 	push	dpl0
   0C25 C0 83              2471 	push	dph0
   0C27 C0 D0              2472 	push	psw
   0C29 75 D0 00           2473 	mov	psw,#0x00
   0C2C C0 86              2474 	push	dps
   0C2E 75 86 00           2475 	mov	dps,#0
                           2476 	;; clear interrupt
   0C31 E5 91              2477 	mov	a,exif
   0C33 C2 E4              2478 	clr	acc.4
   0C35 F5 91              2479 	mov	exif,a
   0C37 90 7F A9           2480 	mov	dptr,#IN07IRQ
   0C3A 74 40              2481 	mov	a,#0x40
   0C3C F0                 2482 	movx	@dptr,a
                           2483 	;; handle interrupt
                           2484 	;; epilogue
   0C3D D0 86              2485 	pop	dps
   0C3F D0 D0              2486 	pop	psw
   0C41 D0 83              2487 	pop	dph0
   0C43 D0 82              2488 	pop	dpl0
   0C45 D0 F0              2489 	pop	b
   0C47 D0 E0              2490 	pop	acc
   0C49 32                 2491 	reti
                           2492 
   0C4A                    2493 usb_ep6out_isr:
   0C4A C0 E0              2494 	push	acc
   0C4C C0 F0              2495 	push	b
   0C4E C0 82              2496 	push	dpl0
   0C50 C0 83              2497 	push	dph0
   0C52 C0 D0              2498 	push	psw
   0C54 75 D0 00           2499 	mov	psw,#0x00
   0C57 C0 86              2500 	push	dps
   0C59 75 86 00           2501 	mov	dps,#0
                           2502 	;; clear interrupt
   0C5C E5 91              2503 	mov	a,exif
   0C5E C2 E4              2504 	clr	acc.4
   0C60 F5 91              2505 	mov	exif,a
   0C62 90 7F AA           2506 	mov	dptr,#OUT07IRQ
   0C65 74 40              2507 	mov	a,#0x40
   0C67 F0                 2508 	movx	@dptr,a
                           2509 	;; handle interrupt
                           2510 	;; epilogue
   0C68 D0 86              2511 	pop	dps
   0C6A D0 D0              2512 	pop	psw
   0C6C D0 83              2513 	pop	dph0
   0C6E D0 82              2514 	pop	dpl0
   0C70 D0 F0              2515 	pop	b
   0C72 D0 E0              2516 	pop	acc
   0C74 32                 2517 	reti
                           2518 
   0C75                    2519 usb_ep7in_isr:
   0C75 C0 E0              2520 	push	acc
   0C77 C0 F0              2521 	push	b
   0C79 C0 82              2522 	push	dpl0
   0C7B C0 83              2523 	push	dph0
   0C7D C0 D0              2524 	push	psw
   0C7F 75 D0 00           2525 	mov	psw,#0x00
   0C82 C0 86              2526 	push	dps
   0C84 75 86 00           2527 	mov	dps,#0
                           2528 	;; clear interrupt
   0C87 E5 91              2529 	mov	a,exif
   0C89 C2 E4              2530 	clr	acc.4
   0C8B F5 91              2531 	mov	exif,a
   0C8D 90 7F A9           2532 	mov	dptr,#IN07IRQ
   0C90 74 80              2533 	mov	a,#0x80
   0C92 F0                 2534 	movx	@dptr,a
                           2535 	;; handle interrupt
                           2536 	;; epilogue
   0C93 D0 86              2537 	pop	dps
   0C95 D0 D0              2538 	pop	psw
   0C97 D0 83              2539 	pop	dph0
   0C99 D0 82              2540 	pop	dpl0
   0C9B D0 F0              2541 	pop	b
   0C9D D0 E0              2542 	pop	acc
   0C9F 32                 2543 	reti
                           2544 
   0CA0                    2545 usb_ep7out_isr:
   0CA0 C0 E0              2546 	push	acc
   0CA2 C0 F0              2547 	push	b
   0CA4 C0 82              2548 	push	dpl0
   0CA6 C0 83              2549 	push	dph0
   0CA8 C0 D0              2550 	push	psw
   0CAA 75 D0 00           2551 	mov	psw,#0x00
   0CAD C0 86              2552 	push	dps
   0CAF 75 86 00           2553 	mov	dps,#0
                           2554 	;; clear interrupt
   0CB2 E5 91              2555 	mov	a,exif
   0CB4 C2 E4              2556 	clr	acc.4
   0CB6 F5 91              2557 	mov	exif,a
   0CB8 90 7F AA           2558 	mov	dptr,#OUT07IRQ
   0CBB 74 80              2559 	mov	a,#0x80
   0CBD F0                 2560 	movx	@dptr,a
                           2561 	;; handle interrupt
                           2562 	;; epilogue
   0CBE D0 86              2563 	pop	dps
   0CC0 D0 D0              2564 	pop	psw
   0CC2 D0 83              2565 	pop	dph0
   0CC4 D0 82              2566 	pop	dpl0
   0CC6 D0 F0              2567 	pop	b
   0CC8 D0 E0              2568 	pop	acc
   0CCA 32                 2569 	reti
                           2570 	
                           2571 	;; -----------------------------------------------------
                           2572 	;; USB descriptors
                           2573 	;; -----------------------------------------------------
                           2574 
                           2575 	;; Device and/or Interface Class codes
                    0000   2576 	USB_CLASS_PER_INTERFACE         = 0
                    0001   2577 	USB_CLASS_AUDIO                 = 1
                    0002   2578 	USB_CLASS_COMM                  = 2
                    0003   2579 	USB_CLASS_HID                   = 3
                    0007   2580 	USB_CLASS_PRINTER               = 7
                    0008   2581 	USB_CLASS_MASS_STORAGE          = 8
                    0009   2582 	USB_CLASS_HUB                   = 9
                    00FF   2583 	USB_CLASS_VENDOR_SPEC           = 0xff
                           2584 
                    0001   2585 	USB_SUBCLASS_AUDIOCONTROL	= 1
                    0002   2586 	USB_SUBCLASS_AUDIOSTREAMING	= 2
                           2587 	
                           2588 	;; Descriptor types
                    0001   2589 	USB_DT_DEVICE                   = 0x01
                    0002   2590 	USB_DT_CONFIG                   = 0x02
                    0003   2591 	USB_DT_STRING                   = 0x03
                    0004   2592 	USB_DT_INTERFACE                = 0x04
                    0005   2593 	USB_DT_ENDPOINT                 = 0x05
                           2594 
                           2595 	;; Audio Class descriptor types
                    0020   2596 	USB_DT_AUDIO_UNDEFINED		= 0x20
                    0021   2597 	USB_DT_AUDIO_DEVICE		= 0x21
                    0022   2598 	USB_DT_AUDIO_CONFIG		= 0x22
                    0023   2599 	USB_DT_AUDIO_STRING		= 0x23
                    0024   2600 	USB_DT_AUDIO_INTERFACE		= 0x24
                    0025   2601 	USB_DT_AUDIO_ENDPOINT		= 0x25
                           2602 
                    0000   2603 	USB_SDT_AUDIO_UNDEFINED		= 0x00
                    0001   2604 	USB_SDT_AUDIO_HEADER		= 0x01
                    0002   2605 	USB_SDT_AUDIO_INPUT_TERMINAL	= 0x02
                    0003   2606 	USB_SDT_AUDIO_OUTPUT_TERMINAL	= 0x03
                    0004   2607 	USB_SDT_AUDIO_MIXER_UNIT	= 0x04
                    0005   2608 	USB_SDT_AUDIO_SELECTOR_UNIT	= 0x05
                    0006   2609 	USB_SDT_AUDIO_FEATURE_UNIT	= 0x06
                    0007   2610 	USB_SDT_AUDIO_PROCESSING_UNIT	= 0x07
                    0008   2611 	USB_SDT_AUDIO_EXTENSION_UNIT	= 0x08
                           2612 	
                           2613 	;; Standard requests
                    0000   2614 	USB_REQ_GET_STATUS              = 0x00
                    0001   2615 	USB_REQ_CLEAR_FEATURE           = 0x01
                    0003   2616 	USB_REQ_SET_FEATURE             = 0x03
                    0005   2617 	USB_REQ_SET_ADDRESS             = 0x05
                    0006   2618 	USB_REQ_GET_DESCRIPTOR          = 0x06
                    0007   2619 	USB_REQ_SET_DESCRIPTOR          = 0x07
                    0008   2620 	USB_REQ_GET_CONFIGURATION       = 0x08
                    0009   2621 	USB_REQ_SET_CONFIGURATION       = 0x09
                    000A   2622 	USB_REQ_GET_INTERFACE           = 0x0A
                    000B   2623 	USB_REQ_SET_INTERFACE           = 0x0B
                    000C   2624 	USB_REQ_SYNCH_FRAME             = 0x0C
                           2625 
                           2626 	;; Audio Class Requests
                    0001   2627 	USB_REQ_AUDIO_SET_CUR		= 0x01
                    0081   2628 	USB_REQ_AUDIO_GET_CUR		= 0x81
                    0002   2629 	USB_REQ_AUDIO_SET_MIN		= 0x02
                    0082   2630 	USB_REQ_AUDIO_GET_MIN		= 0x82
                    0003   2631 	USB_REQ_AUDIO_SET_MAX		= 0x03
                    0083   2632 	USB_REQ_AUDIO_GET_MAX		= 0x83
                    0004   2633 	USB_REQ_AUDIO_SET_RES		= 0x04
                    0084   2634 	USB_REQ_AUDIO_GET_RES		= 0x84
                    0005   2635 	USB_REQ_AUDIO_SET_MEM		= 0x05
                    0085   2636 	USB_REQ_AUDIO_GET_MEM		= 0x85
                    00FF   2637 	USB_REQ_AUDIO_GET_STAT		= 0xff
                           2638 	
                           2639 	;; USB Request Type and Endpoint Directions
                    0000   2640 	USB_DIR_OUT                     = 0
                    0080   2641 	USB_DIR_IN                      = 0x80
                           2642 
                    0000   2643 	USB_TYPE_STANDARD               = (0x00 << 5)
                    0020   2644 	USB_TYPE_CLASS                  = (0x01 << 5)
                    0040   2645 	USB_TYPE_VENDOR                 = (0x02 << 5)
                    0060   2646 	USB_TYPE_RESERVED               = (0x03 << 5)
                           2647 
                    0000   2648 	USB_RECIP_DEVICE                = 0x00
                    0001   2649 	USB_RECIP_INTERFACE             = 0x01
                    0002   2650 	USB_RECIP_ENDPOINT              = 0x02
                    0003   2651 	USB_RECIP_OTHER                 = 0x03
                           2652 
                           2653 	;; Request target types.
                    0000   2654 	USB_RT_DEVICE                   = 0x00
                    0001   2655 	USB_RT_INTERFACE                = 0x01
                    0002   2656 	USB_RT_ENDPOINT                 = 0x02
                           2657 
                    BAC0   2658 	VENDID	= 0xbac0
                    6137   2659 	PRODID	= 0x6137
                           2660 
   0CCB                    2661 devicedescr:
   0CCB 12                 2662 	.db	18			; bLength
   0CCC 01                 2663 	.db	USB_DT_DEVICE		; bDescriptorType
   0CCD 00 01              2664 	.db	0x00, 0x01		; bcdUSB
   0CCF 00                 2665 	.db	USB_CLASS_PER_INTERFACE	; bDeviceClass
   0CD0 00                 2666 	.db	0			; bDeviceSubClass
   0CD1 00                 2667 	.db	0x00			; bDeviceProtocol
   0CD2 40                 2668 	.db	0x40			; bMaxPacketSize0
   0CD3 C0 BA              2669 	.db	<VENDID,>VENDID		; idVendor
   0CD5 37 61              2670 	.db	<PRODID,>PRODID		; idProduct
   0CD7 01 00              2671 	.db	0x01,0x00		; bcdDevice
   0CD9 01                 2672 	.db	1			; iManufacturer
   0CDA 02                 2673 	.db	2			; iProduct
   0CDB 03                 2674 	.db	3			; iSerialNumber
   0CDC 01                 2675 	.db	1			; bNumConfigurations
                           2676 
   0CDD                    2677 config0descr:
   0CDD 09                 2678 	.db	9			; bLength
   0CDE 02                 2679 	.db	USB_DT_CONFIG		; bDescriptorType
   0CDF CE 00              2680 	.db	<config0sz,>config0sz	; wTotalLength
   0CE1 04                 2681 	.db	4			; bNumInterfaces
   0CE2 01                 2682 	.db	1			; bConfigurationValue
   0CE3 00                 2683 	.db	0			; iConfiguration
   0CE4 40                 2684 	.db	0b01000000		; bmAttributs (self powered)
   0CE5 00                 2685 	.db	0			; MaxPower (mA/2) (self powered so 0)
                           2686 	;; standard packet interface (needed so the driver can hook to it)
                           2687 	;; interface descriptor I0:A0
   0CE6 09                 2688 	.db	9			; bLength
   0CE7 04                 2689 	.db	USB_DT_INTERFACE	; bDescriptorType
   0CE8 00                 2690 	.db	0			; bInterfaceNumber
   0CE9 00                 2691 	.db	0			; bAlternateSetting
   0CEA 01                 2692 	.db	1			; bNumEndpoints
   0CEB FF                 2693 	.db	0xff			; bInterfaceClass (vendor specific)
   0CEC 00                 2694 	.db	0x00			; bInterfaceSubClass
   0CED FF                 2695 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0CEE 00                 2696 	.db	0			; iInterface
                           2697 	;; endpoint descriptor I0:A0:E0
   0CEF 07                 2698 	.db	7			; bLength
   0CF0 05                 2699 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0CF1 81                 2700 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0CF2 02                 2701 	.db	0x02			; bmAttributes (bulk)
   0CF3 40 00              2702 	.db	0x40,0x00		; wMaxPacketSize
   0CF5 00                 2703 	.db	0			; bInterval
                           2704 	;; interface descriptor I0:A1
   0CF6 09                 2705 	.db	9			; bLength
   0CF7 04                 2706 	.db	USB_DT_INTERFACE	; bDescriptorType
   0CF8 00                 2707 	.db	0			; bInterfaceNumber
   0CF9 01                 2708 	.db	1			; bAlternateSetting
   0CFA 01                 2709 	.db	1			; bNumEndpoints
   0CFB FF                 2710 	.db	0xff			; bInterfaceClass (vendor specific)
   0CFC 00                 2711 	.db	0x00			; bInterfaceSubClass
   0CFD FF                 2712 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0CFE 00                 2713 	.db	0			; iInterface
                           2714 	;; endpoint descriptor I0:A1:E0
   0CFF 07                 2715 	.db	7			; bLength
   0D00 05                 2716 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0D01 81                 2717 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0D02 03                 2718 	.db	0x03			; bmAttributes (interrupt)
   0D03 40 00              2719 	.db	0x40,0x00		; wMaxPacketSize
   0D05 08                 2720 	.db	8			; bInterval
                           2721 	;; interface descriptor I1:A0
   0D06 09                 2722 	.db	9			; bLength
   0D07 04                 2723 	.db	USB_DT_INTERFACE	; bDescriptorType
   0D08 01                 2724 	.db	1			; bInterfaceNumber
   0D09 00                 2725 	.db	0			; bAlternateSetting
   0D0A 00                 2726 	.db	0			; bNumEndpoints (only control endpoint)
   0D0B 01                 2727 	.db	USB_CLASS_AUDIO		; bInterfaceClass (audio)
   0D0C 01                 2728 	.db	USB_SUBCLASS_AUDIOCONTROL ; bInterfaceSubClass
   0D0D 00                 2729 	.db	0x00			; bInterfaceProtocol (none)
   0D0E 00                 2730 	.db	0			; iInterface
   0D0F                    2731 acctrl:
                           2732 	;; class specific audio interface descriptor I1:A0
   0D0F 0A                 2733 	.db	10			; bLength
   0D10 24                 2734 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D11 01                 2735 	.db	USB_SDT_AUDIO_HEADER	; bDescriptorSubtype
   0D12 00 01              2736 	.db	0x00,0x01		; bcdADC (Audio Class Spec Revision 1.0)
   0D14 34 00              2737 	.db	<acctrlsz,>acctrlsz	; wTotalLength (Audio Class specific desctiptors)
   0D16 02                 2738 	.db	2			; bInCollection (number of streaming interfaces)
   0D17 02                 2739 	.db	2			; baInterfaceNr0 (number of AudioStreaming interface)
   0D18 03                 2740 	.db	3			; baInterfaceNr1 (number of AudioStreaming interface)
                           2741 	;; audio input terminal descriptor
   0D19 0C                 2742 	.db	12			; bLength
   0D1A 24                 2743 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D1B 02                 2744 	.db	USB_SDT_AUDIO_INPUT_TERMINAL ; bDescriptorSubtype
   0D1C 01                 2745 	.db	1			; bTerminalID
   0D1D 10 07              2746 	.db	0x10,0x07		; wTerminalType (Radio Receiver)
   0D1F 00                 2747 	.db	0			; bAssocTerminal (none)
   0D20 01                 2748 	.db	1			; bNrChannels (one channel)
   0D21 00 00              2749 	.db	0,0			; wChannelConfig (mono has no position bits)
   0D23 00                 2750 	.db	0			; iChannelNames
   0D24 00                 2751 	.db	0			; iTerminal
                           2752 	;; audio output terminal descriptor
   0D25 09                 2753 	.db	9			; bLength
   0D26 24                 2754 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D27 03                 2755 	.db	USB_SDT_AUDIO_OUTPUT_TERMINAL ; bDescriptorSubtype
   0D28 02                 2756 	.db	2			; bTerminalID
   0D29 11 07              2757 	.db	0x11,0x07		; wTerminalType (Radio Transmitter)
   0D2B 00                 2758 	.db	0			; bAssocTerminal (none)
   0D2C 03                 2759 	.db	3			; bSourceID
   0D2D 00                 2760 	.db	0			; iTerminal
                           2761 	;; USB input terminal descriptor
   0D2E 0C                 2762 	.db	12			; bLength
   0D2F 24                 2763 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D30 02                 2764 	.db	USB_SDT_AUDIO_INPUT_TERMINAL ; bDescriptorSubtype
   0D31 03                 2765 	.db	3			; bTerminalID
   0D32 01 01              2766 	.db	0x01,0x01		; wTerminalType (USB streaming)
   0D34 00                 2767 	.db	0			; bAssocTerminal (none)
   0D35 01                 2768 	.db	1			; bNrChannels (one channel)
   0D36 00 00              2769 	.db	0,0			; wChannelConfig (mono has no position bits)
   0D38 00                 2770 	.db	0			; iChannelNames
   0D39 00                 2771 	.db	0			; iTerminal
                           2772 	;; USB output terminal descriptor
   0D3A 09                 2773 	.db	9			; bLength
   0D3B 24                 2774 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D3C 03                 2775 	.db	USB_SDT_AUDIO_OUTPUT_TERMINAL ; bDescriptorSubtype
   0D3D 04                 2776 	.db	4			; bTerminalID
   0D3E 01 01              2777 	.db	0x01,0x01		; wTerminalType (USB streaming)
   0D40 00                 2778 	.db	0			; bAssocTerminal (none)
   0D41 01                 2779 	.db	1			; bSourceID
   0D42 00                 2780 	.db	0			; iTerminal
                    0034   2781 acctrlsz = . - acctrl
                           2782 	;; interface descriptor I2:A0
   0D43 09                 2783 	.db	9			; bLength
   0D44 04                 2784 	.db	USB_DT_INTERFACE	; bDescriptorType
   0D45 02                 2785 	.db	2			; bInterfaceNumber
   0D46 00                 2786 	.db	0			; bAlternateSetting
   0D47 00                 2787 	.db	0			; bNumEndpoints (use control endpoint only)
   0D48 01                 2788 	.db	USB_CLASS_AUDIO		; bInterfaceClass (audio)
   0D49 02                 2789 	.db	USB_SUBCLASS_AUDIOSTREAMING ; bInterfaceSubClass
   0D4A 00                 2790 	.db	0x00			; bInterfaceProtocol (none)
   0D4B 00                 2791 	.db	0			; iInterface
                           2792 	;; interface descriptor I2:A1
   0D4C 09                 2793 	.db	9			; bLength
   0D4D 04                 2794 	.db	USB_DT_INTERFACE	; bDescriptorType
   0D4E 02                 2795 	.db	2			; bInterfaceNumber
   0D4F 01                 2796 	.db	1			; bAlternateSetting
   0D50 01                 2797 	.db	1			; bNumEndpoints
   0D51 01                 2798 	.db	USB_CLASS_AUDIO		; bInterfaceClass (audio)
   0D52 02                 2799 	.db	USB_SUBCLASS_AUDIOSTREAMING ; bInterfaceSubClass
   0D53 00                 2800 	.db	0x00			; bInterfaceProtocol (none)
   0D54 00                 2801 	.db	0			; iInterface
                           2802 	;.db	1,0			; wNumClasses
                           2803 	;; audio class specific general interface descriptor I2:A1
   0D55 07                 2804 	.db	7			; bLength
   0D56 24                 2805 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D57 01                 2806 	.db	0x01			; bDescriptorSubtype (general)
   0D58 04                 2807 	.db	4			; bTerminalLink
   0D59 01                 2808 	.db	1			; bDelay (in frames)
   0D5A 01 00              2809 	.db	1,0			; wFormatTag (PCM)
                           2810 	;; audio class specific format type I interface descriptor I2:A1
   0D5C 0B                 2811 	.db	11			; bLength
   0D5D 24                 2812 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D5E 02                 2813 	.db	0x02			; bDescriptorSubtype (format type)
   0D5F 01                 2814 	.db	0x01			; bFormatType (1)
   0D60 01                 2815 	.db	1			; bNrChannels
   0D61 01                 2816 	.db	1			; bSubFrameSize
   0D62 08                 2817 	.db	8			; bBitResolution
   0D63 01                 2818 	.db	1			; bSamFreqType (one sampling frequency supported)
   0D64                    2819 descinsrate:	
   0D64 00 00 00           2820 	.db	0,0,0			; tSamFreq
                           2821 	;; endpoint descriptor I2:A1:E0
   0D67 09                 2822 	.db	9			; bLength
   0D68 05                 2823 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0D69 88                 2824 	.db	(USB_DIR_IN | 8)	; bEndpointAddress
   0D6A 01                 2825 	.db	0x01			; bmAttributes (iso)
   0D6B                    2826 descinframesize:
   0D6B 02 00              2827 	.db	0x02,0x00		; wMaxPacketSize
   0D6D 01                 2828 	.db	1			; bInterval
   0D6E 00                 2829 	.db	0			; bRefresh
   0D6F 00                 2830 	.db	0			; bSynchAddress
                           2831 	;; audio class specific endpoint descriptor I2:A1:E0
   0D70 07                 2832 	.db	7			; bLength
   0D71 25                 2833 	.db	USB_DT_AUDIO_ENDPOINT	; bDescriptorType
   0D72 01                 2834 	.db	1			; bDescriptorSubtype (general)
   0D73 00                 2835 	.db	0			; bmAttributes (no sampling rate control etc.)
   0D74 00                 2836 	.db	0			; bLockDelayUnits
   0D75 00 00              2837 	.db	0,0			; wLockDelay
                           2838 	;; interface descriptor I3:A0
   0D77 09                 2839 	.db	9			; bLength
   0D78 04                 2840 	.db	USB_DT_INTERFACE	; bDescriptorType
   0D79 03                 2841 	.db	3			; bInterfaceNumber
   0D7A 00                 2842 	.db	0			; bAlternateSetting
   0D7B 00                 2843 	.db	0			; bNumEndpoints (use control endpoint only)
   0D7C 01                 2844 	.db	USB_CLASS_AUDIO		; bInterfaceClass (audio)
   0D7D 02                 2845 	.db	USB_SUBCLASS_AUDIOSTREAMING ; bInterfaceSubClass
   0D7E 00                 2846 	.db	0x00			; bInterfaceProtocol (none)
   0D7F 00                 2847 	.db	0			; iInterface
                           2848 	;; interface descriptor I3:A1
   0D80 09                 2849 	.db	9			; bLength
   0D81 04                 2850 	.db	USB_DT_INTERFACE	; bDescriptorType
   0D82 03                 2851 	.db	3			; bInterfaceNumber
   0D83 01                 2852 	.db	1			; bAlternateSetting
   0D84 01                 2853 	.db	1			; bNumEndpoints
   0D85 01                 2854 	.db	USB_CLASS_AUDIO		; bInterfaceClass (audio)
   0D86 02                 2855 	.db	USB_SUBCLASS_AUDIOSTREAMING ; bInterfaceSubClass
   0D87 00                 2856 	.db	0x00			; bInterfaceProtocol (none)
   0D88 00                 2857 	.db	0			; iInterface
                           2858 	;.db	1,0			; wNumClasses
                           2859 	;; audio class specific general interface descriptor I3:A1
   0D89 07                 2860 	.db	7			; bLength
   0D8A 24                 2861 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D8B 01                 2862 	.db	0x01			; bDescriptorSubtype (general)
   0D8C 03                 2863 	.db	3			; bTerminalLink
   0D8D 01                 2864 	.db	1			; bDelay (in frames)
   0D8E 01 00              2865 	.db	1,0			; wFormatTag (PCM)
                           2866 	;; audio class specific format type I interface descriptor I3:A1
   0D90 0B                 2867 	.db	11			; bLength
   0D91 24                 2868 	.db	USB_DT_AUDIO_INTERFACE	; bDescriptorType
   0D92 02                 2869 	.db	0x02			; bDescriptorSubtype (format type)
   0D93 01                 2870 	.db	0x01			; bFormatType (1)
   0D94 01                 2871 	.db	1			; bNrChannels
   0D95 01                 2872 	.db	1			; bSubFrameSize
   0D96 08                 2873 	.db	8			; bBitResolution
   0D97 01                 2874 	.db	1			; bSamFreqType (one sampling frequency supported)
   0D98                    2875 descoutsrate:
   0D98 00 00 00           2876 	.db	0,0,0			; tSamFreq
                           2877 	;; endpoint descriptor I3:A1:E0
   0D9B 09                 2878 	.db	9			; bLength
   0D9C 05                 2879 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0D9D 08                 2880 	.db	(USB_DIR_OUT | 8)	; bEndpointAddress
   0D9E 01                 2881 	.db	0x01			; bmAttributes (iso)
   0D9F                    2882 descoutframesize:
   0D9F 02 00              2883 	.db	0x02,0x00		; wMaxPacketSize
   0DA1 01                 2884 	.db	1			; bInterval
   0DA2 00                 2885 	.db	0			; bRefresh
   0DA3 00                 2886 	.db	0			; bSynchAddress
                           2887 	;; audio class specific endpoint descriptor I3:A1:E0
   0DA4 07                 2888 	.db	7			; bLength
   0DA5 25                 2889 	.db	USB_DT_AUDIO_ENDPOINT	; bDescriptorType
   0DA6 01                 2890 	.db	1			; bDescriptorSubtype (general)
   0DA7 00                 2891 	.db	0			; bmAttributes (no sampling rate control etc.)
   0DA8 00                 2892 	.db	0			; bLockDelayUnits
   0DA9 00 00              2893 	.db	0,0			; wLockDelay
                    00CE   2894 config0sz = . - config0descr
                           2895 
   0DAB                    2896 stringdescr:
   0DAB B3 0D              2897 	.db	<string0,>string0
   0DAD B7 0D              2898 	.db	<string1,>string1
   0DAF C5 0D              2899 	.db	<string2,>string2
   0DB1 E5 0D              2900 	.db	<stringserial,>stringserial
                           2901 
                    0004   2902 numstrings = (. - stringdescr)/2
                           2903 
   0DB3                    2904 string0:
   0DB3 04                 2905 	.db	string0sz		; bLength
   0DB4 03                 2906 	.db	USB_DT_STRING		; bDescriptorType
   0DB5 00 00              2907 	.db	0,0			; LANGID[0]: Lang Neutral
                    0004   2908 string0sz = . - string0
                           2909 
   0DB7                    2910 string1:
   0DB7 0E                 2911 	.db	string1sz		; bLength
   0DB8 03                 2912 	.db	USB_DT_STRING		; bDescriptorType
   0DB9 42 00 61 00 79 00  2913 	.db	'B,0,'a,0,'y,0,'c,0,'o,0,'m,0
        63 00 6F 00 6D 00
                    000E   2914 string1sz = . - string1
                           2915 
   0DC5                    2916 string2:
   0DC5 20                 2917 	.db	string2sz		; bLength
   0DC6 03                 2918 	.db	USB_DT_STRING		; bDescriptorType
   0DC7 55 00 53 00 42 00  2919 	.db	'U,0,'S,0,'B,0,'F,0,'L,0,'E,0,'X,0,' ,0
        46 00 4C 00 45 00
        58 00 20 00
   0DD7 28 00 41 00 75 00  2920 	.db	'(,0,'A,0,'u,0,'d,0,'i,0,'o,0,'),0
        64 00 69 00 6F 00
        29 00
                    0020   2921 string2sz = . - string2
                           2922 	
   0DE5                    2923 stringserial:
   0DE5 02                 2924 	.db	2			; bLength
   0DE6 03                 2925 	.db	USB_DT_STRING		; bDescriptorType
   0DE7 00 00 00 00 00 00  2926 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
   0DF7 00 00 00 00 00 00  2927 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
                           2928 
