/*-------------------------------------------------------------------------
  Register Declarations for Anchorchips AN21xx USB processors
  
       Written By -  Thomas Sailer . sailer@ife.ee.ethz.ch (1999)
         based on reg51.h by Sandeep Dutta . sandeep.dutta@usa.net (1998)

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
   
   In other words, you are welcome to use, share and improve this program.
   You are forbidden to forbid anyone else to use, share and improve
   what you give them.   Help stamp out software-hoarding!  
-------------------------------------------------------------------------*/

#ifndef REGAN21xx_H
#define REGAN21xx_H

/*  BYTE Register  */
sfr at 0x81 SP      ;
sfr at 0x82 DPL     ;
sfr at 0x83 DPH     ;
sfr at 0x82 DPL0    ;
sfr at 0x83 DPH0    ;
sfr at 0x84 DPL1    ;
sfr at 0x85 DPH1    ;
sfr at 0x86 DPS     ;
sfr at 0x87 PCON    ;
sfr at 0x88 TCON    ;
sfr at 0x89 TMOD    ;
sfr at 0x8A TL0     ;
sfr at 0x8B TL1     ;
sfr at 0x8C TH0     ;
sfr at 0x8D TH1     ;
sfr at 0x8E CKCON   ;
sfr at 0x8F SPC_FNC ;
sfr at 0x91 EXIF    ;
sfr at 0x92 MPAGE   ;
sfr at 0x98 SCON0   ;
sfr at 0x99 SBUF0   ;
sfr at 0xA8 IE      ;
sfr at 0xB8 IP      ;
sfr at 0xC0 SCON1   ;
sfr at 0xC1 SBUF1   ;
sfr at 0xC8 T2CON   ;
sfr at 0xCA RCAP2L  ;
sfr at 0xCB RCAP2H  ;
sfr at 0xCC TL2     ;
sfr at 0xCD TH2     ;
sfr at 0xD0 PSW     ;
sfr at 0xD8 EICON   ;
sfr at 0xE0 ACC     ;
sfr at 0xE8 EIE     ;
sfr at 0xF0 B       ;
sfr at 0xF8 EIP     ;


/*  BIT Register  */
/*  TCON  */
sbit at 0x8F TF1  ;
sbit at 0x8E TR1  ;
sbit at 0x8D TF0  ;
sbit at 0x8C TR0  ;
sbit at 0x8B IE1  ;
sbit at 0x8A IT1  ;
sbit at 0x89 IE0  ;
sbit at 0x88 IT0  ;

/* TMOD bits */
#define GATE1   (1<<7)
#define C_T1    (1<<6)
#define M1_1    (1<<5)
#define M0_1    (1<<4)
#define GATE0   (1<<3)
#define C_T0    (1<<2)
#define M1_0    (1<<1)
#define M0_0    (1<<0)

/*  SCON0  */
sbit at 0x9F SM0_0 ;
sbit at 0x9E SM1_0 ;
sbit at 0x9D SM2_0 ;
sbit at 0x9C REN_0 ;
sbit at 0x9B TB8_0 ;
sbit at 0x9A RB8_0 ;
sbit at 0x99 TI_0  ;
sbit at 0x98 RI_0  ;

/*  IE   */
sbit at 0xAF EA    ;
sbit at 0xAE ES1   ;
sbit at 0xAD ET2   ;
sbit at 0xAC ES0   ;
sbit at 0xAB ET1   ;
sbit at 0xAA EX1   ;
sbit at 0xA9 ET0   ;
sbit at 0xA8 EX0   ;

/*  IP   */ 
sbit at 0xBE PS1   ;
sbit at 0xBD PT2   ;
sbit at 0xBC PS0   ;
sbit at 0xBB PT1   ;
sbit at 0xBA PX1   ;
sbit at 0xB9 PT0   ;
sbit at 0xB8 PX0   ;

/*  SCON1  */
sbit at 0xC7 SM0_1 ;
sbit at 0xC6 SM1_1 ;
sbit at 0xC5 SM2_1 ;
sbit at 0xC4 REN_1 ;
sbit at 0xC3 TB8_1 ;
sbit at 0xC2 RB8_1 ;
sbit at 0xC1 TI_1  ;
sbit at 0xC0 RI_1  ;

/*  T2CON  */
sbit at 0xCF TF2   ;
sbit at 0xCE EXF2  ;
sbit at 0xCD RCLK  ;
sbit at 0xCC TCLK  ;
sbit at 0xCB EXEN2 ;
sbit at 0xCA TR2   ;
sbit at 0xC9 CT2   ;
sbit at 0xC8 CPRL2 ;

/*  PSW   */
sbit at 0xD7 CY    ;
sbit at 0xD6 AC    ;
sbit at 0xD5 F0    ;
sbit at 0xD4 RS1   ;
sbit at 0xD3 RS0   ;
sbit at 0xD2 OV    ;
sbit at 0xD0 P     ;

/*  EICON  */
sbit at 0xDF SMOD1 ;
sbit at 0xDD ERESI ;
sbit at 0xDC RESI  ;
sbit at 0xDB INT6  ;

/*  EIE  */
sbit at 0xEC EWDI  ;
sbit at 0xEB EX5   ;
sbit at 0xEA EX4   ;
sbit at 0xE9 EI2C  ;
sbit at 0xE8 EUSB  ;

/*  EIP  */
sbit at 0xFC PX6   ;
sbit at 0xFB PX5   ;
sbit at 0xFA PX4   ;
sbit at 0xF9 PI2C  ;
sbit at 0xF8 PUSB  ;


/* XDATA registers */

/* control/interrupt/bulk endpoints */
xdata at 0x7F00 short IN0BUF[64];
xdata at 0x7EC0 short OUT0BUF[64];
xdata at 0x7E80 short IN1BUF[64];
xdata at 0x7E40 short OUT1BUF[64];
xdata at 0x7E00 short IN2BUF[64];
xdata at 0x7DC0 short OUT2BUF[64];
xdata at 0x7D80 short IN3BUF[64];
xdata at 0x7D40 short OUT3BUF[64];
xdata at 0x7D00 short IN4BUF[64];
xdata at 0x7CC0 short OUT4BUF[64];
xdata at 0x7C80 short IN5BUF[64];
xdata at 0x7C40 short OUT5BUF[64];
xdata at 0x7C00 short IN6BUF[64];
xdata at 0x7BC0 short OUT6BUF[64];
xdata at 0x7B80 short IN7BUF[64];
xdata at 0x7B40 short OUT7BUF[64];
xdata at 0x7FE8 short SETUPBUF[8];
xdata at 0x7FE8 short SETUPDAT[8];

xdata at 0x7FB4 short EP0CS;
xdata at 0x7FB5 short IN0BC;
xdata at 0x7FB6 short IN1CS;
xdata at 0x7FB7 short IN1BC;
xdata at 0x7FB8 short IN2CS;
xdata at 0x7FB9 short IN2BC;
xdata at 0x7FBA short IN3CS;
xdata at 0x7FBB short IN3BC;
xdata at 0x7FBC short IN4CS;
xdata at 0x7FBD short IN4BC;
xdata at 0x7FBE short IN5CS;
xdata at 0x7FBF short IN5BC;
xdata at 0x7FC0 short IN6CS;
xdata at 0x7FC1 short IN6BC;
xdata at 0x7FC2 short IN7CS;
xdata at 0x7FC3 short IN7BC;
xdata at 0x7FC5 short OUT0BC;
xdata at 0x7FC6 short OUT1CS;
xdata at 0x7FC7 short OUT1BC;
xdata at 0x7FC8 short OUT2CS;
xdata at 0x7FC9 short OUT2BC;
xdata at 0x7FCA short OUT3CS;
xdata at 0x7FCB short OUT3BC;
xdata at 0x7FCC short OUT4CS;
xdata at 0x7FCD short OUT4BC;
xdata at 0x7FCE short OUT5CS;
xdata at 0x7FCF short OUT5BC;
xdata at 0x7FD0 short OUT6CS;
xdata at 0x7FD1 short OUT6BC;
xdata at 0x7FD2 short OUT7CS;
xdata at 0x7FD3 short OUT7BC;

xdata at 0x7FA8 short IVEC;
xdata at 0x7FA9 short IN07IRQ;
xdata at 0x7FAA short OUT07IRQ;
xdata at 0x7FAB short USBIRQ;
xdata at 0x7FAC short IN07IEN;
xdata at 0x7FAD short OUT07IEN;
xdata at 0x7FAE short USBIEN;
xdata at 0x7FAF short USBBAV;
xdata at 0x7FB2 short BPADDRH;
xdata at 0x7FB3 short BPADDRL;

xdata at 0x7FD4 short SUDPTRH;
xdata at 0x7FD5 short SUDPTRL;
xdata at 0x7FD6 short USBCS;
xdata at 0x7FD7 short TOGCTL;
xdata at 0x7FD8 short USBFRAMEL;
xdata at 0x7FD9 short USBFRAMEH;
xdata at 0x7FDB short FNADDR;
xdata at 0x7FDD short USBPAIR;
xdata at 0x7FDE short IN07VAL;
xdata at 0x7FDF short OUT07VAL;
xdata at 0x7FE3 short AUTOPTRH;
xdata at 0x7FE4 short AUTOPTRL;
xdata at 0x7FE5 short AUTODATA;

/* isochronous endpoints. only available if ISODISAB=0 */

xdata at 0x7F60 short OUT8DATA;
xdata at 0x7F61 short OUT9DATA;
xdata at 0x7F62 short OUT10DATA;
xdata at 0x7F63 short OUT11DATA;
xdata at 0x7F64 short OUT12DATA;
xdata at 0x7F65 short OUT13DATA;
xdata at 0x7F66 short OUT14DATA;
xdata at 0x7F67 short OUT15DATA;

xdata at 0x7F68 short IN8DATA;
xdata at 0x7F69 short IN9DATA;
xdata at 0x7F6A short IN10DATA;
xdata at 0x7F6B short IN11DATA;
xdata at 0x7F6C short IN12DATA;
xdata at 0x7F6D short IN13DATA;
xdata at 0x7F6E short IN14DATA;
xdata at 0x7F6F short IN15DATA;

xdata at 0x7F70 short OUT8BCH;
xdata at 0x7F71 short OUT8BCL;
xdata at 0x7F72 short OUT9BCH;
xdata at 0x7F73 short OUT9BCL;
xdata at 0x7F74 short OUT10BCH;
xdata at 0x7F75 short OUT10BCL;
xdata at 0x7F76 short OUT11BCH;
xdata at 0x7F77 short OUT11BCL;
xdata at 0x7F78 short OUT12BCH;
xdata at 0x7F79 short OUT12BCL;
xdata at 0x7F7A short OUT13BCH;
xdata at 0x7F7B short OUT13BCL;
xdata at 0x7F7C short OUT14BCH;
xdata at 0x7F7D short OUT14BCL;
xdata at 0x7F7E short OUT15BCH;
xdata at 0x7F7F short OUT15BCL;

xdata at 0x7FF0 short OUT8ADDR;
xdata at 0x7FF1 short OUT9ADDR;
xdata at 0x7FF2 short OUT10ADDR;
xdata at 0x7FF3 short OUT11ADDR;
xdata at 0x7FF4 short OUT12ADDR;
xdata at 0x7FF5 short OUT13ADDR;
xdata at 0x7FF6 short OUT14ADDR;
xdata at 0x7FF7 short OUT15ADDR;
xdata at 0x7FF8 short IN8ADDR;
xdata at 0x7FF9 short IN9ADDR;
xdata at 0x7FFA short IN10ADDR;
xdata at 0x7FFB short IN11ADDR;
xdata at 0x7FFC short IN12ADDR;
xdata at 0x7FFD short IN13ADDR;
xdata at 0x7FFE short IN14ADDR;
xdata at 0x7FFF short IN15ADDR;

xdata at 0x7FA0 short ISOERR;
xdata at 0x7FA1 short ISOCTL;
xdata at 0x7FA2 short ZBCOUNT;
xdata at 0x7FE0 short INISOVAL;
xdata at 0x7FE1 short OUTISOVAL;
xdata at 0x7FE2 short FASTXFR;

/* CPU control registers */

xdata at 0x7F92 short CPUCS;

/* IO port control registers */

xdata at 0x7F93 short PORTACFG;
xdata at 0x7F94 short PORTBCFG;
xdata at 0x7F95 short PORTCCFG;
xdata at 0x7F96 short OUTA;
xdata at 0x7F97 short OUTB;
xdata at 0x7F98 short OUTC;
xdata at 0x7F99 short PINSA;
xdata at 0x7F9A short PINSB;
xdata at 0x7F9B short PINSC;
xdata at 0x7F9C short OEA;
xdata at 0x7F9D short OEB;
xdata at 0x7F9E short OEC;

/* I2C controller registers */

xdata at 0x7FA5 short I2CS;
xdata at 0x7FA6 short I2DAT;

/* interrupts */

#endif
