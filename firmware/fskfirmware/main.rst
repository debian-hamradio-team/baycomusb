                              1 	.module main
                              2 
                              3 	;; ENDPOINTS
                              4 	;; EP0 in/out   Control
                              5 	;; EP1 in       Interrupt:  Status
                              6 	;;              Byte 0: Modem Status
                              7 	;;                Bit 0-1: Transmitter status
                              8 	;;                         0: idle (off)
                              9 	;;                         1: keyup
                             10 	;;                         2: transmitting packets
                             11 	;;                         3: tail
                             12 	;;                Bit 2:   PTT status (1=on)
                             13 	;;                Bit 3:   DCD
                             14 	;;                Bit 4:   RXB (Rx Packet Ready)
                             15 	;;                Bit 5:   UART transmitter empty
                             16 	;;                Bit 6-7: unused
                             17 	;;              Byte 1: Number of empty 64 byte chunks in TX fifo
                             18 	;;              Byte 2: Number of full 64 byte chunks in RX fifo
                             19 	;;              Byte 3: RSSI value
                             20 	;;              Byte 4:	IRQ count
                             21 	;;              Byte 5-20: (as needed) UART receiver chars
                             22 	;; EP2 out      Packets to be transmitted
                             23 	;; EP2 in       Received packets; note they have the CRC appended
                             24 
                             25 	;; COMMAND LIST
                             26 	;; C0 C0  read status (max. 23 bytes, first 6 same as EP1 in)
                             27 	;; C0 C8  read mode
                             28 	;;          Return:
                             29 	;;            Byte 0: 1 (MODE_FSK) or 2 (MODE_EXTERNAL)
                             30 	;; C0 C9  return serial number string
                             31 	;; C0 D0  get/set PTT/DCD/RSSI
                             32 	;;          wIndex = 1:	 set forced ptt to wValue
                             33 	;;          Return:
                             34 	;;            Byte 0: PTT status
                             35 	;;            Byte 1: DCD status
                             36 	;;            Byte 2: RSSI status
                             37 	;; C0 D1  get Bitrate
                             38 	;;          Return:
                             39 	;;            Byte 0-2: TX Bitrate
                             40 	;;            Byte 3-5: RX Bitrate
                             41 	;; 40 D2  set CON/STA led
                             42 	;;          Bits 0-1 of wValue
                             43 	;; 40 D3  send byte to UART
                             44 	;;          Byte in wValue
                             45 	;; C0 D4  get/set modem disconnect port (only if internal modem used, stalls otherwise)
                             46 	;;          wIndex = 1:	 write wValue to output register
                             47 	;;          wIndex = 2:	 write wValue to tristate mask register (1 = input, 0 = output)
                             48 	;;          Return:
                             49 	;;            Byte 0: Modem Disconnect Input
                             50 	;;            Byte 1: Modem Disconnect Output register
                             51 	;;            Byte 2: Modem Disconnect Tristate register
                             52 	;; C0 D5  get/set T7F port
                             53 	;;          wIndex = 1:	 write wValue to T7F output register
                             54 	;;          Return:
                             55 	;;            Byte 0: T7F Input
                             56 	;;            Byte 1: T7F Output register
                             57 
                             58 	;; define code segments link order
                             59 	.area CODE (CODE)
                             60 	.area CSEG (CODE)
                             61 	.area GSINIT (CODE)
                             62 	.area GSINIT2 (CODE)
                             63 
                             64 	;; -----------------------------------------------------
                             65 
                             66 	;; special function registers (which are not predefined)
                    0082     67 	dpl0    = 0x82
                    0083     68 	dph0    = 0x83
                    0084     69 	dpl1    = 0x84
                    0085     70 	dph1    = 0x85
                    0086     71 	dps     = 0x86
                    008E     72 	ckcon   = 0x8E
                    008F     73 	spc_fnc = 0x8F
                    0091     74 	exif    = 0x91
                    0092     75 	mpage   = 0x92
                    0098     76 	scon0   = 0x98
                    0099     77 	sbuf0   = 0x99
                    00C0     78 	scon1   = 0xC0
                    00C1     79 	sbuf1   = 0xC1
                    00D8     80 	eicon   = 0xD8
                    00E8     81 	eie     = 0xE8
                    00F8     82 	eip     = 0xF8
                             83 
                             84 	;; anchor xdata registers
                    7F00     85 	IN0BUF		= 0x7F00
                    7EC0     86 	OUT0BUF		= 0x7EC0
                    7E80     87 	IN1BUF		= 0x7E80
                    7E40     88 	OUT1BUF		= 0x7E40
                    7E00     89 	IN2BUF		= 0x7E00
                    7DC0     90 	OUT2BUF		= 0x7DC0
                    7D80     91 	IN3BUF		= 0x7D80
                    7D40     92 	OUT3BUF		= 0x7D40
                    7D00     93 	IN4BUF		= 0x7D00
                    7CC0     94 	OUT4BUF		= 0x7CC0
                    7C80     95 	IN5BUF		= 0x7C80
                    7C40     96 	OUT5BUF		= 0x7C40
                    7C00     97 	IN6BUF		= 0x7C00
                    7BC0     98 	OUT6BUF		= 0x7BC0
                    7B80     99 	IN7BUF		= 0x7B80
                    7B40    100 	OUT7BUF		= 0x7B40
                    7FE8    101 	SETUPBUF	= 0x7FE8
                    7FE8    102 	SETUPDAT	= 0x7FE8
                            103 
                    7FB4    104 	EP0CS		= 0x7FB4
                    7FB5    105 	IN0BC		= 0x7FB5
                    7FB6    106 	IN1CS		= 0x7FB6
                    7FB7    107 	IN1BC		= 0x7FB7
                    7FB8    108 	IN2CS		= 0x7FB8
                    7FB9    109 	IN2BC		= 0x7FB9
                    7FBA    110 	IN3CS		= 0x7FBA
                    7FBB    111 	IN3BC		= 0x7FBB
                    7FBC    112 	IN4CS		= 0x7FBC
                    7FBD    113 	IN4BC		= 0x7FBD
                    7FBE    114 	IN5CS		= 0x7FBE
                    7FBF    115 	IN5BC		= 0x7FBF
                    7FC0    116 	IN6CS		= 0x7FC0
                    7FC1    117 	IN6BC		= 0x7FC1
                    7FC2    118 	IN7CS		= 0x7FC2
                    7FC3    119 	IN7BC		= 0x7FC3
                    7FC5    120 	OUT0BC		= 0x7FC5
                    7FC6    121 	OUT1CS		= 0x7FC6
                    7FC7    122 	OUT1BC		= 0x7FC7
                    7FC8    123 	OUT2CS		= 0x7FC8
                    7FC9    124 	OUT2BC		= 0x7FC9
                    7FCA    125 	OUT3CS		= 0x7FCA
                    7FCB    126 	OUT3BC		= 0x7FCB
                    7FCC    127 	OUT4CS		= 0x7FCC
                    7FCD    128 	OUT4BC		= 0x7FCD
                    7FCE    129 	OUT5CS		= 0x7FCE
                    7FCF    130 	OUT5BC		= 0x7FCF
                    7FD0    131 	OUT6CS		= 0x7FD0
                    7FD1    132 	OUT6BC		= 0x7FD1
                    7FD2    133 	OUT7CS		= 0x7FD2
                    7FD3    134 	OUT7BC		= 0x7FD3
                            135 
                    7FA8    136 	IVEC		= 0x7FA8
                    7FA9    137 	IN07IRQ		= 0x7FA9
                    7FAA    138 	OUT07IRQ	= 0x7FAA
                    7FAB    139 	USBIRQ		= 0x7FAB
                    7FAC    140 	IN07IEN		= 0x7FAC
                    7FAD    141 	OUT07IEN	= 0x7FAD
                    7FAE    142 	USBIEN		= 0x7FAE
                    7FAF    143 	USBBAV		= 0x7FAF
                    7FB2    144 	BPADDRH		= 0x7FB2
                    7FB3    145 	BPADDRL		= 0x7FB3
                            146 
                    7FD4    147 	SUDPTRH		= 0x7FD4
                    7FD5    148 	SUDPTRL		= 0x7FD5
                    7FD6    149 	USBCS		= 0x7FD6
                    7FD7    150 	TOGCTL		= 0x7FD7
                    7FD8    151 	USBFRAMEL	= 0x7FD8
                    7FD9    152 	USBFRAMEH	= 0x7FD9
                    7FDB    153 	FNADDR		= 0x7FDB
                    7FDD    154 	USBPAIR		= 0x7FDD
                    7FDE    155 	IN07VAL		= 0x7FDE
                    7FDF    156 	OUT07VAL	= 0x7FDF
                    7FE3    157 	AUTOPTRH	= 0x7FE3
                    7FE4    158 	AUTOPTRL	= 0x7FE4
                    7FE5    159 	AUTODATA	= 0x7FE5
                            160 
                            161 	;; isochronous endpoints. only available if ISODISAB=0
                            162 
                    7F60    163 	OUT8DATA	= 0x7F60
                    7F61    164 	OUT9DATA	= 0x7F61
                    7F62    165 	OUT10DATA	= 0x7F62
                    7F63    166 	OUT11DATA	= 0x7F63
                    7F64    167 	OUT12DATA	= 0x7F64
                    7F65    168 	OUT13DATA	= 0x7F65
                    7F66    169 	OUT14DATA	= 0x7F66
                    7F67    170 	OUT15DATA	= 0x7F67
                            171 
                    7F68    172 	IN8DATA		= 0x7F68
                    7F69    173 	IN9DATA		= 0x7F69
                    7F6A    174 	IN10DATA	= 0x7F6A
                    7F6B    175 	IN11DATA	= 0x7F6B
                    7F6C    176 	IN12DATA	= 0x7F6C
                    7F6D    177 	IN13DATA	= 0x7F6D
                    7F6E    178 	IN14DATA	= 0x7F6E
                    7F6F    179 	IN15DATA	= 0x7F6F
                            180 
                    7F70    181 	OUT8BCH		= 0x7F70
                    7F71    182 	OUT8BCL		= 0x7F71
                    7F72    183 	OUT9BCH		= 0x7F72
                    7F73    184 	OUT9BCL		= 0x7F73
                    7F74    185 	OUT10BCH	= 0x7F74
                    7F75    186 	OUT10BCL	= 0x7F75
                    7F76    187 	OUT11BCH	= 0x7F76
                    7F77    188 	OUT11BCL	= 0x7F77
                    7F78    189 	OUT12BCH	= 0x7F78
                    7F79    190 	OUT12BCL	= 0x7F79
                    7F7A    191 	OUT13BCH	= 0x7F7A
                    7F7B    192 	OUT13BCL	= 0x7F7B
                    7F7C    193 	OUT14BCH	= 0x7F7C
                    7F7D    194 	OUT14BCL	= 0x7F7D
                    7F7E    195 	OUT15BCH	= 0x7F7E
                    7F7F    196 	OUT15BCL	= 0x7F7F
                            197 
                    7FF0    198 	OUT8ADDR	= 0x7FF0
                    7FF1    199 	OUT9ADDR	= 0x7FF1
                    7FF2    200 	OUT10ADDR	= 0x7FF2
                    7FF3    201 	OUT11ADDR	= 0x7FF3
                    7FF4    202 	OUT12ADDR	= 0x7FF4
                    7FF5    203 	OUT13ADDR	= 0x7FF5
                    7FF6    204 	OUT14ADDR	= 0x7FF6
                    7FF7    205 	OUT15ADDR	= 0x7FF7
                    7FF8    206 	IN8ADDR		= 0x7FF8
                    7FF9    207 	IN9ADDR		= 0x7FF9
                    7FFA    208 	IN10ADDR	= 0x7FFA
                    7FFB    209 	IN11ADDR	= 0x7FFB
                    7FFC    210 	IN12ADDR	= 0x7FFC
                    7FFD    211 	IN13ADDR	= 0x7FFD
                    7FFE    212 	IN14ADDR	= 0x7FFE
                    7FFF    213 	IN15ADDR	= 0x7FFF
                            214 
                    7FA0    215 	ISOERR		= 0x7FA0
                    7FA1    216 	ISOCTL		= 0x7FA1
                    7FA2    217 	ZBCOUNT		= 0x7FA2
                    7FE0    218 	INISOVAL	= 0x7FE0
                    7FE1    219 	OUTISOVAL	= 0x7FE1
                    7FE2    220 	FASTXFR		= 0x7FE2
                            221 
                            222 	;; CPU control registers
                            223 
                    7F92    224 	CPUCS		= 0x7F92
                            225 
                            226 	;; IO port control registers
                            227 
                    7F93    228 	PORTACFG	= 0x7F93
                    7F94    229 	PORTBCFG	= 0x7F94
                    7F95    230 	PORTCCFG	= 0x7F95
                    7F96    231 	OUTA		= 0x7F96
                    7F97    232 	OUTB		= 0x7F97
                    7F98    233 	OUTC		= 0x7F98
                    7F99    234 	PINSA		= 0x7F99
                    7F9A    235 	PINSB		= 0x7F9A
                    7F9B    236 	PINSC		= 0x7F9B
                    7F9C    237 	OEA		= 0x7F9C
                    7F9D    238 	OEB		= 0x7F9D
                    7F9E    239 	OEC		= 0x7F9E
                            240 
                            241 	;; I2C controller registers
                            242 
                    7FA5    243 	I2CS		= 0x7FA5
                    7FA6    244 	I2DAT		= 0x7FA6
                            245 
                            246 	;; Xilinx FPGA registers
                    C001    247 	FSKRXCNT	= 0xc001
                    C002    248 	FSKRXDATA	= 0xc002
                    C003    249 	FSKRXFLAG	= 0xc003
                    C000    250 	FSKTXCNT	= 0xc000
                    C000    251 	FSKTXDATA	= 0xc000
                    C001    252 	FSKTXCRC	= 0xc001
                    C002    253 	FSKTXRAW	= 0xc002
                    C003    254 	FSKTXRAWCLR	= 0xc003
                    C004    255 	FSKRSSI		= 0xc004
                    C008    256 	FSKCTRL		= 0xc008
                    C009    257 	FSKSTAT		= 0xc009
                    C00A    258 	FSKT7FOUT	= 0xc00a
                    C00B    259 	FSKT7FIN	= 0xc00b
                    C00C    260 	FSKMDISCTRIS	= 0xc00c
                    C00D    261 	FSKMDISCOUT	= 0xc00d
                    C00E    262 	FSKMDISCIN	= 0xc00e
                            263 
                            264 	;; -----------------------------------------------------
                            265 
                            266 	.area CODE (CODE)
   0000 02 0E 83            267 	ljmp	startup
   0003 02 03 EA            268 	ljmp	int0_isr
   0006                     269 	.ds	5
   000B 02 04 0B            270 	ljmp	timer0_isr
   000E                     271 	.ds	5
   0013 02 04 2C            272 	ljmp	int1_isr
   0016                     273 	.ds	5
   001B 02 04 4D            274 	ljmp	timer1_isr
   001E                     275 	.ds	5
   0023 02 04 6E            276 	ljmp	ser0_isr
   0026                     277 	.ds	5
   002B 02 04 AC            278 	ljmp	timer2_isr
   002E                     279 	.ds	5
   0033 02 04 CD            280 	ljmp	resume_isr
   0036                     281 	.ds	5
   003B 02 04 EE            282 	ljmp	ser1_isr
   003E                     283 	.ds	5
   0043 02 01 00            284 	ljmp	usb_isr
   0046                     285 	.ds	5
   004B 02 05 11            286 	ljmp	i2c_isr
   004E                     287 	.ds	5
   0053 02 05 36            288 	ljmp	int4_isr
   0056                     289 	.ds	5
   005B 02 05 5B            290 	ljmp	int5_isr
   005E                     291 	.ds	5
   0063 02 05 80            292 	ljmp	int6_isr
                            293 
                            294 	;; Parameter block at 0xe0
   0066                     295 	.ds	0x7a
   00E0 00 00 00            296 parbitratetx:	.db	0,0,0
   00E3 00 00 00            297 parbitraterx:	.db	0,0,0
   00E6 00                  298 parextmodem:	.db	0
   00E7 01                  299 parpttmute:	.db	1
                            300 
                            301 	;; Serial# string at 0xf0
   00E8                     302 	.ds	8
   00F0                     303 parserial:
   00F0 30 30 30 30 30 30   304 	.db	'0,'0,'0,'0,'0,'0,'0,'0,0
        30 30 00
   00F9                     305 	.ds	7
                            306 
                            307 	;; USB interrupt dispatch table
   0100                     308 usb_isr:
   0100 02 05 A1            309 	ljmp	usb_sudav_isr
   0103                     310 	.ds	1
   0104 02 09 CF            311 	ljmp	usb_sof_isr
   0107                     312 	.ds	1
   0108 02 09 FA            313 	ljmp	usb_sutok_isr
   010B                     314 	.ds	1
   010C 02 0A 25            315 	ljmp	usb_suspend_isr
   010F                     316 	.ds	1
   0110 02 0A 50            317 	ljmp	usb_usbreset_isr
   0113                     318 	.ds	1
   0114 32                  319 	reti
   0115                     320 	.ds	3
   0118 02 0A 7B            321 	ljmp	usb_ep0in_isr
   011B                     322 	.ds	1
   011C 02 0A B6            323 	ljmp	usb_ep0out_isr
   011F                     324 	.ds	1
   0120 02 0B 77            325 	ljmp	usb_ep1in_isr
   0123                     326 	.ds	1
   0124 02 0B A5            327 	ljmp	usb_ep1out_isr
   0127                     328 	.ds	1
   0128 02 0B D0            329 	ljmp	usb_ep2in_isr
   012B                     330 	.ds	1
   012C 02 0B FB            331 	ljmp	usb_ep2out_isr
   012F                     332 	.ds	1
   0130 02 0C 26            333 	ljmp	usb_ep3in_isr
   0133                     334 	.ds	1
   0134 02 0C 51            335 	ljmp	usb_ep3out_isr
   0137                     336 	.ds	1
   0138 02 0C 7C            337 	ljmp	usb_ep4in_isr
   013B                     338 	.ds	1
   013C 02 0C A7            339 	ljmp	usb_ep4out_isr
   013F                     340 	.ds	1
   0140 02 0C D2            341 	ljmp	usb_ep5in_isr
   0143                     342 	.ds	1
   0144 02 0C FD            343 	ljmp	usb_ep5out_isr
   0147                     344 	.ds	1
   0148 02 0D 28            345 	ljmp	usb_ep6in_isr
   014B                     346 	.ds	1
   014C 02 0D 53            347 	ljmp	usb_ep6out_isr
   014F                     348 	.ds	1
   0150 02 0D 7E            349 	ljmp	usb_ep7in_isr
   0153                     350 	.ds	1
   0154 02 0D A9            351 	ljmp	usb_ep7out_isr
                            352 
                            353 	;; -----------------------------------------------------
                            354 
                    0020    355 	RXCHUNKS = 32
                    0010    356 	TXCHUNKS = 16
                            357 
                    0000    358 	DEBUGIOCOPY	= 0
                    0000    359 	DEBUGRECEIVER	= 0
                            360 	
                            361 	.area	OSEG (OVR,DATA)
                            362 	.area	BSEG (BIT)
   0000                     363 ctrl_ptt:	.ds	1
   0001                     364 ctrl_pttmute:	.ds	1
   0002                     365 ctrl_ledptt:	.ds	1
   0003                     366 ctrl_leddcdsrc:	.ds	1
   0004                     367 ctrl_clksel:	.ds	1	; not implemented
   0005                     368 ctrl_indacd:	.ds	1
   0006                     369 ctrl_indacz:	.ds	1
   0007                     370 ctrl_txdsrc:	.ds	1
                    0020    371 ctrlreg		=	0x20	; ((ctrl_ptt/8)+0x20)
                            372 
   0008                     373 extmodem:	.ds	1
   0009                     374 pttmute:	.ds	1
   000A                     375 pttforcechg:	.ds	1
   000B                     376 uartempty:	.ds	1
                            377 		
                            378 	.area	ISEG (DATA)
   0080                     379 txbcnt:		.ds	TXCHUNKS
   0090                     380 rxbcnt:		.ds	RXCHUNKS
   00B0                     381 stack:		.ds	0x80-RXCHUNKS-TXCHUNKS
                            382 	
                            383 	.area	DSEG (DATA)
   0040                     384 ctrlcode:	.ds	1
   0041                     385 ctrlcount:	.ds	2
   0043                     386 leddiv:		.ds	1
   0044                     387 irqcount:	.ds	1
                            388 	;; transmitter variables
   0045                     389 txstate:	.ds	1
   0046                     390 flagcnt:	.ds	2
   0048                     391 txwr:		.ds	1
   0049                     392 txrd:		.ds	1
   004A                     393 txtwr:		.ds	1
   004B                     394 txcnt:		.ds	1
   004C                     395 txaddr:		.ds	2
   004E                     396 txbptr:		.ds	1
   004F                     397 txskip:		.ds	1
   0050                     398 pttforce:	.ds	1
                            399 	;; receiver variables
   0051                     400 rxstate:	.ds	1
   0052                     401 rxwr:		.ds	1
   0053                     402 rxrd:		.ds	1
   0054                     403 rxtwr:		.ds	1
   0055                     404 rxcnt:		.ds	1
   0056                     405 rxcntc:		.ds	1
   0057                     406 rxaddr:		.ds	2
                            407 
   0059                     408 tmprxcnt:	.ds	1
                            409 
                            410 	;; UART receiver
   005A                     411 uartbuf:	.ds	16
   006A                     412 uartwr:		.ds	1
   006B                     413 uartrd:		.ds	1
                            414 
                            415 	;; Port state
   006C                     416 t7fout:		.ds	1
   006D                     417 mdisctris:	.ds	1
   006E                     418 mdiscout:	.ds	1
                            419 	
                            420 	;; USB state
   006F                     421 numconfig:	.ds	1
   0070                     422 altsetting:	.ds	1
                            423 
                            424 	.area	XSEG (DATA)
   1000                     425 txbuf:	.ds	TXCHUNKS*64
   1400                     426 rxbuf:	.ds	RXCHUNKS*64
                            427 
                            428 
                            429 	.area	GSINIT (CODE)
   0E83                     430 startup:
   0E83 75 81 B0            431 	mov	sp,#stack	; -1
   0E86 E4                  432 	clr	a
   0E87 F5 D0               433 	mov	psw,a
   0E89 F5 86               434 	mov	dps,a
                            435 	;lcall	__sdcc_external_startup
                            436 	;mov	a,dpl0
                            437 	;jz	__sdcc_init_data
                            438 	;ljmp	__sdcc_program_startup
   0E8B                     439 __sdcc_init_data:
                            440 
                            441 	.area	GSINIT2 (CODE)
   0E8B                     442 __sdcc_program_startup:
                            443 	;; assembler code startup
   0E8B E4                  444 	clr	a
   0E8C F5 45               445 	mov	txstate,a
   0E8E F5 48               446 	mov	txwr,a
   0E90 F5 49               447 	mov	txrd,a
   0E92 F5 4A               448 	mov	txtwr,a
   0E94 F5 50               449 	mov	pttforce,a
   0E96 F5 51               450 	mov	rxstate,a
   0E98 F5 52               451 	mov	rxwr,a
   0E9A F5 53               452 	mov	rxrd,a
   0E9C F5 54               453 	mov	rxtwr,a
   0E9E F5 44               454  	mov	irqcount,a
   0EA0 F5 6B               455 	mov	uartrd,a
   0EA2 F5 6A               456 	mov	uartwr,a
   0EA4 F5 86               457 	mov	dps,a
   0EA6 D2 0B               458 	setb	uartempty
                            459 	;; some indirect register setup
   0EA8 75 8E 30            460 	mov	ckcon,#0x30	; zero external wait states, to avoid chip bugs
                            461 	;; Timer setup:
                            462 	;; timer 0: LED blink    : T0M=0, CT0=0, MODE0=1
                            463 	;; timer 1: Baudrate gen : T1M=1, CT1=0, MODE1=2
   0EAB 75 89 21            464 	mov	tmod,#0x21
   0EAE 75 88 55            465 	mov	tcon,#0x55	; INT0/INT1 edge
   0EB1 75 8D 64            466 	mov	th1,#256-156	; 1200 bauds
   0EB4 75 87 00            467 	mov	pcon,#0		; SMOD0=0
                            468 	;; init USB subsystem
   0EB7 90 7F A1            469 	mov	dptr,#ISOCTL
   0EBA 74 01               470 	mov	a,#1		; disable ISO endpoints
   0EBC F0                  471 	movx	@dptr,a
   0EBD 90 7F AF            472 	mov	dptr,#USBBAV
   0EC0 74 01               473 	mov	a,#1		; enable autovector, disable breakpoint logic
   0EC2 F0                  474 	movx	@dptr,a
   0EC3 E4                  475 	clr	a
   0EC4 90 7F E0            476 	mov	dptr,#INISOVAL
   0EC7 F0                  477 	movx	@dptr,a
   0EC8 90 7F E1            478 	mov	dptr,#OUTISOVAL
   0ECB F0                  479 	movx	@dptr,a
   0ECC 90 7F DD            480 	mov	dptr,#USBPAIR
   0ECF 74 09               481 	mov	a,#0x9		; pair EP 2&3 for input & output
   0ED1 F0                  482 	movx	@dptr,a
   0ED2 90 7F DE            483 	mov	dptr,#IN07VAL
   0ED5 74 07               484 	mov	a,#0x7		; enable EP0+EP1+EP2
   0ED7 F0                  485 	movx	@dptr,a
   0ED8 90 7F DF            486 	mov	dptr,#OUT07VAL
   0EDB 74 05               487 	mov	a,#0x5		; enable EP0+EP2
   0EDD F0                  488 	movx	@dptr,a
                            489 	;; USB:	init endpoint toggles
   0EDE 90 7F D7            490 	mov	dptr,#TOGCTL
   0EE1 74 12               491 	mov	a,#0x12
   0EE3 F0                  492 	movx	@dptr,a
   0EE4 74 32               493 	mov	a,#0x32		; clear EP 2 in toggle
   0EE6 F0                  494 	movx	@dptr,a
   0EE7 74 02               495 	mov	a,#0x02
   0EE9 F0                  496 	movx	@dptr,a
   0EEA 74 22               497 	mov	a,#0x22		; clear EP 2 out toggle
   0EEC F0                  498 	movx	@dptr,a
                            499 	;; configure IO ports
   0EED 90 7F 93            500 	mov	dptr,#PORTACFG
   0EF0 74 00               501 	mov	a,#0
   0EF2 F0                  502 	movx	@dptr,a
   0EF3 90 7F 96            503 	mov	dptr,#OUTA
   0EF6 74 82               504 	mov	a,#0x82		; set PROG hi
   0EF8 F0                  505 	movx	@dptr,a
   0EF9 90 7F 9C            506 	mov	dptr,#OEA
   0EFC 74 C2               507 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   0EFE F0                  508 	movx	@dptr,a
   0EFF 90 7F 94            509 	mov	dptr,#PORTBCFG
   0F02 74 00               510 	mov	a,#0
   0F04 F0                  511 	movx	@dptr,a
   0F05 90 7F 9D            512 	mov	dptr,#OEB
   0F08 74 00               513 	mov	a,#0
   0F0A F0                  514 	movx	@dptr,a
   0F0B 90 7F 95            515 	mov	dptr,#PORTCCFG
   0F0E 74 C3               516 	mov	a,#0xc3		; RD/WR/TXD0/RXD0 are special function pins
   0F10 F0                  517 	movx	@dptr,a
   0F11 90 7F 98            518 	mov	dptr,#OUTC
   0F14 74 28               519 	mov	a,#0x28
   0F16 F0                  520 	movx	@dptr,a
   0F17 90 7F 9E            521 	mov	dptr,#OEC
   0F1A 74 2A               522 	mov	a,#0x2a		; out: LEDCON,LEDSTA,TCK  in: TDO,INIT
   0F1C F0                  523 	movx	@dptr,a
                            524 	;; enable interrupts
   0F1D 75 A8 92            525 	mov	ie,#0x92	; enable timer 0 and ser 0 int
   0F20 75 E8 01            526 	mov	eie,#0x01	; enable USB interrupts
   0F23 90 7F AE            527 	mov	dptr,#USBIEN
   0F26 74 01               528 	mov	a,#1		; enable SUDAV interrupt
   0F28 F0                  529 	movx	@dptr,a
   0F29 90 7F AC            530 	mov	dptr,#IN07IEN
   0F2C 74 03               531 	mov	a,#3		; enable EP0+EP1 interrupt
   0F2E F0                  532 	movx	@dptr,a
   0F2F 90 7F AD            533 	mov	dptr,#OUT07IEN
   0F32 74 01               534 	mov	a,#1		; enable EP0 interrupt
   0F34 F0                  535 	movx	@dptr,a
                            536 	;; initialize UART 0 for T7F communication
   0F35 75 98 52            537 	mov	scon0,#0x52	; Mode 1, Timer 1, Receiver enable
                            538 	;; Copy serial number
   0F38 78 F0               539 	mov	r0,#parserial
   0F3A 90 0E 63            540 	mov	dptr,#stringserial+2
   0F3D E2                  541 1$:	movx	a,@r0
   0F3E 60 06               542 	jz	2$
   0F40 F0                  543 	movx	@dptr,a
   0F41 A3                  544 	inc	dptr
   0F42 A3                  545 	inc	dptr
   0F43 08                  546 	inc	r0
   0F44 80 F7               547 	sjmp	1$
   0F46 E8                  548 2$:	mov	a,r0
   0F47 24 11               549 	add	a,#1-0xf0	; 1-parserial
   0F49 25 E0               550 	add	a,acc
   0F4B 90 0E 61            551 	mov	dptr,#stringserial
   0F4E F0                  552 	movx	@dptr,a
                            553 	;; copy configuration to bit addressable variables
   0F4F                     554 cpyconfig:
   0F4F 75 20 42            555 	mov	ctrlreg,#0x42
   0F52 78 E6               556 	mov	r0,#parextmodem
   0F54 E2                  557 	movx	a,@r0
   0F55 A2 E0               558 	mov	c,acc.0
   0F57 92 08               559 	mov	extmodem,c
   0F59 92 07               560 	mov	ctrl_txdsrc,c
   0F5B 92 03               561 	mov	ctrl_leddcdsrc,c
   0F5D 78 E7               562 	mov	r0,#parpttmute
   0F5F E2                  563 	movx	a,@r0
   0F60 A2 E0               564 	mov	c,acc.0
   0F62 92 09               565 	mov	pttmute,c
   0F64 92 01               566 	mov	ctrl_pttmute,c
                            567 	;; turn off transmitter
   0F66 E5 20               568 	mov	a,ctrlreg
   0F68 90 C0 08            569 	mov	dptr,#FSKCTRL
   0F6B F0                  570 	movx	@dptr,a
                            571 	;; send reset pulse to external modem if selected
   0F6C 90 C0 0C            572 	mov	dptr,#FSKMDISCTRIS
   0F6F 74 FF               573 	mov	a,#0xff
   0F71 F0                  574 	movx	@dptr,a
   0F72 F5 6D               575 	mov	mdisctris,a
   0F74 30 08 09            576 	jnb	extmodem,3$
   0F77 74 67               577 	mov	a,#0x67		; RESET, RTS and TxD are outputs
   0F79 F0                  578 	movx	@dptr,a
   0F7A 90 C0 0D            579 	mov	dptr,#FSKMDISCOUT
   0F7D 74 7F               580 	mov	a,#0x7f		; activate RESET
   0F7F F0                  581 	movx	@dptr,a
   0F80                     582 3$:
   0F80 90 C0 0A            583 	mov	dptr,#FSKT7FOUT
   0F83 74 1F               584 	mov	a,#0x1f
   0F85 F0                  585 	movx	@dptr,a
   0F86 F5 6C               586 	mov	t7fout,a
                            587 	;; initialize USB state
   0F88 E4                  588 	clr	a
   0F89 F5 6F               589 	mov	numconfig,a
   0F8B F5 70               590 	mov	altsetting,a
                            591 	;; give Windows a chance to finish the writecpucs control transfer
                            592 	;; 20ms delay loop
   0F8D 90 D1 20            593 	mov	dptr,#(-12000)&0xffff
   0F90 A3                  594 2$:	inc	dptr		; 3 cycles
   0F91 E5 82               595 	mov	a,dpl0		; 2 cycles
   0F93 45 83               596 	orl	a,dph0		; 2 cycles
   0F95 70 F9               597 	jnz	2$		; 3 cycles
                    0001    598 	.if	1
                            599 	;; disconnect from USB bus
   0F97 90 7F D6            600 	mov	dptr,#USBCS
   0F9A 74 0A               601 	mov	a,#10
   0F9C F0                  602 	movx	@dptr,a
                            603 	;; wait 0.3 sec
   0F9D 7A 1E               604 	mov	r2,#30
                            605 	;; 10ms delay loop
   0F9F 90 E8 90            606 0$:	mov	dptr,#(-6000)&0xffff
   0FA2 A3                  607 1$:	inc	dptr            ; 3 cycles
   0FA3 E5 82               608 	mov	a,dpl0          ; 2 cycles
   0FA5 45 83               609 	orl	a,dph0          ; 2 cycles
   0FA7 70 F9               610 	jnz	1$              ; 3 cycles
   0FA9 DA F4               611 	djnz	r2,0$
                            612 	;; reconnect to USB bus
   0FAB 90 7F D6            613 	mov	dptr,#USBCS
                            614 	;mov	a,#2		; 8051 handles control
                            615 	;movx	@dptr,a
   0FAE 74 06               616 	mov	a,#6		; reconnect, 8051 handles control
   0FB0 F0                  617 	movx	@dptr,a
                            618 	.endif
                            619 	;; terminate reset pulse to external modem if selected
   0FB1 30 08 08            620 	jnb	extmodem,4$
   0FB4 90 C0 0D            621 	mov	dptr,#FSKMDISCOUT
   0FB7 74 FF               622 	mov	a,#0xff		; deactivate RESET
   0FB9 F0                  623 	movx	@dptr,a
   0FBA F5 6E               624 	mov	mdiscout,a
   0FBC                     625 4$:
                            626 	;; preinit RAM for debugging purposes (optional)
                    0000    627 	.if	0
                            628 inittxbuf:
                            629 	mov	r7,#TXCHUNKS
                            630 	mov	dptr,#txbuf
                            631 	mov	a,#0x55+1
                            632 0$:	mov	r6,#64
                            633 1$:	movx	@dptr,a
                            634 	inc	dptr
                            635 	djnz	r6,1$
                            636 	djnz	r7,0$
                            637 	.endif
                    0000    638 	.if	0
                            639 initrxbuf:
                            640 	mov	r7,#RXCHUNKS
                            641 	mov	dptr,#rxbuf
                            642 	mov	a,#0xaa
                            643 0$:	mov	r6,#64
                            644 1$:	movx	@dptr,a
                            645 	inc	dptr
                            646 	djnz	r6,1$
                            647 	djnz	r7,0$
                            648 	.endif
                            649 
                            650 	;; final
   0FBC 12 0A F1            651 	lcall	fillusbintr
                            652 	;; kludge; first OUT2 packet seems to be bogus
                            653 	;; wait for packet with length 1 and contents 0x55
   0FBF                     654 waitpkt1:
   0FBF 90 7F C8            655 	mov	dptr,#OUT2CS
   0FC2 E0                  656 	movx	a,@dptr
   0FC3 20 E1 F9            657 	jb	acc.1,waitpkt1
   0FC6 90 7F C9            658 	mov	dptr,#OUT2BC
   0FC9 E0                  659 	movx	a,@dptr
   0FCA B4 01 0D            660 	cjne	a,#1,0$
   0FCD 90 7D C0            661 	mov	dptr,#OUT2BUF
   0FD0 E0                  662 	movx	a,@dptr
   0FD1 B4 55 06            663 	cjne	a,#0x55,0$
   0FD4 90 7F C9            664 	mov	dptr,#OUT2BC
   0FD7 F0                  665 	movx	@dptr,a
   0FD8 80 06               666 	sjmp	pkt1received
   0FDA 90 7F C9            667 0$:	mov	dptr,#OUT2BC
   0FDD F0                  668 	movx	@dptr,a
   0FDE 80 DF               669 	sjmp	waitpkt1
                            670 	;; clear RX FIFO
   0FE0                     671 pkt1received:
   0FE0 90 C0 03            672 	mov	dptr,#FSKRXFLAG
   0FE3 7F 40               673 	mov	r7,#64
   0FE5 E0                  674 1$:	movx	a,@dptr
   0FE6 DF FD               675 	djnz	r7,1$
                            676 	;; start normal operation
   0FE8 02 01 8E            677 	ljmp	mainloop
                            678 
                            679 	.area	CSEG (CODE)
                    0002    680 	ar2 = 0x02
                    0003    681 	ar3 = 0x03
                    0004    682 	ar4 = 0x04
                    0005    683 	ar5 = 0x05
                    0006    684 	ar6 = 0x06
                    0007    685 	ar7 = 0x07
                    0000    686 	ar0 = 0x00
                    0001    687 	ar1 = 0x01
                            688 
                            689 	;; WARNING!  The assembler doesn't check for
                            690 	;; out of range short jump labels!! Double check
                            691 	;; that the jump labels are within the range!
                            692 	
   0157                     693 setptt:	
   0157 E5 45               694 	mov	a,txstate
   0159 45 50               695 	orl	a,pttforce
   015B 60 18               696 	jz	pttoff
                            697 
   015D                     698 ptton:
   015D A2 08               699 	mov	c,extmodem
   015F 50 06               700 	jnc	0$
   0161 90 C0 0D            701 	mov	dptr,#FSKMDISCOUT
   0164 74 EF               702 	mov	a,#0xef
   0166 F0                  703 	movx	@dptr,a
   0167 92 01               704 0$:	mov	ctrl_pttmute,c
   0169 B3                  705 	cpl	c
   016A 92 00               706 	mov	ctrl_ptt,c
   016C D2 02               707 	setb	ctrl_ledptt
   016E E5 20               708 	mov	a,ctrlreg
   0170 90 C0 08            709 	mov	dptr,#FSKCTRL
   0173 F0                  710 	movx	@dptr,a
   0174 22                  711 	ret
                            712 
   0175                     713 pttoff:
   0175 C2 00               714 	clr	ctrl_ptt
   0177 A2 08               715 	mov	c,extmodem
   0179 50 06               716 	jnc	3$
   017B 90 C0 0D            717 	mov	dptr,#FSKMDISCOUT
   017E 74 FF               718 	mov	a,#0xff
   0180 F0                  719 	movx	@dptr,a
   0181 72 09               720 3$:	orl	c,pttmute
   0183 92 01               721 	mov	ctrl_pttmute,c
   0185 C2 02               722 	clr	ctrl_ledptt
   0187 E5 20               723 	mov	a,ctrlreg
   0189 90 C0 08            724 	mov	dptr,#FSKCTRL
   018C F0                  725 	movx	@dptr,a
   018D 22                  726 	ret
                            727 
   018E                     728 mainloop:
                            729 	;; debug: copyloop
                    0000    730 	.if	DEBUGIOCOPY
                            731 iocopyloop:
                            732 	mov	a,txrd
                            733 	cjne	a,txwr,0$
                            734 	ljmp	iocopyloop
                            735 1$:	mov	a,@r0
                            736 	mov	@r1,a
                            737 	jz	3$
                            738 	mov	r7,a
                            739 4$:	inc	dps
                            740 	movx	a,@dptr
                            741 	inc	dptr
                            742 	dec	dps
                            743 	movx	@dptr,a
                            744 	inc	dptr
                            745 	djnz	r7,4$
                            746 3$:	mov	a,txrd
                            747 	inc	a
                            748 	anl	a,#(TXCHUNKS-1)
                            749 	mov	txrd,a
                            750 	mov	a,@r0
                            751 	add	a,#-0x40
                            752 	jc	5$
                            753 	mov	rxwr,rxtwr
                            754 5$:	ljmp	iocopyloop
                            755 0$:	;; txpointers
                            756 	add	a,#txbcnt
                            757 	mov	r0,a
                    0001    758 	.if	1
                            759 	mov	a,txrd
                            760 	rr	a
                            761 	rr	a
                            762 	mov	dpl1,a
                            763 	anl	a,#0xc0
                            764 	add	a,#<txbuf
                            765 	xch	a,dpl1
                            766 	anl	a,#0x3f
                            767 	addc	a,#>txbuf
                            768 	mov	dph1,a
                            769 	.else
                            770 	mov	a,txrd
                            771 	rr	a
                            772 	rr	a
                            773 	mov	dpl1,a
                            774 	anl	a,#0xc0
                            775 	xch	a,dpl1
                            776 	anl	a,#0x3f
                            777 	add	a,#>txbuf
                            778 	mov	dph1,a
                            779 	.endif
                            780 	;; rxpointers
                            781 	mov	a,rxtwr
                            782 	add	a,#rxbcnt
                            783 	mov	r1,a
                    0001    784 	.if	1
                            785 	mov	a,rxtwr
                            786 	rr	a
                            787 	rr	a
                            788 	mov	dpl0,a
                            789 	anl	a,#0xc0
                            790 	add	a,#<rxbuf
                            791 	xch	a,dpl0
                            792 	anl	a,#0x3f
                            793 	addc	a,#>rxbuf
                            794 	mov	dph0,a
                            795 	.else
                            796 	mov	a,rxtwr
                            797 	rr	a
                            798 	rr	a
                            799 	mov	dpl0,a
                            800 	anl	a,#0xc0
                            801 	xch	a,dpl0
                            802 	anl	a,#0x3f
                            803 	add	a,#>rxbuf
                            804 	mov	dph0,a
                            805 	.endif
                            806 	;; update rxpointer
                            807 	mov	a,rxtwr
                            808 	inc	a
                            809 	anl	a,#(RXCHUNKS-1)
                            810 	mov	rxtwr,a
                            811 	cjne	a,rxrd,1$
                            812 	ljmp	iocopyloop
                            813 	.endif
                            814 	;; first do TX
   018E                     815 txstartloop:
                            816 	;; check if idle or new packet
   018E E5 45               817 	mov	a,txstate
   0190 60 06               818 	jz	txchknewpkt
   0192 B4 02 52            819 	cjne	a,#2,txflagprepare
   0195 02 02 4F            820 	ljmp	txchunkstart
   0198                     821 txchknewpkt:
   0198 E5 48               822 	mov	a,txwr
   019A B5 49 0C            823 	cjne	a,txrd,txbufnempty
   019D 10 0A 03            824 	jbc	pttforcechg,txidlepttforce
   01A0                     825 rxstartloop1:
   01A0 02 02 B3            826 	ljmp	rxstartloop
   01A3                     827 txidlepttforce:
   01A3 12 01 57            828 	lcall	setptt
   01A6 02 02 B3            829 	ljmp	rxstartloop
   01A9                     830 txbufnempty:
                            831 	;; start txdelay
                            832 	;; check length of first packet
   01A9 E5 49               833 	mov	a,txrd
   01AB 24 80               834 	add	a,#txbcnt
   01AD F8                  835 	mov	r0,a
   01AE E6                  836 	mov	a,@r0
   01AF 24 FC               837 	add	a,#-4
   01B1 40 03               838 	jc	1$
   01B3 02 02 98            839 	ljmp	txnextpacket
   01B6                     840 1$:
                            841 	;; read the number of flags to send from the first two bytes of the first packet
                    0001    842 	.if	1
   01B6 E5 49               843 	mov	a,txrd
   01B8 03                  844 	rr	a
   01B9 03                  845 	rr	a
   01BA F5 82               846 	mov	dpl0,a
   01BC 54 C0               847 	anl	a,#0xc0
   01BE 24 00               848 	add	a,#<txbuf
   01C0 C5 82               849 	xch	a,dpl0
   01C2 54 3F               850 	anl	a,#0x3f
   01C4 34 10               851 	addc	a,#>txbuf
   01C6 F5 83               852 	mov	dph0,a
                            853 	.else
                            854 	mov	a,txrd
                            855 	rr	a
                            856 	rr	a
                            857 	mov	dpl0,a
                            858 	anl	a,#0xc0
                            859 	xch	a,dpl0
                            860 	anl	a,#0x3f
                            861 	add	a,#>txbuf
                            862 	mov	dph0,a
                            863 	.endif
   01C8 E0                  864 	movx	a,@dptr
   01C9 F5 46               865 	mov	flagcnt,a
   01CB A3                  866 	inc	dptr
   01CC E0                  867 	movx	a,@dptr
   01CD F5 47               868 	mov	flagcnt+1,a
   01CF 05 45               869 	inc	txstate
   01D1 75 4F 02            870 	mov	txskip,#2
                            871 	;; sanity check (limit txdelay to about 8s)
   01D4 78 E2               872 	mov	r0,#parbitratetx+2
   01D6 E2                  873 	movx	a,@r0
   01D7 70 0B               874 	jnz	2$
   01D9 78 E1               875 	mov	r0,#parbitratetx+1
   01DB E2                  876 	movx	a,@r0
   01DC C3                  877 	clr	c
   01DD 95 47               878 	subb	a,flagcnt+1
   01DF 50 03               879 	jnc	2$
   01E1 E2                  880 	movx	a,@r0
   01E2 F5 47               881 	mov	flagcnt+1,a
   01E4                     882 2$:
                            883 	;; turn on PTT
   01E4 12 01 5D            884 	lcall	ptton
                            885 	;; flag send routine
   01E7                     886 txflagprepare:
   01E7 90 C0 00            887 	mov	dptr,#FSKTXCNT
   01EA E0                  888 	movx	a,@dptr
   01EB 60 B3               889 	jz	rxstartloop1
   01ED FF                  890 	mov	r7,a
                            891 	;; check if tx ready if external modem
   01EE 90 C0 0E            892 	mov	dptr,#FSKMDISCIN
   01F1 E0                  893 	movx	a,@dptr
   01F2 90 C0 03            894 	mov	dptr,#FSKTXRAWCLR
   01F5 A2 E5               895 	mov	c,acc.5
   01F7 82 08               896 	anl	c,extmodem
   01F9 40 28               897 	jc	txflgcntbig
                            898 	;clr	c
   01FB E5 46               899 	mov	a,flagcnt
   01FD FE                  900 	mov	r6,a
   01FE 9F                  901 	subb	a,r7
   01FF F5 46               902 	mov	flagcnt,a
   0201 E5 47               903 	mov	a,flagcnt+1
   0203 94 00               904 	subb	a,#0
   0205 F5 47               905 	mov	flagcnt+1,a
   0207 50 1A               906 	jnc	txflgcntbig
                            907 	;; send final flags, update state
   0209 EE                  908 	mov	a,r6
   020A 60 05               909 	jz	1$
   020C 74 7E               910 	mov	a,#0x7e
   020E F0                  911 0$:	movx	@dptr,a
   020F DE FD               912 	djnz	r6,0$
   0211 E5 45               913 1$:	mov	a,txstate
   0213 04                  914 	inc	a
   0214 54 03               915 	anl	a,#3
   0216 F5 45               916 	mov	txstate,a
   0218 70 06               917 	jnz	2$
                            918 	;; turn off PTT
   021A 12 01 57            919 	lcall	setptt
   021D 02 02 B3            920 	ljmp	rxstartloop
   0220 02 02 2B            921 2$:	ljmp	txnewchunk
                            922 	;; send txdelay flags
   0223                     923 txflgcntbig:
   0223 74 7E               924 	mov	a,#0x7e
   0225 F0                  925 0$:	movx	@dptr,a
   0226 DF FD               926 	djnz	r7,0$
   0228                     927 rxstartloop2:
   0228 02 02 B3            928 	ljmp	rxstartloop
                            929 
   022B                     930 txnewchunk:
   022B E5 49               931 	mov	a,txrd
   022D 24 80               932 	add	a,#txbcnt
   022F F5 4E               933 	mov	txbptr,a
   0231 F8                  934 	mov	r0,a
   0232 E6                  935 	mov	a,@r0
   0233 F4                  936 	cpl	a
   0234 04                  937 	inc	a
   0235 25 4F               938 	add	a,txskip
   0237 40 5F               939 	jc	txnextpacket
   0239 F5 4B               940 	mov	txcnt,a
                    0001    941 	.if	1
   023B E5 49               942 	mov	a,txrd
   023D 03                  943 	rr	a
   023E 03                  944 	rr	a
   023F F5 4C               945 	mov	txaddr,a
   0241 54 C0               946 	anl	a,#0xc0
   0243 25 4F               947 	add	a,txskip
   0245 24 00               948 	add	a,#<txbuf
   0247 C5 4C               949 	xch	a,txaddr
   0249 54 3F               950 	anl	a,#0x3f
   024B 34 10               951 	addc	a,#>txbuf
   024D F5 4D               952 	mov	txaddr+1,a
                            953 	.else
                            954 	mov	a,txrd
                            955 	rr	a
                            956 	rr	a
                            957 	mov	txaddr,a
                            958 	anl	a,#0xc0
                            959 	add	a,txskip
                            960 	xch	a,txaddr
                            961 	anl	a,#0x3f
                            962 	add	a,#>txbuf
                            963 	mov	txaddr+1,a
                            964 	.endif
   024F                     965 txchunkstart:
   024F 90 C0 00            966 	mov	dptr,#FSKTXCNT
   0252 E0                  967 	movx	a,@dptr
   0253 60 D3               968 	jz	rxstartloop2
   0255 FF                  969 	mov	r7,a
   0256 E5 4B               970 	mov	a,txcnt
   0258 60 25               971 	jz	txchunkend
   025A 2F                  972 	add	a,r7
   025B C5 4B               973 	xch	a,txcnt
   025D 50 06               974 	jnc	0$
   025F F4                  975 	cpl	a
   0260 04                  976 	inc	a
   0261 FF                  977 	mov	r7,a
   0262 75 4B 00            978 	mov	txcnt,#0
   0265 90 C0 00            979 0$:	mov	dptr,#FSKTXDATA
   0268 85 4C 84            980 	mov	dpl1,txaddr
   026B 85 4D 85            981 	mov	dph1,txaddr+1
   026E 05 86               982 1$:	inc	dps
   0270 E0                  983 	movx	a,@dptr
   0271 A3                  984 	inc	dptr
   0272 15 86               985 	dec	dps
   0274 F0                  986 	movx	@dptr,a
   0275 DF F7               987 	djnz	r7,1$
   0277 85 84 4C            988 	mov	txaddr,dpl1
   027A 85 85 4D            989 	mov	txaddr+1,dph1
   027D 80 D0               990 	sjmp	txchunkstart
   027F                     991 txchunkend:
   027F 75 4F 00            992 	mov	txskip,#0
   0282 A8 4E               993 	mov	r0,txbptr
   0284 E6                  994 	mov	a,@r0
   0285 20 E6 13            995 	jb	acc.6,txnonewframe
   0288 EF                  996 	mov	a,r7
   0289 24 FD               997 	add	a,#-3
   028B 50 9B               998 	jnc	rxstartloop2
   028D 90 C0 01            999 	mov	dptr,#FSKTXCRC
   0290 F0                 1000 	movx	@dptr,a
   0291 F0                 1001 	movx	@dptr,a
   0292 90 C0 03           1002 	mov	dptr,#FSKTXRAWCLR
   0295 74 7E              1003 	mov	a,#0x7e
   0297 F0                 1004 	movx	@dptr,a
   0298                    1005 txnextpacket:
   0298 75 4F 02           1006 	mov	txskip,#2
   029B                    1007 txnonewframe:
   029B E5 49              1008 	mov	a,txrd
   029D 04                 1009 	inc	a
   029E 54 0F              1010 	anl	a,#TXCHUNKS-1
   02A0 F5 49              1011 	mov	txrd,a
   02A2 B5 48 86           1012 	cjne	a,txwr,txnewchunk
   02A5 05 45              1013 	inc	txstate
                           1014 	;; note: we need to stuff in more than 32 flags, to fill the buffer
                           1015 	;; (so we don't turn off PTT too early)
   02A7 75 46 28           1016 	mov	flagcnt,#40
   02AA 75 47 00           1017 	mov	flagcnt+1,#0
   02AD 02 01 E7           1018 	ljmp	txflagprepare
                           1019 
                    0000   1020 	.if	DEBUGRECEIVER
                           1021 rxstartloop:
                           1022 	mov	dptr,#FSKRXCNT
                           1023 	movx	a,@dptr
                           1024 	mov	tmprxcnt,a
                           1025 	inc	dpl0
                           1026 	add	a,#-16
                           1027 	jnc	2$
                    0001   1028 	.if	1
                           1029 	mov	a,rxwr
                           1030 	rr	a
                           1031 	rr	a
                           1032 	mov	dpl1,a
                           1033 	anl	a,#0xc0
                           1034 	add	a,#<rxbuf
                           1035 	xch	a,dpl1
                           1036 	anl	a,#0x3f
                           1037 	addc	a,#>rxbuf
                           1038 	mov	dph1,a
                           1039 	.else
                           1040 	mov	a,rxwr
                           1041 	rr	a
                           1042 	rr	a
                           1043 	mov	dpl1,a
                           1044 	anl	a,#0xc0
                           1045 	xch	a,dpl1
                           1046 	anl	a,#0x3f
                           1047 	add	a,#>rxbuf
                           1048 	mov	dph1,a	
                           1049 	.endif
                           1050 	mov	a,rxwr
                           1051 	add	a,#rxbcnt
                           1052 	mov	r0,a
                           1053 	mov	r7,#0
                           1054 0$:	movx	a,@dptr
                           1055 	jb	acc.5,1$
                           1056 	inc	dps
                           1057 	movx	@dptr,a
                           1058 	inc	dptr
                           1059 	dec	dps
                           1060 	inc	r7
                           1061 	jb	acc.7,0$
                           1062 	movx	a,@dptr
                           1063 	inc	dps
                           1064 	movx	@dptr,a
                           1065 	inc	dptr
                           1066 	dec	dps
                           1067 	inc	r7
                           1068 	sjmp	0$
                           1069 1$:	mov	@r0,ar7
                           1070 	mov	a,rxwr
                           1071 	inc	a
                           1072 	anl	a,#RXCHUNKS-1
                           1073 	mov	rxwr,a
                           1074 	mov	rxtwr,a
                           1075 2$:	ljmp	txstartloop
                           1076 	
                           1077 	.else
                           1078 	
   02B0                    1079 usbiostart1:
   02B0 02 03 55           1080 	ljmp	usbiostart
                           1081 	;; real receiver starts here
   02B3                    1082 rxstartloop:
   02B3 90 C0 02           1083 	mov	dptr,#FSKRXDATA
   02B6 E5 51              1084 	mov	a,rxstate
   02B8 70 32              1085 	jnz	rxdataloopstart
                           1086 	;; hunt for flags
   02BA 80 01              1087 	sjmp	$1
   02BC E0                 1088 $0:	movx	a,@dptr		; read data value
   02BD E0                 1089 $1:	movx	a,@dptr		; read control value
   02BE 20 E5 EF           1090 	jb	acc.5,usbiostart1
   02C1 30 E7 F8           1091 	jnb	acc.7,$0
   02C4                    1092 rxpreparepkt:
   02C4 75 56 00           1093 	mov	rxcntc,#0
   02C7 75 51 01           1094 	mov	rxstate,#1
   02CA E5 52              1095 	mov	a,rxwr
   02CC F5 54              1096 	mov	rxtwr,a
   02CE                    1097 rxnewchunk:
                    0001   1098 	.if	1
   02CE E5 54              1099 	mov	a,rxtwr
   02D0 03                 1100 	rr	a
   02D1 03                 1101 	rr	a
   02D2 F5 57              1102 	mov	rxaddr,a
   02D4 54 C0              1103 	anl	a,#0xc0
   02D6 24 00              1104 	add	a,#<rxbuf
   02D8 C5 57              1105 	xch	a,rxaddr
   02DA 54 3F              1106 	anl	a,#0x3f
   02DC 34 14              1107 	addc	a,#>rxbuf
   02DE F5 58              1108 	mov	rxaddr+1,a
                           1109 	.else
                           1110 	mov	a,rxtwr
                           1111 	rr	a
                           1112 	rr	a
                           1113 	mov	rxaddr,a
                           1114 	anl	a,#0xc0
                           1115 	xch	a,rxaddr
                           1116 	anl	a,#0x3f
                           1117 	add	a,#>rxbuf
                           1118 	mov	rxaddr+1,a
                           1119 	.endif
   02E0 75 55 40           1120 	mov	rxcnt,#64
   02E3 02 02 B3           1121 	ljmp	rxstartloop	; sjmp would be enough
                           1122 
   02E6                    1123 rxabortframe:
   02E6 75 51 00           1124 	mov	rxstate,#0
   02E9 02 02 B3           1125 	ljmp	rxstartloop
                           1126 
   02EC                    1127 rxdataloopstart:
   02EC AF 55              1128 	mov	r7,rxcnt
   02EE 85 57 84           1129 	mov	dpl1,rxaddr
   02F1 85 58 85           1130 	mov	dph1,rxaddr+1
   02F4 E0                 1131 0$:	movx	a,@dptr		; read command
   02F5 20 E6 21           1132 	jb	acc.6,1$
   02F8 E0                 1133 	movx	a,@dptr
   02F9 05 86              1134 	inc	dps
   02FB F0                 1135 	movx	@dptr,a
   02FC A3                 1136 	inc	dptr
   02FD 15 86              1137 	dec	dps
   02FF DF F3              1138 	djnz	r7,0$
   0301 E5 54              1139 	mov	a,rxtwr
   0303 24 90              1140 	add	a,#rxbcnt
   0305 F8                 1141 	mov	r0,a
   0306 76 40              1142 	mov	@r0,#64
   0308 E5 54              1143 	mov	a,rxtwr
   030A 04                 1144 	inc	a
   030B 54 1F              1145 	anl	a,#RXCHUNKS-1
   030D B5 53 03           1146 	cjne	a,rxrd,2$
   0310 02 02 E6           1147 	ljmp	rxabortframe
   0313 F5 54              1148 2$:	mov	rxtwr,a
   0315 05 56              1149 	inc	rxcntc
   0317 80 B5              1150 	sjmp	rxnewchunk
   0319 8F 55              1151 1$:	mov	rxcnt,r7
   031B 85 84 57           1152 	mov	rxaddr,dpl1
   031E 85 85 58           1153 	mov	rxaddr+1,dph1
   0321 20 E5 8C           1154 	jb	acc.5,usbiostart1	; empty
   0324 20 E4 BF           1155 	jb	acc.4,rxabortframe	; abort received
   0327 54 0F              1156 	anl	a,#0xf
   0329 B4 0E 98           1157 	cjne	a,#0xe,rxpreparepkt	; CRC not ok or residue error
   032C E5 54              1158 	mov	a,rxtwr
   032E 24 90              1159 	add	a,#rxbcnt
   0330 F8                 1160 	mov	r0,a
   0331 74 40              1161 	mov	a,#64
   0333 C3                 1162 	clr	c
   0334 9F                 1163 	subb	a,r7
   0335 F6                 1164 	mov	@r0,a
   0336 E5 56              1165 	mov	a,rxcntc
   0338 70 07              1166 	jnz	3$
   033A 74 FC              1167 	mov	a,#-4
   033C 26                 1168 	add	a,@r0
   033D 50 13              1169 	jnc	6$		; frame too short
   033F 80 04              1170 	sjmp	4$
   0341 24 F8              1171 3$:	add	a,#-8
   0343 40 0D              1172 	jc	6$		; frame too long
   0345 E5 54              1173 4$:	mov	a,rxtwr
   0347 04                 1174 	inc	a
   0348 54 1F              1175 	anl	a,#RXCHUNKS-1
   034A B5 53 03           1176 	cjne	a,rxrd,5$
   034D 02 02 C4           1177 	ljmp	rxpreparepkt
   0350 F5 52              1178 5$:	mov	rxwr,a
   0352 02 02 C4           1179 6$:	ljmp	rxpreparepkt
                           1180 	.endif
                           1181 
   0355                    1182 usbiostart:
                           1183 	;; check for USB modem->host
   0355 E5 53              1184 	mov	a,rxrd
   0357 B5 52 4F           1185 	cjne	a,rxwr,usbcheckin
                           1186 	;; check for USB host->modem
   035A                    1187 usbcheckout:
   035A 90 7F C8           1188 	mov	dptr,#OUT2CS
   035D E0                 1189 	movx	a,@dptr
   035E 20 E1 46           1190 	jb	acc.1,endusb2
   0361 E5 4A              1191 	mov	a,txtwr
   0363 24 80              1192 	add	a,#txbcnt
   0365 F8                 1193 	mov	r0,a
   0366 E5 4A              1194 	mov	a,txtwr
   0368 04                 1195 	inc	a
   0369 54 0F              1196 	anl	a,#(TXCHUNKS-1)
   036B B5 49 03           1197 	cjne	a,txrd,usbout2
   036E 02 03 E7           1198 	ljmp	endusb
   0371                    1199 usbout2:
   0371 FF                 1200 	mov	r7,a
   0372 90 7F C9           1201 	mov	dptr,#OUT2BC
   0375 E0                 1202 	movx	a,@dptr
   0376 F6                 1203 	mov	@r0,a
   0377 FD                 1204 	mov	r5,a
   0378 60 20              1205 	jz	usbout3
   037A FE                 1206 	mov	r6,a
                    0001   1207 	.if	1
   037B E5 4A              1208 	mov	a,txtwr
   037D 03                 1209 	rr	a
   037E 03                 1210 	rr	a
   037F F5 84              1211 	mov	dpl1,a
   0381 54 C0              1212 	anl	a,#0xc0
   0383 24 00              1213 	add	a,#<txbuf
   0385 C5 84              1214 	xch	a,dpl1
   0387 54 3F              1215 	anl	a,#0x3f
   0389 34 10              1216 	addc	a,#>txbuf
   038B F5 85              1217 	mov	dph1,a
                           1218 	.else
                           1219 	mov	a,txtwr
                           1220 	rr	a
                           1221 	rr	a
                           1222 	mov	dpl1,a
                           1223 	anl	a,#0xc0
                           1224 	xch	a,dpl1
                           1225 	anl	a,#0x3f
                           1226 	add	a,#>txbuf
                           1227 	mov	dph1,a
                           1228 	.endif
   038D 90 7D C0           1229 	mov	dptr,#OUT2BUF
   0390                    1230 usboutloop:
   0390 E0                 1231 	movx	a,@dptr
   0391 A3                 1232 	inc	dptr
   0392 05 86              1233 	inc	dps
   0394 F0                 1234 	movx	@dptr,a
   0395 A3                 1235 	inc	dptr
   0396 15 86              1236 	dec	dps
   0398 DE F6              1237 	djnz	r6,usboutloop
   039A                    1238 usbout3:
   039A 8F 4A              1239 	mov	txtwr,r7
   039C ED                 1240 	mov	a,r5
   039D 24 C0              1241 	add	a,#-64
   039F 60 02              1242 	jz	usbout4
   03A1 8F 48              1243 	mov	txwr,r7
   03A3                    1244 usbout4:	
   03A3 90 7F C9           1245 	mov	dptr,#OUT2BC
   03A6 F0                 1246 	movx	@dptr,a		; rearm OUT2
   03A7                    1247 endusb2:
   03A7 80 3E              1248 	sjmp	endusb
                           1249 
   03A9                    1250 usbcheckin:
   03A9 90 7F B8           1251 	mov	dptr,#IN2CS
   03AC E0                 1252 	movx	a,@dptr
   03AD 20 E1 AA           1253 	jb	acc.1,usbcheckout
                           1254 
                           1255 	;ljmp	xxinbuf
                           1256 	
   03B0 E5 53              1257 	mov	a,rxrd
   03B2 24 90              1258 	add	a,#rxbcnt
   03B4 F8                 1259 	mov	r0,a
   03B5 E6                 1260 	mov	a,@r0
   03B6 60 20              1261 	jz	usbin1
   03B8 FE                 1262 	mov	r6,a
                    0001   1263 	.if	1
   03B9 E5 53              1264 	mov	a,rxrd
   03BB 03                 1265 	rr	a
   03BC 03                 1266 	rr	a
   03BD F5 84              1267 	mov	dpl1,a
   03BF 54 C0              1268 	anl	a,#0xc0
   03C1 24 00              1269 	add	a,#<rxbuf
   03C3 C5 84              1270 	xch	a,dpl1	
   03C5 54 3F              1271 	anl	a,#0x3f
   03C7 34 14              1272 	addc	a,#>rxbuf
   03C9 F5 85              1273 	mov	dph1,a
                           1274 	.else
                           1275 	mov	a,rxrd
                           1276 	rr	a
                           1277 	rr	a
                           1278 	mov	dpl1,a
                           1279 	anl	a,#0xc0
                           1280 	xch	a,dpl1	
                           1281 	anl	a,#0x3f
                           1282 	addc	a,#>rxbuf
                           1283 	mov	dph1,a
                           1284 	.endif
   03CB 90 7E 00           1285 	mov	dptr,#IN2BUF
   03CE                    1286 usbinloop:
   03CE 05 86              1287 	inc	dps
   03D0 E0                 1288 	movx	a,@dptr
   03D1 A3                 1289 	inc	dptr
   03D2 15 86              1290 	dec	dps
   03D4 F0                 1291 	movx	@dptr,a
   03D5 A3                 1292 	inc	dptr
   03D6 DE F6              1293 	djnz	r6,usbinloop
   03D8                    1294 usbin1:
   03D8 E6                 1295 	mov	a,@r0
   03D9 90 7F B9           1296 	mov	dptr,#IN2BC
   03DC F0                 1297 	movx	@dptr,a
   03DD E5 53              1298 	mov	a,rxrd
   03DF 04                 1299 	inc	a
   03E0 54 1F              1300 	anl	a,#(RXCHUNKS-1)
   03E2 F5 53              1301 	mov	rxrd,a
   03E4 02 03 5A           1302 	ljmp	usbcheckout
                           1303 
   03E7                    1304 endusb:
   03E7 02 01 8E           1305 	ljmp	txstartloop
                           1306 
                           1307 
                           1308 	;; ------------------ interrupt handlers ------------------------
                           1309 
   03EA                    1310 int0_isr:
   03EA C0 E0              1311 	push	acc
   03EC C0 F0              1312 	push	b
   03EE C0 82              1313 	push	dpl0
   03F0 C0 83              1314 	push	dph0
   03F2 C0 D0              1315 	push	psw
   03F4 75 D0 00           1316 	mov	psw,#0x00
   03F7 C0 86              1317 	push	dps
   03F9 75 86 00           1318 	mov	dps,#0
                           1319 	;; clear interrupt
   03FC C2 89              1320 	clr	tcon+1
                           1321 	;; handle interrupt
                           1322 	;; epilogue
   03FE D0 86              1323 	pop	dps
   0400 D0 D0              1324 	pop	psw
   0402 D0 83              1325 	pop	dph0
   0404 D0 82              1326 	pop	dpl0
   0406 D0 F0              1327 	pop	b
   0408 D0 E0              1328 	pop	acc
   040A 32                 1329 	reti
                           1330 
   040B                    1331 timer0_isr:
   040B C0 E0              1332 	push	acc
   040D C0 F0              1333 	push	b
   040F C0 82              1334 	push	dpl0
   0411 C0 83              1335 	push	dph0
   0413 C0 D0              1336 	push	psw
   0415 75 D0 00           1337 	mov	psw,#0x00
   0418 C0 86              1338 	push	dps
   041A 75 86 00           1339 	mov	dps,#0
                           1340 	;; clear interrupt
   041D C2 8D              1341 	clr	tcon+5
                           1342 	;; handle interrupt
                    0000   1343 	.if	0
                           1344 	inc	leddiv
                           1345 	mov	a,leddiv
                           1346 	anl	a,#7
                           1347 	jnz	0$
                           1348 	mov	dptr,#OUTC
                           1349 	movx	a,@dptr
                           1350 	xrl	a,#0x08
                           1351 	movx	@dptr,a
                           1352 0$:
                           1353 	.endif
                           1354 	;; epilogue
   041F D0 86              1355 	pop	dps
   0421 D0 D0              1356 	pop	psw
   0423 D0 83              1357 	pop	dph0
   0425 D0 82              1358 	pop	dpl0
   0427 D0 F0              1359 	pop	b
   0429 D0 E0              1360 	pop	acc
   042B 32                 1361 	reti
                           1362 
   042C                    1363 int1_isr:
   042C C0 E0              1364 	push	acc
   042E C0 F0              1365 	push	b
   0430 C0 82              1366 	push	dpl0
   0432 C0 83              1367 	push	dph0
   0434 C0 D0              1368 	push	psw
   0436 75 D0 00           1369 	mov	psw,#0x00
   0439 C0 86              1370 	push	dps
   043B 75 86 00           1371 	mov	dps,#0
                           1372 	;; clear interrupt
   043E C2 8B              1373 	clr	tcon+3
                           1374 	;; handle interrupt
                           1375 	;; epilogue
   0440 D0 86              1376 	pop	dps
   0442 D0 D0              1377 	pop	psw
   0444 D0 83              1378 	pop	dph0
   0446 D0 82              1379 	pop	dpl0
   0448 D0 F0              1380 	pop	b
   044A D0 E0              1381 	pop	acc
   044C 32                 1382 	reti
                           1383 
   044D                    1384 timer1_isr:
   044D C0 E0              1385 	push	acc
   044F C0 F0              1386 	push	b
   0451 C0 82              1387 	push	dpl0
   0453 C0 83              1388 	push	dph0
   0455 C0 D0              1389 	push	psw
   0457 75 D0 00           1390 	mov	psw,#0x00
   045A C0 86              1391 	push	dps
   045C 75 86 00           1392 	mov	dps,#0
                           1393 	;; clear interrupt
   045F C2 8F              1394 	clr	tcon+7
                           1395 	;; handle interrupt
                           1396 	;; epilogue
   0461 D0 86              1397 	pop	dps
   0463 D0 D0              1398 	pop	psw
   0465 D0 83              1399 	pop	dph0
   0467 D0 82              1400 	pop	dpl0
   0469 D0 F0              1401 	pop	b
   046B D0 E0              1402 	pop	acc
   046D 32                 1403 	reti
                           1404 
   046E                    1405 ser0_isr:
   046E C0 E0              1406 	push	acc
   0470 C0 F0              1407 	push	b
   0472 C0 82              1408 	push	dpl0
   0474 C0 83              1409 	push	dph0
   0476 C0 D0              1410 	push	psw
   0478 75 D0 00           1411 	mov	psw,#0x00
   047B C0 86              1412 	push	dps
   047D 75 86 00           1413 	mov	dps,#0
   0480 C0 00              1414 	push	ar0
                           1415 	;; clear interrupt
   0482 10 98 16           1416 	jbc	scon0+0,1$	; RI
   0485 10 99 0F           1417 0$:	jbc	scon0+1,2$	; TI
                           1418 	;; handle interrupt
                           1419 	;; epilogue
   0488 D0 00              1420 3$:	pop	ar0
   048A D0 86              1421 	pop	dps
   048C D0 D0              1422 	pop	psw
   048E D0 83              1423 	pop	dph0
   0490 D0 82              1424 	pop	dpl0
   0492 D0 F0              1425 	pop	b
   0494 D0 E0              1426 	pop	acc
   0496 32                 1427 	reti
                           1428 
   0497 D2 0B              1429 2$:	setb	uartempty
   0499 80 EA              1430 	sjmp	0$
                           1431 
   049B E5 6A              1432 1$:	mov	a,uartwr
   049D 24 5A              1433 	add	a,#uartbuf
   049F F8                 1434 	mov	r0,a
   04A0 E5 99              1435 	mov	a,sbuf0
   04A2 F6                 1436 	mov	@r0,a
   04A3 E5 6A              1437 	mov	a,uartwr
   04A5 04                 1438 	inc	a
   04A6 54 0F              1439 	anl	a,#0xf
   04A8 F5 6A              1440 	mov	uartwr,a
   04AA 80 DC              1441 	sjmp	3$
                           1442 
   04AC                    1443 timer2_isr:
   04AC C0 E0              1444 	push	acc
   04AE C0 F0              1445 	push	b
   04B0 C0 82              1446 	push	dpl0
   04B2 C0 83              1447 	push	dph0
   04B4 C0 D0              1448 	push	psw
   04B6 75 D0 00           1449 	mov	psw,#0x00
   04B9 C0 86              1450 	push	dps
   04BB 75 86 00           1451 	mov	dps,#0
                           1452 	;; clear interrupt
   04BE C2 CF              1453 	clr	t2con+7
                           1454 	;; handle interrupt
                           1455 	;; epilogue
   04C0 D0 86              1456 	pop	dps
   04C2 D0 D0              1457 	pop	psw
   04C4 D0 83              1458 	pop	dph0
   04C6 D0 82              1459 	pop	dpl0
   04C8 D0 F0              1460 	pop	b
   04CA D0 E0              1461 	pop	acc
   04CC 32                 1462 	reti
                           1463 
   04CD                    1464 resume_isr:
   04CD C0 E0              1465 	push	acc
   04CF C0 F0              1466 	push	b
   04D1 C0 82              1467 	push	dpl0
   04D3 C0 83              1468 	push	dph0
   04D5 C0 D0              1469 	push	psw
   04D7 75 D0 00           1470 	mov	psw,#0x00
   04DA C0 86              1471 	push	dps
   04DC 75 86 00           1472 	mov	dps,#0
                           1473 	;; clear interrupt
   04DF C2 DC              1474 	clr	eicon+4
                           1475 	;; handle interrupt
                           1476 	;; epilogue
   04E1 D0 86              1477 	pop	dps
   04E3 D0 D0              1478 	pop	psw
   04E5 D0 83              1479 	pop	dph0
   04E7 D0 82              1480 	pop	dpl0
   04E9 D0 F0              1481 	pop	b
   04EB D0 E0              1482 	pop	acc
   04ED 32                 1483 	reti
                           1484 
   04EE                    1485 ser1_isr:
   04EE C0 E0              1486 	push	acc
   04F0 C0 F0              1487 	push	b
   04F2 C0 82              1488 	push	dpl0
   04F4 C0 83              1489 	push	dph0
   04F6 C0 D0              1490 	push	psw
   04F8 75 D0 00           1491 	mov	psw,#0x00
   04FB C0 86              1492 	push	dps
   04FD 75 86 00           1493 	mov	dps,#0
                           1494 	;; clear interrupt
   0500 C2 C0              1495 	clr	scon1+0
   0502 C2 C1              1496 	clr	scon1+1
                           1497 	;; handle interrupt
                           1498 	;; epilogue
   0504 D0 86              1499 	pop	dps
   0506 D0 D0              1500 	pop	psw
   0508 D0 83              1501 	pop	dph0
   050A D0 82              1502 	pop	dpl0
   050C D0 F0              1503 	pop	b
   050E D0 E0              1504 	pop	acc
   0510 32                 1505 	reti
                           1506 
   0511                    1507 i2c_isr:
   0511 C0 E0              1508 	push	acc
   0513 C0 F0              1509 	push	b
   0515 C0 82              1510 	push	dpl0
   0517 C0 83              1511 	push	dph0
   0519 C0 D0              1512 	push	psw
   051B 75 D0 00           1513 	mov	psw,#0x00
   051E C0 86              1514 	push	dps
   0520 75 86 00           1515 	mov	dps,#0
                           1516 	;; clear interrupt
   0523 E5 91              1517 	mov	a,exif
   0525 C2 E5              1518 	clr	acc.5
   0527 F5 91              1519 	mov	exif,a
                           1520 	;; handle interrupt
                           1521 	;; epilogue
   0529 D0 86              1522 	pop	dps
   052B D0 D0              1523 	pop	psw
   052D D0 83              1524 	pop	dph0
   052F D0 82              1525 	pop	dpl0
   0531 D0 F0              1526 	pop	b
   0533 D0 E0              1527 	pop	acc
   0535 32                 1528 	reti
                           1529 
   0536                    1530 int4_isr:
   0536 C0 E0              1531 	push	acc
   0538 C0 F0              1532 	push	b
   053A C0 82              1533 	push	dpl0
   053C C0 83              1534 	push	dph0
   053E C0 D0              1535 	push	psw
   0540 75 D0 00           1536 	mov	psw,#0x00
   0543 C0 86              1537 	push	dps
   0545 75 86 00           1538 	mov	dps,#0
                           1539 	;; clear interrupt
   0548 E5 91              1540 	mov	a,exif
   054A C2 E6              1541 	clr	acc.6
   054C F5 91              1542 	mov	exif,a
                           1543 	;; handle interrupt
                           1544 	;; epilogue
   054E D0 86              1545 	pop	dps
   0550 D0 D0              1546 	pop	psw
   0552 D0 83              1547 	pop	dph0
   0554 D0 82              1548 	pop	dpl0
   0556 D0 F0              1549 	pop	b
   0558 D0 E0              1550 	pop	acc
   055A 32                 1551 	reti
                           1552 
   055B                    1553 int5_isr:
   055B C0 E0              1554 	push	acc
   055D C0 F0              1555 	push	b
   055F C0 82              1556 	push	dpl0
   0561 C0 83              1557 	push	dph0
   0563 C0 D0              1558 	push	psw
   0565 75 D0 00           1559 	mov	psw,#0x00
   0568 C0 86              1560 	push	dps
   056A 75 86 00           1561 	mov	dps,#0
                           1562 	;; clear interrupt
   056D E5 91              1563 	mov	a,exif
   056F C2 E7              1564 	clr	acc.7
   0571 F5 91              1565 	mov	exif,a
                           1566 	;; handle interrupt
                           1567 	;; epilogue
   0573 D0 86              1568 	pop	dps
   0575 D0 D0              1569 	pop	psw
   0577 D0 83              1570 	pop	dph0
   0579 D0 82              1571 	pop	dpl0
   057B D0 F0              1572 	pop	b
   057D D0 E0              1573 	pop	acc
   057F 32                 1574 	reti
                           1575 
   0580                    1576 int6_isr:
   0580 C0 E0              1577 	push	acc
   0582 C0 F0              1578 	push	b
   0584 C0 82              1579 	push	dpl0
   0586 C0 83              1580 	push	dph0
   0588 C0 D0              1581 	push	psw
   058A 75 D0 00           1582 	mov	psw,#0x00
   058D C0 86              1583 	push	dps
   058F 75 86 00           1584 	mov	dps,#0
                           1585 	;; clear interrupt
   0592 C2 DB              1586 	clr	eicon+3
                           1587 	;; handle interrupt
                           1588 	;; epilogue
   0594 D0 86              1589 	pop	dps
   0596 D0 D0              1590 	pop	psw
   0598 D0 83              1591 	pop	dph0
   059A D0 82              1592 	pop	dpl0
   059C D0 F0              1593 	pop	b
   059E D0 E0              1594 	pop	acc
   05A0 32                 1595 	reti
                           1596 
   05A1                    1597 usb_sudav_isr:
   05A1 C0 E0              1598 	push	acc
   05A3 C0 F0              1599 	push	b
   05A5 C0 82              1600 	push	dpl0
   05A7 C0 83              1601 	push	dph0
   05A9 C0 84              1602 	push	dpl1
   05AB C0 85              1603 	push	dph1
   05AD C0 D0              1604 	push	psw
   05AF 75 D0 00           1605 	mov	psw,#0x00
   05B2 C0 86              1606 	push	dps
   05B4 75 86 00           1607 	mov	dps,#0
   05B7 C0 00              1608 	push	ar0
   05B9 C0 07              1609 	push	ar7
                           1610 	;; clear interrupt
   05BB E5 91              1611 	mov	a,exif
   05BD C2 E4              1612 	clr	acc.4
   05BF F5 91              1613 	mov	exif,a
   05C1 90 7F AB           1614 	mov	dptr,#USBIRQ
   05C4 74 01              1615 	mov	a,#0x01
   05C6 F0                 1616 	movx	@dptr,a
                           1617 	;; handle interrupt
   05C7 75 40 00           1618 	mov	ctrlcode,#0		; reset control out code
   05CA 90 7F E9           1619 	mov	dptr,#SETUPDAT+1
   05CD E0                 1620 	movx	a,@dptr			; bRequest field
                           1621 	;; standard commands
                           1622 	;; USB_REQ_GET_DESCRIPTOR
   05CE B4 06 59           1623 	cjne	a,#USB_REQ_GET_DESCRIPTOR,cmdnotgetdesc
   05D1 90 7F E8           1624 	mov	dptr,#SETUPDAT		; bRequestType == 0x80
   05D4 E0                 1625 	movx	a,@dptr
   05D5 B4 80 4F           1626 	cjne	a,#USB_DIR_IN,setupstallstd
   05D8 90 7F EB           1627 	mov	dptr,#SETUPDAT+3
   05DB E0                 1628 	movx	a,@dptr
   05DC B4 01 0C           1629 	cjne	a,#USB_DT_DEVICE,cmdnotgetdescdev
   05DF 90 7F D4           1630 	mov	dptr,#SUDPTRH
   05E2 74 0D              1631 	mov	a,#>devicedescr
   05E4 F0                 1632 	movx	@dptr,a
   05E5 A3                 1633 	inc	dptr
   05E6 74 D4              1634 	mov	a,#<devicedescr
   05E8 F0                 1635 	movx	@dptr,a
   05E9 80 39              1636 	sjmp	setupackstd
   05EB                    1637 cmdnotgetdescdev:
   05EB B4 02 12           1638 	cjne	a,#USB_DT_CONFIG,cmdnotgetdesccfg
   05EE 90 7F EA           1639 	mov	dptr,#SETUPDAT+2
   05F1 E0                 1640 	movx	a,@dptr
   05F2 70 33              1641 	jnz	setupstallstd
   05F4 90 7F D4           1642 	mov	dptr,#SUDPTRH
   05F7 74 0D              1643 	mov	a,#>config0descr
   05F9 F0                 1644 	movx	@dptr,a
   05FA A3                 1645 	inc	dptr
   05FB 74 E6              1646 	mov	a,#<config0descr
   05FD F0                 1647 	movx	@dptr,a
   05FE 80 24              1648 	sjmp	setupackstd
   0600                    1649 cmdnotgetdesccfg:
   0600 B4 03 24           1650 	cjne	a,#USB_DT_STRING,setupstallstd
   0603 90 7F EA           1651 	mov	dptr,#SETUPDAT+2
   0606 E0                 1652 	movx	a,@dptr
   0607 24 FC              1653 	add	a,#-numstrings
   0609 40 1C              1654 	jc	setupstallstd
   060B E0                 1655 	movx	a,@dptr
   060C 25 E0              1656 	add	a,acc
   060E 24 2B              1657 	add	a,#<stringdescr
   0610 F5 82              1658 	mov	dpl0,a
   0612 E4                 1659 	clr	a
   0613 34 0E              1660 	addc	a,#>stringdescr
   0615 F5 83              1661 	mov	dph0,a
   0617 E0                 1662 	movx	a,@dptr
   0618 F5 F0              1663 	mov	b,a
   061A A3                 1664 	inc	dptr
   061B E0                 1665 	movx	a,@dptr
   061C 90 7F D4           1666 	mov	dptr,#SUDPTRH
   061F F0                 1667 	movx	@dptr,a
   0620 A3                 1668 	inc	dptr
   0621 E5 F0              1669 	mov	a,b
   0623 F0                 1670 	movx	@dptr,a
                           1671 	; sjmp	setupackstd	
   0624                    1672 setupackstd:
   0624 02 09 B4           1673 	ljmp	setupack
   0627                    1674 setupstallstd:
   0627 02 09 B0           1675 	ljmp	setupstall
   062A                    1676 cmdnotgetdesc:
                           1677 	;; USB_REQ_SET_CONFIGURATION
   062A B4 09 41           1678 	cjne	a,#USB_REQ_SET_CONFIGURATION,cmdnotsetconf
   062D 90 7F E8           1679 	mov	dptr,#SETUPDAT
   0630 E0                 1680 	movx	a,@dptr
   0631 70 F4              1681 	jnz	setupstallstd
   0633 90 7F EA           1682 	mov	dptr,#SETUPDAT+2
   0636 E0                 1683 	movx	a,@dptr
   0637 24 FE              1684 	add	a,#-2
   0639 40 EC              1685 	jc	setupstallstd
   063B E0                 1686 	movx	a,@dptr
   063C F5 6F              1687 	mov	numconfig,a
   063E                    1688 cmdresettoggleshalt:
   063E 90 7F D7           1689 	mov	dptr,#TOGCTL
   0641 78 07              1690 	mov	r0,#7
   0643 E8                 1691 0$:	mov	a,r0
   0644 44 10              1692 	orl	a,#0x10
   0646 F0                 1693 	movx	@dptr,a
   0647 44 30              1694 	orl	a,#0x30
   0649 F0                 1695 	movx	@dptr,a
   064A E8                 1696 	mov	a,r0
   064B F0                 1697 	movx	@dptr,a
   064C 44 20              1698 	orl	a,#0x20
   064E F0                 1699 	movx	@dptr,a
   064F D8 F2              1700 	djnz	r0,0$
   0651 E4                 1701 	clr	a
   0652 F0                 1702 	movx	@dptr,a
   0653 74 02              1703 	mov	a,#2
   0655 90 7F B6           1704 	mov	dptr,#IN1CS
   0658 78 07              1705 	mov	r0,#7
   065A F0                 1706 1$:	movx	@dptr,a
   065B A3                 1707 	inc	dptr
   065C A3                 1708 	inc	dptr
   065D D8 FB              1709 	djnz	r0,1$
   065F 90 7F C6           1710 	mov	dptr,#OUT1CS
   0662 78 07              1711 	mov	r0,#7
   0664 F0                 1712 2$:	movx	@dptr,a
   0665 A3                 1713 	inc	dptr
   0666 A3                 1714 	inc	dptr
   0667 D8 FB              1715 	djnz	r0,2$
   0669 12 0A F1           1716 	lcall	fillusbintr
   066C 80 B6              1717 	sjmp	setupackstd
   066E                    1718 cmdnotsetconf:
                           1719 	;; USB_REQ_SET_INTERFACE
   066E B4 0B 1A           1720 	cjne	a,#USB_REQ_SET_INTERFACE,cmdnotsetint
   0671 90 7F E8           1721 	mov	dptr,#SETUPDAT
   0674 E0                 1722 	movx	a,@dptr
   0675 B4 01 AF           1723 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_OUT,setupstallstd
   0678 E5 6F              1724 	mov	a,numconfig
   067A B4 01 AA           1725 	cjne	a,#1,setupstallstd
   067D 90 7F EC           1726 	mov	dptr,#SETUPDAT+4
   0680 E0                 1727 	movx	a,@dptr
   0681 70 A4              1728 	jnz	setupstallstd
   0683 90 7F EA           1729 	mov	dptr,#SETUPDAT+2
   0686 E0                 1730 	movx	a,@dptr
   0687 F5 70              1731 	mov	altsetting,a
   0689 80 B3              1732 	sjmp	cmdresettoggleshalt
   068B                    1733 cmdnotsetint:
                           1734 	;; USB_REQ_GET_INTERFACE
   068B B4 0A 20           1735 	cjne	a,#USB_REQ_GET_INTERFACE,cmdnotgetint
   068E 90 7F E8           1736 	mov	dptr,#SETUPDAT
   0691 E0                 1737 	movx	a,@dptr
   0692 B4 81 92           1738 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,setupstallstd
   0695 E5 6F              1739 	mov	a,numconfig
   0697 B4 01 8D           1740 	cjne	a,#1,setupstallstd
   069A 90 7F EC           1741 	mov	dptr,#SETUPDAT+4
   069D E0                 1742 	movx	a,@dptr
   069E 70 87              1743 	jnz	setupstallstd
   06A0 E5 70              1744 	mov	a,altsetting
   06A2                    1745 cmdrespondonebyte:
   06A2 90 7F 00           1746 	mov	dptr,#IN0BUF
   06A5 F0                 1747 	movx	@dptr,a
   06A6 90 7F B5           1748 	mov	dptr,#IN0BC
   06A9 74 01              1749 	mov	a,#1
   06AB F0                 1750 	movx	@dptr,a	
   06AC 80 4E              1751 	sjmp	setupackstd2
   06AE                    1752 cmdnotgetint:
                           1753 	;; USB_REQ_GET_CONFIGURATION
   06AE B4 08 0B           1754 	cjne	a,#USB_REQ_GET_CONFIGURATION,cmdnotgetconf
   06B1 90 7F E8           1755 	mov	dptr,#SETUPDAT
   06B4 E0                 1756 	movx	a,@dptr
   06B5 B4 80 47           1757 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,setupstallstd2
   06B8 E5 6F              1758 	mov	a,numconfig
   06BA 80 E6              1759 	sjmp	cmdrespondonebyte	
   06BC                    1760 cmdnotgetconf:
                           1761 	;; USB_REQ_GET_STATUS (0)
   06BC 70 44              1762 	jnz	cmdnotgetstat
   06BE 90 7F E8           1763 	mov	dptr,#SETUPDAT
   06C1 E0                 1764 	movx	a,@dptr
   06C2 B4 80 11           1765 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,cmdnotgetstatdev
   06C5 74 01              1766 	mov	a,#1
   06C7                    1767 cmdrespondstat:
   06C7 90 7F 00           1768 	mov	dptr,#IN0BUF
   06CA F0                 1769 	movx	@dptr,a
   06CB A3                 1770 	inc	dptr
   06CC E4                 1771 	clr	a
   06CD F0                 1772 	movx	@dptr,a
   06CE 90 7F B5           1773 	mov	dptr,#IN0BC
   06D1 74 02              1774 	mov	a,#2
   06D3 F0                 1775 	movx	@dptr,a	
   06D4 80 26              1776 	sjmp	setupackstd2
   06D6                    1777 cmdnotgetstatdev:
   06D6 B4 81 03           1778 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,cmdnotgetstatintf
   06D9 E4                 1779 	clr	a
   06DA 80 EB              1780 	sjmp	cmdrespondstat
   06DC                    1781 cmdnotgetstatintf:
   06DC B4 82 20           1782 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_IN,setupstallstd2
   06DF 90 7F EC           1783 	mov	dptr,#SETUPDAT+4
   06E2 E0                 1784 	movx	a,@dptr
   06E3 90 7F C4           1785 	mov	dptr,#OUT1CS-2
   06E6 30 E7 03           1786 	jnb	acc.7,0$
   06E9 90 7F B4           1787 	mov	dptr,#IN1CS-2
   06EC 54 0F              1788 0$:	anl	a,#15
   06EE 60 0F              1789 	jz	setupstallstd2
   06F0 20 E3 0C           1790 	jb	acc.3,setupstallstd2
   06F3 25 E0              1791 	add	a,acc
   06F5 25 82              1792 	add	a,dpl0
   06F7 F5 82              1793 	mov	dpl0,a
   06F9 E0                 1794 	movx	a,@dptr
   06FA 80 CB              1795 	sjmp	cmdrespondstat
   06FC                    1796 setupackstd2:
   06FC 02 09 B4           1797 	ljmp	setupack
   06FF                    1798 setupstallstd2:
   06FF 02 09 B0           1799 	ljmp	setupstall
   0702                    1800 cmdnotgetstat:
                           1801 	;; USB_REQ_SET_FEATURE
   0702 B4 03 05           1802 	cjne	a,#USB_REQ_SET_FEATURE,cmdnotsetftr
   0705 75 F0 01           1803 	mov	b,#1
   0708 80 06              1804 	sjmp	handleftr
   070A                    1805 cmdnotsetftr:
                           1806 	;; USB_REQ_CLEAR_FEATURE
   070A B4 01 44           1807 	cjne	a,#USB_REQ_CLEAR_FEATURE,cmdnotclrftr
   070D 75 F0 00           1808 	mov	b,#0
   0710                    1809 handleftr:
   0710 90 7F E8           1810 	mov	dptr,#SETUPDAT
   0713 E0                 1811 	movx	a,@dptr
   0714 B4 02 E8           1812 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_OUT,setupstallstd2
   0717 A3                 1813 	inc	dptr
   0718 A3                 1814 	inc	dptr
   0719 E0                 1815 	movx	a,@dptr
   071A 70 E3              1816 	jnz	setupstallstd2	; not ENDPOINT_HALT feature
   071C A3                 1817 	inc	dptr
   071D E0                 1818 	movx	a,@dptr
   071E 70 DF              1819 	jnz	setupstallstd2
   0720 A3                 1820 	inc	dptr
   0721 E0                 1821 	movx	a,@dptr
   0722 90 7F C4           1822 	mov	dptr,#OUT1CS-2
   0725 30 E7 05           1823 	jnb	acc.7,0$
   0728 90 7F B4           1824 	mov	dptr,#IN1CS-2
   072B 44 10              1825 	orl	a,#0x10
   072D 20 E3 CF           1826 0$:	jb	acc.3,setupstallstd2
                           1827 	;; clear data toggle
   0730 54 1F              1828 	anl	a,#0x1f
   0732 05 86              1829 	inc	dps
   0734 90 7F D7           1830 	mov	dptr,#TOGCTL
   0737 F0                 1831 	movx	@dptr,a
   0738 44 20              1832 	orl	a,#0x20
   073A F0                 1833 	movx	@dptr,a
   073B 54 0F              1834 	anl	a,#15
   073D F0                 1835 	movx	@dptr,a
   073E 15 86              1836 	dec	dps	
                           1837 	;; clear/set ep halt feature
   0740 25 E0              1838 	add	a,acc
   0742 25 82              1839 	add	a,dpl0
   0744 F5 82              1840 	mov	dpl0,a
   0746 E5 F0              1841 	mov	a,b
   0748 F0                 1842 	movx	@dptr,a
   0749 80 B1              1843 	sjmp	setupackstd2
                           1844 
   074B                    1845 cmdnotc0_1:
   074B 02 08 19           1846 	ljmp	cmdnotc0
   074E                    1847 setupstallc0_1:
   074E 02 09 B0           1848 	ljmp	setupstall
                           1849 	
   0751                    1850 cmdnotclrftr:
                           1851 	;; vendor specific commands
                           1852 	;; 0xc0
   0751 B4 C0 F7           1853 	cjne	a,#0xc0,cmdnotc0_1
   0754 90 7F E8           1854 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   0757 E0                 1855 	movx	a,@dptr
   0758 B4 C0 F3           1856 	cjne	a,#0xc0,setupstallc0_1
                           1857 	;; fill status buffer
   075B E5 45              1858 	mov	a,txstate
   075D F5 F0              1859 	mov	b,a
   075F D2 F2              1860 	setb	b.2
   0761 45 50              1861 	orl	a,pttforce
   0763 B4 00 02           1862 	cjne	a,#0,0$
   0766 C2 F2              1863 	clr	b.2
   0768 90 C0 0E           1864 0$:	mov	dptr,#FSKMDISCIN
   076B E0                 1865 	movx	a,@dptr
   076C A2 E6              1866 	mov	c,acc.6
   076E B3                 1867 	cpl	c
   076F 82 08              1868 	anl	c,extmodem
   0771 92 F3              1869 	mov	b.3,c
   0773 90 C0 09           1870 	mov	dptr,#FSKSTAT
   0776 E0                 1871 	movx	a,@dptr
   0777 A2 E0              1872 	mov	c,acc.0
   0779 B0 08              1873 	anl	c,/extmodem
   077B 72 F3              1874 	orl	c,b.3
   077D 92 F3              1875 	mov	b.3,c
   077F A2 0B              1876 	mov	c,uartempty
   0781 92 F5              1877 	mov	b.5,c
   0783 E4                 1878 	clr	a
   0784 90 7F 04           1879 	mov	dptr,#(IN0BUF+4)
   0787 F0                 1880 	movx	@dptr,a
                           1881 	;; bytewide elements
   0788 90 7F 00           1882 	mov	dptr,#(IN0BUF)
   078B E5 F0              1883 	mov	a,b
   078D F0                 1884 	movx	@dptr,a
   078E E5 48              1885 	mov	a,txwr
   0790 F4                 1886 	cpl	a
   0791 25 49              1887 	add	a,txrd
   0793 54 0F              1888 	anl	a,#(TXCHUNKS-1)
   0795 90 7F 01           1889 	mov	dptr,#(IN0BUF+1)
   0798 F0                 1890 	movx	@dptr,a
   0799 E5 53              1891 	mov	a,rxrd
   079B F4                 1892 	cpl	a
   079C 25 52              1893 	add	a,rxwr
   079E 04                 1894 	inc	a
   079F 54 1F              1895 	anl	a,#(RXCHUNKS-1)
   07A1 90 7F 02           1896 	mov	dptr,#(IN0BUF+2)
   07A4 F0                 1897 	movx	@dptr,a
   07A5 90 C0 04           1898 	mov	dptr,#FSKRSSI
   07A8 E0                 1899 	movx	a,@dptr
   07A9 90 7F 03           1900 	mov	dptr,#(IN0BUF+3)
   07AC F0                 1901 	movx	@dptr,a
                           1902 	;; counter
   07AD 05 44              1903 	inc	irqcount
   07AF E5 44              1904 	mov	a,irqcount
   07B1 90 7F 05           1905 	mov	dptr,#(IN0BUF+5)
   07B4 F0                 1906 	movx	@dptr,a
                           1907 	;; additional fields (HDLC state mach)
   07B5 E5 45              1908 	mov	a,txstate
   07B7 A3                 1909 	inc	dptr
   07B8 F0                 1910 	movx	@dptr,a
   07B9 E5 49              1911 	mov	a,txrd
   07BB A3                 1912 	inc	dptr
   07BC F0                 1913 	movx	@dptr,a
   07BD E5 48              1914 	mov	a,txwr
   07BF A3                 1915 	inc	dptr
   07C0 F0                 1916 	movx	@dptr,a
   07C1 E5 4A              1917 	mov	a,txtwr
   07C3 A3                 1918 	inc	dptr
   07C4 F0                 1919 	movx	@dptr,a
   07C5 E5 4B              1920 	mov	a,txcnt
   07C7 A3                 1921 	inc	dptr
   07C8 F0                 1922 	movx	@dptr,a
   07C9 E5 46              1923 	mov	a,flagcnt
   07CB A3                 1924 	inc	dptr
   07CC F0                 1925 	movx	@dptr,a
   07CD E5 47              1926 	mov	a,flagcnt+1
   07CF A3                 1927 	inc	dptr
   07D0 F0                 1928 	movx	@dptr,a
   07D1 E5 51              1929 	mov	a,rxstate
   07D3 A3                 1930 	inc	dptr
   07D4 F0                 1931 	movx	@dptr,a
   07D5 E5 53              1932 	mov	a,rxrd
   07D7 A3                 1933 	inc	dptr
   07D8 F0                 1934 	movx	@dptr,a
   07D9 E5 52              1935 	mov	a,rxwr
   07DB A3                 1936 	inc	dptr
   07DC F0                 1937 	movx	@dptr,a
   07DD E5 54              1938 	mov	a,rxtwr
   07DF A3                 1939 	inc	dptr
   07E0 F0                 1940 	movx	@dptr,a
   07E1 E5 55              1941 	mov	a,rxcnt
   07E3 A3                 1942 	inc	dptr
   07E4 F0                 1943 	movx	@dptr,a
                           1944 	;; FPGA registers
   07E5 E5 59              1945 	mov	a,tmprxcnt
   07E7 90 7F 12           1946 	mov	dptr,#(IN0BUF+18)
   07EA F0                 1947 	movx	@dptr,a
   07EB 90 C0 00           1948 	mov	dptr,#FSKTXCNT
   07EE E0                 1949 	movx	a,@dptr
   07EF 90 7F 13           1950 	mov	dptr,#(IN0BUF+19)
   07F2 F0                 1951 	movx	@dptr,a
   07F3 A3                 1952 	inc	dptr
   07F4 E5 20              1953 	mov	a,ctrlreg
   07F6 F0                 1954 	movx	@dptr,a
   07F7 90 C0 09           1955 	mov	dptr,#FSKSTAT
   07FA E0                 1956 	movx	a,@dptr
   07FB 90 7F 15           1957 	mov	dptr,#(IN0BUF+21)
   07FE F0                 1958 	movx	@dptr,a
                           1959 	;; Anchor Registers
   07FF 90 7F C8           1960 	mov	dptr,#OUT2CS
   0802 E0                 1961 	movx	a,@dptr
   0803 90 7F 16           1962 	mov	dptr,#(IN0BUF+22)
   0806 F0                 1963 	movx	@dptr,a
                           1964 	;; set length
   0807 90 7F EE           1965 	mov	dptr,#SETUPDAT+6	; wLength
   080A E0                 1966 	movx	a,@dptr
   080B 24 E9              1967 	add	a,#-(6+12+4+1)
   080D 50 01              1968 	jnc	4$
   080F E4                 1969 	clr	a
   0810 24 17              1970 4$:	add	a,#(6+12+4+1)
   0812 90 7F B5           1971 	mov	dptr,#IN0BC
   0815 F0                 1972 	movx	@dptr,a
   0816 02 09 B4           1973 	ljmp	setupack
   0819                    1974 cmdnotc0:
                           1975 	;; 0xc8
   0819 B4 C8 1D           1976 	cjne	a,#0xc8,cmdnotc8
   081C 90 7F E8           1977 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   081F E0                 1978 	movx	a,@dptr
   0820 B4 C0 13           1979 	cjne	a,#0xc0,setupstallc8
   0823 74 01              1980 	mov	a,#1
   0825 30 08 01           1981 	jnb	extmodem,1$
   0828 04                 1982 	inc	a
   0829 90 7F 00           1983 1$:	mov	dptr,#IN0BUF
   082C F0                 1984 	movx	@dptr,a
   082D 90 7F B5           1985 	mov	dptr,#IN0BC
   0830 74 01              1986 	mov	a,#1
   0832 F0                 1987 	movx	@dptr,a
   0833 02 09 B4           1988 	ljmp	setupack
   0836                    1989 setupstallc8:
   0836 02 09 B0           1990 	ljmp	setupstall
   0839                    1991 cmdnotc8:
                           1992 	;; 0xc9
   0839 B4 C9 21           1993 	cjne	a,#0xc9,cmdnotc9
   083C 90 7F E8           1994 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   083F E0                 1995 	movx	a,@dptr
   0840 B4 C0 17           1996 	cjne	a,#0xc0,setupstallc9
   0843 90 7F 00           1997 	mov	dptr,#IN0BUF
   0846 78 F0              1998 	mov	r0,#parserial
   0848 E2                 1999 0$:	movx	a,@r0
   0849 60 05              2000 	jz	1$
   084B F0                 2001 	movx	@dptr,a
   084C 08                 2002 	inc	r0
   084D A3                 2003 	inc	dptr
   084E 80 F8              2004 	sjmp	0$
   0850 E8                 2005 1$:	mov	a,r0
   0851 24 10              2006 	add	a,#-0xf0	; -parserial
   0853 90 7F B5           2007 	mov	dptr,#IN0BC
   0856 F0                 2008 	movx	@dptr,a
   0857 02 09 B4           2009 	ljmp	setupack
   085A                    2010 setupstallc9:
   085A 02 09 B0           2011 	ljmp	setupstall
   085D                    2012 cmdnotc9:
                           2013 	;; 0xd0
   085D B4 D0 5D           2014 	cjne	a,#0xd0,cmdnotd0
   0860 90 7F E8           2015 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0863 E0                 2016 	movx	a,@dptr
   0864 B4 C0 53           2017 	cjne	a,#0xc0,setupstalld0
   0867 90 7F EC           2018 	mov	dptr,#SETUPDAT+4	; wIndex
   086A E0                 2019 	movx	a,@dptr
   086B B4 01 0A           2020 	cjne	a,#1,0$
   086E 90 7F EA           2021 	mov	dptr,#SETUPDAT+2	; wValue
   0871 E0                 2022 	movx	a,@dptr
   0872 54 01              2023 	anl	a,#1
   0874 F5 50              2024 	mov	pttforce,a
   0876 D2 0A              2025 	setb	pttforcechg
   0878                    2026 0$:	;; PTT status
   0878 90 7F 00           2027 	mov	dptr,#IN0BUF
   087B E5 45              2028 	mov	a,txstate
   087D 45 50              2029 	orl	a,pttforce
   087F 60 02              2030 	jz	1$
   0881 74 01              2031 	mov	a,#1
   0883 F0                 2032 1$:	movx	@dptr,a
                           2033 	;; DCD status
   0884 90 C0 0E           2034 	mov	dptr,#FSKMDISCIN
   0887 E0                 2035 	movx	a,@dptr
   0888 A2 E6              2036 	mov	c,acc.6
   088A A0 08              2037 	orl	c,/extmodem
   088C 92 F0              2038 	mov	b.0,c
   088E 90 C0 09           2039 	mov	dptr,#FSKSTAT
   0891 E0                 2040 	movx	a,@dptr
   0892 A2 E0              2041 	mov	c,acc.0
   0894 72 08              2042 	orl	c,extmodem
   0896 82 F0              2043 	anl	c,b.0
   0898 B3                 2044 	cpl	c
   0899 E4                 2045 	clr	a
   089A 92 E0              2046 	mov	acc.0,c
   089C 90 7F 01           2047 	mov	dptr,#IN0BUF+1
   089F F0                 2048 	movx	@dptr,a
                           2049 	;; RSSI
   08A0 90 C0 04           2050 	mov	dptr,#FSKRSSI
   08A3 E0                 2051 	movx	a,@dptr
   08A4 90 7F 02           2052 	mov	dptr,#IN0BUF+2
   08A7 F0                 2053 	movx	@dptr,a
                           2054 	;; length
   08A8 90 7F EE           2055 	mov	dptr,#SETUPDAT+6	; wLength
   08AB E0                 2056 	movx	a,@dptr
   08AC 24 FD              2057 	add	a,#-3
   08AE 50 01              2058 	jnc	2$
   08B0 E4                 2059 	clr	a
   08B1 24 03              2060 2$:	add	a,#3
   08B3 90 7F B5           2061 	mov	dptr,#IN0BC
   08B6 F0                 2062 	movx	@dptr,a
   08B7 02 09 B4           2063 	ljmp	setupack
   08BA                    2064 setupstalld0:
   08BA 02 09 B0           2065 	ljmp	setupstall
   08BD                    2066 cmdnotd0:
                           2067 	;; 0xd1
   08BD B4 D1 29           2068 	cjne	a,#0xd1,cmdnotd1
   08C0 90 7F E8           2069 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   08C3 E0                 2070 	movx	a,@dptr
   08C4 B4 C0 1F           2071 	cjne	a,#0xc0,setupstalld1
   08C7 90 7F 00           2072 	mov	dptr,#IN0BUF
   08CA 78 E0              2073 	mov	r0,#parbitratetx
   08CC 7F 06              2074 	mov	r7,#6
   08CE E2                 2075 1$:	movx	a,@r0
   08CF F0                 2076 	movx	@dptr,a
   08D0 A3                 2077 	inc	dptr
   08D1 08                 2078 	inc	r0
   08D2 DF FA              2079 	djnz	r7,1$
                           2080 	;; length
   08D4 90 7F EE           2081 	mov	dptr,#SETUPDAT+6	; wLength
   08D7 E0                 2082 	movx	a,@dptr
   08D8 24 FA              2083 	add	a,#-6
   08DA 50 01              2084 	jnc	2$
   08DC E4                 2085 	clr	a
   08DD 24 06              2086 2$:	add	a,#6
   08DF 90 7F B5           2087 	mov	dptr,#IN0BC
   08E2 F0                 2088 	movx	@dptr,a
   08E3 02 09 B4           2089 	ljmp	setupack
   08E6                    2090 setupstalld1:
   08E6 02 09 B0           2091 	ljmp	setupstall
   08E9                    2092 cmdnotd1:
                           2093 	;; 0xd2
   08E9 B4 D2 20           2094 	cjne	a,#0xd2,cmdnotd2
   08EC 90 7F E8           2095 	mov	dptr,#SETUPDAT	; bRequestType == 0x40
   08EF E0                 2096 	movx	a,@dptr
   08F0 B4 40 16           2097 	cjne	a,#0x40,setupstalld2
   08F3 90 7F EA           2098 	mov	dptr,#SETUPDAT+2	; wValue
   08F6 E0                 2099 	movx	a,@dptr
   08F7 F5 F0              2100 	mov	b,a
   08F9 90 7F 98           2101 	mov	dptr,#OUTC
   08FC E0                 2102 	movx	a,@dptr
   08FD A2 F0              2103 	mov	c,b.0
   08FF 92 E3              2104 	mov	acc.3,c
   0901 A2 F1              2105 	mov	c,b.1
   0903 92 E5              2106 	mov	acc.5,c
   0905 F0                 2107 	movx	@dptr,a
   0906 02 09 B4           2108 	ljmp	setupack
   0909                    2109 setupstalld2:
   0909 02 09 B0           2110 	ljmp	setupstall
   090C                    2111 cmdnotd2:
                           2112 	;; 0xd3
   090C B4 D3 16           2113 	cjne	a,#0xd3,cmdnotd3
   090F 90 7F E8           2114 	mov	dptr,#SETUPDAT	; bRequestType == 0x40
   0912 E0                 2115 	movx	a,@dptr
   0913 B4 40 07           2116 	cjne	a,#0x40,setupstalld3
   0916 90 7F EA           2117 	mov	dptr,#SETUPDAT+2	; wValue
   0919 E0                 2118 	movx	a,@dptr
   091A 10 0B 03           2119 	jbc	uartempty,cmdd2cont
   091D                    2120 setupstalld3:
   091D 02 09 B0           2121 	ljmp	setupstall
   0920                    2122 cmdd2cont:
   0920 F5 99              2123 	mov	sbuf0,a
   0922 02 09 B4           2124 	ljmp	setupack
   0925                    2125 cmdnotd3:
                           2126 	;; 0xd4
   0925 B4 D4 4C           2127 	cjne	a,#0xd4,cmdnotd4
   0928 90 7F E8           2128 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   092B E0                 2129 	movx	a,@dptr
   092C B4 C0 42           2130 	cjne	a,#0xc0,setupstalld4
   092F 20 08 3F           2131 	jb	extmodem,setupstalld4
   0932 90 7F EC           2132 	mov	dptr,#SETUPDAT+4	; wIndex
   0935 E0                 2133 	movx	a,@dptr
   0936 90 7F EA           2134 	mov	dptr,#SETUPDAT+2	; wValue
   0939 B4 01 09           2135 	cjne	a,#1,0$
   093C E0                 2136 	movx	a,@dptr
   093D 90 C0 0D           2137 	mov	dptr,#FSKMDISCOUT
   0940 F0                 2138 	movx	@dptr,a
   0941 F5 6E              2139 	mov	mdiscout,a
   0943 80 0A              2140 	sjmp	1$
   0945 B4 02 07           2141 0$:	cjne	a,#2,1$
   0948 E0                 2142 	movx	a,@dptr
   0949 90 C0 0C           2143 	mov	dptr,#FSKMDISCTRIS
   094C F0                 2144 	movx	@dptr,a
   094D F5 6D              2145 	mov	mdisctris,a
   094F 90 C0 0E           2146 1$:	mov	dptr,#FSKMDISCIN
   0952 E0                 2147 	movx	a,@dptr
   0953 90 7F 00           2148 	mov	dptr,#IN0BUF+0
   0956 F0                 2149 	movx	@dptr,a
   0957 E5 6E              2150 	mov	a,mdiscout
   0959 A3                 2151 	inc	dptr
   095A F0                 2152 	movx	@dptr,a
   095B E5 6D              2153 	mov	a,mdisctris
   095D A3                 2154 	inc	dptr
   095E F0                 2155 	movx	@dptr,a
                           2156 	;; length
   095F 90 7F EE           2157 	mov	dptr,#SETUPDAT+6	; wLength
   0962 E0                 2158 	movx	a,@dptr
   0963 24 FD              2159 	add	a,#-3
   0965 50 01              2160 	jnc	2$
   0967 E4                 2161 	clr	a
   0968 24 03              2162 2$:	add	a,#3
   096A 90 7F B5           2163 	mov	dptr,#IN0BC
   096D F0                 2164 	movx	@dptr,a
   096E 02 09 B4           2165 	ljmp	setupack
   0971                    2166 setupstalld4:
   0971 02 09 B0           2167 	ljmp	setupstall
   0974                    2168 cmdnotd4:
                           2169 	;; 0xd5
   0974 B4 D5 39           2170 	cjne	a,#0xd5,cmdnotd5
   0977 90 7F E8           2171 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   097A E0                 2172 	movx	a,@dptr
   097B B4 C0 2F           2173 	cjne	a,#0xc0,setupstalld5
   097E 90 7F EC           2174 	mov	dptr,#SETUPDAT+4	; wIndex
   0981 E0                 2175 	movx	a,@dptr
   0982 B4 01 0A           2176 	cjne	a,#1,0$
   0985 90 7F EA           2177 	mov	dptr,#SETUPDAT+2	; wValue
   0988 E0                 2178 	movx	a,@dptr
   0989 90 C0 0A           2179 	mov	dptr,#FSKT7FOUT
   098C F0                 2180 	movx	@dptr,a
   098D F5 6C              2181 	mov	t7fout,a
   098F 90 C0 0B           2182 0$:	mov	dptr,#FSKT7FIN
   0992 E0                 2183 	movx	a,@dptr
   0993 90 7F 00           2184 	mov	dptr,#IN0BUF+0
   0996 F0                 2185 	movx	@dptr,a
   0997 E5 6C              2186 	mov	a,t7fout
   0999 A3                 2187 	inc	dptr
   099A F0                 2188 	movx	@dptr,a
                           2189 	;; length
   099B 90 7F EE           2190 	mov	dptr,#SETUPDAT+6	; wLength
   099E E0                 2191 	movx	a,@dptr
   099F 24 FE              2192 	add	a,#-2
   09A1 50 01              2193 	jnc	2$
   09A3 E4                 2194 	clr	a
   09A4 24 02              2195 2$:	add	a,#2
   09A6 90 7F B5           2196 	mov	dptr,#IN0BC
   09A9 F0                 2197 	movx	@dptr,a
   09AA 02 09 B4           2198 	ljmp	setupack
   09AD                    2199 setupstalld5:
   09AD 02 09 B0           2200 	ljmp	setupstall
   09B0                    2201 cmdnotd5:
                           2202 	;; unknown commands fall through to setupstall
                           2203 
   09B0                    2204 setupstall:
   09B0 74 03              2205 	mov	a,#3
   09B2 80 02              2206 	sjmp	endsetup
   09B4                    2207 setupack:
   09B4 74 02              2208 	mov	a,#2
   09B6                    2209 endsetup:
   09B6 90 7F B4           2210 	mov	dptr,#EP0CS
   09B9 F0                 2211 	movx	@dptr,a
   09BA                    2212 endusbisr:
                           2213 	;; epilogue
   09BA D0 07              2214 	pop	ar7
   09BC D0 00              2215 	pop	ar0
   09BE D0 86              2216 	pop	dps
   09C0 D0 D0              2217 	pop	psw
   09C2 D0 85              2218 	pop	dph1
   09C4 D0 84              2219 	pop	dpl1
   09C6 D0 83              2220 	pop	dph0
   09C8 D0 82              2221 	pop	dpl0
   09CA D0 F0              2222 	pop	b
   09CC D0 E0              2223 	pop	acc
   09CE 32                 2224 	reti
                           2225 
   09CF                    2226 usb_sof_isr:
   09CF C0 E0              2227 	push	acc
   09D1 C0 F0              2228 	push	b
   09D3 C0 82              2229 	push	dpl0
   09D5 C0 83              2230 	push	dph0
   09D7 C0 D0              2231 	push	psw
   09D9 75 D0 00           2232 	mov	psw,#0x00
   09DC C0 86              2233 	push	dps
   09DE 75 86 00           2234 	mov	dps,#0
                           2235 	;; clear interrupt
   09E1 E5 91              2236 	mov	a,exif
   09E3 C2 E4              2237 	clr	acc.4
   09E5 F5 91              2238 	mov	exif,a
   09E7 90 7F AB           2239 	mov	dptr,#USBIRQ
   09EA 74 02              2240 	mov	a,#0x02
   09EC F0                 2241 	movx	@dptr,a
                           2242 	;; handle interrupt
                           2243 	;; epilogue
   09ED D0 86              2244 	pop	dps
   09EF D0 D0              2245 	pop	psw
   09F1 D0 83              2246 	pop	dph0
   09F3 D0 82              2247 	pop	dpl0
   09F5 D0 F0              2248 	pop	b
   09F7 D0 E0              2249 	pop	acc
   09F9 32                 2250 	reti
                           2251 
                           2252 
   09FA                    2253 usb_sutok_isr:
   09FA C0 E0              2254 	push	acc
   09FC C0 F0              2255 	push	b
   09FE C0 82              2256 	push	dpl0
   0A00 C0 83              2257 	push	dph0
   0A02 C0 D0              2258 	push	psw
   0A04 75 D0 00           2259 	mov	psw,#0x00
   0A07 C0 86              2260 	push	dps
   0A09 75 86 00           2261 	mov	dps,#0
                           2262 	;; clear interrupt
   0A0C E5 91              2263 	mov	a,exif
   0A0E C2 E4              2264 	clr	acc.4
   0A10 F5 91              2265 	mov	exif,a
   0A12 90 7F AB           2266 	mov	dptr,#USBIRQ
   0A15 74 04              2267 	mov	a,#0x04
   0A17 F0                 2268 	movx	@dptr,a
                           2269 	;; handle interrupt
                           2270 	;; epilogue
   0A18 D0 86              2271 	pop	dps
   0A1A D0 D0              2272 	pop	psw
   0A1C D0 83              2273 	pop	dph0
   0A1E D0 82              2274 	pop	dpl0
   0A20 D0 F0              2275 	pop	b
   0A22 D0 E0              2276 	pop	acc
   0A24 32                 2277 	reti
                           2278 
   0A25                    2279 usb_suspend_isr:
   0A25 C0 E0              2280 	push	acc
   0A27 C0 F0              2281 	push	b
   0A29 C0 82              2282 	push	dpl0
   0A2B C0 83              2283 	push	dph0
   0A2D C0 D0              2284 	push	psw
   0A2F 75 D0 00           2285 	mov	psw,#0x00
   0A32 C0 86              2286 	push	dps
   0A34 75 86 00           2287 	mov	dps,#0
                           2288 	;; clear interrupt
   0A37 E5 91              2289 	mov	a,exif
   0A39 C2 E4              2290 	clr	acc.4
   0A3B F5 91              2291 	mov	exif,a
   0A3D 90 7F AB           2292 	mov	dptr,#USBIRQ
   0A40 74 08              2293 	mov	a,#0x08
   0A42 F0                 2294 	movx	@dptr,a
                           2295 	;; handle interrupt
                           2296 	;; epilogue
   0A43 D0 86              2297 	pop	dps
   0A45 D0 D0              2298 	pop	psw
   0A47 D0 83              2299 	pop	dph0
   0A49 D0 82              2300 	pop	dpl0
   0A4B D0 F0              2301 	pop	b
   0A4D D0 E0              2302 	pop	acc
   0A4F 32                 2303 	reti
                           2304 
   0A50                    2305 usb_usbreset_isr:
   0A50 C0 E0              2306 	push	acc
   0A52 C0 F0              2307 	push	b
   0A54 C0 82              2308 	push	dpl0
   0A56 C0 83              2309 	push	dph0
   0A58 C0 D0              2310 	push	psw
   0A5A 75 D0 00           2311 	mov	psw,#0x00
   0A5D C0 86              2312 	push	dps
   0A5F 75 86 00           2313 	mov	dps,#0
                           2314 	;; clear interrupt
   0A62 E5 91              2315 	mov	a,exif
   0A64 C2 E4              2316 	clr	acc.4
   0A66 F5 91              2317 	mov	exif,a
   0A68 90 7F AB           2318 	mov	dptr,#USBIRQ
   0A6B 74 10              2319 	mov	a,#0x10
   0A6D F0                 2320 	movx	@dptr,a
                           2321 	;; handle interrupt
                           2322 	;; epilogue
   0A6E D0 86              2323 	pop	dps
   0A70 D0 D0              2324 	pop	psw
   0A72 D0 83              2325 	pop	dph0
   0A74 D0 82              2326 	pop	dpl0
   0A76 D0 F0              2327 	pop	b
   0A78 D0 E0              2328 	pop	acc
   0A7A 32                 2329 	reti
                           2330 
   0A7B                    2331 usb_ep0in_isr:
   0A7B C0 E0              2332 	push	acc
   0A7D C0 F0              2333 	push	b
   0A7F C0 82              2334 	push	dpl0
   0A81 C0 83              2335 	push	dph0
   0A83 C0 84              2336 	push	dpl1
   0A85 C0 85              2337 	push	dph1
   0A87 C0 D0              2338 	push	psw
   0A89 75 D0 00           2339 	mov	psw,#0x00
   0A8C C0 86              2340 	push	dps
   0A8E 75 86 00           2341 	mov	dps,#0
   0A91 C0 00              2342 	push	ar0
   0A93 C0 07              2343 	push	ar7
                           2344 	;; clear interrupt
   0A95 E5 91              2345 	mov	a,exif
   0A97 C2 E4              2346 	clr	acc.4
   0A99 F5 91              2347 	mov	exif,a
   0A9B 90 7F A9           2348 	mov	dptr,#IN07IRQ
   0A9E 74 01              2349 	mov	a,#0x01
   0AA0 F0                 2350 	movx	@dptr,a
                           2351 	;; handle interrupt
                           2352 
                           2353 ;ep0install:
                           2354 ;	mov	a,#3
                           2355 ;	sjmp	ep0incs
                           2356 ;ep0inack:
                           2357 ;	mov	a,#2
                           2358 ;ep0incs:
                           2359 ;	mov	dptr,#EP0CS
                           2360 ;	movx	@dptr,a
                           2361 ;ep0inendisr:
                           2362 	;; epilogue
   0AA1 D0 07              2363 	pop	ar7
   0AA3 D0 00              2364 	pop	ar0
   0AA5 D0 86              2365 	pop	dps
   0AA7 D0 D0              2366 	pop	psw
   0AA9 D0 85              2367 	pop	dph1
   0AAB D0 84              2368 	pop	dpl1
   0AAD D0 83              2369 	pop	dph0
   0AAF D0 82              2370 	pop	dpl0
   0AB1 D0 F0              2371 	pop	b
   0AB3 D0 E0              2372 	pop	acc
   0AB5 32                 2373 	reti
                           2374 
   0AB6                    2375 usb_ep0out_isr:
   0AB6 C0 E0              2376 	push	acc
   0AB8 C0 F0              2377 	push	b
   0ABA C0 82              2378 	push	dpl0
   0ABC C0 83              2379 	push	dph0
   0ABE C0 84              2380 	push	dpl1
   0AC0 C0 85              2381 	push	dph1
   0AC2 C0 D0              2382 	push	psw
   0AC4 75 D0 00           2383 	mov	psw,#0x00
   0AC7 C0 86              2384 	push	dps
   0AC9 75 86 00           2385 	mov	dps,#0
   0ACC C0 00              2386 	push	ar0
   0ACE C0 06              2387 	push	ar6
                           2388 	;; clear interrupt
   0AD0 E5 91              2389 	mov	a,exif
   0AD2 C2 E4              2390 	clr	acc.4
   0AD4 F5 91              2391 	mov	exif,a
   0AD6 90 7F AA           2392 	mov	dptr,#OUT07IRQ
   0AD9 74 01              2393 	mov	a,#0x01
   0ADB F0                 2394 	movx	@dptr,a
                           2395 	;; handle interrupt
                           2396 
                           2397 ;ep0outstall:
                           2398 ;	mov	ctrlcode,#0
                           2399 ;	mov	a,#3
                           2400 ;	sjmp	ep0outcs
                           2401 ;ep0outack:
                           2402 ;	mov	txwr,txtwr
                           2403 ;	mov	ctrlcode,#0
                           2404 ;	mov	a,#2
                           2405 ;ep0outcs:
                           2406 ;	mov	dptr,#EP0CS
                           2407 ;	movx	@dptr,a
                           2408 ;ep0outendisr:
                           2409 	;; epilogue
   0ADC D0 06              2410 	pop	ar6
   0ADE D0 00              2411 	pop	ar0
   0AE0 D0 86              2412 	pop	dps
   0AE2 D0 D0              2413 	pop	psw
   0AE4 D0 85              2414 	pop	dph1
   0AE6 D0 84              2415 	pop	dpl1
   0AE8 D0 83              2416 	pop	dph0
   0AEA D0 82              2417 	pop	dpl0
   0AEC D0 F0              2418 	pop	b
   0AEE D0 E0              2419 	pop	acc
   0AF0 32                 2420 	reti
                           2421 
   0AF1                    2422 fillusbintr::
   0AF1 E5 45              2423 	mov	a,txstate
   0AF3 F5 F0              2424 	mov	b,a
   0AF5 D2 F2              2425 	setb	b.2
   0AF7 45 50              2426 	orl	a,pttforce
   0AF9 B4 00 02           2427 	cjne	a,#0,0$
   0AFC C2 F2              2428 	clr	b.2
   0AFE 90 C0 0E           2429 0$:	mov	dptr,#FSKMDISCIN
   0B01 E0                 2430 	movx	a,@dptr
   0B02 A2 E6              2431 	mov	c,acc.6
   0B04 B3                 2432 	cpl	c
   0B05 82 08              2433 	anl	c,extmodem
   0B07 92 F3              2434 	mov	b.3,c
   0B09 90 C0 09           2435 	mov	dptr,#FSKSTAT
   0B0C E0                 2436 	movx	a,@dptr
   0B0D A2 E0              2437 	mov	c,acc.0
   0B0F B0 08              2438 	anl	c,/extmodem
   0B11 72 F3              2439 	orl	c,b.3
   0B13 92 F3              2440 	mov	b.3,c
   0B15 A2 0B              2441 	mov	c,uartempty
   0B17 92 F5              2442 	mov	b.5,c
   0B19 D3                 2443 	setb	c
   0B1A E5 53              2444 	mov	a,rxrd
   0B1C B5 52 06           2445 	cjne	a,rxwr,1$
   0B1F 90 7F B8           2446 	mov	dptr,#(IN2CS)
   0B22 E0                 2447 	movx	a,@dptr
   0B23 A2 E1              2448 	mov	c,acc.1
   0B25 92 F4              2449 1$:	mov	b.4,c
                           2450 	;; bytewide elements
   0B27 90 7E 80           2451 	mov	dptr,#(IN1BUF)
   0B2A E5 F0              2452 	mov	a,b
   0B2C F0                 2453 	movx	@dptr,a
   0B2D E5 48              2454 	mov	a,txwr
   0B2F F4                 2455 	cpl	a
   0B30 25 49              2456 	add	a,txrd
   0B32 54 0F              2457 	anl	a,#(TXCHUNKS-1)
   0B34 90 7E 81           2458 	mov	dptr,#(IN1BUF+1)
   0B37 F0                 2459 	movx	@dptr,a
   0B38 E5 53              2460 	mov	a,rxrd
   0B3A F4                 2461 	cpl	a
   0B3B 25 52              2462 	add	a,rxwr
   0B3D 04                 2463 	inc	a
   0B3E 54 1F              2464 	anl	a,#(RXCHUNKS-1)
   0B40 90 7E 82           2465 	mov	dptr,#(IN1BUF+2)
   0B43 F0                 2466 	movx	@dptr,a
   0B44 90 C0 04           2467 	mov	dptr,#FSKRSSI
   0B47 E0                 2468 	movx	a,@dptr
   0B48 90 7E 83           2469 	mov	dptr,#(IN1BUF+3)
   0B4B F0                 2470 	movx	@dptr,a
                           2471 	; counter
   0B4C 05 44              2472 	inc	irqcount
   0B4E E5 44              2473 	mov	a,irqcount
   0B50 90 7E 84           2474 	mov	dptr,#(IN1BUF+4)
   0B53 F0                 2475 	movx	@dptr,a
                           2476 	; UART buffer
   0B54 75 F0 05           2477 	mov	b,#5
   0B57 90 7E 85           2478 	mov	dptr,#(IN1BUF+5)
   0B5A E5 6B              2479 2$:	mov	a,uartrd
   0B5C B5 6A 07           2480 	cjne	a,uartwr,3$
                           2481 	; set length
   0B5F 90 7F B7           2482 	mov	dptr,#IN1BC
   0B62 E5 F0              2483 	mov	a,b
   0B64 F0                 2484 	movx	@dptr,a
   0B65 22                 2485 	ret
                           2486 	
   0B66 24 5A              2487 3$:	add	a,#uartbuf
   0B68 F8                 2488 	mov	r0,a
   0B69 E6                 2489 	mov	a,@r0
   0B6A F0                 2490 	movx	@dptr,a
   0B6B A3                 2491 	inc	dptr
   0B6C 05 F0              2492 	inc	b
   0B6E E5 6B              2493 	mov	a,uartrd
   0B70 04                 2494 	inc	a
   0B71 54 0F              2495 	anl	a,#0xf
   0B73 F5 6B              2496 	mov	uartrd,a
   0B75 80 E3              2497 	sjmp	2$
                           2498 
                           2499 	
   0B77                    2500 usb_ep1in_isr:
   0B77 C0 E0              2501 	push	acc
   0B79 C0 F0              2502 	push	b
   0B7B C0 82              2503 	push	dpl0
   0B7D C0 83              2504 	push	dph0
   0B7F C0 D0              2505 	push	psw
   0B81 75 D0 00           2506 	mov	psw,#0x00
   0B84 C0 86              2507 	push	dps
   0B86 75 86 00           2508 	mov	dps,#0
                           2509 	;; clear interrupt
   0B89 E5 91              2510 	mov	a,exif
   0B8B C2 E4              2511 	clr	acc.4
   0B8D F5 91              2512 	mov	exif,a
   0B8F 90 7F A9           2513 	mov	dptr,#IN07IRQ
   0B92 74 02              2514 	mov	a,#0x02
   0B94 F0                 2515 	movx	@dptr,a
                           2516 	;; handle interrupt
   0B95 12 0A F1           2517 	lcall	fillusbintr
                           2518 	;; epilogue
   0B98 D0 86              2519 	pop	dps
   0B9A D0 D0              2520 	pop	psw
   0B9C D0 83              2521 	pop	dph0
   0B9E D0 82              2522 	pop	dpl0
   0BA0 D0 F0              2523 	pop	b
   0BA2 D0 E0              2524 	pop	acc
   0BA4 32                 2525 	reti
                           2526 
   0BA5                    2527 usb_ep1out_isr:
   0BA5 C0 E0              2528 	push	acc
   0BA7 C0 F0              2529 	push	b
   0BA9 C0 82              2530 	push	dpl0
   0BAB C0 83              2531 	push	dph0
   0BAD C0 D0              2532 	push	psw
   0BAF 75 D0 00           2533 	mov	psw,#0x00
   0BB2 C0 86              2534 	push	dps
   0BB4 75 86 00           2535 	mov	dps,#0
                           2536 	;; clear interrupt
   0BB7 E5 91              2537 	mov	a,exif
   0BB9 C2 E4              2538 	clr	acc.4
   0BBB F5 91              2539 	mov	exif,a
   0BBD 90 7F AA           2540 	mov	dptr,#OUT07IRQ
   0BC0 74 02              2541 	mov	a,#0x02
   0BC2 F0                 2542 	movx	@dptr,a
                           2543 	;; handle interrupt
                           2544 	;; epilogue
   0BC3 D0 86              2545 	pop	dps
   0BC5 D0 D0              2546 	pop	psw
   0BC7 D0 83              2547 	pop	dph0
   0BC9 D0 82              2548 	pop	dpl0
   0BCB D0 F0              2549 	pop	b
   0BCD D0 E0              2550 	pop	acc
   0BCF 32                 2551 	reti
                           2552 
   0BD0                    2553 usb_ep2in_isr:
   0BD0 C0 E0              2554 	push	acc
   0BD2 C0 F0              2555 	push	b
   0BD4 C0 82              2556 	push	dpl0
   0BD6 C0 83              2557 	push	dph0
   0BD8 C0 D0              2558 	push	psw
   0BDA 75 D0 00           2559 	mov	psw,#0x00
   0BDD C0 86              2560 	push	dps
   0BDF 75 86 00           2561 	mov	dps,#0
                           2562 	;; clear interrupt
   0BE2 E5 91              2563 	mov	a,exif
   0BE4 C2 E4              2564 	clr	acc.4
   0BE6 F5 91              2565 	mov	exif,a
   0BE8 90 7F A9           2566 	mov	dptr,#IN07IRQ
   0BEB 74 04              2567 	mov	a,#0x04
   0BED F0                 2568 	movx	@dptr,a
                           2569 	;; handle interrupt
                           2570 	;; epilogue
   0BEE D0 86              2571 	pop	dps
   0BF0 D0 D0              2572 	pop	psw
   0BF2 D0 83              2573 	pop	dph0
   0BF4 D0 82              2574 	pop	dpl0
   0BF6 D0 F0              2575 	pop	b
   0BF8 D0 E0              2576 	pop	acc
   0BFA 32                 2577 	reti
                           2578 
   0BFB                    2579 usb_ep2out_isr:
   0BFB C0 E0              2580 	push	acc
   0BFD C0 F0              2581 	push	b
   0BFF C0 82              2582 	push	dpl0
   0C01 C0 83              2583 	push	dph0
   0C03 C0 D0              2584 	push	psw
   0C05 75 D0 00           2585 	mov	psw,#0x00
   0C08 C0 86              2586 	push	dps
   0C0A 75 86 00           2587 	mov	dps,#0
                           2588 	;; clear interrupt
   0C0D E5 91              2589 	mov	a,exif
   0C0F C2 E4              2590 	clr	acc.4
   0C11 F5 91              2591 	mov	exif,a
   0C13 90 7F AA           2592 	mov	dptr,#OUT07IRQ
   0C16 74 04              2593 	mov	a,#0x04
   0C18 F0                 2594 	movx	@dptr,a
                           2595 	;; handle interrupt
                           2596 	;; epilogue
   0C19 D0 86              2597 	pop	dps
   0C1B D0 D0              2598 	pop	psw
   0C1D D0 83              2599 	pop	dph0
   0C1F D0 82              2600 	pop	dpl0
   0C21 D0 F0              2601 	pop	b
   0C23 D0 E0              2602 	pop	acc
   0C25 32                 2603 	reti
                           2604 
   0C26                    2605 usb_ep3in_isr:
   0C26 C0 E0              2606 	push	acc
   0C28 C0 F0              2607 	push	b
   0C2A C0 82              2608 	push	dpl0
   0C2C C0 83              2609 	push	dph0
   0C2E C0 D0              2610 	push	psw
   0C30 75 D0 00           2611 	mov	psw,#0x00
   0C33 C0 86              2612 	push	dps
   0C35 75 86 00           2613 	mov	dps,#0
                           2614 	;; clear interrupt
   0C38 E5 91              2615 	mov	a,exif
   0C3A C2 E4              2616 	clr	acc.4
   0C3C F5 91              2617 	mov	exif,a
   0C3E 90 7F A9           2618 	mov	dptr,#IN07IRQ
   0C41 74 08              2619 	mov	a,#0x08
   0C43 F0                 2620 	movx	@dptr,a
                           2621 	;; handle interrupt
                           2622 	;; epilogue
   0C44 D0 86              2623 	pop	dps
   0C46 D0 D0              2624 	pop	psw
   0C48 D0 83              2625 	pop	dph0
   0C4A D0 82              2626 	pop	dpl0
   0C4C D0 F0              2627 	pop	b
   0C4E D0 E0              2628 	pop	acc
   0C50 32                 2629 	reti
                           2630 
   0C51                    2631 usb_ep3out_isr:
   0C51 C0 E0              2632 	push	acc
   0C53 C0 F0              2633 	push	b
   0C55 C0 82              2634 	push	dpl0
   0C57 C0 83              2635 	push	dph0
   0C59 C0 D0              2636 	push	psw
   0C5B 75 D0 00           2637 	mov	psw,#0x00
   0C5E C0 86              2638 	push	dps
   0C60 75 86 00           2639 	mov	dps,#0
                           2640 	;; clear interrupt
   0C63 E5 91              2641 	mov	a,exif
   0C65 C2 E4              2642 	clr	acc.4
   0C67 F5 91              2643 	mov	exif,a
   0C69 90 7F AA           2644 	mov	dptr,#OUT07IRQ
   0C6C 74 08              2645 	mov	a,#0x08
   0C6E F0                 2646 	movx	@dptr,a
                           2647 	;; handle interrupt
                           2648 	;; epilogue
   0C6F D0 86              2649 	pop	dps
   0C71 D0 D0              2650 	pop	psw
   0C73 D0 83              2651 	pop	dph0
   0C75 D0 82              2652 	pop	dpl0
   0C77 D0 F0              2653 	pop	b
   0C79 D0 E0              2654 	pop	acc
   0C7B 32                 2655 	reti
                           2656 
   0C7C                    2657 usb_ep4in_isr:
   0C7C C0 E0              2658 	push	acc
   0C7E C0 F0              2659 	push	b
   0C80 C0 82              2660 	push	dpl0
   0C82 C0 83              2661 	push	dph0
   0C84 C0 D0              2662 	push	psw
   0C86 75 D0 00           2663 	mov	psw,#0x00
   0C89 C0 86              2664 	push	dps
   0C8B 75 86 00           2665 	mov	dps,#0
                           2666 	;; clear interrupt
   0C8E E5 91              2667 	mov	a,exif
   0C90 C2 E4              2668 	clr	acc.4
   0C92 F5 91              2669 	mov	exif,a
   0C94 90 7F A9           2670 	mov	dptr,#IN07IRQ
   0C97 74 10              2671 	mov	a,#0x10
   0C99 F0                 2672 	movx	@dptr,a
                           2673 	;; handle interrupt
                           2674 	;; epilogue
   0C9A D0 86              2675 	pop	dps
   0C9C D0 D0              2676 	pop	psw
   0C9E D0 83              2677 	pop	dph0
   0CA0 D0 82              2678 	pop	dpl0
   0CA2 D0 F0              2679 	pop	b
   0CA4 D0 E0              2680 	pop	acc
   0CA6 32                 2681 	reti
                           2682 
   0CA7                    2683 usb_ep4out_isr:
   0CA7 C0 E0              2684 	push	acc
   0CA9 C0 F0              2685 	push	b
   0CAB C0 82              2686 	push	dpl0
   0CAD C0 83              2687 	push	dph0
   0CAF C0 D0              2688 	push	psw
   0CB1 75 D0 00           2689 	mov	psw,#0x00
   0CB4 C0 86              2690 	push	dps
   0CB6 75 86 00           2691 	mov	dps,#0
                           2692 	;; clear interrupt
   0CB9 E5 91              2693 	mov	a,exif
   0CBB C2 E4              2694 	clr	acc.4
   0CBD F5 91              2695 	mov	exif,a
   0CBF 90 7F AA           2696 	mov	dptr,#OUT07IRQ
   0CC2 74 10              2697 	mov	a,#0x10
   0CC4 F0                 2698 	movx	@dptr,a
                           2699 	;; handle interrupt
                           2700 	;; epilogue
   0CC5 D0 86              2701 	pop	dps
   0CC7 D0 D0              2702 	pop	psw
   0CC9 D0 83              2703 	pop	dph0
   0CCB D0 82              2704 	pop	dpl0
   0CCD D0 F0              2705 	pop	b
   0CCF D0 E0              2706 	pop	acc
   0CD1 32                 2707 	reti
                           2708 
   0CD2                    2709 usb_ep5in_isr:
   0CD2 C0 E0              2710 	push	acc
   0CD4 C0 F0              2711 	push	b
   0CD6 C0 82              2712 	push	dpl0
   0CD8 C0 83              2713 	push	dph0
   0CDA C0 D0              2714 	push	psw
   0CDC 75 D0 00           2715 	mov	psw,#0x00
   0CDF C0 86              2716 	push	dps
   0CE1 75 86 00           2717 	mov	dps,#0
                           2718 	;; clear interrupt
   0CE4 E5 91              2719 	mov	a,exif
   0CE6 C2 E4              2720 	clr	acc.4
   0CE8 F5 91              2721 	mov	exif,a
   0CEA 90 7F A9           2722 	mov	dptr,#IN07IRQ
   0CED 74 20              2723 	mov	a,#0x20
   0CEF F0                 2724 	movx	@dptr,a
                           2725 	;; handle interrupt
                           2726 	;; epilogue
   0CF0 D0 86              2727 	pop	dps
   0CF2 D0 D0              2728 	pop	psw
   0CF4 D0 83              2729 	pop	dph0
   0CF6 D0 82              2730 	pop	dpl0
   0CF8 D0 F0              2731 	pop	b
   0CFA D0 E0              2732 	pop	acc
   0CFC 32                 2733 	reti
                           2734 
   0CFD                    2735 usb_ep5out_isr:
   0CFD C0 E0              2736 	push	acc
   0CFF C0 F0              2737 	push	b
   0D01 C0 82              2738 	push	dpl0
   0D03 C0 83              2739 	push	dph0
   0D05 C0 D0              2740 	push	psw
   0D07 75 D0 00           2741 	mov	psw,#0x00
   0D0A C0 86              2742 	push	dps
   0D0C 75 86 00           2743 	mov	dps,#0
                           2744 	;; clear interrupt
   0D0F E5 91              2745 	mov	a,exif
   0D11 C2 E4              2746 	clr	acc.4
   0D13 F5 91              2747 	mov	exif,a
   0D15 90 7F AA           2748 	mov	dptr,#OUT07IRQ
   0D18 74 20              2749 	mov	a,#0x20
   0D1A F0                 2750 	movx	@dptr,a
                           2751 	;; handle interrupt
                           2752 	;; epilogue
   0D1B D0 86              2753 	pop	dps
   0D1D D0 D0              2754 	pop	psw
   0D1F D0 83              2755 	pop	dph0
   0D21 D0 82              2756 	pop	dpl0
   0D23 D0 F0              2757 	pop	b
   0D25 D0 E0              2758 	pop	acc
   0D27 32                 2759 	reti
                           2760 
   0D28                    2761 usb_ep6in_isr:
   0D28 C0 E0              2762 	push	acc
   0D2A C0 F0              2763 	push	b
   0D2C C0 82              2764 	push	dpl0
   0D2E C0 83              2765 	push	dph0
   0D30 C0 D0              2766 	push	psw
   0D32 75 D0 00           2767 	mov	psw,#0x00
   0D35 C0 86              2768 	push	dps
   0D37 75 86 00           2769 	mov	dps,#0
                           2770 	;; clear interrupt
   0D3A E5 91              2771 	mov	a,exif
   0D3C C2 E4              2772 	clr	acc.4
   0D3E F5 91              2773 	mov	exif,a
   0D40 90 7F A9           2774 	mov	dptr,#IN07IRQ
   0D43 74 40              2775 	mov	a,#0x40
   0D45 F0                 2776 	movx	@dptr,a
                           2777 	;; handle interrupt
                           2778 	;; epilogue
   0D46 D0 86              2779 	pop	dps
   0D48 D0 D0              2780 	pop	psw
   0D4A D0 83              2781 	pop	dph0
   0D4C D0 82              2782 	pop	dpl0
   0D4E D0 F0              2783 	pop	b
   0D50 D0 E0              2784 	pop	acc
   0D52 32                 2785 	reti
                           2786 
   0D53                    2787 usb_ep6out_isr:
   0D53 C0 E0              2788 	push	acc
   0D55 C0 F0              2789 	push	b
   0D57 C0 82              2790 	push	dpl0
   0D59 C0 83              2791 	push	dph0
   0D5B C0 D0              2792 	push	psw
   0D5D 75 D0 00           2793 	mov	psw,#0x00
   0D60 C0 86              2794 	push	dps
   0D62 75 86 00           2795 	mov	dps,#0
                           2796 	;; clear interrupt
   0D65 E5 91              2797 	mov	a,exif
   0D67 C2 E4              2798 	clr	acc.4
   0D69 F5 91              2799 	mov	exif,a
   0D6B 90 7F AA           2800 	mov	dptr,#OUT07IRQ
   0D6E 74 40              2801 	mov	a,#0x40
   0D70 F0                 2802 	movx	@dptr,a
                           2803 	;; handle interrupt
                           2804 	;; epilogue
   0D71 D0 86              2805 	pop	dps
   0D73 D0 D0              2806 	pop	psw
   0D75 D0 83              2807 	pop	dph0
   0D77 D0 82              2808 	pop	dpl0
   0D79 D0 F0              2809 	pop	b
   0D7B D0 E0              2810 	pop	acc
   0D7D 32                 2811 	reti
                           2812 
   0D7E                    2813 usb_ep7in_isr:
   0D7E C0 E0              2814 	push	acc
   0D80 C0 F0              2815 	push	b
   0D82 C0 82              2816 	push	dpl0
   0D84 C0 83              2817 	push	dph0
   0D86 C0 D0              2818 	push	psw
   0D88 75 D0 00           2819 	mov	psw,#0x00
   0D8B C0 86              2820 	push	dps
   0D8D 75 86 00           2821 	mov	dps,#0
                           2822 	;; clear interrupt
   0D90 E5 91              2823 	mov	a,exif
   0D92 C2 E4              2824 	clr	acc.4
   0D94 F5 91              2825 	mov	exif,a
   0D96 90 7F A9           2826 	mov	dptr,#IN07IRQ
   0D99 74 80              2827 	mov	a,#0x80
   0D9B F0                 2828 	movx	@dptr,a
                           2829 	;; handle interrupt
                           2830 	;; epilogue
   0D9C D0 86              2831 	pop	dps
   0D9E D0 D0              2832 	pop	psw
   0DA0 D0 83              2833 	pop	dph0
   0DA2 D0 82              2834 	pop	dpl0
   0DA4 D0 F0              2835 	pop	b
   0DA6 D0 E0              2836 	pop	acc
   0DA8 32                 2837 	reti
                           2838 
   0DA9                    2839 usb_ep7out_isr:
   0DA9 C0 E0              2840 	push	acc
   0DAB C0 F0              2841 	push	b
   0DAD C0 82              2842 	push	dpl0
   0DAF C0 83              2843 	push	dph0
   0DB1 C0 D0              2844 	push	psw
   0DB3 75 D0 00           2845 	mov	psw,#0x00
   0DB6 C0 86              2846 	push	dps
   0DB8 75 86 00           2847 	mov	dps,#0
                           2848 	;; clear interrupt
   0DBB E5 91              2849 	mov	a,exif
   0DBD C2 E4              2850 	clr	acc.4
   0DBF F5 91              2851 	mov	exif,a
   0DC1 90 7F AA           2852 	mov	dptr,#OUT07IRQ
   0DC4 74 80              2853 	mov	a,#0x80
   0DC6 F0                 2854 	movx	@dptr,a
                           2855 	;; handle interrupt
                           2856 	;; epilogue
   0DC7 D0 86              2857 	pop	dps
   0DC9 D0 D0              2858 	pop	psw
   0DCB D0 83              2859 	pop	dph0
   0DCD D0 82              2860 	pop	dpl0
   0DCF D0 F0              2861 	pop	b
   0DD1 D0 E0              2862 	pop	acc
   0DD3 32                 2863 	reti
                           2864 	
                           2865 	;; -----------------------------------------------------
                           2866 	;; USB descriptors
                           2867 	;; -----------------------------------------------------
                           2868 
                           2869 	;; Device and/or Interface Class codes
                    0000   2870 	USB_CLASS_PER_INTERFACE         = 0
                    0001   2871 	USB_CLASS_AUDIO                 = 1
                    0002   2872 	USB_CLASS_COMM                  = 2
                    0003   2873 	USB_CLASS_HID                   = 3
                    0007   2874 	USB_CLASS_PRINTER               = 7
                    0008   2875 	USB_CLASS_MASS_STORAGE          = 8
                    0009   2876 	USB_CLASS_HUB                   = 9
                    00FF   2877 	USB_CLASS_VENDOR_SPEC           = 0xff
                           2878 
                           2879 	;; Descriptor types
                    0001   2880 	USB_DT_DEVICE                   = 0x01
                    0002   2881 	USB_DT_CONFIG                   = 0x02
                    0003   2882 	USB_DT_STRING                   = 0x03
                    0004   2883 	USB_DT_INTERFACE                = 0x04
                    0005   2884 	USB_DT_ENDPOINT                 = 0x05
                           2885 
                           2886 	;; Standard requests
                    0000   2887 	USB_REQ_GET_STATUS              = 0x00
                    0001   2888 	USB_REQ_CLEAR_FEATURE           = 0x01
                    0003   2889 	USB_REQ_SET_FEATURE             = 0x03
                    0005   2890 	USB_REQ_SET_ADDRESS             = 0x05
                    0006   2891 	USB_REQ_GET_DESCRIPTOR          = 0x06
                    0007   2892 	USB_REQ_SET_DESCRIPTOR          = 0x07
                    0008   2893 	USB_REQ_GET_CONFIGURATION       = 0x08
                    0009   2894 	USB_REQ_SET_CONFIGURATION       = 0x09
                    000A   2895 	USB_REQ_GET_INTERFACE           = 0x0A
                    000B   2896 	USB_REQ_SET_INTERFACE           = 0x0B
                    000C   2897 	USB_REQ_SYNCH_FRAME             = 0x0C
                           2898 
                           2899 	;; USB Request Type and Endpoint Directions
                    0000   2900 	USB_DIR_OUT                     = 0
                    0080   2901 	USB_DIR_IN                      = 0x80
                           2902 
                    0000   2903 	USB_TYPE_STANDARD               = (0x00 << 5)
                    0020   2904 	USB_TYPE_CLASS                  = (0x01 << 5)
                    0040   2905 	USB_TYPE_VENDOR                 = (0x02 << 5)
                    0060   2906 	USB_TYPE_RESERVED               = (0x03 << 5)
                           2907 
                    0000   2908 	USB_RECIP_DEVICE                = 0x00
                    0001   2909 	USB_RECIP_INTERFACE             = 0x01
                    0002   2910 	USB_RECIP_ENDPOINT              = 0x02
                    0003   2911 	USB_RECIP_OTHER                 = 0x03
                           2912 
                           2913 	;; Request target types.
                    0000   2914 	USB_RT_DEVICE                   = 0x00
                    0001   2915 	USB_RT_INTERFACE                = 0x01
                    0002   2916 	USB_RT_ENDPOINT                 = 0x02
                           2917 
                    BAC0   2918 	VENDID	= 0xbac0
                    6136   2919 	PRODID	= 0x6136
                           2920 
   0DD4                    2921 devicedescr:
   0DD4 12                 2922 	.db	18			; bLength
   0DD5 01                 2923 	.db	USB_DT_DEVICE		; bDescriptorType
   0DD6 00 01              2924 	.db	0x00, 0x01		; bcdUSB
   0DD8 FF                 2925 	.db	USB_CLASS_VENDOR_SPEC	; bDeviceClass
   0DD9 00                 2926 	.db	0			; bDeviceSubClass
   0DDA FF                 2927 	.db	0xff			; bDeviceProtocol
   0DDB 40                 2928 	.db	0x40			; bMaxPacketSize0
   0DDC C0 BA              2929 	.db	<VENDID,>VENDID		; idVendor
   0DDE 36 61              2930 	.db	<PRODID,>PRODID		; idProduct
   0DE0 02 00              2931 	.db	0x02,0x00		; bcdDevice
   0DE2 01                 2932 	.db	1			; iManufacturer
   0DE3 02                 2933 	.db	2			; iProduct
   0DE4 03                 2934 	.db	3			; iSerialNumber
   0DE5 01                 2935 	.db	1			; bNumConfigurations
                           2936 
   0DE6                    2937 config0descr:
   0DE6 09                 2938 	.db	9			; bLength
   0DE7 02                 2939 	.db	USB_DT_CONFIG		; bDescriptorType
   0DE8 45 00              2940 	.db	<config0sz,>config0sz	; wTotalLength
   0DEA 01                 2941 	.db	1			; bNumInterfaces
   0DEB 01                 2942 	.db	1			; bConfigurationValue
   0DEC 00                 2943 	.db	0			; iConfiguration
   0DED 40                 2944 	.db	0b01000000		; bmAttributs (self powered)
   0DEE 00                 2945 	.db	0			; MaxPower (mA/2) (self powered so 0)
                           2946 	;; interface descriptor I0:A0
   0DEF 09                 2947 	.db	9			; bLength
   0DF0 04                 2948 	.db	USB_DT_INTERFACE	; bDescriptorType
   0DF1 00                 2949 	.db	0			; bInterfaceNumber
   0DF2 00                 2950 	.db	0			; bAlternateSetting
   0DF3 03                 2951 	.db	3			; bNumEndpoints
   0DF4 FF                 2952 	.db	0xff			; bInterfaceClass (vendor specific)
   0DF5 00                 2953 	.db	0x00			; bInterfaceSubClass
   0DF6 FF                 2954 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0DF7 00                 2955 	.db	0			; iInterface
                           2956 	;; endpoint descriptor I0:A0:E0
   0DF8 07                 2957 	.db	7			; bLength
   0DF9 05                 2958 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0DFA 81                 2959 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0DFB 02                 2960 	.db	0x02			; bmAttributes (bulk)
   0DFC 40 00              2961 	.db	0x40,0x00		; wMaxPacketSize
   0DFE 00                 2962 	.db	0			; bInterval
                           2963 	;; endpoint descriptor I0:A0:E1
   0DFF 07                 2964 	.db	7			; bLength
   0E00 05                 2965 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E01 82                 2966 	.db	(USB_DIR_IN | 2)	; bEndpointAddress
   0E02 02                 2967 	.db	0x02			; bmAttributes (bulk)
   0E03 40 00              2968 	.db	0x40,0x00		; wMaxPacketSize
   0E05 00                 2969 	.db	0			; bInterval
                           2970 	;; endpoint descriptor I0:A0:E2
   0E06 07                 2971 	.db	7			; bLength
   0E07 05                 2972 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E08 02                 2973 	.db	(USB_DIR_OUT | 2)	; bEndpointAddress
   0E09 02                 2974 	.db	0x02			; bmAttributes (bulk)
   0E0A 40 00              2975 	.db	0x40,0x00		; wMaxPacketSize
   0E0C 00                 2976 	.db	0			; bInterval
                           2977 	;; interface descriptor I0:A1
   0E0D 09                 2978 	.db	9			; bLength
   0E0E 04                 2979 	.db	USB_DT_INTERFACE	; bDescriptorType
   0E0F 00                 2980 	.db	0			; bInterfaceNumber
   0E10 01                 2981 	.db	1			; bAlternateSetting
   0E11 03                 2982 	.db	3			; bNumEndpoints
   0E12 FF                 2983 	.db	0xff			; bInterfaceClass (vendor specific)
   0E13 00                 2984 	.db	0x00			; bInterfaceSubClass
   0E14 FF                 2985 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0E15 00                 2986 	.db	0			; iInterface
                           2987 	;; endpoint descriptor I0:A1:E0
   0E16 07                 2988 	.db	7			; bLength
   0E17 05                 2989 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E18 81                 2990 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0E19 03                 2991 	.db	0x03			; bmAttributes (interrupt)
   0E1A 40 00              2992 	.db	0x40,0x00		; wMaxPacketSize
   0E1C 0A                 2993 	.db	10			; bInterval
                           2994 	;; endpoint descriptor I0:A1:E1
   0E1D 07                 2995 	.db	7			; bLength
   0E1E 05                 2996 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E1F 82                 2997 	.db	(USB_DIR_IN | 2)	; bEndpointAddress
   0E20 02                 2998 	.db	0x02			; bmAttributes (bulk)
   0E21 40 00              2999 	.db	0x40,0x00		; wMaxPacketSize
   0E23 00                 3000 	.db	0			; bInterval
                           3001 	;; endpoint descriptor I0:A1:E2
   0E24 07                 3002 	.db	7			; bLength
   0E25 05                 3003 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0E26 02                 3004 	.db	(USB_DIR_OUT | 2)	; bEndpointAddress
   0E27 02                 3005 	.db	0x02			; bmAttributes (bulk)
   0E28 40 00              3006 	.db	0x40,0x00		; wMaxPacketSize
   0E2A 00                 3007 	.db	0			; bInterval
                           3008 
                    0045   3009 config0sz = . - config0descr
                           3010 
   0E2B                    3011 stringdescr:
   0E2B 33 0E              3012 	.db	<string0,>string0
   0E2D 37 0E              3013 	.db	<string1,>string1
   0E2F 45 0E              3014 	.db	<string2,>string2
   0E31 61 0E              3015 	.db	<stringserial,>stringserial
                           3016 
                    0004   3017 numstrings = (. - stringdescr)/2
                           3018 
   0E33                    3019 string0:
   0E33 04                 3020 	.db	string0sz		; bLength
   0E34 03                 3021 	.db	USB_DT_STRING		; bDescriptorType
   0E35 00 00              3022 	.db	0,0			; LANGID[0]: Lang Neutral
                    0004   3023 string0sz = . - string0
                           3024 
   0E37                    3025 string1:
   0E37 0E                 3026 	.db	string1sz		; bLength
   0E38 03                 3027 	.db	USB_DT_STRING		; bDescriptorType
   0E39 42 00 61 00 79 00  3028 	.db	'B,0,'a,0,'y,0,'c,0,'o,0,'m,0
        63 00 6F 00 6D 00
                    000E   3029 string1sz = . - string1
                           3030 
   0E45                    3031 string2:
   0E45 1C                 3032 	.db	string2sz		; bLength
   0E46 03                 3033 	.db	USB_DT_STRING		; bDescriptorType
   0E47 55 00 53 00 42 00  3034 	.db	'U,0,'S,0,'B,0,'F,0,'L,0,'E,0,'X,0,' ,0
        46 00 4C 00 45 00
        58 00 20 00
   0E57 28 00 46 00 53 00  3035 	.db	'(,0,'F,0,'S,0,'K,0,'),0
        4B 00 29 00
                    001C   3036 string2sz = . - string2
                           3037 
   0E61                    3038 stringserial:
   0E61 02                 3039 	.db	2			; bLength
   0E62 03                 3040 	.db	USB_DT_STRING		; bDescriptorType
   0E63 00 00 00 00 00 00  3041 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
   0E73 00 00 00 00 00 00  3042 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
                           3043 
