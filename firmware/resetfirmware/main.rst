                              1 	.module main
                              2 
                              3 	;; this apparently does not work; the anchorchips controller
                              4 	;; sometimes gets into a state where it responds to get_device_descriptor
                              5 	;; requests with zero length descriptors
                              6 	
                              7 	;; define code segments link order
                              8 	.area CODE (CODE)
                              9 
                             10 	;; -----------------------------------------------------
                             11 
                             12 	;; special function registers (which are not predefined)
                    0082     13 	dpl0    = 0x82
                    0083     14 	dph0    = 0x83
                    0084     15 	dpl1    = 0x84
                    0085     16 	dph1    = 0x85
                    0086     17 	dps     = 0x86
                    008E     18 	ckcon   = 0x8E
                    008F     19 	spc_fnc = 0x8F
                    0091     20 	exif    = 0x91
                    0092     21 	mpage   = 0x92
                    0098     22 	scon0   = 0x98
                    0099     23 	sbuf0   = 0x99
                    00C0     24 	scon1   = 0xC0
                    00C1     25 	sbuf1   = 0xC1
                    00D8     26 	eicon   = 0xD8
                    00E8     27 	eie     = 0xE8
                    00F8     28 	eip     = 0xF8
                             29 
                             30 	;; anchor xdata registers
                    7F00     31 	IN0BUF		= 0x7F00
                    7EC0     32 	OUT0BUF		= 0x7EC0
                    7E80     33 	IN1BUF		= 0x7E80
                    7E40     34 	OUT1BUF		= 0x7E40
                    7E00     35 	IN2BUF		= 0x7E00
                    7DC0     36 	OUT2BUF		= 0x7DC0
                    7D80     37 	IN3BUF		= 0x7D80
                    7D40     38 	OUT3BUF		= 0x7D40
                    7D00     39 	IN4BUF		= 0x7D00
                    7CC0     40 	OUT4BUF		= 0x7CC0
                    7C80     41 	IN5BUF		= 0x7C80
                    7C40     42 	OUT5BUF		= 0x7C40
                    7C00     43 	IN6BUF		= 0x7C00
                    7BC0     44 	OUT6BUF		= 0x7BC0
                    7B80     45 	IN7BUF		= 0x7B80
                    7B40     46 	OUT7BUF		= 0x7B40
                    7FE8     47 	SETUPBUF	= 0x7FE8
                    7FE8     48 	SETUPDAT	= 0x7FE8
                             49 
                    7FB4     50 	EP0CS		= 0x7FB4
                    7FB5     51 	IN0BC		= 0x7FB5
                    7FB6     52 	IN1CS		= 0x7FB6
                    7FB7     53 	IN1BC		= 0x7FB7
                    7FB8     54 	IN2CS		= 0x7FB8
                    7FB9     55 	IN2BC		= 0x7FB9
                    7FBA     56 	IN3CS		= 0x7FBA
                    7FBB     57 	IN3BC		= 0x7FBB
                    7FBC     58 	IN4CS		= 0x7FBC
                    7FBD     59 	IN4BC		= 0x7FBD
                    7FBE     60 	IN5CS		= 0x7FBE
                    7FBF     61 	IN5BC		= 0x7FBF
                    7FC0     62 	IN6CS		= 0x7FC0
                    7FC1     63 	IN6BC		= 0x7FC1
                    7FC2     64 	IN7CS		= 0x7FC2
                    7FC3     65 	IN7BC		= 0x7FC3
                    7FC5     66 	OUT0BC		= 0x7FC5
                    7FC6     67 	OUT1CS		= 0x7FC6
                    7FC7     68 	OUT1BC		= 0x7FC7
                    7FC8     69 	OUT2CS		= 0x7FC8
                    7FC9     70 	OUT2BC		= 0x7FC9
                    7FCA     71 	OUT3CS		= 0x7FCA
                    7FCB     72 	OUT3BC		= 0x7FCB
                    7FCC     73 	OUT4CS		= 0x7FCC
                    7FCD     74 	OUT4BC		= 0x7FCD
                    7FCE     75 	OUT5CS		= 0x7FCE
                    7FCF     76 	OUT5BC		= 0x7FCF
                    7FD0     77 	OUT6CS		= 0x7FD0
                    7FD1     78 	OUT6BC		= 0x7FD1
                    7FD2     79 	OUT7CS		= 0x7FD2
                    7FD3     80 	OUT7BC		= 0x7FD3
                             81 
                    7FA8     82 	IVEC		= 0x7FA8
                    7FA9     83 	IN07IRQ		= 0x7FA9
                    7FAA     84 	OUT07IRQ	= 0x7FAA
                    7FAB     85 	USBIRQ		= 0x7FAB
                    7FAC     86 	IN07IEN		= 0x7FAC
                    7FAD     87 	OUT07IEN	= 0x7FAD
                    7FAE     88 	USBIEN		= 0x7FAE
                    7FAF     89 	USBBAV		= 0x7FAF
                    7FB2     90 	BPADDRH		= 0x7FB2
                    7FB3     91 	BPADDRL		= 0x7FB3
                             92 
                    7FD4     93 	SUDPTRH		= 0x7FD4
                    7FD5     94 	SUDPTRL		= 0x7FD5
                    7FD6     95 	USBCS		= 0x7FD6
                    7FD7     96 	TOGCTL		= 0x7FD7
                    7FD8     97 	USBFRAMEL	= 0x7FD8
                    7FD9     98 	USBFRAMEH	= 0x7FD9
                    7FDB     99 	FNADDR		= 0x7FDB
                    7FDD    100 	USBPAIR		= 0x7FDD
                    7FDE    101 	IN07VAL		= 0x7FDE
                    7FDF    102 	OUT07VAL	= 0x7FDF
                    7FE3    103 	AUTOPTRH	= 0x7FE3
                    7FE4    104 	AUTOPTRL	= 0x7FE4
                    7FE5    105 	AUTODATA	= 0x7FE5
                            106 
                            107 	;; isochronous endpoints. only available if ISODISAB=0
                            108 
                    7F60    109 	OUT8DATA	= 0x7F60
                    7F61    110 	OUT9DATA	= 0x7F61
                    7F62    111 	OUT10DATA	= 0x7F62
                    7F63    112 	OUT11DATA	= 0x7F63
                    7F64    113 	OUT12DATA	= 0x7F64
                    7F65    114 	OUT13DATA	= 0x7F65
                    7F66    115 	OUT14DATA	= 0x7F66
                    7F67    116 	OUT15DATA	= 0x7F67
                            117 
                    7F68    118 	IN8DATA		= 0x7F68
                    7F69    119 	IN9DATA		= 0x7F69
                    7F6A    120 	IN10DATA	= 0x7F6A
                    7F6B    121 	IN11DATA	= 0x7F6B
                    7F6C    122 	IN12DATA	= 0x7F6C
                    7F6D    123 	IN13DATA	= 0x7F6D
                    7F6E    124 	IN14DATA	= 0x7F6E
                    7F6F    125 	IN15DATA	= 0x7F6F
                            126 
                    7F70    127 	OUT8BCH		= 0x7F70
                    7F71    128 	OUT8BCL		= 0x7F71
                    7F72    129 	OUT9BCH		= 0x7F72
                    7F73    130 	OUT9BCL		= 0x7F73
                    7F74    131 	OUT10BCH	= 0x7F74
                    7F75    132 	OUT10BCL	= 0x7F75
                    7F76    133 	OUT11BCH	= 0x7F76
                    7F77    134 	OUT11BCL	= 0x7F77
                    7F78    135 	OUT12BCH	= 0x7F78
                    7F79    136 	OUT12BCL	= 0x7F79
                    7F7A    137 	OUT13BCH	= 0x7F7A
                    7F7B    138 	OUT13BCL	= 0x7F7B
                    7F7C    139 	OUT14BCH	= 0x7F7C
                    7F7D    140 	OUT14BCL	= 0x7F7D
                    7F7E    141 	OUT15BCH	= 0x7F7E
                    7F7F    142 	OUT15BCL	= 0x7F7F
                            143 
                    7FF0    144 	OUT8ADDR	= 0x7FF0
                    7FF1    145 	OUT9ADDR	= 0x7FF1
                    7FF2    146 	OUT10ADDR	= 0x7FF2
                    7FF3    147 	OUT11ADDR	= 0x7FF3
                    7FF4    148 	OUT12ADDR	= 0x7FF4
                    7FF5    149 	OUT13ADDR	= 0x7FF5
                    7FF6    150 	OUT14ADDR	= 0x7FF6
                    7FF7    151 	OUT15ADDR	= 0x7FF7
                    7FF8    152 	IN8ADDR		= 0x7FF8
                    7FF9    153 	IN9ADDR		= 0x7FF9
                    7FFA    154 	IN10ADDR	= 0x7FFA
                    7FFB    155 	IN11ADDR	= 0x7FFB
                    7FFC    156 	IN12ADDR	= 0x7FFC
                    7FFD    157 	IN13ADDR	= 0x7FFD
                    7FFE    158 	IN14ADDR	= 0x7FFE
                    7FFF    159 	IN15ADDR	= 0x7FFF
                            160 
                    7FA0    161 	ISOERR		= 0x7FA0
                    7FA1    162 	ISOCTL		= 0x7FA1
                    7FA2    163 	ZBCOUNT		= 0x7FA2
                    7FE0    164 	INISOVAL	= 0x7FE0
                    7FE1    165 	OUTISOVAL	= 0x7FE1
                    7FE2    166 	FASTXFR		= 0x7FE2
                            167 
                            168 	;; CPU control registers
                            169 
                    7F92    170 	CPUCS		= 0x7F92
                            171 
                            172 	;; IO port control registers
                            173 
                    7F93    174 	PORTACFG	= 0x7F93
                    7F94    175 	PORTBCFG	= 0x7F94
                    7F95    176 	PORTCCFG	= 0x7F95
                    7F96    177 	OUTA		= 0x7F96
                    7F97    178 	OUTB		= 0x7F97
                    7F98    179 	OUTC		= 0x7F98
                    7F99    180 	PINSA		= 0x7F99
                    7F9A    181 	PINSB		= 0x7F9A
                    7F9B    182 	PINSC		= 0x7F9B
                    7F9C    183 	OEA		= 0x7F9C
                    7F9D    184 	OEB		= 0x7F9D
                    7F9E    185 	OEC		= 0x7F9E
                            186 
                            187 	;; I2C controller registers
                            188 
                    7FA5    189 	I2CS		= 0x7FA5
                    7FA6    190 	I2DAT		= 0x7FA6
                            191 
                            192 	;; -----------------------------------------------------
                            193 
                            194 	.area CODE (CODE)
   0000 75 81 7F            195 	mov	sp,#0x7f
   0003 E4                  196 	clr	a
   0004 F5 D0               197 	mov	psw,a
   0006 F5 86               198 	mov	dps,a
                            199 
                            200 	;; some indirect register setup
   0008 75 8E 31            201 	mov	ckcon,#0x31	; one external wait state, to avoid chip bugs
                            202 	;; disable interrupts
   000B F5 A8               203 	mov	ie,a		; disable timer 0 int
   000D F5 E8               204 	mov	eie,a		; disable USB interrupts
   000F 90 7F AE            205 	mov	dptr,#USBIEN
   0012 F0                  206 	movx	@dptr,a
   0013 90 7F AC            207 	mov	dptr,#IN07IEN
   0016 F0                  208 	movx	@dptr,a
   0017 90 7F AD            209 	mov	dptr,#OUT07IEN
   001A F0                  210 	movx	@dptr,a
                            211 	;; allow Windows time to finish the writecpucs control transfer
                            212 	;; 10ms delay loop
   001B 90 E8 90            213 	mov	dptr,#-6000
   001E A3                  214 3$:	inc	dptr		; 3 cycles
   001F E5 82               215 	mov	a,dpl0		; 2 cycles
   0021 45 83               216 	orl	a,dph0		; 2 cycles
   0023 70 F9               217 	jnz	3$		; 3 cycles
                            218 	;; disconnect from USB bus
   0025 90 7F D6            219 	mov	dptr,#USBCS
   0028 E4                  220 	clr	a
   0029 F0                  221 	movx	@dptr,a
                            222 	;; init USB subsystem
   002A 90 7F A1            223 	mov	dptr,#ISOCTL
   002D 74 01               224 	mov	a,#1		; disable ISO endpoints
   002F F0                  225 	movx	@dptr,a
   0030 90 7F AF            226 	mov	dptr,#USBBAV
   0033 74 01               227 	mov	a,#1		; enable autovector, disable breakpoint logic
   0035 F0                  228 	movx	@dptr,a
   0036 E4                  229 	clr	a
   0037 90 7F E0            230 	mov	dptr,#INISOVAL
   003A F0                  231 	movx	@dptr,a
   003B 90 7F E1            232 	mov	dptr,#OUTISOVAL
   003E F0                  233 	movx	@dptr,a
   003F 90 7F DD            234 	mov	dptr,#USBPAIR
   0042 F0                  235 	movx	@dptr,a
   0043 90 7F DE            236 	mov	dptr,#IN07VAL
   0046 F0                  237 	movx	@dptr,a
   0047 90 7F DF            238 	mov	dptr,#OUT07VAL
   004A F0                  239 	movx	@dptr,a
                            240 	;; USB:	clear endpoint toggles
   004B 90 7F D7            241 	mov	dptr,#TOGCTL
   004E 7F 07               242 	mov	r7,#7		; clear endpoint toggles 1-7
   0050 EF                  243 10$:	mov	a,r7
   0051 44 10               244 	orl	a,#0x10
   0053 F0                  245 	movx	@dptr,a
   0054 44 30               246 	orl	a,#0x30
   0056 F0                  247 	movx	@dptr,a		; clear EP r7 in toggle
   0057 EF                  248 	mov	a,r7
   0058 F0                  249 	movx	@dptr,a
   0059 44 20               250 	orl	a,#0x20
   005B F0                  251 	movx	@dptr,a		; clear EP r7 out toggle
   005C DF F2               252 	djnz	r7,10$
                            253 	;; configure IO ports
   005E 90 7F 93            254 	mov	dptr,#PORTACFG
   0061 74 00               255 	mov	a,#0
   0063 F0                  256 	movx	@dptr,a
   0064 90 7F 96            257 	mov	dptr,#OUTA
   0067 74 80               258 	mov	a,#0x80		; set PROG lo
   0069 F0                  259 	movx	@dptr,a
   006A 90 7F 9C            260 	mov	dptr,#OEA
   006D 74 C2               261 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   006F F0                  262 	movx	@dptr,a
   0070 90 7F 94            263 	mov	dptr,#PORTBCFG
   0073 74 00               264 	mov	a,#0
   0075 F0                  265 	movx	@dptr,a
   0076 90 7F 9D            266 	mov	dptr,#OEB
   0079 74 00               267 	mov	a,#0
   007B F0                  268 	movx	@dptr,a
   007C 90 7F 95            269 	mov	dptr,#PORTCCFG
   007F 74 00               270 	mov	a,#0
   0081 F0                  271 	movx	@dptr,a
   0082 90 7F 98            272 	mov	dptr,#OUTC
   0085 74 08               273 	mov	a,#0x08
   0087 F0                  274 	movx	@dptr,a
   0088 90 7F 9E            275 	mov	dptr,#OEC
   008B 74 2E               276 	mov	a,#0x2e		; out: LEDCON,LEDSTA,TCK,INIT  in: TDO
   008D F0                  277 	movx	@dptr,a
                            278 	;; wait 1 sec
   008E 7A 64               279 	mov	r2,#100
                            280 	;; 10ms delay loop
   0090 90 E8 90            281 0$:	mov	dptr,#-6000
   0093 A3                  282 1$:	inc	dptr		; 3 cycles
   0094 E5 82               283 	mov	a,dpl0		; 2 cycles
   0096 45 83               284 	orl	a,dph0		; 2 cycles
   0098 70 F9               285 	jnz	1$		; 3 cycles
   009A DA F4               286 	djnz	r2,0$
                            287 	;; reconnect to USB bus
   009C 90 7F D6            288 	mov	dptr,#USBCS
   009F 74 04               289 	mov	a,#4
   00A1 F0                  290 	movx	@dptr,a
                            291 	;; light green led
   00A2 90 7F 98            292 	mov	dptr,#OUTC
   00A5 74 20               293 	mov	a,#0x20
   00A7 F0                  294 	movx	@dptr,a
                            295 
                    0000    296         .if 0
                            297         ;; light green led
                            298         mov     dptr,#OUTC
                            299         mov     a,#0x20
                            300         movx    @dptr,a
                            301 100$:   sjmp    100$
                            302         .else
                            303         ;; light green led
   00A8 90 7F 98            304 100$:   mov     dptr,#OUTC
   00AB 74 20               305         mov     a,#0x20
   00AD F0                  306         movx    @dptr,a
                            307         ;; wait 400ms
   00AE 7A 28               308         mov     r2,#40
                            309         ;; 10ms delay loop
   00B0 90 E8 90            310 101$:   mov     dptr,#(-6000)&0xffff
   00B3 A3                  311 102$:   inc     dptr            ; 3 cycles
   00B4 E5 82               312         mov     a,dpl0          ; 2 cycles
   00B6 45 83               313         orl     a,dph0          ; 2 cycles
   00B8 70 F9               314         jnz     102$            ; 3 cycles
   00BA DA F4               315         djnz    r2,101$
                            316         ;; switch green led off
   00BC 90 7F 98            317         mov     dptr,#OUTC
   00BF 74 28               318         mov     a,#0x28
   00C1 F0                  319         movx    @dptr,a
                            320         ;; wait 100ms
   00C2 7A 0A               321         mov     r2,#10
                            322         ;; 10ms delay loop
   00C4 90 E8 90            323 103$:   mov     dptr,#(-6000)&0xffff
   00C7 A3                  324 104$:   inc     dptr            ; 3 cycles
   00C8 E5 82               325         mov     a,dpl0          ; 2 cycles
   00CA 45 83               326         orl     a,dph0          ; 2 cycles
   00CC 70 F9               327         jnz     104$            ; 3 cycles
   00CE DA F4               328         djnz    r2,103$
   00D0 80 D6               329         sjmp    100$
                            330         .endif
                            331         
                            332 	.area	OSEG (OVR,DATA)
                            333 	.area	BSEG (BIT)
                            334 	.area	ISEG (DATA)
                            335 	.area	DSEG (DATA)
                            336 	.area	XSEG (DATA)
