                              1 	.module main
                              2 	
                              3 	;; same as audiofirmware, but do not announce as
                              4 	;; audio class compliant
                              5 	
                              6 	;; ENDPOINTS
                              7 	;; EP0 in/out   Control
                              8 	;; EP1 in       Interrupt:  Status
                              9 	;;              Byte 0: Modem Status
                             10 	;;                Bit 0-1: Transmitter status
                             11 	;;                         0: idle (off)
                             12 	;;                         1: keyup
                             13 	;;                         2: transmitting packets
                             14 	;;                         3: tail
                             15 	;;                Bit 2:   PTT status (1=on)
                             16 	;;                Bit 3:   DCD
                             17 	;;                Bit 5:   UART transmitter empty
                             18 	;;                Bit 6-7: unused
                             19 	;;              Byte 1: Number of empty 64 byte chunks in TX fifo (sofcount)
                             20 	;;              Byte 2: Number of full 64 byte chunks in RX fifo (INISOVAL)
                             21 	;;              Byte 3: RSSI value
                             22 	;;              Byte 4:	IRQ count
                             23 	;;              Byte 5-20: (as needed) UART receiver chars
                             24 	;; EP8 out      audio output
                             25 	;; EP8 in       audio input
                             26 
                             27 	;; COMMAND LIST
                             28 	;; C0 C0  read status (max. 23 bytes, first 6 same as EP1 in)
                             29 	;; C0 C8  read mode
                             30 	;;          Return:
                             31 	;;            Byte 0: 4 (MODE_AUDIO)
                             32 	;; C0 C9  return serial number string
                             33 	;; C0 D0  get/set PTT/DCD/RSSI
                             34 	;;          wIndex = 1:	set forced ptt to wValue
                             35 	;;          Return:
                             36 	;;            Byte 0: PTT status
                             37 	;;            Byte 1: DCD status
                             38 	;;            Byte 2: RSSI status
                             39 	;; 40 D2  set CON/STA led
                             40 	;;          Bits 0-1 of wValue
                             41 	;; 40 D3  send byte to UART
                             42 	;;          Byte in wValue
                             43 	;; C0 D4  get/set modem disconnect port (only if internal modem used, stalls otherwise)
                             44 	;;          wIndex = 1:	write wValue to output register
                             45 	;;          wIndex = 2:	write wValue to tristate mask register (1 = input, 0 = output)
                             46 	;;          Return:
                             47 	;;            Byte 0: Modem Disconnect Input
                             48 	;;            Byte 1: Modem Disconnect Output register
                             49 	;;            Byte 2: Modem Disconnect Tristate register
                             50 	;; C0 D5  get/set T7F port
                             51 	;;          wIndex = 1:	write wValue to T7F output register
                             52 	;;          Return:
                             53 	;;            Byte 0: T7F Input
                             54 	;;            Byte 1: T7F Output register
                             55 	;; C0 E0  get/set control register/counter values
                             56 	;;          wIndex = 1:	write wValue to control register
                             57 	;;          Return:
                             58 	;;            Byte 0: control register value
                             59 	;;            Byte 1-3: counter0 register
                             60 	;;            Byte 4-6: counter1 register
                             61 	;; C0 E1  get debug status
                             62 	;;          Return:
                             63 	;;            Byte 0: SOF count
                             64 	;;            Byte 1: framesize
                             65 	;;            Byte 2-3: divider
                             66 	;;            Byte 4: divrel
                             67 	;;            Byte 5: pllcorrvar
                             68 	;;            Byte 6: txfifocount
                             69 	;; C0 E2  xxdebug stuff
                             70 	
                             71 	;; define code segments link order
                             72 	.area CODE (CODE)
                             73 	.area CSEG (CODE)
                             74 	.area GSINIT (CODE)
                             75 	.area GSINIT2 (CODE)
                             76 
                             77 	;; -----------------------------------------------------
                             78 
                             79 	;; special function registers (which are not predefined)
                    0082     80 	dpl0    = 0x82
                    0083     81 	dph0    = 0x83
                    0084     82 	dpl1    = 0x84
                    0085     83 	dph1    = 0x85
                    0086     84 	dps     = 0x86
                    008E     85 	ckcon   = 0x8E
                    008F     86 	spc_fnc = 0x8F
                    0091     87 	exif    = 0x91
                    0092     88 	mpage   = 0x92
                    0098     89 	scon0   = 0x98
                    0099     90 	sbuf0   = 0x99
                    00C0     91 	scon1   = 0xC0
                    00C1     92 	sbuf1   = 0xC1
                    00D8     93 	eicon   = 0xD8
                    00E8     94 	eie     = 0xE8
                    00F8     95 	eip     = 0xF8
                             96 
                             97 	;; anchor xdata registers
                    7F00     98 	IN0BUF		= 0x7F00
                    7EC0     99 	OUT0BUF		= 0x7EC0
                    7E80    100 	IN1BUF		= 0x7E80
                    7E40    101 	OUT1BUF		= 0x7E40
                    7E00    102 	IN2BUF		= 0x7E00
                    7DC0    103 	OUT2BUF		= 0x7DC0
                    7D80    104 	IN3BUF		= 0x7D80
                    7D40    105 	OUT3BUF		= 0x7D40
                    7D00    106 	IN4BUF		= 0x7D00
                    7CC0    107 	OUT4BUF		= 0x7CC0
                    7C80    108 	IN5BUF		= 0x7C80
                    7C40    109 	OUT5BUF		= 0x7C40
                    7C00    110 	IN6BUF		= 0x7C00
                    7BC0    111 	OUT6BUF		= 0x7BC0
                    7B80    112 	IN7BUF		= 0x7B80
                    7B40    113 	OUT7BUF		= 0x7B40
                    7FE8    114 	SETUPBUF	= 0x7FE8
                    7FE8    115 	SETUPDAT	= 0x7FE8
                            116 
                    7FB4    117 	EP0CS		= 0x7FB4
                    7FB5    118 	IN0BC		= 0x7FB5
                    7FB6    119 	IN1CS		= 0x7FB6
                    7FB7    120 	IN1BC		= 0x7FB7
                    7FB8    121 	IN2CS		= 0x7FB8
                    7FB9    122 	IN2BC		= 0x7FB9
                    7FBA    123 	IN3CS		= 0x7FBA
                    7FBB    124 	IN3BC		= 0x7FBB
                    7FBC    125 	IN4CS		= 0x7FBC
                    7FBD    126 	IN4BC		= 0x7FBD
                    7FBE    127 	IN5CS		= 0x7FBE
                    7FBF    128 	IN5BC		= 0x7FBF
                    7FC0    129 	IN6CS		= 0x7FC0
                    7FC1    130 	IN6BC		= 0x7FC1
                    7FC2    131 	IN7CS		= 0x7FC2
                    7FC3    132 	IN7BC		= 0x7FC3
                    7FC5    133 	OUT0BC		= 0x7FC5
                    7FC6    134 	OUT1CS		= 0x7FC6
                    7FC7    135 	OUT1BC		= 0x7FC7
                    7FC8    136 	OUT2CS		= 0x7FC8
                    7FC9    137 	OUT2BC		= 0x7FC9
                    7FCA    138 	OUT3CS		= 0x7FCA
                    7FCB    139 	OUT3BC		= 0x7FCB
                    7FCC    140 	OUT4CS		= 0x7FCC
                    7FCD    141 	OUT4BC		= 0x7FCD
                    7FCE    142 	OUT5CS		= 0x7FCE
                    7FCF    143 	OUT5BC		= 0x7FCF
                    7FD0    144 	OUT6CS		= 0x7FD0
                    7FD1    145 	OUT6BC		= 0x7FD1
                    7FD2    146 	OUT7CS		= 0x7FD2
                    7FD3    147 	OUT7BC		= 0x7FD3
                            148 
                    7FA8    149 	IVEC		= 0x7FA8
                    7FA9    150 	IN07IRQ		= 0x7FA9
                    7FAA    151 	OUT07IRQ	= 0x7FAA
                    7FAB    152 	USBIRQ		= 0x7FAB
                    7FAC    153 	IN07IEN		= 0x7FAC
                    7FAD    154 	OUT07IEN	= 0x7FAD
                    7FAE    155 	USBIEN		= 0x7FAE
                    7FAF    156 	USBBAV		= 0x7FAF
                    7FB2    157 	BPADDRH		= 0x7FB2
                    7FB3    158 	BPADDRL		= 0x7FB3
                            159 
                    7FD4    160 	SUDPTRH		= 0x7FD4
                    7FD5    161 	SUDPTRL		= 0x7FD5
                    7FD6    162 	USBCS		= 0x7FD6
                    7FD7    163 	TOGCTL		= 0x7FD7
                    7FD8    164 	USBFRAMEL	= 0x7FD8
                    7FD9    165 	USBFRAMEH	= 0x7FD9
                    7FDB    166 	FNADDR		= 0x7FDB
                    7FDD    167 	USBPAIR		= 0x7FDD
                    7FDE    168 	IN07VAL		= 0x7FDE
                    7FDF    169 	OUT07VAL	= 0x7FDF
                    7FE3    170 	AUTOPTRH	= 0x7FE3
                    7FE4    171 	AUTOPTRL	= 0x7FE4
                    7FE5    172 	AUTODATA	= 0x7FE5
                            173 
                            174 	;; isochronous endpoints. only available if ISODISAB=0
                            175 
                    7F60    176 	OUT8DATA	= 0x7F60
                    7F61    177 	OUT9DATA	= 0x7F61
                    7F62    178 	OUT10DATA	= 0x7F62
                    7F63    179 	OUT11DATA	= 0x7F63
                    7F64    180 	OUT12DATA	= 0x7F64
                    7F65    181 	OUT13DATA	= 0x7F65
                    7F66    182 	OUT14DATA	= 0x7F66
                    7F67    183 	OUT15DATA	= 0x7F67
                            184 
                    7F68    185 	IN8DATA		= 0x7F68
                    7F69    186 	IN9DATA		= 0x7F69
                    7F6A    187 	IN10DATA	= 0x7F6A
                    7F6B    188 	IN11DATA	= 0x7F6B
                    7F6C    189 	IN12DATA	= 0x7F6C
                    7F6D    190 	IN13DATA	= 0x7F6D
                    7F6E    191 	IN14DATA	= 0x7F6E
                    7F6F    192 	IN15DATA	= 0x7F6F
                            193 
                    7F70    194 	OUT8BCH		= 0x7F70
                    7F71    195 	OUT8BCL		= 0x7F71
                    7F72    196 	OUT9BCH		= 0x7F72
                    7F73    197 	OUT9BCL		= 0x7F73
                    7F74    198 	OUT10BCH	= 0x7F74
                    7F75    199 	OUT10BCL	= 0x7F75
                    7F76    200 	OUT11BCH	= 0x7F76
                    7F77    201 	OUT11BCL	= 0x7F77
                    7F78    202 	OUT12BCH	= 0x7F78
                    7F79    203 	OUT12BCL	= 0x7F79
                    7F7A    204 	OUT13BCH	= 0x7F7A
                    7F7B    205 	OUT13BCL	= 0x7F7B
                    7F7C    206 	OUT14BCH	= 0x7F7C
                    7F7D    207 	OUT14BCL	= 0x7F7D
                    7F7E    208 	OUT15BCH	= 0x7F7E
                    7F7F    209 	OUT15BCL	= 0x7F7F
                            210 
                    7FF0    211 	OUT8ADDR	= 0x7FF0
                    7FF1    212 	OUT9ADDR	= 0x7FF1
                    7FF2    213 	OUT10ADDR	= 0x7FF2
                    7FF3    214 	OUT11ADDR	= 0x7FF3
                    7FF4    215 	OUT12ADDR	= 0x7FF4
                    7FF5    216 	OUT13ADDR	= 0x7FF5
                    7FF6    217 	OUT14ADDR	= 0x7FF6
                    7FF7    218 	OUT15ADDR	= 0x7FF7
                    7FF8    219 	IN8ADDR		= 0x7FF8
                    7FF9    220 	IN9ADDR		= 0x7FF9
                    7FFA    221 	IN10ADDR	= 0x7FFA
                    7FFB    222 	IN11ADDR	= 0x7FFB
                    7FFC    223 	IN12ADDR	= 0x7FFC
                    7FFD    224 	IN13ADDR	= 0x7FFD
                    7FFE    225 	IN14ADDR	= 0x7FFE
                    7FFF    226 	IN15ADDR	= 0x7FFF
                            227 
                    7FA0    228 	ISOERR		= 0x7FA0
                    7FA1    229 	ISOCTL		= 0x7FA1
                    7FA2    230 	ZBCOUNT		= 0x7FA2
                    7FE0    231 	INISOVAL	= 0x7FE0
                    7FE1    232 	OUTISOVAL	= 0x7FE1
                    7FE2    233 	FASTXFR		= 0x7FE2
                            234 
                            235 	;; CPU control registers
                            236 
                    7F92    237 	CPUCS		= 0x7F92
                            238 
                            239 	;; IO port control registers
                            240 
                    7F93    241 	PORTACFG	= 0x7F93
                    7F94    242 	PORTBCFG	= 0x7F94
                    7F95    243 	PORTCCFG	= 0x7F95
                    7F96    244 	OUTA		= 0x7F96
                    7F97    245 	OUTB		= 0x7F97
                    7F98    246 	OUTC		= 0x7F98
                    7F99    247 	PINSA		= 0x7F99
                    7F9A    248 	PINSB		= 0x7F9A
                    7F9B    249 	PINSC		= 0x7F9B
                    7F9C    250 	OEA		= 0x7F9C
                    7F9D    251 	OEB		= 0x7F9D
                    7F9E    252 	OEC		= 0x7F9E
                            253 
                            254 	;; I2C controller registers
                            255 
                    7FA5    256 	I2CS		= 0x7FA5
                    7FA6    257 	I2DAT		= 0x7FA6
                            258 
                            259 	;; Xilinx FPGA registers
                    C000    260 	AUDIORXFIFO	= 0xc000
                    C000    261 	AUDIOTXFIFO	= 0xc000
                    C001    262 	AUDIORXFIFOCNT	= 0xc001
                    C002    263 	AUDIOTXFIFOCNT	= 0xc002
                    C001    264 	AUDIODIVIDERLO	= 0xc001
                    C002    265 	AUDIODIVIDERHI	= 0xc002
                    C004    266 	AUDIORSSI	= 0xc004
                    C005    267 	AUDIOCNTLOW	= 0xc005
                    C006    268 	AUDIOCNTMID	= 0xc006
                    C007    269 	AUDIOCNTHIGH	= 0xc007
                    C008    270 	AUDIOCTRL	= 0xc008
                    C009    271 	AUDIOSTAT	= 0xc009
                    C00A    272 	AUDIOT7FOUT	= 0xc00a
                    C00B    273 	AUDIOT7FIN	= 0xc00b
                    C00C    274 	AUDIOMDISCTRIS	= 0xc00c
                    C00D    275 	AUDIOMDISCOUT	= 0xc00d
                    C00E    276 	AUDIOMDISCIN	= 0xc00e
                            277 
                    0001    278 	AUDIOCTRLPTT	= 0x01
                    0002    279 	AUDIOCTRLMUTE	= 0x02
                    0004    280 	AUDIOCTRLLEDPTT	= 0x04
                    0008    281 	AUDIOCTRLLEDDCD	= 0x08
                    0000    282 	AUDIOCTRLCNTRES	= 0x00
                    0010    283 	AUDIOCTRLCNTDIS	= 0x10
                    0040    284 	AUDIOCTRLCNTCK0	= 0x40
                    0050    285 	AUDIOCTRLCNTCK1	= 0x50
                    0060    286 	AUDIOCTRLCNTCK2	= 0x60
                    0070    287 	AUDIOCTRLCNTCK3	= 0x70
                    0080    288 	AUDIOCTRLCNTRD1	= 0x80
                            289 
                            290 	;; -----------------------------------------------------
                            291 
                            292 	.area CODE (CODE)
   0000 02 0D 74            293 	ljmp	startup
   0003 02 01 6D            294 	ljmp	int0_isr
   0006                     295 	.ds	5
   000B 02 01 8E            296 	ljmp	timer0_isr
   000E                     297 	.ds	5
   0013 02 01 AF            298 	ljmp	int1_isr
   0016                     299 	.ds	5
   001B 02 01 D0            300 	ljmp	timer1_isr
   001E                     301 	.ds	5
   0023 02 01 F1            302 	ljmp	ser0_isr
   0026                     303 	.ds	5
   002B 02 02 2F            304 	ljmp	timer2_isr
   002E                     305 	.ds	5
   0033 02 02 50            306 	ljmp	resume_isr
   0036                     307 	.ds	5
   003B 02 02 71            308 	ljmp	ser1_isr
   003E                     309 	.ds	5
   0043 02 01 00            310 	ljmp	usb_isr
   0046                     311 	.ds	5
   004B 02 02 94            312 	ljmp	i2c_isr
   004E                     313 	.ds	5
   0053 02 02 B9            314 	ljmp	int4_isr
   0056                     315 	.ds	5
   005B 02 02 DE            316 	ljmp	int5_isr
   005E                     317 	.ds	5
   0063 02 03 03            318 	ljmp	int6_isr
                            319 	
                            320 	;; Parameter block at 0xe0
   0066                     321 	.ds	0x7a
   00E0 08                  322 parframesize:	.db	8
   00E1 01                  323 parpttmute:	.db	1
                            324 
                            325 	;; Serial# string at 0xf0
   00E2                     326 	.ds	14
   00F0                     327 parserial:
   00F0 30 30 30 30 30 30   328 	.db	'0,'0,'0,'0,'0,'0,'0,'0,0
        30 30 00
   00F9                     329 	.ds	7
                            330 
   0100                     331 usb_isr:
   0100 02 03 24            332 	ljmp	usb_sudav_isr
   0103                     333 	.ds	1
   0104 02 07 D2            334 	ljmp	usb_sof_isr
   0107                     335 	.ds	1
   0108 02 09 1C            336 	ljmp	usb_sutok_isr
   010B                     337 	.ds	1
   010C 02 09 47            338 	ljmp	usb_suspend_isr
   010F                     339 	.ds	1
   0110 02 09 72            340 	ljmp	usb_usbreset_isr
   0113                     341 	.ds	1
   0114 32                  342 	reti
   0115                     343 	.ds	3
   0118 02 09 9D            344 	ljmp	usb_ep0in_isr
   011B                     345 	.ds	1
   011C 02 09 D8            346 	ljmp	usb_ep0out_isr
   011F                     347 	.ds	1
   0120 02 0A 6E            348 	ljmp	usb_ep1in_isr
   0123                     349 	.ds	1
   0124 02 0A 9C            350 	ljmp	usb_ep1out_isr
   0127                     351 	.ds	1
   0128 02 0A C7            352 	ljmp	usb_ep2in_isr
   012B                     353 	.ds	1
   012C 02 0A F2            354 	ljmp	usb_ep2out_isr
   012F                     355 	.ds	1
   0130 02 0B 1D            356 	ljmp	usb_ep3in_isr
   0133                     357 	.ds	1
   0134 02 0B 48            358 	ljmp	usb_ep3out_isr
   0137                     359 	.ds	1
   0138 02 0B 73            360 	ljmp	usb_ep4in_isr
   013B                     361 	.ds	1
   013C 02 0B 9E            362 	ljmp	usb_ep4out_isr
   013F                     363 	.ds	1
   0140 02 0B C9            364 	ljmp	usb_ep5in_isr
   0143                     365 	.ds	1
   0144 02 0B F4            366 	ljmp	usb_ep5out_isr
   0147                     367 	.ds	1
   0148 02 0C 1F            368 	ljmp	usb_ep6in_isr
   014B                     369 	.ds	1
   014C 02 0C 4A            370 	ljmp	usb_ep6out_isr
   014F                     371 	.ds	1
   0150 02 0C 75            372 	ljmp	usb_ep7in_isr
   0153                     373 	.ds	1
   0154 02 0C A0            374 	ljmp	usb_ep7out_isr
                            375 
                            376 	;; -----------------------------------------------------
                            377 
                    0004    378 	NUMINTERFACES = 4
                            379 	
                            380 	.area	OSEG (OVR,DATA)
                            381 	.area	BSEG (BIT)
   0000                     382 ctrl_ptt:	.ds	1
   0001                     383 ctrl_pttmute:	.ds	1
   0002                     384 ctrl_ledptt:	.ds	1
   0003                     385 ctrl_leddcd:	.ds	1	
   0004                     386 ctrl_cntmode0:	.ds	1
   0005                     387 ctrl_cntmode1:	.ds	1
   0006                     388 ctrl_cntmode2:	.ds	1
   0007                     389 ctrl_cntsel:	.ds	1
                    0020    390 ctrlreg		=	0x20	; ((ctrl_ptt/8)+0x20)
                            391 
   0008                     392 pttmute:	.ds	1
   0009                     393 uartempty:	.ds	1
                            394 
                            395 
                            396 	.area	ISEG (DATA)
   0080                     397 txsamples:	.ds	0x40
   00C0                     398 stack:		.ds	0x80-0x40
                            399 
                            400 	.area	DSEG (DATA)
   0040                     401 ctrlcode:	.ds	1
   0041                     402 ctrlcount:	.ds	2
   0043                     403 leddiv:		.ds	1
   0044                     404 irqcount:	.ds	1
   0045                     405 sofcount:	.ds	1
   0046                     406 divider:	.ds	2
   0048                     407 divrel:		.ds	1
   0049                     408 framesize:	.ds	1
   004A                     409 pllcorrvar:	.ds	1
   004B                     410 txfifocount:	.ds	1
   004C                     411 pttforce:	.ds	1
                            412 
                            413 	;; UART receiver
   004D                     414 uartbuf:	.ds	16
   005D                     415 uartwr:		.ds	1
   005E                     416 uartrd:		.ds	1
                            417 
                            418 	;; Port state
   005F                     419 t7fout:		.ds	1
   0060                     420 mdisctris:	.ds	1
   0061                     421 mdiscout:	.ds	1
                            422 	
                            423 	;; USB state
   0062                     424 numconfig:	.ds	1
   0063                     425 altsetting:	.ds	NUMINTERFACES
                            426 
                            427 	.area	XSEG (DATA)
   1000                     428 blah:	.ds	1
                            429 
                            430 
                            431 	.area	GSINIT (CODE)
                    0002    432 	ar2 = 0x02
                    0003    433 	ar3 = 0x03
                    0004    434 	ar4 = 0x04
                    0005    435 	ar5 = 0x05
                    0006    436 	ar6 = 0x06
                    0007    437 	ar7 = 0x07
                    0000    438 	ar0 = 0x00
                    0001    439 	ar1 = 0x01
                            440 
   0D74                     441 startup:
   0D74 75 81 C0            442 	mov	sp,#stack	; -1
   0D77 E4                  443 	clr	a
   0D78 F5 D0               444 	mov	psw,a
   0D7A F5 86               445 	mov	dps,a
                            446 	;lcall	__sdcc_external_startup
                            447 	;mov	a,dpl0
                            448 	;jz	__sdcc_init_data
                            449 	;ljmp	__sdcc_program_startup
   0D7C                     450 __sdcc_init_data:
                            451 
                            452 	.area	GSINIT2 (CODE)
   0D7C                     453 __sdcc_program_startup:
                            454 	;; assembler code startup
   0D7C E4                  455 	clr	a
   0D7D F5 44               456  	mov	irqcount,a
   0D7F F5 45               457 	mov	sofcount,a
   0D81 F5 4C               458 	mov	pttforce,a
   0D83 F5 5E               459 	mov	uartrd,a
   0D85 F5 5D               460 	mov	uartwr,a
   0D87 F5 86               461 	mov	dps,a
   0D89 D2 09               462 	setb	uartempty
                            463 	;; some indirect register setup
   0D8B 75 8E 30            464 	mov	ckcon,#0x30	; zero external wait states, to avoid chip bugs
                            465 	;; Timer setup:
                            466 	;; timer 0: LED blink    : T0M=0, CT0=0, MODE0=1
                            467 	;; timer 1: Baudrate gen : T1M=1, CT1=0, MODE1=2
   0D8E 75 89 21            468 	mov	tmod,#0x21
   0D91 75 88 55            469 	mov	tcon,#0x55	; INT0/INT1 edge
   0D94 75 8D 64            470 	mov	th1,#256-156	; 1200 bauds
   0D97 75 87 00            471 	mov	pcon,#0		; SMOD0=0
                            472 	;; init USB subsystem
   0D9A 74 00               473 	mov	a,#0x00		; IN8 FIFO at address 0x0000
   0D9C 90 7F F8            474 	mov	dptr,#IN8ADDR
   0D9F F0                  475 	movx	@dptr,a
   0DA0 74 04               476 	mov	a,#0x04		; OUT8 FIFO at address 0x0010
   0DA2 90 7F F0            477 	mov	dptr,#OUT8ADDR
   0DA5 F0                  478 	movx	@dptr,a
   0DA6 90 7F A1            479 	mov	dptr,#ISOCTL
   0DA9 E4                  480 	clr	a		; enable ISO endpoints
   0DAA F0                  481 	movx	@dptr,a
   0DAB 90 7F AF            482 	mov	dptr,#USBBAV
   0DAE 74 01               483 	mov	a,#1		; enable autovector, disable breakpoint logic
   0DB0 F0                  484 	movx	@dptr,a
   0DB1 74 01               485 	mov	a,#0x01		; enable ISO endpoint 8 for input/output
   0DB3 90 7F E0            486 	mov	dptr,#INISOVAL
   0DB6 F0                  487 	movx	@dptr,a
   0DB7 90 7F E1            488 	mov	dptr,#OUTISOVAL
   0DBA F0                  489 	movx	@dptr,a
   0DBB 90 7F DD            490 	mov	dptr,#USBPAIR
   0DBE 74 89               491 	mov	a,#0x89		; pair EP 2&3 for input & output, ISOSEND0
   0DC0 F0                  492 	movx	@dptr,a
   0DC1 90 7F DE            493 	mov	dptr,#IN07VAL
   0DC4 74 03               494 	mov	a,#0x3		; enable EP0+EP1
   0DC6 F0                  495 	movx	@dptr,a
   0DC7 90 7F DF            496 	mov	dptr,#OUT07VAL
   0DCA 74 01               497 	mov	a,#0x1		; enable EP0
   0DCC F0                  498 	movx	@dptr,a
                            499 	;; USB:	init endpoint toggles
   0DCD 90 7F D7            500 	mov	dptr,#TOGCTL
   0DD0 74 12               501 	mov	a,#0x12
   0DD2 F0                  502 	movx	@dptr,a
   0DD3 74 32               503 	mov	a,#0x32		; clear EP 2 in toggle
   0DD5 F0                  504 	movx	@dptr,a
   0DD6 74 02               505 	mov	a,#0x02
   0DD8 F0                  506 	movx	@dptr,a
   0DD9 74 22               507 	mov	a,#0x22		; clear EP 2 out toggle
   0DDB F0                  508 	movx	@dptr,a
                            509 	;; configure IO ports
   0DDC 90 7F 93            510 	mov	dptr,#PORTACFG
   0DDF 74 00               511 	mov	a,#0
   0DE1 F0                  512 	movx	@dptr,a
   0DE2 90 7F 96            513 	mov	dptr,#OUTA
   0DE5 74 82               514 	mov	a,#0x82		; set PROG hi
   0DE7 F0                  515 	movx	@dptr,a
   0DE8 90 7F 9C            516 	mov	dptr,#OEA
   0DEB 74 C2               517 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   0DED F0                  518 	movx	@dptr,a
   0DEE 90 7F 94            519 	mov	dptr,#PORTBCFG
   0DF1 74 00               520 	mov	a,#0
   0DF3 F0                  521 	movx	@dptr,a
   0DF4 90 7F 9D            522 	mov	dptr,#OEB
   0DF7 74 00               523 	mov	a,#0
   0DF9 F0                  524 	movx	@dptr,a
   0DFA 90 7F 95            525 	mov	dptr,#PORTCCFG
   0DFD 74 C3               526 	mov	a,#0xc3		; RD/WR/TXD0/RXD0 are special function pins
   0DFF F0                  527 	movx	@dptr,a
   0E00 90 7F 98            528 	mov	dptr,#OUTC
   0E03 74 28               529 	mov	a,#0x28
   0E05 F0                  530 	movx	@dptr,a
   0E06 90 7F 9E            531 	mov	dptr,#OEC
   0E09 74 2A               532 	mov	a,#0x2a		; out: LEDCON,LEDSTA,TCK  in: TDO,INIT
   0E0B F0                  533 	movx	@dptr,a
                            534 	;; enable interrupts
   0E0C 75 A8 92            535 	mov	ie,#0x92	; enable timer 0 and ser 0 int
   0E0F 75 E8 01            536 	mov	eie,#0x01	; enable USB interrupts
   0E12 90 7F AE            537 	mov	dptr,#USBIEN
   0E15 74 01               538 	mov	a,#1		; enable SUDAV interrupt
   0E17 F0                  539 	movx	@dptr,a
   0E18 90 7F AC            540 	mov	dptr,#IN07IEN
   0E1B 74 03               541 	mov	a,#3		; enable EP0+EP1 interrupt
   0E1D F0                  542 	movx	@dptr,a
   0E1E 90 7F AD            543 	mov	dptr,#OUT07IEN
   0E21 74 01               544 	mov	a,#1		; enable EP0 interrupt
   0E23 F0                  545 	movx	@dptr,a
                            546 	;; initialize UART 0 for T7F communication
   0E24 75 98 52            547 	mov	scon0,#0x52	; Mode 1, Timer 1, Receiver enable
                            548         ;; copy configuration to bit addressable variables
   0E27 75 20 00            549         mov     ctrlreg,#AUDIOCTRLCNTRES
   0E2A 78 E1               550         mov     r0,#parpttmute
   0E2C E2                  551         movx    a,@r0
   0E2D A2 E0               552         mov     c,acc.0
   0E2F 92 08               553         mov     pttmute,c
   0E31 92 01               554         mov     ctrl_pttmute,c
                            555 	;; turn off transmitter
   0E33 90 C0 08            556 	mov	dptr,#AUDIOCTRL
   0E36 E5 20               557 	mov	a,ctrlreg
   0E38 F0                  558 	movx	@dptr,a
                            559 	;; Initialize modem disc port / t7f port
   0E39 90 C0 0C            560 	mov	dptr,#AUDIOMDISCTRIS
   0E3C 74 FF               561 	mov	a,#0xff
   0E3E F0                  562 	movx	@dptr,a
   0E3F F5 60               563 	mov	mdisctris,a
   0E41 90 C0 0D            564 	mov	dptr,#AUDIOMDISCOUT
   0E44 E4                  565 	clr	a
   0E45 F0                  566 	movx	@dptr,a
   0E46 F5 61               567 	mov	mdiscout,a
   0E48 90 C0 0A            568 	mov	dptr,#AUDIOT7FOUT
   0E4B 74 1F               569 	mov	a,#0x1f
   0E4D F0                  570 	movx	@dptr,a
   0E4E F5 5F               571 	mov	t7fout,a
                            572 	;; Copy serial number
   0E50 78 F0               573 	mov	r0,#parserial
   0E52 90 0D 54            574 	mov	dptr,#stringserial+2
   0E55 E2                  575 1$:	movx	a,@r0
   0E56 60 06               576 	jz	2$
   0E58 F0                  577 	movx	@dptr,a
   0E59 A3                  578 	inc	dptr
   0E5A A3                  579 	inc	dptr
   0E5B 08                  580 	inc	r0
   0E5C 80 F7               581 	sjmp	1$
   0E5E E8                  582 2$:	mov	a,r0
   0E5F 24 11               583 	add	a,#1-0xf0	; 1-parserial
   0E61 25 E0               584 	add	a,acc
   0E63 90 0D 52            585 	mov	dptr,#stringserial
   0E66 F0                  586 	movx	@dptr,a
                            587 	;; check parameters
   0E67                     588 chkparam:
   0E67 75 48 80            589 	mov	divrel,#0x80
   0E6A 78 E0               590 	mov	r0,#parframesize
   0E6C E2                  591 	movx	a,@r0
   0E6D B4 02 02            592 	cjne	a,#02,11$
   0E70 80 4E               593 	sjmp	2$
   0E72 B4 03 02            594 11$:	cjne	a,#03,1$
   0E75 80 49               595 	sjmp	2$
   0E77 24 F8               596 1$:	add	a,#-8
   0E79 50 0B               597 	jnc	3$
   0E7B 24 F0               598 	add	a,#-16
   0E7D 40 03               599 	jc	4$
   0E7F E2                  600 	movx	a,@r0
   0E80 80 06               601 	sjmp	5$
                            602 	;; sampling rate is: 24000000Hz/(divider+2)
   0E82 74 18               603 4$:	mov	a,#24
   0E84 80 02               604 	sjmp	5$
   0E86 74 08               605 3$:	mov	a,#8
                            606 	;; 16 bit by 8 bit divide
                            607 	;; r2:	divisor
                            608 	;; r3:	loop counter
                            609 	;; r4:	dividend/result low
                            610 	;; r5:	dividend/result mid
                            611 	;; r6:	dividend/result high
   0E88 FA                  612 5$:	mov	r2,a
   0E89 7B 11               613 	mov	r3,#17
   0E8B 7C C0               614 	mov	r4,#24000
   0E8D 7D 5D               615 	mov	r5,#24000>>8
   0E8F 7E 00               616 	mov	r6,#0
   0E91 C3                  617 6$:	clr	c
   0E92 EE                  618 	mov	a,r6
   0E93 9A                  619 	subb	a,r2
   0E94 40 01               620 	jc	7$
   0E96 FE                  621 	mov	r6,a
   0E97 B3                  622 7$:	cpl	c
   0E98 EC                  623 	mov	a,r4
   0E99 33                  624 	rlc	a
   0E9A FC                  625 	mov	r4,a
   0E9B ED                  626 	mov	a,r5
   0E9C 33                  627 	rlc	a
   0E9D FD                  628 	mov	r5,a
   0E9E EE                  629 	mov	a,r6
   0E9F 33                  630 	rlc	a
   0EA0 FE                  631 	mov	r6,a
   0EA1 DB EE               632 	djnz	r3,6$
                            633 	;; subtract two
   0EA3 EC                  634 	mov	a,r4
   0EA4 C3                  635 	clr	c
   0EA5 94 02               636 	subb	a,#2
   0EA7 FC                  637 	mov	r4,a
   0EA8 ED                  638 	mov	a,r5
   0EA9 94 00               639 	subb	a,#0
   0EAB FD                  640 	mov	r5,a
                            641 	;; store result into audio divider
   0EAC 90 C0 02            642 	mov	dptr,#AUDIODIVIDERHI
   0EAF F0                  643 	movx	@dptr,a
   0EB0 EC                  644 	mov	a,r4
   0EB1 90 C0 01            645 	mov	dptr,#AUDIODIVIDERLO
   0EB4 F0                  646 	movx	@dptr,a
   0EB5 C3                  647 	clr	c
   0EB6 94 08               648 	subb	a,#0x08
   0EB8 F5 46               649 	mov	divider,a
   0EBA ED                  650 	mov	a,r5
   0EBB 94 00               651 	subb	a,#0
   0EBD F5 47               652 	mov	divider+1,a
                            653 	;; reload divider into accu
   0EBF EA                  654 	mov	a,r2
   0EC0 F5 49               655 2$:	mov	framesize,a
   0EC2 90 0D 0A            656 	mov	dptr,#descinframesize
   0EC5 F0                  657 	movx	@dptr,a
   0EC6 90 0D 13            658 	mov	dptr,#descoutframesize
   0EC9 F0                  659 	movx	@dptr,a
                            660 	;; set sampling rate in descriptor
                    0000    661 	.if 0
                            662 	mov	b,#1000
                            663 	mul	ab
                            664 	mov	dptr,#descinsrate
                            665 	movx	@dptr,a
                            666 	mov	dptr,#descoutsrate
                            667 	movx	@dptr,a
                            668 	mov	r3,b
                            669 	mov	a,framesize
                            670 	mov	b,#1000>>8
                            671 	mul	ab
                            672 	add	a,r3
                            673 	mov	dptr,#descinsrate+1
                            674 	movx	@dptr,a
                            675 	mov	dptr,#descoutsrate+1
                            676 	movx	@dptr,a
                            677 	clr	a
                            678 	addc	a,b
                            679 	mov	dptr,#descinsrate+2
                            680 	movx	@dptr,a
                            681 	mov	dptr,#descoutsrate+2
                            682 	movx	@dptr,a
                            683 	.endif
                            684 	;; initialize USB state
   0ECA                     685 usbinit:
                            686 
                    0000    687 	.if	0
                            688 	;;  XXXXX
                            689 	;; check if windows needs 11025Hz
                            690 	mov	a,#12
                            691 	mov	dptr,#descinframesize
                            692 	movx	@dptr,a
                            693 	mov	dptr,#descoutframesize
                            694 	movx	@dptr,a
                            695 	mov	a,#0x11
                            696 	mov	dptr,#descinsrate
                            697 	movx	@dptr,a
                            698 	mov	dptr,#descoutsrate
                            699 	movx	@dptr,a
                            700 	mov	a,#0x2b
                            701 	mov	dptr,#descinsrate+1
                            702 	movx	@dptr,a
                            703 	mov	dptr,#descoutsrate+1
                            704 	movx	@dptr,a
                            705 	mov	a,#0
                            706 	mov	dptr,#descinsrate+2
                            707 	movx	@dptr,a
                            708 	mov	dptr,#descoutsrate+2
                            709 	movx	@dptr,a
                            710 	;;  XXXXX
                            711 	.endif
                            712 	
   0ECA E4                  713 	clr	a
   0ECB F5 62               714 	mov	numconfig,a
   0ECD 78 63               715 	mov	r0,#altsetting
   0ECF 7A 04               716 	mov	r2,#NUMINTERFACES
   0ED1 F6                  717 3$:	mov	@r0,a
   0ED2 08                  718 	inc	r0
   0ED3 DA FC               719 	djnz	r2,3$
                            720 	;; give Windows a chance to finish the writecpucs control transfer
                            721 	;; 20ms delay loop
   0ED5 90 D1 20            722 	mov	dptr,#(-12000)&0xffff
   0ED8 A3                  723 2$:	inc	dptr		; 3 cycles
   0ED9 E5 82               724 	mov	a,dpl0		; 2 cycles
   0EDB 45 83               725 	orl	a,dph0		; 2 cycles
   0EDD 70 F9               726 	jnz	2$		; 3 cycles
                    0001    727 	.if	1
                            728 	;; disconnect from USB bus
   0EDF 90 7F D6            729 	mov	dptr,#USBCS
   0EE2 74 0A               730 	mov	a,#10
   0EE4 F0                  731 	movx	@dptr,a
                            732 	;; wait 0.3 sec
   0EE5 7A 1E               733 	mov	r2,#30
                            734 	;; 10ms delay loop
   0EE7 90 E8 90            735 0$:	mov	dptr,#(-6000)&0xffff
   0EEA A3                  736 1$:	inc	dptr            ; 3 cycles
   0EEB E5 82               737 	mov	a,dpl0          ; 2 cycles
   0EED 45 83               738 	orl	a,dph0          ; 2 cycles
   0EEF 70 F9               739 	jnz	1$              ; 3 cycles
   0EF1 DA F4               740 	djnz	r2,0$
                            741 	;; reconnect to USB bus
   0EF3 90 7F D6            742 	mov	dptr,#USBCS
                            743 	;mov	a,#2		; 8051 handles control
                            744 	;movx	@dptr,a
   0EF6 74 06               745 	mov	a,#6		; reconnect, 8051 handles control
   0EF8 F0                  746 	movx	@dptr,a
                            747 	.endif
                            748 
                            749 	;; final
   0EF9 12 0A 13            750 	lcall	fillusbintr
   0EFC                     751 fifoinit:
                            752 	;; first wait for a new frame
   0EFC 90 7F D8            753 	mov	dptr,#USBFRAMEL
   0EFF E0                  754 	movx	a,@dptr
   0F00 FA                  755 	mov	r2,a
   0F01 E0                  756 1$:	movx	a,@dptr
   0F02 B5 02 02            757 	cjne	a,ar2,2$
   0F05 80 FA               758 	sjmp	1$
   0F07 90 C0 01            759 2$:	mov	dptr,#AUDIORXFIFOCNT
   0F0A E0                  760 	movx	a,@dptr
   0F0B 24 FC               761 	add	a,#-4
   0F0D 54 3F               762 	anl	a,#0x3f
   0F0F 60 07               763 	jz	4$
   0F11 FA                  764 	mov	r2,a
   0F12 90 C0 00            765 	mov	dptr,#AUDIORXFIFO
   0F15 E0                  766 3$:	movx	a,@dptr
   0F16 DA FD               767 	djnz	r2,3$
   0F18 90 C0 02            768 4$:	mov	dptr,#AUDIOTXFIFOCNT
   0F1B E0                  769 	movx	a,@dptr
   0F1C 24 FC               770 	add	a,#-4
   0F1E 54 3F               771 	anl	a,#0x3f
   0F20 60 F6               772 	jz	4$
   0F22 FA                  773 	mov	r2,a
   0F23 90 C0 00            774 	mov	dptr,#AUDIOTXFIFO
   0F26 E4                  775 	clr	a
   0F27 F0                  776 5$:	movx	@dptr,a
   0F28 DA FD               777 	djnz	r2,5$
   0F2A                     778 6$:	;; clear SOFIR interrupt
   0F2A 90 7F AB            779 	mov	dptr,#USBIRQ
   0F2D 74 02               780 	mov	a,#0x02
   0F2F F0                  781 	movx	@dptr,a
                            782 	;; finally enable SOFIR interrupt
   0F30 90 7F AE            783 	mov	dptr,#USBIEN
   0F33 74 03               784 	mov	a,#3		; enable SUDAV+SOFIR interrupt
   0F35 F0                  785 	movx	@dptr,a	
   0F36 12 0A 13            786 	lcall	fillusbintr
   0F39 02 01 57            787 	ljmp	mainloop
                            788 
                            789 
                            790 	.area	CSEG (CODE)
                    0002    791 	ar2 = 0x02
                    0003    792 	ar3 = 0x03
                    0004    793 	ar4 = 0x04
                    0005    794 	ar5 = 0x05
                    0006    795 	ar6 = 0x06
                    0007    796 	ar7 = 0x07
                    0000    797 	ar0 = 0x00
                    0001    798 	ar1 = 0x01
                            799 
                            800 	;; WARNING!  The assembler doesn't check for
                            801 	;; out of range short jump labels!! Double check
                            802 	;; that the jump labels are within the range!
   0157                     803 mainloop:
   0157 E5 4C               804 	mov	a,pttforce
   0159 A2 E0               805 	mov	c,acc.0
   015B 92 00               806 	mov	ctrl_ptt,c
   015D 92 02               807 	mov	ctrl_ledptt,c
   015F B3                  808 	cpl	c
   0160 82 08               809 	anl	c,pttmute
   0162 92 01               810 	mov	ctrl_pttmute,c
   0164 90 C0 08            811 	mov	dptr,#AUDIOCTRL
   0167 E5 20               812 	mov	a,ctrlreg
   0169 F0                  813 	movx	@dptr,a
   016A 02 01 57            814 	ljmp	mainloop
                            815 
                            816 	;; ------------------ interrupt handlers ------------------------
                            817 
   016D                     818 int0_isr:
   016D C0 E0               819 	push	acc
   016F C0 F0               820 	push	b
   0171 C0 82               821 	push	dpl0
   0173 C0 83               822 	push	dph0
   0175 C0 D0               823 	push	psw
   0177 75 D0 00            824 	mov	psw,#0x00
   017A C0 86               825 	push	dps
   017C 75 86 00            826 	mov	dps,#0
                            827 	;; clear interrupt
   017F C2 89               828 	clr	tcon+1
                            829 	;; handle interrupt
                            830 	;; epilogue
   0181 D0 86               831 	pop	dps
   0183 D0 D0               832 	pop	psw
   0185 D0 83               833 	pop	dph0
   0187 D0 82               834 	pop	dpl0
   0189 D0 F0               835 	pop	b
   018B D0 E0               836 	pop	acc
   018D 32                  837 	reti
                            838 
   018E                     839 timer0_isr:
   018E C0 E0               840 	push	acc
   0190 C0 F0               841 	push	b
   0192 C0 82               842 	push	dpl0
   0194 C0 83               843 	push	dph0
   0196 C0 D0               844 	push	psw
   0198 75 D0 00            845 	mov	psw,#0x00
   019B C0 86               846 	push	dps
   019D 75 86 00            847 	mov	dps,#0
                            848 	;; clear interrupt
   01A0 C2 8D               849 	clr	tcon+5
                            850 	;; handle interrupt
                    0000    851 	.if	0
                            852 	inc	leddiv
                            853 	mov	a,leddiv
                            854 	anl	a,#7
                            855 	jnz	0$
                            856 	mov	dptr,#OUTC
                            857 	movx	a,@dptr
                            858 	xrl	a,#0x08
                            859 	movx	@dptr,a
                            860 0$:
                            861 	.endif
                            862 	;; epilogue
   01A2 D0 86               863 	pop	dps
   01A4 D0 D0               864 	pop	psw
   01A6 D0 83               865 	pop	dph0
   01A8 D0 82               866 	pop	dpl0
   01AA D0 F0               867 	pop	b
   01AC D0 E0               868 	pop	acc
   01AE 32                  869 	reti
                            870 
   01AF                     871 int1_isr:
   01AF C0 E0               872 	push	acc
   01B1 C0 F0               873 	push	b
   01B3 C0 82               874 	push	dpl0
   01B5 C0 83               875 	push	dph0
   01B7 C0 D0               876 	push	psw
   01B9 75 D0 00            877 	mov	psw,#0x00
   01BC C0 86               878 	push	dps
   01BE 75 86 00            879 	mov	dps,#0
                            880 	;; clear interrupt
   01C1 C2 8B               881 	clr	tcon+3
                            882 	;; handle interrupt
                            883 	;; epilogue
   01C3 D0 86               884 	pop	dps
   01C5 D0 D0               885 	pop	psw
   01C7 D0 83               886 	pop	dph0
   01C9 D0 82               887 	pop	dpl0
   01CB D0 F0               888 	pop	b
   01CD D0 E0               889 	pop	acc
   01CF 32                  890 	reti
                            891 
   01D0                     892 timer1_isr:
   01D0 C0 E0               893 	push	acc
   01D2 C0 F0               894 	push	b
   01D4 C0 82               895 	push	dpl0
   01D6 C0 83               896 	push	dph0
   01D8 C0 D0               897 	push	psw
   01DA 75 D0 00            898 	mov	psw,#0x00
   01DD C0 86               899 	push	dps
   01DF 75 86 00            900 	mov	dps,#0
                            901 	;; clear interrupt
   01E2 C2 8F               902 	clr	tcon+7
                            903 	;; handle interrupt
                            904 	;; epilogue
   01E4 D0 86               905 	pop	dps
   01E6 D0 D0               906 	pop	psw
   01E8 D0 83               907 	pop	dph0
   01EA D0 82               908 	pop	dpl0
   01EC D0 F0               909 	pop	b
   01EE D0 E0               910 	pop	acc
   01F0 32                  911 	reti
                            912 
   01F1                     913 ser0_isr:
   01F1 C0 E0               914 	push	acc
   01F3 C0 F0               915 	push	b
   01F5 C0 82               916 	push	dpl0
   01F7 C0 83               917 	push	dph0
   01F9 C0 D0               918 	push	psw
   01FB 75 D0 00            919 	mov	psw,#0x00
   01FE C0 86               920 	push	dps
   0200 75 86 00            921 	mov	dps,#0
   0203 C0 00               922 	push	ar0
                            923 	;; clear interrupt
   0205 10 98 16            924 	jbc	scon0+0,1$	; RI
   0208 10 99 0F            925 0$:	jbc	scon0+1,2$	; TI
                            926 	;; handle interrupt
                            927 	;; epilogue
   020B D0 00               928 3$:	pop	ar0
   020D D0 86               929 	pop	dps
   020F D0 D0               930 	pop	psw
   0211 D0 83               931 	pop	dph0
   0213 D0 82               932 	pop	dpl0
   0215 D0 F0               933 	pop	b
   0217 D0 E0               934 	pop	acc
   0219 32                  935 	reti
                            936 
   021A D2 09               937 2$:	setb	uartempty
   021C 80 EA               938 	sjmp	0$
                            939 
   021E E5 5D               940 1$:	mov	a,uartwr
   0220 24 4D               941 	add	a,#uartbuf
   0222 F8                  942 	mov	r0,a
   0223 E5 99               943 	mov	a,sbuf0
   0225 F6                  944 	mov	@r0,a
   0226 E5 5D               945 	mov	a,uartwr
   0228 04                  946 	inc	a
   0229 54 0F               947 	anl	a,#0xf
   022B F5 5D               948 	mov	uartwr,a
   022D 80 DC               949 	sjmp	3$
                            950 
   022F                     951 timer2_isr:
   022F C0 E0               952 	push	acc
   0231 C0 F0               953 	push	b
   0233 C0 82               954 	push	dpl0
   0235 C0 83               955 	push	dph0
   0237 C0 D0               956 	push	psw
   0239 75 D0 00            957 	mov	psw,#0x00
   023C C0 86               958 	push	dps
   023E 75 86 00            959 	mov	dps,#0
                            960 	;; clear interrupt
   0241 C2 CF               961 	clr	t2con+7
                            962 	;; handle interrupt
                            963 	;; epilogue
   0243 D0 86               964 	pop	dps
   0245 D0 D0               965 	pop	psw
   0247 D0 83               966 	pop	dph0
   0249 D0 82               967 	pop	dpl0
   024B D0 F0               968 	pop	b
   024D D0 E0               969 	pop	acc
   024F 32                  970 	reti
                            971 
   0250                     972 resume_isr:
   0250 C0 E0               973 	push	acc
   0252 C0 F0               974 	push	b
   0254 C0 82               975 	push	dpl0
   0256 C0 83               976 	push	dph0
   0258 C0 D0               977 	push	psw
   025A 75 D0 00            978 	mov	psw,#0x00
   025D C0 86               979 	push	dps
   025F 75 86 00            980 	mov	dps,#0
                            981 	;; clear interrupt
   0262 C2 DC               982 	clr	eicon+4
                            983 	;; handle interrupt
                            984 	;; epilogue
   0264 D0 86               985 	pop	dps
   0266 D0 D0               986 	pop	psw
   0268 D0 83               987 	pop	dph0
   026A D0 82               988 	pop	dpl0
   026C D0 F0               989 	pop	b
   026E D0 E0               990 	pop	acc
   0270 32                  991 	reti
                            992 
   0271                     993 ser1_isr:
   0271 C0 E0               994 	push	acc
   0273 C0 F0               995 	push	b
   0275 C0 82               996 	push	dpl0
   0277 C0 83               997 	push	dph0
   0279 C0 D0               998 	push	psw
   027B 75 D0 00            999 	mov	psw,#0x00
   027E C0 86              1000 	push	dps
   0280 75 86 00           1001 	mov	dps,#0
                           1002 	;; clear interrupt
   0283 C2 C0              1003 	clr	scon1+0
   0285 C2 C1              1004 	clr	scon1+1
                           1005 	;; handle interrupt
                           1006 	;; epilogue
   0287 D0 86              1007 	pop	dps
   0289 D0 D0              1008 	pop	psw
   028B D0 83              1009 	pop	dph0
   028D D0 82              1010 	pop	dpl0
   028F D0 F0              1011 	pop	b
   0291 D0 E0              1012 	pop	acc
   0293 32                 1013 	reti
                           1014 
   0294                    1015 i2c_isr:
   0294 C0 E0              1016 	push	acc
   0296 C0 F0              1017 	push	b
   0298 C0 82              1018 	push	dpl0
   029A C0 83              1019 	push	dph0
   029C C0 D0              1020 	push	psw
   029E 75 D0 00           1021 	mov	psw,#0x00
   02A1 C0 86              1022 	push	dps
   02A3 75 86 00           1023 	mov	dps,#0
                           1024 	;; clear interrupt
   02A6 E5 91              1025 	mov	a,exif
   02A8 C2 E5              1026 	clr	acc.5
   02AA F5 91              1027 	mov	exif,a
                           1028 	;; handle interrupt
                           1029 	;; epilogue
   02AC D0 86              1030 	pop	dps
   02AE D0 D0              1031 	pop	psw
   02B0 D0 83              1032 	pop	dph0
   02B2 D0 82              1033 	pop	dpl0
   02B4 D0 F0              1034 	pop	b
   02B6 D0 E0              1035 	pop	acc
   02B8 32                 1036 	reti
                           1037 
   02B9                    1038 int4_isr:
   02B9 C0 E0              1039 	push	acc
   02BB C0 F0              1040 	push	b
   02BD C0 82              1041 	push	dpl0
   02BF C0 83              1042 	push	dph0
   02C1 C0 D0              1043 	push	psw
   02C3 75 D0 00           1044 	mov	psw,#0x00
   02C6 C0 86              1045 	push	dps
   02C8 75 86 00           1046 	mov	dps,#0
                           1047 	;; clear interrupt
   02CB E5 91              1048 	mov	a,exif
   02CD C2 E6              1049 	clr	acc.6
   02CF F5 91              1050 	mov	exif,a
                           1051 	;; handle interrupt
                           1052 	;; epilogue
   02D1 D0 86              1053 	pop	dps
   02D3 D0 D0              1054 	pop	psw
   02D5 D0 83              1055 	pop	dph0
   02D7 D0 82              1056 	pop	dpl0
   02D9 D0 F0              1057 	pop	b
   02DB D0 E0              1058 	pop	acc
   02DD 32                 1059 	reti
                           1060 
   02DE                    1061 int5_isr:
   02DE C0 E0              1062 	push	acc
   02E0 C0 F0              1063 	push	b
   02E2 C0 82              1064 	push	dpl0
   02E4 C0 83              1065 	push	dph0
   02E6 C0 D0              1066 	push	psw
   02E8 75 D0 00           1067 	mov	psw,#0x00
   02EB C0 86              1068 	push	dps
   02ED 75 86 00           1069 	mov	dps,#0
                           1070 	;; clear interrupt
   02F0 E5 91              1071 	mov	a,exif
   02F2 C2 E7              1072 	clr	acc.7
   02F4 F5 91              1073 	mov	exif,a
                           1074 	;; handle interrupt
                           1075 	;; epilogue
   02F6 D0 86              1076 	pop	dps
   02F8 D0 D0              1077 	pop	psw
   02FA D0 83              1078 	pop	dph0
   02FC D0 82              1079 	pop	dpl0
   02FE D0 F0              1080 	pop	b
   0300 D0 E0              1081 	pop	acc
   0302 32                 1082 	reti
                           1083 
   0303                    1084 int6_isr:
   0303 C0 E0              1085 	push	acc
   0305 C0 F0              1086 	push	b
   0307 C0 82              1087 	push	dpl0
   0309 C0 83              1088 	push	dph0
   030B C0 D0              1089 	push	psw
   030D 75 D0 00           1090 	mov	psw,#0x00
   0310 C0 86              1091 	push	dps
   0312 75 86 00           1092 	mov	dps,#0
                           1093 	;; clear interrupt
   0315 C2 DB              1094 	clr	eicon+3
                           1095 	;; handle interrupt
                           1096 	;; epilogue
   0317 D0 86              1097 	pop	dps
   0319 D0 D0              1098 	pop	psw
   031B D0 83              1099 	pop	dph0
   031D D0 82              1100 	pop	dpl0
   031F D0 F0              1101 	pop	b
   0321 D0 E0              1102 	pop	acc
   0323 32                 1103 	reti
                           1104 
   0324                    1105 usb_sudav_isr:
   0324 C0 E0              1106 	push	acc
   0326 C0 F0              1107 	push	b
   0328 C0 82              1108 	push	dpl0
   032A C0 83              1109 	push	dph0
   032C C0 84              1110 	push	dpl1
   032E C0 85              1111 	push	dph1
   0330 C0 D0              1112 	push	psw
   0332 75 D0 00           1113 	mov	psw,#0x00
   0335 C0 86              1114 	push	dps
   0337 75 86 00           1115 	mov	dps,#0
   033A C0 00              1116 	push	ar0
   033C C0 07              1117 	push	ar7
                           1118 	;; clear interrupt
   033E E5 91              1119 	mov	a,exif
   0340 C2 E4              1120 	clr	acc.4
   0342 F5 91              1121 	mov	exif,a
   0344 90 7F AB           1122 	mov	dptr,#USBIRQ
   0347 74 01              1123 	mov	a,#0x01
   0349 F0                 1124 	movx	@dptr,a
                           1125 	;; handle interrupt
   034A 75 40 00           1126 	mov	ctrlcode,#0		; reset control out code
   034D 90 7F E9           1127 	mov	dptr,#SETUPDAT+1
   0350 E0                 1128 	movx	a,@dptr			; bRequest field
                           1129 	;; standard commands
                           1130 	;; USB_REQ_GET_DESCRIPTOR
   0351 B4 06 59           1131 	cjne	a,#USB_REQ_GET_DESCRIPTOR,cmdnotgetdesc
   0354 90 7F E8           1132 	mov	dptr,#SETUPDAT		; bRequestType == 0x80
   0357 E0                 1133 	movx	a,@dptr
   0358 B4 80 4F           1134 	cjne	a,#USB_DIR_IN,setupstallstd
   035B 90 7F EB           1135 	mov	dptr,#SETUPDAT+3
   035E E0                 1136 	movx	a,@dptr
   035F B4 01 0C           1137 	cjne	a,#USB_DT_DEVICE,cmdnotgetdescdev
   0362 90 7F D4           1138 	mov	dptr,#SUDPTRH
   0365 74 0C              1139 	mov	a,#>devicedescr
   0367 F0                 1140 	movx	@dptr,a
   0368 A3                 1141 	inc	dptr
   0369 74 CB              1142 	mov	a,#<devicedescr
   036B F0                 1143 	movx	@dptr,a
   036C 80 39              1144 	sjmp	setupackstd
   036E                    1145 cmdnotgetdescdev:
   036E B4 02 12           1146 	cjne	a,#USB_DT_CONFIG,cmdnotgetdesccfg
   0371 90 7F EA           1147 	mov	dptr,#SETUPDAT+2
   0374 E0                 1148 	movx	a,@dptr
   0375 70 33              1149 	jnz	setupstallstd
   0377 90 7F D4           1150 	mov	dptr,#SUDPTRH
   037A 74 0C              1151 	mov	a,#>config0descr
   037C F0                 1152 	movx	@dptr,a
   037D A3                 1153 	inc	dptr
   037E 74 DD              1154 	mov	a,#<config0descr
   0380 F0                 1155 	movx	@dptr,a
   0381 80 24              1156 	sjmp	setupackstd
   0383                    1157 cmdnotgetdesccfg:
   0383 B4 03 24           1158 	cjne	a,#USB_DT_STRING,setupstallstd
   0386 90 7F EA           1159 	mov	dptr,#SETUPDAT+2
   0389 E0                 1160 	movx	a,@dptr
   038A 24 FC              1161 	add	a,#-numstrings
   038C 40 1C              1162 	jc	setupstallstd
   038E E0                 1163 	movx	a,@dptr
   038F 25 E0              1164 	add	a,acc
   0391 24 18              1165 	add	a,#<stringdescr
   0393 F5 82              1166 	mov	dpl0,a
   0395 E4                 1167 	clr	a
   0396 34 0D              1168 	addc	a,#>stringdescr
   0398 F5 83              1169 	mov	dph0,a
   039A E0                 1170 	movx	a,@dptr
   039B F5 F0              1171 	mov	b,a
   039D A3                 1172 	inc	dptr
   039E E0                 1173 	movx	a,@dptr
   039F 90 7F D4           1174 	mov	dptr,#SUDPTRH
   03A2 F0                 1175 	movx	@dptr,a
   03A3 A3                 1176 	inc	dptr
   03A4 E5 F0              1177 	mov	a,b
   03A6 F0                 1178 	movx	@dptr,a
                           1179 	; sjmp	setupackstd	
   03A7                    1180 setupackstd:
   03A7 02 07 B7           1181 	ljmp	setupack
   03AA                    1182 setupstallstd:
   03AA 02 07 B3           1183 	ljmp	setupstall
   03AD                    1184 cmdnotgetdesc:
                           1185 	;; USB_REQ_SET_CONFIGURATION
   03AD B4 09 41           1186 	cjne	a,#USB_REQ_SET_CONFIGURATION,cmdnotsetconf
   03B0 90 7F E8           1187 	mov	dptr,#SETUPDAT
   03B3 E0                 1188 	movx	a,@dptr
   03B4 70 F4              1189 	jnz	setupstallstd
   03B6 90 7F EA           1190 	mov	dptr,#SETUPDAT+2
   03B9 E0                 1191 	movx	a,@dptr
   03BA 24 FE              1192 	add	a,#-2
   03BC 40 EC              1193 	jc	setupstallstd
   03BE E0                 1194 	movx	a,@dptr
   03BF F5 62              1195 	mov	numconfig,a
   03C1                    1196 cmdresettoggleshalt:
   03C1 90 7F D7           1197 	mov	dptr,#TOGCTL
   03C4 78 07              1198 	mov	r0,#7
   03C6 E8                 1199 0$:	mov	a,r0
   03C7 44 10              1200 	orl	a,#0x10
   03C9 F0                 1201 	movx	@dptr,a
   03CA 44 30              1202 	orl	a,#0x30
   03CC F0                 1203 	movx	@dptr,a
   03CD E8                 1204 	mov	a,r0
   03CE F0                 1205 	movx	@dptr,a
   03CF 44 20              1206 	orl	a,#0x20
   03D1 F0                 1207 	movx	@dptr,a
   03D2 D8 F2              1208 	djnz	r0,0$
   03D4 E4                 1209 	clr	a
   03D5 F0                 1210 	movx	@dptr,a
   03D6 74 02              1211 	mov	a,#2
   03D8 90 7F B6           1212 	mov	dptr,#IN1CS
   03DB 78 07              1213 	mov	r0,#7
   03DD F0                 1214 1$:	movx	@dptr,a
   03DE A3                 1215 	inc	dptr
   03DF A3                 1216 	inc	dptr
   03E0 D8 FB              1217 	djnz	r0,1$
   03E2 90 7F C6           1218 	mov	dptr,#OUT1CS
   03E5 78 07              1219 	mov	r0,#7
   03E7 F0                 1220 2$:	movx	@dptr,a
   03E8 A3                 1221 	inc	dptr
   03E9 A3                 1222 	inc	dptr
   03EA D8 FB              1223 	djnz	r0,2$
   03EC 12 0A 13           1224 	lcall	fillusbintr
   03EF 80 B6              1225 	sjmp	setupackstd
   03F1                    1226 cmdnotsetconf:
                           1227 	;; USB_REQ_SET_INTERFACE
   03F1 B4 0B 1E           1228 	cjne	a,#USB_REQ_SET_INTERFACE,cmdnotsetint
   03F4 90 7F E8           1229 	mov	dptr,#SETUPDAT
   03F7 E0                 1230 	movx	a,@dptr
   03F8 B4 01 AF           1231 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_OUT,setupstallstd
   03FB E5 62              1232 	mov	a,numconfig
   03FD B4 01 AA           1233 	cjne	a,#1,setupstallstd
   0400 90 7F EC           1234 	mov	dptr,#SETUPDAT+4
   0403 E0                 1235 	movx	a,@dptr
   0404 24 FC              1236 	add	a,#-NUMINTERFACES
   0406 40 A2              1237 	jc	setupstallstd
   0408 24 67              1238 	add	a,#NUMINTERFACES+altsetting
   040A F8                 1239 	mov	r0,a
   040B 90 7F EA           1240 	mov	dptr,#SETUPDAT+2
   040E E0                 1241 	movx	a,@dptr
   040F F6                 1242 	mov	@r0,a
   0410 80 AF              1243 	sjmp	cmdresettoggleshalt
   0412                    1244 cmdnotsetint:
                           1245 	;; USB_REQ_GET_INTERFACE
   0412 B4 0A 24           1246 	cjne	a,#USB_REQ_GET_INTERFACE,cmdnotgetint
   0415 90 7F E8           1247 	mov	dptr,#SETUPDAT
   0418 E0                 1248 	movx	a,@dptr
   0419 B4 81 8E           1249 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,setupstallstd
   041C E5 62              1250 	mov	a,numconfig
   041E B4 01 89           1251 	cjne	a,#1,setupstallstd
   0421 90 7F EC           1252 	mov	dptr,#SETUPDAT+4
   0424 E0                 1253 	movx	a,@dptr
   0425 24 FC              1254 	add	a,#-NUMINTERFACES
   0427 40 81              1255 	jc	setupstallstd
   0429 24 67              1256 	add	a,#NUMINTERFACES+altsetting
   042B F8                 1257 	mov	r0,a
   042C E6                 1258 	mov	a,@r0
   042D                    1259 cmdrespondonebyte:
   042D 90 7F 00           1260 	mov	dptr,#IN0BUF
   0430 F0                 1261 	movx	@dptr,a
   0431 90 7F B5           1262 	mov	dptr,#IN0BC
   0434 74 01              1263 	mov	a,#1
   0436 F0                 1264 	movx	@dptr,a	
   0437 80 4E              1265 	sjmp	setupackstd2
   0439                    1266 cmdnotgetint:
                           1267 	;; USB_REQ_GET_CONFIGURATION
   0439 B4 08 0B           1268 	cjne	a,#USB_REQ_GET_CONFIGURATION,cmdnotgetconf
   043C 90 7F E8           1269 	mov	dptr,#SETUPDAT
   043F E0                 1270 	movx	a,@dptr
   0440 B4 80 47           1271 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,setupstallstd2
   0443 E5 62              1272 	mov	a,numconfig
   0445 80 E6              1273 	sjmp	cmdrespondonebyte	
   0447                    1274 cmdnotgetconf:
                           1275 	;; USB_REQ_GET_STATUS (0)
   0447 70 44              1276 	jnz	cmdnotgetstat
   0449 90 7F E8           1277 	mov	dptr,#SETUPDAT
   044C E0                 1278 	movx	a,@dptr
   044D B4 80 11           1279 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,cmdnotgetstatdev
   0450 74 01              1280 	mov	a,#1
   0452                    1281 cmdrespondstat:
   0452 90 7F 00           1282 	mov	dptr,#IN0BUF
   0455 F0                 1283 	movx	@dptr,a
   0456 A3                 1284 	inc	dptr
   0457 E4                 1285 	clr	a
   0458 F0                 1286 	movx	@dptr,a
   0459 90 7F B5           1287 	mov	dptr,#IN0BC
   045C 74 02              1288 	mov	a,#2
   045E F0                 1289 	movx	@dptr,a	
   045F 80 26              1290 	sjmp	setupackstd2
   0461                    1291 cmdnotgetstatdev:
   0461 B4 81 03           1292 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,cmdnotgetstatintf
   0464 E4                 1293 	clr	a
   0465 80 EB              1294 	sjmp	cmdrespondstat
   0467                    1295 cmdnotgetstatintf:
   0467 B4 82 20           1296 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_IN,setupstallstd2
   046A 90 7F EC           1297 	mov	dptr,#SETUPDAT+4
   046D E0                 1298 	movx	a,@dptr
   046E 90 7F C4           1299 	mov	dptr,#OUT1CS-2
   0471 30 E7 03           1300 	jnb	acc.7,0$
   0474 90 7F B4           1301 	mov	dptr,#IN1CS-2
   0477 54 0F              1302 0$:	anl	a,#15
   0479 60 0F              1303 	jz	setupstallstd2
   047B 20 E3 0C           1304 	jb	acc.3,setupstallstd2
   047E 25 E0              1305 	add	a,acc
   0480 25 82              1306 	add	a,dpl0
   0482 F5 82              1307 	mov	dpl0,a
   0484 E0                 1308 	movx	a,@dptr
   0485 80 CB              1309 	sjmp	cmdrespondstat
   0487                    1310 setupackstd2:
   0487 02 07 B7           1311 	ljmp	setupack
   048A                    1312 setupstallstd2:
   048A 02 07 B3           1313 	ljmp	setupstall
   048D                    1314 cmdnotgetstat:
                           1315 	;; USB_REQ_SET_FEATURE
   048D B4 03 05           1316 	cjne	a,#USB_REQ_SET_FEATURE,cmdnotsetftr
   0490 75 F0 01           1317 	mov	b,#1
   0493 80 06              1318 	sjmp	handleftr
   0495                    1319 cmdnotsetftr:
                           1320 	;; USB_REQ_CLEAR_FEATURE
   0495 B4 01 44           1321 	cjne	a,#USB_REQ_CLEAR_FEATURE,cmdnotclrftr
   0498 75 F0 00           1322 	mov	b,#0
   049B                    1323 handleftr:
   049B 90 7F E8           1324 	mov	dptr,#SETUPDAT
   049E E0                 1325 	movx	a,@dptr
   049F B4 02 E8           1326 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_OUT,setupstallstd2
   04A2 A3                 1327 	inc	dptr
   04A3 A3                 1328 	inc	dptr
   04A4 E0                 1329 	movx	a,@dptr
   04A5 70 E3              1330 	jnz	setupstallstd2	; not ENDPOINT_HALT feature
   04A7 A3                 1331 	inc	dptr
   04A8 E0                 1332 	movx	a,@dptr
   04A9 70 DF              1333 	jnz	setupstallstd2
   04AB A3                 1334 	inc	dptr
   04AC E0                 1335 	movx	a,@dptr
   04AD 90 7F C4           1336 	mov	dptr,#OUT1CS-2
   04B0 30 E7 05           1337 	jnb	acc.7,0$
   04B3 90 7F B4           1338 	mov	dptr,#IN1CS-2
   04B6 44 10              1339 	orl	a,#0x10
   04B8 20 E3 CF           1340 0$:	jb	acc.3,setupstallstd2
                           1341 	;; clear data toggle
   04BB 54 1F              1342 	anl	a,#0x1f
   04BD 05 86              1343 	inc	dps
   04BF 90 7F D7           1344 	mov	dptr,#TOGCTL
   04C2 F0                 1345 	movx	@dptr,a
   04C3 44 20              1346 	orl	a,#0x20
   04C5 F0                 1347 	movx	@dptr,a
   04C6 54 0F              1348 	anl	a,#15
   04C8 F0                 1349 	movx	@dptr,a
   04C9 15 86              1350 	dec	dps	
                           1351 	;; clear/set ep halt feature
   04CB 25 E0              1352 	add	a,acc
   04CD 25 82              1353 	add	a,dpl0
   04CF F5 82              1354 	mov	dpl0,a
   04D1 E5 F0              1355 	mov	a,b
   04D3 F0                 1356 	movx	@dptr,a
   04D4 80 B1              1357 	sjmp	setupackstd2
                           1358 
   04D6                    1359 cmdnotc0_1:
   04D6 02 05 6E           1360 	ljmp	cmdnotc0
   04D9                    1361 setupstallc0_1:
   04D9 02 07 B3           1362 	ljmp	setupstall
                           1363 	
   04DC                    1364 cmdnotclrftr:
                           1365 	;; vendor specific commands
                           1366 	;; 0xc0
   04DC B4 C0 F7           1367 	cjne	a,#0xc0,cmdnotc0_1
   04DF 90 7F E8           1368 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   04E2 E0                 1369 	movx	a,@dptr
   04E3 B4 C0 F3           1370 	cjne	a,#0xc0,setupstallc0_1
                           1371 	;; fill status buffer
   04E6 E5 4C              1372 	mov	a,pttforce
   04E8 F5 F0              1373 	mov	b,a
   04EA A2 E0              1374 	mov	c,acc.0
   04EC 92 F2              1375 	mov	b.2,c
   04EE 90 C0 09           1376 	mov	dptr,#AUDIOSTAT
   04F1 E0                 1377 	movx	a,@dptr
   04F2 A2 E0              1378 	mov	c,acc.0
   04F4 B3                 1379 	cpl	c
   04F5 92 F3              1380 	mov	b.3,c
   04F7 A2 09              1381 	mov	c,uartempty
   04F9 92 F5              1382 	mov	b.5,c
   04FB E4                 1383 	clr	a
   04FC 90 7F 04           1384 	mov	dptr,#(IN0BUF+4)
   04FF F0                 1385 	movx	@dptr,a
                           1386 	;; bytewide elements
   0500 90 7F 00           1387 	mov	dptr,#(IN0BUF)
   0503 E5 F0              1388 	mov	a,b
   0505 F0                 1389 	movx	@dptr,a
   0506 E4                 1390 	clr	a
   0507 90 7F 01           1391 	mov	dptr,#(IN0BUF+1)
   050A F0                 1392 	movx	@dptr,a
   050B 90 7F 02           1393 	mov	dptr,#(IN0BUF+2)
   050E F0                 1394 	movx	@dptr,a
   050F 90 C0 04           1395 	mov	dptr,#AUDIORSSI
   0512 E0                 1396 	movx	a,@dptr
   0513 90 7F 03           1397 	mov	dptr,#(IN0BUF+3)
   0516 F0                 1398 	movx	@dptr,a
                           1399 	;; counter
   0517 05 44              1400 	inc	irqcount
   0519 E5 44              1401 	mov	a,irqcount
   051B 90 7F 05           1402 	mov	dptr,#(IN0BUF+5)
   051E F0                 1403 	movx	@dptr,a
                           1404 	;; additional fields (HDLC state mach)
   051F E4                 1405 	clr	a
   0520 A3                 1406 	inc	dptr
   0521 F0                 1407 	movx	@dptr,a
   0522 A3                 1408 	inc	dptr
   0523 F0                 1409 	movx	@dptr,a
   0524 A3                 1410 	inc	dptr
   0525 F0                 1411 	movx	@dptr,a
   0526 A3                 1412 	inc	dptr
   0527 F0                 1413 	movx	@dptr,a
   0528 A3                 1414 	inc	dptr
   0529 F0                 1415 	movx	@dptr,a
   052A A3                 1416 	inc	dptr
   052B F0                 1417 	movx	@dptr,a
   052C A3                 1418 	inc	dptr
   052D F0                 1419 	movx	@dptr,a
   052E A3                 1420 	inc	dptr
   052F F0                 1421 	movx	@dptr,a
   0530 A3                 1422 	inc	dptr
   0531 F0                 1423 	movx	@dptr,a
   0532 A3                 1424 	inc	dptr
   0533 F0                 1425 	movx	@dptr,a
   0534 A3                 1426 	inc	dptr
   0535 F0                 1427 	movx	@dptr,a
   0536 A3                 1428 	inc	dptr
   0537 F0                 1429 	movx	@dptr,a
                           1430 	;; FPGA registers
   0538 90 C0 01           1431 	mov	dptr,#AUDIORXFIFOCNT
   053B E0                 1432 	movx	a,@dptr
   053C 90 7F 12           1433 	mov	dptr,#(IN0BUF+18)
   053F F0                 1434 	movx	@dptr,a
   0540 90 C0 02           1435 	mov	dptr,#AUDIOTXFIFOCNT
   0543 E0                 1436 	movx	a,@dptr
   0544 90 7F 13           1437 	mov	dptr,#(IN0BUF+19)
   0547 F0                 1438 	movx	@dptr,a
   0548 E5 20              1439 	mov	a,ctrlreg
   054A A3                 1440 	inc	dptr
   054B F0                 1441 	movx	@dptr,a
   054C 90 C0 09           1442 	mov	dptr,#AUDIOSTAT
   054F E0                 1443 	movx	a,@dptr
   0550 90 7F 15           1444 	mov	dptr,#(IN0BUF+21)
   0553 F0                 1445 	movx	@dptr,a
                           1446 	;; Anchor Registers
   0554 90 7F C8           1447 	mov	dptr,#OUT2CS
   0557 E0                 1448 	movx	a,@dptr
   0558 90 7F 16           1449 	mov	dptr,#(IN0BUF+22)
   055B F0                 1450 	movx	@dptr,a
                           1451 	;; set length
   055C 90 7F EE           1452 	mov	dptr,#SETUPDAT+6	; wLength
   055F E0                 1453 	movx	a,@dptr
   0560 24 E9              1454 	add	a,#-(6+12+4+1)
   0562 50 01              1455 	jnc	4$
   0564 E4                 1456 	clr	a
   0565 24 17              1457 4$:	add	a,#(6+12+4+1)
   0567 90 7F B5           1458 	mov	dptr,#IN0BC
   056A F0                 1459 	movx	@dptr,a
   056B 02 07 B7           1460 	ljmp	setupack
   056E                    1461 cmdnotc0:
                           1462 	;; 0xc8
   056E B4 C8 19           1463 	cjne	a,#0xc8,cmdnotc8
   0571 90 7F E8           1464 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0574 E0                 1465 	movx	a,@dptr
   0575 B4 C0 0F           1466 	cjne	a,#0xc0,setupstallc8
   0578 74 04              1467 	mov	a,#4
   057A 90 7F 00           1468 	mov	dptr,#IN0BUF
   057D F0                 1469 	movx	@dptr,a
   057E 90 7F B5           1470 	mov	dptr,#IN0BC
   0581 74 01              1471 	mov	a,#1
   0583 F0                 1472 	movx	@dptr,a
   0584 02 07 B7           1473 	ljmp	setupack
   0587                    1474 setupstallc8:
   0587 02 07 B3           1475 	ljmp	setupstall
   058A                    1476 cmdnotc8:
                           1477 	;; 0xc9
   058A B4 C9 21           1478 	cjne	a,#0xc9,cmdnotc9
   058D 90 7F E8           1479 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0590 E0                 1480 	movx	a,@dptr
   0591 B4 C0 17           1481 	cjne	a,#0xc0,setupstallc9
   0594 90 7F 00           1482 	mov	dptr,#IN0BUF
   0597 78 F0              1483 	mov	r0,#parserial
   0599 E2                 1484 0$:	movx	a,@r0
   059A 60 05              1485 	jz	1$
   059C F0                 1486 	movx	@dptr,a
   059D 08                 1487 	inc	r0
   059E A3                 1488 	inc	dptr
   059F 80 F8              1489 	sjmp	0$
   05A1 E8                 1490 1$:	mov	a,r0
   05A2 24 10              1491 	add	a,#-0xf0	; -parserial
   05A4 90 7F B5           1492 	mov	dptr,#IN0BC
   05A7 F0                 1493 	movx	@dptr,a
   05A8 02 07 B7           1494 	ljmp	setupack
   05AB                    1495 setupstallc9:
   05AB 02 07 B3           1496 	ljmp	setupstall
   05AE                    1497 cmdnotc9:
                           1498 	;; 0xd0
   05AE B4 D0 45           1499 	cjne	a,#0xd0,cmdnotd0
   05B1 90 7F E8           1500 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   05B4 E0                 1501 	movx	a,@dptr
   05B5 B4 C0 3B           1502 	cjne	a,#0xc0,setupstalld0
   05B8 90 7F EC           1503 	mov	dptr,#SETUPDAT+4	; wIndex
   05BB E0                 1504 	movx	a,@dptr
   05BC B4 01 08           1505 	cjne	a,#1,0$
   05BF 90 7F EA           1506 	mov	dptr,#SETUPDAT+2	; wValue
   05C2 E0                 1507 	movx	a,@dptr
   05C3 54 01              1508 	anl	a,#1
   05C5 F5 4C              1509 	mov	pttforce,a
   05C7                    1510 0$:	;; PTT status
   05C7 90 7F 00           1511 	mov	dptr,#IN0BUF
   05CA E5 4C              1512 	mov	a,pttforce
   05CC F0                 1513 	movx	@dptr,a
                           1514 	;; DCD status
   05CD 90 C0 09           1515 	mov	dptr,#AUDIOSTAT
   05D0 E0                 1516 	movx	a,@dptr
   05D1 54 01              1517 	anl	a,#1
   05D3 64 01              1518 	xrl	a,#1
   05D5 90 7F 01           1519 	mov	dptr,#IN0BUF+1
   05D8 F0                 1520 	movx	@dptr,a
                           1521 	;; RSSI
   05D9 90 C0 04           1522 	mov	dptr,#AUDIORSSI
   05DC E0                 1523 	movx	a,@dptr
   05DD 90 7F 02           1524 	mov	dptr,#IN0BUF+2
   05E0 F0                 1525 	movx	@dptr,a
                           1526 	;; length
   05E1 90 7F EE           1527 	mov	dptr,#SETUPDAT+6	; wLength
   05E4 E0                 1528 	movx	a,@dptr
   05E5 24 FD              1529 	add	a,#-3
   05E7 50 01              1530 	jnc	2$
   05E9 E4                 1531 	clr	a
   05EA 24 03              1532 2$:	add	a,#3
   05EC 90 7F B5           1533 	mov	dptr,#IN0BC
   05EF F0                 1534 	movx	@dptr,a
   05F0 02 07 B7           1535 	ljmp	setupack
   05F3                    1536 setupstalld0:
   05F3 02 07 B3           1537 	ljmp	setupstall
   05F6                    1538 cmdnotd0:
                           1539 	;; 0xd2
   05F6 B4 D2 20           1540 	cjne	a,#0xd2,cmdnotd2
   05F9 90 7F E8           1541 	mov	dptr,#SETUPDAT	; bRequestType == 0x40
   05FC E0                 1542 	movx	a,@dptr
   05FD B4 40 16           1543 	cjne	a,#0x40,setupstalld2
   0600 90 7F EA           1544 	mov	dptr,#SETUPDAT+2	; wValue
   0603 E0                 1545 	movx	a,@dptr
   0604 F5 F0              1546 	mov	b,a
   0606 90 7F 98           1547 	mov	dptr,#OUTC
   0609 E0                 1548 	movx	a,@dptr
   060A A2 F0              1549 	mov	c,b.0
   060C 92 E3              1550 	mov	acc.3,c
   060E A2 F1              1551 	mov	c,b.1
   0610 92 E5              1552 	mov	acc.5,c
   0612 F0                 1553 	movx	@dptr,a
   0613 02 07 B7           1554 	ljmp	setupack
   0616                    1555 setupstalld2:
   0616 02 07 B3           1556 	ljmp	setupstall
   0619                    1557 cmdnotd2:
                           1558 	;; 0xd3
   0619 B4 D3 16           1559 	cjne	a,#0xd3,cmdnotd3
   061C 90 7F E8           1560 	mov	dptr,#SETUPDAT	; bRequestType == 0x40
   061F E0                 1561 	movx	a,@dptr
   0620 B4 40 07           1562 	cjne	a,#0x40,setupstalld3
   0623 90 7F EA           1563 	mov	dptr,#SETUPDAT+2	; wValue
   0626 E0                 1564 	movx	a,@dptr
   0627 10 09 03           1565 	jbc	uartempty,cmdd2cont
   062A                    1566 setupstalld3:
   062A 02 07 B3           1567 	ljmp	setupstall
   062D                    1568 cmdd2cont:
   062D F5 99              1569 	mov	sbuf0,a
   062F 02 07 B7           1570 	ljmp	setupack
   0632                    1571 cmdnotd3:
                           1572 	;; 0xd4
   0632 B4 D4 49           1573 	cjne	a,#0xd4,cmdnotd4
   0635 90 7F E8           1574 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0638 E0                 1575 	movx	a,@dptr
   0639 B4 C0 3F           1576 	cjne	a,#0xc0,setupstalld4
   063C 90 7F EC           1577 	mov	dptr,#SETUPDAT+4	; wIndex
   063F E0                 1578 	movx	a,@dptr
   0640 90 7F EA           1579 	mov	dptr,#SETUPDAT+2	; wValue
   0643 B4 01 09           1580 	cjne	a,#1,0$
   0646 E0                 1581 	movx	a,@dptr
   0647 90 C0 0D           1582 	mov	dptr,#AUDIOMDISCOUT
   064A F0                 1583 	movx	@dptr,a
   064B F5 61              1584 	mov	mdiscout,a
   064D 80 0A              1585 	sjmp	1$
   064F B4 02 07           1586 0$:	cjne	a,#2,1$
   0652 E0                 1587 	movx	a,@dptr
   0653 90 C0 0C           1588 	mov	dptr,#AUDIOMDISCTRIS
   0656 F0                 1589 	movx	@dptr,a
   0657 F5 60              1590 	mov	mdisctris,a
   0659 90 C0 0E           1591 1$:	mov	dptr,#AUDIOMDISCIN
   065C E0                 1592 	movx	a,@dptr
   065D 90 7F 00           1593 	mov	dptr,#IN0BUF+0
   0660 F0                 1594 	movx	@dptr,a
   0661 E5 61              1595 	mov	a,mdiscout
   0663 A3                 1596 	inc	dptr
   0664 F0                 1597 	movx	@dptr,a
   0665 E5 60              1598 	mov	a,mdisctris
   0667 A3                 1599 	inc	dptr
   0668 F0                 1600 	movx	@dptr,a
                           1601 	;; length
   0669 90 7F EE           1602 	mov	dptr,#SETUPDAT+6	; wLength
   066C E0                 1603 	movx	a,@dptr
   066D 24 FD              1604 	add	a,#-3
   066F 50 01              1605 	jnc	2$
   0671 E4                 1606 	clr	a
   0672 24 03              1607 2$:	add	a,#3
   0674 90 7F B5           1608 	mov	dptr,#IN0BC
   0677 F0                 1609 	movx	@dptr,a
   0678 02 07 B7           1610 	ljmp	setupack
   067B                    1611 setupstalld4:
   067B 02 07 B3           1612 	ljmp	setupstall
   067E                    1613 cmdnotd4:
                           1614 	;; 0xd5
   067E B4 D5 3F           1615 	cjne	a,#0xd5,cmdnotd5
   0681 90 7F E8           1616 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   0684 E0                 1617 	movx	a,@dptr
   0685 B4 C0 32           1618 	cjne	a,#0xc0,setupstalld5
   0688 90 7F EC           1619 	mov	dptr,#SETUPDAT+4	; wIndex
   068B E0                 1620 	movx	a,@dptr
   068C B4 01 0A           1621 	cjne	a,#1,0$
   068F 90 7F EA           1622 	mov	dptr,#SETUPDAT+2	; wValue
   0692 E0                 1623 	movx	a,@dptr
   0693 90 C0 0A           1624 	mov	dptr,#AUDIOT7FOUT
   0696 F0                 1625 	movx	@dptr,a
   0697 F5 5F              1626 	mov	t7fout,a
   0699 90 C0 0B           1627 0$:	mov	dptr,#AUDIOT7FIN
   069C E0                 1628 	movx	a,@dptr
   069D 90 7F 00           1629 	mov	dptr,#IN0BUF+0
   06A0 F0                 1630 	movx	@dptr,a
   06A1 E5 5F              1631 	mov	a,t7fout
   06A3 A3                 1632 	inc	dptr
   06A4 90 7F 01           1633 	mov	dptr,#IN0BUF+1
   06A7 F0                 1634 	movx	@dptr,a
                           1635 	;; length
   06A8 90 7F EE           1636 	mov	dptr,#SETUPDAT+6	; wLength
   06AB E0                 1637 	movx	a,@dptr
   06AC 24 FE              1638 	add	a,#-2
   06AE 50 01              1639 	jnc	2$
   06B0 E4                 1640 	clr	a
   06B1 24 02              1641 2$:	add	a,#2
   06B3 90 7F B5           1642 	mov	dptr,#IN0BC
   06B6 F0                 1643 	movx	@dptr,a
   06B7 02 07 B7           1644 	ljmp	setupack
   06BA                    1645 setupstalld5:
   06BA                    1646 setupstalle0:
   06BA 02 07 B3           1647 	ljmp	setupstall
   06BD                    1648 cmdnote0_0:
   06BD 02 07 53           1649 	ljmp    cmdnote0
   06C0                    1650 cmdnotd5:
                           1651 	;; 0xe0
   06C0 B4 E0 FA           1652 	cjne	a,#0xe0,cmdnote0_0
   06C3 90 7F E8           1653 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   06C6 E0                 1654 	movx	a,@dptr
   06C7 B4 C0 F0           1655 	cjne	a,#0xc0,setupstalle0
                           1656 	;; set PTT, LED's and counter mode
   06CA 90 7F EC           1657 	mov	dptr,#SETUPDAT+4	; wIndex
   06CD E0                 1658 	movx	a,@dptr
   06CE B4 01 12           1659 	cjne	a,#1,1$
   06D1 90 7F EA           1660 	mov	dptr,#SETUPDAT+2	; wValue
   06D4 E0                 1661 	movx	a,@dptr
   06D5 54 78              1662 	anl	a,#0x78
   06D7 C5 20              1663 	xch	a,ctrlreg
   06D9 54 07              1664 	anl	a,#0x07
   06DB 45 20              1665 	orl	a,ctrlreg
   06DD F5 20              1666 	mov	ctrlreg,a
   06DF 90 C0 08           1667 	mov	dptr,#AUDIOCTRL
   06E2 F0                 1668 	movx	@dptr,a
   06E3 90 7F EE           1669 1$:	mov	dptr,#SETUPDAT+6	; wLength
   06E6 E0                 1670 	movx	a,@dptr
   06E7 60 0D              1671 	jz	3$
   06E9 B4 01 11           1672 	cjne	a,#1,2$
   06EC E5 20              1673 	mov	a,ctrlreg
   06EE 90 7F 00           1674 	mov	dptr,#IN0BUF
   06F1 54 7F              1675 	anl	a,#0x7f
   06F3 F0                 1676 	movx	@dptr,a
   06F4 74 01              1677 	mov	a,#1
   06F6 90 7F B5           1678 3$:	mov	dptr,#IN0BC
   06F9 F0                 1679 	movx	@dptr,a
   06FA 02 07 B7           1680 	ljmp	setupack
   06FD 90 C0 08           1681 2$:	mov	dptr,#AUDIOCTRL
   0700 D2 07              1682 	setb	ctrl_cntsel
   0702 E5 20              1683 	mov	a,ctrlreg
   0704 F0                 1684 	movx	@dptr,a
   0705 90 C0 05           1685 	mov	dptr,#AUDIOCNTLOW
   0708 E0                 1686 	movx	a,@dptr
   0709 90 7F 04           1687 	mov	dptr,#IN0BUF+4
   070C F0                 1688 	movx	@dptr,a
   070D 90 C0 06           1689 	mov	dptr,#AUDIOCNTMID
   0710 E0                 1690 	movx	a,@dptr
   0711 90 7F 05           1691 	mov	dptr,#IN0BUF+5
   0714 F0                 1692 	movx	@dptr,a
   0715 90 C0 07           1693 	mov	dptr,#AUDIOCNTHIGH
   0718 E0                 1694 	movx	a,@dptr
   0719 90 7F 06           1695 	mov	dptr,#IN0BUF+6
   071C F0                 1696 	movx	@dptr,a
   071D 90 C0 08           1697 	mov	dptr,#AUDIOCTRL
   0720 C2 07              1698 	clr	ctrl_cntsel
   0722 E5 20              1699 	mov	a,ctrlreg
   0724 F0                 1700 	movx	@dptr,a
   0725 90 7F 00           1701 	mov	dptr,#IN0BUF
   0728 F0                 1702 	movx	@dptr,a
   0729 90 C0 05           1703 	mov	dptr,#AUDIOCNTLOW
   072C E0                 1704 	movx	a,@dptr
   072D 90 7F 01           1705 	mov	dptr,#IN0BUF+1
   0730 F0                 1706 	movx	@dptr,a
   0731 90 C0 06           1707 	mov	dptr,#AUDIOCNTMID
   0734 E0                 1708 	movx	a,@dptr
   0735 90 7F 02           1709 	mov	dptr,#IN0BUF+2
   0738 F0                 1710 	movx	@dptr,a
   0739 90 C0 07           1711 	mov	dptr,#AUDIOCNTHIGH
   073C E0                 1712 	movx	a,@dptr
   073D 90 7F 03           1713 	mov	dptr,#IN0BUF+3
   0740 F0                 1714 	movx	@dptr,a
                           1715 	;; length
   0741 90 7F EE           1716 	mov	dptr,#SETUPDAT+6	; wLength
   0744 E0                 1717 	movx	a,@dptr
   0745 24 F9              1718 	add	a,#-7
   0747 50 01              1719 	jnc	4$
   0749 E4                 1720 	clr	a
   074A 24 07              1721 4$:	add	a,#7
   074C 90 7F B5           1722 	mov	dptr,#IN0BC
   074F F0                 1723 	movx	@dptr,a
   0750 02 07 B7           1724 	ljmp	setupack
   0753                    1725 cmdnote0:
                           1726 	;; 0xe1
   0753 B4 E1 3A           1727 	cjne	a,#0xe1,cmdnote1
   0756 90 7F E8           1728 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   0759 E0                 1729 	movx	a,@dptr
   075A B4 C0 30           1730 	cjne	a,#0xc0,setupstalle1
   075D 90 7F 00           1731 	mov	dptr,#IN0BUF
   0760 E5 45              1732 	mov	a,sofcount
   0762 F0                 1733 	movx	@dptr,a
   0763 A3                 1734 	inc	dptr
   0764 E5 49              1735 	mov	a,framesize
   0766 F0                 1736 	movx	@dptr,a
   0767 A3                 1737 	inc	dptr
   0768 E5 46              1738 	mov	a,divider
   076A F0                 1739 	movx	@dptr,a
   076B A3                 1740 	inc	dptr
   076C E5 47              1741 	mov	a,divider+1
   076E F0                 1742 	movx	@dptr,a
   076F A3                 1743 	inc	dptr
   0770 E5 48              1744 	mov	a,divrel
   0772 F0                 1745 	movx	@dptr,a
   0773 A3                 1746 	inc	dptr
   0774 E5 4A              1747 	mov	a,pllcorrvar
   0776 F0                 1748 	movx	@dptr,a
   0777 A3                 1749 	inc	dptr
   0778 E5 4B              1750 	mov	a,txfifocount
   077A F0                 1751 	movx	@dptr,a
                           1752 
                           1753 	;mov	dptr,#AUDIOTXFIFOCNT
                           1754 	;movx	a,@dptr
                           1755 	;mov	dptr,#IN0BUF+6
                           1756 	;movx	@dptr,a
                           1757 
                           1758 	;; length
   077B 90 7F EE           1759 	mov	dptr,#SETUPDAT+6	; wLength
   077E E0                 1760 	movx	a,@dptr
   077F 24 F9              1761 	add	a,#-7
   0781 50 01              1762 	jnc	1$
   0783 E4                 1763 	clr	a
   0784 24 07              1764 1$:	add	a,#7
   0786 90 7F B5           1765 	mov	dptr,#IN0BC
   0789 F0                 1766 	movx	@dptr,a
   078A 02 07 B7           1767 	ljmp	setupack
   078D                    1768 setupstalle1:
   078D 02 07 B3           1769 	ljmp	setupstall
   0790                    1770 cmdnote1:
                           1771 	;; 0xe2
   0790 B4 E2 20           1772 	cjne	a,#0xe2,cmdnote2
   0793 90 7F E8           1773 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   0796 E0                 1774 	movx	a,@dptr
   0797 B4 C0 16           1775 	cjne	a,#0xc0,setupstalle2
   079A 90 7F 00           1776 	mov	dptr,#IN0BUF
   079D 78 80              1777 	mov	r0,#txsamples
   079F AF 49              1778 	mov	r7,framesize
   07A1 E6                 1779 1$:	mov	a,@r0
   07A2 F0                 1780 	movx	@dptr,a
   07A3 08                 1781 	inc	r0
   07A4 A3                 1782 	inc	dptr
   07A5 DF FA              1783 	djnz	r7,1$
   07A7 90 7F B5           1784 	mov	dptr,#IN0BC
   07AA E5 49              1785 	mov	a,framesize
   07AC F0                 1786 	movx	@dptr,a
   07AD 02 07 B7           1787 	ljmp	setupack
   07B0                    1788 setupstalle2:
   07B0 02 07 B3           1789 	ljmp	setupstall
   07B3                    1790 cmdnote2:
                           1791 
                           1792 	;; unknown commands fall through to setupstall
                           1793 
   07B3                    1794 setupstall:
   07B3 74 03              1795 	mov	a,#3
   07B5 80 02              1796 	sjmp	endsetup
   07B7                    1797 setupack:
   07B7 74 02              1798 	mov	a,#2
   07B9                    1799 endsetup:
   07B9 90 7F B4           1800 	mov	dptr,#EP0CS
   07BC F0                 1801 	movx	@dptr,a
   07BD                    1802 endusbisr:
                           1803 	;; epilogue
   07BD D0 07              1804 	pop	ar7
   07BF D0 00              1805 	pop	ar0
   07C1 D0 86              1806 	pop	dps
   07C3 D0 D0              1807 	pop	psw
   07C5 D0 85              1808 	pop	dph1
   07C7 D0 84              1809 	pop	dpl1
   07C9 D0 83              1810 	pop	dph0
   07CB D0 82              1811 	pop	dpl0
   07CD D0 F0              1812 	pop	b
   07CF D0 E0              1813 	pop	acc
   07D1 32                 1814 	reti
                           1815 
   07D2                    1816 usb_sof_isr:
   07D2 C0 E0              1817 	push	acc
   07D4 C0 F0              1818 	push	b
   07D6 C0 82              1819 	push	dpl0
   07D8 C0 83              1820 	push	dph0
   07DA C0 84              1821 	push	dpl1
   07DC C0 85              1822 	push	dph1
   07DE C0 D0              1823 	push	psw
   07E0 75 D0 00           1824 	mov	psw,#0x00
   07E3 C0 86              1825 	push	dps
   07E5 75 86 00           1826 	mov	dps,#0
   07E8 C0 02              1827 	push	ar2
   07EA C0 03              1828 	push	ar3
   07EC C0 00              1829 	push	ar0
                           1830 	;; clear interrupt
   07EE E5 91              1831 	mov	a,exif
   07F0 C2 E4              1832 	clr	acc.4
   07F2 F5 91              1833 	mov	exif,a
   07F4 90 7F AB           1834 	mov	dptr,#USBIRQ
   07F7 74 02              1835 	mov	a,#0x02
   07F9 F0                 1836 	movx	@dptr,a
                           1837 	;; handle interrupt
   07FA 05 45              1838 	inc	sofcount
                           1839 	;; debugging: we have two cases
                           1840 	;; if the ISO frame size is just 2 bytes,
                           1841 	;; we return the frame number, otherwise
                           1842 	;; the required numbers of samples
   07FC E5 49              1843 	mov	a,framesize
   07FE B4 02 14           1844 	cjne	a,#2,1$
                           1845 	;; copy the frame number
   0801 90 7F D8           1846 	mov	dptr,#USBFRAMEL
   0804 E0                 1847 	movx	a,@dptr
   0805 A3                 1848 	inc	dptr
   0806 05 86              1849 	inc	dps
   0808 90 7F 68           1850 	mov	dptr,#IN8DATA
   080B F0                 1851 	movx	@dptr,a
   080C 15 86              1852 	dec	dps
   080E E0                 1853 	movx	a,@dptr
   080F 05 86              1854 	inc	dps
   0811 F0                 1855 	movx	@dptr,a
   0812 02 09 05           1856 	ljmp	sofendisr
   0815 B4 03 36           1857 1$:	cjne	a,#3,sofsampleisr
   0818 90 7F A0           1858 	mov	dptr,#ISOERR
   081B E0                 1859 	movx	a,@dptr
   081C 20 E0 23           1860 	jb	acc.0,3$
   081F 90 7F 70           1861 	mov	dptr,#OUT8BCH
   0822 E0                 1862 	movx	a,@dptr
   0823 70 1D              1863 	jnz	3$
   0825 90 7F 71           1864 	mov	dptr,#OUT8BCL
   0828 E0                 1865 	movx	a,@dptr
   0829 B4 03 16           1866 	cjne	a,#3,3$
   082C FA                 1867 	mov	r2,a
   082D 90 7F 60           1868 	mov	dptr,#OUT8DATA
   0830 05 86              1869 	inc	dps
   0832 90 7F 68           1870 	mov	dptr,#IN8DATA
   0835 15 86              1871 	dec	dps
   0837 E0                 1872 2$:	movx	a,@dptr
   0838 05 86              1873 	inc	dps
   083A F0                 1874 	movx	@dptr,a
   083B 15 86              1875 	dec	dps
   083D DA F8              1876 	djnz	r2,2$
   083F 02 09 05           1877 	ljmp	sofendisr
   0842 7A 03              1878 3$:	mov	r2,#3
   0844 90 7F 68           1879 	mov	dptr,#IN8DATA
   0847 E4                 1880 	clr	a
   0848 F0                 1881 4$:	movx	@dptr,a
   0849 DA FD              1882 	djnz	r2,4$
   084B 02 09 05           1883 	ljmp	sofendisr
   084E                    1884 sofsampleisr:
   084E 90 C0 01           1885 	mov	dptr,#AUDIORXFIFOCNT
   0851 E0                 1886 	movx	a,@dptr
   0852 C3                 1887 	clr	c
   0853 95 49              1888 	subb	a,framesize
   0855 E0                 1889 	movx	a,@dptr
   0856 40 02              1890 	jc	1$
   0858 E5 49              1891 	mov	a,framesize
   085A FA                 1892 1$:	mov	r2,a
   085B FB                 1893 	mov	r3,a
   085C 60 12              1894 	jz	2$
   085E 90 C0 00           1895 	mov	dptr,#AUDIORXFIFO
   0861 05 86              1896 	inc	dps
   0863 90 7F 68           1897 	mov	dptr,#IN8DATA
   0866 15 86              1898 	dec	dps
   0868 E0                 1899 3$:	movx	a,@dptr
   0869 05 86              1900 	inc	dps
   086B F0                 1901 	movx	@dptr,a
   086C 15 86              1902 	dec	dps
   086E DA F8              1903 	djnz	r2,3$
   0870 E5 49              1904 2$:	mov	a,framesize
   0872 C3                 1905 	clr	c
   0873 9B                 1906 	subb	a,r3
   0874 60 08              1907 	jz	4$
   0876 FA                 1908 	mov	r2,a
   0877 90 7F 68           1909 	mov	dptr,#IN8DATA
   087A E4                 1910 	clr	a
   087B F0                 1911 5$:	movx	@dptr,a
   087C DA FD              1912 	djnz	r2,5$
                           1913 	;; sample rate adjustment
   087E 90 C0 01           1914 4$:	mov	dptr,#AUDIORXFIFOCNT
   0881 E0                 1915 	movx	a,@dptr
   0882 F5 4A              1916 	mov	pllcorrvar,a
   0884 24 FC              1917 	add	a,#-4
   0886 60 05              1918 	jz	10$		; no adjustment
   0888 33                 1919 	rlc	a
   0889 95 E0              1920 	subb	a,acc
   088B D2 E0              1921 	setb	acc.0
   088D FB                 1922 10$:	mov	r3,a		; r3 & acc contain the adjust value
   088E 25 48              1923 	add	a,divrel
                           1924 	;; zero is the guard value
   0890 60 02              1925 	jz	11$
   0892 F5 48              1926 	mov	divrel,a
   0894 E5 48              1927 11$:	mov	a,divrel
   0896 03                 1928 	rr	a
   0897 03                 1929 	rr	a
   0898 03                 1930 	rr	a
   0899 03                 1931 	rr	a
   089A 54 0F              1932 	anl	a,#0xf
   089C 2B                 1933 	add	a,r3		; momentary correction
   089D 25 46              1934 	add	a,divider
   089F FA                 1935 	mov	r2,a
   08A0 E5 47              1936 	mov	a,divider+1
   08A2 34 00              1937 	addc	a,#0
   08A4 90 C0 02           1938 	mov	dptr,#AUDIODIVIDERHI
   08A7 F0                 1939 	movx	@dptr,a
   08A8 EA                 1940 	mov	a,r2
   08A9 90 C0 01           1941 	mov	dptr,#AUDIODIVIDERLO
   08AC F0                 1942 	movx	@dptr,a
                           1943 	;; OUT data
   08AD 90 7F A0           1944 	mov	dptr,#ISOERR
   08B0 E0                 1945 	movx	a,@dptr
   08B1 20 E0 39           1946 	jb	acc.0,20$
   08B4 90 7F 70           1947 	mov	dptr,#OUT8BCH
   08B7 E0                 1948 	movx	a,@dptr
   08B8 70 33              1949 	jnz	20$
   08BA 90 7F 71           1950 	mov	dptr,#OUT8BCL
   08BD E0                 1951 	movx	a,@dptr
   08BE C3                 1952 	clr	c
   08BF 95 49              1953 	subb	a,framesize
   08C1 70 2A              1954 	jnz	20$
   08C3 90 C0 02           1955 	mov	dptr,#AUDIOTXFIFOCNT
   08C6 E0                 1956 	movx	a,@dptr
   08C7 F5 4B              1957 	mov	txfifocount,a
   08C9 C3                 1958 	clr	c
   08CA 95 49              1959 	subb	a,framesize
   08CC E5 4B              1960 	mov	a,txfifocount
   08CE 40 02              1961 	jc	14$
   08D0 E5 49              1962 	mov	a,framesize
   08D2 60 17              1963 14$:	jz	13$
   08D4 FA                 1964 	mov	r2,a
   08D5 90 7F 60           1965 	mov	dptr,#OUT8DATA
   08D8 05 86              1966 	inc	dps
   08DA 90 C0 00           1967 	mov	dptr,#AUDIOTXFIFO
   08DD 15 86              1968 	dec	dps
   08DF 78 80              1969 	mov	r0,#txsamples
   08E1 E0                 1970 12$:	movx	a,@dptr
   08E2 05 86              1971 	inc	dps
   08E4 F0                 1972 	movx	@dptr,a
   08E5 15 86              1973 	dec	dps
   08E7 F6                 1974 	mov	@r0,a
   08E8 08                 1975 	inc	r0
   08E9 DA F6              1976 	djnz	r2,12$
   08EB 80 18              1977 13$:	sjmp	sofendisr
                           1978 	;; initialize TX if invalid packet
   08ED 90 C0 02           1979 20$:	mov	dptr,#AUDIOTXFIFOCNT
   08F0 E0                 1980 	movx	a,@dptr
   08F1 F5 4B              1981 	mov	txfifocount,a
   08F3 24 FC              1982 	add	a,#-4
   08F5 54 3F              1983 	anl	a,#0x3f
   08F7 60 0C              1984 	jz	sofendisr
   08F9 FA                 1985 	mov	r2,a
   08FA 90 C0 00           1986 	mov	dptr,#AUDIOTXFIFO
   08FD E4                 1987 	clr	a
   08FE 78 80              1988 	mov	r0,#txsamples
   0900 F0                 1989 21$:	movx	@dptr,a
   0901 F6                 1990 	mov	@r0,a
   0902 08                 1991 	inc	r0
   0903 DA FB              1992 	djnz	r2,21$
                           1993 	
   0905                    1994 sofendisr:
                           1995 	;; epilogue
   0905 D0 00              1996 	pop	ar0
   0907 D0 03              1997 	pop	ar3
   0909 D0 02              1998 	pop	ar2
   090B D0 86              1999 	pop	dps
   090D D0 D0              2000 	pop	psw
   090F D0 85              2001 	pop	dph1
   0911 D0 84              2002 	pop	dpl1
   0913 D0 83              2003 	pop	dph0
   0915 D0 82              2004 	pop	dpl0
   0917 D0 F0              2005 	pop	b
   0919 D0 E0              2006 	pop	acc
   091B 32                 2007 	reti
                           2008 
                           2009 
   091C                    2010 usb_sutok_isr:
   091C C0 E0              2011 	push	acc
   091E C0 F0              2012 	push	b
   0920 C0 82              2013 	push	dpl0
   0922 C0 83              2014 	push	dph0
   0924 C0 D0              2015 	push	psw
   0926 75 D0 00           2016 	mov	psw,#0x00
   0929 C0 86              2017 	push	dps
   092B 75 86 00           2018 	mov	dps,#0
                           2019 	;; clear interrupt
   092E E5 91              2020 	mov	a,exif
   0930 C2 E4              2021 	clr	acc.4
   0932 F5 91              2022 	mov	exif,a
   0934 90 7F AB           2023 	mov	dptr,#USBIRQ
   0937 74 04              2024 	mov	a,#0x04
   0939 F0                 2025 	movx	@dptr,a
                           2026 	;; handle interrupt
                           2027 	;; epilogue
   093A D0 86              2028 	pop	dps
   093C D0 D0              2029 	pop	psw
   093E D0 83              2030 	pop	dph0
   0940 D0 82              2031 	pop	dpl0
   0942 D0 F0              2032 	pop	b
   0944 D0 E0              2033 	pop	acc
   0946 32                 2034 	reti
                           2035 
   0947                    2036 usb_suspend_isr:
   0947 C0 E0              2037 	push	acc
   0949 C0 F0              2038 	push	b
   094B C0 82              2039 	push	dpl0
   094D C0 83              2040 	push	dph0
   094F C0 D0              2041 	push	psw
   0951 75 D0 00           2042 	mov	psw,#0x00
   0954 C0 86              2043 	push	dps
   0956 75 86 00           2044 	mov	dps,#0
                           2045 	;; clear interrupt
   0959 E5 91              2046 	mov	a,exif
   095B C2 E4              2047 	clr	acc.4
   095D F5 91              2048 	mov	exif,a
   095F 90 7F AB           2049 	mov	dptr,#USBIRQ
   0962 74 08              2050 	mov	a,#0x08
   0964 F0                 2051 	movx	@dptr,a
                           2052 	;; handle interrupt
                           2053 	;; epilogue
   0965 D0 86              2054 	pop	dps
   0967 D0 D0              2055 	pop	psw
   0969 D0 83              2056 	pop	dph0
   096B D0 82              2057 	pop	dpl0
   096D D0 F0              2058 	pop	b
   096F D0 E0              2059 	pop	acc
   0971 32                 2060 	reti
                           2061 
   0972                    2062 usb_usbreset_isr:
   0972 C0 E0              2063 	push	acc
   0974 C0 F0              2064 	push	b
   0976 C0 82              2065 	push	dpl0
   0978 C0 83              2066 	push	dph0
   097A C0 D0              2067 	push	psw
   097C 75 D0 00           2068 	mov	psw,#0x00
   097F C0 86              2069 	push	dps
   0981 75 86 00           2070 	mov	dps,#0
                           2071 	;; clear interrupt
   0984 E5 91              2072 	mov	a,exif
   0986 C2 E4              2073 	clr	acc.4
   0988 F5 91              2074 	mov	exif,a
   098A 90 7F AB           2075 	mov	dptr,#USBIRQ
   098D 74 10              2076 	mov	a,#0x10
   098F F0                 2077 	movx	@dptr,a
                           2078 	;; handle interrupt
                           2079 	;; epilogue
   0990 D0 86              2080 	pop	dps
   0992 D0 D0              2081 	pop	psw
   0994 D0 83              2082 	pop	dph0
   0996 D0 82              2083 	pop	dpl0
   0998 D0 F0              2084 	pop	b
   099A D0 E0              2085 	pop	acc
   099C 32                 2086 	reti
                           2087 
   099D                    2088 usb_ep0in_isr:
   099D C0 E0              2089 	push	acc
   099F C0 F0              2090 	push	b
   09A1 C0 82              2091 	push	dpl0
   09A3 C0 83              2092 	push	dph0
   09A5 C0 84              2093 	push	dpl1
   09A7 C0 85              2094 	push	dph1
   09A9 C0 D0              2095 	push	psw
   09AB 75 D0 00           2096 	mov	psw,#0x00
   09AE C0 86              2097 	push	dps
   09B0 75 86 00           2098 	mov	dps,#0
   09B3 C0 00              2099 	push	ar0
   09B5 C0 07              2100 	push	ar7
                           2101 	;; clear interrupt
   09B7 E5 91              2102 	mov	a,exif
   09B9 C2 E4              2103 	clr	acc.4
   09BB F5 91              2104 	mov	exif,a
   09BD 90 7F A9           2105 	mov	dptr,#IN07IRQ
   09C0 74 01              2106 	mov	a,#0x01
   09C2 F0                 2107 	movx	@dptr,a
                           2108 	;; handle interrupt
                           2109 	;; epilogue
   09C3 D0 07              2110 	pop	ar7
   09C5 D0 00              2111 	pop	ar0
   09C7 D0 86              2112 	pop	dps
   09C9 D0 D0              2113 	pop	psw
   09CB D0 85              2114 	pop	dph1
   09CD D0 84              2115 	pop	dpl1
   09CF D0 83              2116 	pop	dph0
   09D1 D0 82              2117 	pop	dpl0
   09D3 D0 F0              2118 	pop	b
   09D5 D0 E0              2119 	pop	acc
   09D7 32                 2120 	reti
                           2121 
   09D8                    2122 usb_ep0out_isr:
   09D8 C0 E0              2123 	push	acc
   09DA C0 F0              2124 	push	b
   09DC C0 82              2125 	push	dpl0
   09DE C0 83              2126 	push	dph0
   09E0 C0 84              2127 	push	dpl1
   09E2 C0 85              2128 	push	dph1
   09E4 C0 D0              2129 	push	psw
   09E6 75 D0 00           2130 	mov	psw,#0x00
   09E9 C0 86              2131 	push	dps
   09EB 75 86 00           2132 	mov	dps,#0
   09EE C0 00              2133 	push	ar0
   09F0 C0 06              2134 	push	ar6
                           2135 	;; clear interrupt
   09F2 E5 91              2136 	mov	a,exif
   09F4 C2 E4              2137 	clr	acc.4
   09F6 F5 91              2138 	mov	exif,a
   09F8 90 7F AA           2139 	mov	dptr,#OUT07IRQ
   09FB 74 01              2140 	mov	a,#0x01
   09FD F0                 2141 	movx	@dptr,a
                           2142 	;; handle interrupt
                           2143 	;; epilogue
   09FE D0 06              2144 	pop	ar6
   0A00 D0 00              2145 	pop	ar0
   0A02 D0 86              2146 	pop	dps
   0A04 D0 D0              2147 	pop	psw
   0A06 D0 85              2148 	pop	dph1
   0A08 D0 84              2149 	pop	dpl1
   0A0A D0 83              2150 	pop	dph0
   0A0C D0 82              2151 	pop	dpl0
   0A0E D0 F0              2152 	pop	b
   0A10 D0 E0              2153 	pop	acc
   0A12 32                 2154 	reti
                           2155 
   0A13                    2156 fillusbintr::
   0A13 E5 4C              2157 	mov	a,pttforce
   0A15 F5 F0              2158 	mov	b,a
   0A17 A2 E0              2159 	mov	c,acc.0
   0A19 92 F2              2160 	mov	b.2,c
   0A1B 90 C0 09           2161 	mov	dptr,#AUDIOSTAT
   0A1E E0                 2162 	movx	a,@dptr
   0A1F A2 E0              2163 	mov	c,acc.0
   0A21 92 F3              2164 	mov	b.3,c
   0A23 A2 09              2165 	mov	c,uartempty
   0A25 92 F5              2166 	mov	b.5,c
                           2167 	;; bytewide elements
   0A27 90 7E 80           2168 	mov	dptr,#(IN1BUF)
   0A2A E5 F0              2169 	mov	a,b
   0A2C F0                 2170 	movx	@dptr,a
   0A2D E5 45              2171 	mov	a,sofcount
   0A2F 90 7E 81           2172 	mov	dptr,#(IN1BUF+1)
   0A32 F0                 2173 	movx	@dptr,a
   0A33 90 7F E0           2174 	mov	dptr,#INISOVAL
   0A36 E0                 2175 	movx	a,@dptr
   0A37 90 7E 82           2176 	mov	dptr,#(IN1BUF+2)
   0A3A F0                 2177 	movx	@dptr,a
   0A3B 90 C0 04           2178 	mov	dptr,#AUDIORSSI
   0A3E E0                 2179 	movx	a,@dptr
   0A3F 90 7E 83           2180 	mov	dptr,#(IN1BUF+3)
   0A42 F0                 2181 	movx	@dptr,a
                           2182 	; counter
   0A43 05 44              2183 	inc	irqcount
   0A45 E5 44              2184 	mov	a,irqcount
   0A47 90 7E 84           2185 	mov	dptr,#(IN1BUF+4)
   0A4A F0                 2186 	movx	@dptr,a
                           2187 	; UART buffer
   0A4B 75 F0 05           2188 	mov	b,#5
   0A4E 90 7E 85           2189 	mov	dptr,#(IN1BUF+5)
   0A51 E5 5E              2190 2$:	mov	a,uartrd
   0A53 B5 5D 07           2191 	cjne	a,uartwr,3$
                           2192 	; set length
   0A56 90 7F B7           2193 	mov	dptr,#IN1BC
   0A59 E5 F0              2194 	mov	a,b
   0A5B F0                 2195 	movx	@dptr,a
   0A5C 22                 2196 	ret
                           2197 	
   0A5D 24 4D              2198 3$:	add	a,#uartbuf
   0A5F F8                 2199 	mov	r0,a
   0A60 E6                 2200 	mov	a,@r0
   0A61 F0                 2201 	movx	@dptr,a
   0A62 A3                 2202 	inc	dptr
   0A63 05 F0              2203 	inc	b
   0A65 E5 5E              2204 	mov	a,uartrd
   0A67 04                 2205 	inc	a
   0A68 54 0F              2206 	anl	a,#0xf
   0A6A F5 5E              2207 	mov	uartrd,a
   0A6C 80 E3              2208 	sjmp	2$
                           2209 
                           2210 	
   0A6E                    2211 usb_ep1in_isr:
   0A6E C0 E0              2212 	push	acc
   0A70 C0 F0              2213 	push	b
   0A72 C0 82              2214 	push	dpl0
   0A74 C0 83              2215 	push	dph0
   0A76 C0 D0              2216 	push	psw
   0A78 75 D0 00           2217 	mov	psw,#0x00
   0A7B C0 86              2218 	push	dps
   0A7D 75 86 00           2219 	mov	dps,#0
                           2220 	;; clear interrupt
   0A80 E5 91              2221 	mov	a,exif
   0A82 C2 E4              2222 	clr	acc.4
   0A84 F5 91              2223 	mov	exif,a
   0A86 90 7F A9           2224 	mov	dptr,#IN07IRQ
   0A89 74 02              2225 	mov	a,#0x02
   0A8B F0                 2226 	movx	@dptr,a
                           2227 	;; handle interrupt
   0A8C 12 0A 13           2228 	lcall	fillusbintr
                           2229 	;; epilogue
   0A8F D0 86              2230 	pop	dps
   0A91 D0 D0              2231 	pop	psw
   0A93 D0 83              2232 	pop	dph0
   0A95 D0 82              2233 	pop	dpl0
   0A97 D0 F0              2234 	pop	b
   0A99 D0 E0              2235 	pop	acc
   0A9B 32                 2236 	reti
                           2237 
   0A9C                    2238 usb_ep1out_isr:
   0A9C C0 E0              2239 	push	acc
   0A9E C0 F0              2240 	push	b
   0AA0 C0 82              2241 	push	dpl0
   0AA2 C0 83              2242 	push	dph0
   0AA4 C0 D0              2243 	push	psw
   0AA6 75 D0 00           2244 	mov	psw,#0x00
   0AA9 C0 86              2245 	push	dps
   0AAB 75 86 00           2246 	mov	dps,#0
                           2247 	;; clear interrupt
   0AAE E5 91              2248 	mov	a,exif
   0AB0 C2 E4              2249 	clr	acc.4
   0AB2 F5 91              2250 	mov	exif,a
   0AB4 90 7F AA           2251 	mov	dptr,#OUT07IRQ
   0AB7 74 02              2252 	mov	a,#0x02
   0AB9 F0                 2253 	movx	@dptr,a
                           2254 	;; handle interrupt
                           2255 	;; epilogue
   0ABA D0 86              2256 	pop	dps
   0ABC D0 D0              2257 	pop	psw
   0ABE D0 83              2258 	pop	dph0
   0AC0 D0 82              2259 	pop	dpl0
   0AC2 D0 F0              2260 	pop	b
   0AC4 D0 E0              2261 	pop	acc
   0AC6 32                 2262 	reti
                           2263 
   0AC7                    2264 usb_ep2in_isr:
   0AC7 C0 E0              2265 	push	acc
   0AC9 C0 F0              2266 	push	b
   0ACB C0 82              2267 	push	dpl0
   0ACD C0 83              2268 	push	dph0
   0ACF C0 D0              2269 	push	psw
   0AD1 75 D0 00           2270 	mov	psw,#0x00
   0AD4 C0 86              2271 	push	dps
   0AD6 75 86 00           2272 	mov	dps,#0
                           2273 	;; clear interrupt
   0AD9 E5 91              2274 	mov	a,exif
   0ADB C2 E4              2275 	clr	acc.4
   0ADD F5 91              2276 	mov	exif,a
   0ADF 90 7F A9           2277 	mov	dptr,#IN07IRQ
   0AE2 74 04              2278 	mov	a,#0x04
   0AE4 F0                 2279 	movx	@dptr,a
                           2280 	;; handle interrupt
                           2281 	;; epilogue
   0AE5 D0 86              2282 	pop	dps
   0AE7 D0 D0              2283 	pop	psw
   0AE9 D0 83              2284 	pop	dph0
   0AEB D0 82              2285 	pop	dpl0
   0AED D0 F0              2286 	pop	b
   0AEF D0 E0              2287 	pop	acc
   0AF1 32                 2288 	reti
                           2289 
   0AF2                    2290 usb_ep2out_isr:
   0AF2 C0 E0              2291 	push	acc
   0AF4 C0 F0              2292 	push	b
   0AF6 C0 82              2293 	push	dpl0
   0AF8 C0 83              2294 	push	dph0
   0AFA C0 D0              2295 	push	psw
   0AFC 75 D0 00           2296 	mov	psw,#0x00
   0AFF C0 86              2297 	push	dps
   0B01 75 86 00           2298 	mov	dps,#0
                           2299 	;; clear interrupt
   0B04 E5 91              2300 	mov	a,exif
   0B06 C2 E4              2301 	clr	acc.4
   0B08 F5 91              2302 	mov	exif,a
   0B0A 90 7F AA           2303 	mov	dptr,#OUT07IRQ
   0B0D 74 04              2304 	mov	a,#0x04
   0B0F F0                 2305 	movx	@dptr,a
                           2306 	;; handle interrupt
                           2307 	;; epilogue
   0B10 D0 86              2308 	pop	dps
   0B12 D0 D0              2309 	pop	psw
   0B14 D0 83              2310 	pop	dph0
   0B16 D0 82              2311 	pop	dpl0
   0B18 D0 F0              2312 	pop	b
   0B1A D0 E0              2313 	pop	acc
   0B1C 32                 2314 	reti
                           2315 
   0B1D                    2316 usb_ep3in_isr:
   0B1D C0 E0              2317 	push	acc
   0B1F C0 F0              2318 	push	b
   0B21 C0 82              2319 	push	dpl0
   0B23 C0 83              2320 	push	dph0
   0B25 C0 D0              2321 	push	psw
   0B27 75 D0 00           2322 	mov	psw,#0x00
   0B2A C0 86              2323 	push	dps
   0B2C 75 86 00           2324 	mov	dps,#0
                           2325 	;; clear interrupt
   0B2F E5 91              2326 	mov	a,exif
   0B31 C2 E4              2327 	clr	acc.4
   0B33 F5 91              2328 	mov	exif,a
   0B35 90 7F A9           2329 	mov	dptr,#IN07IRQ
   0B38 74 08              2330 	mov	a,#0x08
   0B3A F0                 2331 	movx	@dptr,a
                           2332 	;; handle interrupt
                           2333 	;; epilogue
   0B3B D0 86              2334 	pop	dps
   0B3D D0 D0              2335 	pop	psw
   0B3F D0 83              2336 	pop	dph0
   0B41 D0 82              2337 	pop	dpl0
   0B43 D0 F0              2338 	pop	b
   0B45 D0 E0              2339 	pop	acc
   0B47 32                 2340 	reti
                           2341 
   0B48                    2342 usb_ep3out_isr:
   0B48 C0 E0              2343 	push	acc
   0B4A C0 F0              2344 	push	b
   0B4C C0 82              2345 	push	dpl0
   0B4E C0 83              2346 	push	dph0
   0B50 C0 D0              2347 	push	psw
   0B52 75 D0 00           2348 	mov	psw,#0x00
   0B55 C0 86              2349 	push	dps
   0B57 75 86 00           2350 	mov	dps,#0
                           2351 	;; clear interrupt
   0B5A E5 91              2352 	mov	a,exif
   0B5C C2 E4              2353 	clr	acc.4
   0B5E F5 91              2354 	mov	exif,a
   0B60 90 7F AA           2355 	mov	dptr,#OUT07IRQ
   0B63 74 08              2356 	mov	a,#0x08
   0B65 F0                 2357 	movx	@dptr,a
                           2358 	;; handle interrupt
                           2359 	;; epilogue
   0B66 D0 86              2360 	pop	dps
   0B68 D0 D0              2361 	pop	psw
   0B6A D0 83              2362 	pop	dph0
   0B6C D0 82              2363 	pop	dpl0
   0B6E D0 F0              2364 	pop	b
   0B70 D0 E0              2365 	pop	acc
   0B72 32                 2366 	reti
                           2367 
   0B73                    2368 usb_ep4in_isr:
   0B73 C0 E0              2369 	push	acc
   0B75 C0 F0              2370 	push	b
   0B77 C0 82              2371 	push	dpl0
   0B79 C0 83              2372 	push	dph0
   0B7B C0 D0              2373 	push	psw
   0B7D 75 D0 00           2374 	mov	psw,#0x00
   0B80 C0 86              2375 	push	dps
   0B82 75 86 00           2376 	mov	dps,#0
                           2377 	;; clear interrupt
   0B85 E5 91              2378 	mov	a,exif
   0B87 C2 E4              2379 	clr	acc.4
   0B89 F5 91              2380 	mov	exif,a
   0B8B 90 7F A9           2381 	mov	dptr,#IN07IRQ
   0B8E 74 10              2382 	mov	a,#0x10
   0B90 F0                 2383 	movx	@dptr,a
                           2384 	;; handle interrupt
                           2385 	;; epilogue
   0B91 D0 86              2386 	pop	dps
   0B93 D0 D0              2387 	pop	psw
   0B95 D0 83              2388 	pop	dph0
   0B97 D0 82              2389 	pop	dpl0
   0B99 D0 F0              2390 	pop	b
   0B9B D0 E0              2391 	pop	acc
   0B9D 32                 2392 	reti
                           2393 
   0B9E                    2394 usb_ep4out_isr:
   0B9E C0 E0              2395 	push	acc
   0BA0 C0 F0              2396 	push	b
   0BA2 C0 82              2397 	push	dpl0
   0BA4 C0 83              2398 	push	dph0
   0BA6 C0 D0              2399 	push	psw
   0BA8 75 D0 00           2400 	mov	psw,#0x00
   0BAB C0 86              2401 	push	dps
   0BAD 75 86 00           2402 	mov	dps,#0
                           2403 	;; clear interrupt
   0BB0 E5 91              2404 	mov	a,exif
   0BB2 C2 E4              2405 	clr	acc.4
   0BB4 F5 91              2406 	mov	exif,a
   0BB6 90 7F AA           2407 	mov	dptr,#OUT07IRQ
   0BB9 74 10              2408 	mov	a,#0x10
   0BBB F0                 2409 	movx	@dptr,a
                           2410 	;; handle interrupt
                           2411 	;; epilogue
   0BBC D0 86              2412 	pop	dps
   0BBE D0 D0              2413 	pop	psw
   0BC0 D0 83              2414 	pop	dph0
   0BC2 D0 82              2415 	pop	dpl0
   0BC4 D0 F0              2416 	pop	b
   0BC6 D0 E0              2417 	pop	acc
   0BC8 32                 2418 	reti
                           2419 
   0BC9                    2420 usb_ep5in_isr:
   0BC9 C0 E0              2421 	push	acc
   0BCB C0 F0              2422 	push	b
   0BCD C0 82              2423 	push	dpl0
   0BCF C0 83              2424 	push	dph0
   0BD1 C0 D0              2425 	push	psw
   0BD3 75 D0 00           2426 	mov	psw,#0x00
   0BD6 C0 86              2427 	push	dps
   0BD8 75 86 00           2428 	mov	dps,#0
                           2429 	;; clear interrupt
   0BDB E5 91              2430 	mov	a,exif
   0BDD C2 E4              2431 	clr	acc.4
   0BDF F5 91              2432 	mov	exif,a
   0BE1 90 7F A9           2433 	mov	dptr,#IN07IRQ
   0BE4 74 20              2434 	mov	a,#0x20
   0BE6 F0                 2435 	movx	@dptr,a
                           2436 	;; handle interrupt
                           2437 	;; epilogue
   0BE7 D0 86              2438 	pop	dps
   0BE9 D0 D0              2439 	pop	psw
   0BEB D0 83              2440 	pop	dph0
   0BED D0 82              2441 	pop	dpl0
   0BEF D0 F0              2442 	pop	b
   0BF1 D0 E0              2443 	pop	acc
   0BF3 32                 2444 	reti
                           2445 
   0BF4                    2446 usb_ep5out_isr:
   0BF4 C0 E0              2447 	push	acc
   0BF6 C0 F0              2448 	push	b
   0BF8 C0 82              2449 	push	dpl0
   0BFA C0 83              2450 	push	dph0
   0BFC C0 D0              2451 	push	psw
   0BFE 75 D0 00           2452 	mov	psw,#0x00
   0C01 C0 86              2453 	push	dps
   0C03 75 86 00           2454 	mov	dps,#0
                           2455 	;; clear interrupt
   0C06 E5 91              2456 	mov	a,exif
   0C08 C2 E4              2457 	clr	acc.4
   0C0A F5 91              2458 	mov	exif,a
   0C0C 90 7F AA           2459 	mov	dptr,#OUT07IRQ
   0C0F 74 20              2460 	mov	a,#0x20
   0C11 F0                 2461 	movx	@dptr,a
                           2462 	;; handle interrupt
                           2463 	;; epilogue
   0C12 D0 86              2464 	pop	dps
   0C14 D0 D0              2465 	pop	psw
   0C16 D0 83              2466 	pop	dph0
   0C18 D0 82              2467 	pop	dpl0
   0C1A D0 F0              2468 	pop	b
   0C1C D0 E0              2469 	pop	acc
   0C1E 32                 2470 	reti
                           2471 
   0C1F                    2472 usb_ep6in_isr:
   0C1F C0 E0              2473 	push	acc
   0C21 C0 F0              2474 	push	b
   0C23 C0 82              2475 	push	dpl0
   0C25 C0 83              2476 	push	dph0
   0C27 C0 D0              2477 	push	psw
   0C29 75 D0 00           2478 	mov	psw,#0x00
   0C2C C0 86              2479 	push	dps
   0C2E 75 86 00           2480 	mov	dps,#0
                           2481 	;; clear interrupt
   0C31 E5 91              2482 	mov	a,exif
   0C33 C2 E4              2483 	clr	acc.4
   0C35 F5 91              2484 	mov	exif,a
   0C37 90 7F A9           2485 	mov	dptr,#IN07IRQ
   0C3A 74 40              2486 	mov	a,#0x40
   0C3C F0                 2487 	movx	@dptr,a
                           2488 	;; handle interrupt
                           2489 	;; epilogue
   0C3D D0 86              2490 	pop	dps
   0C3F D0 D0              2491 	pop	psw
   0C41 D0 83              2492 	pop	dph0
   0C43 D0 82              2493 	pop	dpl0
   0C45 D0 F0              2494 	pop	b
   0C47 D0 E0              2495 	pop	acc
   0C49 32                 2496 	reti
                           2497 
   0C4A                    2498 usb_ep6out_isr:
   0C4A C0 E0              2499 	push	acc
   0C4C C0 F0              2500 	push	b
   0C4E C0 82              2501 	push	dpl0
   0C50 C0 83              2502 	push	dph0
   0C52 C0 D0              2503 	push	psw
   0C54 75 D0 00           2504 	mov	psw,#0x00
   0C57 C0 86              2505 	push	dps
   0C59 75 86 00           2506 	mov	dps,#0
                           2507 	;; clear interrupt
   0C5C E5 91              2508 	mov	a,exif
   0C5E C2 E4              2509 	clr	acc.4
   0C60 F5 91              2510 	mov	exif,a
   0C62 90 7F AA           2511 	mov	dptr,#OUT07IRQ
   0C65 74 40              2512 	mov	a,#0x40
   0C67 F0                 2513 	movx	@dptr,a
                           2514 	;; handle interrupt
                           2515 	;; epilogue
   0C68 D0 86              2516 	pop	dps
   0C6A D0 D0              2517 	pop	psw
   0C6C D0 83              2518 	pop	dph0
   0C6E D0 82              2519 	pop	dpl0
   0C70 D0 F0              2520 	pop	b
   0C72 D0 E0              2521 	pop	acc
   0C74 32                 2522 	reti
                           2523 
   0C75                    2524 usb_ep7in_isr:
   0C75 C0 E0              2525 	push	acc
   0C77 C0 F0              2526 	push	b
   0C79 C0 82              2527 	push	dpl0
   0C7B C0 83              2528 	push	dph0
   0C7D C0 D0              2529 	push	psw
   0C7F 75 D0 00           2530 	mov	psw,#0x00
   0C82 C0 86              2531 	push	dps
   0C84 75 86 00           2532 	mov	dps,#0
                           2533 	;; clear interrupt
   0C87 E5 91              2534 	mov	a,exif
   0C89 C2 E4              2535 	clr	acc.4
   0C8B F5 91              2536 	mov	exif,a
   0C8D 90 7F A9           2537 	mov	dptr,#IN07IRQ
   0C90 74 80              2538 	mov	a,#0x80
   0C92 F0                 2539 	movx	@dptr,a
                           2540 	;; handle interrupt
                           2541 	;; epilogue
   0C93 D0 86              2542 	pop	dps
   0C95 D0 D0              2543 	pop	psw
   0C97 D0 83              2544 	pop	dph0
   0C99 D0 82              2545 	pop	dpl0
   0C9B D0 F0              2546 	pop	b
   0C9D D0 E0              2547 	pop	acc
   0C9F 32                 2548 	reti
                           2549 
   0CA0                    2550 usb_ep7out_isr:
   0CA0 C0 E0              2551 	push	acc
   0CA2 C0 F0              2552 	push	b
   0CA4 C0 82              2553 	push	dpl0
   0CA6 C0 83              2554 	push	dph0
   0CA8 C0 D0              2555 	push	psw
   0CAA 75 D0 00           2556 	mov	psw,#0x00
   0CAD C0 86              2557 	push	dps
   0CAF 75 86 00           2558 	mov	dps,#0
                           2559 	;; clear interrupt
   0CB2 E5 91              2560 	mov	a,exif
   0CB4 C2 E4              2561 	clr	acc.4
   0CB6 F5 91              2562 	mov	exif,a
   0CB8 90 7F AA           2563 	mov	dptr,#OUT07IRQ
   0CBB 74 80              2564 	mov	a,#0x80
   0CBD F0                 2565 	movx	@dptr,a
                           2566 	;; handle interrupt
                           2567 	;; epilogue
   0CBE D0 86              2568 	pop	dps
   0CC0 D0 D0              2569 	pop	psw
   0CC2 D0 83              2570 	pop	dph0
   0CC4 D0 82              2571 	pop	dpl0
   0CC6 D0 F0              2572 	pop	b
   0CC8 D0 E0              2573 	pop	acc
   0CCA 32                 2574 	reti
                           2575 	
                           2576 	;; -----------------------------------------------------
                           2577 	;; USB descriptors
                           2578 	;; -----------------------------------------------------
                           2579 
                           2580 	;; Device and/or Interface Class codes
                    0000   2581 	USB_CLASS_PER_INTERFACE         = 0
                    0001   2582 	USB_CLASS_AUDIO                 = 1
                    0002   2583 	USB_CLASS_COMM                  = 2
                    0003   2584 	USB_CLASS_HID                   = 3
                    0007   2585 	USB_CLASS_PRINTER               = 7
                    0008   2586 	USB_CLASS_MASS_STORAGE          = 8
                    0009   2587 	USB_CLASS_HUB                   = 9
                    00FF   2588 	USB_CLASS_VENDOR_SPEC           = 0xff
                           2589 
                    0001   2590 	USB_SUBCLASS_AUDIOCONTROL	= 1
                    0002   2591 	USB_SUBCLASS_AUDIOSTREAMING	= 2
                           2592 	
                           2593 	;; Descriptor types
                    0001   2594 	USB_DT_DEVICE                   = 0x01
                    0002   2595 	USB_DT_CONFIG                   = 0x02
                    0003   2596 	USB_DT_STRING                   = 0x03
                    0004   2597 	USB_DT_INTERFACE                = 0x04
                    0005   2598 	USB_DT_ENDPOINT                 = 0x05
                           2599 
                           2600 	;; Audio Class descriptor types
                    0020   2601 	USB_DT_AUDIO_UNDEFINED		= 0x20
                    0021   2602 	USB_DT_AUDIO_DEVICE		= 0x21
                    0022   2603 	USB_DT_AUDIO_CONFIG		= 0x22
                    0023   2604 	USB_DT_AUDIO_STRING		= 0x23
                    0024   2605 	USB_DT_AUDIO_INTERFACE		= 0x24
                    0025   2606 	USB_DT_AUDIO_ENDPOINT		= 0x25
                           2607 
                    0000   2608 	USB_SDT_AUDIO_UNDEFINED		= 0x00
                    0001   2609 	USB_SDT_AUDIO_HEADER		= 0x01
                    0002   2610 	USB_SDT_AUDIO_INPUT_TERMINAL	= 0x02
                    0003   2611 	USB_SDT_AUDIO_OUTPUT_TERMINAL	= 0x03
                    0004   2612 	USB_SDT_AUDIO_MIXER_UNIT	= 0x04
                    0005   2613 	USB_SDT_AUDIO_SELECTOR_UNIT	= 0x05
                    0006   2614 	USB_SDT_AUDIO_FEATURE_UNIT	= 0x06
                    0007   2615 	USB_SDT_AUDIO_PROCESSING_UNIT	= 0x07
                    0008   2616 	USB_SDT_AUDIO_EXTENSION_UNIT	= 0x08
                           2617 	
                           2618 	;; Standard requests
                    0000   2619 	USB_REQ_GET_STATUS              = 0x00
                    0001   2620 	USB_REQ_CLEAR_FEATURE           = 0x01
                    0003   2621 	USB_REQ_SET_FEATURE             = 0x03
                    0005   2622 	USB_REQ_SET_ADDRESS             = 0x05
                    0006   2623 	USB_REQ_GET_DESCRIPTOR          = 0x06
                    0007   2624 	USB_REQ_SET_DESCRIPTOR          = 0x07
                    0008   2625 	USB_REQ_GET_CONFIGURATION       = 0x08
                    0009   2626 	USB_REQ_SET_CONFIGURATION       = 0x09
                    000A   2627 	USB_REQ_GET_INTERFACE           = 0x0A
                    000B   2628 	USB_REQ_SET_INTERFACE           = 0x0B
                    000C   2629 	USB_REQ_SYNCH_FRAME             = 0x0C
                           2630 
                           2631 	;; Audio Class Requests
                    0001   2632 	USB_REQ_AUDIO_SET_CUR		= 0x01
                    0081   2633 	USB_REQ_AUDIO_GET_CUR		= 0x81
                    0002   2634 	USB_REQ_AUDIO_SET_MIN		= 0x02
                    0082   2635 	USB_REQ_AUDIO_GET_MIN		= 0x82
                    0003   2636 	USB_REQ_AUDIO_SET_MAX		= 0x03
                    0083   2637 	USB_REQ_AUDIO_GET_MAX		= 0x83
                    0004   2638 	USB_REQ_AUDIO_SET_RES		= 0x04
                    0084   2639 	USB_REQ_AUDIO_GET_RES		= 0x84
                    0005   2640 	USB_REQ_AUDIO_SET_MEM		= 0x05
                    0085   2641 	USB_REQ_AUDIO_GET_MEM		= 0x85
                    00FF   2642 	USB_REQ_AUDIO_GET_STAT		= 0xff
                           2643 	
                           2644 	;; USB Request Type and Endpoint Directions
                    0000   2645 	USB_DIR_OUT                     = 0
                    0080   2646 	USB_DIR_IN                      = 0x80
                           2647 
                    0000   2648 	USB_TYPE_STANDARD               = (0x00 << 5)
                    0020   2649 	USB_TYPE_CLASS                  = (0x01 << 5)
                    0040   2650 	USB_TYPE_VENDOR                 = (0x02 << 5)
                    0060   2651 	USB_TYPE_RESERVED               = (0x03 << 5)
                           2652 
                    0000   2653 	USB_RECIP_DEVICE                = 0x00
                    0001   2654 	USB_RECIP_INTERFACE             = 0x01
                    0002   2655 	USB_RECIP_ENDPOINT              = 0x02
                    0003   2656 	USB_RECIP_OTHER                 = 0x03
                           2657 
                           2658 	;; Request target types.
                    0000   2659 	USB_RT_DEVICE                   = 0x00
                    0001   2660 	USB_RT_INTERFACE                = 0x01
                    0002   2661 	USB_RT_ENDPOINT                 = 0x02
                           2662 
                    BAC0   2663 	VENDID	= 0xbac0
                    6137   2664 	PRODID	= 0x6137
                           2665 
   0CCB                    2666 devicedescr:
   0CCB 12                 2667 	.db	18			; bLength
   0CCC 01                 2668 	.db	USB_DT_DEVICE		; bDescriptorType
   0CCD 00 01              2669 	.db	0x00, 0x01		; bcdUSB
   0CCF FF                 2670 	.db	USB_CLASS_VENDOR_SPEC	; bDeviceClass
   0CD0 00                 2671 	.db	0			; bDeviceSubClass
   0CD1 FF                 2672 	.db	0xff			; bDeviceProtocol
   0CD2 40                 2673 	.db	0x40			; bMaxPacketSize0
   0CD3 C0 BA              2674 	.db	<VENDID,>VENDID		; idVendor
   0CD5 37 61              2675 	.db	<PRODID,>PRODID		; idProduct
   0CD7 01 00              2676 	.db	0x01,0x00		; bcdDevice
   0CD9 01                 2677 	.db	1			; iManufacturer
   0CDA 02                 2678 	.db	2			; iProduct
   0CDB 03                 2679 	.db	3			; iSerialNumber
   0CDC 01                 2680 	.db	1			; bNumConfigurations
                           2681 
   0CDD                    2682 config0descr:
   0CDD 09                 2683 	.db	9			; bLength
   0CDE 02                 2684 	.db	USB_DT_CONFIG		; bDescriptorType
   0CDF 3B 00              2685 	.db	<config0sz,>config0sz	; wTotalLength
   0CE1 01                 2686 	.db	1			; bNumInterfaces
   0CE2 01                 2687 	.db	1			; bConfigurationValue
   0CE3 00                 2688 	.db	0			; iConfiguration
   0CE4 40                 2689 	.db	0b01000000		; bmAttributs (self powered)
   0CE5 00                 2690 	.db	0			; MaxPower (mA/2) (self powered so 0)
                           2691 	;; standard packet interface (needed so the driver can hook to it)
                           2692 	;; interface descriptor I0:A0
   0CE6 09                 2693 	.db	9			; bLength
   0CE7 04                 2694 	.db	USB_DT_INTERFACE	; bDescriptorType
   0CE8 00                 2695 	.db	0			; bInterfaceNumber
   0CE9 00                 2696 	.db	0			; bAlternateSetting
   0CEA 01                 2697 	.db	1			; bNumEndpoints
   0CEB FF                 2698 	.db	0xff			; bInterfaceClass (vendor specific)
   0CEC 00                 2699 	.db	0x00			; bInterfaceSubClass
   0CED FF                 2700 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0CEE 00                 2701 	.db	0			; iInterface
                           2702 	;; endpoint descriptor I0:A0:E0
   0CEF 07                 2703 	.db	7			; bLength
   0CF0 05                 2704 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0CF1 81                 2705 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0CF2 02                 2706 	.db	0x02			; bmAttributes (bulk)
   0CF3 40 00              2707 	.db	0x40,0x00		; wMaxPacketSize
   0CF5 00                 2708 	.db	0			; bInterval
                           2709 	;; interface descriptor I0:A1
   0CF6 09                 2710 	.db	9			; bLength
   0CF7 04                 2711 	.db	USB_DT_INTERFACE	; bDescriptorType
   0CF8 00                 2712 	.db	0			; bInterfaceNumber
   0CF9 01                 2713 	.db	1			; bAlternateSetting
   0CFA 03                 2714 	.db	3			; bNumEndpoints
   0CFB FF                 2715 	.db	0xff			; bInterfaceClass (vendor specific)
   0CFC 00                 2716 	.db	0x00			; bInterfaceSubClass
   0CFD FF                 2717 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0CFE 00                 2718 	.db	0			; iInterface
                           2719 	;; endpoint descriptor I0:A1:E0
   0CFF 07                 2720 	.db	7			; bLength
   0D00 05                 2721 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0D01 81                 2722 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0D02 03                 2723 	.db	0x03			; bmAttributes (interrupt)
   0D03 40 00              2724 	.db	0x40,0x00		; wMaxPacketSize
   0D05 08                 2725 	.db	8			; bInterval
                           2726 	;; endpoint descriptor I0:A1:E1
   0D06 09                 2727 	.db	9			; bLength
   0D07 05                 2728 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0D08 88                 2729 	.db	(USB_DIR_IN | 8)	; bEndpointAddress
   0D09 01                 2730 	.db	0x01			; bmAttributes (iso)
   0D0A                    2731 descinframesize:
   0D0A 02 00              2732 	.db	0x02,0x00		; wMaxPacketSize
   0D0C 01                 2733 	.db	1			; bInterval
   0D0D 00                 2734 	.db	0			; bRefresh
   0D0E 00                 2735 	.db	0			; bSynchAddress
                           2736 	;; endpoint descriptor I0:A1:E2
   0D0F 09                 2737 	.db	9			; bLength
   0D10 05                 2738 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0D11 08                 2739 	.db	(USB_DIR_OUT | 8)	; bEndpointAddress
   0D12 01                 2740 	.db	0x01			; bmAttributes (iso)
   0D13                    2741 descoutframesize:
   0D13 02 00              2742 	.db	0x02,0x00		; wMaxPacketSize
   0D15 01                 2743 	.db	1			; bInterval
   0D16 00                 2744 	.db	0			; bRefresh
   0D17 00                 2745 	.db	0			; bSynchAddress
                    003B   2746 config0sz = . - config0descr
                           2747 
   0D18                    2748 stringdescr:
   0D18 20 0D              2749 	.db	<string0,>string0
   0D1A 24 0D              2750 	.db	<string1,>string1
   0D1C 32 0D              2751 	.db	<string2,>string2
   0D1E 52 0D              2752 	.db	<stringserial,>stringserial
                           2753 
                    0004   2754 numstrings = (. - stringdescr)/2
                           2755 
   0D20                    2756 string0:
   0D20 04                 2757 	.db	string0sz		; bLength
   0D21 03                 2758 	.db	USB_DT_STRING		; bDescriptorType
   0D22 00 00              2759 	.db	0,0			; LANGID[0]: Lang Neutral
                    0004   2760 string0sz = . - string0
                           2761 
   0D24                    2762 string1:
   0D24 0E                 2763 	.db	string1sz		; bLength
   0D25 03                 2764 	.db	USB_DT_STRING		; bDescriptorType
   0D26 42 00 61 00 79 00  2765 	.db	'B,0,'a,0,'y,0,'c,0,'o,0,'m,0
        63 00 6F 00 6D 00
                    000E   2766 string1sz = . - string1
                           2767 
   0D32                    2768 string2:
   0D32 20                 2769 	.db	string2sz		; bLength
   0D33 03                 2770 	.db	USB_DT_STRING		; bDescriptorType
   0D34 55 00 53 00 42 00  2771 	.db	'U,0,'S,0,'B,0,'F,0,'L,0,'E,0,'X,0,' ,0
        46 00 4C 00 45 00
        58 00 20 00
   0D44 28 00 41 00 75 00  2772 	.db	'(,0,'A,0,'u,0,'d,0,'i,0,'o,0,'),0
        64 00 69 00 6F 00
        29 00
                    0020   2773 string2sz = . - string2
                           2774 	
   0D52                    2775 stringserial:
   0D52 02                 2776 	.db	2			; bLength
   0D53 03                 2777 	.db	USB_DT_STRING		; bDescriptorType
   0D54 00 00 00 00 00 00  2778 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
   0D64 00 00 00 00 00 00  2779 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
                           2780 
