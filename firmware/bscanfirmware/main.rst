                              1 	.module main
                              2 
                              3 	;; define code segments link order
                              4 	.area CODE (CODE)
                              5 	.area CSEG (CODE)
                              6 	.area GSINIT (CODE)
                              7 	.area GSINIT2 (CODE)
                              8 
                              9 	;; -----------------------------------------------------
                             10 
                             11 	;; special function registers (which are not predefined)
                    0082     12 	dpl0    = 0x82
                    0083     13 	dph0    = 0x83
                    0084     14 	dpl1    = 0x84
                    0085     15 	dph1    = 0x85
                    0086     16 	dps     = 0x86
                    008E     17 	ckcon   = 0x8E
                    008F     18 	spc_fnc = 0x8F
                    0091     19 	exif    = 0x91
                    0092     20 	mpage   = 0x92
                    0098     21 	scon0   = 0x98
                    0099     22 	sbuf0   = 0x99
                    00C0     23 	scon1   = 0xC0
                    00C1     24 	sbuf1   = 0xC1
                    00D8     25 	eicon   = 0xD8
                    00E8     26 	eie     = 0xE8
                    00F8     27 	eip     = 0xF8
                             28 
                             29 	;; anchor xdata registers
                    7F00     30 	IN0BUF		= 0x7F00
                    7EC0     31 	OUT0BUF		= 0x7EC0
                    7E80     32 	IN1BUF		= 0x7E80
                    7E40     33 	OUT1BUF		= 0x7E40
                    7E00     34 	IN2BUF		= 0x7E00
                    7DC0     35 	OUT2BUF		= 0x7DC0
                    7D80     36 	IN3BUF		= 0x7D80
                    7D40     37 	OUT3BUF		= 0x7D40
                    7D00     38 	IN4BUF		= 0x7D00
                    7CC0     39 	OUT4BUF		= 0x7CC0
                    7C80     40 	IN5BUF		= 0x7C80
                    7C40     41 	OUT5BUF		= 0x7C40
                    7C00     42 	IN6BUF		= 0x7C00
                    7BC0     43 	OUT6BUF		= 0x7BC0
                    7B80     44 	IN7BUF		= 0x7B80
                    7B40     45 	OUT7BUF		= 0x7B40
                    7FE8     46 	SETUPBUF	= 0x7FE8
                    7FE8     47 	SETUPDAT	= 0x7FE8
                             48 
                    7FB4     49 	EP0CS		= 0x7FB4
                    7FB5     50 	IN0BC		= 0x7FB5
                    7FB6     51 	IN1CS		= 0x7FB6
                    7FB7     52 	IN1BC		= 0x7FB7
                    7FB8     53 	IN2CS		= 0x7FB8
                    7FB9     54 	IN2BC		= 0x7FB9
                    7FBA     55 	IN3CS		= 0x7FBA
                    7FBB     56 	IN3BC		= 0x7FBB
                    7FBC     57 	IN4CS		= 0x7FBC
                    7FBD     58 	IN4BC		= 0x7FBD
                    7FBE     59 	IN5CS		= 0x7FBE
                    7FBF     60 	IN5BC		= 0x7FBF
                    7FC0     61 	IN6CS		= 0x7FC0
                    7FC1     62 	IN6BC		= 0x7FC1
                    7FC2     63 	IN7CS		= 0x7FC2
                    7FC3     64 	IN7BC		= 0x7FC3
                    7FC5     65 	OUT0BC		= 0x7FC5
                    7FC6     66 	OUT1CS		= 0x7FC6
                    7FC7     67 	OUT1BC		= 0x7FC7
                    7FC8     68 	OUT2CS		= 0x7FC8
                    7FC9     69 	OUT2BC		= 0x7FC9
                    7FCA     70 	OUT3CS		= 0x7FCA
                    7FCB     71 	OUT3BC		= 0x7FCB
                    7FCC     72 	OUT4CS		= 0x7FCC
                    7FCD     73 	OUT4BC		= 0x7FCD
                    7FCE     74 	OUT5CS		= 0x7FCE
                    7FCF     75 	OUT5BC		= 0x7FCF
                    7FD0     76 	OUT6CS		= 0x7FD0
                    7FD1     77 	OUT6BC		= 0x7FD1
                    7FD2     78 	OUT7CS		= 0x7FD2
                    7FD3     79 	OUT7BC		= 0x7FD3
                             80 
                    7FA8     81 	IVEC		= 0x7FA8
                    7FA9     82 	IN07IRQ		= 0x7FA9
                    7FAA     83 	OUT07IRQ	= 0x7FAA
                    7FAB     84 	USBIRQ		= 0x7FAB
                    7FAC     85 	IN07IEN		= 0x7FAC
                    7FAD     86 	OUT07IEN	= 0x7FAD
                    7FAE     87 	USBIEN		= 0x7FAE
                    7FAF     88 	USBBAV		= 0x7FAF
                    7FB2     89 	BPADDRH		= 0x7FB2
                    7FB3     90 	BPADDRL		= 0x7FB3
                             91 
                    7FD4     92 	SUDPTRH		= 0x7FD4
                    7FD5     93 	SUDPTRL		= 0x7FD5
                    7FD6     94 	USBCS		= 0x7FD6
                    7FD7     95 	TOGCTL		= 0x7FD7
                    7FD8     96 	USBFRAMEL	= 0x7FD8
                    7FD9     97 	USBFRAMEH	= 0x7FD9
                    7FDB     98 	FNADDR		= 0x7FDB
                    7FDD     99 	USBPAIR		= 0x7FDD
                    7FDE    100 	IN07VAL		= 0x7FDE
                    7FDF    101 	OUT07VAL	= 0x7FDF
                    7FE3    102 	AUTOPTRH	= 0x7FE3
                    7FE4    103 	AUTOPTRL	= 0x7FE4
                    7FE5    104 	AUTODATA	= 0x7FE5
                            105 
                            106 	;; isochronous endpoints. only available if ISODISAB=0
                            107 
                    7F60    108 	OUT8DATA	= 0x7F60
                    7F61    109 	OUT9DATA	= 0x7F61
                    7F62    110 	OUT10DATA	= 0x7F62
                    7F63    111 	OUT11DATA	= 0x7F63
                    7F64    112 	OUT12DATA	= 0x7F64
                    7F65    113 	OUT13DATA	= 0x7F65
                    7F66    114 	OUT14DATA	= 0x7F66
                    7F67    115 	OUT15DATA	= 0x7F67
                            116 
                    7F68    117 	IN8DATA		= 0x7F68
                    7F69    118 	IN9DATA		= 0x7F69
                    7F6A    119 	IN10DATA	= 0x7F6A
                    7F6B    120 	IN11DATA	= 0x7F6B
                    7F6C    121 	IN12DATA	= 0x7F6C
                    7F6D    122 	IN13DATA	= 0x7F6D
                    7F6E    123 	IN14DATA	= 0x7F6E
                    7F6F    124 	IN15DATA	= 0x7F6F
                            125 
                    7F70    126 	OUT8BCH		= 0x7F70
                    7F71    127 	OUT8BCL		= 0x7F71
                    7F72    128 	OUT9BCH		= 0x7F72
                    7F73    129 	OUT9BCL		= 0x7F73
                    7F74    130 	OUT10BCH	= 0x7F74
                    7F75    131 	OUT10BCL	= 0x7F75
                    7F76    132 	OUT11BCH	= 0x7F76
                    7F77    133 	OUT11BCL	= 0x7F77
                    7F78    134 	OUT12BCH	= 0x7F78
                    7F79    135 	OUT12BCL	= 0x7F79
                    7F7A    136 	OUT13BCH	= 0x7F7A
                    7F7B    137 	OUT13BCL	= 0x7F7B
                    7F7C    138 	OUT14BCH	= 0x7F7C
                    7F7D    139 	OUT14BCL	= 0x7F7D
                    7F7E    140 	OUT15BCH	= 0x7F7E
                    7F7F    141 	OUT15BCL	= 0x7F7F
                            142 
                    7FF0    143 	OUT8ADDR	= 0x7FF0
                    7FF1    144 	OUT9ADDR	= 0x7FF1
                    7FF2    145 	OUT10ADDR	= 0x7FF2
                    7FF3    146 	OUT11ADDR	= 0x7FF3
                    7FF4    147 	OUT12ADDR	= 0x7FF4
                    7FF5    148 	OUT13ADDR	= 0x7FF5
                    7FF6    149 	OUT14ADDR	= 0x7FF6
                    7FF7    150 	OUT15ADDR	= 0x7FF7
                    7FF8    151 	IN8ADDR		= 0x7FF8
                    7FF9    152 	IN9ADDR		= 0x7FF9
                    7FFA    153 	IN10ADDR	= 0x7FFA
                    7FFB    154 	IN11ADDR	= 0x7FFB
                    7FFC    155 	IN12ADDR	= 0x7FFC
                    7FFD    156 	IN13ADDR	= 0x7FFD
                    7FFE    157 	IN14ADDR	= 0x7FFE
                    7FFF    158 	IN15ADDR	= 0x7FFF
                            159 
                    7FA0    160 	ISOERR		= 0x7FA0
                    7FA1    161 	ISOCTL		= 0x7FA1
                    7FA2    162 	ZBCOUNT		= 0x7FA2
                    7FE0    163 	INISOVAL	= 0x7FE0
                    7FE1    164 	OUTISOVAL	= 0x7FE1
                    7FE2    165 	FASTXFR		= 0x7FE2
                            166 
                            167 	;; CPU control registers
                            168 
                    7F92    169 	CPUCS		= 0x7F92
                            170 
                            171 	;; IO port control registers
                            172 
                    7F93    173 	PORTACFG	= 0x7F93
                    7F94    174 	PORTBCFG	= 0x7F94
                    7F95    175 	PORTCCFG	= 0x7F95
                    7F96    176 	OUTA		= 0x7F96
                    7F97    177 	OUTB		= 0x7F97
                    7F98    178 	OUTC		= 0x7F98
                    7F99    179 	PINSA		= 0x7F99
                    7F9A    180 	PINSB		= 0x7F9A
                    7F9B    181 	PINSC		= 0x7F9B
                    7F9C    182 	OEA		= 0x7F9C
                    7F9D    183 	OEB		= 0x7F9D
                    7F9E    184 	OEC		= 0x7F9E
                            185 
                            186 	;; I2C controller registers
                            187 
                    7FA5    188 	I2CS		= 0x7FA5
                    7FA6    189 	I2DAT		= 0x7FA6
                            190 
                            191 	;; FPGA defines
                    0003    192 	XC4K_IRLENGTH	= 3
                    0000    193 	XC4K_EXTEST	= 0
                    0001    194 	XC4K_PRELOAD	= 1
                    0005    195 	XC4K_CONFIGURE	= 5
                    0007    196 	XC4K_BYPASS	= 7
                            197 
                    2E64    198 	FPGA_CONFIGSIZE		= 11876
                    0158    199 	FPGA_BOUND		= 344			; in bits
                    002A    200 	FPGA_BOUND_BYTES	= ((FPGA_BOUND-1)/8)
                    0008    201 	FPGA_BOUND_RESIDUE	= (FPGA_BOUND-8*FPGA_BOUND_BYTES)
                    002B    202 	FPGA_BOUND_BSIZE	= ((FPGA_BOUND+7)/8)
                            203 
                            204 	
                            205 	;; -----------------------------------------------------
                            206 
                            207 	.area CODE (CODE)
   0000 02 09 17            208 	ljmp	startup
   0003 02 01 BD            209 	ljmp	int0_isr
   0006                     210 	.ds	5
   000B 02 01 DE            211 	ljmp	timer0_isr
   000E                     212 	.ds	5
   0013 02 02 0E            213 	ljmp	int1_isr
   0016                     214 	.ds	5
   001B 02 02 2F            215 	ljmp	timer1_isr
   001E                     216 	.ds	5
   0023 02 02 50            217 	ljmp	ser0_isr
   0026                     218 	.ds	5
   002B 02 02 73            219 	ljmp	timer2_isr
   002E                     220 	.ds	5
   0033 02 02 94            221 	ljmp	resume_isr
   0036                     222 	.ds	5
   003B 02 02 B5            223 	ljmp	ser1_isr
   003E                     224 	.ds	5
   0043 02 01 00            225 	ljmp	usb_isr
   0046                     226 	.ds	5
   004B 02 02 D8            227 	ljmp	i2c_isr
   004E                     228 	.ds	5
   0053 02 02 FD            229 	ljmp	int4_isr
   0056                     230 	.ds	5
   005B 02 03 22            231 	ljmp	int5_isr
   005E                     232 	.ds	5
   0063 02 03 47            233 	ljmp	int6_isr
   0066                     234 	.ds	0x8a
                            235 
   00F0                     236 stringserial:
   00F0 0E                  237 	.db	stringserialsz		; bLength
   00F1 03                  238 	.db	USB_DT_STRING		; bDescriptorType
   00F2 30 00 30 00 30 00   239 	.db	'0,0,'0,0,'0,0,'0,0,'0,0,'1,0
        30 00 30 00 31 00
                    000E    240 stringserialsz = . - stringserial
   00FE                     241 	.ds	0x10-stringserialsz
                            242 
   0100                     243 usb_isr:
   0100 02 03 B9            244 	ljmp	usb_sudav_isr
   0103                     245 	.ds	1
   0104 02 05 17            246 	ljmp	usb_sof_isr
   0107                     247 	.ds	1
   0108 02 05 42            248 	ljmp	usb_sutok_isr
   010B                     249 	.ds	1
   010C 02 05 6D            250 	ljmp	usb_suspend_isr
   010F                     251 	.ds	1
   0110 02 05 98            252 	ljmp	usb_usbreset_isr
   0113                     253 	.ds	1
   0114 32                  254 	reti
   0115                     255 	.ds	3
   0118 02 05 C3            256 	ljmp	usb_ep0in_isr
   011B                     257 	.ds	1
   011C 02 06 19            258 	ljmp	usb_ep0out_isr
   011F                     259 	.ds	1
   0120 02 06 BA            260 	ljmp	usb_ep1in_isr
   0123                     261 	.ds	1
   0124 02 06 E8            262 	ljmp	usb_ep1out_isr
   0127                     263 	.ds	1
   0128 02 07 13            264 	ljmp	usb_ep2in_isr
   012B                     265 	.ds	1
   012C 02 07 3E            266 	ljmp	usb_ep2out_isr
   012F                     267 	.ds	1
   0130 02 07 69            268 	ljmp	usb_ep3in_isr
   0133                     269 	.ds	1
   0134 02 07 94            270 	ljmp	usb_ep3out_isr
   0137                     271 	.ds	1
   0138 02 07 BF            272 	ljmp	usb_ep4in_isr
   013B                     273 	.ds	1
   013C 02 07 EA            274 	ljmp	usb_ep4out_isr
   013F                     275 	.ds	1
   0140 02 08 15            276 	ljmp	usb_ep5in_isr
   0143                     277 	.ds	1
   0144 02 08 40            278 	ljmp	usb_ep5out_isr
   0147                     279 	.ds	1
   0148 02 08 6B            280 	ljmp	usb_ep6in_isr
   014B                     281 	.ds	1
   014C 02 08 96            282 	ljmp	usb_ep6out_isr
   014F                     283 	.ds	1
   0150 02 08 C1            284 	ljmp	usb_ep7in_isr
   0153                     285 	.ds	1
   0154 02 08 EC            286 	ljmp	usb_ep7out_isr
                            287 
                            288 	;; -----------------------------------------------------
                            289 
                            290 	.area	OSEG (OVR,DATA)
                            291 	.area	BSEG (BIT)
                            292 
                            293 
                            294 	.area	ISEG (DATA)
   0080                     295 stack:		.ds	0x80
                            296 
                            297 	.area	DSEG (DATA)
   0040                     298 errcode:	.ds	1
   0041                     299 errval:		.ds	1
   0042                     300 cfgcount:	.ds	2
   0044                     301 leddiv:		.ds	1
   0045                     302 irqcount:	.ds	1
   0046                     303 ctrlcode:	.ds	1
   0047                     304 ctrladdr:	.ds	2
   0049                     305 ctrllen:	.ds	2
                            306 
                            307 	.area	XSEG (DATA)
   1000 FF FF FF FF FF FF   308 fpgabound:	.db	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
        FF FF
   1008 FF FF FF FF FF FF   309 		.db	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
        FF FF
   1010 FF CF FF FF FF FF   310 		.db	0xff,0xcf,0xff,0xff,0xff,0xff,0xff,0xff
        FF FF
   1018 FF FF 7F FF FF FF   311 		.db	0xff,0xff,0x7f,0xff,0xff,0xff,0xff,0xff
        FF FF
   1020 FF FF FF FF FF FF   312 		.db	0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff
        FF FF
   1028 FF FF FF FF         313 		.db	0xff,0xff,0xff,0xff
                            314 
                            315 	.area	GSINIT (CODE)
   0917                     316 startup:
   0917 75 81 80            317 	mov	sp,#stack	; -1
   091A E4                  318 	clr	a
   091B F5 D0               319 	mov	psw,a
   091D F5 86               320 	mov	dps,a
                            321 	;lcall	__sdcc_external_startup
                            322 	;mov	a,dpl0
                            323 	;jz	__sdcc_init_data
                            324 	;ljmp	__sdcc_program_startup
   091F                     325 __sdcc_init_data:
                            326 
                            327 	.area	GSINIT2 (CODE)
                            328 
                    0002    329 	ar2 = 0x02
                    0003    330 	ar3 = 0x03
                    0004    331 	ar4 = 0x04
                    0005    332 	ar5 = 0x05
                    0006    333 	ar6 = 0x06
                    0007    334 	ar7 = 0x07
                    0000    335 	ar0 = 0x00
                    0001    336 	ar1 = 0x01
                            337 
   091F                     338 __sdcc_program_startup:
                            339 	;; assembler code startup
   091F E4                  340 	clr	a
   0920 F5 40               341 	mov	errcode,a
   0922 F5 41               342 	mov	errval,a
   0924 F5 42               343 	mov	cfgcount,a
   0926 F5 43               344 	mov	cfgcount+1,a
   0928 F5 45               345  	mov	irqcount,a
   092A F5 46               346  	mov	ctrlcode,a
                            347 	;; some indirect register setup
   092C 75 8E 31            348 	mov	ckcon,#0x31	; one external wait state, to avoid chip bugs
                            349 	;; Timer setup:
                            350 	;; timer 0: LED blink    : T0M=0, CT0=0, MODE0=1
                            351 	;; timer 1: Baudrate gen : T1M=1, CT1=0, MODE1=2
   092F 75 89 21            352 	mov	tmod,#0x21
   0932 75 88 55            353 	mov	tcon,#0x55	; INT0/INT1 edge
   0935 75 8D 64            354 	mov	th1,#256-156	; 1200 bauds
   0938 75 87 00            355 	mov	pcon,#0		; SMOD0=0
                            356 	;; init USB subsystem
   093B 90 7F A1            357 	mov	dptr,#ISOCTL
   093E 74 01               358 	mov	a,#1		; disable ISO endpoints
   0940 F0                  359 	movx	@dptr,a
   0941 90 7F AF            360 	mov	dptr,#USBBAV
   0944 74 01               361 	mov	a,#1		; enable autovector, disable breakpoint logic
   0946 F0                  362 	movx	@dptr,a
   0947 E4                  363 	clr	a
   0948 90 7F E0            364 	mov	dptr,#INISOVAL
   094B F0                  365 	movx	@dptr,a
   094C 90 7F E1            366 	mov	dptr,#OUTISOVAL
   094F F0                  367 	movx	@dptr,a
   0950 90 7F DD            368 	mov	dptr,#USBPAIR
   0953 74 09               369 	mov	a,#0x9		; pair EP 2&3 for input & output
   0955 F0                  370 	movx	@dptr,a
   0956 90 7F DE            371 	mov	dptr,#IN07VAL
   0959 74 03               372 	mov	a,#0x3		; enable EP0+EP1
   095B F0                  373 	movx	@dptr,a
   095C 90 7F DF            374 	mov	dptr,#OUT07VAL
   095F 74 05               375 	mov	a,#0x5		; enable EP0+EP2
   0961 F0                  376 	movx	@dptr,a
                            377 	;; USB:	init endpoint toggles
   0962 90 7F D7            378 	mov	dptr,#TOGCTL
   0965 74 12               379 	mov	a,#0x12
   0967 F0                  380 	movx	@dptr,a
   0968 74 32               381 	mov	a,#0x32		; clear EP 2 in toggle
   096A F0                  382 	movx	@dptr,a
   096B 74 02               383 	mov	a,#0x02
   096D F0                  384 	movx	@dptr,a
   096E 74 22               385 	mov	a,#0x22		; clear EP 2 out toggle
   0970 F0                  386 	movx	@dptr,a
                            387 	;; configure IO ports
   0971 90 7F 93            388 	mov	dptr,#PORTACFG
   0974 74 00               389 	mov	a,#0
   0976 F0                  390 	movx	@dptr,a
   0977 90 7F 96            391 	mov	dptr,#OUTA
   097A 74 80               392 	mov	a,#0x80		; set PROG lo
   097C F0                  393 	movx	@dptr,a
   097D 90 7F 9C            394 	mov	dptr,#OEA
   0980 74 C2               395 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   0982 F0                  396 	movx	@dptr,a
   0983 90 7F 94            397 	mov	dptr,#PORTBCFG
   0986 74 00               398 	mov	a,#0
   0988 F0                  399 	movx	@dptr,a
   0989 90 7F 9D            400 	mov	dptr,#OEB
   098C 74 00               401 	mov	a,#0
   098E F0                  402 	movx	@dptr,a
   098F 90 7F 95            403 	mov	dptr,#PORTCCFG
   0992 74 00               404 	mov	a,#0
   0994 F0                  405 	movx	@dptr,a
   0995 90 7F 98            406 	mov	dptr,#OUTC
   0998 74 20               407 	mov	a,#0x20
   099A F0                  408 	movx	@dptr,a
   099B 90 7F 9E            409 	mov	dptr,#OEC
   099E 74 2E               410 	mov	a,#0x2e		; out: LEDCON,LEDSTA,TCK,INIT  in: TDO
   09A0 F0                  411 	movx	@dptr,a
                            412 	;; enable interrupts
   09A1 75 A8 82            413 	mov	ie,#0x82	; enable timer 0 int
   09A4 75 E8 01            414 	mov	eie,#0x01	; enable USB interrupts
   09A7 90 7F AE            415 	mov	dptr,#USBIEN
   09AA 74 01               416 	mov	a,#1		; enable SUDAV interrupt
   09AC F0                  417 	movx	@dptr,a
   09AD 90 7F AC            418 	mov	dptr,#IN07IEN
   09B0 74 01               419 	mov	a,#1		; enable EP0 interrupt
   09B2 F0                  420 	movx	@dptr,a
   09B3 90 7F AD            421 	mov	dptr,#OUT07IEN
   09B6 74 03               422 	mov	a,#3		; enable EP0+EP1 interrupt
   09B8 F0                  423 	movx	@dptr,a
                            424 	;; initialize UART 0 for config loading
   09B9 75 98 20            425 	mov	scon0,#0x20	; mode 0, CLK24/4
   09BC 75 99 FF            426 	mov	sbuf0,#0xff
                            427 	;; initialize EP1 IN (irq)
   09BF 12 06 9B            428 	lcall	fillusbintr
                            429 
                            430 	;; some short delay
   09C2 90 7F 96            431 	mov	dptr,#OUTA
   09C5 74 82               432 	mov	a,#0x82		; set PROG hi
   09C7 F0                  433 	movx	@dptr,a
   09C8 90 7F 99            434 	mov	dptr,#PINSA
   09CB E0                  435 	movx	a,@dptr
   09CC 30 E2 06            436 	jnb	acc.2,1$
                            437 	;; DONE stuck high
   09CF 75 40 80            438 	mov	errcode,#0x80
   09D2 02 0A 3D            439 	ljmp	fpgaerr
   09D5 12 01 57            440 1$:	lcall	jtag_reset_tap
   09D8 7A 05               441 	mov	r2,#5
   09DA 7B 00               442 	mov	r3,#0
   09DC 7C 06               443 	mov	r4,#6
   09DE 12 01 5D            444 	lcall	jtag_shiftout	; enter SHIFT-IR state
   09E1 7A 08               445 	mov	r2,#8
   09E3 7B 00               446 	mov	r3,#0
   09E5 7C 00               447 	mov	r4,#0
   09E7 12 01 5D            448 	lcall	jtag_shiftout	; assume max. 8bit IR
   09EA 7A 08               449 	mov	r2,#8
   09EC 7B 01               450 	mov	r3,#1
   09EE 7C 00               451 	mov	r4,#0
   09F0 12 01 5D            452 	lcall	jtag_shift	; shift in a single bit
   09F3 74 F8               453 	mov	a,#-(1<<XC4K_IRLENGTH)
   09F5 2F                  454 	add	a,r7
   09F6 60 08               455 	jz	2$
                            456 	;; JTAG instruction register error
   09F8 75 40 81            457 	mov	errcode,#0x81
   09FB 8F 41               458 	mov	errval,r7
   09FD 02 0A 3D            459 	ljmp	fpgaerr
   0A00 7A 07               460 2$:	mov	r2,#XC4K_IRLENGTH+4
   0A02 7B 01               461 	mov	r3,#XC4K_PRELOAD
   0A04 7C 1C               462 	mov	r4,#7<<(XC4K_IRLENGTH-1)
   0A06 12 01 5D            463 	lcall	jtag_shiftout	; shift in PRELOAD insn, goto SHIFTDR state
   0A09 12 01 8B            464 	lcall	boundary
   0A0C 7A 05               465 	mov	r2,#5
   0A0E 7B 00               466 	mov	r3,#0
   0A10 7C 07               467 	mov	r4,#7
   0A12 12 01 5D            468 	lcall	jtag_shiftout	; goto SHIFTIR state
   0A15 7A 05               469 	mov	r2,#XC4K_IRLENGTH+2
   0A17 7B 00               470 	mov	r3,#XC4K_EXTEST
   0A19 7C 0C               471 	mov	r4,#3<<(XC4K_IRLENGTH-1)
   0A1B 12 01 5D            472 	lcall	jtag_shiftout	; shift in EXTEST insn, goto RUNTEST/IDLE state
   0A1E                     473 boundaryloop1:
   0A1E 75 40 20            474 	mov	errcode,#0x20
   0A21                     475 boundaryloop:
   0A21 E5 40               476 	mov	a,errcode
   0A23 B4 21 FB            477 	cjne	a,#0x21,boundaryloop
   0A26 7A 03               478 	mov	r2,#3
   0A28 7B 00               479 	mov	r3,#0
   0A2A 7C 01               480 	mov	r4,#1
   0A2C 12 01 5D            481 	lcall	jtag_shiftout
   0A2F 12 01 8B            482 	lcall	boundary
   0A32 7A 02               483 	mov	r2,#2
   0A34 7B 00               484 	mov	r3,#0
   0A36 7C 01               485 	mov	r4,#1
   0A38 12 01 5D            486 	lcall	jtag_shiftout
   0A3B 80 E1               487 	sjmp	boundaryloop1
                            488 
   0A3D                     489 fpgaerr:
   0A3D 80 FE               490 	sjmp	fpgaerr
                            491 
                            492 	.area	CSEG (CODE)
                    0002    493 	ar2 = 0x02
                    0003    494 	ar3 = 0x03
                    0004    495 	ar4 = 0x04
                    0005    496 	ar5 = 0x05
                    0006    497 	ar6 = 0x06
                    0007    498 	ar7 = 0x07
                    0000    499 	ar0 = 0x00
                    0001    500 	ar1 = 0x01
                            501 
                            502 	;; jtag_shift
                            503 	;; r2 = num
                            504 	;; r3 = tdi
                            505 	;; r4 = tms
                            506 	;; return: r7 = tdo
   0157                     507 jtag_reset_tap:
   0157 7A 05               508 	mov	r2,#5
   0159 7B 00               509 	mov	r3,#0
   015B 7C FF               510 	mov	r4,#0xff
   015D                     511 jtag_shiftout:
   015D                     512 jtag_shift:
   015D 7E 01               513 	mov	r6,#1
   015F 7F 00               514 	mov	r7,#0
   0161 74 02               515 1$:	mov	a,#2
   0163 CB                  516 	xch	a,r3
   0164 13                  517 	rrc	a
   0165 CB                  518 	xch	a,r3
   0166 92 E6               519 	mov	acc.6,c
   0168 CC                  520 	xch	a,r4
   0169 13                  521 	rrc	a
   016A CC                  522 	xch	a,r4
   016B 92 E7               523 	mov	acc.7,c
   016D 90 7F 96            524 	mov	dptr,#OUTA
   0170 F0                  525 	movx	@dptr,a
   0171 90 7F 9B            526 	mov	dptr,#PINSC
   0174 E0                  527 	movx	a,@dptr
   0175 30 E0 03            528 	jnb	acc.0,2$
   0178 EE                  529 	mov	a,r6
   0179 42 07               530 	orl	ar7,a
   017B 90 7F 98            531 2$:	mov	dptr,#OUTC
   017E E0                  532 	movx	a,@dptr
   017F D2 E1               533 	setb	acc.1
   0181 F0                  534 	movx	@dptr,a
   0182 C2 E1               535 	clr	acc.1
   0184 F0                  536 	movx	@dptr,a
   0185 EE                  537 	mov	a,r6
   0186 23                  538 	rl	a
   0187 FE                  539 	mov	r6,a
   0188 DA D7               540 	djnz	r2,1$
   018A 22                  541 	ret
                            542 
   018B                     543 boundary:
   018B 75 86 00            544 	mov	dps,#0
   018E 90 10 00            545 	mov	dptr,#fpgabound
   0191 78 2A               546 	mov	r0,#FPGA_BOUND_BYTES
   0193 7A 08               547 1$:	mov	r2,#8
   0195 E0                  548 	movx	a,@dptr
   0196 FB                  549 	mov	r3,a
   0197 7C 00               550 	mov	r4,#0	
   0199 C0 82               551 	push	dpl0
   019B C0 83               552 	push	dph0
   019D 12 01 5D            553 	lcall	jtag_shift
   01A0 D0 83               554 	pop	dph0
   01A2 D0 82               555 	pop	dpl0
   01A4 EF                  556 	mov	a,r7
   01A5 F0                  557 	movx	@dptr,a
   01A6 A3                  558 	inc	dptr
   01A7 D8 EA               559 	djnz	r0,1$
   01A9 7A 08               560 	mov	r2,#FPGA_BOUND_RESIDUE
   01AB E0                  561 	movx	a,@dptr
   01AC FB                  562 	mov	r3,a
   01AD 7C 80               563 	mov	r4,#(1<<(FPGA_BOUND_RESIDUE-1))
   01AF C0 82               564 	push	dpl0
   01B1 C0 83               565 	push	dph0
   01B3 12 01 5D            566 	lcall	jtag_shift
   01B6 D0 83               567 	pop	dph0
   01B8 D0 82               568 	pop	dpl0
   01BA EF                  569 	mov	a,r7
   01BB F0                  570 	movx	@dptr,a
   01BC 22                  571 	ret
                            572 	
                            573 	;; ------------------ interrupt handlers ------------------------
                            574 
   01BD                     575 int0_isr:
   01BD C0 E0               576 	push	acc
   01BF C0 F0               577 	push	b
   01C1 C0 82               578 	push	dpl0
   01C3 C0 83               579 	push	dph0
   01C5 C0 D0               580 	push	psw
   01C7 75 D0 00            581 	mov	psw,#0x00
   01CA C0 86               582 	push	dps
   01CC 75 86 00            583 	mov	dps,#0
                            584 	;; clear interrupt
   01CF C2 89               585 	clr	tcon+1
                            586 	;; handle interrupt
                            587 	;; epilogue
   01D1 D0 86               588 	pop	dps
   01D3 D0 D0               589 	pop	psw
   01D5 D0 83               590 	pop	dph0
   01D7 D0 82               591 	pop	dpl0
   01D9 D0 F0               592 	pop	b
   01DB D0 E0               593 	pop	acc
   01DD 32                  594 	reti
                            595 
   01DE                     596 timer0_isr:
   01DE C0 E0               597 	push	acc
   01E0 C0 F0               598 	push	b
   01E2 C0 82               599 	push	dpl0
   01E4 C0 83               600 	push	dph0
   01E6 C0 D0               601 	push	psw
   01E8 75 D0 00            602 	mov	psw,#0x00
   01EB C0 86               603 	push	dps
   01ED 75 86 00            604 	mov	dps,#0
                            605 	;; clear interrupt
   01F0 C2 8D               606 	clr	tcon+5
                            607 	;; handle interrupt
   01F2 05 44               608 	inc	leddiv
   01F4 E5 44               609 	mov	a,leddiv
   01F6 54 07               610 	anl	a,#7
   01F8 70 07               611 	jnz	0$
   01FA 90 7F 98            612 	mov	dptr,#OUTC
   01FD E0                  613 	movx	a,@dptr
   01FE 64 08               614 	xrl	a,#0x08
   0200 F0                  615 	movx	@dptr,a
   0201                     616 0$:
                            617 	;; epilogue
   0201 D0 86               618 	pop	dps
   0203 D0 D0               619 	pop	psw
   0205 D0 83               620 	pop	dph0
   0207 D0 82               621 	pop	dpl0
   0209 D0 F0               622 	pop	b
   020B D0 E0               623 	pop	acc
   020D 32                  624 	reti
                            625 
   020E                     626 int1_isr:
   020E C0 E0               627 	push	acc
   0210 C0 F0               628 	push	b
   0212 C0 82               629 	push	dpl0
   0214 C0 83               630 	push	dph0
   0216 C0 D0               631 	push	psw
   0218 75 D0 00            632 	mov	psw,#0x00
   021B C0 86               633 	push	dps
   021D 75 86 00            634 	mov	dps,#0
                            635 	;; clear interrupt
   0220 C2 8B               636 	clr	tcon+3
                            637 	;; handle interrupt
                            638 	;; epilogue
   0222 D0 86               639 	pop	dps
   0224 D0 D0               640 	pop	psw
   0226 D0 83               641 	pop	dph0
   0228 D0 82               642 	pop	dpl0
   022A D0 F0               643 	pop	b
   022C D0 E0               644 	pop	acc
   022E 32                  645 	reti
                            646 
   022F                     647 timer1_isr:
   022F C0 E0               648 	push	acc
   0231 C0 F0               649 	push	b
   0233 C0 82               650 	push	dpl0
   0235 C0 83               651 	push	dph0
   0237 C0 D0               652 	push	psw
   0239 75 D0 00            653 	mov	psw,#0x00
   023C C0 86               654 	push	dps
   023E 75 86 00            655 	mov	dps,#0
                            656 	;; clear interrupt
   0241 C2 8F               657 	clr	tcon+7
                            658 	;; handle interrupt
                            659 	;; epilogue
   0243 D0 86               660 	pop	dps
   0245 D0 D0               661 	pop	psw
   0247 D0 83               662 	pop	dph0
   0249 D0 82               663 	pop	dpl0
   024B D0 F0               664 	pop	b
   024D D0 E0               665 	pop	acc
   024F 32                  666 	reti
                            667 
   0250                     668 ser0_isr:
   0250 C0 E0               669 	push	acc
   0252 C0 F0               670 	push	b
   0254 C0 82               671 	push	dpl0
   0256 C0 83               672 	push	dph0
   0258 C0 D0               673 	push	psw
   025A 75 D0 00            674 	mov	psw,#0x00
   025D C0 86               675 	push	dps
   025F 75 86 00            676 	mov	dps,#0
                            677 	;; clear interrupt
   0262 C2 98               678 	clr	scon0+0
   0264 C2 99               679 	clr	scon0+1
                            680 	;; handle interrupt
                            681 	;; epilogue
   0266 D0 86               682 	pop	dps
   0268 D0 D0               683 	pop	psw
   026A D0 83               684 	pop	dph0
   026C D0 82               685 	pop	dpl0
   026E D0 F0               686 	pop	b
   0270 D0 E0               687 	pop	acc
   0272 32                  688 	reti
                            689 
   0273                     690 timer2_isr:
   0273 C0 E0               691 	push	acc
   0275 C0 F0               692 	push	b
   0277 C0 82               693 	push	dpl0
   0279 C0 83               694 	push	dph0
   027B C0 D0               695 	push	psw
   027D 75 D0 00            696 	mov	psw,#0x00
   0280 C0 86               697 	push	dps
   0282 75 86 00            698 	mov	dps,#0
                            699 	;; clear interrupt
   0285 C2 CF               700 	clr	t2con+7
                            701 	;; handle interrupt
                            702 	;; epilogue
   0287 D0 86               703 	pop	dps
   0289 D0 D0               704 	pop	psw
   028B D0 83               705 	pop	dph0
   028D D0 82               706 	pop	dpl0
   028F D0 F0               707 	pop	b
   0291 D0 E0               708 	pop	acc
   0293 32                  709 	reti
                            710 
   0294                     711 resume_isr:
   0294 C0 E0               712 	push	acc
   0296 C0 F0               713 	push	b
   0298 C0 82               714 	push	dpl0
   029A C0 83               715 	push	dph0
   029C C0 D0               716 	push	psw
   029E 75 D0 00            717 	mov	psw,#0x00
   02A1 C0 86               718 	push	dps
   02A3 75 86 00            719 	mov	dps,#0
                            720 	;; clear interrupt
   02A6 C2 DC               721 	clr	eicon+4
                            722 	;; handle interrupt
                            723 	;; epilogue
   02A8 D0 86               724 	pop	dps
   02AA D0 D0               725 	pop	psw
   02AC D0 83               726 	pop	dph0
   02AE D0 82               727 	pop	dpl0
   02B0 D0 F0               728 	pop	b
   02B2 D0 E0               729 	pop	acc
   02B4 32                  730 	reti
                            731 
   02B5                     732 ser1_isr:
   02B5 C0 E0               733 	push	acc
   02B7 C0 F0               734 	push	b
   02B9 C0 82               735 	push	dpl0
   02BB C0 83               736 	push	dph0
   02BD C0 D0               737 	push	psw
   02BF 75 D0 00            738 	mov	psw,#0x00
   02C2 C0 86               739 	push	dps
   02C4 75 86 00            740 	mov	dps,#0
                            741 	;; clear interrupt
   02C7 C2 C0               742 	clr	scon1+0
   02C9 C2 C1               743 	clr	scon1+1
                            744 	;; handle interrupt
                            745 	;; epilogue
   02CB D0 86               746 	pop	dps
   02CD D0 D0               747 	pop	psw
   02CF D0 83               748 	pop	dph0
   02D1 D0 82               749 	pop	dpl0
   02D3 D0 F0               750 	pop	b
   02D5 D0 E0               751 	pop	acc
   02D7 32                  752 	reti
                            753 
   02D8                     754 i2c_isr:
   02D8 C0 E0               755 	push	acc
   02DA C0 F0               756 	push	b
   02DC C0 82               757 	push	dpl0
   02DE C0 83               758 	push	dph0
   02E0 C0 D0               759 	push	psw
   02E2 75 D0 00            760 	mov	psw,#0x00
   02E5 C0 86               761 	push	dps
   02E7 75 86 00            762 	mov	dps,#0
                            763 	;; clear interrupt
   02EA E5 91               764 	mov	a,exif
   02EC C2 E5               765 	clr	acc.5
   02EE F5 91               766 	mov	exif,a
                            767 	;; handle interrupt
                            768 	;; epilogue
   02F0 D0 86               769 	pop	dps
   02F2 D0 D0               770 	pop	psw
   02F4 D0 83               771 	pop	dph0
   02F6 D0 82               772 	pop	dpl0
   02F8 D0 F0               773 	pop	b
   02FA D0 E0               774 	pop	acc
   02FC 32                  775 	reti
                            776 
   02FD                     777 int4_isr:
   02FD C0 E0               778 	push	acc
   02FF C0 F0               779 	push	b
   0301 C0 82               780 	push	dpl0
   0303 C0 83               781 	push	dph0
   0305 C0 D0               782 	push	psw
   0307 75 D0 00            783 	mov	psw,#0x00
   030A C0 86               784 	push	dps
   030C 75 86 00            785 	mov	dps,#0
                            786 	;; clear interrupt
   030F E5 91               787 	mov	a,exif
   0311 C2 E6               788 	clr	acc.6
   0313 F5 91               789 	mov	exif,a
                            790 	;; handle interrupt
                            791 	;; epilogue
   0315 D0 86               792 	pop	dps
   0317 D0 D0               793 	pop	psw
   0319 D0 83               794 	pop	dph0
   031B D0 82               795 	pop	dpl0
   031D D0 F0               796 	pop	b
   031F D0 E0               797 	pop	acc
   0321 32                  798 	reti
                            799 
   0322                     800 int5_isr:
   0322 C0 E0               801 	push	acc
   0324 C0 F0               802 	push	b
   0326 C0 82               803 	push	dpl0
   0328 C0 83               804 	push	dph0
   032A C0 D0               805 	push	psw
   032C 75 D0 00            806 	mov	psw,#0x00
   032F C0 86               807 	push	dps
   0331 75 86 00            808 	mov	dps,#0
                            809 	;; clear interrupt
   0334 E5 91               810 	mov	a,exif
   0336 C2 E7               811 	clr	acc.7
   0338 F5 91               812 	mov	exif,a
                            813 	;; handle interrupt
                            814 	;; epilogue
   033A D0 86               815 	pop	dps
   033C D0 D0               816 	pop	psw
   033E D0 83               817 	pop	dph0
   0340 D0 82               818 	pop	dpl0
   0342 D0 F0               819 	pop	b
   0344 D0 E0               820 	pop	acc
   0346 32                  821 	reti
                            822 
   0347                     823 int6_isr:
   0347 C0 E0               824 	push	acc
   0349 C0 F0               825 	push	b
   034B C0 82               826 	push	dpl0
   034D C0 83               827 	push	dph0
   034F C0 D0               828 	push	psw
   0351 75 D0 00            829 	mov	psw,#0x00
   0354 C0 86               830 	push	dps
   0356 75 86 00            831 	mov	dps,#0
                            832 	;; clear interrupt
   0359 C2 DB               833 	clr	eicon+3
                            834 	;; handle interrupt
                            835 	;; epilogue
   035B D0 86               836 	pop	dps
   035D D0 D0               837 	pop	psw
   035F D0 83               838 	pop	dph0
   0361 D0 82               839 	pop	dpl0
   0363 D0 F0               840 	pop	b
   0365 D0 E0               841 	pop	acc
   0367 32                  842 	reti
                            843 
   0368                     844 xmemread::
   0368 E5 49               845 	mov	a,ctrllen
   036A 45 4A               846 	orl	a,ctrllen+1
   036C 70 0A               847 	jnz	4$
   036E E4                  848 2$:	clr	a
   036F F5 46               849 	mov	ctrlcode,a
   0371 74 02               850 	mov	a,#2		; ack control transfer
   0373 90 7F B4            851 	mov	dptr,#EP0CS
   0376 F0                  852 	movx	@dptr,a
   0377 22                  853 	ret
   0378 E5 49               854 4$:	mov	a,ctrllen
   037A FF                  855 	mov	r7,a
   037B 24 C0               856 	add	a,#-64
   037D F5 49               857 	mov	ctrllen,a
   037F E5 4A               858 	mov	a,ctrllen+1
   0381 34 FF               859 	addc	a,#0xff
   0383 40 0A               860 	jc	0$
   0385 E4                  861 	clr	a
   0386 F5 49               862 	mov	ctrllen,a
   0388 F5 4A               863 	mov	ctrllen+1,a
   038A EF                  864 	mov	a,r7
   038B 60 E1               865 	jz	2$
   038D 80 04               866 	sjmp	1$
   038F F5 4A               867 0$:	mov	ctrllen+1,a
   0391 74 40               868 	mov	a,#64
   0393 FF                  869 1$:	mov	r7,a
   0394 F8                  870 	mov	r0,a
   0395 85 47 82            871 	mov	dpl0,ctrladdr
   0398 85 48 83            872 	mov	dph0,ctrladdr+1
   039B 75 86 01            873 	mov	dps,#1
   039E 90 7F 00            874 	mov	dptr,#IN0BUF
   03A1 15 86               875 	dec	dps
   03A3 E0                  876 3$:	movx	a,@dptr
   03A4 A3                  877 	inc	dptr
   03A5 05 86               878 	inc	dps
   03A7 F0                  879 	movx	@dptr,a
   03A8 A3                  880 	inc	dptr
   03A9 15 86               881 	dec	dps
   03AB D8 F6               882 	djnz	r0,3$
   03AD 85 82 47            883 	mov	ctrladdr,dpl0
   03B0 85 83 48            884 	mov	ctrladdr+1,dph0
   03B3 EF                  885 	mov	a,r7
   03B4 90 7F B5            886 	mov	dptr,#IN0BC
   03B7 F0                  887 	movx	@dptr,a
   03B8 22                  888 	ret
                            889 
   03B9                     890 usb_sudav_isr:
   03B9 C0 E0               891 	push	acc
   03BB C0 F0               892 	push	b
   03BD C0 82               893 	push	dpl0
   03BF C0 83               894 	push	dph0
   03C1 C0 84               895 	push	dpl1
   03C3 C0 85               896 	push	dph1
   03C5 C0 D0               897 	push	psw
   03C7 75 D0 00            898 	mov	psw,#0x00
   03CA C0 86               899 	push	dps
   03CC 75 86 00            900 	mov	dps,#0
   03CF C0 00               901 	push	ar0
   03D1 C0 07               902 	push	ar7
                            903 	;; clear interrupt
   03D3 E5 91               904 	mov	a,exif
   03D5 C2 E4               905 	clr	acc.4
   03D7 F5 91               906 	mov	exif,a
   03D9 90 7F AB            907 	mov	dptr,#USBIRQ
   03DC 74 01               908 	mov	a,#0x01
   03DE F0                  909 	movx	@dptr,a
                            910 	;; handle interrupt
   03DF 75 46 00            911 	mov	ctrlcode,#0		; reset control out code
   03E2 90 7F E9            912 	mov	dptr,#SETUPDAT+1
   03E5 E0                  913 	movx	a,@dptr			; bRequest field
                            914 	;; vendor specific commands
                            915 	;; 0xb1
   03E6 B4 B1 3E            916 	cjne	a,#0xb1,cmdnotb1
   03E9 90 7F E8            917 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   03EC E0                  918 	movx	a,@dptr
   03ED B4 C0 34            919 	cjne	a,#0xc0,setupstallb1
   03F0 90 7F 00            920 	mov	dptr,#IN0BUF
   03F3 E5 40               921 	mov	a,errcode
   03F5 F0                  922 	movx	@dptr,a
   03F6 A3                  923 	inc	dptr
   03F7 E5 41               924 	mov	a,errval
   03F9 F0                  925 	movx	@dptr,a
   03FA A3                  926 	inc	dptr
   03FB E5 81               927 	mov	a,sp
                            928 
                            929 ;;; xxxx
   03FD C0 82               930 	push	dpl0
   03FF C0 83               931 	push	dph0
   0401 90 7F DF            932 	mov	dptr,#OUT07VAL
   0404 E0                  933 	movx	a,@dptr
   0405 D0 83               934 	pop	dph0
   0407 D0 82               935 	pop	dpl0
                            936 ;;; xxxx
                            937 	
   0409 F0                  938 	movx	@dptr,a
   040A A3                  939 	inc	dptr
   040B 74 00               940 	mov	a,#0
                            941 
                            942 ;;; xxxx
   040D C0 82               943 	push	dpl0
   040F C0 83               944 	push	dph0
   0411 90 7F C8            945 	mov	dptr,#OUT2CS
   0414 E0                  946 	movx	a,@dptr
   0415 D0 83               947 	pop	dph0
   0417 D0 82               948 	pop	dpl0
                            949 ;;; xxxx
                            950 	
   0419 F0                  951 	movx	@dptr,a
   041A A3                  952 	inc	dptr
   041B 90 7F B5            953 	mov	dptr,#IN0BC
   041E 74 04               954 	mov	a,#4
   0420 F0                  955 	movx	@dptr,a
   0421 02 04 FC            956 	ljmp	setupack
   0424                     957 setupstallb1:
   0424 02 04 F8            958 	ljmp	setupstall
   0427                     959 cmdnotb1:
                            960 	;; 0xb2
   0427 B4 B2 2B            961 	cjne	a,#0xb2,cmdnotb2
   042A 90 7F E8            962 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   042D E0                  963 	movx	a,@dptr
   042E B4 C0 21            964 	cjne	a,#0xc0,setupstallb2
   0431 90 7F 00            965 	mov	dptr,#IN0BUF
   0434 E5 40               966 	mov	a,errcode
   0436 F0                  967 	movx	@dptr,a
   0437 A3                  968 	inc	dptr
   0438 E5 41               969 	mov	a,errval
   043A F0                  970 	movx	@dptr,a
   043B A3                  971 	inc	dptr
   043C E5 42               972 	mov	a,cfgcount
   043E F0                  973 	movx	@dptr,a
   043F A3                  974 	inc	dptr
   0440 E5 43               975 	mov	a,cfgcount+1
   0442 F0                  976 	movx	@dptr,a
   0443 A3                  977 	inc	dptr
   0444 E5 45               978 	mov	a,irqcount
   0446 F0                  979 	movx	@dptr,a
   0447 90 7F B5            980 	mov	dptr,#IN0BC
   044A 74 05               981 	mov	a,#5
   044C F0                  982 	movx	@dptr,a
   044D 05 45               983 	inc	irqcount
   044F 02 04 FC            984 	ljmp	setupack
   0452                     985 setupstallb2:
   0452 02 04 F8            986 	ljmp	setupstall
   0455                     987 cmdnotb2:
                            988 	;; 0xb8
   0455 B4 B8 3D            989 	cjne	a,#0xb8,cmdnotb8
   0458 E5 40               990 	mov	a,errcode
   045A B4 20 F5            991 	cjne	a,#0x20,setupstallb2
   045D 90 7F EE            992 	mov	dptr,#SETUPDAT+6
   0460 E0                  993 	movx	a,@dptr
   0461 B4 2B EE            994 	cjne	a,#FPGA_BOUND_BSIZE,setupstallb2
   0464 A3                  995 	inc	dptr
   0465 E0                  996 	movx	a,@dptr
   0466 B4 00 E9            997 	cjne	a,#FPGA_BOUND_BSIZE>>8,setupstallb2
   0469 75 47 00            998 	mov	ctrladdr,#fpgabound
   046C 75 48 10            999 	mov	ctrladdr+1,#fpgabound>>8
   046F 75 49 2B           1000 	mov	ctrllen,#FPGA_BOUND_BSIZE
   0472 75 4A 00           1001 	mov	ctrllen+1,#FPGA_BOUND_BSIZE>>8
   0475 90 7F E8           1002 	mov	dptr,#SETUPDAT		; bRequestType == 0x40
   0478 E0                 1003 	movx	a,@dptr
   0479 B4 40 0A           1004 	cjne	a,#0x40,1$
   047C 75 46 01           1005 	mov	ctrlcode,#1
   047F 90 7F C5           1006 	mov	dptr,#OUT0BC
   0482 F0                 1007 	movx	@dptr,a
   0483 02 05 02           1008 	ljmp	endusbisr
   0486 B4 C0 09           1009 1$:	cjne	a,#0xc0,setupstallb8	; bRequestType == 0xc0
   0489 75 46 02           1010 	mov	ctrlcode,#2
   048C 12 03 68           1011 	lcall	xmemread
   048F 02 05 02           1012 	ljmp	endusbisr
   0492                    1013 setupstallb8:
   0492 02 04 F8           1014 	ljmp	setupstall
   0495                    1015 cmdnotb8:
                           1016 	;; 0xb9
   0495 B4 B9 17           1017 	cjne	a,#0xb9,cmdnotb9
   0498 90 7F E8           1018 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   049B E0                 1019 	movx	a,@dptr
   049C B4 C0 F3           1020 	cjne	a,#0xc0,setupstallb8
   049F E5 40              1021 	mov	a,errcode
   04A1 B4 20 EE           1022 	cjne	a,#0x20,setupstallb8
   04A4 75 40 21           1023 	mov	errcode,#0x21
   04A7 90 7F B5           1024 	mov	dptr,#IN0BC
   04AA E4                 1025 	clr	a
   04AB F0                 1026 	movx	@dptr,a
   04AC 02 04 FC           1027 	ljmp	setupack
   04AF                    1028 cmdnotb9:
                           1029 	;; 0xc8
   04AF B4 C8 19           1030 	cjne	a,#0xc8,cmdnotc8
   04B2 90 7F E8           1031 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   04B5 E0                 1032 	movx	a,@dptr
   04B6 B4 C0 0F           1033 	cjne	a,#0xc0,setupstallc8
   04B9 74 05              1034 	mov	a,#5
   04BB 90 7F 00           1035 	mov	dptr,#IN0BUF
   04BE F0                 1036 	movx	@dptr,a
   04BF 90 7F B5           1037 	mov	dptr,#IN0BC
   04C2 74 01              1038 	mov	a,#1
   04C4 F0                 1039 	movx	@dptr,a
   04C5 02 04 FC           1040 	ljmp	setupack
   04C8                    1041 setupstallc8:
   04C8 02 04 F8           1042 	ljmp	setupstall
   04CB                    1043 cmdnotc8:
                           1044 	;; 0xc9
   04CB B4 C9 2A           1045 	cjne	a,#0xc9,cmdnotc9
   04CE 90 7F E8           1046 	mov	dptr,#SETUPDAT	; bRequestType == 0xc0
   04D1 E0                 1047 	movx	a,@dptr
   04D2 B4 C0 20           1048 	cjne	a,#0xc0,setupstallc9
   04D5 90 7F 00           1049 	mov	dptr,#IN0BUF
   04D8 05 86              1050 	inc	dps
   04DA 90 00 F2           1051 	mov	dptr,#stringserial+2
   04DD 7F 06              1052 	mov	r7,#(stringserialsz-2)/2
   04DF E0                 1053 0$:	movx	a,@dptr
   04E0 A3                 1054 	inc	dptr
   04E1 A3                 1055 	inc	dptr
   04E2 15 86              1056 	dec	dps
   04E4 F0                 1057 	movx	@dptr,a
   04E5 A3                 1058 	inc	dptr
   04E6 05 86              1059 	inc	dps
   04E8 DF F5              1060 	djnz	r7,0$
   04EA 15 86              1061 	dec	dps
   04EC 74 06              1062 	mov	a,#(stringserialsz-2)/2
   04EE 90 7F B5           1063 	mov	dptr,#IN0BC
   04F1 F0                 1064 	movx	@dptr,a
   04F2 02 04 FC           1065 	ljmp	setupack
   04F5                    1066 setupstallc9:
   04F5 02 04 F8           1067 	ljmp	setupstall
   04F8                    1068 cmdnotc9:
                           1069 	;; unknown commands fall through to setupstall
                           1070 
   04F8                    1071 setupstall:
   04F8 74 03              1072 	mov	a,#3
   04FA 80 02              1073 	sjmp	endsetup
   04FC                    1074 setupack:
   04FC 74 02              1075 	mov	a,#2
   04FE                    1076 endsetup:
   04FE 90 7F B4           1077 	mov	dptr,#EP0CS
   0501 F0                 1078 	movx	@dptr,a
   0502                    1079 endusbisr:
                           1080 	;; epilogue
   0502 D0 07              1081 	pop	ar7
   0504 D0 00              1082 	pop	ar0
   0506 D0 86              1083 	pop	dps
   0508 D0 D0              1084 	pop	psw
   050A D0 85              1085 	pop	dph1
   050C D0 84              1086 	pop	dpl1
   050E D0 83              1087 	pop	dph0
   0510 D0 82              1088 	pop	dpl0
   0512 D0 F0              1089 	pop	b
   0514 D0 E0              1090 	pop	acc
   0516 32                 1091 	reti
                           1092 
   0517                    1093 usb_sof_isr:
   0517 C0 E0              1094 	push	acc
   0519 C0 F0              1095 	push	b
   051B C0 82              1096 	push	dpl0
   051D C0 83              1097 	push	dph0
   051F C0 D0              1098 	push	psw
   0521 75 D0 00           1099 	mov	psw,#0x00
   0524 C0 86              1100 	push	dps
   0526 75 86 00           1101 	mov	dps,#0
                           1102 	;; clear interrupt
   0529 E5 91              1103 	mov	a,exif
   052B C2 E4              1104 	clr	acc.4
   052D F5 91              1105 	mov	exif,a
   052F 90 7F AB           1106 	mov	dptr,#USBIRQ
   0532 74 02              1107 	mov	a,#0x02
   0534 F0                 1108 	movx	@dptr,a
                           1109 	;; handle interrupt
                           1110 	;; epilogue
   0535 D0 86              1111 	pop	dps
   0537 D0 D0              1112 	pop	psw
   0539 D0 83              1113 	pop	dph0
   053B D0 82              1114 	pop	dpl0
   053D D0 F0              1115 	pop	b
   053F D0 E0              1116 	pop	acc
   0541 32                 1117 	reti
                           1118 
                           1119 
   0542                    1120 usb_sutok_isr:
   0542 C0 E0              1121 	push	acc
   0544 C0 F0              1122 	push	b
   0546 C0 82              1123 	push	dpl0
   0548 C0 83              1124 	push	dph0
   054A C0 D0              1125 	push	psw
   054C 75 D0 00           1126 	mov	psw,#0x00
   054F C0 86              1127 	push	dps
   0551 75 86 00           1128 	mov	dps,#0
                           1129 	;; clear interrupt
   0554 E5 91              1130 	mov	a,exif
   0556 C2 E4              1131 	clr	acc.4
   0558 F5 91              1132 	mov	exif,a
   055A 90 7F AB           1133 	mov	dptr,#USBIRQ
   055D 74 04              1134 	mov	a,#0x04
   055F F0                 1135 	movx	@dptr,a
                           1136 	;; handle interrupt
                           1137 	;; epilogue
   0560 D0 86              1138 	pop	dps
   0562 D0 D0              1139 	pop	psw
   0564 D0 83              1140 	pop	dph0
   0566 D0 82              1141 	pop	dpl0
   0568 D0 F0              1142 	pop	b
   056A D0 E0              1143 	pop	acc
   056C 32                 1144 	reti
                           1145 
   056D                    1146 usb_suspend_isr:
   056D C0 E0              1147 	push	acc
   056F C0 F0              1148 	push	b
   0571 C0 82              1149 	push	dpl0
   0573 C0 83              1150 	push	dph0
   0575 C0 D0              1151 	push	psw
   0577 75 D0 00           1152 	mov	psw,#0x00
   057A C0 86              1153 	push	dps
   057C 75 86 00           1154 	mov	dps,#0
                           1155 	;; clear interrupt
   057F E5 91              1156 	mov	a,exif
   0581 C2 E4              1157 	clr	acc.4
   0583 F5 91              1158 	mov	exif,a
   0585 90 7F AB           1159 	mov	dptr,#USBIRQ
   0588 74 08              1160 	mov	a,#0x08
   058A F0                 1161 	movx	@dptr,a
                           1162 	;; handle interrupt
                           1163 	;; epilogue
   058B D0 86              1164 	pop	dps
   058D D0 D0              1165 	pop	psw
   058F D0 83              1166 	pop	dph0
   0591 D0 82              1167 	pop	dpl0
   0593 D0 F0              1168 	pop	b
   0595 D0 E0              1169 	pop	acc
   0597 32                 1170 	reti
                           1171 
   0598                    1172 usb_usbreset_isr:
   0598 C0 E0              1173 	push	acc
   059A C0 F0              1174 	push	b
   059C C0 82              1175 	push	dpl0
   059E C0 83              1176 	push	dph0
   05A0 C0 D0              1177 	push	psw
   05A2 75 D0 00           1178 	mov	psw,#0x00
   05A5 C0 86              1179 	push	dps
   05A7 75 86 00           1180 	mov	dps,#0
                           1181 	;; clear interrupt
   05AA E5 91              1182 	mov	a,exif
   05AC C2 E4              1183 	clr	acc.4
   05AE F5 91              1184 	mov	exif,a
   05B0 90 7F AB           1185 	mov	dptr,#USBIRQ
   05B3 74 10              1186 	mov	a,#0x10
   05B5 F0                 1187 	movx	@dptr,a
                           1188 	;; handle interrupt
                           1189 	;; epilogue
   05B6 D0 86              1190 	pop	dps
   05B8 D0 D0              1191 	pop	psw
   05BA D0 83              1192 	pop	dph0
   05BC D0 82              1193 	pop	dpl0
   05BE D0 F0              1194 	pop	b
   05C0 D0 E0              1195 	pop	acc
   05C2 32                 1196 	reti
                           1197 
   05C3                    1198 usb_ep0in_isr:
   05C3 C0 E0              1199 	push	acc
   05C5 C0 F0              1200 	push	b
   05C7 C0 82              1201 	push	dpl0
   05C9 C0 83              1202 	push	dph0
   05CB C0 84              1203 	push	dpl1
   05CD C0 85              1204 	push	dph1
   05CF C0 D0              1205 	push	psw
   05D1 75 D0 00           1206 	mov	psw,#0x00
   05D4 C0 86              1207 	push	dps
   05D6 75 86 00           1208 	mov	dps,#0
   05D9 C0 00              1209 	push	ar0
   05DB C0 07              1210 	push	ar7
                           1211 	;; clear interrupt
   05DD E5 91              1212 	mov	a,exif
   05DF C2 E4              1213 	clr	acc.4
   05E1 F5 91              1214 	mov	exif,a
   05E3 90 7F A9           1215 	mov	dptr,#IN07IRQ
   05E6 74 01              1216 	mov	a,#0x01
   05E8 F0                 1217 	movx	@dptr,a
                           1218 	;; handle interrupt
   05E9 E5 46              1219 	mov	a,ctrlcode
   05EB B4 02 05           1220 	cjne	a,#2,0$
   05EE 12 03 68           1221 	lcall	xmemread
   05F1 80 11              1222 	sjmp	ep0inendisr
   05F3 90 7F B5           1223 0$:	mov	dptr,#IN0BC
   05F6 E4                 1224 	clr	a
   05F7 F0                 1225 	movx	@dptr,a
   05F8 80 04              1226 	sjmp	ep0inack
                           1227 
   05FA                    1228 ep0install:
   05FA 74 03              1229 	mov	a,#3
   05FC 80 02              1230 	sjmp	ep0incs
   05FE                    1231 ep0inack:
   05FE 74 02              1232 	mov	a,#2
   0600                    1233 ep0incs:
   0600 90 7F B4           1234 	mov	dptr,#EP0CS
   0603 F0                 1235 	movx	@dptr,a
   0604                    1236 ep0inendisr:
                           1237 	;; epilogue
   0604 D0 07              1238 	pop	ar7
   0606 D0 00              1239 	pop	ar0
   0608 D0 86              1240 	pop	dps
   060A D0 D0              1241 	pop	psw
   060C D0 85              1242 	pop	dph1
   060E D0 84              1243 	pop	dpl1
   0610 D0 83              1244 	pop	dph0
   0612 D0 82              1245 	pop	dpl0
   0614 D0 F0              1246 	pop	b
   0616 D0 E0              1247 	pop	acc
   0618 32                 1248 	reti
                           1249 
   0619                    1250 usb_ep0out_isr:
   0619 C0 E0              1251 	push	acc
   061B C0 F0              1252 	push	b
   061D C0 82              1253 	push	dpl0
   061F C0 83              1254 	push	dph0
   0621 C0 84              1255 	push	dpl1
   0623 C0 85              1256 	push	dph1
   0625 C0 D0              1257 	push	psw
   0627 75 D0 00           1258 	mov	psw,#0x00
   062A C0 86              1259 	push	dps
   062C 75 86 00           1260 	mov	dps,#0
   062F C0 07              1261 	push	ar7
                           1262 	;; clear interrupt
   0631 E5 91              1263 	mov	a,exif
   0633 C2 E4              1264 	clr	acc.4
   0635 F5 91              1265 	mov	exif,a
   0637 90 7F AA           1266 	mov	dptr,#OUT07IRQ
   063A 74 01              1267 	mov	a,#0x01
   063C F0                 1268 	movx	@dptr,a
                           1269 	;; handle interrupt
   063D E5 46              1270 	mov	a,ctrlcode		; check control code
   063F B4 01 36           1271 	cjne	a,#0x01,ep0outstall
                           1272 	;; write to external memory
   0642 90 7F C5           1273 	mov	dptr,#OUT0BC
   0645 E0                 1274 	movx	a,@dptr
   0646 60 28              1275 	jz	0$
   0648 FF                 1276 	mov	r7,a
   0649 C3                 1277 	clr	c
   064A E5 49              1278 	mov	a,ctrllen
   064C 9F                 1279 	subb	a,r7
   064D F5 49              1280 	mov	ctrllen,a
   064F E5 4A              1281 	mov	a,ctrllen+1
   0651 94 00              1282 	subb	a,#0
   0653 F5 4A              1283 	mov	ctrllen+1,a
   0655 40 21              1284 	jc	ep0outstall
   0657 90 7E C0           1285 	mov	dptr,#OUT0BUF
   065A 85 47 84           1286 	mov	dpl1,ctrladdr
   065D 85 48 85           1287 	mov	dph1,ctrladdr+1
   0660 E0                 1288 1$:	movx	a,@dptr
   0661 A3                 1289 	inc	dptr
   0662 05 86              1290 	inc	dps
   0664 F0                 1291 	movx	@dptr,a
   0665 A3                 1292 	inc	dptr
   0666 15 86              1293 	dec	dps
   0668 DF F6              1294 	djnz	r7,1$
   066A 85 84 47           1295 	mov	ctrladdr,dpl1
   066D 85 85 48           1296 	mov	ctrladdr+1,dph1
   0670 E5 49              1297 0$:	mov	a,ctrllen
   0672 45 4A              1298 	orl	a,ctrllen+1
   0674 60 09              1299 	jz	ep0outack
   0676 80 10              1300 	sjmp	ep0outendisr
                           1301 
   0678                    1302 ep0outstall:
   0678 75 46 00           1303 	mov	ctrlcode,#0
   067B 74 03              1304 	mov	a,#3
   067D 80 05              1305 	sjmp	ep0outcs
   067F                    1306 ep0outack:
   067F 75 46 00           1307 	mov	ctrlcode,#0
   0682 74 02              1308 	mov	a,#2
   0684                    1309 ep0outcs:
   0684 90 7F B4           1310 	mov	dptr,#EP0CS
   0687 F0                 1311 	movx	@dptr,a
   0688                    1312 ep0outendisr:
                           1313 	;; epilogue
   0688 D0 07              1314 	pop	ar7
   068A D0 86              1315 	pop	dps
   068C D0 D0              1316 	pop	psw
   068E D0 85              1317 	pop	dph1
   0690 D0 84              1318 	pop	dpl1
   0692 D0 83              1319 	pop	dph0
   0694 D0 82              1320 	pop	dpl0
   0696 D0 F0              1321 	pop	b
   0698 D0 E0              1322 	pop	acc
   069A 32                 1323 	reti
                           1324 
   069B                    1325 fillusbintr::
   069B 90 7E 80           1326 	mov	dptr,#IN1BUF
   069E E5 40              1327 	mov	a,errcode
   06A0 F0                 1328 	movx	@dptr,a
   06A1 A3                 1329 	inc	dptr
   06A2 E5 41              1330 	mov	a,errval
   06A4 F0                 1331 	movx	@dptr,a
   06A5 A3                 1332 	inc	dptr
   06A6 E5 42              1333 	mov	a,cfgcount
   06A8 F0                 1334 	movx	@dptr,a
   06A9 A3                 1335 	inc	dptr
   06AA E5 43              1336 	mov	a,cfgcount+1
   06AC F0                 1337 	movx	@dptr,a
   06AD A3                 1338 	inc	dptr
   06AE E5 45              1339 	mov	a,irqcount
   06B0 F0                 1340 	movx	@dptr,a
   06B1 90 7F B7           1341 	mov	dptr,#IN1BC
   06B4 74 05              1342 	mov	a,#5
   06B6 F0                 1343 	movx	@dptr,a
   06B7 05 45              1344 	inc	irqcount
   06B9 22                 1345 	ret
                           1346 
   06BA                    1347 usb_ep1in_isr:
   06BA C0 E0              1348 	push	acc
   06BC C0 F0              1349 	push	b
   06BE C0 82              1350 	push	dpl0
   06C0 C0 83              1351 	push	dph0
   06C2 C0 D0              1352 	push	psw
   06C4 75 D0 00           1353 	mov	psw,#0x00
   06C7 C0 86              1354 	push	dps
   06C9 75 86 00           1355 	mov	dps,#0
                           1356 	;; clear interrupt
   06CC E5 91              1357 	mov	a,exif
   06CE C2 E4              1358 	clr	acc.4
   06D0 F5 91              1359 	mov	exif,a
   06D2 90 7F A9           1360 	mov	dptr,#IN07IRQ
   06D5 74 02              1361 	mov	a,#0x02
   06D7 F0                 1362 	movx	@dptr,a
                           1363 	;; handle interrupt
   06D8 12 06 9B           1364 	lcall	fillusbintr
                           1365 	;; epilogue
   06DB D0 86              1366 	pop	dps
   06DD D0 D0              1367 	pop	psw
   06DF D0 83              1368 	pop	dph0
   06E1 D0 82              1369 	pop	dpl0
   06E3 D0 F0              1370 	pop	b
   06E5 D0 E0              1371 	pop	acc
   06E7 32                 1372 	reti
                           1373 
   06E8                    1374 usb_ep1out_isr:
   06E8 C0 E0              1375 	push	acc
   06EA C0 F0              1376 	push	b
   06EC C0 82              1377 	push	dpl0
   06EE C0 83              1378 	push	dph0
   06F0 C0 D0              1379 	push	psw
   06F2 75 D0 00           1380 	mov	psw,#0x00
   06F5 C0 86              1381 	push	dps
   06F7 75 86 00           1382 	mov	dps,#0
                           1383 	;; clear interrupt
   06FA E5 91              1384 	mov	a,exif
   06FC C2 E4              1385 	clr	acc.4
   06FE F5 91              1386 	mov	exif,a
   0700 90 7F AA           1387 	mov	dptr,#OUT07IRQ
   0703 74 02              1388 	mov	a,#0x02
   0705 F0                 1389 	movx	@dptr,a
                           1390 	;; handle interrupt
                           1391 	;; epilogue
   0706 D0 86              1392 	pop	dps
   0708 D0 D0              1393 	pop	psw
   070A D0 83              1394 	pop	dph0
   070C D0 82              1395 	pop	dpl0
   070E D0 F0              1396 	pop	b
   0710 D0 E0              1397 	pop	acc
   0712 32                 1398 	reti
                           1399 
   0713                    1400 usb_ep2in_isr:
   0713 C0 E0              1401 	push	acc
   0715 C0 F0              1402 	push	b
   0717 C0 82              1403 	push	dpl0
   0719 C0 83              1404 	push	dph0
   071B C0 D0              1405 	push	psw
   071D 75 D0 00           1406 	mov	psw,#0x00
   0720 C0 86              1407 	push	dps
   0722 75 86 00           1408 	mov	dps,#0
                           1409 	;; clear interrupt
   0725 E5 91              1410 	mov	a,exif
   0727 C2 E4              1411 	clr	acc.4
   0729 F5 91              1412 	mov	exif,a
   072B 90 7F A9           1413 	mov	dptr,#IN07IRQ
   072E 74 04              1414 	mov	a,#0x04
   0730 F0                 1415 	movx	@dptr,a
                           1416 	;; handle interrupt
                           1417 	;; epilogue
   0731 D0 86              1418 	pop	dps
   0733 D0 D0              1419 	pop	psw
   0735 D0 83              1420 	pop	dph0
   0737 D0 82              1421 	pop	dpl0
   0739 D0 F0              1422 	pop	b
   073B D0 E0              1423 	pop	acc
   073D 32                 1424 	reti
                           1425 
   073E                    1426 usb_ep2out_isr:
   073E C0 E0              1427 	push	acc
   0740 C0 F0              1428 	push	b
   0742 C0 82              1429 	push	dpl0
   0744 C0 83              1430 	push	dph0
   0746 C0 D0              1431 	push	psw
   0748 75 D0 00           1432 	mov	psw,#0x00
   074B C0 86              1433 	push	dps
   074D 75 86 00           1434 	mov	dps,#0
                           1435 	;; clear interrupt
   0750 E5 91              1436 	mov	a,exif
   0752 C2 E4              1437 	clr	acc.4
   0754 F5 91              1438 	mov	exif,a
   0756 90 7F AA           1439 	mov	dptr,#OUT07IRQ
   0759 74 04              1440 	mov	a,#0x04
   075B F0                 1441 	movx	@dptr,a
                           1442 	;; handle interrupt
                           1443 	;; epilogue
   075C D0 86              1444 	pop	dps
   075E D0 D0              1445 	pop	psw
   0760 D0 83              1446 	pop	dph0
   0762 D0 82              1447 	pop	dpl0
   0764 D0 F0              1448 	pop	b
   0766 D0 E0              1449 	pop	acc
   0768 32                 1450 	reti
                           1451 
   0769                    1452 usb_ep3in_isr:
   0769 C0 E0              1453 	push	acc
   076B C0 F0              1454 	push	b
   076D C0 82              1455 	push	dpl0
   076F C0 83              1456 	push	dph0
   0771 C0 D0              1457 	push	psw
   0773 75 D0 00           1458 	mov	psw,#0x00
   0776 C0 86              1459 	push	dps
   0778 75 86 00           1460 	mov	dps,#0
                           1461 	;; clear interrupt
   077B E5 91              1462 	mov	a,exif
   077D C2 E4              1463 	clr	acc.4
   077F F5 91              1464 	mov	exif,a
   0781 90 7F A9           1465 	mov	dptr,#IN07IRQ
   0784 74 08              1466 	mov	a,#0x08
   0786 F0                 1467 	movx	@dptr,a
                           1468 	;; handle interrupt
                           1469 	;; epilogue
   0787 D0 86              1470 	pop	dps
   0789 D0 D0              1471 	pop	psw
   078B D0 83              1472 	pop	dph0
   078D D0 82              1473 	pop	dpl0
   078F D0 F0              1474 	pop	b
   0791 D0 E0              1475 	pop	acc
   0793 32                 1476 	reti
                           1477 
   0794                    1478 usb_ep3out_isr:
   0794 C0 E0              1479 	push	acc
   0796 C0 F0              1480 	push	b
   0798 C0 82              1481 	push	dpl0
   079A C0 83              1482 	push	dph0
   079C C0 D0              1483 	push	psw
   079E 75 D0 00           1484 	mov	psw,#0x00
   07A1 C0 86              1485 	push	dps
   07A3 75 86 00           1486 	mov	dps,#0
                           1487 	;; clear interrupt
   07A6 E5 91              1488 	mov	a,exif
   07A8 C2 E4              1489 	clr	acc.4
   07AA F5 91              1490 	mov	exif,a
   07AC 90 7F AA           1491 	mov	dptr,#OUT07IRQ
   07AF 74 08              1492 	mov	a,#0x08
   07B1 F0                 1493 	movx	@dptr,a
                           1494 	;; handle interrupt
                           1495 	;; epilogue
   07B2 D0 86              1496 	pop	dps
   07B4 D0 D0              1497 	pop	psw
   07B6 D0 83              1498 	pop	dph0
   07B8 D0 82              1499 	pop	dpl0
   07BA D0 F0              1500 	pop	b
   07BC D0 E0              1501 	pop	acc
   07BE 32                 1502 	reti
                           1503 
   07BF                    1504 usb_ep4in_isr:
   07BF C0 E0              1505 	push	acc
   07C1 C0 F0              1506 	push	b
   07C3 C0 82              1507 	push	dpl0
   07C5 C0 83              1508 	push	dph0
   07C7 C0 D0              1509 	push	psw
   07C9 75 D0 00           1510 	mov	psw,#0x00
   07CC C0 86              1511 	push	dps
   07CE 75 86 00           1512 	mov	dps,#0
                           1513 	;; clear interrupt
   07D1 E5 91              1514 	mov	a,exif
   07D3 C2 E4              1515 	clr	acc.4
   07D5 F5 91              1516 	mov	exif,a
   07D7 90 7F A9           1517 	mov	dptr,#IN07IRQ
   07DA 74 10              1518 	mov	a,#0x10
   07DC F0                 1519 	movx	@dptr,a
                           1520 	;; handle interrupt
                           1521 	;; epilogue
   07DD D0 86              1522 	pop	dps
   07DF D0 D0              1523 	pop	psw
   07E1 D0 83              1524 	pop	dph0
   07E3 D0 82              1525 	pop	dpl0
   07E5 D0 F0              1526 	pop	b
   07E7 D0 E0              1527 	pop	acc
   07E9 32                 1528 	reti
                           1529 
   07EA                    1530 usb_ep4out_isr:
   07EA C0 E0              1531 	push	acc
   07EC C0 F0              1532 	push	b
   07EE C0 82              1533 	push	dpl0
   07F0 C0 83              1534 	push	dph0
   07F2 C0 D0              1535 	push	psw
   07F4 75 D0 00           1536 	mov	psw,#0x00
   07F7 C0 86              1537 	push	dps
   07F9 75 86 00           1538 	mov	dps,#0
                           1539 	;; clear interrupt
   07FC E5 91              1540 	mov	a,exif
   07FE C2 E4              1541 	clr	acc.4
   0800 F5 91              1542 	mov	exif,a
   0802 90 7F AA           1543 	mov	dptr,#OUT07IRQ
   0805 74 10              1544 	mov	a,#0x10
   0807 F0                 1545 	movx	@dptr,a
                           1546 	;; handle interrupt
                           1547 	;; epilogue
   0808 D0 86              1548 	pop	dps
   080A D0 D0              1549 	pop	psw
   080C D0 83              1550 	pop	dph0
   080E D0 82              1551 	pop	dpl0
   0810 D0 F0              1552 	pop	b
   0812 D0 E0              1553 	pop	acc
   0814 32                 1554 	reti
                           1555 
   0815                    1556 usb_ep5in_isr:
   0815 C0 E0              1557 	push	acc
   0817 C0 F0              1558 	push	b
   0819 C0 82              1559 	push	dpl0
   081B C0 83              1560 	push	dph0
   081D C0 D0              1561 	push	psw
   081F 75 D0 00           1562 	mov	psw,#0x00
   0822 C0 86              1563 	push	dps
   0824 75 86 00           1564 	mov	dps,#0
                           1565 	;; clear interrupt
   0827 E5 91              1566 	mov	a,exif
   0829 C2 E4              1567 	clr	acc.4
   082B F5 91              1568 	mov	exif,a
   082D 90 7F A9           1569 	mov	dptr,#IN07IRQ
   0830 74 20              1570 	mov	a,#0x20
   0832 F0                 1571 	movx	@dptr,a
                           1572 	;; handle interrupt
                           1573 	;; epilogue
   0833 D0 86              1574 	pop	dps
   0835 D0 D0              1575 	pop	psw
   0837 D0 83              1576 	pop	dph0
   0839 D0 82              1577 	pop	dpl0
   083B D0 F0              1578 	pop	b
   083D D0 E0              1579 	pop	acc
   083F 32                 1580 	reti
                           1581 
   0840                    1582 usb_ep5out_isr:
   0840 C0 E0              1583 	push	acc
   0842 C0 F0              1584 	push	b
   0844 C0 82              1585 	push	dpl0
   0846 C0 83              1586 	push	dph0
   0848 C0 D0              1587 	push	psw
   084A 75 D0 00           1588 	mov	psw,#0x00
   084D C0 86              1589 	push	dps
   084F 75 86 00           1590 	mov	dps,#0
                           1591 	;; clear interrupt
   0852 E5 91              1592 	mov	a,exif
   0854 C2 E4              1593 	clr	acc.4
   0856 F5 91              1594 	mov	exif,a
   0858 90 7F AA           1595 	mov	dptr,#OUT07IRQ
   085B 74 20              1596 	mov	a,#0x20
   085D F0                 1597 	movx	@dptr,a
                           1598 	;; handle interrupt
                           1599 	;; epilogue
   085E D0 86              1600 	pop	dps
   0860 D0 D0              1601 	pop	psw
   0862 D0 83              1602 	pop	dph0
   0864 D0 82              1603 	pop	dpl0
   0866 D0 F0              1604 	pop	b
   0868 D0 E0              1605 	pop	acc
   086A 32                 1606 	reti
                           1607 
   086B                    1608 usb_ep6in_isr:
   086B C0 E0              1609 	push	acc
   086D C0 F0              1610 	push	b
   086F C0 82              1611 	push	dpl0
   0871 C0 83              1612 	push	dph0
   0873 C0 D0              1613 	push	psw
   0875 75 D0 00           1614 	mov	psw,#0x00
   0878 C0 86              1615 	push	dps
   087A 75 86 00           1616 	mov	dps,#0
                           1617 	;; clear interrupt
   087D E5 91              1618 	mov	a,exif
   087F C2 E4              1619 	clr	acc.4
   0881 F5 91              1620 	mov	exif,a
   0883 90 7F A9           1621 	mov	dptr,#IN07IRQ
   0886 74 40              1622 	mov	a,#0x40
   0888 F0                 1623 	movx	@dptr,a
                           1624 	;; handle interrupt
                           1625 	;; epilogue
   0889 D0 86              1626 	pop	dps
   088B D0 D0              1627 	pop	psw
   088D D0 83              1628 	pop	dph0
   088F D0 82              1629 	pop	dpl0
   0891 D0 F0              1630 	pop	b
   0893 D0 E0              1631 	pop	acc
   0895 32                 1632 	reti
                           1633 
   0896                    1634 usb_ep6out_isr:
   0896 C0 E0              1635 	push	acc
   0898 C0 F0              1636 	push	b
   089A C0 82              1637 	push	dpl0
   089C C0 83              1638 	push	dph0
   089E C0 D0              1639 	push	psw
   08A0 75 D0 00           1640 	mov	psw,#0x00
   08A3 C0 86              1641 	push	dps
   08A5 75 86 00           1642 	mov	dps,#0
                           1643 	;; clear interrupt
   08A8 E5 91              1644 	mov	a,exif
   08AA C2 E4              1645 	clr	acc.4
   08AC F5 91              1646 	mov	exif,a
   08AE 90 7F AA           1647 	mov	dptr,#OUT07IRQ
   08B1 74 40              1648 	mov	a,#0x40
   08B3 F0                 1649 	movx	@dptr,a
                           1650 	;; handle interrupt
                           1651 	;; epilogue
   08B4 D0 86              1652 	pop	dps
   08B6 D0 D0              1653 	pop	psw
   08B8 D0 83              1654 	pop	dph0
   08BA D0 82              1655 	pop	dpl0
   08BC D0 F0              1656 	pop	b
   08BE D0 E0              1657 	pop	acc
   08C0 32                 1658 	reti
                           1659 
   08C1                    1660 usb_ep7in_isr:
   08C1 C0 E0              1661 	push	acc
   08C3 C0 F0              1662 	push	b
   08C5 C0 82              1663 	push	dpl0
   08C7 C0 83              1664 	push	dph0
   08C9 C0 D0              1665 	push	psw
   08CB 75 D0 00           1666 	mov	psw,#0x00
   08CE C0 86              1667 	push	dps
   08D0 75 86 00           1668 	mov	dps,#0
                           1669 	;; clear interrupt
   08D3 E5 91              1670 	mov	a,exif
   08D5 C2 E4              1671 	clr	acc.4
   08D7 F5 91              1672 	mov	exif,a
   08D9 90 7F A9           1673 	mov	dptr,#IN07IRQ
   08DC 74 80              1674 	mov	a,#0x80
   08DE F0                 1675 	movx	@dptr,a
                           1676 	;; handle interrupt
                           1677 	;; epilogue
   08DF D0 86              1678 	pop	dps
   08E1 D0 D0              1679 	pop	psw
   08E3 D0 83              1680 	pop	dph0
   08E5 D0 82              1681 	pop	dpl0
   08E7 D0 F0              1682 	pop	b
   08E9 D0 E0              1683 	pop	acc
   08EB 32                 1684 	reti
                           1685 
   08EC                    1686 usb_ep7out_isr:
   08EC C0 E0              1687 	push	acc
   08EE C0 F0              1688 	push	b
   08F0 C0 82              1689 	push	dpl0
   08F2 C0 83              1690 	push	dph0
   08F4 C0 D0              1691 	push	psw
   08F6 75 D0 00           1692 	mov	psw,#0x00
   08F9 C0 86              1693 	push	dps
   08FB 75 86 00           1694 	mov	dps,#0
                           1695 	;; clear interrupt
   08FE E5 91              1696 	mov	a,exif
   0900 C2 E4              1697 	clr	acc.4
   0902 F5 91              1698 	mov	exif,a
   0904 90 7F AA           1699 	mov	dptr,#OUT07IRQ
   0907 74 80              1700 	mov	a,#0x80
   0909 F0                 1701 	movx	@dptr,a
                           1702 	;; handle interrupt
                           1703 	;; epilogue
   090A D0 86              1704 	pop	dps
   090C D0 D0              1705 	pop	psw
   090E D0 83              1706 	pop	dph0
   0910 D0 82              1707 	pop	dpl0
   0912 D0 F0              1708 	pop	b
   0914 D0 E0              1709 	pop	acc
   0916 32                 1710 	reti
                           1711 
                           1712 	
                           1713 	;; -----------------------------------------------------
                           1714 	;; USB descriptors
                           1715 	;; -----------------------------------------------------
                           1716 
                           1717 	;; Device and/or Interface Class codes
                    0000   1718 	USB_CLASS_PER_INTERFACE         = 0
                    0001   1719 	USB_CLASS_AUDIO                 = 1
                    0002   1720 	USB_CLASS_COMM                  = 2
                    0003   1721 	USB_CLASS_HID                   = 3
                    0007   1722 	USB_CLASS_PRINTER               = 7
                    0008   1723 	USB_CLASS_MASS_STORAGE          = 8
                    0009   1724 	USB_CLASS_HUB                   = 9
                    00FF   1725 	USB_CLASS_VENDOR_SPEC           = 0xff
                           1726 
                    0001   1727 	USB_SUBCLASS_AUDIOCONTROL	= 1
                    0002   1728 	USB_SUBCLASS_AUDIOSTREAMING	= 2
                           1729 	
                           1730 	;; Descriptor types
                    0001   1731 	USB_DT_DEVICE                   = 0x01
                    0002   1732 	USB_DT_CONFIG                   = 0x02
                    0003   1733 	USB_DT_STRING                   = 0x03
                    0004   1734 	USB_DT_INTERFACE                = 0x04
                    0005   1735 	USB_DT_ENDPOINT                 = 0x05
                           1736 
                           1737 	;; Audio Class descriptor types
                    0020   1738 	USB_DT_AUDIO_UNDEFINED		= 0x20
                    0021   1739 	USB_DT_AUDIO_DEVICE		= 0x21
                    0022   1740 	USB_DT_AUDIO_CONFIG		= 0x22
                    0023   1741 	USB_DT_AUDIO_STRING		= 0x23
                    0024   1742 	USB_DT_AUDIO_INTERFACE		= 0x24
                    0025   1743 	USB_DT_AUDIO_ENDPOINT		= 0x25
                           1744 
                    0000   1745 	USB_SDT_AUDIO_UNDEFINED		= 0x00
                    0001   1746 	USB_SDT_AUDIO_HEADER		= 0x01
                    0002   1747 	USB_SDT_AUDIO_INPUT_TERMINAL	= 0x02
                    0003   1748 	USB_SDT_AUDIO_OUTPUT_TERMINAL	= 0x03
                    0004   1749 	USB_SDT_AUDIO_MIXER_UNIT	= 0x04
                    0005   1750 	USB_SDT_AUDIO_SELECTOR_UNIT	= 0x05
                    0006   1751 	USB_SDT_AUDIO_FEATURE_UNIT	= 0x06
                    0007   1752 	USB_SDT_AUDIO_PROCESSING_UNIT	= 0x07
                    0008   1753 	USB_SDT_AUDIO_EXTENSION_UNIT	= 0x08
                           1754 	
                           1755 	;; Standard requests
                    0000   1756 	USB_REQ_GET_STATUS              = 0x00
                    0001   1757 	USB_REQ_CLEAR_FEATURE           = 0x01
                    0003   1758 	USB_REQ_SET_FEATURE             = 0x03
                    0005   1759 	USB_REQ_SET_ADDRESS             = 0x05
                    0006   1760 	USB_REQ_GET_DESCRIPTOR          = 0x06
                    0007   1761 	USB_REQ_SET_DESCRIPTOR          = 0x07
                    0008   1762 	USB_REQ_GET_CONFIGURATION       = 0x08
                    0009   1763 	USB_REQ_SET_CONFIGURATION       = 0x09
                    000A   1764 	USB_REQ_GET_INTERFACE           = 0x0A
                    000B   1765 	USB_REQ_SET_INTERFACE           = 0x0B
                    000C   1766 	USB_REQ_SYNCH_FRAME             = 0x0C
                           1767 
                           1768 	;; Audio Class Requests
                    0001   1769 	USB_REQ_AUDIO_SET_CUR		= 0x01
                    0081   1770 	USB_REQ_AUDIO_GET_CUR		= 0x81
                    0002   1771 	USB_REQ_AUDIO_SET_MIN		= 0x02
                    0082   1772 	USB_REQ_AUDIO_GET_MIN		= 0x82
                    0003   1773 	USB_REQ_AUDIO_SET_MAX		= 0x03
                    0083   1774 	USB_REQ_AUDIO_GET_MAX		= 0x83
                    0004   1775 	USB_REQ_AUDIO_SET_RES		= 0x04
                    0084   1776 	USB_REQ_AUDIO_GET_RES		= 0x84
                    0005   1777 	USB_REQ_AUDIO_SET_MEM		= 0x05
                    0085   1778 	USB_REQ_AUDIO_GET_MEM		= 0x85
                    00FF   1779 	USB_REQ_AUDIO_GET_STAT		= 0xff
                           1780 	
                           1781 	;; USB Request Type and Endpoint Directions
                    0000   1782 	USB_DIR_OUT                     = 0
                    0080   1783 	USB_DIR_IN                      = 0x80
                           1784 
                    0000   1785 	USB_TYPE_STANDARD               = (0x00 << 5)
                    0020   1786 	USB_TYPE_CLASS                  = (0x01 << 5)
                    0040   1787 	USB_TYPE_VENDOR                 = (0x02 << 5)
                    0060   1788 	USB_TYPE_RESERVED               = (0x03 << 5)
                           1789 
                    0000   1790 	USB_RECIP_DEVICE                = 0x00
                    0001   1791 	USB_RECIP_INTERFACE             = 0x01
                    0002   1792 	USB_RECIP_ENDPOINT              = 0x02
                    0003   1793 	USB_RECIP_OTHER                 = 0x03
                           1794 
                           1795 	;; Request target types.
                    0000   1796 	USB_RT_DEVICE                   = 0x00
                    0001   1797 	USB_RT_INTERFACE                = 0x01
                    0002   1798 	USB_RT_ENDPOINT                 = 0x02
                           1799 
                    BAC0   1800 	VENDID	= 0xbac0
                    6134   1801 	PRODID	= 0x6134
                           1802 
