                              1 	.module main
                              2 
                              3 	;; define code segments link order
                              4 	.area CODE (CODE)
                              5 	.area CSEG (CODE)
                              6 	.area GSINIT (CODE)
                              7 	.area GSINIT2 (CODE)
                              8 
                              9 	;; -----------------------------------------------------
                             10 
                             11 	;; special function registers (which are not predefined)
                    0082     12 	dpl0    = 0x82
                    0083     13 	dph0    = 0x83
                    0084     14 	dpl1    = 0x84
                    0085     15 	dph1    = 0x85
                    0086     16 	dps     = 0x86
                    008E     17 	ckcon   = 0x8E
                    008F     18 	spc_fnc = 0x8F
                    0091     19 	exif    = 0x91
                    0092     20 	mpage   = 0x92
                    0098     21 	scon0   = 0x98
                    0099     22 	sbuf0   = 0x99
                    00C0     23 	scon1   = 0xC0
                    00C1     24 	sbuf1   = 0xC1
                    00D8     25 	eicon   = 0xD8
                    00E8     26 	eie     = 0xE8
                    00F8     27 	eip     = 0xF8
                             28 
                             29 	;; anchor xdata registers
                    7F00     30 	IN0BUF		= 0x7F00
                    7EC0     31 	OUT0BUF		= 0x7EC0
                    7E80     32 	IN1BUF		= 0x7E80
                    7E40     33 	OUT1BUF		= 0x7E40
                    7E00     34 	IN2BUF		= 0x7E00
                    7DC0     35 	OUT2BUF		= 0x7DC0
                    7D80     36 	IN3BUF		= 0x7D80
                    7D40     37 	OUT3BUF		= 0x7D40
                    7D00     38 	IN4BUF		= 0x7D00
                    7CC0     39 	OUT4BUF		= 0x7CC0
                    7C80     40 	IN5BUF		= 0x7C80
                    7C40     41 	OUT5BUF		= 0x7C40
                    7C00     42 	IN6BUF		= 0x7C00
                    7BC0     43 	OUT6BUF		= 0x7BC0
                    7B80     44 	IN7BUF		= 0x7B80
                    7B40     45 	OUT7BUF		= 0x7B40
                    7FE8     46 	SETUPBUF	= 0x7FE8
                    7FE8     47 	SETUPDAT	= 0x7FE8
                             48 
                    7FB4     49 	EP0CS		= 0x7FB4
                    7FB5     50 	IN0BC		= 0x7FB5
                    7FB6     51 	IN1CS		= 0x7FB6
                    7FB7     52 	IN1BC		= 0x7FB7
                    7FB8     53 	IN2CS		= 0x7FB8
                    7FB9     54 	IN2BC		= 0x7FB9
                    7FBA     55 	IN3CS		= 0x7FBA
                    7FBB     56 	IN3BC		= 0x7FBB
                    7FBC     57 	IN4CS		= 0x7FBC
                    7FBD     58 	IN4BC		= 0x7FBD
                    7FBE     59 	IN5CS		= 0x7FBE
                    7FBF     60 	IN5BC		= 0x7FBF
                    7FC0     61 	IN6CS		= 0x7FC0
                    7FC1     62 	IN6BC		= 0x7FC1
                    7FC2     63 	IN7CS		= 0x7FC2
                    7FC3     64 	IN7BC		= 0x7FC3
                    7FC5     65 	OUT0BC		= 0x7FC5
                    7FC6     66 	OUT1CS		= 0x7FC6
                    7FC7     67 	OUT1BC		= 0x7FC7
                    7FC8     68 	OUT2CS		= 0x7FC8
                    7FC9     69 	OUT2BC		= 0x7FC9
                    7FCA     70 	OUT3CS		= 0x7FCA
                    7FCB     71 	OUT3BC		= 0x7FCB
                    7FCC     72 	OUT4CS		= 0x7FCC
                    7FCD     73 	OUT4BC		= 0x7FCD
                    7FCE     74 	OUT5CS		= 0x7FCE
                    7FCF     75 	OUT5BC		= 0x7FCF
                    7FD0     76 	OUT6CS		= 0x7FD0
                    7FD1     77 	OUT6BC		= 0x7FD1
                    7FD2     78 	OUT7CS		= 0x7FD2
                    7FD3     79 	OUT7BC		= 0x7FD3
                             80 
                    7FA8     81 	IVEC		= 0x7FA8
                    7FA9     82 	IN07IRQ		= 0x7FA9
                    7FAA     83 	OUT07IRQ	= 0x7FAA
                    7FAB     84 	USBIRQ		= 0x7FAB
                    7FAC     85 	IN07IEN		= 0x7FAC
                    7FAD     86 	OUT07IEN	= 0x7FAD
                    7FAE     87 	USBIEN		= 0x7FAE
                    7FAF     88 	USBBAV		= 0x7FAF
                    7FB2     89 	BPADDRH		= 0x7FB2
                    7FB3     90 	BPADDRL		= 0x7FB3
                             91 
                    7FD4     92 	SUDPTRH		= 0x7FD4
                    7FD5     93 	SUDPTRL		= 0x7FD5
                    7FD6     94 	USBCS		= 0x7FD6
                    7FD7     95 	TOGCTL		= 0x7FD7
                    7FD8     96 	USBFRAMEL	= 0x7FD8
                    7FD9     97 	USBFRAMEH	= 0x7FD9
                    7FDB     98 	FNADDR		= 0x7FDB
                    7FDD     99 	USBPAIR		= 0x7FDD
                    7FDE    100 	IN07VAL		= 0x7FDE
                    7FDF    101 	OUT07VAL	= 0x7FDF
                    7FE3    102 	AUTOPTRH	= 0x7FE3
                    7FE4    103 	AUTOPTRL	= 0x7FE4
                    7FE5    104 	AUTODATA	= 0x7FE5
                            105 
                            106 	;; isochronous endpoints. only available if ISODISAB=0
                            107 
                    7F60    108 	OUT8DATA	= 0x7F60
                    7F61    109 	OUT9DATA	= 0x7F61
                    7F62    110 	OUT10DATA	= 0x7F62
                    7F63    111 	OUT11DATA	= 0x7F63
                    7F64    112 	OUT12DATA	= 0x7F64
                    7F65    113 	OUT13DATA	= 0x7F65
                    7F66    114 	OUT14DATA	= 0x7F66
                    7F67    115 	OUT15DATA	= 0x7F67
                            116 
                    7F68    117 	IN8DATA		= 0x7F68
                    7F69    118 	IN9DATA		= 0x7F69
                    7F6A    119 	IN10DATA	= 0x7F6A
                    7F6B    120 	IN11DATA	= 0x7F6B
                    7F6C    121 	IN12DATA	= 0x7F6C
                    7F6D    122 	IN13DATA	= 0x7F6D
                    7F6E    123 	IN14DATA	= 0x7F6E
                    7F6F    124 	IN15DATA	= 0x7F6F
                            125 
                    7F70    126 	OUT8BCH		= 0x7F70
                    7F71    127 	OUT8BCL		= 0x7F71
                    7F72    128 	OUT9BCH		= 0x7F72
                    7F73    129 	OUT9BCL		= 0x7F73
                    7F74    130 	OUT10BCH	= 0x7F74
                    7F75    131 	OUT10BCL	= 0x7F75
                    7F76    132 	OUT11BCH	= 0x7F76
                    7F77    133 	OUT11BCL	= 0x7F77
                    7F78    134 	OUT12BCH	= 0x7F78
                    7F79    135 	OUT12BCL	= 0x7F79
                    7F7A    136 	OUT13BCH	= 0x7F7A
                    7F7B    137 	OUT13BCL	= 0x7F7B
                    7F7C    138 	OUT14BCH	= 0x7F7C
                    7F7D    139 	OUT14BCL	= 0x7F7D
                    7F7E    140 	OUT15BCH	= 0x7F7E
                    7F7F    141 	OUT15BCL	= 0x7F7F
                            142 
                    7FF0    143 	OUT8ADDR	= 0x7FF0
                    7FF1    144 	OUT9ADDR	= 0x7FF1
                    7FF2    145 	OUT10ADDR	= 0x7FF2
                    7FF3    146 	OUT11ADDR	= 0x7FF3
                    7FF4    147 	OUT12ADDR	= 0x7FF4
                    7FF5    148 	OUT13ADDR	= 0x7FF5
                    7FF6    149 	OUT14ADDR	= 0x7FF6
                    7FF7    150 	OUT15ADDR	= 0x7FF7
                    7FF8    151 	IN8ADDR		= 0x7FF8
                    7FF9    152 	IN9ADDR		= 0x7FF9
                    7FFA    153 	IN10ADDR	= 0x7FFA
                    7FFB    154 	IN11ADDR	= 0x7FFB
                    7FFC    155 	IN12ADDR	= 0x7FFC
                    7FFD    156 	IN13ADDR	= 0x7FFD
                    7FFE    157 	IN14ADDR	= 0x7FFE
                    7FFF    158 	IN15ADDR	= 0x7FFF
                            159 
                    7FA0    160 	ISOERR		= 0x7FA0
                    7FA1    161 	ISOCTL		= 0x7FA1
                    7FA2    162 	ZBCOUNT		= 0x7FA2
                    7FE0    163 	INISOVAL	= 0x7FE0
                    7FE1    164 	OUTISOVAL	= 0x7FE1
                    7FE2    165 	FASTXFR		= 0x7FE2
                            166 
                            167 	;; CPU control registers
                            168 
                    7F92    169 	CPUCS		= 0x7F92
                            170 
                            171 	;; IO port control registers
                            172 
                    7F93    173 	PORTACFG	= 0x7F93
                    7F94    174 	PORTBCFG	= 0x7F94
                    7F95    175 	PORTCCFG	= 0x7F95
                    7F96    176 	OUTA		= 0x7F96
                    7F97    177 	OUTB		= 0x7F97
                    7F98    178 	OUTC		= 0x7F98
                    7F99    179 	PINSA		= 0x7F99
                    7F9A    180 	PINSB		= 0x7F9A
                    7F9B    181 	PINSC		= 0x7F9B
                    7F9C    182 	OEA		= 0x7F9C
                    7F9D    183 	OEB		= 0x7F9D
                    7F9E    184 	OEC		= 0x7F9E
                            185 
                            186 	;; I2C controller registers
                            187 
                    7FA5    188 	I2CS		= 0x7FA5
                    7FA6    189 	I2DAT		= 0x7FA6
                            190 
                            191 	;; FPGA defines
                    0003    192 	XC4K_IRLENGTH	= 3
                    0000    193 	XC4K_EXTEST	= 0
                    0001    194 	XC4K_PRELOAD	= 1
                    0005    195 	XC4K_CONFIGURE	= 5
                    0007    196 	XC4K_BYPASS	= 7
                            197 
                    2E64    198 	FPGA_CONFIGSIZE = 11876
                    0158    199 	FPGA_BOUND	= 344
                            200 
                    0000    201 	SOFTWARECONFIG	= 0
                            202 
                            203 	;; -----------------------------------------------------
                            204 
                            205 	.area CODE (CODE)
   0000 02 0B DF            206 	ljmp	startup
   0003 02 02 36            207 	ljmp	int0_isr
   0006                     208 	.ds	5
   000B 02 02 57            209 	ljmp	timer0_isr
   000E                     210 	.ds	5
   0013 02 02 87            211 	ljmp	int1_isr
   0016                     212 	.ds	5
   001B 02 02 A8            213 	ljmp	timer1_isr
   001E                     214 	.ds	5
   0023 02 02 C9            215 	ljmp	ser0_isr
   0026                     216 	.ds	5
   002B 02 02 EC            217 	ljmp	timer2_isr
   002E                     218 	.ds	5
   0033 02 03 0D            219 	ljmp	resume_isr
   0036                     220 	.ds	5
   003B 02 03 2E            221 	ljmp	ser1_isr
   003E                     222 	.ds	5
   0043 02 01 00            223 	ljmp	usb_isr
   0046                     224 	.ds	5
   004B 02 03 51            225 	ljmp	i2c_isr
   004E                     226 	.ds	5
   0053 02 03 76            227 	ljmp	int4_isr
   0056                     228 	.ds	5
   005B 02 03 9B            229 	ljmp	int5_isr
   005E                     230 	.ds	5
   0063 02 03 C0            231 	ljmp	int6_isr
                            232 
   0066                     233 	.ds	0x9a
                            234 	;; USB interrupt dispatch table
   0100                     235 usb_isr:
   0100 02 04 29            236 	ljmp	usb_sudav_isr
   0103                     237 	.ds	1
   0104 02 07 11            238 	ljmp	usb_sof_isr
   0107                     239 	.ds	1
   0108 02 07 3C            240 	ljmp	usb_sutok_isr
   010B                     241 	.ds	1
   010C 02 07 67            242 	ljmp	usb_suspend_isr
   010F                     243 	.ds	1
   0110 02 07 92            244 	ljmp	usb_usbreset_isr
   0113                     245 	.ds	1
   0114 32                  246 	reti
   0115                     247 	.ds	3
   0118 02 07 BD            248 	ljmp	usb_ep0in_isr
   011B                     249 	.ds	1
   011C 02 08 13            250 	ljmp	usb_ep0out_isr
   011F                     251 	.ds	1
   0120 02 08 CF            252 	ljmp	usb_ep1in_isr
   0123                     253 	.ds	1
   0124 02 08 FD            254 	ljmp	usb_ep1out_isr
   0127                     255 	.ds	1
   0128 02 09 28            256 	ljmp	usb_ep2in_isr
   012B                     257 	.ds	1
   012C 02 09 53            258 	ljmp	usb_ep2out_isr
   012F                     259 	.ds	1
   0130 02 09 7E            260 	ljmp	usb_ep3in_isr
   0133                     261 	.ds	1
   0134 02 09 A9            262 	ljmp	usb_ep3out_isr
   0137                     263 	.ds	1
   0138 02 09 D4            264 	ljmp	usb_ep4in_isr
   013B                     265 	.ds	1
   013C 02 09 FF            266 	ljmp	usb_ep4out_isr
   013F                     267 	.ds	1
   0140 02 0A 2A            268 	ljmp	usb_ep5in_isr
   0143                     269 	.ds	1
   0144 02 0A 55            270 	ljmp	usb_ep5out_isr
   0147                     271 	.ds	1
   0148 02 0A 80            272 	ljmp	usb_ep6in_isr
   014B                     273 	.ds	1
   014C 02 0A AB            274 	ljmp	usb_ep6out_isr
   014F                     275 	.ds	1
   0150 02 0A D6            276 	ljmp	usb_ep7in_isr
   0153                     277 	.ds	1
   0154 02 0B 01            278 	ljmp	usb_ep7out_isr
                            279 
                            280 	;; -----------------------------------------------------
                            281 
                            282 	.area   BSEG (BIT)
                            283 
                            284 	.area	ISEG (DATA)
   0080                     285 stack:		.ds	0x80
                            286 
                            287 	.area	DSEG (DATA)
   0040                     288 errcode:	.ds	1
   0041                     289 errval:		.ds	1
   0042                     290 cfgcount:	.ds	2
   0044                     291 leddiv:		.ds	1
   0045                     292 irqcount:	.ds	1
   0046                     293 ctrlcode:	.ds	1
   0047                     294 ctrladdr:	.ds	2
   0049                     295 ctrllen:	.ds	2
                            296 
                            297 	;; USB state
   004B                     298 numconfig:	.ds	1
   004C                     299 altsetting:	.ds	1
                            300 	
                            301 	.area	XSEG (DATA)
                            302 
                            303 
                            304 	.area	GSINIT (CODE)
   0BDF                     305 startup:
   0BDF 75 81 80            306 	mov	sp,#stack	; -1
   0BE2 E4                  307 	clr	a
   0BE3 F5 D0               308 	mov	psw,a
   0BE5 F5 86               309 	mov	dps,a
                            310 	;lcall	__sdcc_external_startup
                            311 	;mov	a,dpl0
                            312 	;jz	__sdcc_init_data
                            313 	;ljmp	__sdcc_program_startup
   0BE7                     314 __sdcc_init_data:
                            315 
                            316 	.area	GSINIT2 (CODE)
   0BE7                     317 __sdcc_program_startup:
                            318 	;; assembler code startup
   0BE7 E4                  319 	clr	a
   0BE8 F5 40               320 	mov	errcode,a
   0BEA F5 41               321 	mov	errval,a
   0BEC F5 42               322 	mov	cfgcount,a
   0BEE F5 43               323 	mov	cfgcount+1,a
   0BF0 F5 45               324  	mov	irqcount,a
   0BF2 F5 46               325  	mov	ctrlcode,a
   0BF4 F5 86               326 	mov	dps,a
   0BF6 F5 A8               327 	mov	ie,a
                            328 	;; some indirect register setup
   0BF8 75 8E 31            329 	mov	ckcon,#0x31	; one external wait state, to avoid chip bugs
                            330 	;; Timer setup:
                            331 	;; timer 0: LED blink    : T0M=0, CT0=0, MODE0=1
                            332 	;; timer 1: Baudrate gen : T1M=1, CT1=0, MODE1=2
   0BFB 75 89 21            333 	mov	tmod,#0x21
   0BFE 75 88 55            334 	mov	tcon,#0x55	; INT0/INT1 edge
   0C01 75 8D 64            335 	mov	th1,#256-156	; 1200 bauds
   0C04 75 87 00            336 	mov	pcon,#0		; SMOD0=0
                            337 	;; give Windows a chance to finish the writecpucs control transfer
                            338 	;; 10ms delay loop
   0C07 90 E8 90            339 	mov	dptr,#(-6000)&0xffff
   0C0A A3                  340 9$:	inc	dptr		; 3 cycles
   0C0B E5 82               341 	mov	a,dpl0		; 2 cycles
   0C0D 45 83               342 	orl	a,dph0		; 2 cycles
   0C0F 70 F9               343 	jnz	9$		; 3 cycles
                            344 	;; init USB subsystem
   0C11 90 7F A1            345 	mov	dptr,#ISOCTL
   0C14 74 01               346 	mov	a,#1		; disable ISO endpoints
   0C16 F0                  347 	movx	@dptr,a
   0C17 90 7F AF            348 	mov	dptr,#USBBAV
   0C1A 74 01               349 	mov	a,#1		; enable autovector, disable breakpoint logic
   0C1C F0                  350 	movx	@dptr,a
   0C1D E4                  351 	clr	a
   0C1E 90 7F E0            352 	mov	dptr,#INISOVAL
   0C21 F0                  353 	movx	@dptr,a
   0C22 90 7F E1            354 	mov	dptr,#OUTISOVAL
   0C25 F0                  355 	movx	@dptr,a
   0C26 90 7F DD            356 	mov	dptr,#USBPAIR
   0C29 74 09               357 	mov	a,#0x9		; pair EP 2&3 for input & output
   0C2B F0                  358 	movx	@dptr,a
   0C2C 90 7F DE            359 	mov	dptr,#IN07VAL
   0C2F 74 07               360 	mov	a,#0x7		; enable EP0+EP1+EP2
   0C31 F0                  361 	movx	@dptr,a
   0C32 90 7F DF            362 	mov	dptr,#OUT07VAL
   0C35 74 05               363 	mov	a,#0x5		; enable EP0+EP2
   0C37 F0                  364 	movx	@dptr,a
                            365 	;; USB:	init endpoint toggles
   0C38 90 7F D7            366 	mov	dptr,#TOGCTL
   0C3B 74 12               367 	mov	a,#0x12
   0C3D F0                  368 	movx	@dptr,a
   0C3E 74 32               369 	mov	a,#0x32		; clear EP 2 in toggle
   0C40 F0                  370 	movx	@dptr,a
   0C41 74 02               371 	mov	a,#0x02
   0C43 F0                  372 	movx	@dptr,a
   0C44 74 22               373 	mov	a,#0x22		; clear EP 2 out toggle
   0C46 F0                  374 	movx	@dptr,a
                            375 	;; configure IO ports
   0C47 90 7F 93            376 	mov	dptr,#PORTACFG
   0C4A 74 00               377 	mov	a,#0
   0C4C F0                  378 	movx	@dptr,a
   0C4D 90 7F 96            379 	mov	dptr,#OUTA
   0C50 74 80               380 	mov	a,#0x80		; set PROG lo
   0C52 F0                  381 	movx	@dptr,a
   0C53 90 7F 9C            382 	mov	dptr,#OEA
   0C56 74 C2               383 	mov	a,#0xc2		; out: TMS,TDI,PROG  in: DONE
   0C58 F0                  384 	movx	@dptr,a
   0C59 90 7F 94            385 	mov	dptr,#PORTBCFG
   0C5C 74 00               386 	mov	a,#0
   0C5E F0                  387 	movx	@dptr,a
   0C5F 90 7F 9D            388 	mov	dptr,#OEB
   0C62 74 00               389 	mov	a,#0
   0C64 F0                  390 	movx	@dptr,a
   0C65 90 7F 95            391 	mov	dptr,#PORTCCFG
   0C68 74 00               392 	mov	a,#0
   0C6A F0                  393 	movx	@dptr,a
   0C6B 90 7F 98            394 	mov	dptr,#OUTC
   0C6E 74 20               395 	mov	a,#0x20
   0C70 F0                  396 	movx	@dptr,a
   0C71 90 7F 9E            397 	mov	dptr,#OEC
   0C74 74 2E               398 	mov	a,#0x2e		; out: LEDCON,LEDSTA,TCK,INIT  in: TDO
   0C76 F0                  399 	movx	@dptr,a
                            400 	;; enable interrupts
   0C77 75 A8 82            401 	mov	ie,#0x82	; enable timer 0
   0C7A 75 E8 01            402 	mov	eie,#0x01	; enable USB interrupts
   0C7D 90 7F AE            403 	mov	dptr,#USBIEN
   0C80 74 01               404 	mov	a,#1		; enable SUDAV interrupt
   0C82 F0                  405 	movx	@dptr,a
   0C83 90 7F AC            406 	mov	dptr,#IN07IEN
   0C86 74 03               407 	mov	a,#3		; enable EP0+EP1 interrupt
   0C88 F0                  408 	movx	@dptr,a
   0C89 90 7F AD            409 	mov	dptr,#OUT07IEN
   0C8C 74 01               410 	mov	a,#1		; enable EP0 interrupt
   0C8E F0                  411 	movx	@dptr,a
                            412 	;; initialize UART 0 for config loading
   0C8F 75 98 20            413 	mov	scon0,#0x20	; mode 0, CLK24/4
   0C92 75 99 FF            414 	mov	sbuf0,#0xff
                            415 	;; initialize USB state
   0C95 E4                  416 	clr	a
   0C96 F5 4B               417 	mov	numconfig,a
   0C98 F5 4C               418 	mov	altsetting,a
                    0000    419 	.if	0
                            420 	;; disconnect from USB bus
                            421 	mov	dptr,#USBCS
                            422 	mov	a,#10
                            423 	movx	@dptr,a
                            424 	;; wait 0.3 sec
                            425 	mov	r2,#30
                            426 	;; 10ms delay loop
                            427 0$:	mov	dptr,#(-6000)&0xffff
                            428 1$:	inc	dptr            ; 3 cycles
                            429 	mov	a,dpl0          ; 2 cycles
                            430 	orl	a,dph0          ; 2 cycles
                            431 	jnz	1$              ; 3 cycles
                            432 	djnz	r2,0$
                            433 	;; reconnect to USB bus
                            434 	mov	dptr,#USBCS
                            435 	mov	a,#2		; 8051 handles control
                            436 	movx	@dptr,a
                            437 	mov	a,#6		; reconnect, 8051 handles control
                            438 	movx	@dptr,a
                            439 	.else
   0C9A 90 7F D6            440 	mov	dptr,#USBCS
   0C9D 74 06               441 	mov	a,#6		; reconnect, 8051 handles control
   0C9F F0                  442 	movx	@dptr,a
                            443 	.endif
                            444 
                            445 	;; final
   0CA0 12 08 B0            446 	lcall	fillusbintr
                            447 	;; kludge; first OUT2 packet seems to be bogus
                            448 	;; wait for packet with length 1 and contents 0x55
                            449 
                            450 	;; some short delay
   0CA3 90 7F 96            451 	mov	dptr,#OUTA
   0CA6 74 82               452 	mov	a,#0x82		; set PROG hi
   0CA8 F0                  453 	movx	@dptr,a
   0CA9 90 7F 99            454 	mov	dptr,#PINSA
   0CAC E0                  455 	movx	a,@dptr
   0CAD 30 E2 06            456 	jnb	acc.2,8$
                            457 	;; DONE stuck high
   0CB0 75 40 80            458 	mov	errcode,#0x80
   0CB3 02 0D DF            459 	ljmp	fpgaerr
   0CB6 12 01 57            460 8$:	lcall	jtag_reset_tap
   0CB9 7A 05               461 	mov	r2,#5
   0CBB 7B 00               462 	mov	r3,#0
   0CBD 7C 06               463 	mov	r4,#6
   0CBF 12 01 5D            464 	lcall	jtag_shiftout	; enter SHIFT-IR state
   0CC2 7A 08               465 	mov	r2,#8
   0CC4 7B 00               466 	mov	r3,#0
   0CC6 7C 00               467 	mov	r4,#0
   0CC8 12 01 5D            468 	lcall	jtag_shiftout	; assume max. 8bit IR
   0CCB 7A 08               469 	mov	r2,#8
   0CCD 7B 01               470 	mov	r3,#1
   0CCF 7C 00               471 	mov	r4,#0
   0CD1 12 01 5D            472 	lcall	jtag_shift	; shift in a single bit
   0CD4 74 F8               473 	mov	a,#-(1<<XC4K_IRLENGTH)
   0CD6 2F                  474 	add	a,r7
   0CD7 60 08               475 	jz	2$
                            476 	;; JTAG instruction register error
   0CD9 75 40 81            477 	mov	errcode,#0x81
   0CDC 8F 41               478 	mov	errval,r7
   0CDE 02 0D DF            479 	ljmp	fpgaerr
   0CE1 7A 05               480 2$:	mov	r2,#XC4K_IRLENGTH+2
   0CE3 7B 05               481 	mov	r3,#XC4K_CONFIGURE
   0CE5 7C 0C               482 	mov	r4,#3<<(XC4K_IRLENGTH-1)
   0CE7 12 01 5D            483 	lcall	jtag_shiftout	; shift in CONFIGURE insn, goto RUNTEST/IDLE state
   0CEA 90 7F 9E            484 	mov	dptr,#OEC
   0CED 74 2A               485 	mov	a,#0x2a		; out: LEDCON,LEDSTA,TCK  in: TDO,INIT
   0CEF F0                  486 	movx	@dptr,a
   0CF0 75 40 01            487 	mov	errcode,#1
   0CF3 90 7F 9B            488 	mov	dptr,#PINSC
   0CF6 E0                  489 3$:	movx	a,@dptr
   0CF7 30 E2 FC            490 	jnb	acc.2,3$
   0CFA 7A 03               491 	mov	r2,#3
   0CFC 7B FF               492 	mov	r3,#0xff
   0CFE 7C 01               493 	mov	r4,#1
   0D00 12 01 5D            494 	lcall	jtag_shiftout	; advance to SHIFT-DR state
                            495 	;; software configuration: enable UART special func pins
   0D03 90 7F 96            496 	mov	dptr,#OUTA
   0D06 74 42               497 	mov	a,#0x42		; PROG hi, TMS lo, TDI hi
   0D08 F0                  498 	movx	@dptr,a
                    0000    499 	.if	SOFTWARECONFIG
                            500 	.else
   0D09 90 7F 93            501 	mov	dptr,#PORTACFG
   0D0C 74 40               502 	mov	a,#0x40		; TDI is special function pin
   0D0E F0                  503 	movx	@dptr,a
   0D0F 90 7F 95            504 	mov	dptr,#PORTCCFG
   0D12 74 02               505 	mov	a,#0x02		; TCK is special function pin
   0D14 F0                  506 	movx	@dptr,a
                            507 	.endif
                            508 	;; start
   0D15 75 40 10            509 	mov	errcode,#0x10
                            510 
   0D18 90 7F C8            511 	mov	dptr,#OUT2CS
   0D1B E4                  512 	clr	a
   0D1C F0                  513 	movx	@dptr,a
   0D1D 90 7F C9            514 	mov	dptr,#OUT2BC
   0D20 F0                  515 	movx	@dptr,a		; arm EP2 OUT
                            516 	
                            517 	
   0D21                     518 ldstloop:
   0D21 90 7F C9            519 	mov	dptr,#OUT2BC
   0D24 F0                  520 	movx	@dptr,a		; arm EP2 OUT
   0D25 90 7F C8            521 0$:	mov	dptr,#OUT2CS
   0D28 E0                  522 	movx	a,@dptr
   0D29 20 E1 F9            523 	jb	acc.1,0$
   0D2C 90 7F C9            524 	mov	dptr,#OUT2BC
   0D2F E0                  525 	movx	a,@dptr
   0D30 24 FE               526 	add	a,#-2
   0D32 50 ED               527 	jnc	ldstloop
   0D34 90 7D C0            528 	mov	dptr,#OUT2BUF
   0D37 E0                  529 	movx	a,@dptr
   0D38 B4 FF E6            530 	cjne	a,#0xff,ldstloop
   0D3B A3                  531 	inc	dptr
   0D3C E0                  532 	movx	a,@dptr
   0D3D B4 04 E1            533 	cjne	a,#0x04,ldstloop
   0D40 75 40 11            534 	mov	errcode,#0x11
   0D43 80 0B               535 	sjmp	xxloadloop
                            536 
   0D45                     537 loadloop:
   0D45 90 7F C9            538 	mov	dptr,#OUT2BC
   0D48 F0                  539 	movx	@dptr,a		; arm EP2 OUT
   0D49 90 7F C8            540 0$:	mov	dptr,#OUT2CS
   0D4C E0                  541 	movx	a,@dptr
   0D4D 20 E1 F9            542 	jb	acc.1,0$
   0D50                     543 xxloadloop:
   0D50 90 7F C9            544 	mov	dptr,#OUT2BC
   0D53 E0                  545 	movx	a,@dptr
   0D54 60 EF               546 	jz	loadloop
   0D56 FF                  547 	mov	r7,a
   0D57 25 42               548 	add	a,cfgcount
   0D59 F5 42               549 	mov	cfgcount,a
   0D5B E4                  550 	clr	a
   0D5C 35 43               551 	addc	a,cfgcount+1
   0D5E F5 43               552 	mov	cfgcount+1,a
   0D60 90 7D C0            553 	mov	dptr,#OUT2BUF
                    0000    554 	.if	SOFTWARECONFIG
                            555 1$:	movx	a,@dptr
                            556 	mov	r3,a
                            557 	mov	r2,#8
                            558 	inc	dps
                            559 2$:	mov	a,#2
                            560 	xch	a,r3
                            561 	rrc	a
                            562 	xch	a,r3
                            563 	mov	acc.6,c
                            564 	mov	dptr,#OUTA
                            565 	movx	@dptr,a
                            566 	mov	dptr,#OUTC
                            567 	movx	a,@dptr
                            568 	setb	acc.1
                            569 	movx	@dptr,a
                            570 	clr	acc.1
                            571 	movx	@dptr,a
                            572 	djnz	r2,2$
                            573 	dec	dps
                            574 	inc	dptr
                            575 	djnz	r7,1$
                            576 	.else
   0D63 E0                  577 	movx	a,@dptr
   0D64 80 04               578 	sjmp	3$
   0D66 E0                  579 1$:	movx	a,@dptr
   0D67 30 99 FD            580 2$:	jnb	scon0+1,2$
   0D6A C2 99               581 3$:	clr	scon0+1
   0D6C F5 99               582 	mov	sbuf0,a
   0D6E A3                  583 	inc	dptr
   0D6F DF F5               584 	djnz	r7,1$
                            585 	.endif
                            586 	;; check if init went low - indicates configuration error
   0D71 90 7F 9B            587 	mov	dptr,#PINSC
   0D74 E0                  588 	movx	a,@dptr
   0D75 20 E2 0C            589 	jb	acc.2,5$
                            590 	;; INIT low
   0D78 90 7F C8            591 	mov	dptr,#OUT2CS
   0D7B 74 01               592 	mov	a,#0x01
   0D7D F0                  593 	movx	@dptr,a
   0D7E 75 40 90            594 	mov	errcode,#0x90
   0D81 02 0D DF            595 	ljmp	fpgaerr
   0D84                     596 5$:	;; check for end of FPGA configuration data
   0D84 74 9C               597 	mov	a,#<(-FPGA_CONFIGSIZE)
   0D86 25 42               598 	add	a,cfgcount
   0D88 74 D1               599 	mov	a,#>(-FPGA_CONFIGSIZE)
   0D8A 35 43               600 	addc	a,cfgcount+1
   0D8C 50 B7               601 	jnc	loadloop
                            602 	;; configuration download complete, start FPGA
   0D8E 90 7F 94            603 	mov	dptr,#PORTBCFG
   0D91 74 00               604 	mov	a,#0		; TDI no longer special function pin
   0D93 F0                  605 	movx	@dptr,a
   0D94 90 7F 95            606 	mov	dptr,#PORTCCFG
   0D97 74 00               607 	mov	a,#0		; TCK no longer special function pin
   0D99 F0                  608 	movx	@dptr,a
                            609 	;; wait for done loop
   0D9A 7F FA               610 	mov	r7,#250
   0D9C                     611 doneloop:
   0D9C 90 7F 99            612 	mov	dptr,#PINSA
   0D9F E0                  613 	movx	a,@dptr
   0DA0 20 E2 1F            614 	jb	acc.2,doneactive
   0DA3 90 7F 9B            615 	mov	dptr,#PINSC
   0DA6 E0                  616 	movx	a,@dptr
   0DA7 20 E2 06            617 	jb	acc.2,1$
                            618 	;; INIT low
   0DAA 75 40 90            619 	mov	errcode,#0x90
   0DAD 02 0D DF            620 	ljmp	fpgaerr
   0DB0 90 7F 98            621 1$:	mov	dptr,#OUTC
   0DB3 E0                  622 	movx	a,@dptr
   0DB4 D2 E1               623 	setb	acc.1
   0DB6 F0                  624 	movx	@dptr,a
   0DB7 C2 E1               625 	clr	acc.1
   0DB9 F0                  626 	movx	@dptr,a
   0DBA DF E0               627 	djnz	r7,doneloop
                            628 	;; DONE stuck low
   0DBC 75 40 91            629 	mov	errcode,#0x91
   0DBF 02 0D DF            630 	ljmp	fpgaerr
                            631 	
   0DC2                     632 doneactive:
                            633 	;; generate 16 clock pulses to start up FPGA
   0DC2 7F 10               634 	mov	r7,#16
   0DC4 90 7F 98            635 	mov	dptr,#OUTC
   0DC7 E0                  636 	movx	a,@dptr
   0DC8 D2 E1               637 0$:	setb	acc.1
   0DCA F0                  638 	movx	@dptr,a
   0DCB C2 E1               639 	clr	acc.1
   0DCD F0                  640 	movx	@dptr,a
   0DCE DF F8               641 	djnz	r7,0$
   0DD0 90 7F 95            642 	mov	dptr,#PORTCCFG
   0DD3 74 C0               643 	mov	a,#0xc0		; RD/WR is special function pin
   0DD5 F0                  644 	movx	@dptr,a
   0DD6 75 40 20            645 	mov	errcode,#0x20
                            646 
   0DD9 90 C0 08            647 	mov	dptr,#0xc008
   0DDC 74 C7               648 	mov	a,#0xc7
   0DDE F0                  649 	movx	@dptr,a
                            650 	
   0DDF                     651 fpgaerr:
   0DDF 80 FE               652 	sjmp	fpgaerr
                            653 
                            654 	.area	CSEG (CODE)
                    0002    655 	ar2 = 0x02
                    0003    656 	ar3 = 0x03
                    0004    657 	ar4 = 0x04
                    0005    658 	ar5 = 0x05
                    0006    659 	ar6 = 0x06
                    0007    660 	ar7 = 0x07
                    0000    661 	ar0 = 0x00
                    0001    662 	ar1 = 0x01
                            663 
                            664 	;; jtag_shift
                            665 	;; r2 = num
                            666 	;; r3 = tdi
                            667 	;; r4 = tms
                            668 	;; return: r7 = tdo
   0157                     669 jtag_reset_tap:
   0157 7A 05               670 	mov	r2,#5
   0159 7B 00               671 	mov	r3,#0
   015B 7C FF               672 	mov	r4,#0xff
   015D                     673 jtag_shiftout:
   015D                     674 jtag_shift:
   015D 7E 01               675 	mov	r6,#1
   015F 7F 00               676 	mov	r7,#0
   0161 74 02               677 1$:	mov	a,#2
   0163 CB                  678 	xch	a,r3
   0164 13                  679 	rrc	a
   0165 CB                  680 	xch	a,r3
   0166 92 E6               681 	mov	acc.6,c
   0168 CC                  682 	xch	a,r4
   0169 13                  683 	rrc	a
   016A CC                  684 	xch	a,r4
   016B 92 E7               685 	mov	acc.7,c
   016D 90 7F 96            686 	mov	dptr,#OUTA
   0170 F0                  687 	movx	@dptr,a
   0171 90 7F 9B            688 	mov	dptr,#PINSC
   0174 E0                  689 	movx	a,@dptr
   0175 30 E0 03            690 	jnb	acc.0,2$
   0178 EE                  691 	mov	a,r6
   0179 42 07               692 	orl	ar7,a
   017B 90 7F 98            693 2$:	mov	dptr,#OUTC
   017E E0                  694 	movx	a,@dptr
   017F D2 E1               695 	setb	acc.1
   0181 F0                  696 	movx	@dptr,a
   0182 C2 E1               697 	clr	acc.1
   0184 F0                  698 	movx	@dptr,a
   0185 EE                  699 	mov	a,r6
   0186 23                  700 	rl	a
   0187 FE                  701 	mov	r6,a
   0188 DA D7               702 	djnz	r2,1$
   018A 22                  703 	ret
                            704 
                            705 	;; EEPROM address where the serial number is located
                            706 	
   018B                     707 eepromstraddr:
   018B 10                  708 	.db	0x10
                            709 
                            710 	;; I2C Routines
                            711 	;; note: ckcon should be set to #0x31 to avoid chip bugs
                            712 
   018C                     713 writei2c:
                            714 	;; dptr: data to be sent
                            715 	;; b:    device address
                            716 	;; r7:   transfer length
                            717 	;; return: a: status (bytes remaining, 0xff bus error)
   018C 05 86               718 	inc	dps
   018E 90 7F A5            719 	mov	dptr,#I2CS
   0191 C2 F0               720 	clr	b.0		; r/w = 0
   0193 74 80               721 	mov	a,#0x80		; start condition
   0195 F0                  722 	movx	@dptr,a
   0196 90 7F A6            723 	mov	dptr,#I2DAT
   0199 E5 F0               724 	mov	a,b
   019B F0                  725 	movx	@dptr,a
   019C 90 7F A5            726 	mov	dptr,#I2CS	
   019F E0                  727 0$:	movx	a,@dptr
   01A0 20 E2 20            728 	jb	acc.2,3$	; BERR
   01A3 30 E0 F9            729 	jnb	acc.0,0$	; DONE
   01A6 30 E1 0F            730 	jnb	acc.1,1$	; ACK
   01A9 90 7F A6            731 	mov	dptr,#I2DAT
   01AC 15 86               732 	dec	dps
   01AE E0                  733 	movx	a,@dptr
   01AF A3                  734 	inc	dptr
   01B0 05 86               735 	inc	dps
   01B2 F0                  736 	movx	@dptr,a
   01B3 90 7F A5            737 	mov	dptr,#I2CS
   01B6 DF E7               738 	djnz	r7,0$
   01B8 74 40               739 1$:	mov	a,#0x40		; stop condition
   01BA F0                  740 	movx	@dptr,a
   01BB E0                  741 2$:	movx	a,@dptr
   01BC 20 E6 FC            742 	jb	acc.6,2$
   01BF 15 86               743 	dec	dps
   01C1 EF                  744 	mov	a,r7
   01C2 22                  745 	ret
   01C3 7F FF               746 3$:	mov	r7,#255
   01C5 80 F1               747 	sjmp	1$
                            748 
   01C7                     749 readi2c:
                            750 	;; dptr: data to be sent
                            751 	;; b:    device address
                            752 	;; r7:   transfer length
                            753 	;; return: a: status (bytes remaining, 0xff bus error)
   01C7 05 86               754 	inc	dps
   01C9 90 7F A5            755 	mov	dptr,#I2CS
   01CC D2 F0               756 	setb	b.0		; r/w = 1
   01CE 74 80               757 	mov	a,#0x80		; start condition
   01D0 F0                  758 	movx	@dptr,a
   01D1 90 7F A6            759 	mov	dptr,#I2DAT
   01D4 E5 F0               760 	mov	a,b
   01D6 F0                  761 	movx	@dptr,a
   01D7 90 7F A5            762 	mov	dptr,#I2CS
   01DA E0                  763 0$:	movx	a,@dptr
   01DB 20 E2 4C            764 	jb	acc.2,9$	; BERR
   01DE 30 E0 F9            765 	jnb	acc.0,0$	; DONE
   01E1 30 E1 03            766 	jnb	acc.1,5$	; ACK
   01E4 EF                  767 	mov	a,r7
   01E5 70 0B               768 	jnz	1$
   01E7 74 40               769 5$:	mov	a,#0x40		; stop condition
   01E9 F0                  770 	movx	@dptr,a
   01EA E0                  771 2$:	movx	a,@dptr
   01EB 20 E6 FC            772 	jb	acc.6,2$
   01EE 15 86               773 	dec	dps
   01F0 E4                  774 	clr	a
   01F1 22                  775 	ret
   01F2 B4 01 03            776 1$:	cjne	a,#1,3$
   01F5 74 20               777 	mov	a,#0x20		; LASTRD = 1
   01F7 F0                  778 	movx	@dptr,a
   01F8 90 7F A6            779 3$:	mov	dptr,#I2DAT
   01FB E0                  780 	movx	a,@dptr		; initiate first read
   01FC 90 7F A5            781 	mov	dptr,#I2CS
   01FF E0                  782 4$:	movx	a,@dptr
   0200 20 E2 27            783 	jb	acc.2,9$	; BERR
   0203 30 E0 F9            784 	jnb	acc.0,4$	; DONE
   0206 EF                  785 	mov	a,r7
   0207 B4 02 03            786 	cjne	a,#2,6$
   020A 74 20               787 	mov	a,#0x20		; LASTRD = 1
   020C F0                  788 	movx	@dptr,a
   020D B4 01 03            789 6$:	cjne	a,#1,7$
   0210 74 40               790 	mov	a,#0x40		; stop condition
   0212 F0                  791 	movx	@dptr,a
   0213 90 7F A6            792 7$:	mov	dptr,#I2DAT
   0216 E0                  793 	movx	a,@dptr		; read data
   0217 15 86               794 	dec	dps
   0219 F0                  795 	movx	@dptr,a
   021A A3                  796 	inc	dptr
   021B 05 86               797 	inc	dps
   021D 90 7F A5            798 	mov	dptr,#I2CS
   0220 DF DD               799 	djnz	r7,4$
   0222 E0                  800 8$:	movx	a,@dptr
   0223 20 E6 FC            801 	jb	acc.6,8$
   0226 15 86               802 	dec	dps
   0228 E4                  803 	clr	a
   0229 22                  804 	ret
   022A 74 40               805 9$:	mov	a,#0x40		; stop condition
   022C F0                  806 	movx	@dptr,a
   022D E0                  807 10$:	movx	a,@dptr
   022E 20 E6 FC            808 	jb	acc.6,10$
   0231 74 FF               809 	mov	a,#255
   0233 15 86               810 	dec	dps
   0235 22                  811 	ret
                            812 
                            813 	;; ------------------ interrupt handlers ------------------------
                            814 
   0236                     815 int0_isr:
   0236 C0 E0               816 	push	acc
   0238 C0 F0               817 	push	b
   023A C0 82               818 	push	dpl0
   023C C0 83               819 	push	dph0
   023E C0 D0               820 	push	psw
   0240 75 D0 00            821 	mov	psw,#0x00
   0243 C0 86               822 	push	dps
   0245 75 86 00            823 	mov	dps,#0
                            824 	;; clear interrupt
   0248 C2 89               825 	clr	tcon+1
                            826 	;; handle interrupt
                            827 	;; epilogue
   024A D0 86               828 	pop	dps
   024C D0 D0               829 	pop	psw
   024E D0 83               830 	pop	dph0
   0250 D0 82               831 	pop	dpl0
   0252 D0 F0               832 	pop	b
   0254 D0 E0               833 	pop	acc
   0256 32                  834 	reti
                            835 
   0257                     836 timer0_isr:
   0257 C0 E0               837 	push	acc
   0259 C0 F0               838 	push	b
   025B C0 82               839 	push	dpl0
   025D C0 83               840 	push	dph0
   025F C0 D0               841 	push	psw
   0261 75 D0 00            842 	mov	psw,#0x00
   0264 C0 86               843 	push	dps
   0266 75 86 00            844 	mov	dps,#0
                            845 	;; clear interrupt
   0269 C2 8D               846 	clr	tcon+5
                            847 	;; handle interrupt
   026B 05 44               848 	inc	leddiv
   026D E5 44               849 	mov	a,leddiv
   026F 54 07               850 	anl	a,#7
   0271 70 07               851 	jnz	0$
   0273 90 7F 98            852 	mov	dptr,#OUTC
   0276 E0                  853 	movx	a,@dptr
   0277 64 08               854 	xrl	a,#0x08
   0279 F0                  855 	movx	@dptr,a
   027A                     856 0$:
                            857 	;; epilogue
   027A D0 86               858 	pop	dps
   027C D0 D0               859 	pop	psw
   027E D0 83               860 	pop	dph0
   0280 D0 82               861 	pop	dpl0
   0282 D0 F0               862 	pop	b
   0284 D0 E0               863 	pop	acc
   0286 32                  864 	reti
                            865 
   0287                     866 int1_isr:
   0287 C0 E0               867 	push	acc
   0289 C0 F0               868 	push	b
   028B C0 82               869 	push	dpl0
   028D C0 83               870 	push	dph0
   028F C0 D0               871 	push	psw
   0291 75 D0 00            872 	mov	psw,#0x00
   0294 C0 86               873 	push	dps
   0296 75 86 00            874 	mov	dps,#0
                            875 	;; clear interrupt
   0299 C2 8B               876 	clr	tcon+3
                            877 	;; handle interrupt
                            878 	;; epilogue
   029B D0 86               879 	pop	dps
   029D D0 D0               880 	pop	psw
   029F D0 83               881 	pop	dph0
   02A1 D0 82               882 	pop	dpl0
   02A3 D0 F0               883 	pop	b
   02A5 D0 E0               884 	pop	acc
   02A7 32                  885 	reti
                            886 
   02A8                     887 timer1_isr:
   02A8 C0 E0               888 	push	acc
   02AA C0 F0               889 	push	b
   02AC C0 82               890 	push	dpl0
   02AE C0 83               891 	push	dph0
   02B0 C0 D0               892 	push	psw
   02B2 75 D0 00            893 	mov	psw,#0x00
   02B5 C0 86               894 	push	dps
   02B7 75 86 00            895 	mov	dps,#0
                            896 	;; clear interrupt
   02BA C2 8F               897 	clr	tcon+7
                            898 	;; handle interrupt
                            899 	;; epilogue
   02BC D0 86               900 	pop	dps
   02BE D0 D0               901 	pop	psw
   02C0 D0 83               902 	pop	dph0
   02C2 D0 82               903 	pop	dpl0
   02C4 D0 F0               904 	pop	b
   02C6 D0 E0               905 	pop	acc
   02C8 32                  906 	reti
                            907 
   02C9                     908 ser0_isr:
   02C9 C0 E0               909 	push	acc
   02CB C0 F0               910 	push	b
   02CD C0 82               911 	push	dpl0
   02CF C0 83               912 	push	dph0
   02D1 C0 D0               913 	push	psw
   02D3 75 D0 00            914 	mov	psw,#0x00
   02D6 C0 86               915 	push	dps
   02D8 75 86 00            916 	mov	dps,#0
                            917 	;; clear interrupt
   02DB C2 98               918 	clr	scon0+0
   02DD C2 99               919 	clr	scon0+1
                            920 	;; handle interrupt
                            921 	;; epilogue
   02DF D0 86               922 	pop	dps
   02E1 D0 D0               923 	pop	psw
   02E3 D0 83               924 	pop	dph0
   02E5 D0 82               925 	pop	dpl0
   02E7 D0 F0               926 	pop	b
   02E9 D0 E0               927 	pop	acc
   02EB 32                  928 	reti
                            929 
   02EC                     930 timer2_isr:
   02EC C0 E0               931 	push	acc
   02EE C0 F0               932 	push	b
   02F0 C0 82               933 	push	dpl0
   02F2 C0 83               934 	push	dph0
   02F4 C0 D0               935 	push	psw
   02F6 75 D0 00            936 	mov	psw,#0x00
   02F9 C0 86               937 	push	dps
   02FB 75 86 00            938 	mov	dps,#0
                            939 	;; clear interrupt
   02FE C2 CF               940 	clr	t2con+7
                            941 	;; handle interrupt
                            942 	;; epilogue
   0300 D0 86               943 	pop	dps
   0302 D0 D0               944 	pop	psw
   0304 D0 83               945 	pop	dph0
   0306 D0 82               946 	pop	dpl0
   0308 D0 F0               947 	pop	b
   030A D0 E0               948 	pop	acc
   030C 32                  949 	reti
                            950 
   030D                     951 resume_isr:
   030D C0 E0               952 	push	acc
   030F C0 F0               953 	push	b
   0311 C0 82               954 	push	dpl0
   0313 C0 83               955 	push	dph0
   0315 C0 D0               956 	push	psw
   0317 75 D0 00            957 	mov	psw,#0x00
   031A C0 86               958 	push	dps
   031C 75 86 00            959 	mov	dps,#0
                            960 	;; clear interrupt
   031F C2 DC               961 	clr	eicon+4
                            962 	;; handle interrupt
                            963 	;; epilogue
   0321 D0 86               964 	pop	dps
   0323 D0 D0               965 	pop	psw
   0325 D0 83               966 	pop	dph0
   0327 D0 82               967 	pop	dpl0
   0329 D0 F0               968 	pop	b
   032B D0 E0               969 	pop	acc
   032D 32                  970 	reti
                            971 
   032E                     972 ser1_isr:
   032E C0 E0               973 	push	acc
   0330 C0 F0               974 	push	b
   0332 C0 82               975 	push	dpl0
   0334 C0 83               976 	push	dph0
   0336 C0 D0               977 	push	psw
   0338 75 D0 00            978 	mov	psw,#0x00
   033B C0 86               979 	push	dps
   033D 75 86 00            980 	mov	dps,#0
                            981 	;; clear interrupt
   0340 C2 C0               982 	clr	scon1+0
   0342 C2 C1               983 	clr	scon1+1
                            984 	;; handle interrupt
                            985 	;; epilogue
   0344 D0 86               986 	pop	dps
   0346 D0 D0               987 	pop	psw
   0348 D0 83               988 	pop	dph0
   034A D0 82               989 	pop	dpl0
   034C D0 F0               990 	pop	b
   034E D0 E0               991 	pop	acc
   0350 32                  992 	reti
                            993 
   0351                     994 i2c_isr:
   0351 C0 E0               995 	push	acc
   0353 C0 F0               996 	push	b
   0355 C0 82               997 	push	dpl0
   0357 C0 83               998 	push	dph0
   0359 C0 D0               999 	push	psw
   035B 75 D0 00           1000 	mov	psw,#0x00
   035E C0 86              1001 	push	dps
   0360 75 86 00           1002 	mov	dps,#0
                           1003 	;; clear interrupt
   0363 E5 91              1004 	mov	a,exif
   0365 C2 E5              1005 	clr	acc.5
   0367 F5 91              1006 	mov	exif,a
                           1007 	;; handle interrupt
                           1008 	;; epilogue
   0369 D0 86              1009 	pop	dps
   036B D0 D0              1010 	pop	psw
   036D D0 83              1011 	pop	dph0
   036F D0 82              1012 	pop	dpl0
   0371 D0 F0              1013 	pop	b
   0373 D0 E0              1014 	pop	acc
   0375 32                 1015 	reti
                           1016 
   0376                    1017 int4_isr:
   0376 C0 E0              1018 	push	acc
   0378 C0 F0              1019 	push	b
   037A C0 82              1020 	push	dpl0
   037C C0 83              1021 	push	dph0
   037E C0 D0              1022 	push	psw
   0380 75 D0 00           1023 	mov	psw,#0x00
   0383 C0 86              1024 	push	dps
   0385 75 86 00           1025 	mov	dps,#0
                           1026 	;; clear interrupt
   0388 E5 91              1027 	mov	a,exif
   038A C2 E6              1028 	clr	acc.6
   038C F5 91              1029 	mov	exif,a
                           1030 	;; handle interrupt
                           1031 	;; epilogue
   038E D0 86              1032 	pop	dps
   0390 D0 D0              1033 	pop	psw
   0392 D0 83              1034 	pop	dph0
   0394 D0 82              1035 	pop	dpl0
   0396 D0 F0              1036 	pop	b
   0398 D0 E0              1037 	pop	acc
   039A 32                 1038 	reti
                           1039 
   039B                    1040 int5_isr:
   039B C0 E0              1041 	push	acc
   039D C0 F0              1042 	push	b
   039F C0 82              1043 	push	dpl0
   03A1 C0 83              1044 	push	dph0
   03A3 C0 D0              1045 	push	psw
   03A5 75 D0 00           1046 	mov	psw,#0x00
   03A8 C0 86              1047 	push	dps
   03AA 75 86 00           1048 	mov	dps,#0
                           1049 	;; clear interrupt
   03AD E5 91              1050 	mov	a,exif
   03AF C2 E7              1051 	clr	acc.7
   03B1 F5 91              1052 	mov	exif,a
                           1053 	;; handle interrupt
                           1054 	;; epilogue
   03B3 D0 86              1055 	pop	dps
   03B5 D0 D0              1056 	pop	psw
   03B7 D0 83              1057 	pop	dph0
   03B9 D0 82              1058 	pop	dpl0
   03BB D0 F0              1059 	pop	b
   03BD D0 E0              1060 	pop	acc
   03BF 32                 1061 	reti
                           1062 
   03C0                    1063 int6_isr:
   03C0 C0 E0              1064 	push	acc
   03C2 C0 F0              1065 	push	b
   03C4 C0 82              1066 	push	dpl0
   03C6 C0 83              1067 	push	dph0
   03C8 C0 D0              1068 	push	psw
   03CA 75 D0 00           1069 	mov	psw,#0x00
   03CD C0 86              1070 	push	dps
   03CF 75 86 00           1071 	mov	dps,#0
                           1072 	;; clear interrupt
   03D2 C2 DB              1073 	clr	eicon+3
                           1074 	;; handle interrupt
                           1075 	;; epilogue
   03D4 D0 86              1076 	pop	dps
   03D6 D0 D0              1077 	pop	psw
   03D8 D0 83              1078 	pop	dph0
   03DA D0 82              1079 	pop	dpl0
   03DC D0 F0              1080 	pop	b
   03DE D0 E0              1081 	pop	acc
   03E0 32                 1082 	reti
                           1083 
   03E1                    1084 xmemread::
   03E1 E5 49              1085 	mov	a,ctrllen
   03E3 FF                 1086 	mov	r7,a
   03E4 24 C0              1087 	add	a,#-64
   03E6 F5 49              1088 	mov	ctrllen,a
   03E8 E5 4A              1089 	mov	a,ctrllen+1
   03EA 34 00              1090 	addc	a,#0
   03EC 50 12              1091 	jnc	0$
   03EE E4                 1092 	clr	a
   03EF F5 49              1093 	mov	ctrllen,a
   03F1 F5 4A              1094 	mov	ctrllen+1,a
   03F3 F5 46              1095 	mov	ctrlcode,a
   03F5 74 02              1096 	mov	a,#2		; ack control transfer
   03F7 90 7F B4           1097 	mov	dptr,#EP0CS
   03FA F0                 1098 	movx	@dptr,a
   03FB EF                 1099 	mov	a,r7
   03FC 60 25              1100 	jz	2$
   03FE 80 04              1101 	sjmp	1$
   0400 F5 4A              1102 0$:	mov	ctrllen+1,a
   0402 7F 40              1103 	mov	r7,#64
   0404 8F 00              1104 1$:	mov	ar0,r7
   0406 85 47 82           1105 	mov	dpl0,ctrladdr
   0409 85 48 83           1106 	mov	dph0,ctrladdr+1
   040C 05 86              1107 	inc	dps
   040E 90 7F 00           1108 	mov	dptr,#IN0BUF
   0411 15 86              1109 	dec	dps
   0413 E0                 1110 3$:	movx	a,@dptr
   0414 A3                 1111 	inc	dptr
   0415 05 86              1112 	inc	dps
   0417 F0                 1113 	movx	@dptr,a
   0418 A3                 1114 	inc	dptr
   0419 15 86              1115 	dec	dps
   041B D8 F6              1116 	djnz	r0,3$
   041D 85 82 47           1117 	mov	ctrladdr,dpl0
   0420 85 83 48           1118 	mov	ctrladdr+1,dph0
   0423 EF                 1119 2$:	mov	a,r7
   0424 90 7F B5           1120 	mov	dptr,#IN0BC
   0427 F0                 1121 	movx	@dptr,a
   0428 22                 1122 	ret
                           1123 
   0429                    1124 usb_sudav_isr:
   0429 C0 E0              1125 	push	acc
   042B C0 F0              1126 	push	b
   042D C0 82              1127 	push	dpl0
   042F C0 83              1128 	push	dph0
   0431 C0 84              1129 	push	dpl1
   0433 C0 85              1130 	push	dph1
   0435 C0 D0              1131 	push	psw
   0437 75 D0 00           1132 	mov	psw,#0x00
   043A C0 86              1133 	push	dps
   043C 75 86 00           1134 	mov	dps,#0
   043F C0 00              1135 	push	ar0
   0441 C0 07              1136 	push	ar7
                           1137 	;; clear interrupt
   0443 E5 91              1138 	mov	a,exif
   0445 C2 E4              1139 	clr	acc.4
   0447 F5 91              1140 	mov	exif,a
   0449 90 7F AB           1141 	mov	dptr,#USBIRQ
   044C 74 01              1142 	mov	a,#0x01
   044E F0                 1143 	movx	@dptr,a
                           1144 	;; handle interrupt
   044F 75 46 00           1145 	mov	ctrlcode,#0		; reset control out code
   0452 90 7F E9           1146 	mov	dptr,#SETUPDAT+1
   0455 E0                 1147 	movx	a,@dptr			; bRequest field
                           1148 	;; standard commands
                           1149 	;; USB_REQ_GET_DESCRIPTOR
   0456 B4 06 59           1150 	cjne	a,#USB_REQ_GET_DESCRIPTOR,cmdnotgetdesc
   0459 90 7F E8           1151 	mov	dptr,#SETUPDAT		; bRequestType == 0x80
   045C E0                 1152 	movx	a,@dptr
   045D B4 80 4F           1153 	cjne	a,#USB_DIR_IN,setupstallstd
   0460 90 7F EB           1154 	mov	dptr,#SETUPDAT+3
   0463 E0                 1155 	movx	a,@dptr
   0464 B4 01 0C           1156 	cjne	a,#USB_DT_DEVICE,cmdnotgetdescdev
   0467 90 7F D4           1157 	mov	dptr,#SUDPTRH
   046A 74 0B              1158 	mov	a,#>devicedescr
   046C F0                 1159 	movx	@dptr,a
   046D A3                 1160 	inc	dptr
   046E 74 2C              1161 	mov	a,#<devicedescr
   0470 F0                 1162 	movx	@dptr,a
   0471 80 39              1163 	sjmp	setupackstd
   0473                    1164 cmdnotgetdescdev:
   0473 B4 02 12           1165 	cjne	a,#USB_DT_CONFIG,cmdnotgetdesccfg
   0476 90 7F EA           1166 	mov	dptr,#SETUPDAT+2
   0479 E0                 1167 	movx	a,@dptr
   047A 70 33              1168 	jnz	setupstallstd
   047C 90 7F D4           1169 	mov	dptr,#SUDPTRH
   047F 74 0B              1170 	mov	a,#>config0descr
   0481 F0                 1171 	movx	@dptr,a
   0482 A3                 1172 	inc	dptr
   0483 74 3E              1173 	mov	a,#<config0descr
   0485 F0                 1174 	movx	@dptr,a
   0486 80 24              1175 	sjmp	setupackstd
   0488                    1176 cmdnotgetdesccfg:
   0488 B4 03 24           1177 	cjne	a,#USB_DT_STRING,setupstallstd
   048B 90 7F EA           1178 	mov	dptr,#SETUPDAT+2
   048E E0                 1179 	movx	a,@dptr
   048F 24 FC              1180 	add	a,#-numstrings
   0491 40 1C              1181 	jc	setupstallstd
   0493 E0                 1182 	movx	a,@dptr
   0494 25 E0              1183 	add	a,acc
   0496 24 83              1184 	add	a,#<stringdescr
   0498 F5 82              1185 	mov	dpl0,a
   049A E4                 1186 	clr	a
   049B 34 0B              1187 	addc	a,#>stringdescr
   049D F5 83              1188 	mov	dph0,a
   049F E0                 1189 	movx	a,@dptr
   04A0 F5 F0              1190 	mov	b,a
   04A2 A3                 1191 	inc	dptr
   04A3 E0                 1192 	movx	a,@dptr
   04A4 90 7F D4           1193 	mov	dptr,#SUDPTRH
   04A7 F0                 1194 	movx	@dptr,a
   04A8 A3                 1195 	inc	dptr
   04A9 E5 F0              1196 	mov	a,b
   04AB F0                 1197 	movx	@dptr,a
                           1198 	; sjmp	setupackstd	
   04AC                    1199 setupackstd:
   04AC 02 06 F6           1200 	ljmp	setupack
   04AF                    1201 setupstallstd:
   04AF 02 06 F2           1202 	ljmp	setupstall
   04B2                    1203 cmdnotgetdesc:
                           1204 	;; USB_REQ_SET_CONFIGURATION
   04B2 B4 09 41           1205 	cjne	a,#USB_REQ_SET_CONFIGURATION,cmdnotsetconf
   04B5 90 7F E8           1206 	mov	dptr,#SETUPDAT
   04B8 E0                 1207 	movx	a,@dptr
   04B9 70 F4              1208 	jnz	setupstallstd
   04BB 90 7F EA           1209 	mov	dptr,#SETUPDAT+2
   04BE E0                 1210 	movx	a,@dptr
   04BF 24 FE              1211 	add	a,#-2
   04C1 40 EC              1212 	jc	setupstallstd
   04C3 E0                 1213 	movx	a,@dptr
   04C4 F5 4B              1214 	mov	numconfig,a
   04C6                    1215 cmdresettoggleshalt:
   04C6 90 7F D7           1216 	mov	dptr,#TOGCTL
   04C9 78 07              1217 	mov	r0,#7
   04CB E8                 1218 0$:	mov	a,r0
   04CC 44 10              1219 	orl	a,#0x10
   04CE F0                 1220 	movx	@dptr,a
   04CF 44 30              1221 	orl	a,#0x30
   04D1 F0                 1222 	movx	@dptr,a
   04D2 E8                 1223 	mov	a,r0
   04D3 F0                 1224 	movx	@dptr,a
   04D4 44 20              1225 	orl	a,#0x20
   04D6 F0                 1226 	movx	@dptr,a
   04D7 D8 F2              1227 	djnz	r0,0$
   04D9 E4                 1228 	clr	a
   04DA F0                 1229 	movx	@dptr,a
   04DB 74 02              1230 	mov	a,#2
   04DD 90 7F B6           1231 	mov	dptr,#IN1CS
   04E0 78 07              1232 	mov	r0,#7
   04E2 F0                 1233 1$:	movx	@dptr,a
   04E3 A3                 1234 	inc	dptr
   04E4 A3                 1235 	inc	dptr
   04E5 D8 FB              1236 	djnz	r0,1$
   04E7 90 7F C6           1237 	mov	dptr,#OUT1CS
   04EA 78 07              1238 	mov	r0,#7
   04EC F0                 1239 2$:	movx	@dptr,a
   04ED A3                 1240 	inc	dptr
   04EE A3                 1241 	inc	dptr
   04EF D8 FB              1242 	djnz	r0,2$
   04F1 12 08 B0           1243 	lcall	fillusbintr
   04F4 80 B6              1244 	sjmp	setupackstd
   04F6                    1245 cmdnotsetconf:
                           1246 	;; USB_REQ_SET_INTERFACE
   04F6 B4 0B 1A           1247 	cjne	a,#USB_REQ_SET_INTERFACE,cmdnotsetint
   04F9 90 7F E8           1248 	mov	dptr,#SETUPDAT
   04FC E0                 1249 	movx	a,@dptr
   04FD B4 01 AF           1250 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_OUT,setupstallstd
   0500 E5 4B              1251 	mov	a,numconfig
   0502 B4 01 AA           1252 	cjne	a,#1,setupstallstd
   0505 90 7F EC           1253 	mov	dptr,#SETUPDAT+4
   0508 E0                 1254 	movx	a,@dptr
   0509 70 A4              1255 	jnz	setupstallstd
   050B 90 7F EA           1256 	mov	dptr,#SETUPDAT+2
   050E E0                 1257 	movx	a,@dptr
   050F F5 4C              1258 	mov	altsetting,a
   0511 80 B3              1259 	sjmp	cmdresettoggleshalt
   0513                    1260 cmdnotsetint:
                           1261 	;; USB_REQ_GET_INTERFACE
   0513 B4 0A 20           1262 	cjne	a,#USB_REQ_GET_INTERFACE,cmdnotgetint
   0516 90 7F E8           1263 	mov	dptr,#SETUPDAT
   0519 E0                 1264 	movx	a,@dptr
   051A B4 81 92           1265 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,setupstallstd
   051D E5 4B              1266 	mov	a,numconfig
   051F B4 01 8D           1267 	cjne	a,#1,setupstallstd
   0522 90 7F EC           1268 	mov	dptr,#SETUPDAT+4
   0525 E0                 1269 	movx	a,@dptr
   0526 70 87              1270 	jnz	setupstallstd
   0528 E5 4C              1271 	mov	a,altsetting
   052A                    1272 cmdrespondonebyte:
   052A 90 7F 00           1273 	mov	dptr,#IN0BUF
   052D F0                 1274 	movx	@dptr,a
   052E 90 7F B5           1275 	mov	dptr,#IN0BC
   0531 74 01              1276 	mov	a,#1
   0533 F0                 1277 	movx	@dptr,a	
   0534 80 4E              1278 	sjmp	setupackstd2
   0536                    1279 cmdnotgetint:
                           1280 	;; USB_REQ_GET_CONFIGURATION
   0536 B4 08 0B           1281 	cjne	a,#USB_REQ_GET_CONFIGURATION,cmdnotgetconf
   0539 90 7F E8           1282 	mov	dptr,#SETUPDAT
   053C E0                 1283 	movx	a,@dptr
   053D B4 80 47           1284 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,setupstallstd2
   0540 E5 4B              1285 	mov	a,numconfig
   0542 80 E6              1286 	sjmp	cmdrespondonebyte	
   0544                    1287 cmdnotgetconf:
                           1288 	;; USB_REQ_GET_STATUS (0)
   0544 70 44              1289 	jnz	cmdnotgetstat
   0546 90 7F E8           1290 	mov	dptr,#SETUPDAT
   0549 E0                 1291 	movx	a,@dptr
   054A B4 80 11           1292 	cjne	a,#USB_RECIP_DEVICE|USB_DIR_IN,cmdnotgetstatdev
   054D 74 01              1293 	mov	a,#1
   054F                    1294 cmdrespondstat:
   054F 90 7F 00           1295 	mov	dptr,#IN0BUF
   0552 F0                 1296 	movx	@dptr,a
   0553 A3                 1297 	inc	dptr
   0554 E4                 1298 	clr	a
   0555 F0                 1299 	movx	@dptr,a
   0556 90 7F B5           1300 	mov	dptr,#IN0BC
   0559 74 02              1301 	mov	a,#2
   055B F0                 1302 	movx	@dptr,a	
   055C 80 26              1303 	sjmp	setupackstd2
   055E                    1304 cmdnotgetstatdev:
   055E B4 81 03           1305 	cjne	a,#USB_RECIP_INTERFACE|USB_DIR_IN,cmdnotgetstatintf
   0561 E4                 1306 	clr	a
   0562 80 EB              1307 	sjmp	cmdrespondstat
   0564                    1308 cmdnotgetstatintf:
   0564 B4 82 20           1309 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_IN,setupstallstd2
   0567 90 7F EC           1310 	mov	dptr,#SETUPDAT+4
   056A E0                 1311 	movx	a,@dptr
   056B 90 7F C4           1312 	mov	dptr,#OUT1CS-2
   056E 30 E7 03           1313 	jnb	acc.7,0$
   0571 90 7F B4           1314 	mov	dptr,#IN1CS-2
   0574 54 0F              1315 0$:	anl	a,#15
   0576 60 0F              1316 	jz	setupstallstd2
   0578 20 E3 0C           1317 	jb	acc.3,setupstallstd2
   057B 25 E0              1318 	add	a,acc
   057D 25 82              1319 	add	a,dpl0
   057F F5 82              1320 	mov	dpl0,a
   0581 E0                 1321 	movx	a,@dptr
   0582 80 CB              1322 	sjmp	cmdrespondstat
   0584                    1323 setupackstd2:
   0584 02 06 F6           1324 	ljmp	setupack
   0587                    1325 setupstallstd2:
   0587 02 06 F2           1326 	ljmp	setupstall
   058A                    1327 cmdnotgetstat:
                           1328 	;; USB_REQ_SET_FEATURE
   058A B4 03 05           1329 	cjne	a,#USB_REQ_SET_FEATURE,cmdnotsetftr
   058D 75 F0 01           1330 	mov	b,#1
   0590 80 06              1331 	sjmp	handleftr
   0592                    1332 cmdnotsetftr:
                           1333 	;; USB_REQ_CLEAR_FEATURE
   0592 B4 01 3E           1334 	cjne	a,#USB_REQ_CLEAR_FEATURE,cmdnotclrftr
   0595 75 F0 00           1335 	mov	b,#0
   0598                    1336 handleftr:
   0598 90 7F E8           1337 	mov	dptr,#SETUPDAT
   059B E0                 1338 	movx	a,@dptr
   059C B4 02 E8           1339 	cjne	a,#USB_RECIP_ENDPOINT|USB_DIR_OUT,setupstallstd2
   059F A3                 1340 	inc	dptr
   05A0 A3                 1341 	inc	dptr
   05A1 E0                 1342 	movx	a,@dptr
   05A2 70 E3              1343 	jnz	setupstallstd2	; not ENDPOINT_HALT feature
   05A4 A3                 1344 	inc	dptr
   05A5 E0                 1345 	movx	a,@dptr
   05A6 70 DF              1346 	jnz	setupstallstd2
   05A8 A3                 1347 	inc	dptr
   05A9 E0                 1348 	movx	a,@dptr
   05AA 90 7F C4           1349 	mov	dptr,#OUT1CS-2
   05AD 30 E7 05           1350 	jnb	acc.7,0$
   05B0 90 7F B4           1351 	mov	dptr,#IN1CS-2
   05B3 44 10              1352 	orl	a,#0x10
   05B5 20 E3 CF           1353 0$:	jb	acc.3,setupstallstd2
                           1354 	;; clear data toggle
   05B8 54 1F              1355 	anl	a,#0x1f
   05BA 05 86              1356 	inc	dps
   05BC 90 7F D7           1357 	mov	dptr,#TOGCTL
   05BF F0                 1358 	movx	@dptr,a
   05C0 44 20              1359 	orl	a,#0x20
   05C2 F0                 1360 	movx	@dptr,a
   05C3 54 0F              1361 	anl	a,#15
   05C5 F0                 1362 	movx	@dptr,a
   05C6 15 86              1363 	dec	dps	
                           1364 	;; clear/set ep halt feature
   05C8 25 E0              1365 	add	a,acc
   05CA 25 82              1366 	add	a,dpl0
   05CC F5 82              1367 	mov	dpl0,a
   05CE E5 F0              1368 	mov	a,b
   05D0 F0                 1369 	movx	@dptr,a
   05D1 80 B1              1370 	sjmp	setupackstd2
   05D3                    1371 cmdnotclrftr:
                           1372 	;; vendor specific commands
                           1373 	;; 0xa3
   05D3 B4 A3 3E           1374 	cjne	a,#0xa3,cmdnota3
   05D6 90 7F EA           1375 	mov	dptr,#SETUPDAT+2
   05D9 E0                 1376 	movx	a,@dptr
   05DA F5 47              1377 	mov	ctrladdr,a
   05DC A3                 1378 	inc	dptr
   05DD E0                 1379 	movx	a,@dptr
   05DE F5 48              1380 	mov	ctrladdr+1,a
   05E0 24 50              1381 	add	a,#80
   05E2 50 2D              1382 	jnc	setupstalla3
   05E4 90 7F EE           1383 	mov	dptr,#SETUPDAT+6
   05E7 E0                 1384 	movx	a,@dptr
   05E8 F5 49              1385 	mov	ctrllen,a
   05EA 25 47              1386 	add	a,ctrladdr
   05EC A3                 1387 	inc	dptr
   05ED E0                 1388 	movx	a,@dptr
   05EE F5 4A              1389 	mov	ctrllen+1,a
   05F0 35 48              1390 	addc	a,ctrladdr+1
   05F2 40 1D              1391 	jc	setupstalla3
   05F4 90 7F E8           1392 	mov	dptr,#SETUPDAT		; bRequestType == 0x40
   05F7 E0                 1393 	movx	a,@dptr
   05F8 B4 40 0A           1394 	cjne	a,#0x40,1$
   05FB 75 46 01           1395 	mov	ctrlcode,#1
   05FE 90 7F C5           1396 	mov	dptr,#OUT0BC
   0601 F0                 1397 	movx	@dptr,a
   0602 02 06 FC           1398 	ljmp	endusbisr
   0605 B4 C0 09           1399 1$:	cjne	a,#0xc0,setupstalla3	; bRequestType == 0xc0
   0608 75 46 02           1400 	mov	ctrlcode,#2
   060B 12 03 E1           1401 	lcall	xmemread
   060E 02 06 FC           1402 	ljmp	endusbisr
   0611                    1403 setupstalla3:
   0611 02 06 F2           1404 	ljmp	setupstall
   0614                    1405 cmdnota3:
                           1406 	;; 0xb1
   0614 B4 B1 3E           1407 	cjne	a,#0xb1,cmdnotb1
   0617 90 7F E8           1408 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   061A E0                 1409 	movx	a,@dptr
   061B B4 C0 34           1410 	cjne	a,#0xc0,setupstallb1
   061E 90 7F 00           1411 	mov	dptr,#IN0BUF
   0621 E5 40              1412 	mov	a,errcode
   0623 F0                 1413 	movx	@dptr,a
   0624 A3                 1414 	inc	dptr
   0625 E5 41              1415 	mov	a,errval
   0627 F0                 1416 	movx	@dptr,a
   0628 A3                 1417 	inc	dptr
   0629 E5 81              1418 	mov	a,sp
                           1419 
                           1420 ;;; xxxx
   062B C0 82              1421 	push	dpl0
   062D C0 83              1422 	push	dph0
   062F 90 7F DF           1423 	mov	dptr,#OUT07VAL
   0632 E0                 1424 	movx	a,@dptr
   0633 D0 83              1425 	pop	dph0
   0635 D0 82              1426 	pop	dpl0
                           1427 ;;; xxxx
                           1428 	
   0637 F0                 1429 	movx	@dptr,a
   0638 A3                 1430 	inc	dptr
   0639 74 00              1431 	mov	a,#0
                           1432 
                           1433 ;;; xxxx
   063B C0 82              1434 	push	dpl0
   063D C0 83              1435 	push	dph0
   063F 90 7F C8           1436 	mov	dptr,#OUT2CS
   0642 E0                 1437 	movx	a,@dptr
   0643 D0 83              1438 	pop	dph0
   0645 D0 82              1439 	pop	dpl0
                           1440 ;;; xxxx
                           1441 	
   0647 F0                 1442 	movx	@dptr,a
   0648 A3                 1443 	inc	dptr
   0649 90 7F B5           1444 	mov	dptr,#IN0BC
   064C 74 04              1445 	mov	a,#4
   064E F0                 1446 	movx	@dptr,a
   064F 02 06 F6           1447 	ljmp	setupack
   0652                    1448 setupstallb1:
   0652 02 06 F2           1449 	ljmp	setupstall
   0655                    1450 cmdnotb1:
                           1451 	;; 0xb2
   0655 B4 B2 2B           1452 	cjne	a,#0xb2,cmdnotb2
   0658 90 7F E8           1453 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   065B E0                 1454 	movx	a,@dptr
   065C B4 C0 21           1455 	cjne	a,#0xc0,setupstallb2
   065F 90 7F 00           1456 	mov	dptr,#IN0BUF
   0662 E5 40              1457 	mov	a,errcode
   0664 F0                 1458 	movx	@dptr,a
   0665 A3                 1459 	inc	dptr
   0666 E5 41              1460 	mov	a,errval
   0668 F0                 1461 	movx	@dptr,a
   0669 A3                 1462 	inc	dptr
   066A E5 42              1463 	mov	a,cfgcount
   066C F0                 1464 	movx	@dptr,a
   066D A3                 1465 	inc	dptr
   066E E5 43              1466 	mov	a,cfgcount+1
   0670 F0                 1467 	movx	@dptr,a
   0671 A3                 1468 	inc	dptr
   0672 E5 45              1469 	mov	a,irqcount
   0674 F0                 1470 	movx	@dptr,a
   0675 90 7F B5           1471 	mov	dptr,#IN0BC
   0678 74 05              1472 	mov	a,#5
   067A F0                 1473 	movx	@dptr,a
   067B 05 45              1474 	inc	irqcount
   067D 02 06 F6           1475 	ljmp	setupack
   0680                    1476 setupstallb2:
   0680 02 06 F2           1477 	ljmp	setupstall
   0683                    1478 cmdnotb2:
                           1479 	;; 0xb3
   0683 B4 B3 2A           1480 	cjne	a,#0xb3,cmdnotb3
   0686 90 7F E8           1481 	mov	dptr,#SETUPDAT		; bRequestType == 0xc0
   0689 E0                 1482 	movx	a,@dptr
   068A B4 C0 20           1483 	cjne	a,#0xc0,setupstallb3
                           1484 	;; read EEPROM
                    0006   1485 	serstrlen = 6
   068D 75 F0 A0           1486 	mov	b,#0xa0		; EEPROM address
   0690 90 01 8B           1487 	mov	dptr,#eepromstraddr
   0693 7F 01              1488 	mov	r7,#1
   0695 12 01 8C           1489 	lcall	writei2c
   0698 70 13              1490 	jnz	setupstallb3
   069A 90 7F 00           1491 	mov	dptr,#IN0BUF
   069D 7F 06              1492 	mov	r7,#serstrlen
   069F 12 01 C7           1493 	lcall	readi2c
   06A2 70 09              1494 	jnz	setupstallb3
   06A4 74 06              1495 	mov	a,#serstrlen
   06A6 90 7F B5           1496 	mov	dptr,#IN0BC
   06A9 F0                 1497 	movx	@dptr,a
   06AA 02 06 F6           1498 	ljmp	setupack
   06AD                    1499 setupstallb3:
   06AD 02 06 F2           1500 	ljmp	setupstall
   06B0                    1501 cmdnotb3:
                           1502 	;; 0xb4
   06B0 B4 B4 3F           1503 	cjne	a,#0xb4,cmdnotb4
   06B3 90 7F EF           1504 	mov	dptr,#SETUPDAT+7
   06B6 E0                 1505 	movx	a,@dptr
   06B7 70 36              1506 	jnz	setupstallb4
   06B9 90 7F EE           1507 	mov	dptr,#SETUPDAT+6
   06BC E0                 1508 	movx	a,@dptr
   06BD F8                 1509 	mov	r0,a
   06BE FF                 1510 	mov	r7,a
   06BF 24 C0              1511 	add	a,#-64
   06C1 40 2C              1512 	jc	setupstallb4
   06C3 90 7F EC           1513 	mov	dptr,#SETUPDAT+4	; wIndex
   06C6 E0                 1514 	movx	a,@dptr
   06C7 F5 F0              1515 	mov	b,a
   06C9 F5 47              1516 	mov	ctrladdr,a
   06CB 90 7F E8           1517 	mov	dptr,#SETUPDAT
   06CE E0                 1518 	movx	a,@dptr
   06CF B4 40 0A           1519 	cjne	a,#0x40,0$		; bRequestType == 0x40
   06D2 75 46 02           1520 	mov	ctrlcode,#2
   06D5 90 7F C5           1521 	mov	dptr,#OUT0BC
   06D8 F0                 1522 	movx	@dptr,a
   06D9 02 06 FC           1523 	ljmp	endusbisr
   06DC B4 C0 10           1524 0$:	cjne	a,#0xc0,setupstallb4	; bRequestType == 0xc0
   06DF 90 7F 00           1525 	mov	dptr,#IN0BUF
   06E2 12 01 C7           1526 	lcall	readi2c
   06E5 70 08              1527 	jnz	setupstallb4
   06E7 E8                 1528 	mov	a,r0
   06E8 90 7F B5           1529 	mov	dptr,#IN0BC
   06EB F0                 1530 	movx	@dptr,a
   06EC 02 06 F6           1531 	ljmp	setupack
   06EF                    1532 setupstallb4:
   06EF 02 06 F2           1533 	ljmp	setupstall
   06F2                    1534 cmdnotb4:
                           1535 	;; unknown commands fall through to setupstall
                           1536 
   06F2                    1537 setupstall:
   06F2 74 03              1538 	mov	a,#3
   06F4 80 02              1539 	sjmp	endsetup
   06F6                    1540 setupack:
   06F6 74 02              1541 	mov	a,#2
   06F8                    1542 endsetup:
   06F8 90 7F B4           1543 	mov	dptr,#EP0CS
   06FB F0                 1544 	movx	@dptr,a
   06FC                    1545 endusbisr:
                           1546 	;; epilogue
   06FC D0 07              1547 	pop	ar7
   06FE D0 00              1548 	pop	ar0
   0700 D0 86              1549 	pop	dps
   0702 D0 D0              1550 	pop	psw
   0704 D0 85              1551 	pop	dph1
   0706 D0 84              1552 	pop	dpl1
   0708 D0 83              1553 	pop	dph0
   070A D0 82              1554 	pop	dpl0
   070C D0 F0              1555 	pop	b
   070E D0 E0              1556 	pop	acc
   0710 32                 1557 	reti
                           1558 
   0711                    1559 usb_sof_isr:
   0711 C0 E0              1560 	push	acc
   0713 C0 F0              1561 	push	b
   0715 C0 82              1562 	push	dpl0
   0717 C0 83              1563 	push	dph0
   0719 C0 D0              1564 	push	psw
   071B 75 D0 00           1565 	mov	psw,#0x00
   071E C0 86              1566 	push	dps
   0720 75 86 00           1567 	mov	dps,#0
                           1568 	;; clear interrupt
   0723 E5 91              1569 	mov	a,exif
   0725 C2 E4              1570 	clr	acc.4
   0727 F5 91              1571 	mov	exif,a
   0729 90 7F AB           1572 	mov	dptr,#USBIRQ
   072C 74 02              1573 	mov	a,#0x02
   072E F0                 1574 	movx	@dptr,a
                           1575 	;; handle interrupt
                           1576 	;; epilogue
   072F D0 86              1577 	pop	dps
   0731 D0 D0              1578 	pop	psw
   0733 D0 83              1579 	pop	dph0
   0735 D0 82              1580 	pop	dpl0
   0737 D0 F0              1581 	pop	b
   0739 D0 E0              1582 	pop	acc
   073B 32                 1583 	reti
                           1584 
                           1585 
   073C                    1586 usb_sutok_isr:
   073C C0 E0              1587 	push	acc
   073E C0 F0              1588 	push	b
   0740 C0 82              1589 	push	dpl0
   0742 C0 83              1590 	push	dph0
   0744 C0 D0              1591 	push	psw
   0746 75 D0 00           1592 	mov	psw,#0x00
   0749 C0 86              1593 	push	dps
   074B 75 86 00           1594 	mov	dps,#0
                           1595 	;; clear interrupt
   074E E5 91              1596 	mov	a,exif
   0750 C2 E4              1597 	clr	acc.4
   0752 F5 91              1598 	mov	exif,a
   0754 90 7F AB           1599 	mov	dptr,#USBIRQ
   0757 74 04              1600 	mov	a,#0x04
   0759 F0                 1601 	movx	@dptr,a
                           1602 	;; handle interrupt
                           1603 	;; epilogue
   075A D0 86              1604 	pop	dps
   075C D0 D0              1605 	pop	psw
   075E D0 83              1606 	pop	dph0
   0760 D0 82              1607 	pop	dpl0
   0762 D0 F0              1608 	pop	b
   0764 D0 E0              1609 	pop	acc
   0766 32                 1610 	reti
                           1611 
   0767                    1612 usb_suspend_isr:
   0767 C0 E0              1613 	push	acc
   0769 C0 F0              1614 	push	b
   076B C0 82              1615 	push	dpl0
   076D C0 83              1616 	push	dph0
   076F C0 D0              1617 	push	psw
   0771 75 D0 00           1618 	mov	psw,#0x00
   0774 C0 86              1619 	push	dps
   0776 75 86 00           1620 	mov	dps,#0
                           1621 	;; clear interrupt
   0779 E5 91              1622 	mov	a,exif
   077B C2 E4              1623 	clr	acc.4
   077D F5 91              1624 	mov	exif,a
   077F 90 7F AB           1625 	mov	dptr,#USBIRQ
   0782 74 08              1626 	mov	a,#0x08
   0784 F0                 1627 	movx	@dptr,a
                           1628 	;; handle interrupt
                           1629 	;; epilogue
   0785 D0 86              1630 	pop	dps
   0787 D0 D0              1631 	pop	psw
   0789 D0 83              1632 	pop	dph0
   078B D0 82              1633 	pop	dpl0
   078D D0 F0              1634 	pop	b
   078F D0 E0              1635 	pop	acc
   0791 32                 1636 	reti
                           1637 
   0792                    1638 usb_usbreset_isr:
   0792 C0 E0              1639 	push	acc
   0794 C0 F0              1640 	push	b
   0796 C0 82              1641 	push	dpl0
   0798 C0 83              1642 	push	dph0
   079A C0 D0              1643 	push	psw
   079C 75 D0 00           1644 	mov	psw,#0x00
   079F C0 86              1645 	push	dps
   07A1 75 86 00           1646 	mov	dps,#0
                           1647 	;; clear interrupt
   07A4 E5 91              1648 	mov	a,exif
   07A6 C2 E4              1649 	clr	acc.4
   07A8 F5 91              1650 	mov	exif,a
   07AA 90 7F AB           1651 	mov	dptr,#USBIRQ
   07AD 74 10              1652 	mov	a,#0x10
   07AF F0                 1653 	movx	@dptr,a
                           1654 	;; handle interrupt
                           1655 	;; epilogue
   07B0 D0 86              1656 	pop	dps
   07B2 D0 D0              1657 	pop	psw
   07B4 D0 83              1658 	pop	dph0
   07B6 D0 82              1659 	pop	dpl0
   07B8 D0 F0              1660 	pop	b
   07BA D0 E0              1661 	pop	acc
   07BC 32                 1662 	reti
                           1663 
   07BD                    1664 usb_ep0in_isr:
   07BD C0 E0              1665 	push	acc
   07BF C0 F0              1666 	push	b
   07C1 C0 82              1667 	push	dpl0
   07C3 C0 83              1668 	push	dph0
   07C5 C0 84              1669 	push	dpl1
   07C7 C0 85              1670 	push	dph1
   07C9 C0 D0              1671 	push	psw
   07CB 75 D0 00           1672 	mov	psw,#0x00
   07CE C0 86              1673 	push	dps
   07D0 75 86 00           1674 	mov	dps,#0
   07D3 C0 00              1675 	push	ar0
   07D5 C0 07              1676 	push	ar7
                           1677 	;; clear interrupt
   07D7 E5 91              1678 	mov	a,exif
   07D9 C2 E4              1679 	clr	acc.4
   07DB F5 91              1680 	mov	exif,a
   07DD 90 7F A9           1681 	mov	dptr,#IN07IRQ
   07E0 74 01              1682 	mov	a,#0x01
   07E2 F0                 1683 	movx	@dptr,a
                           1684 	;; handle interrupt
   07E3 E5 46              1685 	mov	a,ctrlcode
   07E5 B4 02 05           1686 	cjne	a,#2,0$
   07E8 12 03 E1           1687 	lcall	xmemread
   07EB 80 11              1688 	sjmp	ep0inendisr
   07ED 90 7F B5           1689 0$:	mov	dptr,#IN0BC
   07F0 E4                 1690 	clr	a
   07F1 F0                 1691 	movx	@dptr,a
   07F2 80 04              1692 	sjmp	ep0inack
                           1693 
   07F4                    1694 ep0install:
   07F4 74 03              1695 	mov	a,#3
   07F6 80 02              1696 	sjmp	ep0incs
   07F8                    1697 ep0inack:
   07F8 74 02              1698 	mov	a,#2
   07FA                    1699 ep0incs:
   07FA 90 7F B4           1700 	mov	dptr,#EP0CS
   07FD F0                 1701 	movx	@dptr,a
   07FE                    1702 ep0inendisr:
                           1703 	;; epilogue
   07FE D0 07              1704 	pop	ar7
   0800 D0 00              1705 	pop	ar0
   0802 D0 86              1706 	pop	dps
   0804 D0 D0              1707 	pop	psw
   0806 D0 85              1708 	pop	dph1
   0808 D0 84              1709 	pop	dpl1
   080A D0 83              1710 	pop	dph0
   080C D0 82              1711 	pop	dpl0
   080E D0 F0              1712 	pop	b
   0810 D0 E0              1713 	pop	acc
   0812 32                 1714 	reti
                           1715 
   0813                    1716 usb_ep0out_isr:
   0813 C0 E0              1717 	push	acc
   0815 C0 F0              1718 	push	b
   0817 C0 82              1719 	push	dpl0
   0819 C0 83              1720 	push	dph0
   081B C0 84              1721 	push	dpl1
   081D C0 85              1722 	push	dph1
   081F C0 D0              1723 	push	psw
   0821 75 D0 00           1724 	mov	psw,#0x00
   0824 C0 86              1725 	push	dps
   0826 75 86 00           1726 	mov	dps,#0
   0829 C0 00              1727 	push	ar0
   082B C0 06              1728 	push	ar6
                           1729 	;; clear interrupt
   082D E5 91              1730 	mov	a,exif
   082F C2 E4              1731 	clr	acc.4
   0831 F5 91              1732 	mov	exif,a
   0833 90 7F AA           1733 	mov	dptr,#OUT07IRQ
   0836 74 01              1734 	mov	a,#0x01
   0838 F0                 1735 	movx	@dptr,a
                           1736 	;; handle interrupt
   0839 E5 46              1737 	mov	a,ctrlcode		; check control code
   083B B4 01 36           1738 	cjne	a,#0x01,i2cwr
                           1739 	;; write to external memory
   083E 90 7F C5           1740 	mov	dptr,#OUT0BC
   0841 E0                 1741 	movx	a,@dptr
   0842 60 28              1742 	jz	0$
   0844 FF                 1743 	mov	r7,a
   0845 C3                 1744 	clr	c
   0846 E5 49              1745 	mov	a,ctrllen
   0848 9F                 1746 	subb	a,r7
   0849 F5 49              1747 	mov	ctrllen,a
   084B E5 4A              1748 	mov	a,ctrllen+1
   084D 94 00              1749 	subb	a,#0
   084F F5 4A              1750 	mov	ctrllen+1,a
   0851 40 38              1751 	jc	ep0outstall
   0853 90 7E C0           1752 	mov	dptr,#OUT0BUF
   0856 85 47 84           1753 	mov	dpl1,ctrladdr
   0859 85 48 85           1754 	mov	dph1,ctrladdr+1
   085C E0                 1755 1$:	movx	a,@dptr
   085D A3                 1756 	inc	dptr
   085E 05 86              1757 	inc	dps
   0860 F0                 1758 	movx	@dptr,a
   0861 A3                 1759 	inc	dptr
   0862 15 86              1760 	dec	dps
   0864 DF F6              1761 	djnz	r7,1$
   0866 85 84 47           1762 	mov	ctrladdr,dpl1
   0869 85 85 48           1763 	mov	ctrladdr+1,dph1
   086C E5 49              1764 0$:	mov	a,ctrllen
   086E 45 4A              1765 	orl	a,ctrllen+1
   0870 60 20              1766 	jz	ep0outack
   0872 80 27              1767 	sjmp	ep0outendisr
                           1768 
                           1769 	;; write I2C eeprom
   0874 B4 02 14           1770 i2cwr:	cjne	a,#0x02,ep0outstall
   0877 90 7F C5           1771 	mov	dptr,#OUT0BC
   087A E0                 1772 	movx	a,@dptr
   087B 60 15              1773 	jz	ep0outack
   087D FF                 1774 	mov	r7,a
   087E 85 47 F0           1775 	mov	b,ctrladdr
   0881 90 7E C0           1776 	mov	dptr,#OUT0BUF
   0884 12 01 8C           1777 	lcall	writei2c
   0887 70 02              1778 	jnz	ep0outstall
   0889 80 07              1779 	sjmp	ep0outack
                           1780 
   088B                    1781 ep0outstall:
   088B 75 46 00           1782 	mov	ctrlcode,#0
   088E 74 03              1783 	mov	a,#3
   0890 80 05              1784 	sjmp	ep0outcs
   0892                    1785 ep0outack:
   0892 75 46 00           1786 	mov	ctrlcode,#0
   0895 74 02              1787 	mov	a,#2
   0897                    1788 ep0outcs:
   0897 90 7F B4           1789 	mov	dptr,#EP0CS
   089A F0                 1790 	movx	@dptr,a
   089B                    1791 ep0outendisr:
                           1792 	;; epilogue
   089B D0 06              1793 	pop	ar6
   089D D0 00              1794 	pop	ar0
   089F D0 86              1795 	pop	dps
   08A1 D0 D0              1796 	pop	psw
   08A3 D0 85              1797 	pop	dph1
   08A5 D0 84              1798 	pop	dpl1
   08A7 D0 83              1799 	pop	dph0
   08A9 D0 82              1800 	pop	dpl0
   08AB D0 F0              1801 	pop	b
   08AD D0 E0              1802 	pop	acc
   08AF 32                 1803 	reti
                           1804 
   08B0                    1805 fillusbintr::
   08B0 90 7E 80           1806 	mov	dptr,#IN1BUF
   08B3 E5 40              1807 	mov	a,errcode
   08B5 F0                 1808 	movx	@dptr,a
   08B6 A3                 1809 	inc	dptr
   08B7 E5 41              1810 	mov	a,errval
   08B9 F0                 1811 	movx	@dptr,a
   08BA A3                 1812 	inc	dptr
   08BB E5 42              1813 	mov	a,cfgcount
   08BD F0                 1814 	movx	@dptr,a
   08BE A3                 1815 	inc	dptr
   08BF E5 43              1816 	mov	a,cfgcount+1
   08C1 F0                 1817 	movx	@dptr,a
   08C2 A3                 1818 	inc	dptr
   08C3 E5 45              1819 	mov	a,irqcount
   08C5 F0                 1820 	movx	@dptr,a
   08C6 90 7F B7           1821 	mov	dptr,#IN1BC
   08C9 74 05              1822 	mov	a,#5
   08CB F0                 1823 	movx	@dptr,a
   08CC 05 45              1824 	inc	irqcount
   08CE 22                 1825 	ret
                           1826 
   08CF                    1827 usb_ep1in_isr:
   08CF C0 E0              1828 	push	acc
   08D1 C0 F0              1829 	push	b
   08D3 C0 82              1830 	push	dpl0
   08D5 C0 83              1831 	push	dph0
   08D7 C0 D0              1832 	push	psw
   08D9 75 D0 00           1833 	mov	psw,#0x00
   08DC C0 86              1834 	push	dps
   08DE 75 86 00           1835 	mov	dps,#0
                           1836 	;; clear interrupt
   08E1 E5 91              1837 	mov	a,exif
   08E3 C2 E4              1838 	clr	acc.4
   08E5 F5 91              1839 	mov	exif,a
   08E7 90 7F A9           1840 	mov	dptr,#IN07IRQ
   08EA 74 02              1841 	mov	a,#0x02
   08EC F0                 1842 	movx	@dptr,a
                           1843 	;; handle interrupt
   08ED 12 08 B0           1844 	lcall	fillusbintr
                           1845 	;; epilogue
   08F0 D0 86              1846 	pop	dps
   08F2 D0 D0              1847 	pop	psw
   08F4 D0 83              1848 	pop	dph0
   08F6 D0 82              1849 	pop	dpl0
   08F8 D0 F0              1850 	pop	b
   08FA D0 E0              1851 	pop	acc
   08FC 32                 1852 	reti
                           1853 
   08FD                    1854 usb_ep1out_isr:
   08FD C0 E0              1855 	push	acc
   08FF C0 F0              1856 	push	b
   0901 C0 82              1857 	push	dpl0
   0903 C0 83              1858 	push	dph0
   0905 C0 D0              1859 	push	psw
   0907 75 D0 00           1860 	mov	psw,#0x00
   090A C0 86              1861 	push	dps
   090C 75 86 00           1862 	mov	dps,#0
                           1863 	;; clear interrupt
   090F E5 91              1864 	mov	a,exif
   0911 C2 E4              1865 	clr	acc.4
   0913 F5 91              1866 	mov	exif,a
   0915 90 7F AA           1867 	mov	dptr,#OUT07IRQ
   0918 74 02              1868 	mov	a,#0x02
   091A F0                 1869 	movx	@dptr,a
                           1870 	;; handle interrupt
                           1871 	;; epilogue
   091B D0 86              1872 	pop	dps
   091D D0 D0              1873 	pop	psw
   091F D0 83              1874 	pop	dph0
   0921 D0 82              1875 	pop	dpl0
   0923 D0 F0              1876 	pop	b
   0925 D0 E0              1877 	pop	acc
   0927 32                 1878 	reti
                           1879 
   0928                    1880 usb_ep2in_isr:
   0928 C0 E0              1881 	push	acc
   092A C0 F0              1882 	push	b
   092C C0 82              1883 	push	dpl0
   092E C0 83              1884 	push	dph0
   0930 C0 D0              1885 	push	psw
   0932 75 D0 00           1886 	mov	psw,#0x00
   0935 C0 86              1887 	push	dps
   0937 75 86 00           1888 	mov	dps,#0
                           1889 	;; clear interrupt
   093A E5 91              1890 	mov	a,exif
   093C C2 E4              1891 	clr	acc.4
   093E F5 91              1892 	mov	exif,a
   0940 90 7F A9           1893 	mov	dptr,#IN07IRQ
   0943 74 04              1894 	mov	a,#0x04
   0945 F0                 1895 	movx	@dptr,a
                           1896 	;; handle interrupt
                           1897 	;; epilogue
   0946 D0 86              1898 	pop	dps
   0948 D0 D0              1899 	pop	psw
   094A D0 83              1900 	pop	dph0
   094C D0 82              1901 	pop	dpl0
   094E D0 F0              1902 	pop	b
   0950 D0 E0              1903 	pop	acc
   0952 32                 1904 	reti
                           1905 
   0953                    1906 usb_ep2out_isr:
   0953 C0 E0              1907 	push	acc
   0955 C0 F0              1908 	push	b
   0957 C0 82              1909 	push	dpl0
   0959 C0 83              1910 	push	dph0
   095B C0 D0              1911 	push	psw
   095D 75 D0 00           1912 	mov	psw,#0x00
   0960 C0 86              1913 	push	dps
   0962 75 86 00           1914 	mov	dps,#0
                           1915 	;; clear interrupt
   0965 E5 91              1916 	mov	a,exif
   0967 C2 E4              1917 	clr	acc.4
   0969 F5 91              1918 	mov	exif,a
   096B 90 7F AA           1919 	mov	dptr,#OUT07IRQ
   096E 74 04              1920 	mov	a,#0x04
   0970 F0                 1921 	movx	@dptr,a
                           1922 	;; handle interrupt
                           1923 	;; epilogue
   0971 D0 86              1924 	pop	dps
   0973 D0 D0              1925 	pop	psw
   0975 D0 83              1926 	pop	dph0
   0977 D0 82              1927 	pop	dpl0
   0979 D0 F0              1928 	pop	b
   097B D0 E0              1929 	pop	acc
   097D 32                 1930 	reti
                           1931 
   097E                    1932 usb_ep3in_isr:
   097E C0 E0              1933 	push	acc
   0980 C0 F0              1934 	push	b
   0982 C0 82              1935 	push	dpl0
   0984 C0 83              1936 	push	dph0
   0986 C0 D0              1937 	push	psw
   0988 75 D0 00           1938 	mov	psw,#0x00
   098B C0 86              1939 	push	dps
   098D 75 86 00           1940 	mov	dps,#0
                           1941 	;; clear interrupt
   0990 E5 91              1942 	mov	a,exif
   0992 C2 E4              1943 	clr	acc.4
   0994 F5 91              1944 	mov	exif,a
   0996 90 7F A9           1945 	mov	dptr,#IN07IRQ
   0999 74 08              1946 	mov	a,#0x08
   099B F0                 1947 	movx	@dptr,a
                           1948 	;; handle interrupt
                           1949 	;; epilogue
   099C D0 86              1950 	pop	dps
   099E D0 D0              1951 	pop	psw
   09A0 D0 83              1952 	pop	dph0
   09A2 D0 82              1953 	pop	dpl0
   09A4 D0 F0              1954 	pop	b
   09A6 D0 E0              1955 	pop	acc
   09A8 32                 1956 	reti
                           1957 
   09A9                    1958 usb_ep3out_isr:
   09A9 C0 E0              1959 	push	acc
   09AB C0 F0              1960 	push	b
   09AD C0 82              1961 	push	dpl0
   09AF C0 83              1962 	push	dph0
   09B1 C0 D0              1963 	push	psw
   09B3 75 D0 00           1964 	mov	psw,#0x00
   09B6 C0 86              1965 	push	dps
   09B8 75 86 00           1966 	mov	dps,#0
                           1967 	;; clear interrupt
   09BB E5 91              1968 	mov	a,exif
   09BD C2 E4              1969 	clr	acc.4
   09BF F5 91              1970 	mov	exif,a
   09C1 90 7F AA           1971 	mov	dptr,#OUT07IRQ
   09C4 74 08              1972 	mov	a,#0x08
   09C6 F0                 1973 	movx	@dptr,a
                           1974 	;; handle interrupt
                           1975 	;; epilogue
   09C7 D0 86              1976 	pop	dps
   09C9 D0 D0              1977 	pop	psw
   09CB D0 83              1978 	pop	dph0
   09CD D0 82              1979 	pop	dpl0
   09CF D0 F0              1980 	pop	b
   09D1 D0 E0              1981 	pop	acc
   09D3 32                 1982 	reti
                           1983 
   09D4                    1984 usb_ep4in_isr:
   09D4 C0 E0              1985 	push	acc
   09D6 C0 F0              1986 	push	b
   09D8 C0 82              1987 	push	dpl0
   09DA C0 83              1988 	push	dph0
   09DC C0 D0              1989 	push	psw
   09DE 75 D0 00           1990 	mov	psw,#0x00
   09E1 C0 86              1991 	push	dps
   09E3 75 86 00           1992 	mov	dps,#0
                           1993 	;; clear interrupt
   09E6 E5 91              1994 	mov	a,exif
   09E8 C2 E4              1995 	clr	acc.4
   09EA F5 91              1996 	mov	exif,a
   09EC 90 7F A9           1997 	mov	dptr,#IN07IRQ
   09EF 74 10              1998 	mov	a,#0x10
   09F1 F0                 1999 	movx	@dptr,a
                           2000 	;; handle interrupt
                           2001 	;; epilogue
   09F2 D0 86              2002 	pop	dps
   09F4 D0 D0              2003 	pop	psw
   09F6 D0 83              2004 	pop	dph0
   09F8 D0 82              2005 	pop	dpl0
   09FA D0 F0              2006 	pop	b
   09FC D0 E0              2007 	pop	acc
   09FE 32                 2008 	reti
                           2009 
   09FF                    2010 usb_ep4out_isr:
   09FF C0 E0              2011 	push	acc
   0A01 C0 F0              2012 	push	b
   0A03 C0 82              2013 	push	dpl0
   0A05 C0 83              2014 	push	dph0
   0A07 C0 D0              2015 	push	psw
   0A09 75 D0 00           2016 	mov	psw,#0x00
   0A0C C0 86              2017 	push	dps
   0A0E 75 86 00           2018 	mov	dps,#0
                           2019 	;; clear interrupt
   0A11 E5 91              2020 	mov	a,exif
   0A13 C2 E4              2021 	clr	acc.4
   0A15 F5 91              2022 	mov	exif,a
   0A17 90 7F AA           2023 	mov	dptr,#OUT07IRQ
   0A1A 74 10              2024 	mov	a,#0x10
   0A1C F0                 2025 	movx	@dptr,a
                           2026 	;; handle interrupt
                           2027 	;; epilogue
   0A1D D0 86              2028 	pop	dps
   0A1F D0 D0              2029 	pop	psw
   0A21 D0 83              2030 	pop	dph0
   0A23 D0 82              2031 	pop	dpl0
   0A25 D0 F0              2032 	pop	b
   0A27 D0 E0              2033 	pop	acc
   0A29 32                 2034 	reti
                           2035 
   0A2A                    2036 usb_ep5in_isr:
   0A2A C0 E0              2037 	push	acc
   0A2C C0 F0              2038 	push	b
   0A2E C0 82              2039 	push	dpl0
   0A30 C0 83              2040 	push	dph0
   0A32 C0 D0              2041 	push	psw
   0A34 75 D0 00           2042 	mov	psw,#0x00
   0A37 C0 86              2043 	push	dps
   0A39 75 86 00           2044 	mov	dps,#0
                           2045 	;; clear interrupt
   0A3C E5 91              2046 	mov	a,exif
   0A3E C2 E4              2047 	clr	acc.4
   0A40 F5 91              2048 	mov	exif,a
   0A42 90 7F A9           2049 	mov	dptr,#IN07IRQ
   0A45 74 20              2050 	mov	a,#0x20
   0A47 F0                 2051 	movx	@dptr,a
                           2052 	;; handle interrupt
                           2053 	;; epilogue
   0A48 D0 86              2054 	pop	dps
   0A4A D0 D0              2055 	pop	psw
   0A4C D0 83              2056 	pop	dph0
   0A4E D0 82              2057 	pop	dpl0
   0A50 D0 F0              2058 	pop	b
   0A52 D0 E0              2059 	pop	acc
   0A54 32                 2060 	reti
                           2061 
   0A55                    2062 usb_ep5out_isr:
   0A55 C0 E0              2063 	push	acc
   0A57 C0 F0              2064 	push	b
   0A59 C0 82              2065 	push	dpl0
   0A5B C0 83              2066 	push	dph0
   0A5D C0 D0              2067 	push	psw
   0A5F 75 D0 00           2068 	mov	psw,#0x00
   0A62 C0 86              2069 	push	dps
   0A64 75 86 00           2070 	mov	dps,#0
                           2071 	;; clear interrupt
   0A67 E5 91              2072 	mov	a,exif
   0A69 C2 E4              2073 	clr	acc.4
   0A6B F5 91              2074 	mov	exif,a
   0A6D 90 7F AA           2075 	mov	dptr,#OUT07IRQ
   0A70 74 20              2076 	mov	a,#0x20
   0A72 F0                 2077 	movx	@dptr,a
                           2078 	;; handle interrupt
                           2079 	;; epilogue
   0A73 D0 86              2080 	pop	dps
   0A75 D0 D0              2081 	pop	psw
   0A77 D0 83              2082 	pop	dph0
   0A79 D0 82              2083 	pop	dpl0
   0A7B D0 F0              2084 	pop	b
   0A7D D0 E0              2085 	pop	acc
   0A7F 32                 2086 	reti
                           2087 
   0A80                    2088 usb_ep6in_isr:
   0A80 C0 E0              2089 	push	acc
   0A82 C0 F0              2090 	push	b
   0A84 C0 82              2091 	push	dpl0
   0A86 C0 83              2092 	push	dph0
   0A88 C0 D0              2093 	push	psw
   0A8A 75 D0 00           2094 	mov	psw,#0x00
   0A8D C0 86              2095 	push	dps
   0A8F 75 86 00           2096 	mov	dps,#0
                           2097 	;; clear interrupt
   0A92 E5 91              2098 	mov	a,exif
   0A94 C2 E4              2099 	clr	acc.4
   0A96 F5 91              2100 	mov	exif,a
   0A98 90 7F A9           2101 	mov	dptr,#IN07IRQ
   0A9B 74 40              2102 	mov	a,#0x40
   0A9D F0                 2103 	movx	@dptr,a
                           2104 	;; handle interrupt
                           2105 	;; epilogue
   0A9E D0 86              2106 	pop	dps
   0AA0 D0 D0              2107 	pop	psw
   0AA2 D0 83              2108 	pop	dph0
   0AA4 D0 82              2109 	pop	dpl0
   0AA6 D0 F0              2110 	pop	b
   0AA8 D0 E0              2111 	pop	acc
   0AAA 32                 2112 	reti
                           2113 
   0AAB                    2114 usb_ep6out_isr:
   0AAB C0 E0              2115 	push	acc
   0AAD C0 F0              2116 	push	b
   0AAF C0 82              2117 	push	dpl0
   0AB1 C0 83              2118 	push	dph0
   0AB3 C0 D0              2119 	push	psw
   0AB5 75 D0 00           2120 	mov	psw,#0x00
   0AB8 C0 86              2121 	push	dps
   0ABA 75 86 00           2122 	mov	dps,#0
                           2123 	;; clear interrupt
   0ABD E5 91              2124 	mov	a,exif
   0ABF C2 E4              2125 	clr	acc.4
   0AC1 F5 91              2126 	mov	exif,a
   0AC3 90 7F AA           2127 	mov	dptr,#OUT07IRQ
   0AC6 74 40              2128 	mov	a,#0x40
   0AC8 F0                 2129 	movx	@dptr,a
                           2130 	;; handle interrupt
                           2131 	;; epilogue
   0AC9 D0 86              2132 	pop	dps
   0ACB D0 D0              2133 	pop	psw
   0ACD D0 83              2134 	pop	dph0
   0ACF D0 82              2135 	pop	dpl0
   0AD1 D0 F0              2136 	pop	b
   0AD3 D0 E0              2137 	pop	acc
   0AD5 32                 2138 	reti
                           2139 
   0AD6                    2140 usb_ep7in_isr:
   0AD6 C0 E0              2141 	push	acc
   0AD8 C0 F0              2142 	push	b
   0ADA C0 82              2143 	push	dpl0
   0ADC C0 83              2144 	push	dph0
   0ADE C0 D0              2145 	push	psw
   0AE0 75 D0 00           2146 	mov	psw,#0x00
   0AE3 C0 86              2147 	push	dps
   0AE5 75 86 00           2148 	mov	dps,#0
                           2149 	;; clear interrupt
   0AE8 E5 91              2150 	mov	a,exif
   0AEA C2 E4              2151 	clr	acc.4
   0AEC F5 91              2152 	mov	exif,a
   0AEE 90 7F A9           2153 	mov	dptr,#IN07IRQ
   0AF1 74 80              2154 	mov	a,#0x80
   0AF3 F0                 2155 	movx	@dptr,a
                           2156 	;; handle interrupt
                           2157 	;; epilogue
   0AF4 D0 86              2158 	pop	dps
   0AF6 D0 D0              2159 	pop	psw
   0AF8 D0 83              2160 	pop	dph0
   0AFA D0 82              2161 	pop	dpl0
   0AFC D0 F0              2162 	pop	b
   0AFE D0 E0              2163 	pop	acc
   0B00 32                 2164 	reti
                           2165 
   0B01                    2166 usb_ep7out_isr:
   0B01 C0 E0              2167 	push	acc
   0B03 C0 F0              2168 	push	b
   0B05 C0 82              2169 	push	dpl0
   0B07 C0 83              2170 	push	dph0
   0B09 C0 D0              2171 	push	psw
   0B0B 75 D0 00           2172 	mov	psw,#0x00
   0B0E C0 86              2173 	push	dps
   0B10 75 86 00           2174 	mov	dps,#0
                           2175 	;; clear interrupt
   0B13 E5 91              2176 	mov	a,exif
   0B15 C2 E4              2177 	clr	acc.4
   0B17 F5 91              2178 	mov	exif,a
   0B19 90 7F AA           2179 	mov	dptr,#OUT07IRQ
   0B1C 74 80              2180 	mov	a,#0x80
   0B1E F0                 2181 	movx	@dptr,a
                           2182 	;; handle interrupt
                           2183 	;; epilogue
   0B1F D0 86              2184 	pop	dps
   0B21 D0 D0              2185 	pop	psw
   0B23 D0 83              2186 	pop	dph0
   0B25 D0 82              2187 	pop	dpl0
   0B27 D0 F0              2188 	pop	b
   0B29 D0 E0              2189 	pop	acc
   0B2B 32                 2190 	reti
                           2191 	
                           2192 	;; -----------------------------------------------------
                           2193 	;; USB descriptors
                           2194 	;; -----------------------------------------------------
                           2195 
                           2196 	;; Device and/or Interface Class codes
                    0000   2197 	USB_CLASS_PER_INTERFACE         = 0
                    0001   2198 	USB_CLASS_AUDIO                 = 1
                    0002   2199 	USB_CLASS_COMM                  = 2
                    0003   2200 	USB_CLASS_HID                   = 3
                    0007   2201 	USB_CLASS_PRINTER               = 7
                    0008   2202 	USB_CLASS_MASS_STORAGE          = 8
                    0009   2203 	USB_CLASS_HUB                   = 9
                    00FF   2204 	USB_CLASS_VENDOR_SPEC           = 0xff
                           2205 
                           2206 	;; Descriptor types
                    0001   2207 	USB_DT_DEVICE                   = 0x01
                    0002   2208 	USB_DT_CONFIG                   = 0x02
                    0003   2209 	USB_DT_STRING                   = 0x03
                    0004   2210 	USB_DT_INTERFACE                = 0x04
                    0005   2211 	USB_DT_ENDPOINT                 = 0x05
                           2212 
                           2213 	;; Standard requests
                    0000   2214 	USB_REQ_GET_STATUS              = 0x00
                    0001   2215 	USB_REQ_CLEAR_FEATURE           = 0x01
                    0003   2216 	USB_REQ_SET_FEATURE             = 0x03
                    0005   2217 	USB_REQ_SET_ADDRESS             = 0x05
                    0006   2218 	USB_REQ_GET_DESCRIPTOR          = 0x06
                    0007   2219 	USB_REQ_SET_DESCRIPTOR          = 0x07
                    0008   2220 	USB_REQ_GET_CONFIGURATION       = 0x08
                    0009   2221 	USB_REQ_SET_CONFIGURATION       = 0x09
                    000A   2222 	USB_REQ_GET_INTERFACE           = 0x0A
                    000B   2223 	USB_REQ_SET_INTERFACE           = 0x0B
                    000C   2224 	USB_REQ_SYNCH_FRAME             = 0x0C
                           2225 
                           2226 	;; USB Request Type and Endpoint Directions
                    0000   2227 	USB_DIR_OUT                     = 0
                    0080   2228 	USB_DIR_IN                      = 0x80
                           2229 
                    0000   2230 	USB_TYPE_STANDARD               = (0x00 << 5)
                    0020   2231 	USB_TYPE_CLASS                  = (0x01 << 5)
                    0040   2232 	USB_TYPE_VENDOR                 = (0x02 << 5)
                    0060   2233 	USB_TYPE_RESERVED               = (0x03 << 5)
                           2234 
                    0000   2235 	USB_RECIP_DEVICE                = 0x00
                    0001   2236 	USB_RECIP_INTERFACE             = 0x01
                    0002   2237 	USB_RECIP_ENDPOINT              = 0x02
                    0003   2238 	USB_RECIP_OTHER                 = 0x03
                           2239 
                           2240 	;; Request target types.
                    0000   2241 	USB_RT_DEVICE                   = 0x00
                    0001   2242 	USB_RT_INTERFACE                = 0x01
                    0002   2243 	USB_RT_ENDPOINT                 = 0x02
                           2244 
                    BAC0   2245 	VENDID	= 0xbac0
                    6135   2246 	PRODID	= 0x6135
                           2247 
   0B2C                    2248 devicedescr:
   0B2C 12                 2249 	.db	18			; bLength
   0B2D 01                 2250 	.db	USB_DT_DEVICE		; bDescriptorType
   0B2E 00 01              2251 	.db	0x00, 0x01		; bcdUSB
   0B30 FF                 2252 	.db	USB_CLASS_VENDOR_SPEC	; bDeviceClass
   0B31 00                 2253 	.db	0			; bDeviceSubClass
   0B32 FF                 2254 	.db	0xff			; bDeviceProtocol
   0B33 40                 2255 	.db	0x40			; bMaxPacketSize0
   0B34 C0 BA              2256 	.db	<VENDID,>VENDID		; idVendor
   0B36 35 61              2257 	.db	<PRODID,>PRODID		; idProduct
   0B38 02 00              2258 	.db	0x02,0x00		; bcdDevice
   0B3A 01                 2259 	.db	1			; iManufacturer
   0B3B 02                 2260 	.db	2			; iProduct
   0B3C 03                 2261 	.db	3			; iSerialNumber
   0B3D 01                 2262 	.db	1			; bNumConfigurations
                           2263 
   0B3E                    2264 config0descr:
   0B3E 09                 2265 	.db	9			; bLength
   0B3F 02                 2266 	.db	USB_DT_CONFIG		; bDescriptorType
   0B40 45 00              2267 	.db	<config0sz,>config0sz	; wTotalLength
   0B42 01                 2268 	.db	1			; bNumInterfaces
   0B43 01                 2269 	.db	1			; bConfigurationValue
   0B44 00                 2270 	.db	0			; iConfiguration
   0B45 40                 2271 	.db	0b01000000		; bmAttributs (self powered)
   0B46 00                 2272 	.db	0			; MaxPower (mA/2) (self powered so 0)
                           2273 	;; interface descriptor I0:A0
   0B47 09                 2274 	.db	9			; bLength
   0B48 04                 2275 	.db	USB_DT_INTERFACE	; bDescriptorType
   0B49 00                 2276 	.db	0			; bInterfaceNumber
   0B4A 00                 2277 	.db	0			; bAlternateSetting
   0B4B 03                 2278 	.db	3			; bNumEndpoints
   0B4C FF                 2279 	.db	0xff			; bInterfaceClass (vendor specific)
   0B4D 00                 2280 	.db	0x00			; bInterfaceSubClass
   0B4E FF                 2281 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0B4F 00                 2282 	.db	0			; iInterface
                           2283 	;; endpoint descriptor I0:A0:E0
   0B50 07                 2284 	.db	7			; bLength
   0B51 05                 2285 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B52 81                 2286 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0B53 02                 2287 	.db	0x02			; bmAttributes (bulk)
   0B54 40 00              2288 	.db	0x40,0x00		; wMaxPacketSize
   0B56 00                 2289 	.db	0			; bInterval
                           2290 	;; endpoint descriptor I0:A0:E1
   0B57 07                 2291 	.db	7			; bLength
   0B58 05                 2292 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B59 82                 2293 	.db	(USB_DIR_IN | 2)	; bEndpointAddress
   0B5A 02                 2294 	.db	0x02			; bmAttributes (bulk)
   0B5B 40 00              2295 	.db	0x40,0x00		; wMaxPacketSize
   0B5D 00                 2296 	.db	0			; bInterval
                           2297 	;; endpoint descriptor I0:A0:E2
   0B5E 07                 2298 	.db	7			; bLength
   0B5F 05                 2299 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B60 02                 2300 	.db	(USB_DIR_OUT | 2)	; bEndpointAddress
   0B61 02                 2301 	.db	0x02			; bmAttributes (bulk)
   0B62 40 00              2302 	.db	0x40,0x00		; wMaxPacketSize
   0B64 00                 2303 	.db	0			; bInterval
                           2304 	;; interface descriptor I0:A1
   0B65 09                 2305 	.db	9			; bLength
   0B66 04                 2306 	.db	USB_DT_INTERFACE	; bDescriptorType
   0B67 00                 2307 	.db	0			; bInterfaceNumber
   0B68 01                 2308 	.db	1			; bAlternateSetting
   0B69 03                 2309 	.db	3			; bNumEndpoints
   0B6A FF                 2310 	.db	0xff			; bInterfaceClass (vendor specific)
   0B6B 00                 2311 	.db	0x00			; bInterfaceSubClass
   0B6C FF                 2312 	.db	0xff			; bInterfaceProtocol (vendor specific)
   0B6D 00                 2313 	.db	0			; iInterface
                           2314 	;; endpoint descriptor I0:A1:E0
   0B6E 07                 2315 	.db	7			; bLength
   0B6F 05                 2316 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B70 81                 2317 	.db	(USB_DIR_IN | 1)	; bEndpointAddress
   0B71 03                 2318 	.db	0x03			; bmAttributes (interrupt)
   0B72 40 00              2319 	.db	0x40,0x00		; wMaxPacketSize
   0B74 0A                 2320 	.db	10			; bInterval
                           2321 	;; endpoint descriptor I0:A1:E1
   0B75 07                 2322 	.db	7			; bLength
   0B76 05                 2323 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B77 82                 2324 	.db	(USB_DIR_IN | 2)	; bEndpointAddress
   0B78 02                 2325 	.db	0x02			; bmAttributes (bulk)
   0B79 40 00              2326 	.db	0x40,0x00		; wMaxPacketSize
   0B7B 00                 2327 	.db	0			; bInterval
                           2328 	;; endpoint descriptor I0:A1:E2
   0B7C 07                 2329 	.db	7			; bLength
   0B7D 05                 2330 	.db	USB_DT_ENDPOINT		; bDescriptorType
   0B7E 02                 2331 	.db	(USB_DIR_OUT | 2)	; bEndpointAddress
   0B7F 02                 2332 	.db	0x02			; bmAttributes (bulk)
   0B80 40 00              2333 	.db	0x40,0x00		; wMaxPacketSize
   0B82 00                 2334 	.db	0			; bInterval
                           2335 
                    0045   2336 config0sz = . - config0descr
                           2337 
   0B83                    2338 stringdescr:
   0B83 8B 0B              2339 	.db	<string0,>string0
   0B85 8F 0B              2340 	.db	<string1,>string1
   0B87 9D 0B              2341 	.db	<string2,>string2
   0B89 BD 0B              2342 	.db	<stringserial,>stringserial
                           2343 
                    0004   2344 numstrings = (. - stringdescr)/2
                           2345 
   0B8B                    2346 string0:
   0B8B 04                 2347 	.db	string0sz		; bLength
   0B8C 03                 2348 	.db	USB_DT_STRING		; bDescriptorType
   0B8D 00 00              2349 	.db	0,0			; LANGID[0]: Lang Neutral
                    0004   2350 string0sz = . - string0
                           2351 
   0B8F                    2352 string1:
   0B8F 0E                 2353 	.db	string1sz		; bLength
   0B90 03                 2354 	.db	USB_DT_STRING		; bDescriptorType
   0B91 42 00 61 00 79 00  2355 	.db	'B,0,'a,0,'y,0,'c,0,'o,0,'m,0
        63 00 6F 00 6D 00
                    000E   2356 string1sz = . - string1
                           2357 
   0B9D                    2358 string2:
   0B9D 20                 2359 	.db	string2sz		; bLength
   0B9E 03                 2360 	.db	USB_DT_STRING		; bDescriptorType
   0B9F 55 00 53 00 42 00  2361 	.db	'U,0,'S,0,'B,0,'F,0,'L,0,'E,0,'X,0,' ,0
        46 00 4C 00 45 00
        58 00 20 00
   0BAF 28 00 62 00 6C 00  2362 	.db	'(,0,'b,0,'l,0,'a,0,'n,0,'k,0,'),0
        61 00 6E 00 6B 00
        29 00
                    0020   2363 string2sz = . - string2
                           2364 
   0BBD                    2365 stringserial:
   0BBD 02                 2366 	.db	2			; bLength
   0BBE 03                 2367 	.db	USB_DT_STRING		; bDescriptorType
   0BBF 00 00 00 00 00 00  2368 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
   0BCF 00 00 00 00 00 00  2369 	.dw	0,0,0,0,0,0,0,0
        00 00 00 00 00 00
        00 00 00 00
                           2370 
