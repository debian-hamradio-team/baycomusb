/*****************************************************************************/

/*
 *      fsk_fpga.h  -- HDLC packet radio modem for EPP using FPGA utility.
 *
 *      Copyright (C) 1998  Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 */

/*****************************************************************************/
/* automatically generated, do not edit */

#define FPGA_PART "XCS10_PC84"
#define FPGA_BOUND 344
#define FPGA_FRAMELEN 166
#define FPGA_NUMFRAMES 572
/*#define FPGA_CONFIGSIZE ((FPGA_FRAMELEN*FPGA_NUMFRAMES+56)/8)*/
#define FPGA_CONFIGSIZE 11876
#define FPGA_BOUNDSIZE ((FPGA_BOUND+7)/8)

/* safe boundary */
static const unsigned char fpga_safebound[] = {
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xcf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff
};

/* IO pin names */
#define FPGA_PIN_CLK24_T              86
#define FPGA_PIN_CLK24_O              87
#define FPGA_PIN_CLK24_I              88
#define FPGA_PIN_CLKMODEM_T           175
#define FPGA_PIN_CLKMODEM_O           176
#define FPGA_PIN_CLKMODEM_I           177
#define FPGA_PIN_ANADDR0_T            232
#define FPGA_PIN_ANADDR0_O            233
#define FPGA_PIN_ANADDR0_I            234
#define FPGA_PIN_ANADDR1_T            229
#define FPGA_PIN_ANADDR1_O            230
#define FPGA_PIN_ANADDR1_I            231
#define FPGA_PIN_ANADDR2_T            220
#define FPGA_PIN_ANADDR2_O            221
#define FPGA_PIN_ANADDR2_I            222
#define FPGA_PIN_ANADDR3_T            217
#define FPGA_PIN_ANADDR3_O            218
#define FPGA_PIN_ANADDR3_I            219
#define FPGA_PIN_ANRD_T               2
#define FPGA_PIN_ANRD_O               3
#define FPGA_PIN_ANRD_I               4
#define FPGA_PIN_ANWR_T               5
#define FPGA_PIN_ANWR_O               6
#define FPGA_PIN_ANWR_I               7
#define FPGA_PIN_ANDATA0_T            316
#define FPGA_PIN_ANDATA0_O            317
#define FPGA_PIN_ANDATA0_I            318
#define FPGA_PIN_ANDATA1_T            313
#define FPGA_PIN_ANDATA1_O            314
#define FPGA_PIN_ANDATA1_I            315
#define FPGA_PIN_ANDATA2_T            304
#define FPGA_PIN_ANDATA2_O            305
#define FPGA_PIN_ANDATA2_I            306
#define FPGA_PIN_ANDATA3_T            301
#define FPGA_PIN_ANDATA3_O            302
#define FPGA_PIN_ANDATA3_I            303
#define FPGA_PIN_ANDATA4_T            283
#define FPGA_PIN_ANDATA4_O            284
#define FPGA_PIN_ANDATA4_I            285
#define FPGA_PIN_ANDATA5_T            271
#define FPGA_PIN_ANDATA5_O            272
#define FPGA_PIN_ANDATA5_I            273
#define FPGA_PIN_ANDATA6_T            262
#define FPGA_PIN_ANDATA6_O            263
#define FPGA_PIN_ANDATA6_I            264
#define FPGA_PIN_ANDATA7_T            259
#define FPGA_PIN_ANDATA7_O            260
#define FPGA_PIN_ANDATA7_I            261
#define FPGA_PIN_ANPA0_T              256
#define FPGA_PIN_ANPA0_O              257
#define FPGA_PIN_ANPA0_I              258
#define FPGA_PIN_ANPA3_T              253
#define FPGA_PIN_ANPA3_O              254
#define FPGA_PIN_ANPA3_I              255
#define FPGA_PIN_ANPA4_T              244
#define FPGA_PIN_ANPA4_O              245
#define FPGA_PIN_ANPA4_I              246
#define FPGA_PIN_ANPA5_T              241
#define FPGA_PIN_ANPA5_O              242
#define FPGA_PIN_ANPA5_I              243
#define FPGA_PIN_ANPA6_T              -1
#define FPGA_PIN_ANPA6_O              -1
#define FPGA_PIN_ANPA6_I              -1
#define FPGA_PIN_ANPA7_T              -1
#define FPGA_PIN_ANPA7_O              -1
#define FPGA_PIN_ANPA7_I              -1
#define FPGA_PIN_ANPB0_T              340
#define FPGA_PIN_ANPB0_O              341
#define FPGA_PIN_ANPB0_I              342
#define FPGA_PIN_ANPB1_T              337
#define FPGA_PIN_ANPB1_O              338
#define FPGA_PIN_ANPB1_I              339
#define FPGA_PIN_ANPB2_T              328
#define FPGA_PIN_ANPB2_O              329
#define FPGA_PIN_ANPB2_I              330
#define FPGA_PIN_ANPB3_T              325
#define FPGA_PIN_ANPB3_O              326
#define FPGA_PIN_ANPB3_I              327
#define FPGA_PIN_ANPB5_T              298
#define FPGA_PIN_ANPB5_O              299
#define FPGA_PIN_ANPB5_I              300
#define FPGA_PIN_ANPB6_T              295
#define FPGA_PIN_ANPB6_O              296
#define FPGA_PIN_ANPB6_I              297
#define FPGA_PIN_ANPB7_T              286
#define FPGA_PIN_ANPB7_O              287
#define FPGA_PIN_ANPB7_I              288
#define FPGA_PIN_ANPC0_T              -1
#define FPGA_PIN_ANPC0_O              -1
#define FPGA_PIN_ANPC0_I              -1
#define FPGA_PIN_ANPC1_T              -1
#define FPGA_PIN_ANPC1_O              -1
#define FPGA_PIN_ANPC1_I              -1
#define FPGA_PIN_ANPC2_T              -1
#define FPGA_PIN_ANPC2_O              -1
#define FPGA_PIN_ANPC2_I              -1
#define FPGA_PIN_ANPC4_T              14
#define FPGA_PIN_ANPC4_O              15
#define FPGA_PIN_ANPC4_I              16
#define FPGA_PIN_MDATAOUT0_T          47
#define FPGA_PIN_MDATAOUT0_O          48
#define FPGA_PIN_MDATAOUT0_I          49
#define FPGA_PIN_MDATAOUT1_T          44
#define FPGA_PIN_MDATAOUT1_O          45
#define FPGA_PIN_MDATAOUT1_I          46
#define FPGA_PIN_MDATAOUT2_T          41
#define FPGA_PIN_MDATAOUT2_O          42
#define FPGA_PIN_MDATAOUT2_I          43
#define FPGA_PIN_MDATAOUT3_T          38
#define FPGA_PIN_MDATAOUT3_O          39
#define FPGA_PIN_MDATAOUT3_I          40
#define FPGA_PIN_MDATAOUT4_T          29
#define FPGA_PIN_MDATAOUT4_O          30
#define FPGA_PIN_MDATAOUT4_I          31
#define FPGA_PIN_MDATAOUT5_T          26
#define FPGA_PIN_MDATAOUT5_O          27
#define FPGA_PIN_MDATAOUT5_I          28
#define FPGA_PIN_MDATAOUT6_T          17
#define FPGA_PIN_MDATAOUT6_O          18
#define FPGA_PIN_MDATAOUT6_I          19
#define FPGA_PIN_MDATAIN_T            68
#define FPGA_PIN_MDATAIN_O            69
#define FPGA_PIN_MDATAIN_I            70
#define FPGA_PIN_MDATAINDAC_T         71
#define FPGA_PIN_MDATAINDAC_O         72
#define FPGA_PIN_MDATAINDAC_I         73
#define FPGA_PIN_PTT_T                190
#define FPGA_PIN_PTT_O                191
#define FPGA_PIN_PTT_I                192
#define FPGA_PIN_RSSIIN_T             59
#define FPGA_PIN_RSSIIN_O             60
#define FPGA_PIN_RSSIIN_I             61
#define FPGA_PIN_RSSIINDAC_T          56
#define FPGA_PIN_RSSIINDAC_O          57
#define FPGA_PIN_RSSIINDAC_I          58
#define FPGA_PIN_MRXC_T               211
#define FPGA_PIN_MRXC_O               212
#define FPGA_PIN_MRXC_I               213
#define FPGA_PIN_MRXD_T               199
#define FPGA_PIN_MRXD_O               200
#define FPGA_PIN_MRXD_I               201
#define FPGA_PIN_MTXC_T               202
#define FPGA_PIN_MTXC_O               203
#define FPGA_PIN_MTXC_I               204
#define FPGA_PIN_MTXD_T               167
#define FPGA_PIN_MTXD_O               168
#define FPGA_PIN_MTXD_I               169
#define FPGA_PIN_MRTS_T               178
#define FPGA_PIN_MRTS_O               179
#define FPGA_PIN_MRTS_I               180
#define FPGA_PIN_MCTS_T               164
#define FPGA_PIN_MCTS_O               165
#define FPGA_PIN_MCTS_I               166
#define FPGA_PIN_MNDCD_T              152
#define FPGA_PIN_MNDCD_O              153
#define FPGA_PIN_MNDCD_I              154
#define FPGA_PIN_MRES_T               143
#define FPGA_PIN_MRES_O               144
#define FPGA_PIN_MRES_I               145
#define FPGA_PIN_LEDDCD_T             131
#define FPGA_PIN_LEDDCD_O             132
#define FPGA_PIN_LEDDCD_I             133
#define FPGA_PIN_LEDPTT_T             140
#define FPGA_PIN_LEDPTT_O             141
#define FPGA_PIN_LEDPTT_I             142
#define FPGA_PIN_T7FD0_T              128
#define FPGA_PIN_T7FD0_O              129
#define FPGA_PIN_T7FD0_I              130
#define FPGA_PIN_T7FD1_T              125
#define FPGA_PIN_T7FD1_O              126
#define FPGA_PIN_T7FD1_I              127
#define FPGA_PIN_T7FD2_T              122
#define FPGA_PIN_T7FD2_O              123
#define FPGA_PIN_T7FD2_I              124
#define FPGA_PIN_T7FD3_T              113
#define FPGA_PIN_T7FD3_O              114
#define FPGA_PIN_T7FD3_I              115
#define FPGA_PIN_T7FTXD_T             89
#define FPGA_PIN_T7FTXD_O             90
#define FPGA_PIN_T7FTXD_I             91
#define FPGA_PIN_T7FRXD_T             83
#define FPGA_PIN_T7FRXD_O             84
#define FPGA_PIN_T7FRXD_I             85
#define FPGA_PIN_T7F25KHZ_T           80
#define FPGA_PIN_T7F25KHZ_O           81
#define FPGA_PIN_T7F25KHZ_I           82
