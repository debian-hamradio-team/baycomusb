/*****************************************************************************/

/*
 *      ifconftest.c  --  Test SIOCGIFCONF ioctl.
 *
 *      Copyright (C) 2000
 *          Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 *  History:
 *   0.1  19.09.2000  Created
 *
 */

/*****************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define _REENTRANT

#include "trx.h"

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <string.h>

#ifdef HAVE_GETOPT_H
#include <getopt.h>
#else
#include "getopt.h"
#endif

#ifdef HAVE_SYSLOG_H
#include <syslog.h>
#endif

//#ifdef HAVE_NET_IF_H
//#include <net/if.h>
//#endif
#ifdef HAVE_LINUX_IF_H
#include <linux/if.h>
#endif
#ifdef HAVE_LINUX_AX25_H
#include <linux/ax25.h>
#endif
#ifdef HAVE_LINUX_SOCKIOS_H
#include <linux/sockios.h>
#endif
#ifdef HAVE_LINUX_IF_ETHER_H
#include <linux/if_ether.h>
#endif
#if defined(HAVE_ARPA_INET_H) && !defined(WIN32)
#include <arpa/inet.h>
#endif

/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
	unsigned char buf[8192];
	struct ifconf ifconf;
	struct ifreq *ifr, *cur, *end, ifr2;
	int fd;

	if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}
	ifconf.ifc_len = sizeof(buf);
	ifconf.ifc_buf = buf;
	if (ioctl(fd, SIOCGIFCONF, &ifconf) == -1) {
		perror("ioctl SIOCGIFCONF");
		exit(1);
	}
	ifr = (struct ifreq *)ifconf.ifc_buf;
	end = (struct ifreq *)(((char *)ifconf.ifc_buf) + ifconf.ifc_len);
	while (ifr < end) {
		cur = ifr;
		ifr = (struct ifreq *)(((char *)ifr) + sizeof(struct ifreq));
		memcpy(&ifr2, cur, sizeof(ifr2));
		if (ioctl(fd, SIOCGIFHWADDR, &ifr2)) {
			perror("ioctl SIOCGIFHWADDR");
			exit(1);
		}
		printf("IF Family: %u Name: %s  Hwaddr Family: %d\n", cur->ifr_addr.sa_family, cur->ifr_name, ifr2.ifr_addr.sa_family);

	}
	exit(0);
}
