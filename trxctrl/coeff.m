%
fs=16000;
overs=16;
fg1=3500;
fg2=4500;
flen=16;

%c=remez(overs*flen-1,[ 0 2*fg1/fs/overs 2*fg2/fs/overs 1 ], [ 1 1 0 0 ]);
c=sinc((0.5-overs*flen/2:overs*flen/2-0.5)*(2*fg2/fs/overs)).*hamming(overs*flen)';

for t=1:overs,
  tt(t) = sum(abs(c(t:16:overs*flen)));
end;

c=c./max(tt);

t1=fft(c);
plot((0:overs*flen-1)*fs/flen,20*log10(abs(t1)));

fid=fopen('filter.txt','w');
fprintf(fid,'static const int16_t rxfilter[%d][%d] = {\n',overs,flen);
for t=1:overs,
  fprintf(fid,'\t{ ');
  for tt=0:flen-1,
    fprintf(fid, '%6d, ', round(c(tt*overs+t) * 65536));
  end;
  fprintf(fid,'},\n');
end;
fprintf(fid,'};');
fclose(fid);
