/*****************************************************************************/

/*
 *      coinst.c  --  Windows Coinstaller (downloads firmware).
 *
 *      Copyright (C) 2000-2001  Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdarg.h>

#include <windows.h>

//typedef UINT * UINT_PTR;
//typedef ULONG * ULONG_PTR;
//typedef DWORD * DWORD_PTR;

#include <setupapi.h>
#include <stdio.h>

/* ---------------------------------------------------------------------- */

int lprintf(unsigned vl, const char *format, ...)
	__attribute__ ((format (printf, 2, 3)));

int lprintf(unsigned vl, const char *format, ...)
{
	va_list ap;
	char buf[512];
	int i;
	
        va_start(ap, format);
	i = vsnprintf(buf, sizeof(buf), format, ap);
	va_end(ap);
	OutputDebugString(buf);
        return i;
}

/* ---------------------------------------------------------------------- */

_stdcall
HRESULT CoDeviceInstall(IN     DI_FUNCTION               InstallFunction,
                        IN     HDEVINFO                  DeviceInfoSet,
                        IN     PSP_DEVINFO_DATA          DeviceInfoData,  OPTIONAL
                        IN OUT PCOINSTALLER_CONTEXT_DATA Context)
{
        OutputDebugString("My Co Device Installer\n");
	switch (InstallFunction) {
	case DIF_INSTALLDEVICE:
		if (!Context->PostProcessing) {
			lprintf(0, "DIF_INSTALLDEVICE: preprocessing\n");
			return ERROR_DI_POSTPROCESSING_REQUIRED;
		}
		lprintf(0, "DIF_INSTALLDEVICE: postprocessing\n");
		return NO_ERROR;
        
	case DIF_REMOVE:
		lprintf(0, "DIF_REMOVE: postprocessing\n");
		return NO_ERROR;

	default:
		return NO_ERROR;
	}
}

