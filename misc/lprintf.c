/*****************************************************************************/

/*
 *      lprintf.c  --  printf to console, syslog, debuglog.
 *
 *      Copyright (C) 2000,2001  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#endif
#ifdef HAVE_ERRNO_H
#include <errno.h>
#endif

#include "sysdeps.h"

/* ---------------------------------------------------------------------- */

unsigned int verboselevel = 0;
unsigned int syslogmsg = 0;

/* --------------------------------------------------------------------- */

#if defined(WIN32)

int lprintf(unsigned int vl, const char *format, ...)
{
        va_list ap;
        int r;

        if (vl > verboselevel)
                return 0;
        va_start(ap, format);
        if (syslogmsg) {
		char buf[512];
		r = vsnprintf(buf, sizeof(buf), format, ap);
		OutputDebugString(buf);
        } else {
                r = vfprintf(stderr, format, ap);
		fflush(stderr);
	}
        va_end(ap);
        return r;
}

int lerr(unsigned int vl, const char *fn)
{
	unsigned int e = GetLastError();
	return lprintf(vl, "baycomusb: Error %d in %s\n", e, fn);
}

#else

/* --------------------------------------------------------------------- */

int lprintf(unsigned int vl, const char *format, ...)
{
        va_list ap;
        int r;

        if (vl > verboselevel)
                return 0;
        va_start(ap, format);
#ifdef HAVE_VSYSLOG
        if (syslogmsg) {
                static const int logprio[] = { LOG_ERR, LOG_INFO };
                vsyslog((vl > 1) ? LOG_DEBUG : logprio[vl], format, ap);
                r = 0;
        } else
#endif
		{
			r = vfprintf(stderr, format, ap);
			fflush(stderr);
		}
        va_end(ap);
        return r;
}

int lerr(unsigned int vl, const char *fn)
{
	int e = errno;
	return lprintf(vl, "baycomusb: Error %s (%d) in %s\n", strerror(e), e, fn);
}

/* ---------------------------------------------------------------------- */
#endif
