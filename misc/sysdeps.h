/*****************************************************************************/

/*
 *      sysdeps.h  --  System dependencies.
 *
 *      Copyright (C) 1998-2001  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 */

/*****************************************************************************/

#ifndef _SYSDEPS_H
#define _SYSDEPS_H

/* ---------------------------------------------------------------------- */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef GETOPT_H
#include <getopt.h>
#endif

#ifdef UNISTD_H
#include <unistd.h>
#endif

#include <sys/types.h>
#include <stdarg.h>
#include <stdlib.h>

/* ---------------------------------------------------------------------- */

#if __GNUC__ < 5
#define only_inline extern inline
#else
#define only_inline inline
#endif

/* ---------------------------------------------------------------------- */

/*
 * Bittypes
 */

#ifndef HAVE_BITTYPES

#if __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)
typedef int int8_t __attribute__((__mode__(__QI__)));
typedef unsigned int u_int8_t __attribute__((__mode__(__QI__)));
typedef int int16_t __attribute__((__mode__(__HI__)));
typedef unsigned int u_int16_t __attribute__((__mode__(__HI__)));
typedef int int32_t __attribute__((__mode__(__SI__)));
typedef unsigned int u_int32_t __attribute__((__mode__(__SI__)));
typedef int int64_t __attribute__((__mode__(__DI__)));
typedef unsigned int u_int64_t __attribute__((__mode__(__DI__)));
#else
typedef char /* deduced */ int8_t __attribute__((__mode__(__QI__)));
typedef unsigned char /* deduced */ u_int8_t __attribute__((__mode__(__QI__)));
typedef short /* deduced */ int16_t __attribute__((__mode__(__HI__)));
typedef unsigned short /* deduced */ u_int16_t __attribute__((__mode__(__HI__)));
typedef long /* deduced */ int32_t __attribute__((__mode__(__SI__)));
typedef unsigned long /* deduced */ u_int32_t __attribute__((__mode__(__SI__)));
typedef long long /* deduced */ int64_t __attribute__((__mode__(__DI__)));
typedef unsigned long long /* deduced */ u_int64_t __attribute__((__mode__(__DI__)));
#endif

#endif /* !HAVE_BITTYPES */

/* ---------------------------------------------------------------------- */
/*
 * syslog routines
 */

#ifdef HAVE_SYS_SYSLOG_H
#include <sys/syslog.h>
#else

#define LOG_EMERG       0       /* system is unusable */
#define LOG_ALERT       1       /* action must be taken immediately */
#define LOG_CRIT        2       /* critical conditions */
#define LOG_ERR         3       /* error conditions */
#define LOG_WARNING     4       /* warning conditions */
#define LOG_NOTICE      5       /* normal but significant condition */
#define LOG_INFO        6       /* informational */
#define LOG_DEBUG       7       /* debug-level messages */

/* facility codes */
#define LOG_KERN        (0<<3)  /* kernel messages */
#define LOG_USER        (1<<3)  /* random user-level messages */
#define LOG_MAIL        (2<<3)  /* mail system */
#define LOG_DAEMON      (3<<3)  /* system daemons */
#define LOG_AUTH        (4<<3)  /* security/authorization messages */
#define LOG_SYSLOG      (5<<3)  /* messages generated internally by syslogd */
#define LOG_LPR         (6<<3)  /* line printer subsystem */
#define LOG_NEWS        (7<<3)  /* network news subsystem */
#define LOG_UUCP        (8<<3)  /* UUCP subsystem */
#define LOG_CRON        (9<<3)  /* clock daemon */
#define LOG_AUTHPRIV    (10<<3) /* security/authorization messages (private) */
#define LOG_FTP         (11<<3) /* ftp daemon */

only_inline void closelog(void) {}
only_inline void openlog(__const char *__ident, int __option, int __facility) {}
only_inline void vsyslog(int __pri, __const char *__fmt, va_list __ap) {}

#endif

/* ---------------------------------------------------------------------- */

#ifdef __MINGW32__
#include <windows.h>
only_inline void usleep(unsigned long x)
{
	Sleep(x / 1000);
}
#endif

/* ---------------------------------------------------------------------- */

#ifdef HAVE_GETTIMEOFDAY

#include <sys/time.h>
#include <unistd.h>

only_inline int gettime(struct timeval *tv)
{
	return gettimeofday(tv, NULL);
}

only_inline unsigned int gettimems(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

#elif HAVE_GETSYSTEMTIME

#ifdef HAVE_WINDOWS_H
#include <windows.h>
#endif

#if 0
struct timeval {
        long    tv_sec;
        long    tv_usec;
};
#endif

only_inline int gettime(struct timeval *tv)
{
	SYSTEMTIME tm;
	FILETIME ft;
	LARGE_INTEGER li;

	GetSystemTime(&tm);
	SystemTimeToFileTime(&tm, &ft);
	li._STRUCT_NAME(u.)LowPart = ft.dwLowDateTime;
	li._STRUCT_NAME(u.)HighPart = ft.dwHighDateTime;
	li.QuadPart /= 10;
	tv->tv_sec = li.QuadPart / 1000000;
	tv->tv_usec = li.QuadPart - 1000000 * tv->tv_sec;
	return 0;
}

only_inline unsigned int gettimems(void)
{
	SYSTEMTIME tm;
	FILETIME ft;
	LARGE_INTEGER li;

	GetSystemTime(&tm);
	SystemTimeToFileTime(&tm, &ft);
	li._STRUCT_NAME(u.)LowPart = ft.dwLowDateTime;
	li._STRUCT_NAME(u.)HighPart = ft.dwHighDateTime;
	li.QuadPart /= 10000;
	return li.QuadPart;
}

#else

#error "Don't know how to get a high resolution time"

#endif

/* ---------------------------------------------------------------------- */

#ifndef HAVE_RANDOM

only_inline long int random(void)
{
	return rand();
}

#endif

/* ---------------------------------------------------------------------- */

#ifndef HAVE_VSNPRINTF
extern int snprintf(char *str, size_t n, char const *fmt, ...) __attribute__ ((format (printf, 3, 4)));
extern int vsnprintf(char *str, size_t n, char const *fmt, va_list args);
#endif

extern int snprintpkt(char *buf, size_t sz, const u_int8_t *pkt, unsigned len);
        
extern int lprintf(unsigned vl, const char *format, ...) __attribute__ ((format (printf, 2, 3)));
extern int lerr(unsigned int vl, const char *fn);

extern unsigned int verboselevel;
extern unsigned int syslogmsg;

/* ---------------------------------------------------------------------- */
#endif /* _SYSDEPS_H */
