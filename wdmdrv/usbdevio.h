/*****************************************************************************/

/*
 *	usbdevio.h  --  USB device file system.
 *
 *	Copyright (C) 2000-2001
 *          Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  History:
 *   0.1  04.01.2000  Created
 *
 */

/*****************************************************************************/

#ifndef _USBDEVIO_H
#define _USBDEVIO_H

/* --------------------------------------------------------------------- */

#define USBDEVFS_URB_DISABLE_SPD         0x0001
#define USBDEVFS_URB_ISO_ASAP            0x0002
#define USBDEVFS_URB_QUEUE_BULK          0x0010
#define USBDEVFS_URB_NO_FSBR             0x0020
#define USBDEVFS_URB_ZERO_PACKET         0x0040

/* --------------------------------------------------------------------- */

/* usbdevio ioctl codes */

struct usbdevfs_ctrltransfer {
        unsigned int usberr;   /* set by kernel */
	unsigned int timeout;  /* in milliseconds */
	unsigned int cookie;   /* needed to cancel request */
	unsigned int flags;
	unsigned char requesttype;
	unsigned char request;
	unsigned short value;
	unsigned short index;
	unsigned int len;      /* updated by kernel */
        /* data follows */
};

struct usbdevfs_bulktransfer {
        unsigned int usberr;   /* set by kernel */
	unsigned int timeout;  /* in milliseconds */
	unsigned int cookie;   /* needed to cancel request */
	unsigned int flags;
	unsigned int ep;
	unsigned int len;      /* updated by kernel */
        /* data follows */        
};

struct usbdevfs_isopacketdesc {
	unsigned int offset;
	unsigned int length;
	unsigned int usberr;   /* set by kernel */
};

struct usbdevfs_isotransfer {
        unsigned int usberr;   /* set by kernel */
	unsigned int timeout;  /* in milliseconds */
	unsigned int cookie;   /* needed to cancel request */
	unsigned int flags;
	unsigned int ep;
	unsigned int start_frame;
	unsigned int number_of_packets;
	unsigned int len;           /* updated by kernel */
	unsigned int error_count;   /* set by kernel */
	/* variable length */
	struct usbdevfs_isopacketdesc iso_frame_desc[0];
        /* data follows */
};

struct usbdevfs_setinterface {
        unsigned int usberr;   /* set by kernel */
	unsigned int timeout;  /* in milliseconds */
        unsigned int interf;
	unsigned int altsetting;
};

struct usbdevfs_setconfig {
        unsigned int usberr;   /* set by kernel */
	unsigned int timeout;  /* in milliseconds */
	unsigned int config;
};

struct usbdevfs_resetep {
        unsigned int usberr;   /* set by kernel */
	unsigned int timeout;  /* in milliseconds */
        unsigned int ep;
};

struct usbdevfs_disconnectsignal {
	unsigned int signr;
	void *context;
};


/* --------------------------------------------------------------------- */

#define FILE_DEVICE_USBDEVIO       0x80fe

#define USBDEVFS_CONTROL           CTL_CODE(FILE_DEVICE_USBDEVIO,20,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_BULK              CTL_CODE(FILE_DEVICE_USBDEVIO,21,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_ISO               CTL_CODE(FILE_DEVICE_USBDEVIO,22,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_RESETEP           CTL_CODE(FILE_DEVICE_USBDEVIO,23,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_ABORTEP           CTL_CODE(FILE_DEVICE_USBDEVIO,24,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_SETINTERFACE      CTL_CODE(FILE_DEVICE_USBDEVIO,25,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_SETCONFIGURATION  CTL_CODE(FILE_DEVICE_USBDEVIO,26,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_SUBMITURB         CTL_CODE(FILE_DEVICE_USBDEVIO,27,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_DISCARDURB        CTL_CODE(FILE_DEVICE_USBDEVIO,28,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_REAPURB           CTL_CODE(FILE_DEVICE_USBDEVIO,29,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_REAPURBNDELAY     CTL_CODE(FILE_DEVICE_USBDEVIO,30,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_DISCSIGNAL        CTL_CODE(FILE_DEVICE_USBDEVIO,31,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_CLAIMINTERFACE    CTL_CODE(FILE_DEVICE_USBDEVIO,32,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_RELEASEINTERFACE  CTL_CODE(FILE_DEVICE_USBDEVIO,33,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_GETLASTERROR      CTL_CODE(FILE_DEVICE_USBDEVIO,34,METHOD_BUFFERED,FILE_ANY_ACCESS)
#define USBDEVFS_CANCELTRANSFER    CTL_CODE(FILE_DEVICE_USBDEVIO,35,METHOD_BUFFERED,FILE_ANY_ACCESS)

/* --------------------------------------------------------------------- */
#endif /* _USBDEVIO_H */
