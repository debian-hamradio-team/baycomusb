Summary: Driver and diagnostic utilities for the Baycom USB modem family
Name: baycomusb
Version: 0.10
Release: 1
URL: http://www.baycom.org
Source0: baycomusb-%{version}.tar.gz
Copyright: GPL
Group: Networking/Hamradio
BuildRoot: %{_tmppath}/baycomusb-%{version}-buildroot
Packager: Thomas Sailer <sailer@ife.ee.ethz.ch>
# Requires: /sbin/ifconfig /sbin/route /sbin/arp

%description
This package contains the server and diagnostic utilities
for the Baycom USB modem.

%prep
%setup

%build
%configure --enable-userdrv --enable-mmx
make

%install
rm -rf $RPM_BUILD_ROOT
%{makeinstall}
install -g root -o root -m 0755 -d $RPM_BUILD_ROOT/var/hamradio/transceivers/
install -g root -o root -m 0755 -d $RPM_BUILD_ROOT/etc/ax25/
touch $RPM_BUILD_ROOT/etc/ax25/baycomusb.conf
install -g root -o root -m 0755 baycomusb.ifchange $RPM_BUILD_ROOT/etc/ax25
install -g root -o root -m 0755 -d $RPM_BUILD_ROOT/etc/rc.d/init.d
install -g root -o root -m 0755 baycomusb.initscript $RPM_BUILD_ROOT/etc/rc.d/init.d/baycomusb
install -g root -o root -m 0755 -d $RPM_BUILD_ROOT/dev
mknod -m 0700 $RPM_BUILD_ROOT/dev/baycomusb c 180 240

%clean
rm -rf $RPM_BUILD_ROOT

%changelog

* Tue Jun 26 2001 Thomas Sailer <t.sailer@alumni.ethz.ch>
- use usermode server for now, so we do not require a patched kernel

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%attr(0755,root,root) %dir 
%attr(0644,root,root) %config(noreplace) /etc/ax25/baycomusb.conf
%attr(0644,root,root) %config(noreplace) /etc/ax25/baycomusb.ifchange
%attr(0755,root,root) %config /etc/rc.d/init.d/baycomusb
%attr(-,root,root) /dev/baycomusb
#%attr(0755,root,root) %{_sbindir}/baycomusb
#%attr(0755,root,root) %{_sbindir}/writeeeprom
%attr(0755,root,root) %{_sbindir}/baycomusbserv
%attr(0755,root,root) %{_bindir}/baycomusbtrx
#%attr(0644,root,root) %{_mandir}/man8/baycomusb.8*
