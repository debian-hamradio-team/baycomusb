/* define if int8_t and friends are available */
#undef HAVE_BITTYPES

/* define if struct ifreq has the ifr_newname symbol */
#undef HAVE_GETSYSTEMTIME

/* define if MMX is to be used */
#undef USEMMX

/* define if VIS is to be used */
#undef USEVIS

/* define if we have found the MKISS driver (line discipline) */
#undef HAVE_MKISS

/* define if we want baycomusbserv to not require the kernel mode driver */
#undef USERMODEDRV

/* use ORBit for interprocess communication */
#undef ORBITIPC

/* define if M_PI is not defined by math.h */
#undef M_PI

/* define if M_LN10 is not defined by math.h */
#undef M_LN10

/* define if struct ifreq has the ifr_newname symbol */
#undef HAVE_IFRNEWNAME

/* define if we have DirectX includes */
#undef HAVE_DIRECTX

/* define if National Language Support is requested */
#undef ENABLE_NLS

/* define if compiling under Windows32 */
#undef WIN32

/* NLS stuff */
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY

/* various directories */
#undef PACKAGE_LOCALE_DIR
#undef PACKAGE_DATA_DIR
#undef PACKAGE_SOURCE_DIR
