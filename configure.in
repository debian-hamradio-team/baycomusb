dnl Process this file with autoconf to produce a configure script.

AC_INIT(baycomusb.spec)
AC_CANONICAL_SYSTEM

AM_INIT_AUTOMAKE(baycomusb, 0.10)
AM_CONFIG_HEADER(config.h)

dnl AC_CHECK_TOOL()

AC_PROG_MAKE_SET
AC_ISC_POSIX
AC_PROG_CC
AM_PROG_CC_STDC
AC_GNU_SOURCE
dnl AC_PROG_RANLIB
AC_C_CONST
AC_C_INLINE
AC_HEADER_STDC

AC_CHECK_PROG(RANLIB, ranlib, ranlib, :)
AC_CHECK_PROG(DLLTOOL, dlltool, dlltool, dlltool)
AC_CHECK_PROG(AS, as, as, as)
AC_CHECK_PROG(AR, ar, ar, ar)
AC_CHECK_PROG(WINDRES, windres, windres, i686-pc-cygwin-windres)

AC_CYGWIN
AC_MINGW32
AC_EXEEXT
AC_OBJEXT

AC_TYPE_SIGNAL

dnl check for cross compiler path
if test x$ac_cv_prog_cc_cross = xyes; then
  AC_MSG_CHECKING(for cross compiler path)
  if test -d /usr/local/cross/i686-pc-cygwin; then
    CROSSCOMPPATH=/usr/local/cross/i686-pc-cygwin
  elif test -d /usr/local/cygwin/i686-pc-cygwin; then
    CROSSCOMPPATH=/usr/local/cygwin/i686-pc-cygwin
  else
    AC_ERROR("cross compiler not found")
  fi
  AC_MSG_RESULT($CROSSCOMPPATH)
fi

if test x$ac_cv_mingw32 != xyes; then
AC_CHECK_LIB(m,cos)
fi
AC_CHECK_LIB(pthread,pthread_create,LIBTHREAD="$LIBTHREAD -lpthread")
AC_CHECK_LIB(uuid,GUID_NULL)
AC_CHECK_HEADERS(getopt.h sys/ioctl.h windows.h sys/syslog.h sys/uio.h errno.h unistd.h pthread.h)
AC_CHECK_HEADERS(net/if.h net/if_arp.h net/route.h sys/socket.h arpa/inet.h netinet/in.h netax25/ax25.h)
AC_CHECK_HEADERS(linux/if.h linux/if_ether.h linux/ax25.h linux/sockios.h linux/baycom_usb.h)
AC_CHECK_HEADERS(sys/soundcard.h linux/soundcard.h)
AC_CHECK_FUNCS(snprintf gettimeofday random syslog vsyslog revoke)
AC_REPLACE_FUNCS(vsnprintf)
AC_CHECK_FUNCS(getopt_long,,LIBOBJS="$LIBOBJS getopt.o getopt1.o")
xlibs=$LIBS
LIBS=
AC_CHECK_LIB(util,openpty)
AC_CHECK_FUNCS(openpty,DRVLIBOBJS="$DRVLIBOBJS $LIBS",DRVLIBOBJS="$DRVLIBOBJS openpty.o")
LIBS=$xlibs

AC_CHECK_PROG(AS8051, asx8051, asx8051, :, /usr/local/8051/bin:/usr/local/cross/8051/bin)
AC_CHECK_PROG(LD8051, aslink, aslink, :, /usr/local/8051/bin:/usr/local/cross/8051/bin)

AC_CHECK_HEADERS(linux/usbdevice_fs.h)
AC_HEADER_TIME
AC_FUNC_ALLOCA

AC_MSG_CHECKING(for GetSystemTime)
getsystemtime=no
AC_TRY_COMPILE([#include <windows.h>], 
     [ SYSTEMTIME tm; GetSystemTime(&tm); ],
     AC_DEFINE(HAVE_GETSYSTEMTIME) getsystemtime=yes)
AC_MSG_RESULT($getsystemtime)

dnl AC_CHECK_TYPE(int8_t,char)
dnl AC_CHECK_TYPE(u_int8_t,unsigned char)
dnl AC_CHECK_TYPE(int16_t,short)
dnl AC_CHECK_TYPE(u_int16_t,unsigned short)
dnl AC_CHECK_TYPE(int32_t,long)
dnl AC_CHECK_TYPE(u_int32_t,unsigned long)

AC_CHECK_TYPE(size_t,unsigned int)

AC_MSG_CHECKING(for bittypes)
bittypes=no
AC_TRY_COMPILE([#include <sys/types.h>], 
     [ int8_t a; u_int8_t b; int16_t c; u_int16_t d; int32_t e; u_int32_t f; ],
     AC_DEFINE(HAVE_BITTYPES) bittypes=yes)
AC_MSG_RESULT($bittypes)

AC_MSG_CHECKING(for M_PI)
mpi=no
AC_TRY_COMPILE([#include <math.h>], 
     [ double f = M_PI; ],
     mpi=yes,AC_DEFINE(M_PI,3.14159265358979323846))
AC_MSG_RESULT($mpi)

AC_MSG_CHECKING(for M_LN10)
mln10=no
AC_TRY_COMPILE([#include <math.h>],
     [ double f = M_LN10; ],
     mln10=yes,AC_DEFINE(M_LN10,2.30258509299404568402))
AC_MSG_RESULT($mln10)

AC_MSG_CHECKING(for MKISS (N_AX25 line discipline))
mkiss=no
AC_EGREP_CPP(yes,
     [#include <sys/ioctl.h>
     #ifdef N_AX25
       yes
     #endif
     ], mkiss=yes)
AC_MSG_RESULT($mkiss)

AC_MSG_CHECKING(for ifr_newname in struct ifreq)
ifrnewname=no
AC_TRY_COMPILE([#include <linux/if.h>],
     [struct ifreq ifr; ifr.ifr_newname[0]=0; ], AC_DEFINE(HAVE_IFRNEWNAME) ifrnewname=yes)
AC_MSG_RESULT($ifrnewname)

if test x$ac_cv_cygwin = xyes -o x$ac_cv_mingw32 = xyes; then
  AC_DEFINE(WIN32)
  LIBS="$LIBS -lgdi32"
fi

PKG_CHECK_MODULES(XML, libxml-2.0)

dnl Only use -Wall if we have gcc
if test "x$GCC" = "xyes"; then
  if test -z "`echo "$CFLAGS" | grep "\-Wall" 2> /dev/null`" ; then
    CFLAGS="$CFLAGS -Wall -O2"
  fi
fi

if test x$ac_cv_cygwin = xyes -o x$ac_cv_mingw32 = xyes; then
  XMLCFLAGS=
  XMLLIBS=
else
  xlibs=$LIBS
  LIBS=
  AC_CHECK_LIB(util,openpty)
  AC_CHECK_FUNCS(openpty,LIBTHREAD="$LIBTHREAD $LIBS",LIBOBJS="$LIBOBJS openpty.o")
  LIBS=$xlibs
fi

if test x$ac_cv_cygwin = xyes -o x$ac_cv_mingw32 = xyes; then
  xcflags="$CFLAGS"
  CFLAGS="$CFLAGS -I$srcdir/directx/include -I$srcdir/directx/include/directx6"
  AC_MSG_CHECKING(for DirectX includes)
  directx=no
  AC_TRY_COMPILE([#include <directx.h>], 
       [ LPDIRECTSOUND dsplay; LPDIRECTSOUNDCAPTURE dsrec; ],
       AC_DEFINE(HAVE_DIRECTX) directx=yes)
  AC_MSG_RESULT($directx)
  CFLAGS="$xcflags"
fi

dnl Set PACKAGE_LOCALE_DIR in config.h.
if test "x${prefix}" = "xNONE"; then
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${ac_default_prefix}/${DATADIRNAME}/locale")
else
  AC_DEFINE_UNQUOTED(PACKAGE_LOCALE_DIR, "${prefix}/${DATADIRNAME}/locale")
fi

dnl Set PACKAGE_DATA_DIR in config.h.
if test "x${datadir}" = 'x${prefix}/share'; then
  if test "x${prefix}" = "xNONE"; then
    AC_DEFINE_UNQUOTED(PACKAGE_DATA_DIR, "${ac_default_prefix}/share/${PACKAGE}")
  else
    AC_DEFINE_UNQUOTED(PACKAGE_DATA_DIR, "${prefix}/share/${PACKAGE}")
  fi
else
  AC_DEFINE_UNQUOTED(PACKAGE_DATA_DIR, "${datadir}/${PACKAGE}")
fi

dnl Set PACKAGE_SOURCE_DIR in config.h.
packagesrcdir=`cd $srcdir && pwd`
AC_DEFINE_UNQUOTED(PACKAGE_SOURCE_DIR, "${packagesrcdir}")

AC_ARG_ENABLE(userdrv,
[  --enable-userdrv        Build user mode driver],
[case "${enableval}" in
  yes) userdrv=true ;;
  no)  userdrv=false ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --enable-userdrv) ;;
esac],[userdrv=false])

if test x$userdrv = xtrue; then
    AC_DEFINE(USERMODEDRV)
fi

AC_ARG_ENABLE(mkiss,
[  --disable-mkiss         Don not use the MKISS driver even if available],
[mkissena=false],[mkissena=true])

if test x$mkiss = xyes; then
    AC_DEFINE(HAVE_MKISS)
fi

AC_ARG_ENABLE(mmx,
[  --enable-mmx            Utilize MMX(tm) instructions if available (x86 only)],
[case "${enableval}" in
  yes) usemmx=true ;;
  no)  usemmx=false ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --enable-mmx) ;;
esac],[usemmx=false])

AC_ARG_ENABLE(vis,
[  --enable-vis            Utilize VIS(tm) instructions if available (ultrasparc only)],
[case "${enableval}" in
  yes) usevis=true ;;
  no)  usevis=false ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --enable-vis) ;;
esac],[usevis=false])

if test x$usemmx = xtrue; then
    AC_DEFINE(USEMMX)
fi

if test x$usevis = xtrue; then
    AC_DEFINE(USEVIS)
    dnl CFLAGS="$CFLAGS -mv8plus -mvis -Wa,-xarch=v8plusa"
    CFLAGS="$CFLAGS -Wa,-xarch=v8plusa"
fi

AM_CONDITIONAL(USEMMX, test x$usemmx = xtrue)
AM_CONDITIONAL(USEVIS, test x$usevis = xtrue)
AM_CONDITIONAL(HAVEGTK, test x$gtk = xyes)
AM_CONDITIONAL(WIN32, test x$ac_cv_cygwin = xyes -o x$ac_cv_mingw32 = xyes)
AM_CONDITIONAL(CROSSCOMP, test x$ac_cv_prog_cc_cross = xyes)
AM_CONDITIONAL(DIRECTX, test x$directx = xyes)
AM_CONDITIONAL(HAVEAS51, test "x$ac_cv_prog_AS8051" != "x:")
AM_CONDITIONAL(HAVELD51, test "x$ac_cv_prog_LD8051" != "x:")
AM_CONDITIONAL(USERMODEDRV, test x$userdrv = xtrue)
AM_CONDITIONAL(ORBITIPC, test x$orbitipc = xtrue)

AC_SUBST(USERMODEDRV)
AC_SUBST(HAVE_MKISS)
AC_SUBST(USEMMX)
AC_SUBST(USEVIS)
AC_SUBST(XMLCFLAGS)
AC_SUBST(XMLLIBS)
AC_SUBST(GTK_LIBS)
AC_SUBST(GTK_CFLAGS)
AC_SUBST(ORBITIPC)
AC_SUBST(ORBIT_EXEC_PREFIX)
AC_SUBST(ORBIT_IDL)
AC_SUBST(ORBIT_CFLAGS)
AC_SUBST(ORBIT_VERSION)
AC_SUBST(ORBIT_LIBS)
AC_SUBST(HAVE_BITTYPES)
AC_SUBST(M_PI)
AC_SUBST(LIBOBJS)
AC_SUBST(LIBTHREAD)
AC_SUBST(DRVLIBOBJS)
AC_SUBST(HAVE_IFRNEWNAME)
AC_SUBST(HAVE_DIRECTX)
AC_SUBST(WIN32)
AC_OUTPUT([Makefile
fpga/Makefile
firmware/Makefile
firmware/fskfirmware/Makefile
firmware/afskfirmware/Makefile
firmware/audiofirmware/Makefile
firmware/audio2firmware/Makefile
firmware/dlfirmware/Makefile
firmware/dl2firmware/Makefile
firmware/resetfirmware/Makefile
firmware/reset2firmware/Makefile
firmware/bscanfirmware/Makefile
directx/Makefile
kerneldrv/Makefile
wdmdrv/Makefile
misc/Makefile
usbdrv/Makefile
trxapi/Makefile
flexdrv/Makefile
winserv/Makefile
diag/Makefile
coinstaller/Makefile])
