/*****************************************************************************/

/*
 *      baycom_usb.h  --  baycom USB radio modem driver, ioctl structures etc.
 *
 *      Copyright (C) 2000  Thomas Sailer (sailer@ife.ee.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 *  History:
 *   0.1  09.07.2000  Started
 */


/*****************************************************************************/

#ifndef _BAYCOM_USB_H
#define _BAYCOM_USB_H

#include <linux/ioctl.h>
#include <linux/if.h>

/* -------------------------------------------------------------------- */

#define BAYCOMUSB_MINOR_BASE    240


struct baycomusb_startnetdev {
	unsigned int busnr;
	unsigned int devnr;
	char ifname[IFNAMSIZ];
};

#define BAYCOMUSB_STARTNETDEV   _IOWR('u', 0, struct baycomusb_startnetdev)

struct baycomusb_status {
	int ptt;
	int dcd;
	unsigned int rssi;
	unsigned int uartchars;
};

#define BAYCOMUSB_GETSTATUS     _IOWR('u', 1, struct baycomusb_status)

struct baycomusb_receiveuart {
	unsigned int len;
	unsigned char buffer[64];
};

#define BAYCOMUSB_RECEIVEUART   _IOWR('u', 2, struct baycomusb_receiveuart)

struct baycomusb_transmituart {
	unsigned char txchar;
};

#define BAYCOMUSB_TRANSMITUART  _IOW('u', 3, struct baycomusb_transmituart)

struct baycomusb_modemdisc {
	int setdirection;
	int setoutput;
	unsigned int input;
	unsigned int output;
	unsigned int direction;
};

#define BAYCOMUSB_MODEMDISC     _IOWR('u', 4, struct baycomusb_modemdisc)

struct baycomusb_t7fcontrol {
	int setoutput;
	unsigned int input;
	unsigned int output;
};

#define BAYCOMUSB_T7FCONTROL    _IOWR('u', 5, struct baycomusb_t7fcontrol)

struct baycomusb_forceptt {
	int ptt;
};

#define BAYCOMUSB_FORCEPTT      _IOW('u', 6, struct baycomusb_forceptt)

struct baycomusb_setleds {
	int leds;
};

#define BAYCOMUSB_SETLEDS       _IOW('u', 7, struct baycomusb_setleds)

/* -------------------------------------------------------------------- */
#endif /* _BAYCOM_USB_H */
