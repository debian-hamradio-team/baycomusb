#include <gtk/gtk.h>


gboolean
on_mainwindow_destroy_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_mainwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_freqlist_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_modemconfig_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_selectmodem_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_audioparams_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_spkscope_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_spkspectrum_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_micscope_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_micspectrum_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_audiodtmf_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_audiopwrs_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_terminal_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_channelwindow_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_progchmem_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_qrg_changed                         (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_stn_changed                         (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_buttonptt_toggled                   (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_button1750_pressed                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_button1750_released                 (GtkButton       *button,
                                        gpointer         user_data);

GtkWidget*
create_led_pixmap (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

GtkWidget*
create_led_pixmap (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

GtkWidget*
signalhistory_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

gboolean
on_terminalwindow_delete_event         (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_text_key_press_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_text_key_release_event              (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_chmemorywindow_delete_event         (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_chprogwindow_delete_event           (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_chprog_channel_changed              (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_chprog_raster_toggled               (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_chprogcancel_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_chprogok_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_freqlistwindow_delete_event         (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_freqlist_select_row                 (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_freqlist_unselect_row               (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_freqlistset_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_freqlistnew_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_freqlistupdate_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_freqlistdelete_clicked              (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_newstationwindow_delete_event       (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_newstnmodeentry_changed             (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_newstnok_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_newstncancel_clicked                (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_modemselectwindow_delete_event      (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_modemlist_select_row                (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_modemlist_unselect_row              (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_modemselok_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_modemselcancel_clicked              (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_configwindow_delete_event           (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_configwindowok_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_configwindowcancel_clicked          (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_dtmfwindow_key_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_dtmfwindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_dtmfclear_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_dtmf_key                            (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_spkscopewindow_delete_event         (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

GtkWidget*
scope_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

gboolean
on_spkspectrumwindow_delete_event      (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

GtkWidget*
spectrum_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

gboolean
on_micscopewindow_delete_event         (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

GtkWidget*
scope_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

gboolean
on_micspectrumwindow_delete_event      (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

GtkWidget*
spectrum_new (gchar *widget_name, gchar *string1, gchar *string2,
                gint int1, gint int2);

gboolean
on_paramwindow_delete_event            (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_paramwindowok_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_paramwindowcancel_clicked           (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_audiopwrwindow_delete_event         (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);
