/*****************************************************************************/

/*
 *      trxapp.h  --  Trx Application Header File.
 *
 *      Copyright (C) 2000-2001
 *        Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*****************************************************************************/

#ifndef _TRXAPP_H
#define _TRXAPP_H

/* ---------------------------------------------------------------------- */

#define _GNU_SOURCE
#define _REENTRANT

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gtk/gtk.h>

#include "trxapi.h"

/* ---------------------------------------------------------------------- */

#define MAXSTNNAME 64

struct stndb_entry {
	char name[MAXSTNNAME];
	trxapi_frequency_t rx;
	trxapi_frequency_t tx;
	int mode;  /* trxapi_baycomusb_mode_t or -1 */
	unsigned long bitraterx, bitratetx;
};

extern void stndb_open(void);
extern void stndb_close(void);
extern int stndb_delete(const char *name);
extern int stndb_write(const struct stndb_entry *entry);
extern int stndb_retrieve(struct stndb_entry *entry);
extern GList *stndb_list(GList *list);
extern void stndb_clist(GtkCList *clist);

/* ---------------------------------------------------------------------- */
#endif /* _TRXAPP_H */
