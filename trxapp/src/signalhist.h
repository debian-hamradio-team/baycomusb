/* 
 * Signal History Widget
 * Copyright (C) 2000 Thomas Sailer <sailer@ife.ee.ethz.ch>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SIGNALHISTORY_H__
#define __SIGNALHISTORY_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SIGNALHISTORY(obj)          GTK_CHECK_CAST(obj, signalhistory_get_type(), Signalhistory)
#define SIGNALHISTORY_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, signalhistory_get_type(), SignalhistoryClass)
#define IS_SIGNALHISTORY(obj)       GTK_CHECK_TYPE(obj, signalhistory_get_type())

typedef struct _Signalhistory        Signalhistory;
typedef struct _SignalhistoryClass   SignalhistoryClass;

#define SIGNALHISTORY_NUMSAMPLES  256

#define SIGNALHISTORY_WIDTH       (SIGNALHISTORY_NUMSAMPLES)
#define SIGNALHISTORY_HEIGHT      64

struct _Signalhistory 
{
	GtkWidget widget;

	guint idlefunc;
	GdkGC *col0_gc;
	GdkGC *col1_gc;
        GdkGC *col2_gc;
	GdkColor col0;
	GdkColor col1;
	GdkColor col2;

	GdkPixmap *pixmap;

	unsigned int ptr;
	struct signalhistdata {
		unsigned char sample;
		unsigned char col;
	} data[SIGNALHISTORY_WIDTH];
};

struct _SignalhistoryClass
{
	GtkWidgetClass parent_class;
};


guint signalhistory_get_type(void);
GtkWidget* signalhistory_new(gchar *name, gchar *string1, gchar *string2, gint int1, gint int2);
void signalhistory_newdata(Signalhistory *signalhistory, unsigned char sample, unsigned char col);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __SIGNALHISTORY_H__ */
