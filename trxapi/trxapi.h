/*****************************************************************************/

/*
 *      trxapi.h  --  Transceiver control API.
 *
 *      Copyright (C) 2000, 2001
 *          Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 *  History:
 *   0.1  19.09.2000  Created
 *
 */

/*****************************************************************************/

#ifndef _TRXAPI_H
#define _TRXAPI_H

/* --------------------------------------------------------------------- */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sysdeps.h"
//#include "trxctrl.h"

/* --------------------------------------------------------------------- */

typedef u_int64_t trxapi_frequency_t;
typedef float trxapi_signalstrength_t;

struct trxapi_banddescription {
	trxapi_frequency_t low;  /* lower band edge */
	trxapi_frequency_t high; /* upper band edge */
	trxapi_frequency_t step; /* step size */
};

struct trxapi_description {
	char manufacturer[256];    /* transceiver manufacturer */
	char model[256];           /* model number */
	char copyright[256];
	char instid[16];           /* serial number or similar */
	unsigned int nrrxbands;
	struct trxapi_banddescription *rxbands;
	unsigned int nrtxbands;
	struct trxapi_banddescription *txbands;
	trxapi_signalstrength_t rssimin;
	trxapi_signalstrength_t rssimax;
};

typedef enum {
	trxapi_mode_unspec, trxapi_mode_packet, trxapi_mode_audio
} trxapi_mode_t;

struct trxapi_state {
	trxapi_mode_t mode;
	trxapi_frequency_t rxfreq;
	trxapi_frequency_t txfreq;
	unsigned int ptt : 1;
	unsigned int manualptt : 1;
	unsigned int dcd : 1;
	trxapi_signalstrength_t rssi;	
};

typedef enum {
	trxapi_baycomusb_mode_fsk,
	trxapi_baycomusb_mode_external,
	trxapi_baycomusb_mode_afsk,
	trxapi_baycomusb_mode_audio
} trxapi_baycomusb_mode_t;

typedef enum {
	trxapi_baycomusb_loopback_off,
	trxapi_baycomusb_loopback_scrambler,
	trxapi_baycomusb_loopback_modem
} trxapi_baycomusb_loopback_t;

typedef char trxapi_audiodevice_t[64];

struct trxapi_baycomusb_adapter_config {
	trxapi_baycomusb_mode_t mode;
	unsigned long fclk;
	unsigned long bitraterx;
	unsigned long bitratetx;
	trxapi_baycomusb_loopback_t loopback;
	unsigned int pttmute : 1;
	unsigned long filtmode;
	/* audio stuff */
	unsigned long samplerate;
	float gain;
	trxapi_audiodevice_t audiodevin;
	trxapi_audiodevice_t audiodevout;
	trxapi_signalstrength_t rfsquelch;
	unsigned int audiosquelch : 1;
};

struct trxapi_baycomusb_adapter_audio_devs {
	unsigned int nraudiodevsin;
	trxapi_audiodevice_t *audiodevsin;
	unsigned int nraudiodevsout;
	trxapi_audiodevice_t *audiodevsout;
};

struct trxapi_channel_access_config {
	unsigned long txdelay;
	unsigned long slottime;
	unsigned long ppersistence;
	unsigned long txtail;
	unsigned int fullduplex : 1;
};

typedef u_int8_t trxapi_ip_address_t[4];

struct trxapi_interface_config {
	char ifname[16];
	char hwaddr[16];
	trxapi_ip_address_t ipaddr;
	trxapi_ip_address_t netmask;
	trxapi_ip_address_t broadcast;
	char gwhwaddr[128];
	trxapi_ip_address_t gwipaddr;
	trxapi_ip_address_t gwnetmask;
	char gwipmode;
};

typedef enum {
	trxapi_modem_disconnect_rxc_normal,
	trxapi_modem_disconnect_rxc_rxclock
} trxapi_modem_disconnect_rxc_mode_t;

typedef enum {
	trxapi_modem_disconnect_txc_normal,
	trxapi_modem_disconnect_txc_txclock
} trxapi_modem_disconnect_txc_mode_t;

typedef enum {
	trxapi_modem_disconnect_txd_normal,
	trxapi_modem_disconnect_txd_txdata
} trxapi_modem_disconnect_txd_mode_t;

struct trxapi_modem_disconnect_config {
	unsigned long output;
	unsigned long direction;
	trxapi_modem_disconnect_rxc_mode_t rxc;
	trxapi_modem_disconnect_txc_mode_t txc;
	trxapi_modem_disconnect_txd_mode_t txd;
};

struct trxapi_audio_state {
	unsigned long samplerate;
	unsigned long micbufsz;
	unsigned long micbufptr;
	unsigned long spkbufsz;
	unsigned long spkbufptr;
	float micsigpwr;
	float spksigpwr;
	float spknoisepwr;
};

struct trxapi;

/* --------------------------------------------------------------------- */

extern int trxapi_init(int *argc, char **argv);
extern struct trxapi *trxapi_open_byindex(unsigned int index);
extern void trxapi_close(struct trxapi *api);

extern const struct trxapi_description *trxapi_get_description(struct trxapi *api);
extern int trxapi_get_state(struct trxapi *api, struct trxapi_state *state);
extern int trxapi_set_ptt(struct trxapi *api, unsigned int ptt);
extern int trxapi_set_frequency(struct trxapi *api, trxapi_frequency_t rx, trxapi_frequency_t tx);

/* Raw UART */
extern int trxapi_uart_send(struct trxapi *api, const char *str);
extern int trxapi_uart_receive(struct trxapi *api, unsigned long *ptr, char *str, unsigned int strsz);

/* Configuration stuff */
extern int trxapi_baycomusb_adapter_get_config(struct trxapi *api, struct trxapi_baycomusb_adapter_config *cfg);
extern int trxapi_baycomusb_adapter_set_config(struct trxapi *api, const struct trxapi_baycomusb_adapter_config *cfg);
extern const struct trxapi_baycomusb_adapter_audio_devs *trxapi_baycomusb_adapter_get_audiodevices(struct trxapi *api);

extern int trxapi_channel_access_get_config(struct trxapi *api, struct trxapi_channel_access_config *cfg);
extern int trxapi_channel_access_set_config(struct trxapi *api, const struct trxapi_channel_access_config *cfg);

extern int trxapi_interface_get_config(struct trxapi *api, struct trxapi_interface_config *cfg);
extern int trxapi_interface_set_config(struct trxapi *api, const struct trxapi_interface_config *cfg);

extern int trxapi_modem_disconnect_get_config(struct trxapi *api, struct trxapi_modem_disconnect_config *cfg);
extern int trxapi_modem_disconnect_set_config(struct trxapi *api, const struct trxapi_modem_disconnect_config *cfg);

/* Audio Stuff */
extern int trxapi_get_audio_state(struct trxapi *api, struct trxapi_audio_state *state);
extern int trxapi_get_audio_mic_samples(struct trxapi *api, unsigned long ptr, unsigned long len, unsigned char *samples);
extern int trxapi_get_audio_spk_samples(struct trxapi *api, unsigned long ptr, unsigned long len, unsigned char *samples);
extern int trxapi_set_audio_dtmf(struct trxapi *api, long ch);

/* --------------------------------------------------------------------- */
#endif /* _TRXAPI_H */
