/*****************************************************************************/

/*
 *      trxapi.h  --  Transceiver control API.
 *
 *      Copyright (C) 2000, 2001
 *          Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Please note that the GPL allows you to use the driver, NOT the radio.
 *  In order to use the radio, you need a license from the communications
 *  authority of your country.
 *
 *  History:
 *   0.1  19.09.2000  Created
 *
 */

/*****************************************************************************/

#ifndef _WIN32CLIENT_H
#define _WIN32CLIENT_H

/* --------------------------------------------------------------------- */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "trxapi.h"

/* --------------------------------------------------------------------- */

struct trxapi {
	struct trx_thread_state *state;
        HANDLE hmutex;
        HANDLE hflxtxevent;	
        HANDLE hflxrxevent;	

        struct trxapi_description *desc;
        struct trxapi_baycomusb_adapter_audio_devs *audiodevs;
};

extern struct trxapi *win32shmem_open_byindex(unsigned int index);
extern void win32shmem_close(struct trxapi *api);

extern void win32shmem_lock(struct trxapi *api);
extern void win32shmem_unlock(struct trxapi *api);
extern void win32shmem_wakeup(struct trxapi *api);

extern int win32shmem_init(int *argc, char **argv);

extern const struct trxapi_description *win32shmem_get_description(struct trxapi *api);
extern int win32shmem_get_state(struct trxapi *api, struct trxapi_state *state);
extern int win32shmem_set_ptt(struct trxapi *api, unsigned int ptt);
extern int win32shmem_set_frequency(struct trxapi *api, trxapi_frequency_t rx, trxapi_frequency_t tx);

/* Raw UART */
extern int win32shmem_uart_send(struct trxapi *api, const char *str);
extern int win32shmem_uart_receive(struct trxapi *api, unsigned long *ptr, char *str, unsigned int strsz);

/* Configuration stuff */
extern int win32shmem_baycomusb_adapter_get_config(struct trxapi *api, struct trxapi_baycomusb_adapter_config *cfg);
extern int win32shmem_baycomusb_adapter_set_config(struct trxapi *api, const struct trxapi_baycomusb_adapter_config *cfg);
extern const struct trxapi_baycomusb_adapter_audio_devs *win32shmem_baycomusb_adapter_get_audiodevices(struct trxapi *api);

extern int win32shmem_channel_access_get_config(struct trxapi *api, struct trxapi_channel_access_config *cfg);
extern int win32shmem_channel_access_set_config(struct trxapi *api, const struct trxapi_channel_access_config *cfg);

extern int win32shmem_interface_get_config(struct trxapi *api, struct trxapi_interface_config *cfg);
extern int win32shmem_interface_set_config(struct trxapi *api, const struct trxapi_interface_config *cfg);

extern int win32shmem_modem_disconnect_get_config(struct trxapi *api, struct trxapi_modem_disconnect_config *cfg);
extern int win32shmem_modem_disconnect_set_config(struct trxapi *api, const struct trxapi_modem_disconnect_config *cfg);

/* Audio Stuff */
extern int win32shmem_get_audio_state(struct trxapi *api, struct trxapi_audio_state *state);
extern int win32shmem_get_audio_mic_samples(struct trxapi *api, unsigned long ptr, unsigned long len, unsigned char *samples);
extern int win32shmem_get_audio_spk_samples(struct trxapi *api, unsigned long ptr, unsigned long len, unsigned char *samples);
extern int win32shmem_set_audio_dtmf(struct trxapi *api, long ch);

/* --------------------------------------------------------------------- */
#endif /* _WIN32CLIENT_H */
